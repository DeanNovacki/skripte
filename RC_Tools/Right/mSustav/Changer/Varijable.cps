Program Changes;  


{$I ..\..\..\include\rcMessages.cps}
{$I ..\..\..\include\rcPrint.cps}
{$I ..\..\..\include\rcStrings.cps}
{$I ..\..\..\include\rcControls.cps}
{$I ..\..\..\include\rcObjects.cps}
{$I ..\..\..\include\rcEvents.cps}
{$I ..\..\..\include\rcCurves.cps}
{$I ..\..\..\include\rcMath.cps}
         
var
   Prozor:TForm;
   
   Lab: TLabel;
	
	SB:TScrollBox;
	
	PanCap, PanVar:TPanel;
		CheckVar:TCheckBox;
		VarFind, VarReplace : TEdit;

	PanVarFind:TPanel;
		CheckVarFind: TCheckBox;
		VarFindOnly:tEdit;
		
	PanDecFind:TPanel;
		CheckDecFind:TCheckBox;
		EditDecFind : TEdit;
		
	PanDelVar:TPanel;
		CheckDelVar:TCheckBox;
		VarDel : TEdit;		
		
	PanOK:TPanel;
			ButOK, ButCancel: TButton;
	razmak: string;

Procedure promijeni_varijablu(elem3:TElement);
var
	i:integer;
Begin
	Print (Razmak+elem3.naziv);
	
	For i:=0 to elem3.fVarijable.count-1 do begin
		if elem3.fVarijable[i]=VarFind.text then begin
			Print (razmak + '==>      Na�ao sam: "'+ elem3.Fvarijable[i]+'"');
			elem3.fVarijable[i]:=VarReplace.text;
			Print (razmak + '==> Promijenjeno u: "'+ elem3.Fvarijable[i]+'"');
		end;

	end;
	
End;

Procedure nadji_varijablu(elem3:TElement);
var
	i:integer;
Begin
	Print (Razmak+elem3.naziv);
	
	// For i:=0 to elem3.fVarijable.count-1 do begin
		if elem3.fVarijable.isvar(VarFindOnly.text) then begin
			Print (razmak + '==>      Na�ao sam: "'+ VarFindOnly.text+'"');
			// elem3.fVarijable[i]:=VarReplace.text;
			// Print (razmak + '==> Promijenjeno u: "'+ elem3.Fvarijable[i]+'"');
		end;

	// end;
	
End;

Procedure obrisi_varijablu(elem3:TElement);
var
	i:integer;
Begin
	Print (Razmak+elem3.naziv);
	
	// For i:=0 to elem3.fVarijable.count-1 do begin
		if elem3.fVarijable[i]=VarDel.text then begin
			Print (razmak + '==>      Na�ao sam: "'+ VarDel.text+'"');
			// elem3.fVarijable[i]:=VarReplace.text;
			elem3.fVarijable.delete(i);
			Print (razmak + '==> Obrisano!');
		end;

	// end;
	
End;

Procedure nadji_dekor(elem3:TElement);
var
	i:integer;
	d1:TDaska;
Begin
	Print (Razmak+'E:'+elem3.naziv);
	
	For i:=0 to elem3.childdaska.count-1 do begin
		d1:=elem3.childdaska.daska[i];
		Print (Razmak+'  d:'+d1.naziv+'  Dekor: "'+ IntToStr(d1.TexIndex));
		if d1.TexIndex=StrToInt(EditDecFind.text) then begin
			Print (razmak + razmak + '==>      Na�ao sam dekor: "'+ IntToStr(d1.TexIndex));
		end;

	end;
	
End;	
	
Procedure Obrada_elementa(elem2:TElement);
// Rekurzivna procedura koja obra�uje sve elemente i elemente unutar njih

var
   i:integer;
   tmpEl:Telement;

begin
	// print('Obrada elementa start');
	// if Elem2.ElmList.Count=0 then Print('U elementu nema elemenata!');
	
	
	if CheckVar.checked then promijeni_varijablu(Elem2);
	if CheckDelVar.checked then obrisi_varijablu(Elem2);
	if CheckVarFind.checked then nadji_varijablu(Elem2);
	if CheckDecFind.checked then nadji_dekor(Elem2);
	
	for i:=0 to Elem2.ElmList.Count-1 do begin
		
		tmpEl:=TElement(Elem2.ElmList.Items[i]);
		razmak:=razmak+'  ';			// za uvla�enje ispisa podelemenata
		
		// Obrada_varijabli(tmpEl);
			
		// rekurzija
		Obrada_elementa(tmpEl);
		
		
	end;
	
	razmak:=copy(razmak,1,length(razmak)-2);
	
	// print('Obrada projekta end');
	
end;

procedure obrada_projekta(sender:TObject);
var
	i:integer;
	elem:TElement;
Begin
	// print('Obrada projekta start');
  
	for i:=0 to ElmList.Count-1 do begin
		elem:=TElement(ElmList.Items[i]);
		
		if  elem.selected then begin
				Obrada_elementa(elem);
			end else begin
				// print ('Nije selektiran '+elem.Naziv);
		end;
	end;

	// print('Obrada projekta end');
End;

procedure kreiraj_formu;
Begin
	Prozor:=rcTool('Izmijeni_elemente');
	// Prozor.BorderStyle:=bsSizeToolWin;
	Prozor.Position:=poScreenCenter;
	Prozor.Width:=510;
	Prozor.Height:=500;
	
	// Uputa
	
	PanCap:=rcPanel('pan_varijable',Prozor);
	PanCap.Height:=105; 
	PanCap.Top:=2;
	PanCap.Align:=alTop;
	Lab:=rcLabel( 'll2', 'opis', PanCap, 10, 10, taLeftJustify);
	Lab.Font.Size:=10;
	Lab.Caption:='Ovom skriptom se obra�uju SELEKTIRANI elementi i svi njihovi podelementi ' + #13#10 + 
					 'ili daske koji zadovoljavaju navedeni uvijet. ' + #13#10 + 
					 #13#10 + 
					 'Za kontrolu u�injenog preporu�a se otvoriti konzolni prozor koji se aktivira' + #13#10 + 
					 'pomo�u skripte RC Console (unutar grupe Sustav) u desnom RC izborniku.';

	
	// Scroll Box
	
	SB:=rcScrollBox( 'SB', Prozor);
	SB.Top:=200;
	SB.Align:=alClient;
	
	// Na�i varijablu
	
	PanVarFind:=rcPanel('panel_Var_Find',SB);
	PanVarFind.Height:=75; 
	PanVarFind.Top:=2000;
	PanVarFind.Align:=alTop;
	
	CheckVarFind:=rcCheckBox('chvarfind', 'Na�i varijablu',PanVarFind, 10, 10);
	CheckVarFind.Alignment:=taRightJustify;
	CheckVarFind.width:=300;
	
	VarFindOnly:=rcEditL ('Na�i varijablu: ','', PanVarFind, 250, 35, 'right');
	VarFindOnly.text:='Naziv_Varijable';
	VarFindOnly.Width:=250;  
	
	// Zamijeni varijablu i vrijednost
	
	PanVar:=rcPanel('panel_varijable',SB);
	PanVar.Height:=95; PanVar.Top:=2000;
	PanVar.Align:=alTop;
	
	CheckVar:=rcCheckBox('chvar', 'Izmjena vrijednosti varijabli',PanVar, 10, 10);
	CheckVar.Alignment:=taRightJustify;
	CheckVar.width:=300;
	
	VarFind:=rcEditL ('Na�i varijablu i vrijednost: ','', PanVar, 250, 35, 'right');
	VarFind.text:='Naziv_Varijable=Vrijednost + 123';
	VarFind.Width:=250;                                        
	VarReplace:=rcEditL('i zamijeni sa: ','', PanVar, 250, 60, 'right');
	VarReplace.Width:=250;                                        
	
	// obri�i varijablu
	
	PanDelVar:=rcPanel('panel_Del_var',SB);
	PanDelVar.Height:=95; PanDelVar.Top:=2000;
	PanDelVar.Align:=alTop;
	
	CheckDelVar:=rcCheckBox('chvar', 'Brisanje varijabli',PanDelVar, 10, 10);
	CheckDelVar.Alignment:=taRightJustify;
	CheckDelVar.width:=270;
	
	VarDel:=rcEditL ('Obri�i varijablu i vrijednost: ','', PanDelVar, 250, 35, 'right');
	VarDel.text:='Naziv_Varijable=Vrijednost + 123';
	VarDel.Width:=250;
	
	// Na�i dekor
	
	PanDecFind:=rcPanel('panel_Dec_find',SB);
	PanDecFind.Height:=70; 
	PanDecFind.Top:=2000;
	PanDecFind.Align:=alTop;
	
	CheckDecFind:=rcCheckBox('chdecfind', 'Na�i dekor',PanDecFind, 10, 10);
	CheckDecFind.Alignment:=taRightJustify;
	CheckDecFind.width:=270;
	
	EditDecFind:=rcEditL ('Na�i dekor broj: ','', PanDecFind, 250, 35, 'right');
	EditDecFind.text:='123';
	EditDecFind.Width:=50;
	
	// Na�i password
	
	{
	
	PanPassFind:=rcPanel('panel_Pass_find',SB);
	PanPassFind.Height:=70; 
	PanPassFind.Top:=2000;
	PanPassFind.Align:=alTop;
	
	CheckPassFind.Alignment:=taRightJustify;
	CheckPassFind:=rcCheckBox('chpassfind', 'Na�i pass',PanPassFind, 10, 10);
	CheckPassFind.width:=270;
	
	EditPassFind:=rcEditL ('Na�i dekor broj: ','', PanDecFind, 250, 35, 'right');
	EditDecFind.text:='123';
	EditDecFind.Width:=50;
	
	}
	
	// OK CANCEL tipke
	
	PanOK:=rcPanel('pan_ok',Prozor);
	PanOK.Height:=50; 
	PanOK.Top:=2000;
	PanOK.Align:=alBottom;

	ButOK:=rcButton('OK','OK',PanOK,350,10);
	ButCancel:=rcButton('Cancel','Cancel',PanOK,150,10);
		
	ButOk.default:=True;
	ButOk.OnClick:=@Obrada_Projekta;
	ButCancel.ModalResult:=mrCancel;
	
End;


Begin
  print('---------------------------');
  printTime('Procedura VArijable Start');
  
  Kreiraj_formu;
  
  prozor.showmodal;
  Prozor.free;
  printTime('Procedura Varijable End');
End.
