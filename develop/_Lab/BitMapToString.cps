..
const
  BPP = 8; //Note: BYTES per pixel

{---------------------------------------------------------}
function BitmapToString(Bitmap: TBitmap): String;
var
  x, y: Integer;
  S: String;

begin
  S := '';
  for y := 0 to Bitmap.Height-1 do
    begin
      for x := 0 to Bitmap.Width-1 do
        begin
          S := S + IntToHex(Bitmap.Canvas.Pixels[x,y], BPP);
        end;
      S := S + '\';
    end;
  Result := S;
end;
{---------------------------------------------------------}
function StringToBitmap(S: String): TBitmap;
var
  Bitmap: TBitmap;
  Line: String;
  P: Integer;
  x, y: Integer;

begin
  Bitmap := TBitmap.Create;
  P := pos('\', S);
  Bitmap.Width := P div BPP;
  Bitmap.Height := 1;

  Line := Copy(S, 1, P-1);
  Delete(S, 1, P);
  x := 0;
  y := 0;

  While P <> 0 do
    begin
      if length(Line) <> 0 then
        begin
          Bitmap.Canvas.Pixels[x, y] := StrToInt('$' + Copy(Line, 1, BPP));
          Delete(Line, 1, BPP);
          inc(x);
        end
      else
        begin
          P := pos('\', S);
          Line := Copy(S, 1, P-1);
          Delete(S, 1, P);
          inc(y);
          Bitmap.Height := y+1;
          x := 0;
        end;
    end;
  Bitmap.Height := Bitmap.Height - 1;
  Result := Bitmap;
end;
{---------------------------------------------------------}
procedure TForm1.Button1Click(Sender: TObject);
var
  S: String;
begin
  S := BitmapToString(Image1.Picture.Bitmap);
  Image2.Picture.Bitmap := StringToBitmap(S);
end;
{---------------------------------------------------------}
