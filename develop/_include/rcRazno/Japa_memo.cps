procedure memo_srednji(docname:string);
// pdf_memo_srednji

Begin
   try  
        pdf.bitmapout(30,20,200,29,PrgFolder+'\info\Japa_Logo.jpg');
   except
        showmessage('Nisam na�ao sliku "PrgFolder\info\Japa_Logo.jpg"!'); 
   end;

	
   pdf.FontStyles:=[];
   PDF.setRgbcolor(255,255,255);
   PDF.LineWidth:=0.01;
   PDF.DrawLine(20,55,pw-30,55);
	
	pdf.setfont('Arial',7);
   pdf.TextBox( PDF.PageWidth-30,25,0,taRightJustify,'Glogove�ka 16, 10020 ZAGREB');
   pdf.TextBox( PDF.PageWidth-30,33,0,taRightJustify,'MB: 2253828, OIB: 78193693257');
   pdf.TextBox( PDF.PageWidth-30,41,0,taRightJustify,'Tel.:01 561 561 3, Fax: 01 561 33 19');
   pdf.TextBox( PDF.PageWidth-30,49,0,taRightJustify,'IBAN HR9423600001102268848');
   pdf.setfont('Arial',5);
   PDF.setRgbcolor(255,255,255);
   pdf.TextBox( PDF.PageWidth-30,60,0,taRightJustify,docname);
   
End;

procedure memo_srednji_iso(docname,author,version,date,owner:string);
// pdf_memo_srednji

Begin
   try  
        pdf.bitmapout(30,20,200,29,PrgFolder+'\info\Japa_Logo.jpg');
   except
        showmessage('Nisam na�ao sliku "PrgFolder\info\Japa_Logo.jpg"!'); 
   end;

	
   pdf.FontStyles:=[];
   PDF.setRgbcolor(255,255,255);
   PDF.LineWidth:=0.01;
   PDF.DrawLine(20,55,pw-30,55);
	
	PDF.DrawLine(20,69,PDF.PageWidth-30,69);
	pdf.setfont('Arial',4);
	PDF.TextOut(30,59,'dokument');
	PDF.TextOut(200,59,'autor');
	PDF.TextOut(350,59,'indeks');
	PDF.TextOut(400,59,'datum');
	pdf.TextBox( PDF.PageWidth-30,59,0,taRightJustify,#169);
	
	pdf.setfont('Arial',8);
	PDF.TextOut(30,67,docname);
	PDF.TextOut(200,67,author);
	PDF.TextOut(350,67,version);
	PDF.TextOut(400,67,date);
	pdf.TextBox( PDF.PageWidth-30,67,0,taRightJustify,owner);
	
{
	
	pdf.setfont('Arial',7);
   pdf.TextBox( PDF.PageWidth-30,25,0,taRightJustify,'Glogove�ka 16, 10020 ZAGREB');
   pdf.TextBox( PDF.PageWidth-30,33,0,taRightJustify,'MB: 2253828, OIB: 78193693257');
   pdf.TextBox( PDF.PageWidth-30,41,0,taRightJustify,'Tel.:01 561 561 3, Fax: 01 561 33 19');
   pdf.TextBox( PDF.PageWidth-30,49,0,taRightJustify,'IBAN HR9423600001102268848');
   pdf.setfont('Arial',5);
   PDF.setRgbcolor(255,255,255);
   pdf.TextBox( PDF.PageWidth-30,60,0,taRightJustify,docname);
}
   
End;


procedure memo_mali(docname:string);
// pdf_memo_srednji

Begin
   try  
        pdf.bitmapout(30,20,100,14,PrgFolder+'\info\Japa_Logo.jpg');
   except
        showmessage('Nisam na�ao sliku "PrgFolder\info\Japa_Logo.jpg"!'); 
   end;

	
   pdf.FontStyles:=[];
   PDF.setRgbcolor(255,255,255);
   PDF.LineWidth:=0.01;
   PDF.DrawLine(20,36,PDF.PageWidth-30,36);
	
   pdf.setfont('Arial',5);
   PDF.setRgbcolor(255,255,255);
   pdf.TextBox( PDF.PageWidth-30,41,0,taRightJustify,docname);
End;

procedure memo_mali_iso(docname,author,version,date,owner:string);
// pdf_memo_srednji

Begin
   try  
        pdf.bitmapout(30,20,100,14,PrgFolder+'\info\Japa_Logo.jpg');
   except
        showmessage('Nisam na�ao sliku "PrgFolder\info\Japa_Logo.jpg"!'); 
   end;

	
   pdf.FontStyles:=[];
   PDF.setRgbcolor(255,255,255);
   PDF.LineWidth:=0.01;
   PDF.DrawLine(20,36,PDF.PageWidth-30,36);
	PDF.DrawLine(20,50,PDF.PageWidth-30,50);
	pdf.setfont('Arial',4);
	PDF.TextOut(30,40,'dokument');
	PDF.TextOut(200,40,'autor');
	PDF.TextOut(350,40,'indeks');
	PDF.TextOut(400,40,'datum');
	pdf.TextBox( PDF.PageWidth-30,40,0,taRightJustify,#169);
	
	pdf.setfont('Arial',8);
	PDF.TextOut(30,48,docname);
	PDF.TextOut(200,48,author);
	PDF.TextOut(350,48,version);
	PDF.TextOut(400,48,date);
	pdf.TextBox( PDF.PageWidth-30,48,0,taRightJustify,owner);
	

End;

Procedure PrintFooter(ftext:string);
begin
	PDF.setRgbcolor(255,255,255);
	PDF.LineWidth:=0.01;
	// v     PDF.
     PDF.DrawLine(20,PDF.PageHeight-25,PDF.PageWidth-30,PDF.PageHeight-25);     
     pdf.setfont('Arial',5);
     PDF.Textbox(PDF.PageWidth/2,PDF.PageHeight-20,0,tacenter,ftext);
end;

Procedure PrintSignature(sX,sY:Integer);
Begin
   pdf.FontStyles:=[];
   PDF.setRgbcolor(255,255,255);
   PDF.LineWidth:=0.01;
	PDF.DrawLine(sx,sy,PDF.PageWidth-sx,sy);
	PDF.DrawLine(sx,sy+30,PDF.PageWidth-sx,sy+30);
	pdf.setfont('Arial',7);
	PDF.TextOut(sx,sy+7,'obradio:');
	PDF.TextOut(sx+130,sy+7,'datum:');
	PDF.TextOut(350,sy+7,'kontrolirao:');
	PDF.TextOut(485,sy+7,'datum');
	// pdf.TextBox( PDF.PageWidth-30,40,0,taRightJustify,#169);
	{
	pdf.setfont('Arial',8);
	PDF.TextOut(30,48,docname);
	PDF.TextOut(200,48,author);
	PDF.TextOut(350,48,version);
	PDF.TextOut(400,48,date);
	pdf.TextBox( PDF.PageWidth-30,48,0,taRightJustify,owner);
	}
End;