// rotacija vektora u prostoru
// slu�i za rotaciju oko nulte to�ke po nekoj osi (1=Y)
// ulazni podaci: vektor to�ke (x,y,z), kut
// izlazni podaci: vektor to�ke

type
 //THomogeneousFltVector = array[0..3] of Single; 	
 THomogeneousFltMatrix  = array[0..3] of THomogeneousFltVector;
 TMatrix = THomogeneousFltMatrix;
 // TVector4f = THomogeneousFltVector;
 // TAffineFltVector = array[0..2] of Single;
 // TVector3f = TAffineFltVector;

const
     x=0;
     y=1;
     z=2;
     w=3;
    
//----------------------------------------------------------------------------------------------------------------------

function CreateRotationMatrixY(Sine, Cosine: Single): TMatrix; 

// creates matrix for rotation about y-axis

begin
  // Result := EmptyMatrix;
  Result[0][X] := Cosine;
  Result[X][Z] := -Sine;
  Result[Y][Y] := 1;
  Result[Z][X] := Sine;
  Result[Z][Z] := Cosine;
  Result[W][W] := 1;
end;

function VectorTransform(V: TVector4f; M: TMatrix): TVector4f; 


// transforms a homogeneous vector by multiplying it with a matrix

var TV: TVector4f;

begin
  TV[X] := V[X] * M[X][X] + V[Y] * M[Y][X] + V[Z] * M[Z][X] + V[W] * M[W][X];
  TV[Y] := V[X] * M[X][Y] + V[Y] * M[Y][Y] + V[Z] * M[Z][Y] + V[W] * M[W][Y];
  TV[Z] := V[X] * M[X][Z] + V[Y] * M[Y][Z] + V[Z] * M[Z][Z] + V[W] * M[W][Z];
  TV[W] := V[X] * M[X][W] + V[Y] * M[Y][W] + V[Z] * M[Z][W] + V[W] * M[W][W];
  Result := TV
end;




//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------


function rotateVector(Vect:tvector4f;angle:single;osrotacije:byte):Tvector4f;
var
   // vector:TAffineFltVector;
   m:tmatrix;
   asin,acos:single;
begin
     asin:=sin(degtorad(angle));
     acos:=cos(degtorad(angle));
     case osrotacije of
          // 0:m:=createRotationMatrixX(asin,acos);
          1:m:=createRotationMatrixY(asin,acos);
          // 2:m:=createRotationMatrixZ(asin,acos);
     end;
     Result:=VectorTransform(Vect,m);
end;


function rotateVector2(Vect,Centar:tvector4f;angle:single):Tvector4f;
// rotacija to�ke Vect oko to�ke Cent za kut Angle (oko Y osi)
var
  Vect_priv,Temp_Result:tvector4f;
 begin
  Vect_priv[0]:=Vect[0]-Centar[0];
  Vect_priv[1]:=Vect[1]-Centar[1];        
  Vect_priv[2]:=Vect[2]-Centar[2]; 
  Temp_Result:=rotateVector(Vect_priv,angle,1);
  Result[0]:=Temp_Result[0]+Centar[0];
  Result[1]:=Temp_Result[1]+Centar[1];
  Result[2]:=Temp_Result[2]+Centar[2];

end;
