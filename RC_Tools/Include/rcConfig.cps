﻿{
function find_value_int(FileName,ValueName:string):integer;
function find_value_str(FileName,ValueName:string):string;
function set_value_str(FileName,ValueName, Value :string):boolean;
function set_value_int(FileName,ValueName :string; Value:Integer):boolean;
}


Function rcFileCheck(file_name:string):boolean;
// provjerava da li postoji datoteka
// ako ne postoji, daje poruku 
Begin
	// Showmessage ('fc1' +#13#10+ file_name)	;
  	if not (FileExists(file_name)) then begin
			// Showmessage ('fc2 NE POSTOJI' +#13#10+ file_name)	;
			// Showmessage ('Nema datoteke:' +#13#10+file_name+#13#10+#13#10
			//				+'Prekidam izvođenje skripte!'+#13#10+#13#10
			//				+'Doviđenja!');
			result:=false;
		end else begin
			// Showmessage ('fc2 POSTOJI' +#13#10+ file_name)	;
			// Showmessage ('Postoji datoteka:' +#13#10+ file_name);
			result:=true;
	
	end;
	// Showmessage ('fc3 IZLAZ' +#13#10+ file_name)	;
end;


function find_value_int(FileName,ValueName:string):integer;
// Find property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if ValueName not found or result is not integer, return 0 (zero)
// if ValueName found, but without value, ValueName will be deleted, and file will be saved
var
   list:TStringList;
   ind:integer;
   Value,ResultValue:string;
Begin
   
   list:=TStringList.Create;
   try list.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_int'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   value:=list.Values[ValueName];          // BUT, in this case value will be ''
   if (ind>=0) and (value='') then begin                      //  Name exist but no value
                List.Delete(List.IndexOfName(ValueName));     // delete name from list
                list.SaveToFile(FileName);                    // save new list to file
                // search again!!
                ind:=List.IndexOfName(ValueName);             // search again :-)
                value:=list.Values[ValueName];
   end; // if 
	
   if (ind>=0) and (not(value='')) then ResultValue:=value
                                   else begin  
                                      // ShowMessage( 'Function: Find_value_int'+#13#10+'Cannot find name "'+ ValueName+'" in file "'+FileName+'"');
                                      ResultValue:='0';
                                   end;
   // try to convert to integer   
   try 
       Result:=StrToInt(ResultValue); 
   except 
       Result:=0; 
       Showmessage( 'Value "'+ValueName+'" in file "'+FileName+'" cannot be converted to integer number!'+#10#13+'Assigning zero');   
   end;

   
end;   // function find_value_int 
 
function find_value_str(FileName,ValueName:string):string;
// Find property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if ValueName not found return ' '
// if ValueName found, but without value, ValueName will be deleted, and file will be saved
var
   list:TStringList;
   ind:integer;
   Value,ResultValue:string;
Begin
	// print('f(x): find_value_str Start');
	// print('ulaz: '+FileName+' | '+ValueName);
   list:=TStringList.Create;
   try list.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   value:=list.Values[ValueName];          // BUT, in this case value will be ''
   if (ind>=0) and (value='') then begin                      //  Name exist but no value
                List.Delete(List.IndexOfName(ValueName));     // delete name from list
                list.SaveToFile(FileName);                    // save new list to file
                // search again!!
                ind:=List.IndexOfName(ValueName);             // search again :-)
                value:=list.Values[ValueName];
   end; // if 
   if (ind>=0) and (not(value='')) then ResultValue:=value
                                   else begin  
                                      // ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find name "'+ ValueName+'" in file "'+FileName+'"');
                                      ResultValue:=' ';
                                   end;



   Result:=ResultValue; 
   
	// print('result: '+result);
   // print('f(x): find_value_str End');   
end;   // function find_value_int 


function rcFindSS(FileName,SectionName,ValueName:string):string;
// FIND STRING VALUE IN SECTION
// Find property value in external file in SECTION
// FileName  = external file (with path)
// ValueName = variable name
// if ValueName not found return ' '
// if ValueName found, but without value, ValueName will be deleted, and file will be saved
var
   templist,list:TStringList;
   ind,i,j,seclen:integer;
   Value,ResultValue:string;
	SectionFound:Boolean;
Begin
	// print('f(x): rcFindSS Start');
	// print('ulaz: '+FileName+' | '+SectionName+' | '+ ValueName );
   
	
	// load file
	
	templist:=TStringList.Create;
   try templist.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
	
	// extract SECTION to List
	
	// Find Section start
	// print ('Find section start');
	seclen:=Length(SectionName); 		// duzina naziva sekcije
	// print ('1');
	// print ('Broj redova ulaza: '+IntToStr(tempList.count));
	// print ('2');
	for i:=0 to tempList.count-1 do begin
		if Copy(templist[i],1,seclen+2)='['+SectionName+']' then Begin
				// Print('Našao sam traženu sekciju!');
				SectionFound:=True;
				Break;			// Nasilni izlazak iz petlje za traženje sekcije
			end else begin
				SectionFound:=False;
		end;
	end;
	
	// Find Tekst
	
	// print ('Find tekst');
	if SectionFound then begin
			i:=i+1;				// i je brojač redova, sad ga treba pomaknuti na sljedeći red
			list:=TStringList.Create;
			for j:=i to tempList.count-1 do begin
				if  Copy(templist[j],1,seclen+1)='[' then break; 	// Izlazak jer je dosegnut kraj sekcije 
				list.add(templist[j]);
			end;
		End Else Begin
			list:=TStringList.Create;
			list.add('');
	End;
	
	
	
	
	
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   
	value:=list.Values[ValueName];          // BUT, in this case value will be ''
	
   if (ind>=0) and (value='') then begin                      //  Name exist but no value
                List.Delete(List.IndexOfName(ValueName));     // delete name from list
                list.SaveToFile(FileName);                    // save new list to file
                // search again!!
                ind:=List.IndexOfName(ValueName);             // search again :-)
                value:=list.Values[ValueName];
   end; // if 
  
  if (ind>=0) and (not(value='')) then ResultValue:=value
                                   else begin  
                                      // ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find name "'+ ValueName+'" in file "'+FileName+'"');
                                      ResultValue:=' ';
                                   end;



   Result:=ResultValue; 
   
	// print('result: '+result);
   // print('f(x): find_value_str End');   
end;   // function find_value_int 

function rcFindVar2(InTxt,fTxt:string):string;
// Ulaz 1: JEDAN RED teksta
// Ulaz 2: varijable/parametra
// Izlaz: vrijednost između navodnika
// Napravljeno za *.cxm datoteke potrošnog materijala

Var
   i,j:Integer;
	tt:string;
	nasao:boolean;
Begin
	result:='';
	tt:='';
	fTxt:=fTxt+'="';
	//traženje varijable
	nasao:=false;
	
	// print(fTxt+' duzina: '+inttostr(length(fTxt)));
	for i:=1 to length(InTxt) do begin
		// print (copy(InTxt,i,Length(fTxt)));
		if copy(InTxt,i,Length(fTxt))=fTxt then begin
					nasao:=true;
					// print('Našao!');
					Break;   // izlazi iz petlje
		end;
	end; // for i
		
	// traženje ostatka do idućeg navodnika
	// i punjenje u string
	if nasao then begin
		nasao:=false;
		for j:=i+Length(fTxt) to length(inTxt) do begin
			if copy(inTxt,j,1)='"' then begin
					nasao:=true;
					Break;
				end else begin
					tt:=tt+copy(inTxt,j,1);
					
			end;
		end; // for j
	end;  // if nasao
	
	// ako je nađen drugi navodnik onda puni izlazak
	if nasao then begin
			result:=tt 
		end else begin
			result:='' 
	end;	// if nasao
	
End;  // function rcFindVar2


function set_value_str(FileName,ValueName, Value :string):boolean;
// Set property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if not succed return false

var
   list:TStringList;
   ind:integer;

Begin
   list:=TStringList.Create;
   result:=true;
   try begin 
            list.LoadFromFile(FileName);
            ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
            if ind =-1 then list.Add(ValueName+'='+Value) else list[ind]:=ValueName+'='+Value;
            list.SaveToFile(FileName);
       end; 
   except begin 
               ShowMessage( 'Function: Set_value_str'+#13#10+'Error load/save file "'+ FileName+'"!');
               result:=False;
          end;
   end;   // try

      
end;   // function set_value_str 



function rcSetSS(FileName, SectionName, ValueName,  Value :string):boolean;
// SET STRING in SECTION
// Set property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if not succed return false

var
   lista:TStringList;
   ind,i,seclen:integer;
	SectionFound : Boolean;
Begin

	// print('f(x): rcSetSS Start');
	// print('ulaz: '+FileName+' | '+SectionName+' | '+ ValueName +' | ' + Value);
   
	
	// load file
	
	lista:=TStringList.Create;
   try lista.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
	
	seclen:=Length(SectionName); 		// duzina naziva sekcije
	
	SectionFound:=False;
	
	for i:=0 to Lista.count-1 do begin
		// Print ('red '+IntToStr(i)+':'+Lista[i]);
		if Copy(lista[i],1,seclen+2)='['+SectionName+']' then Begin
				// Print('Našao sam traženu sekciju!');
				SectionFound:=True;
		end;
		
		if SectionFound then Begin
			if rcLeftSide(Lista[i])=ValueName then begin
				// Print ('Našao sam traženi izraz: '+ValueName);
				Lista[i]:=ValueName+'='+Value;
				// Print ('Novi izraz je: '+Lista[i])
			End;
			
			if  Copy(lista[i],1,seclen+1)='[' then Begin
				// Print ('Došao sam do nove sekcije');
				SectionFound:=False;
			End;		
		
		End;
		
	end;   // for i
	
	
	// zapisivanje na disk
	
	result:=true;
   try begin 
				lista.SaveToFile(FileName);
			end; 
		except begin 
         ShowMessage( 'Function: Set_value_str'+#13#10+'Error load/save file "'+ FileName+'"!');
         result:=False;
      end;
   end;   // try

end;	
{	
	// Find Tekst
	
	// print ('Find tekst');
	if SectionFound then begin
			i:=i+1;				// i je brojač redova, sad ga treba pomaknuti na sljedeći red



			
			for j:=i to tempList.count-1 do begin
				if rcLeftSide(tempList[j])=ValueName then
			
				if  Copy(templist[j],1,seclen+1)='[' then break; 	// Izlazak jer je dosegnut kraj sekcije 
				list.add(templist[j]);
			end;
		End Else Begin
			list:=TStringList.Create;
			list.add('');
	End;
	
	// test učitane liste
	for i:=0 to list.count-1 do begin
		print(list[i]);
		
	End;




   // list:=TStringList.Create;
   result:=true;
   try begin 
            // list.LoadFromFile(FileName);
            ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
            if ind =-1 then list.Add(ValueName+'='+Value) else list[ind]:=ValueName+'='+Value;
            list.SaveToFile(FileName);
       end; 
   except begin 
               ShowMessage( 'Function: Set_value_str'+#13#10+'Error load/save file "'+ FileName+'"!');
               result:=False;
          end;
   end;   // try

      
end;   // function set_value_str 

}


function set_value_int(FileName,ValueName :string; Value:Integer):boolean;
// Set property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if not succed return false

var
   list:TStringList;
   ind:integer;
Begin
   list:=TStringList.Create;
   result:=true;
   try begin 
            list.LoadFromFile(FileName);
            ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
            if ind =-1 then list.Add(ValueName+'='+IntToStr(Value)) else list[ind]:=ValueName+'='+IntToStr(Value);
            list.SaveToFile(FileName);
      end ; 
   except begin 
               ShowMessage( 'except problem:' +#13#10+
									 'rcConfig/Set_value_int'+#13#10+
									 'Error saving file: "'+ FileName+'"!');
               result:=False;
          end;
   end;   // try

end;   // function set_value_int 

procedure save_form(aform:tForm; dato:string);
var
	lista:TStringList;
Begin
	dato:=dato+'.ini';
	if not rcFileCheck(dato) then begin
												lista:=TStringList.Create;
												lista.SaveToFile(dato);
											end;
	//function set_value_int(FileName,ValueName :string; Value:Integer):boolean;
	set_value_int(dato,'Left',aForm.Left);
	set_value_int(dato,'Top',aForm.Top);
	set_value_int(dato,'Width',aForm.Width);
	set_value_int(dato,'Height',aForm.Height);
	if aForm.WindowState in [wsNormal] then set_value_str(dato,'WindowState','wsNormal');
	if aForm.WindowState=wsMinimized then set_value_str(dato,'WindowState','wsMinimized');
	if aForm.WindowState=wsMaximized then set_value_str(dato,'WindowState','wsMaximized');
	// if aForm.WindowState=wsFullScreen then set_value_str(dato,'WindowState','wsFullScreen');
	{
	Showmessage('Spremam:::   Naslov: '+aForm.caption+#13#10+
					'Left: '+IntToStr(aForm.Left)+' Top: '+IntToStr(aForm.Top) +#13#10+
					'Width: '+IntToStr(aForm.Width)+' Height: '+IntToStr(aForm.Height));
	}

End;

Procedure read_form(aform:tForm; dato:string);
var
	i,sh,sw:integer;
	lista:TStringList;
Begin
	// Print ('proc: read_form START');
	// sw:=ScreenWidth;  // zbog ovoga sve trepče pa sam izbacio!
	// sh:=ScreenHeight;
	sw:=1920;
	sh:=1080;

	If aform=nil then Showmessage('rcConfig:'+#13#10+
											'  proc: read_form:' +#13#10+
											'                   aForm je NIL!');
	
	dato:=dato+'.ini';
	// Showmessage ('dato= "'+dato+'"');
	// function find_value_int(FileName,ValueName:string):integer;
	// ako ne postoji vraća vrijednost 0
	
	// Function rcFileCheck(file_name:string):boolean;
	// Showmessage ('rf2');
	if  not rcFileCheck(dato) then begin 
		// Showmessage ('rf3');
		lista:=TStringList.Create;
		// Showmessage ('rf4');
		lista.SaveToFile(dato);
	end; 
	// Print ('a1');
	i:=find_value_int(dato,'Left');  If i=0 then aForm.Left:=1 		else aForm.Left:=i;
	i:=find_value_int(dato,'Top');   If i=0 then aForm.Top:=1 		else aForm.Top:=i;
	i:=find_value_int(dato,'Width'); If i=0 then aForm.Width:=200  else aForm.Width:=i;
	i:=find_value_int(dato,'Height');If i=0 then aForm.Height:=200 else aForm.Height:=i;
	{
	Showmessage('Čitam:::   Naslov: '+aForm.caption+#13#10+
					'Left: '+IntToStr(aForm.Left)+' Top: '+IntToStr(aForm.Top) +#13#10+
					'Width: '+IntToStr(aForm.Width)+' Height: '+IntToStr(aForm.Height));
	}
	// Print ('a2');
	
	// ako je spremljeno na računalu s dodatnim monitorom kojeg više nema
	
	if aForm.Left+aForm.Width<10 then aForm.Left:=10;				// ako je prozor na nepostojećem monitoru lijevo
	if aForm.Left>sw-50 then aForm.Left:=sw-400;						// ako je prozor na nepostojećem monitoru desno
	if aForm.Top<0 then aForm.Top:=0;									// ako je prozor na nepostojećem monitoru gore
	if aForm.Top>sh-100 then aForm.Top:=sh-aForm.Height;			// ako je prozor na nepostojećem monitoru dolje
	// Print ('a3');
	if find_value_str(dato,'WindowState')='wsNormal' then aForm.WindowState:=wsNormal;	
	if find_value_str(dato,'WindowState')='wsMinimized' then aForm.WindowState:=wsMinimized;	
	if find_value_str(dato,'WindowState')='wsMaximized' then aForm.WindowState:=wsMaximized;
	{
	Showmessage('Čitam:::   Naslov: '+aForm.caption+#13#10+
					'Left: '+IntToStr(aForm.Left)+' Top: '+IntToStr(aForm.Top) +#13#10+
					'Width: '+IntToStr(aForm.Width)+' Height: '+IntToStr(aForm.Height));
	}
	// Print ('proc: read_form END');
End;	




