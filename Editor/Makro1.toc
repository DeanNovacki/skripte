[10]
Scriptname=Pod i strop za kuhinje
TOOLNNAME=Spojevi
Scfile=\Makro_01\Pod_kuhinje.cps
Hint=Kuhinje: pod, strop
Icon=\Makro_01\Pod_kuhinje.bmp
Ver=1
[15]
Scriptname=Pod i strop za ormare
TOOLNNAME=Spojevi
Scfile=\Makro_01\Pod_ormari.cps
Hint=Ormari: pod, strop, horizontalna pregrada
Icon=\Makro_01\Pod_ormari.bmp
Ver=1
[17]
Scriptname=Pod ispod bokova
TOOLNNAME=Spojevi
Scfile=\Makro_01\Pod_ispod_bokova.cps
Hint=Pod ispod bokova
Icon=\Makro_01\Pod_ispod_bokova.bmp
Ver=1
[20]
Scriptname=Ojacanje
TOOLNNAME=Spojevi
Scfile=\Makro_01\Ojacanje.cps
Hint=Oja�anje
Icon=\Makro_01\Ojacanje.bmp
Ver=1
[21]
Scriptname=Frontalno_ojacanje
TOOLNNAME=Spojevi
Scfile=\Makro_01\Ojacanje_frontalno.cps
Hint=Frontalno oja�anje
Icon=\Makro_01\Ojacanje_frontalno.bmp
Ver=1
[22]
Scriptname=Cokla_ormar
TOOLNNAME=Spojevi
Scfile=\Makro_01\Cokla_ormar.cps
Hint=Cokla ormara
Icon=\Makro_01\Cokla_ormar.bmp
Ver=1
[30]
Scriptname=Polica
TOOLNNAME=Spojevi
Scfile=\Makro_01\Polica.cps
Hint=Polica
Icon=\Makro_01\Polica.bmp
Ver=1
[35]
Scriptname=Vertikala
TOOLNNAME=Spojevi
Scfile=\Makro_01\Vertikala.cps
Hint=Vertikalna pregrada
Icon=\Makro_01\Vertikala.bmp
Ver=1
[40]
Scriptname=Vrata
TOOLNNAME=Spojevi
Scfile=\Makro_01\Vrata.cps
Hint=Vanjska vrata
Icon=\Makro_01\Vrata.bmp
Ver=1
[42]
Scriptname=Vrata_unutarnja
TOOLNNAME=Spojevi
Scfile=\Makro_01\Vrata_unutarnja.cps
Hint=Unutarnja  vrata
Icon=\Makro_01\Vrata_unutarnja.bmp
Ver=1
[45]
Scriptname=Strop_iznad_bokova
TOOLNNAME=Spojevi
Scfile=\Makro_01\Strop_iznad_bokova.cps
Hint=Strop iznad bokova
Icon=\Makro_01\Strop_iznad_bokova.bmp
Ver=1
[60]
Scriptname=Maska
TOOLNNAME=Spojevi
Scfile=\Makro_01\Maska.cps
Hint=Prednja maska
Icon=\Makro_01\Maska.bmp
Ver=1
[70]
Scriptname=Ledja_utor
TOOLNNAME=Spojevi
Scfile=\Makro_01\Ledja_utor.cps
Hint=Le�a utor
Icon=\Makro_01\Ledja_utor.bmp
Ver=1
[73]
Scriptname=Puna_ledja
TOOLNNAME=Spojevi
Scfile=\Makro_01\Puna_ledja.cps
Hint=Puna le�a
Icon=\Makro_01\Puna_ledja.bmp
Ver=1
[80]
Scriptname=Raster_L
TOOLNNAME=Spojevi
Scfile=\Makro_01\Raster_L.cps
Hint=Raster za lijevu vertikalu
Icon=\Makro_01\Raster_L.bmp
Ver=1
[85]
Scriptname=Raster_D
TOOLNNAME=Spojevi
Scfile=\Makro_01\Raster_D.cps
Hint=Raster za desnu vertikalu
Icon=\Makro_01\Raster_D.bmp
Ver=1
[90]
Scriptname=Klizac
TOOLNNAME=Spojevi
Scfile=\Makro_01\Klizac.cps
Hint=Kliza� za bok
Icon=\Makro_01\Klizac.bmp
Ver=1
[1000]
Scriptname=Brisanje
TOOLNNAME=Spojevi
Scfile=\Makro_01\Ukloni.cps
Hint=Ukloni sve spojeve
Icon=\Makro_01\Ukloni.bmp
Ver=1
[1010]
Scriptname=Upute
TOOLNNAME=Spojevi
Scfile=\Makro_01\Upute.cps
Hint=Upute
Icon=\Makro_01\Upute.bmp
Ver=1