// d_Police5

// v5.9 - PROVJERITI ZA�TO NE RADE DINAMI�KI SPOJEVI
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// v5.8 - Dodano "Polica kra�a"
// - popravljen rad s fiksnim policama
// - dodano polje za upis koliko je polica kra�a
// - ispravljen bug koji se pojavljivao kad nije blo le�a
// - ispravljen bug kod provjere uvu�enosti od prednjeg ruba
// - ispravljen bug kod rada s dvije decimale



// v5.71 20.04.2015. 
// - kompatibilnost za Corpus 4.2.2.2770 (na manjimverzijama ne radi!!!!)
//		- 110: if ts[1] in BrojPlus  -> if char(ts[1]) in BrojPlus  ... 
// 	- 218: if ss[i+1] -> if char(ss[i+1]) ...
	


	

// v5.7 03.03.2014 (Japa?)
//		- u formulu za �irinu daske dodano na kraj daska.z umjesto fiksnog odmaka
//      sada se mijenja sirina pregrade ili police automatski kad se promijeni z od police ili pregrade

// v5.6 16.01.2014.
// ispravljen bug - zaokru�ivanje je radilo kad je isklju�eno i obrnuto

// v5.5 14.01.2014.
// - dodan Checkbox zaokruzi na milimetar


// v5.4 09.01.2014.
// - ispisuje razmak izme�u fiksnih polica 
// - zaokru�uje poziciju na cijeli broj

// v5.3 19.11.2013.
// izmjena:
// - ne odre�uje se vi�e koliko je polica manja nego da li je fiksna ili ne
// - u skladu s tim, formula ima na kraju "-polica_uza' ili nema
// - sada je skripta kompatibilna sa skriptom za dodavanje elemenata u ormar
// - NETKO (Vjerojatno Darko) je dodao naredbe DINACONN - nemam pojma �to to�no rade
// - ispravljena gre�ka: kod odre�ivanja rasporeda sa oznakom '+' tip je polica (prije je bio proizvoljan)





// dodavanje polica na jednake razmake
// ili prema upisanom rasporedu u svijetli otvor
// 
// SVIJETLI OTVOR
// Ako je zadan onda mora biti zadan sa 4 daske
// Ako nije zadan, ne smije biti selektirana niti jedna daska
//
// RASPORED
//     apsulutna pozicija - broj ili broj cm ili broj mm
//     relativna pozicija - broj %
//     relativna pozicija u odnosu na prethodnu dasku + broj
//     ako fali izraza u rasporedu:
//         - ako je zadnji izraz sa plus onda �e se taj izraz ponavljati
//         - ako nije, onda �e se preostale police rasporediti u preostali svijetli otvor

Program dodavanje_polica_proc;

var
   Prozor:TForm;
   T1, T2, T3:TButton;
   N1, N1E4, Upozorenje:TLabel;
   E1, E2, E3, E4, E5, Erazmak:TEdit;
   Podloga:TmImage;
   putanja:string;
   i:integer;
   //CB1, 
   CB1,CB2,CBzaokruzi:TCheckBox;
   //Rbox:TRadioGroup;
   

   SO_Vis, SO_Sir : Integer;                 // visina i sirina svijetlog otvora
   Raspored : String;

   // daske oko svijetlog otvora
   LD, DD, GD, DoD, ZD:string;
   LDx, DDx, GDy, DoDy, ZDz,
   LDd, DDd, GDd, Dodd, ZDd : integer;
   
   // polica
    
   PolSir:Integer;
   BrojPol,oDMAK, PolManja, PolDeb : Integer;
   
	razmak:single;   // razmak izme�u polica
	
   SO_OK:Boolean;
   
   Tokeni: array of array of integer;
   dinatip:integer;
   //

 
 {$I dex_unit.cps}
// Window (TForm), naslov (TLabel), natpis (TLabel), Polje (TEdit)
// RadioBox (TradioGroup), CBox (TCheckBox), Tipka (TButton)
// S_otvor 

Function BuildToken(TokeniX:array of string):Boolean;
var
   i,j,cc :integer;
   ts: string;
   BrojPlus:Set of Char;
   cm:integer;
   begin
     result:=true;
     BrojPlus:=['+','0','1','2','3','4','5','6','7','8','9'];
     cc:=GetArrayLength(TokeniX);
     SetArrayLength(Tokeni,2);
     SetArrayLength(Tokeni[0],cc);
     SetArrayLength(Tokeni[1],cc);
     for i:=0 to cc-1 do
         begin
            cm:=1;
            ts:=Tokenix[i];
            // if ts[1] in BrojPlus  then 
				if char(ts[1]) in BrojPlus  then 
               begin
                    If ts[1]='+' then 
                       begin
                            Tokeni[1][i]:=1;                // +
                            delete(ts,1,1);
                       end        
                       else Tokeni[1][i]:=0;
                    j:=pos('%',ts);
                    If j>0 then                                             
                       begin
                            
                          Tokeni[1][i]:=2;
                          delete(ts,j,1);
                       end;
                    j:=pos('mm',ts);
                    If j>0 then
                       begin
                          delete(ts,j,2);
                       end; 
                    j:=pos('cm',ts);
                    If j>0 then
                       begin
                          cm:=10;
                          delete(ts,j,2);
                       end;        
                   
                    Try 
                        Tokeni[0][i]:=StrToInt(ts)*cm;
                    Except
                          Tokeni[1][i]:=10;
                          result:=false;
                    End;
                    writeln(IntToStr(Tokeni[0][i])+' tip: '+IntToStr(Tokeni[1][i]));
                                      
               end                
               Else 
                    begin
                      Tokeni[1][i]:=10;
                      result:=false;
                    end;
            
            
         end;

     
     
     
     
   end;
Procedure UnBlankAll(var ss:string);  // micanje svih razmaka iz tokena
var
   i:integer;

   begin
      For i:=Length(ss) downto 1 do
          begin
             If ss[i]=' ' then 
				 begin
                    delete(ss,i,1);
                end;
          end;
   end;

Function UnBlank(ss:string):string;           // micanje pocetnih razmaka
var
   
   c:string;
     Begin
         If Length(ss)>0 then 
            begin 
               c:=ss[1];
               while (Length(ss)>0) and (c=' ')  do 
                  begin
                       
                       delete (ss,1,1);
                       if Length(ss)>0 then c:=ss[1]
                              else c:='';
                  end;
               result:=ss;
            end
            else result:=ss;
     End;
   
Function GetToken(ss:string):string;
     var
            i:integer;
            BrojPlus:set of char;
     Begin
        BrojPlus:=['+','0','1','2','3','4','5','6','7','8','9'];
        result:='';
        If Length(ss)>0 then
           begin
              If pos(' ',ss)=0 then
                 Begin
                   result:=ss;
                   exit;
                 end;
              for i:=1 to Length(ss) do
                begin
                   If i<Length(ss) then
                     begin
                         if ss[i]=' ' then 
                           begin
                               // if ss[i+1] in BrojPlus then
										 if char(ss[i+1]) in BrojPlus then
                                  begin
                                      result:=copy(ss,1,i);
                                      exit;
                                  end;
                                  
                           end;
                           
                     end
                     else result:=ss;
                end;   
           end
           else result:='';
     End;
   
Function Parser(ss:string):boolean;
var
   tokenX:string;
   tokeniX: array of string;
   tc,i:integer;    // brojac tokena
     Begin
        tc:=0;
        ss:=UnBlank(ss);
        while Length(ss)>0 do
           begin
                tokenX:=getToken(ss);
                if length(tokenX)>0 then 
                   begin
                      delete(ss,1,length(tokenX));
                      ss:=UnBlank(ss);
                      SetArrayLength(TokeniX,tc+1);
                      TokeniX[tc]:=TokenX;
                      writeln(tokenX);
                      tc:=tc+1;
                      
                   end
                   else break;
           end;
        For i:=0 to GetArrayLength(TokeniX)-1 do 
            begin
                 UnBlankAll(Tokenix[i]);
                 writeln(tokeniX[i]);
            end;     
        result:=BuildToken(TokeniX);                    // true ili false
          
     End;
procedure PoljeOnKeyPress(sender:Tobject; var Key: Char);
     begin
          // if not (Key in ['0','9',#8]) then key:=#0;  -- NE RADI!
          if not ((key='0') or (key='1') or (key='2') or 
                  (key='3') or (key='4') or (key='5') or
                  (key='6') or (key='7') or (key='8') or
                  (key='9') or (key=#8)) then key:=#0;
     end;

procedure RasporedOnKeyPress(sender:Tobject; var Key: Char);
var
 BrojPlus:set of char;
     Begin
         BrojPlus:=['+',' ','0','1','2','3','4','5','6','7','8','9','c','m','%',#8];
             // if not (Key in ['0','9',#8]) then key:=#0;  -- NE RADI!
          if not (key in BrojPlus) then key:=#0;
     end;
  
Procedure RasporedOnChange(sender:Tobject);
    begin
        If not parser(E3.text) then 
           begin
                //E3.setFocus;
                Upozorenje.caption:='Gre�ka u zadavanju rasporeda!';
                T1.enabled:=false;
           end
           else begin
                Upozorenje.caption:='';     
                T1.enabled:=true;
           end;
        
    end;  
 
Procedure Racunaj_razmak;  
// promjena vrijednosti razmaka izme�u polica
var
	rrr:Integer;
	r2:Double;
Begin
	if cbzaokruzi.checked then razmak:=round((SO_vis-BrojPol*Poldeb)/(BrojPol+1))
	 							 else razmak:=(SO_vis-BrojPol*Poldeb)/(BrojPol+1);
								 
	eRazmak.text:=FloatToStrC1(razmak,8,2);
	// Showmessage('Razmak: '+eRazmak.text);
	// rrr:=2;
	// r2:=1/rrr;
	// Showmessage(FloatToStrC1(r2,8,2));
End;
 
procedure RasporedOnKlik(sender:Tobject);
     begin
          
          if CB2.checked=true then 
             begin
                  E3.enabled:=false;
                  N1.enabled:=false;
						eRazmak.visible:=true;
						CBZaokruzi.visible:=true;
             end
             else begin
                  E3.enabled:=true;
                  N1.enabled:=true;
						eRazmak.visible:=false;
						CBZaokruzi.visible:=false;
             end;
				 Racunaj_razmak;
     end;    

procedure FiksnaOnKlik(sender:Tobject);
     begin
          
          if CB1.checked=true then 
             begin
                  E4.enabled:=false;
						E4.text:='0';
             end
             else begin
                  E4.enabled:=true;
						E4.text:=IntToStr(PolManja);
             end;
				 // Racunaj_razmak;
     end;    

procedure ZaokruziOnKlik(Sender:TObject); 
Begin
	Racunaj_razmak;
End;
 
procedure PoljaOnChange (sender:TObject);

// ---------------------------------------------------------------------
// provjera upisanih vrijednosti i dodjela varijablama
// ---------------------------------------------------------------------

var 

   dodijeljivanje:boolean;  // pro�la je provjera unosa?
    tag:integer;
    value:string;
begin
     
     // �itanje svijetlog otvora
     
           
     dodijeljivanje:=true;
     if TEdit(Sender).modified then         // radi samo ako je ne�to u poljime promijenjeno
       begin
         if TEdit(Sender).text='' then      // �to ako je polje postalo prazno?
            begin 
              // ShowMessage('PRAZAN! ');
              TEdit(Sender).text:='0';      // umjesto praznog se upisuje 0
              TEdit(Sender).SelectAll;      // selektira unos da se idu�i broj nebi pisao ispred nule
            end;
      BrojPol:=StrToInt(E1.text);   
         // provjera svih polja (osim rasporeda)

         tag:=TEdit(Sender).tag;            // �itanje taga od po�iljatelja (polja)
         value:=TEdit(Sender).text;         // �itanje sadr�aja po�iljatelja

         Case tag of
           1: begin                               // Broj polica
                   BrojPol:=StrToInt(value);
                   If BrojPol*PolDeb > SO_Vis then 
                      begin
                           Upozorenje.caption:='U svijetli otvor visine '+IntToStr(SO_Vis)+' mm '+ 'nije' + #13#10+
                                               'mogu�e postaviti '+IntToStr(BrojPol)+' polica'+#13#10+
                                               'debljine ' + IntToStr(PolDeb) + ' mm!';
                           TEdit(Sender).color:=CLRed;         // farba polje u crveno
                           E5.color:=clRed;                                            
                           dodijeljivanje:=false;
                      end
                      else begin
                           TEdit(Sender).color:=clWindow;      // farba polje u default
                           E5.color:=clWindow;             
                           Upozorenje.caption:='';
                           dodijeljivanje:=true;
                      end;
              end;
           2: begin                                // Polica uvu�ena
                   Odmak:=StrToInt(value);
						 // Showmessage ('Odmak= ;' + IntToStr(Odmak) + '; PolSir= ' + IntToStr(PolSir)); 
                   If Odmak > PolSir then begin
                           TEdit(Sender).color:=CLRed;         // farba polje u crveno
                           Upozorenje.caption:='Uvu�enost polica ne smije biti ' + #13#10 +
                                               've�a od dubine svijetlog otvora';  
                           dodijeljivanje:=false
                      end else begin
                           TEdit(Sender).color:=clWindow;      // farba polje u default
									E2.color:=clWindow;
                           Upozorenje.caption:='';
                           dodijeljivanje:=true;
                   end;
						 // Showmessage ('Odmak= ;' + IntToStr(Odmak) + '; PolSir= ' + IntToStr(PolSir)); 
                 //  beep;

              end;
           3: Raspored:=value;                    // Raspored
           4: Begin
                  // { 
						PolManja:=StrToInt(value);          // polica kra�a
                  If PolManja > 2 then 
                      begin
                           Upozorenje.caption:='Mogu�a vrijednosti su: '  + #13#10+
                                               '0 ako je fiksna polica ili 1 ili 2 ako nije.';
                           TEdit(Sender).color:=CLRed;         // farba polje u crveno
                           TEdit(Sender).SelectAll; 
                           dodijeljivanje:=false;
                           
                      end
                      else begin
                           TEdit(Sender).color:=clWindow;      // farba polje u default 
                           Upozorenje.caption:='';
                           TEdit(Sender).SelectAll;
									// pop4.x+pop4.debljina+raz_popl;
                           dodijeljivanje:=true;
                  end;   
						// }
              End;
           5: Begin
                   PolDeb:=StrToInt(value);            // debljina polica


                   If BrojPol*PolDeb > SO_Vis then 
                      begin
                           Upozorenje.caption:='U svijetli otvor visine '+IntToStr(SO_Vis)+' mm '+ 'nije' + #13#10+
                                               'mogu�e postaviti '+IntToStr(BrojPol)+' polica'+#13#10+
                                               'debljine ' + IntToStr(PolDeb) + ' mm!';
                           TEdit(Sender).color:=CLRed;         // farba polje u crveno
                           E1.color:=clWindow;      // farba polje u default
                           dodijeljivanje:=false;
                      end
                      else begin
                           TEdit(Sender).color:=clWindow;      // farba polje u default 
                           E1.color:=clWindow;      // farba polje u default 
                           Upozorenje.caption:=''; 
                           dodijeljivanje:=true;
                      end;
              End;

         end;    // end case
         If dodijeljivanje then T1.enabled:=true
                           else T1.enabled:=false;
       end;   // end od if TEdit(Sender).modified
		 
		 Racunaj_razmak;  
      
end;


procedure Dod_jednaki(NDod:string;NBrojPol:integer);

// ---------------------------------------------------------------------
// crtanje polica
// ---------------------------------------------------------------------


var
     i,z :integer;
     postotak:string;
     polica1: tDaska;
     yStartn,ySpacen: string;
begin

       Odmak:=StrToInt(E2.text);
       PolManja:=StrToInt(E4.text);
       PolDeb:=StrToInt(E5.Text);
       z:=0-Odmak;
       
       for i:=1 to NBrojPol do 
           begin
                Polica1:=TDaska.CreateDaska(e,0,0,z,10,10,PolDeb);
                Polica1.smjer:=horiz;
                
                Polica1.Naziv:='Polica_';
                Polica1.TexIndex:=DefaultData.DefElmTex[5];         // tekstura police po defaultu
                dodajpotrosni(e,polica1,popolica); 
                Polica1.Potrosni.SetDekorTrake(Polica1.TexIndex);                           // <== PRIVREMENO!!! dok se ne rije�e poc. post.
                               
                Polica1.XF:=(LD+'.x+'+LD+'.de');
                // Polica1.HF:=(DD+'.x-'+LD+'.x-'+LD+'.De-'+IntToStr(PolManja));
                If cb1.checked then Polica1.HF:=(DD+'.x-'+LD+'.x-'+LD+'.De')
                               else Polica1.HF:=(DD+'.x-'+LD+'.x-'+LD+'.De-'+IntToStr(PolManja));
                //Polica1.SF:=('0-'+ZD+'.z-'+ZD+'.De-'+IntToStr(Odmak));
                


					 // If ZD='Nema zadnje daske' then Polica1.SF:=('dubina-'+IntToStr(Odmak))
                //                           else Polica1.SF:=('0-'+ZD+'.z-'+ZD+'.De-'+IntToStr(Odmak));
					 
					// v5.7
					If ZD='Nema zadnje daske' then Polica1.SF:=('dubina+'+Polica1.Naziv+'.z')
                                         else Polica1.SF:=('0-'+ZD+'.z-'+ZD+'.De+'+Polica1.Naziv+'.z');												
														
														
                yStartn:='('+NDod+'.y+'+NDod+'.de)';
                ySpacen:='('+GD+'.y-'+yStartn+'-'+IntToStr(NBrojPol*PolDeb)+')';
					 
                postotak:=FloatToStrC1(round((i/(NBrojPol+1))*1000)/1000.0,8,2);
					 
					 
					 // v5.9
					 
                if cbzaokruzi.checked then Polica1.YF:='round('+yStartn+'+'+ySpacen+'*'+postotak+'+'+IntToStr((i-1)*PolDeb)+')'
												  else Polica1.YF:=         yStartn+'+'+ySpacen+'*'+postotak+'+'+IntToStr((i-1)*PolDeb);  
					 eRazmak.text:=FloatToStrC1(razmak,8,2);							  
					 
					 // 								
                E.AddDaska(Polica1);
					 Polica1.tipdaske:=polica;
                DinaConn(DinaTip,Polica1,nil,e,0);
                DinaConn(DinaTip,Polica1,nil,e,1);
           end; 

end;      

Function dod_fiksni(daska:string;y_pol:integer):Tdaska;

          begin
              result:=TDaska.CreateDaska(e,0,0,0-Odmak,10,10,PolDeb);
              
				  
				  
				  if cbzaokruzi.checked then result.yf:='round('+'('+daska+'.y+'+daska+'.de)+'+IntToStr(y_pol)+')'
                                    else result.yf:=             daska+'.y+'+daska+'.de)+'+IntToStr(y_pol);   
				  eRazmak.text:=FloatToStrC1(razmak,8,2);								  
														
				  
          end;

Function dod_posto(daska:string;y_pol:integer):Tdaska;
var
   postotak: string;
   YStartn, ySpacen:string;

          begin
              yStartn:='('+Daska+'.y+'+Daska+'.de)';
              ySpacen:='('+GD+'.y-'+yStartn+')';
              postotak:=FloatToStrC1(y_pol/100.0,8,2);
              result:=TDaska.CreateDaska(e,0,0,0-Odmak,10,10,PolDeb);
				  if cbzaokruzi.checked then result.yf:='round('+yStartn+'+'+ySpacen+'*'+postotak+')'
												else result.yf:=         yStartn+'+'+ySpacen+'*'+postotak;
				  eRazmak.text:=FloatToStrC1(razmak,8,2);
				  
				  
              // result.yf:='round('+yStartn+'+'+ySpacen+'*'+postotak+')';
          end;

procedure Dodavanje(sender:Tobject);

// ---------------------------------------------------------------------
// crtanje polica
// ---------------------------------------------------------------------


var

     // i,j :integer;
     // ,x,y,z, yStart, ySpace, korak :integer;


     polica1: tDaska;
     //BrojPoln, yStartn, korakn, ySpacen, SO_Visn : string;

     prethodna:string;
     TB: integer;           // broj tokena
     TV,TT:integer;    // TV = token vrijednost, TT = token tip
begin

       BrojPol:=StrToInt(E1.text);
       Odmak:=StrToInt(E2.text);
       PolManja:=StrToInt(E4.text);
       PolDeb:=StrToInt(E5.Text);
       if cb1.checked then DinaTip:=3;
if  not CB2.checked then
    begin
 
       prethodna:=dod;
       parser(E3.text);
       TB:=GetArrayLength(Tokeni);
       if tb>0 then
         TB:=GetArrayLength(Tokeni[0]);
         For i:=1 To BrojPol do
           Begin
               If i<=TB then
                  begin   
                          tv:=Tokeni[0][i-1];
                          tt:=Tokeni[1][i-1];
                  End
                  else if tt<>1 then tt:=3; 
               
                Case tt of
                   0:  Polica1:=dod_fiksni(dod,tv);             // dod je pod
                   1:  Polica1:=dod_fiksni(prethodna,tv);   // 
                   2:  Polica1:=dod_posto(Dod, tv);
                   3:  begin
                            Dod_jednaki(prethodna, BrojPol-i+1);
                            break;
                       end;
                end; // case
    
                Polica1.smjer:=horiz;
                Polica1.Naziv:='Polica_';
                prethodna:=Polica1.Naziv;
                Polica1.TexIndex:=DefaultData.DefElmTex[5];                // tekstura police po defaultu
                Polica1.XF:=(LD+'.x+'+LD+'.de');
                Polica1.HF:=(DD+'.x-'+LD+'.x-'+LD+'.De-'+IntToStr(PolManja));
                If ZD='Nema zadnje daske' then Polica1.SF:=('dubina-'+IntToStr(Odmak))
                                          else Polica1.SF:=('0-'+ZD+'.z-'+ZD+'.De-'+IntToStr(Odmak));
                dodajpotrosni(e,polica1,popolica); 
                Polica1.Potrosni.SetDekorTrake(Polica1.TexIndex);  
                E.AddDaska(Polica1);
					 Polica1.tipdaske:=polica;
                DinaConn(DinaTip,Polica1,nil,e,0);
                DinaConn(DinaTip,Polica1,nil,e,1);

           end; //  for i 
     End  // if tb>0
     else dod_jednaki(Dod, BrojPol);


     prozor.close;
     e.RecalcFormula(e);   // if tb>0
        

end;      



procedure Pomoc(sender:Tobject);
var
   pTekst:string;
begin
         pTekst:='DODAVANJE POLICA'+#13#10+#13#10
					 'v5.7 03.03.2014.'+#13#10+#13#10
					 'Broj polica... - Koli�ina polica koje trebaju do�i u svijetli otvor'+#13#10+#13#10
                'Police uvu�ene... - Udaljenost od prednje granice elementa do polica'+#13#10+#13#10
                'Police manje... - 0 ako je fiksna polica, 1 ako nije (bit �e kra�a za 1mm)'+#13#10+#13#10
                'Debljina polica - Debljina svake pojedine police'+#13#10+#13#10
                'Jednaki razmaci... - Police �e biti raspore�ene na jednake razmake'+#13#10+#13#10
                'Raspored - Upisivanje polo�aja za svaku pojedinu policu'+#13#10+#13#10
                '           sintaksa:'+#13#10+#13#10
                '                      brojevi bez jedinica mjere se odnose na milimetre'+#13#10+
                '                      brojevi sa mm ili cm ozna�avaju milimetre ili centimetre'+#13#10+
                '                      brojevi sa % ozna�avaju relativni polo�aj u svijetlom otvoru'+#13#10+
                '                      ako je ispred broja oznaka "+" broj ozna�ava razmak od prethodne police'+#13#10+#13#10         
                '           primjeri:'+#13#10+#13#10
                '                      100   250   380   400'+#13#10+
					 '								100mm od dna svijetlog otvora (SO), 250mm od dna SO, 380 od dna SO, 400 od dna SO'+#13#10+
					 '								'+#13#10+
                '                      10cm   250mm   38cm   400'+#13#10+
					 '                      10cm od dna svjetlog otvora, 250mm od dna SO, 38cm od dna SO, 400mm od dna SO'+#13#10+
					 '								'+#13#10+
                '                      10%  20%   45%'+#13#10+
					 '								'+#13#10+
                '                      100   20%   56cm   245mm' +#13#10+
                '                      200 +170 +170 +170'                  
        ShowMessage(pTekst);
     
end;      

// ------------------------------------------------------------------------
//            GLAVNI PROGRAM
// ------------------------------------------------------------------------
begin

     

     Dinatip:=2;
     // Postavljanje po�etnih vrijednosti
     BrojPol:=1;
     //Odmak:=defaultdata.DefPolice.PolicaUvucena;  // polica uvucena iz poc.pos.      //     <===  ??????????
     Odmak:=0;
     PolManja:=1;                                                           
     // PolDeb:=round(defaultdata.DefPolice.PolDeb);   // NE RADI! ????              //     <===  ??????????
     PolDeb:=18;          
     Prozor:=Window('Red Cat',260,350);
     Podloga:=TmImage.create(Prozor);
     Podloga.parent:=Prozor;
     Podloga.Align:=AlClient;
     Podloga.stretch:=True;
     putanja:=Application.ExeName;
     {
     i:=Length(Putanja)
     Repeat 
          delete(Putanja,i,1);
          i:=i-1;
     Until Putanja[i]='\'; 
     //Writeln(Putanja);      
     Podloga.LoadFromFile(putanja+'scripte\blue_black_back.bmp');
     }


     naslov('Dodavanje polica',Prozor);
     E1:=polje('Broj polica koji se postavlja:',Prozor,200,45);
     E1.text:=IntToStr(BrojPol);
     E1.tag:=1;
     E2:=polje('Police uvu�ene od prednjeg ruba:',Prozor,200,70);
     E2.text:=IntToStr(Odmak);  
     E2.tag:=2;

     CB1:=CBox('Fiksna polica',Prozor,20,113);
     CB1.checked:=false;
	  CB1.onClick:=@FiksnaOnKlik;
	  // {
     E4:=polje('Polica kra�a:',Prozor,110,135);
     PolManja:=1;
	  E4.text:=IntToStr(PolManja);
	  // 
	  // }
     CB1.tag:=4;     

     E5:=polje('Debljina polica:',Prozor,200,95);
     E5.text:=IntToStr(PolDeb);
     E5.tag:=5;
     CB2:=CBox('Jednaki razmaci ',Prozor,20,158);
	  
     Erazmak:=polje('', Prozor, 200,160);
	  Erazmak.enabled:=false;
	  
	  
	  CBZaokruzi:=CBox('Zaokru�i na cijeli milimetar',Prozor,50,180);
	  CBZaokruzi.checked:=false;
	  // CBZaokruzi.visible:=false;
	  	
	  
	  
     N1:=natpis('Raspored:',Prozor,15,208,'lijevo');
     E3:=polje('',Prozor,70,208);
     E3.tag:=3;
     E3.width:=150;
     E3.text:=' 100 200 300 ';
     E3.enabled:=false;

     N1.enabled:=false;
     CB2.checked:=true;
     CB2.onClick:=@RasporedOnKlik;
     Upozorenje:=natpis('',Prozor,15,230,'lijevo');
     Upozorenje.font.color:=clRed;       // farba upozorenje u crveno
     Upozorenje.font.style:=[fsBold];
     T1:=Tipka('Dodaj',Prozor,10,290);
     T2:=Tipka('Pomo�',Prozor,90,290);
     T3:=Tipka('Odustani',Prozor,170,290);
     //Prozor.ActiveControl:=T1;
     T1.default:=True;

     E1.OnChange:=@PoljaOnChange;
    
     E2.OnChange:=@PoljaOnChange;
     E3.OnChange:=@PoljaOnChange;
     // E4.OnChange:=@PoljaOnChange;          
     E5.OnChange:=@PoljaOnChange;
	  CBzaokruzi.OnClick:=@ZaokruziOnKlik;	  // refresh result on screen

     E1.OnKeyPress:=@PoljeOnKeyPress;   // provjera integera
     E2.OnKeyPress:=@PoljeOnKeyPress;
     E3.OnKeyPress:=@RasporedOnKeyPress;     
     // E4.OnKeyPress:=@PoljeOnKeyPress;
     
     // Provjera svijetlog otvora

     S_otvor(LD, DD, GD, DoD, ZD, LDx, DDx, GDy, DoDy, ZDz,LDd, DDd, GDd, DoDd, ZDd, SO_OK);
     If not SO_OK then 
           begin
               ShowMessage('skripta - Dodavanje polica:'+#13#10+'Nije prona�en svijetli otvor!');
               Prozor.close;                                 // zatvara prozor
               exit;                                         // izlazi van iz procedure Dodavanje
           end;

     SO_Vis:= GDy-DoDy-DoDd;
     SO_Sir:= DDx-LDx-DDd;
	  // Showmessage('zdz= '+IntToStr(zdz)+' Zdd= '+ IntToStr(zdd));
	  
	  // --------
	  // ispravak gre�ke kod S_otvor;
     If ZDd<1 then begin
								PolSir:= ZDz+ZDd;
								// Showmessage('Nema le�a ili su 0. PolSir= '+IntToStr(PolSir));
							end else begin
							   PolSir:= -ZDz-ZDd;
								// Showmessage('Ima le�a. PolSir= '+IntToStr(PolSir));
							end;
							
	  // --------
	  
	  // PolSir:= -ZDz-ZDd;
          
     E3.OnChange:=@RasporedOnChange;
     T1.OnClick:=@Dodavanje;
     T2.OnClick:=@Pomoc;

     T3.ModalResult:=mrCancel;          // ide dalje (iza prozor.showmodal)

	  Racunaj_razmak;    // prvi put
	  
     prozor.showmodal;                       // preko ove naredbe �e pro�i i prozor.close

     Prozor.free;


end.