// popis 
{  
   procedure el_replace(old_el:Telement; new_el_path:string);
   function el_find_selected:TElement;
   procedure el_set_name(elm:TElement; e_name:string);
   Procedure el_set_width(elm:TElement; e_width:Integer);
   Procedure el_set_height(elm:TElement;e_height:Integer);
   Procedure el_set_depth(elm:TElement;e_depth:Integer);
   Function el_x(elm:Telement):Single;
   Function el_y(elm:Telement):Single;
   Function el_z(elm:Telement):Single; 
   Procedure var_set(elm:Telement; variable_name,variable_value:string);
   Function var_exist(elm:Telement; variable_name:string):boolean;
   Procedure var_add(elm:Telement; variable_name,variable_value:string);
   Function plate_find_by_name(elm:TElement;plate_name:String):Tdaska;
   function element_add(e_path,e_name:string; e_parent:Telement; x,y,z:single):Telement;
   function element_add_F(e_path,e_name:string; e_parent:Telement; x,y,z:string):Telement;
   procedure el_delete_legs(elm:Telement);
   
}
procedure el_replace2(old_el:Telement; new_el_path:string);
// element old_el replace with element on disc with path new_el_path
var
   temp_el1,temp_el2:TElement;
begin 
     message('Procedure e_replace(old_el:Telement; new_el_path:string)');
        // temp_el1:= telement.create(0,0,0,1000,1000,1000, 1000);                    // create new element (not visible!)
        if FileExists(new_el_path)then begin
           temp_el2:=LoadParentedElement(nil,new_el_path);                     // load from disc in new element (still unvisible)
           message('Done: LoadParentedElement(nil,new_el_path)');
           old_el.kopiraj(temp_el2);                                            // copy from new element to existed element
           message('Done! Element "'+old_el.Naziv+'" replaced with "'+new_el_path);
        end else begin
           showmessage('File "'+old_el.naziv+'" not found!');
           message('NOT DONE! "'+new_el_path+'" not found!');
        End;           
end;  // end procedure element_replace

procedure el_replace(old_el:Telement; new_el_path:string);
// element old_el replace with element on disc with path new_el_path
var
   temp_el1,temp_el2:TElement;
begin 
     message('Procedure element_replace(old_el:Telement; new_el_path:string)');
     // temp_el1:= telement.create(0,0,0,1000,1000,1000, 1000);      // create new element (not visible!)
	  temp_el1:= telement.create(nil);      // create new element (not visible!)
        if FileExists(new_el_path) then begin
           temp_el2:=LoadParentedElement(temp_el1,new_el_path);   // load in new element (still unvisible)
           old_el.kopiraj(temp_el2);                              // copy from new element to existed element
           message('Done! Element "'+old_el.Naziv+'" replaced with "'+new_el_path);
        end else begin
           showmessage('File "'+old_el.naziv+'" not found!');
           message('NOT DONE! "'+new_el_path+'" not found!');
        End;  
     temp_el1.free;           
end;  // end procedure element_replace

function el_createxx:TElement;    // ??????
// create new element
// work in project AND editor
var
   i:integer;
   elem:TElement;
begin
   if e<>nil then begin 
                  // Editor!
                  // showmessage('I am in editor! e<>nil');
                  result:=e;
                  // showmessage('Element is '+result.naziv);      
              end else begin
                  // Project!     
                  // showmessage('I am in project! e=nil')
                  for i:=0 to ElmList.Count-1 do begin
                      elem:=TElement(ElmList.Items[i]);
                      if  elem.selected then begin
                              result:=elem;
                              // showmessage('Element is '+elem.naziv);
                              Break;
                          end else begin 
                              result:=nil;
                      end; // if  elem.selected
                  end; // for i
   end; // if e<>nil

end;  // function el_find_selected:TElement;

function el_find_selected:TElement;
// if in editor, return top element
// if in project, return first selected element. If nothing selected, return nil
var
   i:integer;
   elem:TElement;
begin
   if e<>nil then begin 
                  // Editor!
                  // showmessage('I am in editor! e<>nil');
                  result:=e;
                  // showmessage('Element is '+result.naziv);      
              end else begin
                  // Project!     
                  // showmessage('I am in project! e=nil')
                  for i:=0 to ElmList.Count-1 do begin
                      elem:=TElement(ElmList.Items[i]);
                      if  elem.selected then begin
                              result:=elem;
                              // showmessage('Element is '+elem.naziv);
                              Break;
                          end else begin 
                              result:=nil;
                      end; // if  elem.selected
                  end; // for i
   end; // if e<>nil

end;  // function el_find_selected:TElement;

procedure el_set_name(elm:TElement; e_name:string);
begin
  elm.naziv:=e_name;
end;

Procedure el_set_width(elm:TElement; e_width:Integer);
Begin
  elm.sirina:=e_width;
end;
    
Procedure el_set_height(elm:TElement;e_height:Integer);
Begin
   elm.visina:=e_height;
End;

Procedure el_set_depth(elm:TElement;e_depth:Integer);
Begin
   elm.dubina:=e_depth;
End;

Function el_x(elm:Telement):Single;
begin
    result:=elm.Xpos;
end;

Function el_y(elm:Telement):Single;
begin
    result:=elm.Ypos;
end;

Function el_z(elm:Telement):Single;
begin
    result:=elm.Zpos;
end;

Procedure var_set(elm:Telement; variable_name,variable_value:string);
// fill variable of element with value
begin                                                                    
   if elm.fvarijable.isvar(variable_name)                                // if variable name exist
       then                                                               
            elm.fvarijable.setvarvalue(variable_name,variable_value)     // set variable value (as string)
       else
            message('VARIABLE "'+variable_name+'" NOT EXIST!!!');     
end;

Function var_exist(elm:Telement; variable_name:string):boolean;
// test if exist variable in element
begin                                          
   if elm.fvarijable.isvar(variable_name) then result:=true
                                          else result:=false;
                                               
end;

Function var_read_str(elm:Telement; variable_name:string):string;
// NOT WORK!!!!
// read string value from element variable
begin
   if elm.fvarijable.isvar(variable_name) then begin                 // if variable exist
      // result:=readvarvaluename(elm,variable_name);        // string result
      // result:=elm.fvarijable.parsevar(varable_name);        // string result
   end else begin
      message('VARIABLE "'+variable_name+'" NOT EXIST!!!');
      result :='';
   end;

end;

Procedure var_add(elm:Telement; variable_name,variable_value:string);
// add new variable with value in element
begin
   elm.fvarijable.add(variable_name+'='+variable_value);        
end;

Function plate_find_by_name(elm:TElement;plate_name:String):Tdaska;
// if plate with wanted name not exist, result will be nil
var
   i:integer;
   temp_plate:TDaska;
   pass:boolean;
Begin
    message('plate_find_by_name(elm:TElement;plate_name:String):Tdaska;');
    pass:=false
    for i:=0 to elm.childdaska.CountObj-1 do Begin      //el.childdaska.CountObj = quantity of plates in top element
        temp_plate:=elm.childdaska.Daska[i];             //plate with index i
        if uppercase(temp_plate.naziv)=uppercase(plate_name) then Begin 
                                            
                                            pass:=true;
                                            Break;
                                       end else begin
                                            pass:=false;
                                            
        End; //  if temp_plate.naziv=plate_name
     End;  // for i
     if pass then begin
                  result:=temp_plate;
                  message('Plate "'+temp_plate.naziv+'" found.');
             end else begin
                  result:=nil;
                  message('PLATE "'+plate_name+'" NOT EXIST!!!');
     end; // if pass
      
end;

function element_add(e_path,e_name:string; e_parent:Telement; x,y,z:single):Telement;
// add element with apsolute position
// DELETE formulas for x,y,z!!!
var
   temp_el:TElement;
begin 
     message('function element_add(e_path,e_name:string; e_parent:Telement; x,y,z:single):Telement;');

     if FileExists(e_path)then begin
           temp_el:=LoadParentedElement(e_parent,e_path);
           temp_el.Naziv:=e_name;
           temp_el.XF:='';
           temp_el.YF:='';
           temp_el.ZF:='';
           temp_el.Xpos:=x;
           temp_el.Ypos:=y;
           temp_el.Zpos:=z;

           result:=temp_el;
           message('Element "'+temp_el.naziv+'" inserted');
        end else begin
           showmessage('File "'+e_path+'" not found!');
           message('NOT DONE! "'+e_path+'" not found!');
           result:=nil;
     end; // if
             
end;  //   function element_add

function element_add_F(e_path,e_name:string; e_parent:Telement; x,y,z:string):Telement;
// add element with position in formulas
// DELETE formulas for x,y,z!!!
var
   temp_el:TElement;
begin 
     message('function element_add_F(e_path,e_name:string; e_parent:Telement; x,y,z:string):Telement;');

     if FileExists(e_path)then begin
           temp_el:=LoadParentedElement(e_parent,e_path);
           temp_el.Naziv:=e_name;
           temp_el.XF:=X;
           temp_el.YF:=Y;
           temp_el.ZF:=Z;


           result:=temp_el;
           message('Element "'+temp_el.naziv+'" inserted');
        end else begin
           showmessage('File "'+e_path+'" not found!');
           message('NOT DONE! "'+e_path+'" not found!');
           result:=nil;
     end; // if
             
end;  //   function element_add

procedure el_delete_legs(elm:Telement);
// delete all legs in element 
var
   tel:Telement;
   tpl:Tdaska;
   j,jj:Integer;
	
   
begin
    // if leg is element
    for j:=elm.elmlist.count-1 downto 0 do Begin              // elm.elmlist.count = number of elements in top element
	     // showmessage( 'broj prostalih elemenata: '+IntToStr(j+1) );
        tel:=elm.elmlist.element[j];                         // element with index i
        // showmessage ('Found element "'+tel.naziv+' type: '+IntToStr(integer(tel.tipelementa)));
        if (integer(tel.tipelementa)=16) then Begin         // 16=leg
                                                 // showmessage('deleting element "'+tel.naziv+'"');
                                                 elm.elmlist.obrisi(j);   
																 jj:=elm.elmlist.count;
																 // showmessage('preostalo elemenata: '+IntToStr(jj));
																 
                                              End; //  if 
    End;  // for i
     
    // if leg is plate
    for j:=elm.childdaska.count-1 downto 0 do Begin         // elm.childdaska.count = number of elements in top element
        tpl:=elm.childdaska.daska[j];                                        // plate with index j
        // message ('Found plate "'+tpl.naziv+'" type: '+IntToStr(integer(tpl.tipdaske)));
        if (integer(tpl.tipdaske)=5) then Begin         // 16=leg
                                             message('deleting element "'+tpl.naziv+'"');
                                             elm.childdaska.obrisi(j);
                                           End; //  if 
    End;  // for j
     
end;   

procedure el_delete_sub(elm:Telement;ename:string);
// delete element ename from element elm
var
   tel:Telement;
   j:Integer;
   
begin
    message('procedure el_delete_sub(elm:Telement,ename)');
    for j:=elm.elmlist.count-1 downto 0 do Begin              // elm.elmlist.count = number of elements in top element
        tel:=elm.elmlist.element[j];                         // element with index i
        // message ('Found element "'+tel.naziv+' type: '+IntToStr(integer(tel.tipelementa)));
        if tel.naziv=ename then Begin  
                                      message('deleting element "'+tel.naziv+'"');
                                      elm.elmlist.obrisi(j);   
        End; //  if 
    End;  // for i
     
     
end;   