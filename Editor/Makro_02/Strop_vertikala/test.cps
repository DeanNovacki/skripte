﻿
// Out 2******************************************
Procedure Test_dasaka;
var
	i, broj_horizontalnih, broj_vertikala:integer;
	ss:string;
	td:Tdaska;
Begin
	// brojanje količine
	broj_horizontalnih:=0;
	Broj_vertikala:=0;
	for i:=0 to e.childdaska.CountObj-1 do Begin  // broji daske u editoru
		td:=e.childdaska.Daska[i];   
		// tražim frontu
		if td.selected and (td.smjer in [VertBok]) then begin				// VertBok, VertFront, Horiz
			broj_vertikala:=broj_vertikala+1;
			daska_2:=td;
			// ShowMessage('i:='+IntToStr(i)+' Našao sam vertikalu broj: '+IntToStr(broj_vertikala));
		end; // if td.selected
		// tražim vertikalu
		if td.selected and (td.smjer in [Horiz]) then begin				// VertBok, VertFront, Horiz
			broj_horizontalnih:=broj_horizontalnih+1;
			// ShowMessage('i:='+IntToStr(i)+' Našao sam horizontalnu broj: '+IntToStr(broj_horizontalnih));
			daska_1:=td;
		end; //if td.selected
	end;  // for i:=0 to e.childdaska.CountObj-1 


	// provjera količine
	If (broj_horizontalnih=1) and (Broj_vertikala=1) Then Begin
		// provjera kuteva
		If (Daska_1.kut<>0) or (Daska_1.xkut<>0) or (Daska_1.zkut<>0) or 
			(Daska_2.kut<>0) or (Daska_2.xkut<>0) or (Daska_2.zkut<>0) Then Begin 
					Showmessage('Neke selektirane daska su rotirane,' +#13#10+
									'a ne smije biti za ovaj spoj.' );
					ButOK.Enabled:=False;			 
			End else begin
				// kutevi su ok
				// provjera dodira
				// visina,dubina,debljina,xPos,yPos,zPos
				If (daska_1.ZPos<daska_2.ZPos-daska_2.dubina) 							// 1 je iza 2
					or	(daska_1.ZPos-daska_1.dubina>daska_2.ZPos)						// 1 je ispred 2
					or	(daska_1.xPos>daska_2.XPos+Daska_2.debljina)  					// 1 je desno od 2
					or	(daska_1.xPos+daska_1.visina<daska_1.XPos)    					// 1 je lijevo od 2
					or	(daska_1.yPos<>daska_2.Ypos+daska_2.visina)  				// 1 nije točno po Y
					Then begin
						Showmessage('Izgleda da se daske ne dodiruju.'+#13#10+
										'Makro se može postaviti, ali dobro provjerite što radite!');
						// Showmessage(FloatToStr(daska_1.ZPos+daska_1.debljina)+'<>'+FloatToStr(daska_2.ZPos-daska_2.dubina));
				End;  // 	If (daska_1.ZPos<>daska_2.ZPos) 
		end;	// If (Daska_1.kut<>0) 
	end else begin  // If (broj_fronti=1) ;	
		ButOK.Enabled:=False;
		Showmessage('1. Moraju biti selektirane dvije daske!' +#13#10+
						'Horizontalna (pod, polica...)  i vertikala ispod nje.');
	end;  // If (broj_fronti=1)
	
		
End;
// Out 2******************************************
