// Oznaci neselektirane elemente tako da u prvo mjesto opisa stavi� znak | (okomita crta)

{$I ..\..\..\include\rcPrint.cps}
{$I ..\..\..\include\rcStrings.cps}
{$I ..\..\..\include\rcObjects.cps}   

var
   elm:telement;
   i:integer;
	tekst:string;
   
begin

// brisanje prija�njih oznaka, ako postoje
	if ElmHolder<>nil then begin                               // ima elemenata
      for i := 0 to elmholder.Elementi.count-1 do begin
         elm:=Telement(ElmHolder.Elementi.items[i]); 
			tekst:=elm.Napomena;
			if copy(tekst,0,1)='|' then begin
				// showmessage (elm.napomena);
				elm.Napomena:=copy(tekst,2,Length(tekst)-1);
				// showmessage (elm.napomena);
			end;
		end;
	end;		

// upisivanje oznake '|' u elemente koji NISU selektirani	
   if ElmHolder<>nil then begin                               // ima elemenata
      for i := 0 to elmholder.Elementi.count-1 do begin
         elm:=Telement(ElmHolder.Elementi.items[i]);            
            if not elm.selected then begin
					// showmessage (elm.napomena);
					tekst:=elm.Napomena;
					elm.Napomena:='|'+tekst;
					// showmessage (elm.napomena);
				end;
		end;
	end;

// refresh

	rcRefresh;
	
end.
              