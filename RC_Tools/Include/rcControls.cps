// rcControls /////////////////////////////////////////////////////////////

function find_control(root_object:TWinControl; vc_name:string):TObject;
//
// Recursive function for finding visual components in WinControl components (panel, window etc..)   
// Result is TObject (TPanel, TButton, TLabel, TEdit, TImage, TShape...)
// 
// root_object = parent object for searching (goes to subobjects also)
// c_name = name of object

var
   i:integer;
   xo:TObject;
begin 
  // writeln ('Trying to found control: '+vc_name);
  for i:=0 to root_object.ControlCount-1 do begin
     // writeln ('Analising object number '+IntToStr(i));
     xo:=root_object.Controls[i];
     if xo is Tlabel       then if UpperCase(vc_name)=UpperCase(TLabel(xo).name)       then result:=xo; 
     if xo is Tpanel       then if UpperCase(vc_name)=UpperCase(TPanel(xo).name)       then result:=xo;
     if xo is TButton      then if UpperCase(vc_name)=UpperCase(TButton(xo).name)      then result:=xo;
     if xo is TEdit        then if UpperCase(vc_name)=UpperCase(TEdit(xo).name)        then result:=xo;
     if xo is TMImage      then if UpperCase(vc_name)=UpperCase(TMImage(xo).name)      then result:=xo;
     if xo is TShape       then if UpperCase(vc_name)=UpperCase(TShape(xo).name)       then result:=xo;
     if xo is TForm        then if UpperCase(vc_name)=UpperCase(TForm(xo).name )       then result:=xo; 
     if xo is TScrollBox   then if UpperCase(vc_name)=UpperCase(TScrollBox(xo).name)   then result:=xo;
     if xo is TListBox     then if UpperCase(vc_name)=UpperCase(TListBox(xo).name)     then result:=xo;
     if xo is TGroupBox    then if UpperCase(vc_name)=UpperCase(TGroupBox(xo).name)    then result:=xo;
     if xo is TCheckBox    then if UpperCase(vc_name)=UpperCase(TCheckBox(xo).name)    then result:=xo;
     if xo is TRadioButton then if UpperCase(vc_name)=UpperCase(TRadioButton(xo).name) then result:=xo;
     if xo is TComboBox    then if UpperCase(vc_name)=UpperCase(TComboBox(xo).name)    then result:=xo;
     if (result=nil) and (xo is TWinControl) then result:=find_control(TWinControl(xo),vc_name);
 
  end;  // for 
end;  // function
//--------------------------------------------------------------------------------------
Function rcTool(tName:string):TForm;
// form with custom caption, after creation MUST use show or showmodal
var 
    TF:TForm;
Begin
    TF:=TForm.create(nil);
    // TF.Left:=x;TF.Top:=y;TF.Width:=w;TF.Height:=h;
    // TF.FormStyle:=fsStayOnTop; 
    TF.BorderStyle:=bsDialog; 	// bsToolWindow 	- ikona NE, pomak DA, resize NE, caption DA, tipkice DA, border ne 
											// bsSizeToolWin 	- ikona NE, pomak DA, resize DA, caption DA, tipkice DA, border ne
											// bsNone 			- ikona NE, pomak NE, resize NE, caption NE, tipkice NE, border ne 
											// bsDialog 		- ikona NE, pomak DA, resize NE, caption DA, tipkice DA, border DA (1 linija)
											// bsSizeable     - ikona da, pomak DA, resize DA, caption DA, tipkice DA, border DA (1 linija)
											// bsSingle       - ikona da, pomak DA, resize NE, caption DA, tipkice DA, border DA (1 linija)
    TF.Name:=tName;                                
	 // TF.Icon:=nesto;	// ne radi                 
	 // Pozicioniranje forme na ekranu
   // TF.Position:=poScreenCenter;  // u ovom slu�aju ne�e raditi proizvoljno pozicioniranje 
	TF.Position:=poDesigned; 		   // sad radi TF.Top i TF.Left
	// TF.Top:=50;
	// TF.Left:=screenwidth-TF.Width-50;
    TF.Font.Name:='Arial';
    TF.Font.Size:=9;
	 // TF.BorderIcons:=[biSystemMenu];   // biHelp; biMaximize; biMinimize; biSystemMenu
	 // 
    
    // TF.Caption:=tCaption;
    result:=TF;
End; 
//------------------------------------------------------------------------------
function rcPanel( pName: string; pParent: TWinControl):TPanel;
var
   Panel_1:TPanel;
   xo:TObject;
 begin
     if pName<>'' then begin 
                       xo:=nil; 
                       find_control(pParent,pName);
                       if xo<>nil then Begin 
                          ShowMessage ('Panel with name: "'+pName+'" already exist!'); 
                          exit 
                       end;
                   end;    
     Panel_1:=Tpanel.create(pParent);
     if pName<>'' then Panel_1.name:=pName;
     //Panel_1.align:=alNone;    // alNone, alTop, alBottom, alLeft, alRight, alClient 
     Panel_1.parent:=pParent;
     //Panel_1.BevelInner:=bvNone;
     //Panel_1.BevelOuter:=bvRaised;
     //Panel_1.BorderStyle:=bsNone;

     Panel_1.Caption:='';
     Panel_1.Left:=100;
     Panel_1.Top:=100;
     Panel_1.Width:=300;
     Panel_1.Height:=300;
     //Panel_1.BevelInner:=bvNone;
     //Panel_1.BevelOuter:=bvNone;
     Panel_1.BevelWidth:=1;
     //Panel_1.BorderStyle:=bsSingle;
     //Panel_1.BorderWidth:=3;
     //Panel_1.Ctl3D:=False;
     //PanelP1.Font.style:=[fsBold];
     Panel_1.ParentFont:=True;
     // Panel_1.Font.Name:='Arial';
     // Panel_1.Font.Size:=10;
     // ne radi Panel_1.FullRepaint:=True;
     result:=Panel_1;
end;

function rcScrollBox( pName: string; pParent: TWinControl):TScrollBox;
var
   Panel_1:TScrollBox;
   xo:TObject;
 begin
     if pName<>'' then begin 
                       xo:=nil; 
                       find_control(pParent,pName);
                       if xo<>nil then Begin 
                          ShowMessage ('Panel with name: "'+pName+'" already exist!'); 
                          exit 
                       end;
                   end;    
     Panel_1:=TScrollBox.create(pParent);
     if pName<>'' then Panel_1.name:=pName;
     //Panel_1.align:=alNone;    // alNone, alTop, alBottom, alLeft, alRight, alClient 
     Panel_1.parent:=pParent;
     //Panel_1.BevelInner:=bvNone;
     //Panel_1.BevelOuter:=bvRaised;
     //Panel_1.BorderStyle:=bsNone;

     // Panel_1.Caption:='';  NE RADI!
     Panel_1.Left:=100;
     Panel_1.Top:=100;
     Panel_1.Width:=300;
     Panel_1.Height:=300;
     //Panel_1.BevelInner:=bvNone;
     //Panel_1.BevelOuter:=bvNone;
     // Panel_1.BevelWidth:=1;  NE RAdI!!
     //Panel_1.BorderStyle:=bsSingle;
     //Panel_1.BorderWidth:=3;
     //Panel_1.Ctl3D:=False;
     //PanelP1.Font.style:=[fsBold];
     Panel_1.ParentFont:=True;
     // Panel_1.Font.Name:='Arial';
     // Panel_1.Font.Size:=10;
     // ne radi Panel_1.FullRepaint:=True;
     result:=Panel_1;
end;


//------------------------------------------------------------------------------
function rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
// Talignment: taLeftJustify, taCenter, taRightJustify 
 var
   Text_1:TLabel;
   xo:TObject;
 begin
     if lName<>'' then begin 
                       xo:=nil; 
                       find_control(LParent,LName);
                       if xo<>nil then Begin 
                          ShowMessage ('Label name: "'+LName+'" already exist!'); 
                          exit 
                       end;
                   end;    
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     if LName<>'' then Text_1.name:=LName;
     Text_1.caption:=LCaption; 
     Text_1.ParentFont:=True;
     Text_1.font.name:='arial';
     Text_1.font.size:=8;
     Text_1.Alignment:=LA;
	  Text_1.ShowHint:=True;
     if LA=taRightJustify then Text_1.left:=LX-text_1.width
         else if LA=taLeftJustify then Text_1.left:=LX
				else if LA=taCenter then Text_1.left:=Round(LX-text_1.width/2);
			
     Text_1.top:=LY;
     result:=Text_1;
 end; 
 //------------------------------------------------------------------------
Procedure rcSpaceT( LParent: TWinControl; LY:integer);
// make spacing (top aligned)
 var Text_1:TLabel;
 begin
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     Text_1.caption:='  '; Text_1.font.size:=3; Text_1.top:=LY;
     Text_1.Align:=alTop;
 end; 
 Procedure rcSpaceB( LParent: TWinControl; LY:integer);
// make spacing (bottom aligned)
 var Text_1:TLabel;
 begin
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     Text_1.caption:='  '; Text_1.font.size:=3; Text_1.top:=LY;
     Text_1.Align:=alBottom;
 end; 
 Procedure rcSpaceL( LParent: TWinControl; LX:integer);
// make spacing (left aligned)
// Left position must be BIG!!! (100,200,300)
 var Text_1:TLabel;
 begin
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     Text_1.caption:='  '; Text_1.font.size:=3; Text_1.left:=LX;
     Text_1.Align:=alLeft;
 end; 
 Procedure rcSpaceR( LParent: TWinControl; LX:integer);
// make spacing (right aligned)
 var Text_1:TLabel;
 begin
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     Text_1.caption:='   '; Text_1.font.size:=3; Text_1.left:=LX;
     Text_1.Align:=alRight;
 end; 
 //------------------------------------------------------------------------
function rcButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TButton;
// Create Button
 var
   Button_1:TButton;
   xo:TObject;
 begin
     if bName<>'' then begin 
                       xo:=nil; 
                       find_control(bParent,bName);
                       if xo<>nil then Begin 
                          ShowMessage ('Button with name: "'+bName+'" already exist!'); 
                          exit 
                       end;
                   end;     
     Button_1:=TButton.create(bParent);
     Button_1.parent:=bParent;
     if bName<>'' then Button_1.name:=bName;  
     Button_1.caption:=bCaption;
     Button_1.Left:=bX;
     Button_1.Top:=bY;
     Button_1.Width:=120;
     Button_1.Height:=25
     Button_1.ShowHint:=true;
     Button_1.ParentFont:=True;
     // Button_1.Font.Name:='Arial';
     // Button_1.Font.Size:=9;
     result:=Button_1;
end;

function rcSpeedButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TSpeedButton;
// Create Speed Button
 var
   Button_1:TSpeedButton;
   xo:TObject;
 begin
     if bName<>'' then begin 
                       xo:=nil; 
                       find_control(bParent,bName);
                       if xo<>nil then Begin 
                          ShowMessage ('Button with name: "'+bName+'" already exist!'); 
                          exit 
                       end;
                   end;     
     Button_1:=TSpeedButton.create(bParent);
     Button_1.parent:=bParent;
     if bName<>'' then Button_1.name:=bName;  
     Button_1.caption:=bCaption;
     Button_1.Left:=bX;
     Button_1.Top:=bY;
     Button_1.Width:=120;
     Button_1.Height:=25
     Button_1.ShowHint:=true;
     Button_1.ParentFont:=True;
     // Button_1.Font.Name:='Arial';
     // Button_1.Font.Size:=9;
	  // Button_1.Font.Style:=[FsBold];
	  // Button_1.Color:= ClRed;  // NE RADI!
	  // Button_1.Flat:= True;		// NE RADI!
	  // Button_1.Down:= True;    // Radi, ali se ne primijeti razlika
	  
	  
	  
     result:=Button_1;
end;

function rcCheckBox(cbName, cbCaption: string; cbParent: TWinControl; cbX, cbY: integer):TCheckBox;
// Create CheckBox
 var
   CB1:TCheckBox;
   xo:TObject;
 begin
     if cbName<>'' then begin 
                       xo:=nil; 
                       find_control(cbParent,cbName);
                       if xo<>nil then Begin 
                          ShowMessage ('CheckBox with name: "'+cbName+'" already exist!'); 
                          exit 
                       end;
                   end;     
     CB1:=TCheckBox.create(cbParent);
     CB1.parent:=cbParent;
     if cbName<>'' then CB1.name:=cbName;  
     CB1.caption:=cbCaption;
     CB1.Left:=cbX;
     CB1.Top:=cbY;
     CB1.Alignment:=taLeftJustify;
     // CB1.Width:=120;
     // CB1.Height:=25
     CB1.ShowHint:=true;
     CB1.ParentFont:=True;
     // CB1.Font.Name:='Arial';
     // CB1.Font.Size:=9;
     result:=CB1;
end;

function rcRadioButton(cbName, cbCaption: string; cbParent: TWinControl; cbX, cbY: integer):TRadioButton;
// Create Radio Button
 var
   CB1:TRadioButton;
   xo:TObject;
 begin
     if cbName<>'' then begin 
                       xo:=nil; 
                       find_control(cbParent,cbName);
                       if xo<>nil then Begin 
                          ShowMessage ('CheckBox with name: "'+cbName+'" already exist!'); 
                          exit 
                       end;
                   end;     
     CB1:=TRadioButton.create(cbParent);
     CB1.parent:=cbParent;
     if cbName<>'' then CB1.name:=cbName;  
     CB1.caption:=cbCaption;
     CB1.Left:=cbX;
     CB1.Top:=cbY;
     CB1.Alignment:=taLeftJustify;
     // CB1.Width:=120;
     // CB1.Height:=25
     CB1.ShowHint:=true;
     CB1.ParentFont:=True;
     // CB1.Font.Name:='Arial';
     // CB1.Font.Size:=9;
     result:=CB1;
end;

function rcMemo(mName:string; mParent: TWinControl; mX, mY, mW, mH: integer):TMemo;
var
   Memo_1:TMemo;
   xo:TObject;
begin
   if mName<>'' then begin 
                       xo:=nil; 
                       find_control(mParent,mName);
                       if xo<>nil then Begin 
                          ShowMessage ('Memo name: "'+mName+'" already exist!'); 
                          exit 
                       end;
                   end;     
   Memo_1:=TMemo.create(mParent);
   Memo_1.parent:=mParent;
   if mName<>'' then Memo_1.name:=mName;
   Memo_1.Left  :=mX;
   Memo_1.Top   :=mY;
   Memo_1.Width :=mW;
   Memo_1.Height:=mH;
   Memo_1.Lines.Clear;
   memo_1.ParentFont:=True;
   memo_1.ScrollBars:=ssVertical;
   result:=Memo_1;
end;

Function rcEdit(eName: string; eDest: TWinControl; eX, eY: integer):TEdit;
var
     e1:TEdit;
     xo:TObject;
begin
      if eName<>'' then begin 
                       xo:=nil; 
                       find_control(eDest,eName);
                       if xo<>nil then Begin 
                          ShowMessage ('Edit name: "'+eName+'" already exist!'); 
                          exit 
                       end;
                   end;              
     e1:=TEdit.create(eDest);
     e1.parent:=eDest;
     if eName<>'' then e1.name:=eName;
     e1.Left:=eX; e1.Top:=eY;
     e1.ParentFont:=True;
     e1.Text:='';
     e1.ctl3D:=False;
     e1.width:=30;
     e1.Showhint:=true;
     // e1.Alignment:=taRightJustify;   NOT POSSIBLE!
     // e1.showhint:=true;
     
     
     result:=e1;
end;

Function rcEditL(cap, units: string; destination: TWinControl; eX, eY: integer; kind:string):TEdit;
// create field for entering data (TEdit) and caption
// units is text next to edit field
// placement of caption depend of kind string
// 'left' - caption is left of the field, origin is on the left side of the field
// 'right' - caption is right of the field, origin is on the right side of the field
// 'top-left' - caption is under field. left aligned
// 'top-right' - caption is under field. right aligned
// cap - caption
// destination - parent
// eX, eY - position
// eg.
// input:=RC_Edit('Insert Your name: ','mm', Panel2, 100, 20, 'left');
 var
   Txt1,u2:TLabel;   // u2=caption
   ce1:TEdit;
begin
     
     if cap='' then cap:=' ';
     //   begin             
            Txt1:=TLabel.create(destination);	
            Txt1.parent:=destination;				
            Txt1.ParentFont:=True;					
            Txt1.caption:=cap;						
     //   End;
     ce1:=TEdit.create(destination);
     ce1.parent:=destination;
     //ce1.ParentFont:=True;
     ce1.width:=50;
     ce1.showhint:=true;
     
     u2:=TLabel.create(destination);
     u2.parent:=destination;
     u2.ParentFont:=True;
     u2.caption:=units;
     
     Case kind of 
          'left'     : begin
                         txt1.left:=eX;
                         txt1.top:=eY+3;
                         txt1.alignment:=taLeftJustify;
                         ce1.left:=txt1.left+txt1.width+2;
                         ce1.top:=eY;
								 // caption
                         u2.left:=ce1.left+ce1.width+5;
                         u2.top:=txt1.top;
								 
                       end;
          'right'    : Begin
                        
                         
                         ce1.left:=ex-ce1.width-u2.width;
                         ce1.top:=eY;
                         txt1.alignment:=taLeftJustify;
                         txt1.left:=eX-ce1.width-txt1.width-5-u2.width;
                         txt1.top:=eY+3;
								 u2.left:=ce1.left+ce1.width+5;
                         u2.top:=txt1.top;
								 u2.alignment:=taRightJustify;
                         {
                         ce1.left:=ex-ce1.width;
                         ce1.top:=eY;
                         txt1.alignment:=taLeftJustify;
                         txt1.left:=eX-ce1.width-txt1.width-5;
                         txt1.top:=eY+3;
                         }
                         
                       end;
          'top-left' : Begin 
                         txt1.left:=eX;
                         txt1.top:=eY;                         
                         txt1.alignment:=taLeftJustify;
                         ce1.left:=ex;
                         ce1.top:=eY+15;
                         u2.left:=eX+ce1.width+5;
                         u2.top:=eY+3+15;
                         u2.alignment:=taLeftJustify;
                       end;            
          'top-right': Begin
                         txt1.left:=eX;
                         txt1.alignment:=taRightJustify;
                         txt1.top:=eY;                  
                         u2.left:=eX;
                         u2.alignment:=taRightJustify;
                         u2.top:=eY+3+20;
                         ce1.left:=eX-u2.width-5;
                         ce1.top:=eY+20;
                       End;
     end;
     result:=ce1;
end;
 
Function rcScroll(sName: string; sDest: TWinControl; sX, sY: integer):TScrollBar;
var
     s1:TScrollBar;
     xo:TObject;
 begin
      if sName<>'' then begin 
                       xo:=nil; 
                       find_control(sDest,sName);
                       if xo<>nil then Begin 
                          ShowMessage ('Edit name: "'+sName+'" already exist!'); 
                          exit 
                       end;
                   end;              
     s1:=TScrollBar.create(sDest);
     s1.parent:=sDest;
     if sName<>'' then s1.name:=sName;
     s1.Left:=sX; s1.Top:=sY;
     s1.Height:=10;
     s1.showhint:=true;
     result:=s1;
 end;
 
 // CoB_tip:=BoardType(Pan_Red[i],1,1);
Function rcBoardType(BotDest: TWinControl; bX, bY: integer):TComboBox;
var
     bot1:TComboBox;
     xo:TObject;
     dir,path,dat,poc_nule:string;
     tipovi2:TStringList;
     i:Integer;
     ok:boolean;     
 begin
     bot1:=TComboBox.create(BotDest);
     bot1.parent:=BotDest;
     bot1.Left:=bX; bot1.Top:=bY;
     bot1.ParentFont:=True;
     bot1.Font.Size:=9;
     // bot1.Style:=csDropDownList;
     bot1.Style:=csOwnerDrawFixed;   // sad se moze mijenjati visina
     bot1.ItemHeight:=13;
     bot1.font.size:=9;
     bot1.ItemIndex:=-1;
     with bot1 do begin
      showhint:=true;
      // hint:='With radi!!!!';
    End;
     // bot1.showhint:=true;
     
     bot1.Items.Add('000 Korpus');
     bot1.Items.Add('001 Fronta');
     bot1.Items.Add('002 Polica');
     bot1.Items.Add('003 Cokla');
     bot1.Items.Add('004 Radna ploca');
     bot1.Items.Add('005 Nogica');
     bot1.Items.Add('006 Sudoper');
     bot1.Items.Add('007 Ruckica');
     bot1.Items.Add('008 Le�a');
     bot1.Items.Add('009 Zid donji');
     bot1.Items.Add('010 Zid gornji');
     bot1.Items.Add('011 Pod');
     bot1.Items.Add('012 Strop');
     bot1.Items.Add('013 Okvir fronte');
     bot1.Items.Add('014 Proizvoljni');
     bot1.Items.Add('015 Bok');
     bot1.Items.Add('016 Zidna letva');
     bot1.Items.Add('017 Stropna ploha');
     bot1.Items.Add('018 Podna ploha');
     
     // read user types
     dir:=PrgFolder;
     // path:='System\';    STARA PUTANJA
	  path:='System\LNG\Croatian\';     // NOVA PUTANJA!!!!!
     dat:='TipDaske.dat';
     
     tipovi2:=TStringList.Create;
     // ShowD('Ucitavam korisnicke tipove dasaka');
     OK:=True;
     try 
        tipovi2.LoadFromFile(dir+path+dat);  // Load from Testing.txt file
     except begin
        // Print('Ne mogu pronaci datoteku "'+dir+path+dat+'"!!!'); 
		  ShowMessage('Ne mogu pronaci datoteku "'+dir+path+dat+'"!!!'); 
        ok:=false;
            end;
     end;
     // ShowD(Tipovi2.text); 
     if ok then begin
         for i:=0 to tipovi2.count-1 do begin
            if i<10 then poc_nule:='00';
            if (i>9) and (i<100) then poc_nule:='0';
            if (i>99) and (i<1000) then poc_nule:=''; 
            // ShowD(poc_nule+IntToStr(99+i)+' '+tipovi2[i]);   
            bot1.Items.Add(IntToStr(100+i)+' '+tipovi2[i]); 
         end;  // for i
         // ShowD('Ucitani!');
     end;  // if ok
     
     
     
     result:=bot1;
 end;
 
Function rcElementType(BotDest: TWinControl; bX, bY: integer):TComboBox;
var
     bot1:TComboBox;
     xo:TObject;
     dir,path,dat,poc_nule:string;
     tipovi2:TStringList;
     i:Integer;
     ok:boolean;     
 begin
     bot1:=TComboBox.create(BotDest);
     bot1.parent:=BotDest;
     bot1.Left:=bX; bot1.Top:=bY;
     bot1.ParentFont:=True;
     bot1.Font.Size:=9;
     // bot1.Style:=csDropDownList;
     bot1.Style:=csOwnerDrawFixed;   // sad se moze mijenjati visina
     bot1.ItemHeight:=13;
     bot1.font.size:=9;
     bot1.ItemIndex:=-1;
     with bot1 do begin
      showhint:=true;
      // hint:='With radi!!!!';
    End;
     // bot1.showhint:=true;
     
     bot1.Items.Add('000 Element');
     bot1.Items.Add('001 Aparat (3D - import)');
     bot1.Items.Add('002 Podni element');
     bot1.Items.Add('003 Podni kutni element');
     bot1.Items.Add('004 Vise�i element');
     bot1.Items.Add('005 Vise�i kutni element');
     bot1.Items.Add('006 Ladica');
     bot1.Items.Add('007 Fronta elementa');
     bot1.Items.Add('008 Radna plo�a - element');
     bot1.Items.Add('009 Vrata');
     bot1.Items.Add('010 Prozor');
     bot1.Items.Add('011 Svjetlo - element');
     bot1.Items.Add('012 Zid');
     bot1.Items.Add('013 Corpusov aparat');
     bot1.Items.Add('014 Ruckica element');
     bot1.Items.Add('015 Kolona elemet');
     bot1.Items.Add('016 Nogica - element');
     bot1.Items.Add('017 Sudoper - element');
     bot1.Items.Add('018 Le�na maska - element');
	  bot1.Items.Add('019 Cokla - element');
	  bot1.Items.Add('020 Zidna letva - element');
	  bot1.Items.Add('021 Bo�na maska - element');
	  bot1.Items.Add('022 Stropna ploha - element');
	  bot1.Items.Add('023 Podna ploha - element');
     
     result:=bot1;
 end;
 
Function rcComboBox(BotDest: TWinControl; bX, bY: integer):TComboBox;
var
     bot1:TComboBox;
     xo:TObject;
     ok:boolean;     
 begin
     bot1:=TComboBox.create(BotDest);
     bot1.parent:=BotDest;
     bot1.Left:=bX; bot1.Top:=bY;
     bot1.ParentFont:=True;
     bot1.Font.Size:=9;
     // bot1.Style:=csDropDownList;
     bot1.Style:=csOwnerDrawFixed;   // sad se moze mijenjati visina
     bot1.ItemHeight:=13;
     
	  // bot1.AutoSize:=True; NE RADI
	  // bot1.Font.Size:=True; NE MIJENJA VISINU COMBOBOXA
	  // bot1.Height:=50; NE RADI NI�TA IAKO NE JAVLJA GRE�KU
	  
	  // bot1.ItemHeight:=16;  RADI!!!!
	  
	  
     bot1.ItemIndex:=-1;
     with bot1 do begin
      showhint:=true;
      // hint:='With radi!!!!';
    End;
     // bot1.showhint:=true;
{     
     bot1.Items.Add('000 Korpus');
     bot1.Items.Add('001 Fronta');
     bot1.Items.Add('002 Polica');
     bot1.Items.Add('003 Cokla');
}
     
     result:=bot1;
 end;
 
 
 
Function rcEdge(EdgeDest: TWinControl; eX, eY: integer):TComboBox;
// rubne trake
var
     Edge1:TComboBox;
     dir,path,dat,poc_nule:string;
     trake:TStringList;
     red,tekst,zav_znak, new_edge:String;
     i,j,poz_poc,poz_zav:Integer;
     ok:boolean;     
 begin
     Edge1:=TComboBox.create(EdgeDest);
     Edge1.parent:=EdgeDest;
     Edge1.Left:=eX; Edge1.Top:=eY;
     Edge1.ParentFont:=True;
     Edge1.Font.Size:=9;
     // Edge.Style:=csDropDownList;
     Edge1.Style:=csOwnerDrawFixed;   // sad se moze mijenjati visina
     Edge1.ItemHeight:=13;
     Edge1.font.size:=9;
     Edge1.ItemIndex:=-1;
     Edge1.showhint:=true;
     
     Edge1.Items.Add('nema trake');
     Edge1.Items.Add('razno');
     // Edge1.Items.Add('002 ABS 2');
     
     
     dir:=PrgFolder;
     path:='Potrosni\';
     dat:='Trake.cxm';
     
     trake:=TStringList.Create;
     // ShowD('Ucitavam trake');
     OK:=True;
     try 
        trake.LoadFromFile(dir+path+dat);  // Load from Testing.txt file
     except begin
        // Print('Ne mogu pronaci datoteku "'+dir+path+dat+'"!!!'); 
        ok:=false;
            end;
     end;
     // ShowD(Tipovi2.text); 
     if ok then begin
         for i:=0 to trake.count-1 do begin
            red:=trake[i];             // jedan red iz "trake.cxm"
            tekst:='<PotItem Naziv="';      // tekst koji ozna�ava novu rubnu traku
            j:=Pos(tekst,red);
            if not (j=0) then begin    // pronadjen je trazeni tekst!!!
               //showmessage('Tekst "'+tekst+'" se nalazi u redu: '+IntToStr(i)+' na mjestu: '+IntToStr(j));
               // trazi zavrsni navodnik
               poz_poc:=(j+Length(tekst));   // pozicija pocetka trazenog naziva
               //showmessage('Tekst zavrsava na mjestu: '+IntToStr(poz_poc));
               poz_zav:=poz_poc+1;
               repeat
                     // zav_znak:=copy(red,poz_zav,1);
                     zav_znak:=red[poz_zav];
                     // showmessage('zav_znak '+zav_znak+' '+IntToStr(Poz_zav));
                     if zav_Znak='"' then begin     //  pronadjen je zavrsni znak
                           new_edge:=copy(red,poz_poc,poz_zav-poz_poc);
                           // showmessage('nova traka: '+new_edge);           
                           Edge1.Items.Add(new_edge);   
                     end;
                     poz_zav:=poz_zav+1;
               until (zav_Znak='"') or (poz_zav=Length(red));
            end; //  if not (j=0)
         end; // for i:=0
         // result:=copy(popis,1,Length(popis)-1);
         // T1.enabled:=true;
     end;  // if ok
     // Edge1.Items.Add('Razno');
     
     
     result:=Edge1;
 end; 
 
Function rcLoadImage(iName,iPath:String;iDest: TWinControl; iX, iY: integer):TMImage;
var
     im1:TMImage;
     xo:TObject;
     OK:Boolean;
 begin
      if iName<>'' then begin 
                       xo:=nil; 
                       find_control(iDest,iName);
                       if xo<>nil then Begin 
                          ShowMessage ('Image name: "'+iName+'" already exist!'); 
                          exit 
                       end;
                   end;              
     im1:=TMImage.create(iDest);
     im1.parent:=iDest;
     if iName<>'' then im1.name:=iName;
     im1.Left:=iX; im1.Top:=iY;
 
     im1.loadFromFile(ipath);
     im1.Stretch := False ;     // da li da se stisne ili razvu�e na veli�inu TMImage-a
     im1.ShowHint:=True;
     im1.AutoSize:=true;      // da li da veli�inu TMImage-a prilagodi uvoznoj slici
     //im1.width:=100;
     //im1.height:=100;
     result:=im1;

End;

function rcBox(bName:String;bDest:TWinControl;bX,bY,bW,bH:Integer):TShape;
// Shape
var 
   BB:TShape;
   xo:TObject;
Begin
      if bName<>'' then begin 
                       xo:=nil; 
                       find_control(bDest,bName);
                       if xo<>nil then Begin 
                          ShowMessage ('Image name: "'+bName+'" already exist!'); 
                          exit 
                       end;
                   end;              
     bb:=TShape.create(bDest);
     bb.parent:=bDest;
     if bName<>'' then bb.name:=bName; 
     BB.Shape:=stRectangle;
     BB.Left:=bx;BB.Top:=by;BB.width:=bw;BB.Height:=bH;
     result:=BB;
end; 

function rcMessage(title,text,bbNo,bbYes:String):Boolean;
// YES/NO MEssage
var
	MesForm :  TForm;
	labTitle : TLabel;
	labText : TMemo;
	ButYes, ButNo : TButton;
Begin
	// kreiranje prozora
	MesForm:=TForm.create(nil);
	MesForm.BorderStyle:=bsDialog;
	MesForm.FormStyle:=fsStayOnTop; 
	MesForm.Position:=poScreenCenter;
	MesForm.Width:=300;
	MesForm.Height:=130;
	MesForm.Font.Name:='Arial';
   MesForm.Font.Size:=9;
	MesForm.Caption:=title;
	
	// kreiranje teksta
	labText:=TMemo.create(MesForm);
	labText.parent:=MesForm;
	labText.ParentFont:=True;
	labText.WordWrap:=True;
	labText.color:=mesForm.color;
	labText.ReadOnly:=True;
	labText.Lines.Add(text);
	labText.TabStop:=False;
	labText.Align:=alClient;
	
	// Tipke
	butNo:=TButton.create(mesForm);
   ButNo.parent:=mesForm;
	ButNo.Top:=60;
	ButNo.Left:=20;
	ButNo.Width:=120;
	ButNo.Caption:=bbNo;
	ButNo.ModalResult:=mrCancel;
	ButNo.Cancel:=True;
	ButNo.TabStop:=False;
	
	butYes:=TButton.create(mesForm);
   ButYes.parent:=mesForm;
	ButYes.Top:=60;
	ButYes.Left:=160;
	ButYes.Width:=120;
	ButYes.Caption:=bbYes;
	ButYes.ModalResult:=mrOK;
	ButYes.Default:=True;
	
	// poka�i formicu
	// MesForm.ShowModal;
	result:=false;
	if MesForm.ShowModal = mrOk then result:=true;
									
	MesForm.Free;
	
End;

Function ScreenWidth:integer;
// Find width of monitor screen
 var
     sForm:TForm;
 begin
     sForm:=TForm.create(nil);
     sForm.BorderStyle:=bsNone;        
     sForm.WindowState:=wsMaximized;
     sForm.show;
     Application.ProcessMessages;
     // showmessage('1');
     Application.ProcessMessages;
     // showmessage('2');
     // showmessage('Function ScreenWidth');
     result:=sForm.clientwidth;
     // showmessage('3');
     sForm.free;
     // nForm.ModalResult := 1;     
end; 

Function ScreenHeight:integer;
// Find width of monitor screen
 var
     nForm:TForm;
 begin
     nForm:=TForm.create(nil);
     nForm.BorderStyle:=bsNone;        
     nForm.WindowState:=wsMaximized;
     nForm.show;
     Application.ProcessMessages;
     // showmessage('Function ScreenHeight');
     result:=nForm.clientheight;
     nForm.free;
     // nForm.ModalResult := 1;
end; 


