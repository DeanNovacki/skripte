// dexunit v1.3 20.05.2011
// - popravljena procedura za pronala�enje svjetlog otvora
// - sada tra�i dasku tipa le�a za zadnju granicu
// 
// Window (TForm), naslov (TLabel), natpis (TLabel), Polje (TEdit)
// RadioBox (TradioGroup), CBox (TCheckBox), Tipka (TButton)
// S_otvor 

Function Sqr(a:single):single;
// kvadriranje
begin
     result:=a*a;
end;

Function Tg(a:single):single;
// tangens
begin
     result:=sin(a)/cos(a);
end;

Function Ctg(a:single):single;
// kotangens
begin
     result:=cos(a)/sin(a);
end;

// Function random(a:integer):integer;
     

function Window(nTekst: string; nS, nV: integer):TForm;
// kreira prozor
// nTekst = naziv prozora
// nS = Sirina prozora
// nV = Visina prozora
 var
     nProzor:TForm;
 begin
     nProzor:=TForm.create(nil);              // definiranje forme
     nProzor.borderIcons:=[];
     nProzor.borderStyle:=bsToolWindow;
     nProzor.position:=poScreenCenter;
     nProzor.Width:=nS;
     nProzor.Height:=nV;                       //top,width,height
     nProzor.caption:=nTekst; 
     result:=nProzor;
 end;

// function input(nTekst:string; 
 
function naslov( nTekst: string; nProzor: TWinControl):TLabel;
// procedura koja napravi label na gornjoj sredini prozora (panela)
// primjer: napisi('Ovo je naslov', Prozor)
// nTekst = Caption labele
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira labela
 var
   Tekst_1:TLabel;
 begin
     Tekst_1:=TLabel.create(nProzor);
     Tekst_1.parent:=nProzor;
     // Tekst_1.font.style:=[FsBold];
     Tekst_1.font.size:=14;
     Tekst_1.font.name:='Arial';
     Tekst_1.caption:=nTekst;
     // Tekst_1.left:=nProzor.width/2-Tekst_1.width/2;
	  Tekst_1.left:=50;
     Tekst_1.top:=10;
     result:=Tekst_1;
end;

function copanel( nTekst: string; nProzor: TWinControl):TPanel;
// procedura koja radi panel
// 
// 
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira labela
 var
   Panel_1:TPanel;
 begin
     Panel_1:=Tpanel.create(nProzor);
     //Panel_1.align:=alNone;    // alNone, alTop, alBottom, alLeft, alRight, alClient 
     Panel_1.parent:=nProzor;
     Panel_1.BevelInner:=bvNone;
     Panel_1.BevelOuter:=bvRaised;
     Panel_1.BorderStyle:=bsNone;
     Panel_1.BorderWidth:=0;
     Panel_1.Caption:='';
     //Panel_1.Left:=100;
     //Panel_1.Top:=100;
     //Panel_1.Width:=300;
     Panel_1.Height:=300;
     // ne radi Panel_1.FullRepaint:=True;
     result:=Panel_1;
end;



function natpis( nTekst: string; nProzor: TWinControl; nX, nY:integer; poravnanje:String):TLabel;
// procedura koja napravi label na nekoj poziciji
// primjer: natpis('Ovo je labela', Prozor, 100, 200, 'desno')
// nTekst = Caption labele
// nProzor = naziv prozora u kojem se kreira labela
// nX = udaljenost od lijevog ruba
// nY = udaljenost od vrha
// nPoravnanje = poravnanje    'desno' ili 'lijevo'
 var
   Tekst_1:TLabel;
 begin
     Tekst_1:=TLabel.create(nProzor);
     Tekst_1.parent:=nProzor;
     Tekst_1.caption:=nTekst; 
     if poravnanje='desno' then Tekst_1.left:=nX-tekst_1.width
                           else Tekst_1.left:=nX;
     Tekst_1.top:=nY;
     result:=Tekst_1;
 end;

function polje ( nTekst: string; nProzor: TWinControl; nX, nY: integer):TEdit;
// kreira edit za unos i tekst koji stoji kraj edita
//    polje(varijabla,'proizvoljni tekst',Prozor,150,160);
// nTekst = Tekst ispred (iza) polja za unos
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira edit i labela
// nX = udaljenost od lijevog ruba
// nY = udaljenost od vrha
// pozicija je po�etak edita, tekst ide prema lijevoj strani
 var
   Tekst_1:TLabel;
   Unos_1:TEdit;
 begin
     
     if nTekst<>'' then 
        begin
            Tekst_1:=TLabel.create(nProzor);
            Tekst_1.parent:=nProzor;
            Tekst_1.font.style:=[];
            Tekst_1.top:=nY;
            Tekst_1.caption:=nTekst;
            Tekst_1.Left:=nX-Tekst_1.Width-5; 
        End;
     Unos_1:=TEdit.create(nProzor);
     Unos_1.parent:=nProzor;
     Unos_1.width:=50;
     Unos_1.left:=nX;
     Unos_1.top:=nY-3;
     Unos_1.Text:='';
     result:=Unos_1;
 end;
 
function RadioBox(nTekst: string; nazivi:Array of String; def:integer; 
                          nProzor: TWinControl; nX, nY: integer):TRadioGroup;
// kreira Radio Grupu
// nTekst = Naslov Radio Grupe
// nazivi = niz koji odre�uje check boxove ['prvi','drugi', ...]
// def = defaultni check
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira edit i labela
// nX = udaljenost od lijevog ruba
// nY = udaljenost od vrha

 
 var
     RB_1:TRadioGroup;
     I:Integer;
 begin
     RB_1:=TRadioGroup.create(nProzor);
     RB_1.parent:=nProzor;
     RB_1.caption:=nTekst;
     For i:=0 to GetArrayLength(nazivi)-1 do 
         begin
              RB_1.Items.Add(nazivi[i]);
         end;
     If def>RB_1.Items.Count-1 then def:=RB_1.Items.Count-1;
     RB_1.Height:=10+RB_1.Items.Count*20;
     RB_1.ItemIndex:=def;
     RB_1.Left:=nX;
     RB_1.Top:=nY;
     result:=RB_1;
 end;
 
function CBox(nTekst: string; nProzor: TWinControl; nX, nY: integer):TCheckBox;
// kreira jedan CheckBox
// nTekst = Tekst koji pi�e ispred CheckBoxa
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira edit i labela
// nX = udaljenost od lijevog ruba
// nY = udaljenost od vrha
 var
     CBox_1:TCheckBox;
 begin
     CBox_1:=TCheckBox.create(nProzor);
     CBox_1.parent:=nProzor;
     CBox_1.width:=300;
     CBox_1.caption:=nTekst;
     CBox_1.Left:=nX;
     CBox_1.Top:=nY;
     result:=CBox_1;
 end;
 
function tipka(nTekst: string; nProzor: TWinControl; nX, nY: integer):TButton;
// kreira tipku
// nTekst = Tekst koji pi�e na tipki
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira edit i labela
// nX = udaljenost od lijevog ruba
// nY = udaljenost od vrha
 var
   Tipka_1:TButton;
 begin
     Tipka_1:=TButton.create(nProzor);
     Tipka_1.parent:=nProzor;
     Tipka_1.caption:=nTekst;
     Tipka_1.Left:=nX;
     Tipka_1.Top:=nY;
     result:=Tipka_1;
 end;
 
 function Memo(nProzor: TWinControl; nX, nY, nS, nV: integer):TMemo;
// kreira memo
// 
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira memo
// nX = udaljenost od lijevog ruba
// nY = udaljenost od vrha
// nS = �irina
// nV = visina

 var
   Memo_1:TMemo;
 begin
     Memo_1:=TMemo.create(nProzor);
     Memo_1.parent:=nProzor;

     Memo_1.Left:=nX;
     Memo_1.Top:=nY;
     Memo_1.Width:=nS;
     Memo_1.Height:=nV;
     result:=Memo_1;

 end;
 
 
 

 procedure S_otvor(var Li_naziv, De_naziv, Go_naziv, Do_naziv, Naz_naziv: string;
                              var Li_x, De_x, Go_y, Do_y, Naz_z,
                                  Li_xx, De_xx, Go_yy, Do_yy, Naz_zz: Integer;
                              var OK:Boolean) ;


// Detektiranje svijetlog otvora i njegovih granica
// Ako nisu selektirane 4 daske, tra�i svijetli otvor elementa

// Li_naziv = naziv daske koja je lijeva granica otvora
// De_naziv = naziv daske koja je desna granica otvora
// Go_naziv = naziv daske koja je gornja granica otvora
// Do_naziv = naziv daske koja je donja granica otvora
// Naz_naziv = naziv daske koja je zadnja granica otvora

// Li_x = pozicija X lijeve daske
// De_x = pozicija X desne daske
// Go_y = pozicija Y gornje daske
// Do_y = pozicija Y gornje daske
// Naz_z = pozicija Z straznje daske

// Li_xx = debljina lijeve daske
// De_xx = debljina desne daske
// Go_yy = debljina gornje daske
// Do_yy = debljina donje daske
// Naz_zz = debljina lijeve daske

// OK = svijetli otvor na�en ili nije (Boolean) 

                              
  var
	b:tbox; 
        i,j,TipD:integer;
        zz:Tdaska;
begin
        
        OK:=true;                        // procedura ispravno izvsena?
        // brojanje selektiranih dasaka
        j:=0; 
        for i:=0 to e.ChildDaska.CountObj-1 do begin
            if e.ChildDaska.Daska[i].selected then j:=j+1;
        end; 

        if (j<>4) and (j<>0) then begin
           ShowMessage('Selektirajte 4 daske ili niti jednu!');
           OK:=False;                  // procedura nije ispravno izvrsena!
           Exit;
        end;
        

	b:=inbox(e,0);                                    // �itanje svijetlog otvora

        Li_Naziv:='NEMA lijeve daske';Li_x:=100000;                   // po�etne vrijednosti 
        De_Naziv:='NEMA desne daske'; De_x:=-1;
        Go_Naziv:='NEMA gornje daske';Go_y:=-1;
        Do_Naziv:='NEMA donje daske'; Do_y:=100000;

      // pronalazenje naziva, dimenzija i pozicija ako su selektirane 4 daske
        
        if j=4 then begin
  
	   if (MinMaxObj.Left is tdaska) then begin
              Li_naziv:= Tdaska(MinMaxObj.Left).naziv;
              Li_x:=round(Tdaska(MinMaxObj.Left).xPos);                         // polozaj
              Li_xx:=round(Tdaska(MinMaxObj.Left).Debljina);                    // debljina
           end;
	   if (MinMaxObj.Right is tdaska) then begin 
              de_naziv:= Tdaska(MinMaxObj.Right).naziv;
              De_x:=round(Tdaska(MinMaxObj.Right).xPos);
              De_xx:=round(Tdaska(MinMaxObj.Right).Debljina);
           end;
	   if (MinMaxObj.Top is tdaska) then begin
              Go_naziv:= Tdaska(MinMaxObj.Top).naziv;
              Go_y:=round(Tdaska(MinMaxObj.Top).yPos);
              Go_yy:=round(Tdaska(MinMaxObj.Top).Debljina);
           end;
	   if (MinMaxObj.Bottom is tdaska) then begin
              Do_naziv:= Tdaska(MinMaxObj.Bottom).naziv;
              Do_y:=round(Tdaska(MinMaxObj.Bottom).yPos);
              Do_yy:=round(Tdaska(MinMaxObj.Bottom).Debljina);
           end;
        end 
        
        // pronalazenje naziva, dimenzija i debljina ako nije selektirano nista
          
        else begin      
                  for i:=0 to e.ChildDaska.CountObj-1 do                        // pro�i kroz sve daske u elementu
                     if (e.ChildDaska.Daska[i].Visible) and (e.ChildDaska.Daska[i].TipDaske<>Nogica) then 
                     begin
                        if e.ChildDaska.Daska[i].smjer = horiz then             // ispitavanje da li je horizontalna
                            begin 
                            If e.ChildDaska.Daska[i].yPos < Do_y  then          // trazi najnizu
                               begin 
                                  Do_naziv:= e.ChildDaska.Daska[i].Naziv;
                                  Do_y:=round(e.ChildDaska.Daska[i].yPos);
                                  Do_yy:=round(e.ChildDaska.Daska[i].Debljina);
                               end;
                            If e.ChildDaska.Daska[i].yPos > Go_y  then          // trazi najvisu 
                               begin 
                                  Go_naziv:= e.ChildDaska.Daska[i].Naziv;
                                  Go_y:=round(e.ChildDaska.Daska[i].yPos);
                                  Go_yy:=round(e.ChildDaska.Daska[i].Debljina);
                               end;
                            End;
                        if e.ChildDaska.Daska[i].smjer = VertBok then           // ispitavanje da li je horizontalna
                            Begin
                            If e.ChildDaska.Daska[i].xPos < Li_x  then          // trazi najljeviju 
                               begin 
                                  Li_naziv:= e.ChildDaska.Daska[i].Naziv;
                                  Li_x:=round(e.ChildDaska.Daska[i].xPos);
                                  Li_xx:=round(e.ChildDaska.Daska[i].Debljina);
                               end;                            
                            If e.ChildDaska.Daska[i].xPos > De_x  then          // trazi najdesniju 
                               begin
                                  De_naziv:= e.ChildDaska.Daska[i].Naziv;
                                  De_x:=round(e.ChildDaska.Daska[i].xPos);
                                  De_xx:=round(e.ChildDaska.Daska[i].Debljina);
                               end;
                            End; 
                     end;
        end;
        // trazi ledja
        Naz_Naziv:='Nema zadnje daske'; Naz_z:=round(0-e.dubina-101);
        for i:=0 to e.ChildDaska.CountObj-1 do                        // pro�i kroz sve daske u elementu
             if e.ChildDaska.Daska[i].Visible then begin                     // samo one koje su visible
             // -------------------
                   zz:=e.ChildDaska.Daska[i];
                   TipD:=integer(zz.tipdaske);                
                   // uvjeti da bude zadnja granica
                   if  (TipD=8) and                              // dali su le�a
                       (zz.smjer=VertFront) and                  // dali su frontalno okrenuta
                       (zz.xpos+zz.Dubina>Li_x) and              // dali su desno od lijeve granice
                       (zz.xpos<De_x) and                        // dali su lijevo od desne granice
                       (zz.ypos+zz.Visina>Do_y) and              // dali je iznad donje granice
                       (zz.ypos<Go_y) and                        // da li je ispod gornje granice
                       (zz.zpos<0) Then begin                    // da li je iza prednje strane elementa
                             Naz_naziv:= zz.Naziv;
                             Naz_z:=round(zz.zPos);
                             Naz_zz:=round(zz.Debljina);
                       end; 
                   {
                   if e.ChildDaska.Daska[i].smjer = VertFront then           // ispitavanje da li je fronta
                        If (e.ChildDaska.Daska[i].zPos > Naz_z) and 
                           (e.ChildDaska.Daska[i].zPos < 0)  then  begin    // trazi najdalju za ledja 
                             Naz_naziv:= e.ChildDaska.Daska[i].Naziv;
                             Naz_z:=round(e.ChildDaska.Daska[i].zPos);
                             Naz_zz:=round(e.ChildDaska.Daska[i].Debljina);
                        end; 
                   // end;      if e.ChildDaska.Daska[i].smjer = VertFront
                   }                        
            end;   //   if e.ChildDaska.Daska[i].Visible
        If Naz_z = 0-e.dubina-101 then          // ako je z ostao defaultni
                        Begin                             // zna�i da nema ledja
                             Naz_z:=round(e.dubina);             
                             Naz_zz:=0;
                        End; 
        If (Do_Naziv=Go_Naziv) or (Li_Naziv=De_Naziv) then
                Begin
                     ShowMessage('Nema dovoljno dasaka za odrediti svijetli otvor!.');
                     OK:=False;                  // procedura nije ispravno izvrsena!
                
                End;

        Writeln ('Dolje: '+Do_naziv+', y: '+ IntToStr(Do_y) + ', Debljina: '+ IntToStr(Do_yy));
        Writeln ('Gore: '+Go_naziv+', y: '+ IntToStr(Go_y) + ', Debljina: '+ IntToStr(Go_yy));

        
end;
