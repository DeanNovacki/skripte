var
   win,rf:tForm; 
   m:TMemo;
   xo:TObject;
   counter,im:integer;
   ime: array of string;
   indent:string;
   {$I \..\Include\rc_controls.cps}
function find_control2(root_object:TWinControl):TObject;
// find_control(root_object:TWinControl; c_type, c_name:string):TObject;
var
   i:integer;
   xo:TObject;
begin
  // for i:=0 to root_object.ControlCount-1 do begin
  for i:=0 to root_object.ControlCount-1 do begin
     xo:=root_object.Controls[i];
     counter:=counter+1;
     // writeln ('Child number '+IntToStr(i)+'  Total number of controls: '+IntToStr(counter));

     // if counter=1 then break;



     if xo is Tlabel  then begin 
                                 M.lines.add(indent+'TLabel named: '+TLabel(xo).name+' Caption: '+TLabel(xo).caption); 
                                 result:=xo; 
                           end; 
     if xo is Tpanel  then begin 
                                 M.lines.add(indent+'TPanel named: '+TPanel(xo).name+' Caption: '+Tpanel(xo).caption); 
                                 result:=xo; 
                           end; 
     if xo is TButton then begin 
                                 M.lines.add(indent+'TButton named: '+TButton(xo).name+' Caption: '+Tbutton(xo).caption);
                                 result:=xo; 
                           end; 
     if xo is TEdit   then begin M.lines.add(indent+'TEdit named: '+TEdit(xo).name);  result:=xo; end; 
     if xo is TMImage then begin M.lines.add(indent+'TMImage named: '+TMImage(xo).name);result:=xo; end; 
     if xo is TShape  then begin M.lines.add(indent+'TShape named: '+TShape(xo).name); result:=xo; end; 
     if xo is TForm      then begin 
                                    if TForm(xo).name='' then begin TForm(xo).name:=ime[im];
                                                                    TForm(xo).visible:=True;
                                                                    im:=im+1;
                                                         end; 
                                    M.lines.add(indent+'TForm named: '+TForm(xo).name+' Caption: '+TForm(xo).caption); 
                                    
                                    result:=xo; 
                               end;
     if xo is TScrollBox then begin M.lines.add(indent+'TScrollBox named: '+TScrollBox(xo).name); result:=xo; end;
     if xo is TGroupBox  then begin M.lines.add(indent+'TGroupBox named: '+TGroupBox(xo).name); result:=xo; end;     
     // if xo is TTabSheet   then begin writeln('TForm named: '+TForm(xo).name); result:=xo; end;
     // if xo is TPageControl   then begin writeln('TForm named: '+TForm(xo).name); result:=xo; end;
     // if xo is TStatusBar then begin writeln('TStatusBar named: '+TStatusBar(xo).name); result:=xo; end;
     // if xo is TToolBar then begin writeln('TToolBar named: '+TToolBar(xo).name); result:=xo; end;
     
     // if (result=nil) and (xo is TWinControl)  then begin
     if xo is TWinControl  then begin 
                                                   indent:=indent+'_ _ _';
                                                   result:=find_control2(TWinControl(xo));
                                                   indent:=copy(indent,1,Length(indent)-5);
                            end; 
  end;  // for 
  // writeln ('end for i');
end;  // function

var
   pp:TPanel;

begin
   // create form  
   
   win:=TForm.create(nil);
   win.width:=400; win.height:=1000;
   win.name:='Find_controls';
   win.caption:='Find controls';
   win.FormStyle:=fsStayOnTop;
   m:=TMemo.create(Win); m.parent:=win; 
   m.align:=alClient;
   m.ScrollBArs:=ssBoth; 
   
   SetArrayLength(ime, 10);
   im:=0;
   ime[0]:='Forma0';
   ime[1]:='Forma1';
   ime[2]:='Forma2';
   ime[3]:='Forma3';
   ime[4]:='Forma4';
   ime[5]:='Forma5';
   ime[6]:='Forma6';
   ime[7]:='Forma7';
   ime[8]:='Forma8';
   
   
   
   xo:=Find_Control2(application.MainForm); // Za sve
   
   
   
   // stool = Desno Stablo
   // pp:=TPanel(find_control(application.MainForm,'sdfsdstool'));
   //pp.color:=clBlue;
   // pp.visible:=True;
   
   // rf:=TForm(find_control(application.MainForm,'Forma0'));
   // if rf<>nil then rf.color:=clRed;
   
   
   // xo:=Find_Control2(pp); // Za sve


   // win.showmodal;
   
   win.show;
   while win.visible do begin
        application.processmessages;
        end;

   // destroy form
   win.free; 
end.