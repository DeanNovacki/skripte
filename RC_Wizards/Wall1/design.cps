{
procedure draw_adjustment_panels;
procedure draw_main_buttons;
procedure draw_debug_section;
procedure draw_headline;
procedure draw_presets;
Procedure Welcome;
Procedure draw_AP1;
Procedure draw_AP2;
Procedure draw_AP3;
Procedure draw_AP4;
Procedure draw_AP5;
Procedure draw_AP6;
Procedure Draw_AP7;
Procedure draw_AP8; 
Procedure draw_AP9;
Procedure Create_window;





}

procedure draw_adjustment_panels;
//  Main adjustment part

var
   i:Integer;
   VarNam:string;
begin

   // main box (with ScrollBar)  
   Adj_SB:=TScrollBox.create(W_Window);
   Adj_SB.parent:=W_Window;
   Adj_SB.Ctl3D:=False;
   Adj_SB.left:=220;
   Adj_SB.top:=60;
   Adj_SB.width:=550;
   Adj_SB.Height:=595;
   Adj_SB.Color:=clWindow;
   Adj_SB.VertScrollBar.Visible:=True;
   Adj_SB.VertScrollBar.Tracking:=True;   
   // Adj_SB.VertScrollBar.Smooth:=True;  ne radi!!
   Adj_SB.HorzScrollBar.Visible:=False;

   for i:=1 to 9 do begin

      // ADJUSTMENT PANELS
      
      // create panels for each adjustment
      AP[i]:=RC_Panel('',Adj_SB);   // create panel inside ScrollBox (without caption)
      // AP[i].name:='Adjustment_panel'+IntToStr(i);      
      AP[i].Left:=5;
      
      if i=1 then AP[i].Top:=5                                      // Top position depend of  
             else AP[i].Top:=AP[i-1].Top+AP[i-1].Height+5;          // above panel (except first)
      
      VarNam:='AP'+IntToStr(i);        // Make names for reading from ini file (AP1, AP2, AP3...)       

      AP[i].width:=520;     
      AP[1].Color:=clBtnFace; 
      AP[1].Ctl3D:=False;    // flat look 
      
      // MINIMIZE BUTTON
      
      // create little symbol panels with minimize/open symbol
      APS[i]:=RC_Panel('',AP[i]);                // this panel is inside each adjustment panel
      APS[i].Left:=-1;
      APS[i].Top:=-1;
      APS[i].Height:=25;
      APS[i].width:=25;     
      APS[i].Font.Name:='Wingdings';
      APS[i].Alignment:=taCenter;
      APS[i].Caption:='!'
      APS[i].ShowHint:=True;
      APS[i].Hint:=txt_change_panel_visibility;      
      APS[i].Color:=clBtnShadow; 
      APS[i].Ctl3D:=False;


      
   End;  // for

   Mini_panel:=RC_Panel('',Adj_SB);     // unvisible panel, for aestetic reasons :-)
   Mini_panel.borderStyle:=bsNone;
   Mini_panel.Left:=1005;
   Mini_panel.Color:=clBtnFace;
   Mini_panel.width:=520;
   Mini_panel.Height:=5;
   

   // HEADLINE
   
   // create headline panel with name of adjustment
   // this panel is inside each adjustment panel
      
   // This panel MUST be created one by one becouse different names
   // Also, each panel have diferent hint
    
   APH[1]:=RC_Panel(txt_shape_panel_headline,AP[1]);
   APH[1].Hint:=txt_shape_panel_hint;
   
   APH[2]:=RC_Panel(txt_size_panel_headline,AP[2]);
   APH[2].Hint:=txt_size_panel_hint;
   
   APH[3]:=RC_Panel(txt_top_panel_headline,AP[3]);
   APH[3].Hint:=txt_top_panel_hint; 
      
   APH[4]:=RC_Panel(txt_bottom_panel_headline,AP[4]);
   APH[4].Hint:=txt_bottom_panel_hint;        
      
   APH[5]:=RC_Panel(txt_back_panel_headline,AP[5]);
   APH[5].Hint:=txt_back_panel_hint; 
   
   APH[6]:=RC_Panel(txt_sides_panel_headline,AP[6]);
   APH[6].Hint:=txt_sides_panel_hint;
   
   APH[7]:=RC_Panel(txt_shelves_panel_headline,AP[7]);
   APH[7].Hint:=txt_shelves_panel_hint; 
   
   APH[8]:=RC_Panel(txt_legs_panel_headline,AP[8]);
   APH[8].Hint:=txt_legs_panel_hint;  
   
   APH[9]:=RC_Panel(txt_door_panel_headline,AP[9]);
   APH[9].Hint:=txt_door_panel_hint;  
   
   // other properties
   For i:=1 to 9 do begin   
      APH[i].Left:=23;
      APH[i].Top:=-1;
      APH[i].Height:=25;
      APH[i].width:=496;     
      APH[i].Font.Name:='Arial';
      APH[i].Alignment:=taLeftJustify; 
      APH[i].ShowHint:=true;
      APH[i].Color:=clBtnShadow; 
      APS[1].Ctl3D:=False; 
   end; // if
   
   
   
  
end;   // procedure draw_adjustments
   
procedure draw_main_buttons;
// 
begin 

   // OK_Button
   Button_ok:=RC_Button(txt_ok_button, W_Window, 790,60);
   Button_ok.Hint:=txt_ok_button_hint;
   Button_ok.default:=False;
   
   // Cancel_Button
   Button_Cancel:=RC_Button(txt_cancel_button, W_Window, 790, 100);
   Button_Cancel.Hint:=txt_cancel_button_hint;
   Button_Cancel.default:=False;
   
   // Help_Button
   Button_Help:=RC_Button(txt_help_button, W_Window, 790, 140);
   Button_Help.Hint:=txt_help_button_hint;
   Button_Help.default:=False;
   Button_Help.visible:=False;
   
   // About_Button
   Button_About:=RC_Button(txt_about_button, W_Window, 790, 180);
   Button_About.Hint:=txt_about_button_hint;
   Button_About.default:=False;

end; // procedure draw_main_buttons;

procedure draw_debug_section;

begin
  
   // warning 
   warning_header:=RC_label('header text', W_Window,790,260, 'left');
   warning_header.font.color:=clRed;
   warning_header.font.style:=[fsBold];
   warning_header.visible:=False;
   warning_line1:=RC_label('line 1 text', W_Window,790,280, 'left');
   warning_line1.font.color:=clRed;
   warning_line1.font.size:=9;
   warning_line1.visible:=False;                         
   warning_line2:=RC_label('line 2 text', W_Window,790,295, 'left');
   warning_line2.font.color:=clRed;
   warning_line2.font.size:=9;
   warning_line2.visible:=False;                         
   warning_line3:=RC_label('line 3 text', W_Window,790,310, 'left');
   warning_line3.font.color:=clRed;
   warning_line3.font.size:=9;
   warning_line3.visible:=False;                         






end;  // procedure draw_main_buttons

procedure draw_headline;
// v1.07
begin
   // Main Headline
   Headline:=RC_Label(txt_Wall_Element_Wizard, W_Window, 40, 10, 'lijevo');
   Headline.Font.Name:='Arial';
   Headline.Font.Size:=16;
   // Bevel below headline
   BLine:=TBevel.create(W_Window);
   BLine.parent:=W_Window;
   BLine.Shape:=bsBottomLine;   
   BLine.Left:=20;
   BLine.Top:=1;
   BLine.Width:=955;
   BLine.Height:=40;
end;    //procedure draw_headline                   
 
  
procedure draw_presets;

begin
   // Headline 'Presets'
   PanelP1:=RC_Panel(txt_presets,W_Window);
   PanelP1.Left:=20;
   PanelP1.Top:=60;
   PanelP1.Height:=20;
   PanelP1.width:=180;     
   PanelP1.Alignment:=taLeftJustify;
   PanelP1.Font.Name:='Arial';
   // PanelP1.Font.style:=[fsBold];  
   PanelP1.Color:=clBtnShadow;  

   // Presets Content
   Preset_ListBox:=TListBox.Create(W_Window);
   Preset_ListBox.parent:=W_Window;
   Preset_ListBox.Left:=20;
   Preset_ListBox.Top:=83;
   Preset_ListBox.Width:=180;
   Preset_ListBox.Height:=460;
   Preset_ListBox.Font.Size:=8;
   Preset_ListBox.Font.Name:='Arial';
   // Preset_ListBox.Font.Style:=[fsBold];
   Preset_ListBox.ShowHint:=True;
   Preset_ListBox.hint:=txt_presets_hint;
   Preset_ListBox.Ctl3D:=False;
   
   {
   // Headline 'Info'
   PanelP2:=RC_Panel(txt_Info,W_Window);
   PanelP2.Left:=20;
   PanelP2.Top:=380;
   PanelP2.Height:=20;
   PanelP2.width:=180;     
   PanelP2.Alignment:=taLeftJustify;
   PanelP2.Font.Name:='Arial';
   //PanelP2.Font.style:=[fsBold];
   PanelP2.Color:=clBtnShadow;   

   
   // Memo for Info
   PresetM:=TMemo.create(W_Window);
   PresetM.parent:=W_Window;
   PresetM.Left:=20;
   PresetM.Top:=399;
   PresetM.Width:=180;
   PresetM.Height:=150;
   PresetM.Color:=clWindow;
   PresetM.Font.Name:='Arial';
   PresetM.Font.Size:=9;
   PresetM.Ctl3D:=False;
   PresetM.ShowHint:=True;
   PresetM.Hint:=txt_info_hint;
   }
   
   // Update preset button
   Button_update:=RC_Button(txt_Update_current_preset,W_Window,20,560);
   Button_update.Hint:=txt_update_current_preset_hint;   
   Button_update.enabled:=false;
   
   // Delete selected preset button
   Button_Delete:=RC_Button(txt_delete_selected_preset,W_Window,20,595);
   Button_Delete.Hint:=txt_delete_selected_preset_hint;
   Button_Delete.enabled:=false;      

   // Save as new preset button
   Button_Save:=RC_Button(txt_save_as_new_preset,W_Window,20,630);
   Button_Save.Hint:=txt_save_as_new_preset_hint;   
   
   // Field for new preset name
   
   // Unvisible until save
   Save_panel:=RC_Panel( '', W_Window);
   Save_panel.left:=20;
   Save_panel.top:=560;
   Save_panel.width:=180;
   Save_panel.Height:=100;
   Save_Field:=RC_Edit(txt_new_preset_name+': ','',Save_panel, 10, 5, 'top-left');
   Save_Field.width:=160;
   Save_Field.tag:=20;
   Butt_ok:=RC_Button(txt_ok_button,Save_panel,20,60);
   Butt_ok.width:=60;
   Butt_ok.default:=true;
   Butt_cancel:=RC_Button(txt_cancel_button,Save_panel,100,60);
   Butt_cancel.width:=60;
   
   Save_panel.visible:=false;
   
   // Field for update preset
   
   // Unvisible until update
   Update_panel:=RC_Panel( '', W_Window);
   Update_panel.left:=20;
   Update_panel.top:=560;
   Update_panel.width:=180;
   Update_panel.Height:=100;
   Update_field:=RC_Edit( txt_w_update_preset,'actual preset',Update_Panel, 10, 5, 'top-left');
   Update_field.width:=160;
   Update_field.enabled:=false;
   Update_field.tag:=30;
   Update_ok:=RC_Button(txt_ok_button,Update_panel,20,60);
   Update_ok.width:=60;
   Update_ok.default:=true;
   Update_cancel:=RC_Button(txt_cancel_button,Update_panel,100,60);
   Update_cancel.width:=60;
   
   Update_panel.visible:=false;
   
   // Field for delete preset
   
   // Unvisible until update
   Delete_panel:=RC_Panel( '', W_Window);
   Delete_panel.left:=20;
   Delete_panel.top:=560;
   Delete_panel.width:=180;
   Delete_panel.Height:=100;
   //Save_panel.font.Size:=8;
   
   Delete_field:=RC_Edit( txt_w_delete_preset,'actual preset',delete_Panel, 10, 5, 'top-left');
   Delete_field.width:=160;
   Delete_field.enabled:=false;
   Delete_ok:=RC_Button(txt_ok_button,Delete_panel,20,60);
   Delete_ok.width:=60;
   Delete_cancel:=RC_Button(txt_cancel_button,Delete_panel,100,60);
   Delete_cancel.width:=60;
   
   Delete_panel.visible:=false;
   


   
end;    //procedure draw_presets





Procedure Welcome;
// not used
var
   wl,w2,w3,w4:Tlabel;
Begin

   // message(Chr(13)+Chr(10)+'WELCOME STARTED'+Chr(13)+Chr(10));
   Hello:=RC_Panel('',W_Window);
   Hello.Height:=200;
   Hello.Width:=400;
   Hello.Left:=300;
   Hello.Top:=250;


   wl:=RC_Label('Red Cat Wizards',hello,50,15,'lijevo');
   wl.Font.name:='Times New Roman';
   wl.Font.style:=[fsBold];
   wl.Font.Size:=15;
   
   w2:=RC_Label('Base element wizard',hello,50,40,'lijevo');
   w2.Font.name:='Times New Roman';   
   w2.Font.style:=[fsBold];
   w2.Font.Size:=20;
   w3:=RC_Label('v 0.7 ',hello,50,70,'lijevo');   
   
      
   cc:=RC_Label('Click to continue',hello,50,100,'lijevo');
   cc.Font.Size:=10;
   
   w4:=RC_Label('Copyright: Red Cat Ltd. 2012',hello,50,180,'lijevo');   
End; 


 
Procedure draw_AP1;   // TYPE
// var
   // i:Integer;
   // TL, image_name_on: string;
   // image_name_off: string;
   
begin
    // message(Chr(13)+Chr(10)+'PROCEDURE DRAW_AP1 STARTED!'+Chr(13)+Chr(10));
    //showmessage('draw AP1 started');
    
    
     RC_Radio_image_Create (
    // RI_shelves:=RC_Radio_image (
                  {parent}             AP[1], 
                  {caption}            txt_select_shape_type,
                  {start_tag}          101,
                  {quantity}           6,
                  {default}            2,
                  {left}               10,
                  {top}                35,
                  {width}              500,
                  {image_width}        128,
                  {image_height}       128,
                  {image_names}        'shape',                // tag+on/of+'shape'+'.jpg'
                  {image_descriptions} txt_shape_descriptions,
                  {script_path}        script_path
                  );     
 
 // create memo for info
    AP1_memo:=TMemo.Create(AP[1]);
    AP1_memo.parent:=AP[1];
    AP1_Memo.left:=34;
    AP1_Memo.Top:=212+160;
    Ap1_Memo.Width:=452;
    AP1_Memo.Height:=110;
    AP1_Memo.ScrollBArs:=ssVertical;
    AP1_Memo.ReadOnly:=True; 
    AP1_Memo.font.Name:='Arial';
    AP1_Memo.font.Size:=8;
    AP1_Memo.Ctl3D:=False;
    AP1_Memo.Color:=ClBtnFace;
    
    // message(Chr(13)+Chr(10)+'PROCEDURE DRAW_AP1 FINISHED!'+Chr(13)+Chr(10));

end;

Procedure draw_AP2;    // SIZE
//var

begin
   message('PROCEDURE DRAW_AP2 STARTED!');

   if FileExists(script_path+'201_width.jpg') then message('Picture 201 Exist ')
										  else message('Picture 201 Not Exist: '+script_path+'201_width.jpg');		

   RC_Image_edit_create (
                        AP[2],
                        script_path+'201_width.jpg',
                        20,              
						35,               
						128,       
						160,      
						201,         
						1,          
						txt_width+':',
						IntToStr(el_width_min)+' - '+IntToStr(el_width_max), 
						'',
						'',
						'',
						'',
						'mm'
                        );	   
   

   edit_name:=RC_Edit(txt_name+':', '', AP[2], 310, 35, 'top-left');
   edit_name.width:=150;
   edit_name.tag:=204;  
   
   RC_Image_edit_create (
                        AP[2],
                        script_path+'202_height.jpg',
                        20,              
						175,               
						128,       
						160,      
						202,         
						1,          
						txt_height+':',
						IntToStr(el_height_min)+' - '+IntToStr(el_height_max), 
						'',
						'',
						'',
						'',
						'mm'
                        );	   
						
	 RC_Image_edit_create (
                        AP[2],
                        script_path+'203_depth.jpg',
                        20,              
						315,               
						128,       
						160,      
						203,         
						1,          
						txt_depth+':',
						IntToStr(el_depth_min)+' - '+IntToStr(el_depth_max), 
						'',
						'',
						'',
						'',
						'mm'
                        );	   

end;   

Procedure draw_AP3;   // TOP
// var

begin
    // message(Chr(13)+Chr(10)+'PROCEDURE DRAW_AP3 STARTED!'+Chr(13)+Chr(10));
    //showmessage('draw AP1 started');
    // AP[3].height:=AP_Height[3]; 

    // select top type
     RC_Radio_image_Create (
    // RI_shelves:=RC_Radio_image (
                  {parent}             AP[3], 
                  {caption}            txt_select_top_type,
                  {start_tag}          301,
                  {quantity}           3,
                  {default}            2,
                  {left}               30,
                  {top}                35,
                  {width}              460,
                  {image_width}        256,
                  {image_height}       117,
                  {image_names}        'top',                // tag+on/of+'shape'+'.jpg'
                  {image_descriptions} txt_top_descriptions,
                  {script_path}        script_path
                  );     


    // adjust top thickness
    RC_Image_edit_create (
                        AP[3],
                        script_path+'top_thickness.jpg',
                        30,              
						510,
						128,
						64,
						304,
						1,
						txt_top_thickness+':',
						IntToStr(top_thick_min)+' - '+IntToStr(top_thick_max),  
						' ',
						' ',
						' ',
						' ',
						'mm'
                        );	   

   // top width

   // RC_Image_edit_create (
   //                     AP[3],
   //                    script_path+'top_width.jpg',
   //                     30,              
   //               	594,               
	//			128,       
    //              	128,      
	//		305,         
    //              	2,          
	//		txt_back_top_width+':',
	//		IntToStr(top_width_back_min)+' - '+IntToStr(top_width_back_max), 
	//		txt_front_top_width+':',
	//		IntToStr(top_width_front_min)+' - '+IntToStr(top_width_front_max),
	//		' ',
	//		' ',
	//		'mm'
     //                   );
 
   // top distance
   RC_Image_edit_create (
                        AP[3],
                        script_path+'top_distance.jpg',
                        30,              
						594,               
						128,       
						128,      
						305,         
						2,          
						txt_back_top_distance+':',
						IntToStr(top_dist_back_min)+' - '+IntToStr(top_dist_back_max), 
						txt_front_top_distance+':',
						IntToStr(top_dist_front_min)+' - '+IntToStr(top_dist_front_max),
						' ',
						' ',
						'mm'
                        );
   
    // message(Chr(13)+Chr(10)+'PROCEDURE DRAW_AP3 FINISHED!'+Chr(13)+Chr(10));

end;

Procedure draw_AP4;   // BOTTOM
//var

begin
    // message(Chr(13)+Chr(10)+'PROCEDURE DRAW_AP4 STARTED!'+Chr(13)+Chr(10));
    //showmessage('draw AP1 started');

    // select bottom type
     RC_Radio_image_Create (
                  {parent}             AP[4], 
                  {caption}            txt_select_bottom_type,
                  {start_tag}          401,
                  {quantity}           3,
                  {default}            2,
                  {left}               30,
                  {top}                40,
                  {width}              460,
                  {image_width}        256,
                  {image_height}       117,
                  {image_names}        'bottom_type',                // tag+on/of+'shape'+'.jpg'
                  {image_descriptions} txt_bottom_type_descriptions,
                  {script_path}        script_path
                  );     

    // draw bottom thickness
     RC_Image_edit_create (
                        AP[4],
                        script_path+'bottom_thickness.jpg',
                        30,              
                  	515,               
			128,       
                  	82,      
			404,         
                  	1,          
			txt_bottom_thickness+':',
			IntToStr(bottom_thick_min)+' - '+IntToStr(bottom_thick_max), 
			' ',
			' ',
			' ',
			' ',
			'mm'
                        );
                        
     // bottom distances
     RC_Image_edit_create (
                        AP[4],
                        script_path+'bottom_distance.jpg',
                        30,              
                  	610,               
			128,       
                  	128,      
			405,         
                  	2,          
			'1. '+txt_front_bottom_distance+':',
			IntToStr(bottom_dist_front_min)+' - '+IntToStr(bottom_dist_front_max),  
			'2. '+txt_back_bottom_distance+':',
			IntToStr(bottom_dist_back_min)+' - '+IntToStr(bottom_dist_back_max),
			' ',
			' ',
			'mm'
                        );
                        
    // message(Chr(13)+Chr(10)+'PROCEDURE DRAW_AP4 FINISHED!'+Chr(13)+Chr(10)); 
end;
    
Procedure draw_AP5;   // Back

// var

begin

    //showmessage('draw AP1 started');
     

    // back exist
     RC_Radio_image_Create (
                  {parent}             AP[5], 
                  {caption}            txt_back_presence,
                  {start_tag}          501,
                  {quantity}           2,
                  {default}            1,
                  {left}               30,
                  {top}                40,
                  {width}              460,
                  {image_width}        128,
                  {image_height}       128,
                  {image_names}        'back',                // tag+on/of+'shape'+'.jpg'
                  {image_descriptions} txt_back_exist,
                  {script_path}        script_path
                  );     
      
     RC_Image_edit_create (
                        AP[5],
                        script_path+'back_distance.jpg',
                        30,              
                  	240,               
			128,       
                  	128,      
			503,         
                  	1,          
			txt_back_distance+':',
			IntToStr(back_dist_min)+' - '+IntToStr(back_dist_max), 
			' ',
			' ',
			' ',
			' ',
			'mm'
                        );
    
     RC_Image_edit_create (
                        AP[5],
                        script_path+'back_thickness.jpg',
                        30,              
                  	380,               
			128,       
                  	128,      
			504,         
                  	1,          
			txt_back_thickness+':',
			IntToStr(back_thick_min)+' - '+IntToStr(back_thick_max), 
			' ',
			' ',
			' ',
			' ',
			'mm'
                        );
 
     RC_Image_edit_create (
                        AP[5],
                        script_path+'back_insert.jpg',
                        30,              
                  	520,               
			128,       
                  	128,      
			505,         
                  	1,          
			txt_back_insert+':',
			IntToStr(back_ins_min)+' - '+IntToStr(back_ins_max), 
			' ',
			' ',
			' ',
			' ',
			'mm'
                        );
   
    // RC_Image_edit_create (
    //                    AP[5],
    //                    script_path+'back_width.jpg',
    //                    30,              
    //              	660,               
	//		128,       
    //              	128,      
	//		506,         
    //              	2,          
	//		'1. '+txt_back_height_1+':',
	//		IntToStr(back_top_width_min)+' - '+IntToStr(back_top_width_max), 
	//		'2. '+txt_back_height_2+':',
	//		IntToStr(back_down_width_min)+' - '+IntToStr(back_down_width_max),
	//		' ',
	//		' ',
	//		'mm'
    //                    );
 
 // message(Chr(13)+Chr(10)+'PROCEDURE DRAW_AP5 (BACK) FINISHED!'+Chr(13)+Chr(10));

end;



Procedure draw_AP6;      // sides
// var

begin
    // message(Chr(13)+Chr(10)+'PROCEDURE DRAW_AP6 (SIDES) STARTED!'+Chr(13)+Chr(10));

     RC_Image_edit_create (
                          {parent}              AP[6],
                          {image_path}          script_path+'side_thickness.jpg',
                          {left}                30,              
                          {top}     	        50,               
	                  {image_width}		128,       
                          {image_height}    	128,      
	                  {start_tag}		601,         
                          {number_of_tags}      1,          
			  {tag_1_label}         txt_side_thickess+':',
			  {tag_1_hint}          IntToStr(side_thick_min)+' - '+IntToStr(side_thick_max), 
			  {tag_2_label}         ' ',
			  {tag_2_hint}          ' ',
			  {tag_3_label}         ' ',
			  {tag_4_hint}          ' ',
			  {unit}                'mm'
                        );

    // message(Chr(13)+Chr(10)+'PROCEDURE DRAW_AP6 (SIDES) FINISHED!'+Chr(13)+Chr(10));

end;

// shelves
Procedure Draw_AP7;  
Begin
     // Showmessage('Shelf section AP7 create started');
     RC_Radio_image_Create (
                  {parent}             AP[7], 
                  {caption}            txt_shelves_quantity,
                  {start_tag}          701,
                  {quantity}           3,
                  {default}            2,
                  {left}               30,
                  {top}                40,
                  {width}              460,
                  {image_width}        128,
                  {image_height}       128,
                  {image_names}        'shelf',                // tag+on/of+'shape'+'.jpg'
                  {image_descriptions} txt_shelves_description,
                  {script_path}        script_path
                  );     

     RC_Image_edit_create (
                        AP[7],
                        script_path+'705_shelf_thickness.jpg',
                        30,              
                  	225,               
			128,       
                  	128,      
			705,         
                  	1,          
			txt_shelf_thickness+':',
			IntToStr(shelf_thick_min)+' - '+IntToStr(shelf_thick_max), 
			' ',
			' ',
			' ',
			' ',
			'mm'
                        );

     RC_Image_edit_create (
                          {parent}              AP[7],
                          {image_path}          script_path+'706_shelf_front_distance.jpg',
                          {left}                30,              
                          {top}     	        365,               
	                  {image_width}		128,       
                          {image_height}    	128,      
	                  {start_tag}		706,         
                          {number_of_tags}      1,          
			  {tag_1_label}         txt_shelf_front_dist+':',
			  {tag_1_hint}          IntToStr(shelf_front_dist_min)+' - '+IntToStr(shelf_front_dist_max), 
			  {tag_2_label}         ' ',
			  {tag_2_hint}          ' ',
			  {tag_3_label}         ' ',
			  {tag_4_hint}          ' ',
			  {unit}                'mm'
                        );

     RC_Radio_image_Create (
                  {parent}             AP[7], 
                  {caption}            txt_shelf_adjusting,
                  {start_tag}          707,
                  {quantity}           2,
                  {default}            1,
                  {left}               30,
                  {top}                515,
                  {width}              460,
                  {image_width}        128,
                  {image_height}       128,
                  {image_names}        'shelf_movable',                // tag+on/of+'name'+'.jpg'
                  {image_descriptions} txt_shelf_movable_description,
                  {script_path}        script_path
                  );     

    
End;

   

Procedure draw_AP8;      // legs
// var
  
Begin
     RC_Radio_image_Create (
                  {parent}             AP[8], 
                  {caption}            txt_legs_height,
                  {start_tag}          801,
                  {quantity}           2,
                  {default}            1,
                  {left}               30,
                  {top}                50,
                  {width}              460,
                  {image_width}        128,
                  {image_height}       128,
                  {image_names}        'leg_height',                // tag+on/of+'name'+'.jpg'
                  {image_descriptions} txt_legs_height_description,
                  {script_path}        script_path
                  );      
End;


Procedure draw_AP9;   //  doors 
// var
  
Begin
     RC_Radio_image_Create (
                  {parent}             AP[9], 
                  {caption}            txt_door_qnt,
                  {start_tag}          901,
                  {quantity}           4,
                  {default}            1,
                  {left}               30,
                  {top}                50,
                  {width}              460,
                  {image_width}        128,
                  {image_height}       128,
                  {image_names}        'doors_qnt',                // tag+on/of+'name'+'.jpg'
                  {image_descriptions} txt_door_qnt_description,
                  {script_path}        script_path
                  );      
End;


Procedure Create_window;
// Create window and all visual object

Begin
   // set heights of adjusment panels
   AP_Height[1]:=0;             // shape
   AP_Height[2]:=455;             // size
   AP_Height[3]:=740;             // top
   AP_Height[4]:=755;             // bottom
   AP_Height[5]:=670;             // back
   AP_Height[6]:=200;             // side
   AP_Height[7]:=705;             // shelves
   AP_Height[8]:=0;             // legs
   AP_Height[9]:=390;             // doors

   // create main form
   W_Window:=Window('',1000,800);
   W_Window.caption:='RC';
   W_Window.Width:=1000;
   W_Window.Height:=700;
   
   draw_headline;

   Draw_Debug_Section;         // create debug section
   Draw_Main_buttons;          // create main buttons (ok, cancel, about, help)
   Draw_Adjustment_panels;     // panels width objects for changing element variables
   
   Draw_Presets;               // create presets list and buttons for manage preset list
   
   Progress_create(W_Window);
End;

