﻿Program Nekakav_makro;   
// Red Cat Ltd 
// v1.0 30.05.2017.

// - 
// - 
// - 

// 
{$I ..\..\..\RC_Tools\Include\rcPrint.cps}
{$I ..\..\..\RC_Tools\Include\rcMessages.cps}
{$I ..\..\..\RC_Tools\Include\rcStrings.cps}
{$I ..\..\..\RC_Tools\Include\rcControls.cps}
{$I ..\..\..\RC_Tools\Include\rcObjects.cps}
{$I ..\..\..\RC_Tools\Include\rcEvents.cps}
{$I ..\..\..\RC_Tools\Include\rcCurves.cps}
{$I ..\..\..\RC_Tools\Include\rcMath.cps}
{$I ..\..\..\RC_Tools\Include\rcConfig.cps}
    

var
   // Startni prozor
	Main:TForm;
   Panel_Left, Panel_right, Panel_Down, Panel_Title, Panel_Info, Panel_Table, Panel_Memo, Panel_MemoO :TPanel;
   
	// Pan_var, Pan_val, Pan_for, Pan_cb, Pan_hint : array of TPanel;
   ButOK,ButPomoc,ButCancel:TButton;
	
	
	// testA: array [1..10 ][1..10] of integer;
	// testA: array [1..10, 1..10] of integer;
	
	MPanel: Array [1..4] of TPanel;
	CB_Makro_on: Array [1..4] of TCheckBox;    // 4 cb-a, za svaki panel/makro po jedan
	
	Naziv: Array [1..4] of array [0..101] of TLabel;
	Polje: Array [1..4] of array [0..101] of TEdit;
	Potrosni_1:Array [1..4] of  TEdit;
	Potrosni_2:Array [1..4] of  TEdit;
	
	
	
	
	makro_file, makro :string;		
	// makro_obj1 :string;		
	panel_name:TPanel;
	sirpan:Integer;
	
	
	
	Slika1, Slika2, Slika3, Slika4 :TMImage;
	Memo1, Memo2, Memo3, Memo4 :TMemo;
	temp_str:string;

	Debug, MozeZatvaranje, Izvrseno :boolean;    // za debug poruke u Journalu

	
	i,Main_start_Width, Main_start_height :Integer;

	daska_1, daska_2 : TDaska;


Procedure write_ini(dato:string);
var
	lista:TStringList;
	i:integer;
Begin
	dato:=prgFolder+Copy(dato,4,200)+'set.ini';
	if not rcFileCheck(dato) then begin
												lista:=TStringList.Create;
												lista.SaveToFile(dato);
											end;
	//function set_value_int(FileName,ValueName :string; Value:Integer):boolean;
	set_value_int(dato,'Left',Main.Left)
	set_value_int(dato,'Top',Main.Top);
	set_value_int(dato,'Width',Main.Width);
	set_value_int(dato,'Height',Main.Height);
	
	for i:=1 to 4 do begin
		If CB_Makro_on[i].checked 	then set_value_int(dato,'Makro'+IntToStr(i)+'_ON',1)
											else set_value_int(dato,'Makro'+IntToStr(i)+'_ON',0)
		
	end;
	
	
	
	
	
	
									
End;	
		
	
Procedure read_ini(dato:string);
var
	i:integer;
Begin
	dato:=prgFolder+Copy(dato,4,200)+'set.ini';
	// function find_value_int(FileName,ValueName:string):integer;
	// ako ne postoji vraća vrijednost 0
	i:=find_value_int(dato,'Left'); If i=0 then Main.Left:=10 Else Main.Left:=i;
	i:=find_value_int(dato,'Top');  If i=0 then Main.Top:=20 Else Main.Top:=i;
	i:=find_value_int(dato,'Width');If not i=0 then Main.Width:=i;
	i:=find_value_int(dato,'Height');If not i=0 then Main.Height:=i;
	
	// ako je spremljeno na računalu s dodatnim monitorom kojeg više nema
	if Main.Left+Main.Width<50 then Main.Left:=10;											// ako je prozor na nepostojećem monitoru lijevo
	if Main.Left>ScreenWidth-50 then Main.Left:=ScreenWidth-400;						// ako je prozor na nepostojećem monitoru desno
	if Main.Top<20 then Main.Top:=20;															// ako je prozor na nepostojećem monitoru gore
	if Main.Top>ScreenHeight-100 then Main.Top:=ScreenHeight-Main.Height;				// ako je prozor na nepostojećem monitoru dolje
	
	// ShowMessage('1');
	for i:=1 to 4 do begin
		If find_value_int(dato,'Makro'+IntToStr(i)+'_ON')=1 then CB_Makro_on[i].checked:=true
																			 else CB_Makro_on[i].checked:=false;
	end;
	
End;	
	
Procedure spoji;
var
	m1,m2:string;
Begin
	For i:=1 to 4 do begin
		m1:=makro_file+IntToStr(i)+'a.CMK';
		m2:=makro_file+IntToStr(i)+'b.CMK';
		if CB_Makro_ON[i].checked then begin
			ShowN('Sad ću spojiti:'+#13#10+m1 +#13#10+m2);
			e.MakeSpoj(daska_1,daska_2,m1,m2);
			
			e.recalcformula(e);
			ShowN('Spojio');
		end;
	End;				
End;	
	
Procedure Kraj_skripte;
Begin
	// Main.Hide;     
   // Main.ShowModal;   
   ShowN('ZATVARANJE PROGRAMA');
	
	Main.close;
	
   // e.RecalcFormula(e); 
	// Main.free; 
End;
	
Procedure write_macro(m:integer);

var
	lista:TStringList;
	base_file, macro_file, redak :String;
	i:integer;
	
Begin
	ShowN('Proc START: write_makro, M='+IntToStr(m));
	base_file:=makro_file+IntToStr(m);
	macro_file:=prgFolder+Copy(base_file,4,200)+'a.CMK';
	// učitaj makro
	lista:=TStringList.Create;
	lista.Clear;
	if rcFileCheck(macro_file) then lista.LoadFromFile(macro_file)
										else ShowMessage ('Procedura: Write_Macro' +#13#10+
																'Nedostaje datoteka:'+#13#10+
																macro_file);
	
	// zapiši nove stvari ako je označen check box
	if CB_Makro_ON[m].checked then begin
		// naziv makroa
		set_value_str(macro_file,'GN', Polje[m][0].text)
		
			
		
		// varijable
		For i:=1 to 100 do begin
			// novi način
			if not (Contain(lista[i],'=')) then begin
				
				ShowN('Izlazim zbog toga što ovaj redak nema znaka =')
				Break;
			end;
		
			{
			// stari način
			if Polje[m][i].text='' then begin 
					ShowD('Nema više varijabli u makrou '+IntToStr(m)+'!')
					Break;
			end; // if not (Contain(redak,'='))
			}
			ShowN('Sad ću u '+Naziv[m][i].caption+' zapisati '+ Polje[m][i].text );
			set_value_str(macro_file,Naziv[m][i].caption,Polje[m][i].text);
			ShowN('Jesam');
			
		end;
		
		// postavljanje potrosnog
		If (Potrosni_1[m].text='') or (Potrosni_1[m].text=' ') then Potrosni_1[m].text:='*nema*';
		If (Potrosni_2[m].text='') or (Potrosni_2[m].text=' ') then Potrosni_2[m].text:='*nema*';
		set_value_str(macro_file,'PS1',Potrosni_1[m].text);
		set_value_str(macro_file,'PS2',Potrosni_2[m].text);
	
		
		
	End;
	
	
	
	ShowN('Proc END: write_makro, M='+IntToStr(m));
End;	
	
	
Procedure Napravi_posao;
var
	makro_RB : integer;
	i,j,k:integer;
Begin
	ShowN('*************************');
	ShowN('Ovdje se izvršava program');
	ShowN('*************************');
	// ShowMessage('POSAO');
	
	// Obnovi makroe na disku
	
	for makro_RB:=1 to 4 do begin
		If CB_Makro_on[Makro_RB].checked then begin
			write_macro(Makro_RB);
			// execute_macro(Makro_RB);
		end;
	end; // for makro_RB:=1 to 4
	
	Spoji;
	write_ini(makro_file);
	
	// end program
	
End;

Procedure Pomoc;
var
	PDF:TpdfPrinter;
Begin
	ShowD('Pomoć');
	PDF:=TpdfPrinter.create(1250);
	PDF.FileName := 'https:\\www.redcat.hr/h2/spojevi_2';
	PDF.ShowPdf;
	ShowD('PDF.ShowPdf - DONE');
	PDF.Free;
End;

Procedure create_panel(i:Integer);
var
	j:integer;
Begin
	// Panel Raster 1
	
		// Showmessage('i:'+IntToStr(i));
		// Showmessage('Sad ću kreirati '+intToStr(i)+'. Panel.');
		MPanel[i]:=rcPanel( 'panel'+IntToStr(i),Panel_Left ); 
		if i=1 	then MPanel[i].Left:=0
					else MPanel[i].Left:=MPanel[i-1].Left+MPanel[i-1].Width+10;
		MPanel[i].ALign:=alLeft; 
		MPanel[i].Width:=sirpan;      
		MPanel[i].BorderWidth:=5;
		// Showmessage(intToStr(i)+'. Panel gotov');
	
	// Showmessage('Create panel gotov');
End;

Procedure Call_makro (RB:integer; Makro_obj:string);
// poziv makroe i stavlja ih u panele;
// RB je redni broj panela

var
	i,j,k:integer;
	vrijednost,znakic:string;
	temp_str, temp_str2, redak: string;
	naslov:String;
	Ime_M:TEdit;
	panel_dest:TPanel;
	Lista: TStringList;
	Makro_obj2:String;
Begin
	ShowN('Proc START: call_makro, RB='+IntToStr(rb));
	panel_dest:=MPanel[RB];
	
	temp_str:=prgFolder+Copy(Makro_obj,4,200)+'a.CMK';
	temp_str2:=prgFolder+Copy(Makro_obj,4,200)+'b.CMK';
	if not (rcFileCheck(temp_str)) then 
		begin
			ShowMessage ('Nema datoteke:' +#13#10+temp_str+'"'+#13#10+#13#10
							+'Koja je dodijeljena makrou 1.');
		end else begin
			Makro_obj:=temp_str;
			// ShowMessage ('Ima datoteke: "'+temp_str+'"');
		end;
	
	// CheckBox
	CB_Makro_on[RB]:=rcCheckBox('cbr'+IntToStr(RB),'Postavi ovaj spoj?',Panel_dest,3,2);
	CB_Makro_on[RB].hint:='Uključi ako želiš da se ovaj spoj primijeni na selektirane daske.'+#13#10+#13#10+
								 'VAŽNO!'+#13#10+ 
								 'Isključivanjem	 ove opcije se neće izbrisati postojeći spoj iz elementa!';
	CB_Makro_on[RB].width:=200;
	CB_Makro_on[RB].alignment:=taRightJustify;
	CB_Makro_on[RB].font.style:=[fsBold];
	
		
	
	/// function find_value_str(FileName,ValueName:string):string;
	// NAZIV MAKROA
	naziv[RB][0]:=rcLabel( 'n'+IntToStr(RB)+IntToStr(i), 'NAZIV',Panel_dest, 50,28,taRightJustify);  // taCenter ne radi
	naziv[RB][0].font.size:=9;

	// čitanje naziva iz datoteke makroa   (GN polje)
	naslov:=find_value_str(temp_str,'GN');   
	Polje[RB][0]:=rcEdit('',Panel_dest,naziv[RB][0].Left+naziv[RB][0].width+4,25);
	Polje[RB][0].font.size:=10;
	Polje[RB][0].Width:=Panel_dest.width-Polje[RB][0].Left-6;
	Polje[RB][0].font.style:=[fsBold];
	Polje[RB][0].text:=naslov;
	
	// učitavanje varijabli iz makroa
	Lista:=TStringList.Create;
	Lista.LoadFromFile(Makro_obj);
	
	For i:=1 to 100 do begin							
		redak:=Lista[i];						// čitanje pojedinačnih redova iz makroa
		// ShowD('redak="'+redak+'"');
		// Showmessage('i='+IntToStr(i)+' redak="'+redak+'"');
		// ShowMessage('Još uvijek radim!');
		if not (Contain(redak,'=')) then begin
			ShowD('Izlazim zbog toga što ovaj redak nema znaka =')
			Break;
		end;
		
		// Čitanje NazivR1[i] - lijevo od znaka jednakosti
		// prvo se kreira labela da imam kamo pisati
		// function rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
		naziv[RB][i]:=rcLabel( 'naziv'+IntToStr(RB)+IntToStr(i), '',Panel_dest, sirpan-55,51+(i-1)*22,taRightJustify);  // taCenter ne radi
		
		for j:=1 to Length(redak) do begin
			if redak[j]='=' then begin
				ShowD('Izlazim zbog toga što sam došao do znaka =');
				Break;
			end;
			naziv[RB][i].caption:=naziv[RB][i].caption+redak[j];
			naziv[RB][i].font.size:=9;
			ShowD('i='+IntToStr(i)+' j='+IntToStr(j));
			ShowD('redak="'+redak+'"');
		end;
		ShowD('Došao sam do znaka jednako, sad tražim vrjednost');
		// kreiranje polja jer inače sve ode kvragu
		// Function rcEdit(eName: string; eDest: TWinControl; eX, eY: integer):TEdit;
		// Showmessage('i='+IntToStr(i)+' mjesto: 4');
		Polje[RB][i]:=rcEdit('polje_'+IntToStr(RB)+IntToStr(i), Panel_dest, sirpan-52,48+(i-1)*22);
		Polje[RB][i].font.size:=9;
		Polje[RB][i].font.style:=[fsBold];	
		// čitanje vrijednosti - desno od znaka jednakosto
		Polje[RB][i].text:=Copy(redak,j+1,200);
		Polje[RB][i].width:=46;
		// Polje[RB][i].hint:='Ovo je panel '+IntToStr(RB)+' i polje '+IntToStr(i);
		Polje[RB][i].hint:=Polje[RB][i].text;
		// Showmessage('i='+IntToStr(i)+' mjesto: 5');
		// Showmessage(nazivR1[i]+' jednako '+PoljeR1[i].text);
	end;  // For i:=1
	
	
	// temp_str:=prgFolder+Copy(Makro_obj,4,200)+'a.CMK';
	
	// postavljanje potrosnog
	naziv[RB][i]:=rcLabel( 'naziv'+IntToStr(RB)+IntToStr(i), 'Potrošni 1',Panel_dest, sirpan-75,51+(i-1)*22,taRightJustify);  // taCenter ne radi
	naziv[RB][i].font.size:=9;
	Potrosni_1[RB]:=rcEdit('polje_'+IntToStr(RB)+IntToStr(i), Panel_dest, sirpan-72,48+(i-1)*22);
	Potrosni_1[RB].font.size:=9;
	Potrosni_1[RB].font.style:=[fsBold];		
	Potrosni_1[RB].width:=66;
	Potrosni_1[RB].text:=find_value_str(temp_str,'PS1');
	Potrosni_1[RB].hint:='Upiši šifru potrošnog materijala';
	If (Potrosni_1[RB].text='') or (Potrosni_1[RB].text=' ') then Potrosni_1[RB].text:='*nema*';
	
	i:=i+1;
	naziv[RB][i]:=rcLabel( 'naziv'+IntToStr(RB)+IntToStr(i), 'Potrošni 2',Panel_dest, sirpan-75,51+(i-1)*22,taRightJustify);  // taCenter ne radi
	naziv[RB][i].font.size:=9;
	Potrosni_2[RB]:=rcEdit('polje_'+IntToStr(RB)+IntToStr(i), Panel_dest, sirpan-72,48+(i-1)*22);
	Potrosni_2[RB].width:=66;
	Potrosni_2[RB].font.size:=9;
	Potrosni_2[RB].font.style:=[fsBold];
	Potrosni_2[RB].text:=find_value_str(temp_str,'PS2');	
	Potrosni_2[RB].hint:='Upiši šifru potrošnog materijala';
	If (Potrosni_2[RB].text='') or (Potrosni_2[RB].text=' ') then Potrosni_2[RB].text:='*nema*';
	i:=i+1;
	
	
		
	
	// Učitavanje slike
	// Function rcLoadImage(iName,iPath:String;iDest: TWinControl; iX, iY: integer):TMImage;
	
	temp_str:=prgFolder+Copy(Makro,4,400)+'.jpg';
	if not (rcFileCheck(temp_str)) then 
		begin
			Slika1:=TMImage.create(panel_dest);
			Slika1.parent:=panel_dest;
			Slika1.top:=48+(i-1)*22+5;
			Slika1.left:=1;
			Slika1.Height:=2;
			{
			ShowMessage ('Nema slike:' +#13#10+temp_str+'"'+#13#10+#13#10
							+'Koja je dodijeljena makrou.');
			ShowN ('Nema slike:' +#13#10+temp_str+'"'+#13#10+#13#10
							+'Koja je dodijeljena makrou.');				
			}
		end else begin
			
			Slika1:= rcLoadImage('',temp_str,panel_dest,1,48+(i-1)*22+5);
			
			// Slika1.AutoSize:=False;	  // automatska veličina podloge za sliku	
			// Slika1.Stretch :=True;   // dozvoli razvlačenje slike
			Slika1.width:=200;
			Slika1.Height:=200;
			Slika1.Left:=round(Panel_dest.Width/2-Slika1.Width/2);
		end;
	
	// Učitavanje teksta 
	// function rcMemo(mName:string; mParent: TWinControl; mX, mY, mW, mH: integer):TMemo;
	Memo1:=rcMemo('',Panel_dest,Slika1.Left,Slika1.Top+Slika1.Height+5,200,100);
	Memo1.Height:=MPanel[RB].Height-Memo1.Top-5;
	temp_str:=prgFolder+Copy(Makro,4,400)+'.txt';
	Memo1.Lines.LoadFromFile(temp_str);
	Memo1.Color:=clBTNFace;Memo1.Font.Size:=8;MemoJ.Ctl3D:=False;Memo1.Font.Color:=clMaroon;
	
	// drugi makro
	// temp_str2 je dodijeljen na vrhu procedure
	if not (rcFileCheck(temp_str2)) then 
		begin
			ShowMessage ('Nema datoteke:' +#13#10+temp_str+'"'+#13#10+#13#10
							+'Koja je dodijeljena makrou 2.');
		end else begin
			Makro_obj2:=temp_str2;
			// ShowMessage ('Ima datoteke: "'+temp_str+'"');
		end;
	ShowN('Proc END: call_makro, RB='+IntToStr(rb));

End;

// ------------------------------------------------	
{$I Skripta_servis.cps}
// ------------------------------------------------


Procedure Test_selektirano;
var
	i:integer;
Begin 
	//function el_find_sel_plates:TStringList;
	if el_find_sel_plates.count<>2 then begin
		Showmessage ('Moraju biti selektirane dvije daske!' );
		// Kraj_skripte;   // Ne radi :-(
		ButOK.Caption:='Trebam 2 daske!';
		ButOK.Enabled:=False;

	end;	
end;

{$I test.cps}
{
// Out 2******************************************
Procedure Test_dasaka;
var
	i, broj_fronti, Broj_vertikala:integer;
	ss:string;
	td:Tdaska;
Begin
	// brojanje količine
	broj_fronti:=0;
	Broj_vertikala:=0;
	for i:=0 to e.childdaska.CountObj-1 do Begin  // broji daske u editoru
		td:=e.childdaska.Daska[i];   
		// tražim frontu
		if td.selected and (td.smjer in [VertFront]) then begin
			broj_fronti:=broj_fronti+1;
			daska_1:=td;
			// ShowMessage('Našao sam frontalnu broj: '+IntToStr(broj_fronti));
		end; // if td.selected
		// tražim vertikalu
		if td.selected and (td.smjer in [VertBok]) then begin
			broj_vertikala:=broj_vertikala+1;
			// ShowMessage('Našao sam vertikalu broj: '+IntToStr(broj_vertikala));
			daska_2:=td;
		end; //if td.selected
	end;  // for i:=0 to e.childdaska.CountObj-1 


	// provjera količine
	If (broj_fronti=1) and (Broj_vertikala=1) Then Begin
		// provjera kuteva
		If (Daska_1.kut<>0) or (Daska_1.xkut<>0) or (Daska_1.zkut<>0) or 
			(Daska_2.kut<>0) or (Daska_2.xkut<>0) or (Daska_2.zkut<>0) Then Begin 
					Showmessage('Neke selektirane daska su rotirane,' +#13#10+
									'a ne smije biti za ovaj spoj.' );
					ButOK.Enabled:=False;			 
			End else begin
				// kutevi su ok
				// provjera dodira
				If (daska_1.ZPos<>daska_2.ZPos) or										// 2 nije točno iza 1
					(daska_2.xPos+daska_2.debljina<daska_1.XPos) or 				// 2 je lijevo od 1
					(daska_2.xPos>daska_1.XPos+daska_1.Dubina) or   				// 2 je desno od 1
					(daska_2.yPos+daska_2.visina<daska_1.Ypos) or 					// 2 je ispod 1
					(daska_2.yPos>daska_1.ypos+daska_1.visina) 						// 2 je iznad 1
					Then begin
						Showmessage('Izgleda da se daske ne dodiruju.'+#13#10+
										'Makro se može postaviti, ali dobro provjerite što radite!');
				End;  // 	If (daska_1.ZPos<>daska_2.ZPos) 
		end;	// If (Daska_1.kut<>0) 
	end else begin  // If (broj_fronti=1) ;	
		ButOK.Enabled:=False;
		Showmessage('Moraju biti selektirane dvije daske!' +#13#10+
						'Naprijed fronta i otraga vertikala');
	end;  // If (broj_fronti=1)
	
		
End;
// Out 2******************************************
}

// Glavni   
Begin
   // Debug:=true;         // debuger ispis on/off
	Debug:=false;
	Izvrseno:=false;
	Main_start_Width:=1050;
	Main_start_Height:=650;
	
	Create_Main_Window;
	
	
	
	ShowN('skripta: NAPRAVI SPOJ v1.0');
	ShowN('01.06.2017.');
	ShowN('==============');
	
	Makro_file:='nema';
	
	// Showmessage ('Prije sorsa');
	{$I source_macro.cps}
	{
	// Out 1 **************************

	
	// Postavljanje panela
	
	makro_file:='..\skripte\Editor\Makro_02\Fronta_vertikala\Fronta_Vertikala_';
	// Out 1 **************************
	}
	
	// Showmessage ('Nakon sorsa');
	if Makro_file='nema' then Showmessage ('A gdje je source macro?');
	
	sirpan:=round(Panel_Left.width/4);
	for i:=1 to 4 do begin
		// ShowMessage('počinjem sa i='+IntToStr(i));
		Create_panel(i);
		// ShowMessage('2');
		makro:=makro_file+IntToStr(i);
		call_makro(i, makro);
		// ShowMessage('gotov sam sa i='+IntToStr(i));
	
	End;
	ShowN('Sirpan='+IntToStr(Sirpan));
	// MPanel[i].ALign:=alLeft;
	read_ini(makro_file);
		
	// Test_selektirano;
	Test_dasaka;

	
	
	// eventi
	cbDebug_OnClick(cbDebug);  // poziva "cbDebug_OnClick" bez da je išta kliknuto
	ButOK.OnClick:=@ButOK_OnClick;
	ButCancel.OnClick:=@ButCancel_OnClick;
	ButPomoc.OnClick:=@ButPomoc_OnClick;
	
	ButOK.default:=True;
	ButCancel.ModalResult:=mrCancel;
	
	
   // Naslov
   
   // zaglavlje
   
   // tablica
   
   Main.ShowModal;
	// Main.Show;
	while Main.visible do begin
        application.processmessages;
   end;
   
   Main.free; 
   
   
End.