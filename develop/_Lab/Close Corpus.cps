var
   win:TForm; BClose, BCancel:TButton;

Procedure BCloseClick(sender:TObject);
Begin
   application.MainForm.close;
End;      

Procedure BCancelClick(sender:TObject);
Begin
   win.ModalResult := 1;
End;      
         
begin

   win:=TForm.create(nil);
   win.width:=320; win.height:=100; 
   win.BorderStyle:=bsNone;
   win.position:=poScreenCenter;
   
   BClose:=TButton.create(win); BClose.parent:=win;
   BClose.caption:='Close Corpus'; BClose.left:=10; BClose.top:=10;BClose.width:=300;BClose.height:=30;
   BClose.onclick:=@BCloseclick;
   
   BCancel:=TButton.create(win); BCancel.parent:=win;
   BCancel.caption:='Cancel'; BCancel.left:=10; BCancel.top:=50; BCancel.width:=300; BCancel.height:=30;
   BCancel.onclick:=@BCancelclick;

   win.showmodal;
   // win.free;
end.