Procedure End_Program(Sender:TObject);
// ovdje se nesmije raditi s nikakvom komponentom jer je sve ugašeno
var
	dummy:boolean;
Begin
	dummy:=set_value_int(conf_path,'Lijevo',Main.left);
	dummy:=set_value_int(conf_path,'Gore', Main.Top);
	dummy:=set_value_int(conf_path,'Visina',Main.Height);
	If Debug then set_value_int(conf_path,'Debug',1)
				else set_value_int(conf_path,'Debug',0);

End;	




Begin
...

	Main.onDeactivate:=@End_program;


	Main.Show; 

	while Main.visible do begin
         application.processmessages;
   end;
   
	
   Main.free; 
	
end.

