Program refresh;   
// Red Cat Ltd 
// v1.0 07.06.2017.

// - 
// - 

{$I ..\include\rcPrint.cps}
// {$I ..\include\rcMessages.cps}
// 
{$I ..\include\rcStrings.cps}
// {$I ..\include\rcControls.cps}
{$I ..\include\rcObjects.cps}
//{$I ..\include\rcEvents.cps}
//{$I ..\include\rcProgress.cps}
//{$I ..\include\rcCurves.cps}
//{$I ..\include\rcMath.cps}
//{$I ..\include\rcConfig.cps}

    


// ------------------------------------------------	
// ------------------------------------------------

// Glavni  

Begin
	print ('Skripta refresh element start');
   rcRefreshElm(e);
	print ('Skripta refresh element end');
End.