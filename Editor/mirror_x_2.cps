program mirror_x;
// okrece dasku u x smijeru
var
   d:Tdaska;
   sel:boolean;
   i:integer;
   td:Tdaska;
   Prozor:TForm;
   Tocka: array of String;           // Dinamicki niz
   //   T1:TButton;
  
  {$I dex_unit.cps}

Procedure Pamti_vrhove(kriv:TrubneTocke);
var
   j:integer;
begin
    SetArrayLength(Tocka,kriv.count);        // duljina dinamickog niza
   
   // Odredjivanje koja je tocka vrh, a koja "pomocna"
   Writeln('Pam�enje S-va');
   
   Tocka[0]:='vrh';
   for j:=1 to kriv.count-1 do begin

      if kriv.mainTocke[j].S=kriv.mainTocke[j-1].S then Tocka[j]:='pomocna'
                                                   else Tocka[j]:='vrh'; 
      Writeln('Tocka('+IntToStr(j)+').S='+IntToStr(kriv.mainTocke[j].S)+' Tocka='+Tocka[j]);
   end;
   Writeln('provjera S-ova');
   for j:=0 to kriv.count-1 do begin
      Writeln(Tocka[j]);
   end

end;
Procedure unatrag(kriv:TrubneTocke);
// nakon rotiranja tocke krivulje idu u suprotnom smijeru
// pa treba okrenuti redoslijed tocaka
var
   dod,i,j:integer;
   pp:string;
   nkriv:TrubneTocke;
   Tocka_p: array of String; 
begin
   SetArrayLength(Tocka_p,kriv.count);        // duljina dinamickog niza
   
   //kopiranje u pomocni niz   -   S-ovi
   for j:=0 to kriv.count-1 do begin
       Tocka_p[j]:=Tocka[j];
   end;
   
   nkriv:=TrubneTocke.create(td,kriv.owner); // kreiranje nove krivulje
   nkriv.kopiraj(kriv); 
   for j:=0 to nkriv.count-1 do begin   
         kriv.mainTocke[j].kopiraj(nkriv.mainTocke[nkriv.count-1-j],true);
         Tocka[j]:=Tocka_p[kriv.count-1-j];
   end;

// ako na doljnjoj liniji ima dodatnih tocaka, novonastala prva tocka nece biti 
// na donjem lijevom cosku nego na prvoj tocki lijevo od desnog donjeg coska.
// da bi se to ispravilo niz treba pomaknuti za broj dodatnih tocaka na donjoj liniji 

// brojanje dodatnih tocaka
   dod:=0;
   for j:= 0 to kriv.count-1 do begin
      if kriv.mainTocke[j].S<6 then break;
      dod:=j;
   end;

// shiftanje dod puta
  
   for j:=1 to dod do begin
       kriv.MainList.move(0,kriv.count-1);
       pp:=Tocka[0];
       for i:=0 to kriv.count-2 do begin
           Tocka[i]:=Tocka[i+1];
       end;
       Tocka[kriv.count-1]:=pp;
   end;



end;

procedure vrati_prop_rad(kriv:TrubneTocke);
// propertije od radiusa trebaju pomaknuti za 1 gore zbog novog redoslijeda
// jer su okretanjem smijera na krivim tockama
var
   j:integer;
   nkriv:TRubneTocke;

begin
   nkriv:=TrubneTocke.create(td,kriv.owner); // kreiranje nove krivulje
   nkriv.kopiraj(kriv);

   for j:=0 to nkriv.count-1 do begin
       
         if j=(nkriv.count-1) then begin
                      kriv.mainTocke[j].AStyle:=nkriv.mainTocke[0].Astyle;
                      kriv.mainTocke[j].ArcRadius:=nkriv.mainTocke[0].ArcRadius;
                      kriv.mainTocke[j].ArcStyle:=nkriv.mainTocke[0].ArcStyle;
                      kriv.mainTocke[j].ArcCentar:=nkriv.mainTocke[0].ArcCentar;
                      
                 end
                 else begin
                      kriv.mainTocke[j].AStyle:=nkriv.mainTocke[j+1].Astyle;
                      kriv.mainTocke[j].ArcRadius:=nkriv.mainTocke[j+1].ArcRadius;
                      kriv.mainTocke[j].ArcStyle:=nkriv.mainTocke[j+1].ArcStyle;
                      kriv.mainTocke[j].ArcCentar:=nkriv.mainTocke[j+1].ArcCentar;
                 end;


   end;

end;

procedure vrati_prop_bezier(kriv:TrubneTocke);
// zamjena prethodne i slijede�e kontrolne to�ke za svaku to�ku
// jer su okretanjem smijera zamijenjene
var
   j:integer;
   nkriv:TRubneTocke;
   kontrol_priv:Tvector3f;
begin
   nkriv:=TrubneTocke.create(td,kriv.owner); // kreiranje nove krivulje
   nkriv.kopiraj(kriv);
   for j:=0 to nkriv.count-1 do begin
         // zamjena prethodne i slijede�e kontrolne to�ke
         kontrol_priv:=kriv.mainTocke[j].PrevControlPoint;
         kriv.mainTocke[j].PrevControlPoint:=kriv.mainTocke[j].NextControlPoint;
         kriv.mainTocke[j].NextControlPoint:=kontrol_priv;
   end;

end;


procedure vrati_rubne_trake(kriv:TrubneTocke);
// S-propertije treba okrenuti
// jer su okretanjem smijera "okrenuti" prema krivoj strani
var
   j, prva, ss:integer;

begin
  
    
   prva:=kriv.count-1;
   // Postavljanje S-ova
   SS:=6;
   For j:=0 to kriv.count-1 do begin
      if Tocka[j]='vrh' then ss:=ss+2;
      If ss>6 then ss:=0;
      kriv.mainTocke[j].S:=SS;
      
   end;
 
end;

//------------------------------------------------------
//------------------------------------------------------


Procedure Mirror_formula_x(kriv:TrubneTocke);
var
   i,j,duz_str:integer;
   ff,fpoc,fkraj:string;
begin
   
   for j:=0 to kriv.count-1 do begin
       ff:=kriv.mainTocke[j].YF;
       
       if ff <> '' then begin
          duz_str:=Length(ff);
          fkraj:=ff[duz_str];
          fpoc:='';
          if duz_str>=3 then begin
             for i:=1 to 3 do begin
                fpoc:=fpoc+ff[i];
             end;
          end
          writeln(ff);
         
          if (fpoc='L-(') or (fpoc='l-(') then kriv.mainTocke[j].YF:=copy(ff,4,duz_str-4)
                                          else kriv.mainTocke[j].YF:='L-('+ff+')';
                              
       end;
   end;
end;
   
procedure S_print(kriv:TrubneTocke);
// ispis s-ova (za rubne trake
var
   j:integer;
   SSS,TTT,XXX,YYY:string;
   
begin
   for j:=0 to kriv.count-1 do begin
      TTT:=inttostr(j);                                 // index to�ke
      SSS:=IntToStr(kriv.mainTocke[j].S);               // S-ovi
      XXX:=IntToStr(Round(kriv.mainTocke[j].x*1000));   // X
      YYY:=IntToStr(Round(kriv.mainTocke[j].z*1000));   // Y
      writeln ('Tocka('+TTT+') S='+SSS+', x='+XXX+', Y='+YYY);
   end;  

end;

Procedure mirror_krivulje_x(kriv:TRubneTocke);

begin
   writeln ('======================================================');
   writeln ('S-ovi na pocetku');
   S_print(kriv);                   // ispis S-ova za rubne trake

   // CENTAR ROTACIJE NA SREDINU DASKE!!!
   kriv.CentarMode:=TCFix;
   Pamti_vrhove(kriv);
   kriv.Rotate(1,180);              // rotacija daske
   
   writeln ('S-ovi nakon rotacije');
   S_print(kriv);                 // ispis S-ova za rubne trake
   
   unatrag(kriv);                   // vra�a prvobitni redoslijed tocaka
                                    // da nebi bilo iscrtano naopako
                                    
   writeln ('S-ovi nakon "Unatrag"');
   S_print(kriv);                   // ispis S-ova za rubne trake
                                       
   vrati_prop_rad(kriv);            // vra�a propertije za novi redoslijed
                                    // vrsta krivulje, radijus i centar luka (kruznice)
   vrati_prop_bezier(kriv);         // zamijenjuje prethodnu i sljede�u kontrolnu tocku 
                                   
   vrati_rubne_trake(kriv);         // vraca S-ove za rubne trake 
   
   mirror_formula_x(kriv);          // 

   
   kriv.BildOutList;                // ponovo racuna i crta krivulje
      
   writeln ('S-ovi nakon obrade rubnih traka');
   S_print(kriv);                 // ispis S-ova za rubne trake
end;



procedure obrada_daske;
// daska ima vanjsku krivulju (ndaska.TockeiRupe.Rubne)
// i unutarnje krivulje (td.TockeIRupe.Rupa[i]).

var
   j:integer;
   kriv:TRubneTocke;
begin

   
   kriv:=td.TockeiRupe.Rubne;               //vanjska krivulja
   mirror_krivulje_x(kriv);

   for j:=0 to td.TockeIRupe.Count-1 do Begin  // = broj "ostalih" krivulja (osim vanjske) u elementu
     kriv:=td.TockeIRupe.Rupa[j];              // = jedna krivulja u dasci (s rednim brojem i) 
     mirror_krivulje_x(kriv);
     //writeln('Miror unutarnje krivulje broj '+inttostr(j)+ ' je napravljen');

   end;

end;



//================GLAVNI PRODGRAM=================

begin

  sel:=false;
  for i:=0 to e.childdaska.CountObj-1 do Begin     //el.childdaska.CountObj = broj dasaka u lementu
     td:=e.childdaska.Daska[i];                    //jedna daska iz elementa (s rednim brojem i)
     If td.selected then begin 
                         obrada_daske;
                         sel:=true;
     end;
  end;  
  td.recalcformula;
  e.recalcformula(e);     // ponovo iscrtava - ne treba klik na ekran
 
  if not sel then ShowMessage('Za okretanje po X - osi treba biti odabrana' + + #13#10 + 'barem jedna daska s krivuljom!');

end.
