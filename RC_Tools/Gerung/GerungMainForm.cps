// design
// ===============================================================================
// Osnovni dio forme - uvijek isti
   Main:=rcTool('Corpus_RC'); 
	// Main.Width:=800;
   // Main.Height:=430;
   // Main.position:=poMainFormCenter;
   // desni panel
   Panel_Right:= rcPanel ( '' ,Main ); Panel_Right.ALign:=alRight; 
   Panel_Right.Width:=150;       // Sirina desnog panela!!!!
   Panel_Right.BorderWidth:=5;
   // lijevi panel
   Panel_Left:= rcPanel ( '' ,Main ); Panel_Left.ALign:=alClient;
   // tipke 
   ButOK:=rcButton('bOK','OK',Panel_Right,1,1);ButOK.Align:=alTop;
   ButOK.Default:=True;
   rcSpaceT(Panel_Right,2);
   ButTest:=rcButton('bTest','Test',Panel_Right,3,3);ButTest.Align:=alTop;
   ButTest.visible:=False;
   rcSpaceT(Panel_Right,4);
   ButCancel:=rcButton('bCancel','O�isti i zavr�i',Panel_Right,3,20);ButCancel.Align:=alBottom;
	ButCancel.Hint:='Napravi obi�nu dasku bez gerunga i zatvori skriptu.';
   rcSpaceB(Panel_Right,19);
   // MemoJ
   Panel_Memo:=rcPanel ( '' ,Panel_Right ); Panel_memo.Top:=18;Panel_Memo.ALign:=alClient; 
   cbDebug:=rcCheckBox('cbDB','Debug',Panel_Memo, 1,20);cbDebug.Align:=alBottom; 
   cbDebug.Font.Size:=7; cbDebug.visible:=False;
   If debug then cbDebug.checked:=True;
   MemoJ:=rcMemo('Memo_debug', Panel_Memo, 1,18,1,1);MemoJ.Align:=alClient;
   MemoJ.Color:=clBTNFace;MemoJ.Font.Size:=8;MemoJ.Ctl3D:=False;MemoJ.Font.Color:=clMaroon;
   
   // Main.show;    // prikazi formu da se vidi crtanje ostalih componenti
   // Main.FormStyle:=fsStayOnTop;
      
// ===============================================================================
	// Dio koji je specifican za skriptu
   Panel_Title:=rcPanel ( '' ,Panel_Left ); 
	Panel_Title.ALign:=alTop;
   Panel_Title.Height:=30;
      LabTemp:=rcLabel( '', 'GERUNG  ',Panel_Title, 10,7,taLeftJustify);  // taCenter ne radi
		LabTemp.Font.Size:=11; LabTemp.Font.Style:=[fsBold];
		// LabTemp.Left:=round(Panel_Title.ClientWidth/2) - round(LabTemp.width/2);
		// LabTemp.alignment:=taCenter;
      // ShowD('Panel_title je postavljen');
	// Panel za stranu 0	
	Panel_Side0:=rcPanel ( '' ,Panel_Left ); 
	Panel_Side0.ALign:=alTop;
	Panel_Side0.Height:=60;
		// panel za edit box 
		PPR0 := rcPanel('', Panel_Side0); 
		PPR0.Width:=100;
		PPR0.align:=alRight;
			// edit box
			PEdit0:=rcEdit('',PPR0,1,1);
			PEdit0.Text:='0';
			PEdit0.Font.Height:=60;
			PEdit0.align:=AlClient;
		// panel za ostatak
		PPL0 := rcPanel('', Panel_Side0); 
		PPL0.align:=alClient;
			// scrollbar
			PScroll0:=rcScroll('', PPL0, 1, 1);
			PScroll0.Height:=18;
			PScroll0.align:=AlBottom;
			// Natpis 
			PNaziv0:=rcPanel('', PPL0);
			PNaziv0.Height:=18;
			PNaziv0.Align:=alTop;
				TNaziv0:=rcLabel( '', 'Strana 0',Panel_Side0, 10,3,taLeftJustify);  // taCenter ne radi
				TNaziv0.Font.Style:=[fsBold];
				TNaziv0.Font.Size:=9;
			// Tipka -45
			Bminus0:=rcButton ('','-45',PPL0,20,20);
			Bminus0.width:=30;Bminus0.Height:=18;
			Bminus0.left:=45;
			// Tipka 0
			BNula0:=rcButton ('','0',PPL0,20,20);
			BNula0.width:=30;BNula0.Height:=18;
			BNula0.left:=92;
			// Tipka +45
			Bplus0:=rcButton ('','+45',PPL0,20,20);
			Bplus0.width:=30;Bplus0.Height:=18;
			Bplus0.left:=135;
	Razmak0:=rcPanel ( '' ,Panel_Left ); 
	Razmak0.Height:=15;Razmak0.Top:=100; Razmak0.align:=alTop;		
	
	// Panel za stranu 1	
	Panel_Side1:=rcPanel ( '' ,Panel_Left ); 
	Panel_Side1.ALign:=alTop;
	Panel_Side1.Height:=60;
		// panel za edit box 
		PPR1 := rcPanel('', Panel_Side1); 
		PPR1.Width:=100;
		PPR1.align:=alRight;
			// edit box
			PEdit1:=rcEdit('',PPR1,1,1);
			PEdit1.Text:='0';
			PEdit1.Font.Height:=60;
			PEdit1.align:=AlClient;
		// panel za ostatak
		PPL1 := rcPanel('', Panel_Side1); 
		PPL1.align:=alClient;
			// scrollbar
			PScroll1:=rcScroll('', PPL1, 1, 1);
			PScroll1.Height:=18;
			PScroll1.align:=AlBottom;
			// Natpis 
			PNaziv1:=rcPanel('', PPL1);
			PNaziv1.Height:=18;
			PNaziv1.Align:=alTop;
				TNaziv1:=rcLabel( '', 'Strana 1',Panel_Side1, 10,3,taLeftJustify);  // taCenter ne radi
				TNaziv1.Font.Style:=[fsBold];
				TNaziv1.Font.Size:=9;
			// Tipka -45
			Bminus1:=rcButton ('','-45',PPL1,20,20);
			Bminus1.width:=30;Bminus1.Height:=18;
			Bminus1.left:=45;
			// Tipka 0
			BNula1:=rcButton ('','0',PPL1,20,20);
			BNula1.width:=30;BNula1.Height:=18;
			BNula1.left:=92;
			// Tipka +45
			Bplus1:=rcButton ('','+45',PPL1,20,20);
			Bplus1.width:=30;Bplus1.Height:=18;
			Bplus1.left:=135;		
	Razmak1:=rcPanel ( '' ,Panel_Left ); 
	Razmak1.Height:=15;Razmak1.Top:=100; Razmak1.align:=alTop;		
	
	// Panel za stranu 2	
	Panel_Side2:=rcPanel ( '' ,Panel_Left ); 
	Panel_Side2.ALign:=alTop;
	Panel_Side2.Height:=60;
		// panel za edit box 
		PPR2 := rcPanel('', Panel_Side2); 
		PPR2.Width:=100;
		PPR2.align:=alRight;
			// edit box
			PEdit2:=rcEdit('',PPR2,1,1);
			PEdit2.Text:='0';
			PEdit2.Font.Height:=60;
			PEdit2.align:=AlClient;
		// panel za ostatak
		PPL2 := rcPanel('', Panel_Side2); 
		PPL2.align:=alClient;
			// scrollbar
			PScroll2:=rcScroll('', PPL2, 1, 1);
			PScroll2.Height:=18;
			PScroll2.align:=AlBottom;
			// Natpis 
			PNaziv2:=rcPanel('', PPL2);
			PNaziv2.Height:=18;
			PNaziv2.Align:=alTop;
				TNaziv2:=rcLabel( '', 'Strana 2',Panel_Side2, 10,3,taLeftJustify);  // taCenter ne radi
				TNaziv2.Font.Style:=[fsBold];
				TNaziv2.Font.Size:=9;
			// Tipka -45
			Bminus2:=rcButton ('','-45',PPL2,20,20);
			Bminus2.width:=30;Bminus2.Height:=18;
			Bminus2.left:=45;
			// Tipka 0
			BNula2:=rcButton ('','0',PPL2,20,20);
			BNula2.width:=30;BNula2.Height:=18;
			BNula2.left:=92;
			// Tipka +45
			Bplus2:=rcButton ('','+45',PPL2,20,20);
			Bplus2.width:=30;Bplus2.Height:=18;
			Bplus2.left:=135;		
	Razmak2:=rcPanel ( '' ,Panel_Left ); 
	Razmak2.Height:=15;Razmak2.Top:=100; Razmak2.align:=alTop;
			
			
	// Panel za stranu 3	
	Panel_Side3:=rcPanel ( '' ,Panel_Left ); 
	Panel_Side3.ALign:=alTop;
	Panel_Side3.Height:=60;
		// panel za edit box 
		PPR3 := rcPanel('', Panel_Side3); 
		PPR3.Width:=100;
		PPR3.align:=alRight;
			// edit box
			PEdit3:=rcEdit('',PPR3,1,1);
			PEdit3.Text:='0';
			PEdit3.Font.Height:=60;
			PEdit3.align:=AlClient;
		// panel za ostatak
		PPL3 := rcPanel('', Panel_Side3); 
		PPL3.align:=alClient;
			// scrollbar
			PScroll3:=rcScroll('', PPL3, 1, 1);
			PScroll3.Height:=18;
			PScroll3.align:=AlBottom;
			// Natpis 
			PNaziv3:=rcPanel('', PPL3);
			PNaziv3.Height:=18;
			PNaziv3.Align:=alTop;
				TNaziv3:=rcLabel( '', 'Strana 3',Panel_Side3, 10,3,taLeftJustify);  // taCenter ne radi
				TNaziv3.Font.Style:=[fsBold];
				TNaziv3.Font.Size:=9;
			// Tipka -45
			Bminus3:=rcButton ('','-45',PPL3,20,20);
			Bminus3.width:=30;Bminus3.Height:=18;
			Bminus3.left:=45;
			// Tipka 0
			BNula3:=rcButton ('','0',PPL3,20,20);
			BNula3.width:=30;BNula3.Height:=18;
			BNula3.left:=92;
			// Tipka +45
			Bplus3:=rcButton ('','+45',PPL3,20,20);
			Bplus3.width:=30;Bplus3.Height:=18;
			Bplus3.left:=135;		


	
			// ShowD('ScrollBar:'+IntToStr(PScroll0.Width));
		
		
      
		// LabTemp.Left:=round(Panel_Title.ClientWidth/2) - round(LabTemp.width/2);
		// LabTemp.alignment:=taCenter;
      // ShowD('Panel_Side0 je postavljen');	
  

