
var
	debugForm:TForm;
	debugPanel:TPanel;
	debugPanel2:TPanel;
	debugMemo:TMemo;
	ButFly, ButDock:TButton;

	
function Find_mDebug(root_object:TWinControl;ime:string):TObject;
var
   i:integer;
   xo:TObject;
begin
	for i:=root_object.ControlCount-1 downto 0 do begin
		xo:=root_object.Controls[i];
		if xo is TForm then if  TForm(xo).caption=ime Then result:=xo;
		if xo is TPanel then if TPanel(xo).hint=ime Then result:=xo;
		if xo is TMemo then if TMemo(xo).hint=ime Then result:=xo;
	end;  // for 
end;  // function


Procedure Print(poruka:string);
var
	XoD,XoT:TObject;
	dTempObject, dTempObject2, dto3, dto4, dto5 :TObject;
	i,j,SirinaForme:Integer;
Begin
	// ShowMessage ('Debug Start');
	
	
		// TRA�I SE DEBUG FORMA )ozadana u Project.toc
				
		if dTempObject<>nil then dTempObject.free;	
		
		dTempObject:=Find_mDebug(application.MainForm,'DebugForma');
		if dTempObject<>nil then begin
		
			// ShowMessage ('Na�ao sam formu koja se zove "DebugForma".');
			
			debugForm:=TForm(dTempObject);
			SirinaForme:=debugForm.width;
			// ShowMessage ('DebugForma ima �irinu: '+IntToStr(SirinaForme));
			
			// if SirinaForme>0 then begin
			
				// debugForm.borderStyle:=bsSizeToolWin; ovako se neda selektirati 
				debugForm.borderStyle:=bsNone; 			// caption=NE; rub=NE; resize=NE
	
	
				// TRA�I SE PANEL unutar DEBUG FORME
				
				if dTempObject2<>nil then dTempObject2.free ;
	
				dTempObject2:=Find_mDebug(debugForm,'mPanel');
				if (dTempObject2<>nil) then begin 
						// ShowMessage ('dTempObject2 je razli�it od nil');
						if dTempObject2 is TPanel then begin
							debugPanel:=Tpanel(dTempObject2);
						end; // if dTempObject2 is TPanel
					end else begin
						// ShowMessage ('Nisam na�ao Panel mPanel pa ga moram kreirati ' );
						debugPanel:=Tpanel.create(debugForm); 
						debugPanel.parent:=debugForm;
						debugPanel.Caption:='mPanel'; 
						debugPanel.hint:='mPanel'; 
						debugPanel.left:=1; debugPanel.top:=1;
						debugPanel.width:=300; debugPanel.height:=300;
						debugPanel.align:=alClient;
				end;	// if (dTempObject2<>nil)
				
				// TRA�I SE MEMO, ako ga nema treba ga kreirati
				// ShowMessage ('Idem tra�iti memo sa hintom zove "DebugMemoHint"');
				if dto4<>nil then dto4.free ;
				dto4:=Find_mDebug(debugPanel,'DebugMemo');
				if dto4<>nil then begin
						if dto4 is TMemo then debugMemo:=TMemo(dto4)
											  else // ShowMessage ('dto4 nije TMemo' );
				end else begin
						debugMemo:=TMemo.Create(debugPanel); 
						// debugMemo.Font.CharSet:=ANSI_CHARSET;
						debugMemo.Parent:=debugPanel; 
						debugMemo.ScrollBars:=ssVertical;
						debugMemo.Align:=alClient;
				
				end;  // if debugMemo=nil
			
			// end;  // if SirinaForme>0

		End else begin
			// ShowMessage ('Nisam na�ao formu koja se zove "DebugForma".');
		end;  // dTempObject<>nil 
		
		
		
		
		
	// end;   // IF memo ne postoji (nakon else)
	
	// Kona�no, pisanje u Memo
	if debugMemo<>nil then begin	
		if	SirinaForme>0	Then Begin
			// treba ispisati jedino ako je vidljiva DebugForma
			// ina�e je bespotreban gubitak vremena
			
			if DebugForm.width > 100 then begin  
				debugMemo.Lines.BeginUpdate;
				debugMemo.Lines.Add(poruka);
				debugMemo.Lines.EndUpdate;
				debugMemo.SelStart:=debugMemo.Lines.Count*1000;
				// debugMemo.SetFocus;
				debugMemo.SelLength:=0;
				// debugMemo.FormStyle:=fsStayOnTop;
			end; // if DebugForm.width > 100			
		end; // if	SirinaForme>0
	end; // if debugMemo<>nil
	
	 // win.show;
   {
	while win.visible do begin
        application.processmessages;
   end;
}
   // destroy form
   // win.free; 
	
	
End; // Procedure Debug

Procedure PrintTime(poruka:string);
var
	XoD,XoT:TObject;
	dTempObject, dTempObject2, dto3, dto4, dto5 :TObject;
	i,j:Integer;
	MyDate:tDateTime;
Begin
	// ShowMessage ('Debug Start');
	
	
		// TRA�I SE za DebugForma
		// ShowMessage ('TRA�I SE za DebugForma!!!');
		
		if dTempObject<>nil then dTempObject.free ;
		dTempObject:=Find_mDebug(application.MainForm,'DebugForma');
		
		if dTempObject<>nil then begin
		
		// ShowMessage ('Na�ao sam formu koja se zove "DebugForma".');
			debugForm:=TForm(dTempObject);

			// debugForm.borderStyle:=bsSizeToolWin; ovako se neda selektirati 
			debugForm.borderStyle:=bsNone; 			// caption=NE; rub=NE; resize=NE


			// ShowMessage ('Idem tra�iti panel koji se zove mPanel');
			if dTempObject2<>nil then dTempObject2.free ;

			dTempObject2:=Find_mDebug(debugForm,'mPanel');
			if (dTempObject2<>nil) then begin 
				// ShowMessage ('dTempObject2 je razli�it od nil');
				if dTempObject2 is TPanel then begin
					debugPanel:=Tpanel(dTempObject2);
				end;
			end else begin
				// ShowMessage ('Nisam na�ao Panel mPanel pa ga moram kreirati ' );
				debugPanel:=Tpanel.create(debugForm); 
				debugPanel.parent:=debugForm;
				debugPanel.Caption:='mPanel'; 
				debugPanel.hint:='mPanel'; 
				debugPanel.left:=1; debugPanel.top:=1;
				debugPanel.width:=300; debugPanel.height:=300;
				debugPanel.align:=alClient;
			end;
			
			// Postoji li Memo ili ga treba kreirati?
			// ShowMessage ('Idem tra�iti memo sa hintom zove "DebugMemoHint"');
			if dto4<>nil then dto4.free ;
			dto4:=Find_mDebug(debugPanel,'DebugMemo');
			if dto4<>nil then begin
				if dto4 is TMemo then debugMemo:=TMemo(dto4)
										else // ShowMessage ('dto4 nije TMemo' );
			end else begin
				// ShowMessage ('kreiranje debugMemo jer je prvi put' );
				debugMemo:=TMemo.Create(debugPanel); 
				debugMemo.Parent:=debugPanel; 
				// debugMemo.Name:='DebugMemo';
				debugMemo.Hint:='DebugMemo';

				debugMemo.ScrollBars:=ssVertical;
				debugMemo.Align:=alClient;

				// application.processmessages;
				
			end;  // if debugMemo=nil
			
		
		End else begin
			// ShowMessage ('Nisam na�ao formu koja se zove "DebugForma".');
		end; ; // dTempObject<>nil
		
		
		
		
		
	// end;   // IF memo ne postoji (nakon else)
	
	// Kona�no, pisanje u Memo
	if debugMemo<>nil then begin	
		debugMemo.Lines.BeginUpdate;
		
		debugMemo.Font.Color:=clSilver; 
		debugMemo.Font.Name:='Consolas';
		debugMemo.Font.Size:=10;
		debugMemo.Color:=clMaroon; 
		
		MyDate:=Now;
		
		poruka:=FormatDateTime('tt', myDate)+' '+poruka;
		debugMemo.Lines.Add(poruka);
		
		debugMemo.Lines.EndUpdate;
		debugMemo.SelStart:=debugMemo.Lines.Count*1000;
			// debugMemo.SetFocus;
		debugMemo.SelLength:=0;
		// debugMemo.FormStyle:=fsStayOnTop;
	end;
	
	 // win.show;
   {
	while win.visible do begin
        application.processmessages;
   end;
}
   // destroy form
   // win.free; 
	
	
End; // Procedure Debug
