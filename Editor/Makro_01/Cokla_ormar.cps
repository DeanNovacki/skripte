program spoj;
// 
var
   napravljeno:boolean;
   i:integer;
   td:Tdaska;
	pogresna:integer;

begin
	napravljeno:=false;
	for i:=0 to e.childdaska.CountObj-1 do Begin     //el.childdaska.CountObj = broj dasaka u lementu
		td:=e.childdaska.Daska[i];                    //jedna daska iz elementa (s rednim brojem i)
		If td.selected then begin
			pogresna:=0;
			// provjera smijera
			If not (td.smjer in [VertFront]) Then Begin 			//( VertBok, VertFront, Horiz, Gore )
				Showmessage('Selektirana daska "'+td.naziv+'"nema smijer "Fronta",' +#13#10+
								 'a trebala bi za ovaj spoj.' );
				pogresna:=pogresna+1;				 
			End;
			// provjera kuta
			If (td.kut<>0) or (td.xkut<>0) or (td.zkut<>0) Then Begin 
				Showmessage('Selektirana daska "'+td.naziv+'"je rotirana",' +#13#10+
								'a ne smije biti za ovaj spoj.' );
				pogresna:=pogresna+1;				 
			End;
			// pozivanje makroa
			if Pogresna=0	Then Begin
				If td.visina<>50 Then Begin 			//
				Showmessage('Cokla ima visinu razli�itu od 5cm.' +#13#10+
								'Obratite pa�nju na parametre za udaljenost tiple od ruba' +#13#10+
								'i rastera ako �elite simetri�no bu�enje.' );
				pogresna:=pogresna+1;				 
			End;
				e.MakeSpoj(td,nil,'..\skripte\Editor\Makro_01\cokla_ormar.cmk','');
				napravljeno:=true;
			end;
		end; // If td.selected
	end; // for i 
  
  e.recalcformula(e);     // ponovo iscrtava - ne treba klik na ekran
 
  if not napravljeno then ShowMessage('Nije postavljen niti jedan spoj.' + #13#10+
												  'Odaberite barem jednu ispravnu dasku ');

end.