{
 BarType   BarName
 0  bcCode_2_5_interleaved,
 1  bcCode_2_5_industrial,
 2  bcCode_2_5_matrix,
 
 3  bcCode39,
 
 4  bcCode39Extended,
 5  bcCode128A,
 6  bcCode128B,
 7  bcCode128C,
 8  bcCode93,
 9  bcCode93Extended,
 10 bcCodeMSI,
 11 bcCodePostNet,
 12 bcCodeCodabar,
 13 bcCodeEAN8,
 14 bcCodeEAN13,
 15 bcCodeUPC_A,
 16 bcCodeUPC_E0,
 17 bcCodeUPC_E1,
 18 bcCodeUPC_Supp2,    
 19 bcCodeUPC_Supp5,    
 20 bcCodeEAN128A,
 21 bcCodeEAN128B,
 22 bcCodeEAN128C
}

//Function PrintBarcode(BarType, BmpWidth, BmpHeight, FontHeight: integer; Code: String): Tbitmap;

{
  ako je BmpHeight > BmpWidth barkod je rotiran za 90 stupnjeva
  ako je FontHeight > 0 ispisuje se text.
  ako je barkod rotiran text se ne ispisuje.
  
} 


var
   PDF:TpdfPrinter; 
   bmp:tbitmap;
begin
  PDF:=TpdfPrinter.create(1250);
   //PDF.Compress:=true; 
   PDF.TITLE := 'Ispis ponude';                     // ?
   pdf.newpage;
   // normalni horizontalni barcode
   // �irina linija se automatski prilagodjava �irini bitmape ako je mogu�e
   // ako je bitmapa premala kod se neda �itat.
   bmp:= PrintBarcode(3,200,50,20,'1234567890'); 
   if bmp<> nil then begin   
      pdf.PrintBitmap(50,0,200,50,bmp);
      bmp.free;
   end;
   // rotirani, vertikalni barcode
   // na ovome ne radi text
   // ne pode�ava sam visinu, pa ako je visina bitmape premala kod se neda �itat
   // trik je napraviti ve�u bitmapu pa ju isprintati na manje dimenzije
   bmp:= PrintBarcode(3,50,700,0,'7234567897');
   if bmp<> nil then begin    
      pdf.PrintBitmap(0,100,50,300,bmp);
      bmp.free;
   end;
   
   pdf.setfont('Microsoft Sans Serif',10);
   
   pdf.TextOut(60,150,'7234567897');
   PDF.FileName := 'bartest.pdf';        
   PDF.EndDoc;
   PDF.ShowPdf;
   PDF.Free;

end.