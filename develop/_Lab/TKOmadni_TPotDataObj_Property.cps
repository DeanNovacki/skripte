
Function booltostr(v:boolean):string;         
begin
     case v of
          True:result:='True';
          False:result:='False';
     end;
end;

Procedure GetKomadni(Po:Tpotrosni;List:tlist);
var
   i:integer;
   pt : TPotrosniOsnovni;
begin
  // svaka daska i svaki element imaju objeth tipa Tpotrosni
  // tu se nalaze trake i sav potrošni
  // Zajednički tip trakama i potrošnom je TPotrosniOsnovni.
   
  for i:= 0 to po.count-1 do begin
      pt := TPotrosniOsnovni(po.Getitem(i));
      if pt.tip = Komadni then List.add(pt);
  end;   
end;

procedure GetKomadniFromElement(elm:telement;List:tlist);
var       
  i:integer;
  Po:Tpotrosni;
begin
  // potrosni elementa
  po:=Elm.Potrosni;
  GetKomadni(po,List);
  
  // sve daske u elementu
  for i := 0 to elm.childdaska.count-1 do begin
    po:=elm.childdaska.Daska[I].Potrosni;
    GetKomadni(po,List);
  end;
  
  
  //svi elementi u elementu
  for i := 0 to elm.ElmList.count-1 do begin
     GetKomadniFromElement(elm.ElmList.element[i],List);
  end;
end; 

Function CollectTkomadni:Tlist;
begin
  result:=tlist.create;
  GetKomadniFromElement(e,result);
end;


Procedure ShowKomadni(List:Tlist);
var
   i:integer;
   tk:tkomadni;    
begin     
  for i:= 0 to List.count-1 do begin
    // sad znam da je u listi samo komadni potrošni
    // pa je sigurno kastati direktno u Tkomadni.
    tk := TKomadni(List.items[i]);
      writeln('------------');      
      writeln('Naziv : '+tk.Naziv);      
      writeln('Šifra : '+tk.Sifra);	    
      writeln('Komada : '+floattostr(tk.Komada));
      writeln('Cijena : '+floattostr(tk.Cijena));
      writeln('JMJ : '+tk.jmj);
      writeln('Rabat : '+floattostr(tk.Rabat));
      writeln('Iznos : '+floattostr(tk.Iznos));
      writeln('------------'); 
      writeln('DobavljacAsString : '+tk.DobavljacAsString);
      writeln('ProizvodjacAsString : '+tk.ProizvodjacAsString);
      writeln('CategoryAsString : '+tk.CategoryAsString);
      writeln('SubCategoryAsString : '+tk.SubCategoryAsString);
      writeln('------------'); 
      writeln('Dobavljac : '+inttostr(tk.Dobavljac));
      writeln('Proizvodjac : '+inttostr(tk.Proizvodjac));
      writeln('Category : '+inttostr(tk.Category));
      writeln('SubCategory : '+inttostr(tk.SubCategory));      
      writeln('ŠifraDobavljaca : '+tk.sifraDobavljaca);
      writeln('ŠifraProizvodjaca : '+tk.sifraProizvodjaca);
      writeln('Težina : '+floattostr(tk.Tezina));
      writeln('ModelFile : '+tk.ModelFile);
      writeln('ElementFile : '+tk.ElementFile);
      writeln('------------'); 
      // ista stvar ko i gore ali je direktni pristup u TPotDataObj objekt
      writeln('Dobavljac : '+inttostr(tk.data.DPKSExtra.Dobavljac));
      writeln('Proizvodjac : '+inttostr(tk.data.DPKSExtra.Proizvodjac));
      writeln('Category : '+inttostr(tk.data.DPKSExtra.Category));
      writeln('SubCategory : '+inttostr(tk.data.DPKSExtra.SubCategory));      
      writeln('ŠifraDobavljaca : '+tk.data.DPKSExtra.sifraDobavljaca);
      writeln('ŠifraProizvodjaca : '+tk.data.DPKSExtra.sifraProizvodjaca);
      writeln('Težina : '+floattostr(tk.data.DPKSExtra.Tezina));
      writeln('ModelFile : '+tk.data.DPKSExtra.ModelFile);
      writeln('ElementFile : '+tk.data.DPKSExtra.ElementFile);
  end;      
      
end;      

var
   fList:tlist;
begin	
  flist:=CollectTkomadni;
  showmessage(inttostr(flist.count));
  ShowKomadni(flist);
  flist.free;
end.
