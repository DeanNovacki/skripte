Program Makro_remover_01;  
// v1.00 (19.08.2015)
// Skripta koja uklanja makroe

   {$I math_rot.cps}
   {$I dex_unit.cps}
         
var
   Prozor:TForm;
   T1, T2, T3: TButton;
   M1:TMemo;
   N1, N2:TLabel;

Procedure Obrisi_spojeve(el:TElement);              // rekurzivna!
var
   i:integer;
begin 
      el.destroyLinks;                              // brise sve spojeve u elementu
      el.RecalcFormula(nil);                        // ponovo pokrece izracun formula u elementu
      m1.lines.add ('Spojevi su obrisani (ako ih je bilo)');
   for i:=0 to el.elmlist.count-1 do obrisi_spojeve(el.elmlist.element[i]);
end;

Procedure Obrada_projekta(sender:Tobject);
// obra�uje selektirane elemente koji se nalaze u projektu

var
   i:integer;
   elem:Telement;
begin

  m1.lines.add ('============================================');
  
  if ElmList.Count=0 then m1.lines.add ('U PROJEKTU NEMA ELEMENATA!');
  
  
  for i:=0 to ElmList.Count-1 do begin
      elem:=TElement(ElmList.Items[i]);
      if  elem.selected then begin
         m1.lines.add ('==>El: "'+elem.naziv+'", Br. dasaka: '+IntToStr(elem.childdaska.CountObj)+
                ', Br. podelemenata: '+IntToStr(elem.ElmList.CountObj));
         obrisi_spojeve(elem);
      end else m1.lines.add ('El: "'+elem.Naziv+'" nije selektiran niti obra�en.');
    m1.lines.add ('============================================');
  end;
    m1.lines.add ('Posao zavrsen!');
end;

procedure Pomoc(sender:Tobject);
var
   pTekst:string;
begin
        pTekst:='FR v1.0'+#13#10+
                'Skripta za uklanjanje makroa '+#13#10
                'na selektiranim elementima.'+#13#10+#13#10+
                ''+#13#10+
                ''+#13#10+
                ''+#13#10+#13#10+
                'Skripta ne radi u editoru nego samo u projektu.';
        ShowMessage(pTekst);
     
end;   


Begin
  //showmessage ('Makro remover');
  Prozor:=Window('Red Cat',420,440);
  naslov('Brisanje makroa',Prozor);
  M1:=Memo(Prozor,10,120,395,250);
  M1.ScrollBars:=ssVertical;
  
  N1:=natpis('Svim selektiranim elementima �e biti obrisani makroi!',Prozor,15,50,'lijevo');
  N2:=natpis('Makroi slu�e za automatsko kreiranje rupa i dodavanje'+#13#10+
				'potro�nog materijala.',Prozor,15,70,'lijevo');  
				
  N1.font.size:=10;
  N2.font.size:=10;
       
  T1:=Tipka('Napravi',Prozor,10,375);
  // T2:=Tipka('Pomoc',Prozor,90,530);
  T3:=Tipka('Izlaz',Prozor,330,375);
  
  T1.default:=True;
  
  T1.OnClick:=@Obrada_Projekta;
  // T2.OnClick:=@Pomoc;
  T3.ModalResult:=mrCancel;          // ide dalje (iza prozor.showmodal)
  prozor.showmodal;
  Prozor.free;
  
End.
