{
Procedure Radio_Image_OnClick(sender:Tobject);
Procedure Fields_Name_OnChange(sender:Tobject);
Procedure Fields_OnChange(sender:Tobject);

}
   
Procedure Radio_Image_OnClick(sender:Tobject);

Begin
   // Showmessage ('Radio_Image_OnClick');
   
   // shape
   if (TMImage(sender).Tag>=101) and (TMImage(sender).Tag<=106) then begin
      el_shape:=TMImage(sender).Tag-100;
      message('el_shape: '+IntToStr(el_shape)+' | picture tag: '+IntToStr(TMImage(sender).Tag));
	  // ss:=IntToStr(el_shape);
	  draw_AP2s(IntToStr(el_shape));

   end;
   
   // top type
   if (TMImage(sender).Tag>=301) and (TMImage(sender).Tag<=303) then begin
      top_type:=TMImage(sender).Tag-300;
      message('top_type: '+IntToStr(top_type));
   end;
   
   // bottom type
   if (TMImage(sender).Tag>=401) and (TMImage(sender).Tag<=403) then begin 

      bottom_type:=TMImage(sender).Tag-400;
      message('bottom_type: '+IntToStr(bottom_type));       
   end;

   
{   
   // back exist
   if (TMImage(sender).Tag>=501) and (TMImage(sender).Tag<=502) then begin

      back_on:=TMImage(sender).Tag-500;
      message('back_on: '+IntToStr(back_on));
   end;                             
}                                
   // shelves quantity
   if (TMImage(sender).Tag>=701) and (TMImage(sender).Tag<=703) then begin 

      shelf_qnt:=TMImage(sender).Tag-700;
      message('shelf_qnt: '+IntToStr(shelf_qnt));
   end;

{
   // shelves movabilitiy
   if (TMImage(sender).Tag>=707) and (TMImage(sender).Tag<=708) then begin 

      shelf_mov:=TMImage(sender).Tag-706;
      message('shelf_mov: '+IntToStr(shelf_mov));
   end;
}
   // Legs (base) height
   if (TMImage(sender).Tag>=801) and (TMImage(sender).Tag<=802) then begin 
      leg_height:=TMImage(sender).Tag-800;
      message('leg_height: '+IntToStr(leg_height));
   end;

{   
   // Doors
   if (TMImage(sender).Tag>=901) and (TMImage(sender).Tag<=904) then begin 
      door_qnt:=TMImage(sender).Tag-900;
      message('door_qnt: '+IntToStr(door_qnt));
   end;
}                                                                                               // <<====================== 
                                                                                               // <<====================== 
                                                                                               // <<======================                                                                                                              
                 
   
   Apply_preset;   
   
end;   

Procedure Fields_Name_OnChange(sender:Tobject);
var
   tag,len:Integer;
Begin
   if TEdit(Sender).modified then begin         // if field is changed after user input
       if TEdit(Sender).text='' then begin     // If user delete all content of field
              TEdit(Sender).text:=' ';        // 
              TEdit(Sender).SelectAll;      // select entry to delete space when start typing
       end;  // if TEdit(sender...
      
         try
           tag:=TEdit(Sender).tag;                  // read tag
           len:=Length(TEdit(Sender).text);         // Length of string
         except
           showmessage('??? 206 ???');
         end;  // try
        case tag of
   
           // process el_name on change
           206 : begin
                     if (len<3) or (len>32) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_length( 3,32);
                      end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           Warning_off;
                           el_name:=TEdit(Sender).text;
                     end; // if (val<...
                     lock_main_buttons;
           end;  // 204
        
           // name of new preset
           20 : begin
                     if (len<3) or (len>48) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_length( 3,48);
                      end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           Warning_off;
                           

                     end; // if (val<...
                     lock_main_buttons;
           end;  // 204
        end;   // case
   end;   // if if TEdit(Sender).modified 
end;

Procedure Fields_OnChange(sender:Tobject);
// 1.07

var
   val:integer;
   tag:integer;
   pass:boolean;
begin
   // message('Something changed on fields');
   
    pass:=true;
    
    if TEdit(Sender).modified then begin         // if field is changed after user input
         if TEdit(Sender).text='' then begin     // If user delete all content of field
              TEdit(Sender).text:='0';        // 
              TEdit(Sender).SelectAll;      // select entry to delet zero when start typing
         end;  // if TEdit(sender...
         
         try
           tag:=TEdit(Sender).tag;            // read tag
           if TEdit(Sender).text='-' then val:=0         // read value
                                     else val:=StrToInt(TEdit(Sender).text);
         except
           // if TEdit(Sender).text='-' then begin 
           //                               // must pass, because of typing negative values
           //                               val:=0;
                                          
           //                           end else begin
                                          showmessage('RCE: Field value is not a number!');
           //end;  // if
         end;  // try
       
         
       
         case tag of
   
           // process el_width on change
           201 : begin
                     if (val<el_width_min) or (val>el_width_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( el_width_min,el_width_max);
                           pass:=false;
                        end else begin       // ok result
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           el_width:=StrToInt(TEdit(Sender).text);
                           Warning_off;
                           // pass:=true;
                     end; // if (val<...
                 end;  // 201
                 
           // process el_height on change      
           202 : begin
                     if (val<el_height_min) or (val>el_height_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( el_height_min,el_height_max);
                           // warning.caption:='WARNING';
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           el_height:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  // 202     
                 
            // process el_depth on change      
           203 : begin
                     if (val<el_depth_min) or (val>el_depth_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( el_depth_min,el_depth_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           el_depth:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  // 203          
                
                
            // process left_depth on change      
           204 : begin
                     if (val<left_depth_min) or (val>left_depth_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( left_depth_min,left_depth_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           left_depth:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  // 203          

                
            // process el_depth on change      
           205 : begin
                     if (val<right_depth_min) or (val>right_depth_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( right_depth_min,right_depth_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           right_depth:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  // 203          
				 
            // process top_thicknes_depth on change      
           304 : begin
                     if (val<top_thick_min) or (val>top_thick_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( top_thick_min,top_thick_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           top_thick:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  // 304 
                 
                 
                
            // process top_dist_back on change      
           
		   305 : begin
                     if (val<top_dist_back_min) or (val>top_dist_back_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( top_dist_back_min,top_dist_back_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           top_dist_back_fix:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  // 305
           {      
           // process Front_top_width on change      
           306 : begin
                     if (val<top_width_front_min) or (val>top_width_front_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( top_width_front_min,top_width_front_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           top_width_front:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                          
			
           // process top_dist_back on change      
           307 : begin
                     if (val<top_dist_back_min) or (val>top_dist_back_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( top_dist_back_min,top_dist_back_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           top_dist_back_fix:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  // 307                         


           // process top_dist_front on change      
           308 : begin
                     if (val<top_dist_front_min) or (val>top_dist_front_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( top_dist_front_min,top_dist_front_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           top_dist_front:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                          
			}
           // process bottom_thick on change      
           404 : begin
                     if (val<bottom_thick_min) or (val>bottom_thick_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range( bottom_thick_min,bottom_thick_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           bottom_thick:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                          


           // process bottom_dist_back on change     
           405 : begin
                     if (val<bottom_dist_back_min) or (val>bottom_dist_back_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(bottom_dist_back_min,bottom_dist_back_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           bottom_dist_back:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                          
			{
           // process bottom_dist_back on change      
           406 : begin
                     if (val<bottom_dist_back_min) or (val>bottom_dist_back_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(bottom_dist_back_min,bottom_dist_back_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           bottom_dist_back:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                          
			}
           // process back_dist on change      
           503 : begin
                     // message('val:'+IntToStr(val)+' back_dist_min: '+IntToStr(back_dist_min)+' back_dist_max: '+IntToStr(back_dist_max));
                     if (val<back_dist_min) or (val>back_dist_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(back_dist_min,back_dist_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           if TEdit(Sender).text<>'-' then back_dist:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  // 


           // process back_thick on change      
           504 : begin
                     if (val<back_thick_min) or (val>back_thick_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(back_thick_min,back_thick_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           back_thick:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  // 
                 

           // process back_ins on change      
           505 : begin
                     if (val<back_ins_min) or (val>back_ins_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(back_ins_min,back_ins_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           back_ins:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                  

           // process back_top_width on change      
           506 : begin
                     if (val<back_top_width_min) or (val>back_top_width_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(back_top_width_min,back_top_width_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           back_top_width:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                       

           // process back_down_width on change      
           507 : begin
                     if (val<back_down_width_min) or (val>back_down_width_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(back_down_width_min,back_down_width_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           back_down_width:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                   

           // process side_thick on change      
           601 : begin
                     if (val<side_thick_min) or (val>side_thick_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(side_thick_min,side_thick_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           side_thick:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                  
                                                                                 
            // process shelf_thick on change      
           705 : begin
                     if (val<shelf_thick_min) or (val>shelf_thick_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(shelf_thick_min,shelf_thick_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           shelf_thick:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                      
                 
           // process shelf_front_dist on change      
           706 : begin
                     if (val<shelf_front_dist_min) or (val>shelf_front_dist_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(shelf_front_dist_min,shelf_front_dist_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           shelf_front_dist:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //       
                 
                 
           // process radius      
           707 : begin
                     if (val<shelf_radius_min) or (val>shelf_radius_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(shelf_radius_min,shelf_radius_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           shelf_radius:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //   
                 
           // process front cut      
           708 : begin
                     if (val<shelf_cut_front_min) or (val>shelf_cut_front_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(shelf_cut_front_min,shelf_cut_front_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           shelf_cut_front:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //   
                 
           // process side cut     
           709 : begin
                     if (val<shelf_cut_side_min) or (val>shelf_cut_side_max) then begin
                           TEdit(Sender).font.color:=clRed;
                           TEdit(Sender).font.style:=[fsBold];
                           Warning_range(shelf_cut_side_min,shelf_cut_side_max);
                           pass:=false;
                        end else begin
                           TEdit(Sender).font.color:=clWindowText;
                           TEdit(Sender).font.style:=[];
                           shelf_cut_side:=StrToInt(TEdit(Sender).text);
                           // pass:=true;
                           Warning_off;
                     end; // if (val<...
                 end;  //                                                          
                 
                                                                                               // <<====================== 
                                                                                               // <<====================== 
                                                                                               // <<====================== 

         end; // case

   end; // if TEedit(Sender).modified...
   
   lock_main_buttons;   // disable main buttons in case of wrong input


end; 
