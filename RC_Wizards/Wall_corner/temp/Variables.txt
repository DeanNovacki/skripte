Name			Tag Description
-----------		--- ---------------
el_name
el_type			
el_date
el_shape 		101-106



el_width 		201 Width
el_width_min
el_width_max
el_height		202 Height
el_height_min
el_height_max
el_depth		203 Depth
el_depth_min
el_depth_max

top_type		301-303
top_thick		304 Top thickness
top_thick_min
top_thick_max
top_width_back		305 Back Top Width
top_width_back_min
top_width_back_max
top_width_front		306 Front Top Width
top_width_front_min
top_width_front_max
top_dist_back_fix	307 Back Top Distance
top_dist_back_min
top_dist_back_max
top_dist_front		308 Front Top distance
top_dist_front_min
top_dist_front_max



bottom_type		401-403
bottom_thick		404 Bottom Thickness
bottom_thick_min
bottom_thick_max
bottom_dist_front  	405 Front Bottom Distance
bottom_dist_front_min
bottom_dist_front_max
bottom_dist_back	406 Back Bottom Distance
bottom_dist_back_min
bottom_dist_back_max


Back_on			501-502
back_dist		503 Back Distance -
back_dist_min
back_dist_max
back_thick		504 Back Thickness
back_thick_min
back_thick_max
back_ins		505 Back Insert
back_ins_min
back_ins_max
back_top_width		506 Top Back Height
back_top_width_min
back_top_width_max
back_down_width		507 Bottom Back Height
back_down_width_min
back_down_width_max

side_thick		601 Side Thickness
side_thick_min
side_thick_max

shelf_qnt=1		701-703 (704)
shelf_thick=18		705 Shelf thickness
shelf_thick_min=12	
shelf_thick_max=50
shelf_front_dist=20	706 Shelf front distance
shelf_front_dist_min=1
shelf_front_dist_max=50
shelf_mov=1		707-708 Shelf movable
shelf_mov_min=0
shelf_mov_max=3

leg_height=1
leg_dist_front=100
leg_dist_front_min=50
leg_dist_front_max=200
leg_dist_side=60
leg_dist_side_min=50
leg_dist_side_max=200
leg_dist_back=100
leg_dist_back_min=50
leg_dist_back_max=200

door_type=1
door_orient='left'





Postaviti event (Set_Events procedura) za
	- Radio_Image_OnClick
	- Check_Int_Pos
	- Fields_OnChange

Obraditi u eventima za 
	- Radio_Image_onClick

Obraditi u procedurama:
	- Apply_preset 
	
