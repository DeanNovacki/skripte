unit uPSI_PDFPrinter;
{
This file has been generated by UnitParser v0.7, written by M. Knight
and updated by NP. v/d Spek and George Birbilis. 
Source Code from Carlo Kok has been used to implement various sections of
UnitParser. Components of ROPS are used in the construction of UnitParser,
code implementing the class wrapper is taken from Carlo Kok's conv utility

}
interface
 
uses
   SysUtils
  ,Classes
  ,uPSComponent
  ,uPSRuntime
  ,uPSCompiler
  ;
 
type 
(*----------------------------------------------------------------------------*)
  TPSImport_PDFPrinter = class(TPSPlugin)
  protected
    procedure CompileImport1(CompExec: TPSScript); override;
    procedure ExecImport1(CompExec: TPSScript; const ri: TPSRuntimeClassImporter); override;
  end;
 
 
{ compile-time registration functions }
procedure SIRegister_TPrintPDF(CL: TPSPascalCompiler);
procedure SIRegister_TPDFFont(CL: TPSPascalCompiler);
procedure SIRegister_PDFPrinter(CL: TPSPascalCompiler);

{ run-time registration functions }
procedure RIRegister_TPrintPDF(CL: TPSRuntimeClassImporter);
procedure RIRegister_TPDFFont(CL: TPSRuntimeClassImporter);
procedure RIRegister_PDFPrinter(CL: TPSRuntimeClassImporter);

procedure Register;

implementation


uses
   WinProcs
  ,WinTypes
  ,Messages
  ,Graphics
  ,Controls
  ,StdCtrls
  ,ExtCtrls
  ,Forms
  ,Dialogs
  ,ZLib
  ,PDFPrinter
  ;
 
 
procedure Register;
begin
  RegisterComponents('Pascal Script', [TPSImport_PDFPrinter]);
end;

(* === compile-time registration functions === *)
(*----------------------------------------------------------------------------*)
procedure SIRegister_TPrintPDF(CL: TPSPascalCompiler);
begin
  //with RegClassS(CL,'TComponent', 'TPrintPDF') do
  with CL.AddClassN(CL.FindClass('TComponent'),'TPrintPDF') do
  begin
    RegisterMethod('Procedure BeginDoc');
    RegisterMethod('Procedure EndDoc');
    RegisterMethod('Procedure NewPage');
    RegisterMethod('Procedure DrawLine( x1, y1, x2, y2 : Integer)');
    RegisterMethod('Procedure DrawRectangle( x1, y1, x2, y2 : Integer)');
    RegisterMethod('Procedure TextOut( X, Y : Integer; const Text : string)');
    RegisterMethod('Procedure MemoOut( X, Y : Integer; Memo : TMemo)');
    RegisterMethod('Procedure ImageOut( X, Y : Integer; ABitmap : TImage)');
    RegisterMethod('Procedure DrawBitmap( X, Y : Integer; ABitmap : TBitmap)');
    RegisterMethod('Procedure Draw( X, Y : Integer; ABitmap : TImage)');
    RegisterMethod('Procedure SetColor( Acolor : integer)');
    RegisterMethod('Procedure SetRGBColor( R, G, B : single)');
    RegisterMethod('Procedure ShowPdf');
    RegisterProperty('FileName', 'string', iptrw);
    RegisterProperty('TITLE', 'string', iptrw);
    RegisterProperty('PageNumber', 'Integer', iptr);
    RegisterProperty('PageWidth', 'Integer', iptrw);
    RegisterProperty('PageHeight', 'Integer', iptrw);
    RegisterProperty('LineWidth', 'Integer', iptrw);
    RegisterProperty('Author', 'string', iptrw);
    RegisterProperty('Creator', 'string', iptrw);
    RegisterProperty('Keywords', 'string', iptrw);
    RegisterProperty('Subject', 'string', iptrw);
    RegisterProperty('Producer', 'string', iptrw);
    RegisterProperty('Font', 'TPDFFont', iptrw);
    RegisterProperty('Compress', 'boolean', iptrw);
  end;
end;

(*----------------------------------------------------------------------------*)
procedure SIRegister_TPDFFont(CL: TPSPascalCompiler);
begin
  //with RegClassS(CL,'TOBJECT', 'TPDFFont') do
  with CL.AddClassN(CL.FindClass('TOBJECT'),'TPDFFont') do
  begin
    RegisterProperty('Name', 'TPDFFontName', iptrw);
    RegisterProperty('Size', 'Integer', iptrw);
    RegisterProperty('color', 'integer', iptrw);
  end;
end;

(*----------------------------------------------------------------------------*)
procedure SIRegister_PDFPrinter(CL: TPSPascalCompiler);
begin
  CL.AddTypeS('TPDFOrientation', '( poPortrait, poLandscape )');
  CL.AddTypeS('TPDFBrushStyle', '( poSolid, poDashed, poBeveled, poInset, poUnd'
   +'erline )');
  CL.AddTypeS('TPDFFontName', '( poHelvetica, poHelveticaBold, poHelveticaObliq'
   +'ue, poHelveticaBoldOblique, poCourier, poCourierBold, poCourierOblique, po'
   +'CourierBoldOblique, poTimesRoman, poTimesBold, poTimesItalic, poTimesBoldI'
   +'talic, poSymbol, poZapfDingbats )');
  SIRegister_TPDFFont(CL);
  SIRegister_TPrintPDF(CL);
end;

(* === run-time registration functions === *)
(*----------------------------------------------------------------------------*)
procedure TPrintPDFCompress_W(Self: TPrintPDF; const T: boolean);
begin Self.Compress := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFCompress_R(Self: TPrintPDF; var T: boolean);
begin T := Self.Compress; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFFont_W(Self: TPrintPDF; const T: TPDFFont);
begin Self.Font := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFFont_R(Self: TPrintPDF; var T: TPDFFont);
begin T := Self.Font; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFProducer_W(Self: TPrintPDF; const T: string);
begin Self.Producer := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFProducer_R(Self: TPrintPDF; var T: string);
begin T := Self.Producer; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFSubject_W(Self: TPrintPDF; const T: string);
begin Self.Subject := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFSubject_R(Self: TPrintPDF; var T: string);
begin T := Self.Subject; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFKeywords_W(Self: TPrintPDF; const T: string);
begin Self.Keywords := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFKeywords_R(Self: TPrintPDF; var T: string);
begin T := Self.Keywords; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFCreator_W(Self: TPrintPDF; const T: string);
begin Self.Creator := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFCreator_R(Self: TPrintPDF; var T: string);
begin T := Self.Creator; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFAuthor_W(Self: TPrintPDF; const T: string);
begin Self.Author := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFAuthor_R(Self: TPrintPDF; var T: string);
begin T := Self.Author; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFLineWidth_W(Self: TPrintPDF; const T: Integer);
begin Self.LineWidth := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFLineWidth_R(Self: TPrintPDF; var T: Integer);
begin T := Self.LineWidth; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFPageHeight_W(Self: TPrintPDF; const T: Integer);
begin Self.PageHeight := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFPageHeight_R(Self: TPrintPDF; var T: Integer);
begin T := Self.PageHeight; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFPageWidth_W(Self: TPrintPDF; const T: Integer);
begin Self.PageWidth := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFPageWidth_R(Self: TPrintPDF; var T: Integer);
begin T := Self.PageWidth; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFPageNumber_R(Self: TPrintPDF; var T: Integer);
begin T := Self.PageNumber; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFTITLE_W(Self: TPrintPDF; const T: string);
begin Self.TITLE := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFTITLE_R(Self: TPrintPDF; var T: string);
begin T := Self.TITLE; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFFileName_W(Self: TPrintPDF; const T: string);
begin Self.FileName := T; end;

(*----------------------------------------------------------------------------*)
procedure TPrintPDFFileName_R(Self: TPrintPDF; var T: string);
begin T := Self.FileName; end;

(*----------------------------------------------------------------------------*)
procedure TPDFFontcolor_W(Self: TPDFFont; const T: integer);
Begin Self.color := T; end;

(*----------------------------------------------------------------------------*)
procedure TPDFFontcolor_R(Self: TPDFFont; var T: integer);
Begin T := Self.color; end;

(*----------------------------------------------------------------------------*)
procedure TPDFFontSize_W(Self: TPDFFont; const T: Integer);
Begin Self.Size := T; end;

(*----------------------------------------------------------------------------*)
procedure TPDFFontSize_R(Self: TPDFFont; var T: Integer);
Begin T := Self.Size; end;

(*----------------------------------------------------------------------------*)
procedure TPDFFontName_W(Self: TPDFFont; const T: TPDFFontName);
Begin Self.Name := T; end;

(*----------------------------------------------------------------------------*)
procedure TPDFFontName_R(Self: TPDFFont; var T: TPDFFontName);
Begin T := Self.Name; end;

(*----------------------------------------------------------------------------*)
procedure RIRegister_TPrintPDF(CL: TPSRuntimeClassImporter);
begin
  with CL.Add(TPrintPDF) do
  begin
    RegisterMethod(@TPrintPDF.BeginDoc, 'BeginDoc');
    RegisterMethod(@TPrintPDF.EndDoc, 'EndDoc');
    RegisterMethod(@TPrintPDF.NewPage, 'NewPage');
    RegisterMethod(@TPrintPDF.DrawLine, 'DrawLine');
    RegisterMethod(@TPrintPDF.DrawRectangle, 'DrawRectangle');
    RegisterMethod(@TPrintPDF.TextOut, 'TextOut');
    RegisterMethod(@TPrintPDF.MemoOut, 'MemoOut');
    RegisterMethod(@TPrintPDF.ImageOut, 'ImageOut');
    RegisterMethod(@TPrintPDF.DrawBitmap, 'DrawBitmap');
    RegisterMethod(@TPrintPDF.Draw, 'Draw');
    RegisterMethod(@TPrintPDF.SetColor, 'SetColor');
    RegisterMethod(@TPrintPDF.SetRGBColor, 'SetRGBColor');
    RegisterMethod(@TPrintPDF.ShowPdf, 'ShowPdf');
    RegisterPropertyHelper(@TPrintPDFFileName_R,@TPrintPDFFileName_W,'FileName');
    RegisterPropertyHelper(@TPrintPDFTITLE_R,@TPrintPDFTITLE_W,'TITLE');
    RegisterPropertyHelper(@TPrintPDFPageNumber_R,nil,'PageNumber');
    RegisterPropertyHelper(@TPrintPDFPageWidth_R,@TPrintPDFPageWidth_W,'PageWidth');
    RegisterPropertyHelper(@TPrintPDFPageHeight_R,@TPrintPDFPageHeight_W,'PageHeight');
    RegisterPropertyHelper(@TPrintPDFLineWidth_R,@TPrintPDFLineWidth_W,'LineWidth');
    RegisterPropertyHelper(@TPrintPDFAuthor_R,@TPrintPDFAuthor_W,'Author');
    RegisterPropertyHelper(@TPrintPDFCreator_R,@TPrintPDFCreator_W,'Creator');
    RegisterPropertyHelper(@TPrintPDFKeywords_R,@TPrintPDFKeywords_W,'Keywords');
    RegisterPropertyHelper(@TPrintPDFSubject_R,@TPrintPDFSubject_W,'Subject');
    RegisterPropertyHelper(@TPrintPDFProducer_R,@TPrintPDFProducer_W,'Producer');
    RegisterPropertyHelper(@TPrintPDFFont_R,@TPrintPDFFont_W,'Font');
    RegisterPropertyHelper(@TPrintPDFCompress_R,@TPrintPDFCompress_W,'Compress');
  end;
end;

(*----------------------------------------------------------------------------*)
procedure RIRegister_TPDFFont(CL: TPSRuntimeClassImporter);
begin
  with CL.Add(TPDFFont) do
  begin
    RegisterPropertyHelper(@TPDFFontName_R,@TPDFFontName_W,'Name');
    RegisterPropertyHelper(@TPDFFontSize_R,@TPDFFontSize_W,'Size');
    RegisterPropertyHelper(@TPDFFontcolor_R,@TPDFFontcolor_W,'color');
  end;
end;

(*----------------------------------------------------------------------------*)
procedure RIRegister_PDFPrinter(CL: TPSRuntimeClassImporter);
begin
  RIRegister_TPDFFont(CL);
  RIRegister_TPrintPDF(CL);
end;

 
 
{ TPSImport_PDFPrinter }
(*----------------------------------------------------------------------------*)
procedure TPSImport_PDFPrinter.CompileImport1(CompExec: TPSScript);
begin
  SIRegister_PDFPrinter(CompExec.Comp);
end;
(*----------------------------------------------------------------------------*)
procedure TPSImport_PDFPrinter.ExecImport1(CompExec: TPSScript; const ri: TPSRuntimeClassImporter);
begin
  RIRegister_PDFPrinter(ri);
end;
(*----------------------------------------------------------------------------*)
 
 
end.
