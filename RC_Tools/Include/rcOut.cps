// Create Journal window with memo and display text with
// If Journal window is already created then add new line of text
// Primjer:
// 
var
   rcOut_enabled:Boolean;
   rc_TF:TForm;
   rc_MD:TMemo;
Procedure rcOut(ss:string);
Begin
   // try to write to memo
   if rcOut_enabled=true then begin 
      // Showmessage('rcOut procedure: rcOut_enabled=true');
      if rc_TF=nil then Begin  // if form not exist create it
			// Showmessage('Kreiram formu za RC Out iz procedure RCOut');
         rc_TF:=TForm.create(nil);
         
			rc_TF.BorderStyle:=bsSizeToolWin;
         rc_TF.Show;  
			
         rc_TF.FormStyle:=fsNormal;
			{
			testirano na ActiveScript
			fsMDIChild = LO� IZBOR! Novi prozor postaje "brat" projektu, a dijete Corpusu, neda se zagasiti nista, acces violation. Projekt izlazi iz maximizea.  
			fsMDIForm = Radi, ne mo�e se dirati, gasi se sa editorom, ali je ispod njega;
			fsNormal = Radi, ne mo�e se dirati, gasi se sa editorom, ali je ispod njega;
			fsSplash = NE KOMPAJLIRA!
			fsStayOnTop = Radi, ne mo�e se dirati kad radi editor, ali ostaje nakon ga�enja editora. Normalno se sve gasi
			fsSystemStayOnTop = NE KOMPAJLIRA!
			nije definirano = Radi, ne mo�e se dirati, gasi se sa editorom, ali je ispod njega;
			}
         rc_TF.Caption:='Journal'; 
			rc_TF.Width:=350;
         rc_TF.Height:=1000;
			
         rc_MD:=TMemo.Create(rc_TF);
         rc_MD.Parent:=rc_TF;  
         rc_MD.Color:=clBtnFace;
         rc_MD.Font.Name:='Arial';
         rc_MD.Font.Size:=7;
         // rc_MD.Font.Color:=clMaroon;
			// rc_MD.Font.Color:=clGreen;
         rc_MD.ScrollBars:=ssVertical;
         rc_MD.Align:=alClient;
         
      end; // if rc_TF=nil
      if rc_MD=nil then Begin  // Ako memo ne postoji. To se de�ava ako je Forma ve� ranije kreirana, ali memo nije
			
         rc_MD:=TMemo.Create(rc_TF);
         rc_MD.Parent:=rc_TF;  
         rc_MD.Color:=clBtnFace;
         rc_MD.Font.Name:='Arial';
         rc_MD.Font.Size:=7;
         // rc_MD.Font.Color:=clMaroon;
			rc_MD.Font.Color:=clGreen;
         rc_MD.ScrollBars:=ssVertical;
         rc_MD.Align:=alClient;
		End;
		
		
		
		rc_MD.Lines.BeginUpdate;
      rc_MD.Lines.Add(ss);
      rc_MD.Lines.EndUpdate;
      rc_MD.SelStart:=rc_MD.Lines.Count*1000;
      // rc_MD.SetFocus;
      rc_MD.SelLength:=0;
		rc_TF.FormStyle:=fsStayOnTop;

   end else begin  // if rcOut_enabled=true
      // Showmessage('rcOut procedure: rcOut_enabled=False');
      // Showmessage('rcOut procedure: Test if not nil');
      if rc_TF<>nil then begin 
                           // Showmessage('rcOut procedure: Try to close');
                           rc_TF.free;  // release, close
                           //rc_TF.close;    // na free se ru�i, izgleda da je ovo bolje  
      end; // if rc_TF<>nil   
   end ; // if rcOut_enabled=true
end;

Procedure rcOutP(ss:string;parentF:TForm);
// ova procedura je napravljena da bi radila u editoru i da njen prozor bude parent Editoru
// parentF je forma koja je roditelj
// da bi to radilo u editoru, treba deklarirati prozor editora ovako:
//
// var
// 	PForm:TForm;
// begin 
// 	pForm:=TForm(twincontrol(parent));    // prozor editora!
// 	RCOutP('Journal forma je upravo kreirana',pForm); 

Begin
   // try to write to memo
	// Showmessage('RcOutP START'); 
	// if rcOut_enabled=true then Showmessage('rcOut_enabled=true')
	// 							 else Showmessage('rcOut_enabled=false');
   if rcOut_enabled=true then begin 
      // Showmessage('rcOut procedure: rcOut_enabled=true');
      if rc_TF=nil then Begin  // if form not exist create it
         rc_TF:=TForm.create(parentF);
			rc_TF.parent:=parentF;
         rc_TF.BorderStyle:=bsSizeToolWin;
			// rc_TF.BorderStyle:=bsSizeable;
														// bsToolWindow - pomak DA, resize NE, caption DA, tipke X, sitni, ZGODNI
														// bsSizeToolWin - pomak DA, resize DA, caption DA, tipke X, KRUPNI 
														// bsNone - pomak NE, resize NE, caption NE, tipke NE, predobar, mali, bez resizea 
														// bsDialog - pomak DA, resize NE, caption DA, tipke X, BORDER DA, sitni, ZGODNI 
														// bsSizeable - debeo, ru�an ima sve tipke, ima resize
														// bsSingle
         rc_TF.Show;  
			// rc_TF.BorderWidth:=0; NE RADI!!!
			
         rc_TF.FormStyle:=fsStayOnTop
			{
			testirano na ActiveScript
			fsMDIChild = LO� IZBOR! Novi prozor postaje "brat" projektu, a dijete Corpusu, neda se zagasiti nista, acces violation. Projekt izlazi iz maximizea.  
			fsMDIForm = Radi, ne mo�e se dirati, gasi se sa editorom, ali je ispod njega;
			fsNormal = Radi, ne mo�e se dirati, gasi se sa editorom, ali je ispod njega;
			fsSplash = NE KOMPAJLIRA!
			fsStayOnTop = Radi, ne mo�e se dirati kad radi editor, ali ostaje nakon ga�enja editora. Normalno se sve gasi
			fsSystemStayOnTop = NE KOMPAJLIRA!
			nije definirano = Radi, ne mo�e se dirati, gasi se sa editorom, ali je ispod njega;
			}
         rc_TF.Caption:='Journal'; 
			rc_TF.Width:=350;
         rc_TF.Height:=1000;
			read_form(rc_TF,PrgFolder+'skripte\RC_Tools\Include\Journal');  // postavljanje prija�nje pozicije, dimenzija i stanja
			
			
         rc_MD:=TMemo.Create(rc_TF);
         rc_MD.Parent:=rc_TF;  
         rc_MD.Color:=clBtnFace;
         rc_MD.Font.Name:='Arial';
         rc_MD.Font.Size:=7;
         rc_MD.Font.Color:=clMaroon;
         rc_MD.ScrollBars:=ssVertical;
         rc_MD.Align:=alClient;
			rc_MD.ReadOnly:=False;
         
      end; // if rc_TF=nil
      rc_MD.Lines.BeginUpdate;
      rc_MD.Lines.Add(ss);
      rc_MD.Lines.EndUpdate;
      rc_MD.SelStart:=rc_MD.Lines.Count*1000;
      //rc_MD.SetFocus;
      rc_MD.SelLength:=0;
		
		rc_TF.FormStyle:=fsStayOnTop;
		

   end else begin  // if rcOut_enabled=true
      // Showmessage('rcOut procedure: rcOut_enabled=False');
      // Showmessage('rcOut procedure: Test if not nil');
      if rc_TF<>nil then begin 
                           // Showmessage('rcOut procedure: Try to close');
                           rc_TF.free;  // release, close
                           //rc_TF.close;    // na free se ru�i, izgleda da je ovo bolje  
      end; // if rc_TF<>nil   
   end ; // if rcOut_enabled=true
end;


