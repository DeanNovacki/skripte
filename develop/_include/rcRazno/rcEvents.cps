Procedure rcCheckInt_Pos(sender:Tobject; var Key: Char); 
// allow only integer and positive when typing
// OnKeyPress
// var
  //// OldColor: TColor;
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key=#8)) then key:=#0;             

end; 

Procedure rcCheckInt(sender:Tobject; var Key: Char); 
// allow only integer typing
// OnKeyPress
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
          
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key='-') or
             (key=#45) or  (key=#8)) then key:=#0;              // 45=dash (minus), 8=backspace
end; 

Procedure rcCheckReal(sender:Tobject; var Key: Char); 
// allow only real number when typing
// OnKeyPress
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
          
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key='.') or                          
             (key='-') or (key=#8)) then key:=#0;              // 8=backspace
 end; 

Procedure rcCheckStrPrg(sender:Tobject; var Key: Char); 
// allow only international alphabet and numbers when typing (and some dummy proof characters)
// OnKeyPress
Var
  Valid_Char:set of char;
Begin
  Valid_Char:=[ '0','1','2','3','4','5','6','7','8','9',
                'a','b','c','d','e','f','g','h','i','j','k','l','m',
                'n','o','p','q','r','s','t','u','v','w','x','y','z',
                'A','B','C','D','E','F','G','H','I','J','K','L','M',
                'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                '!','#','$','%','&','(',')','=','+','-','@','�',' ','.','_',#8];
  if not (key in Valid_Char) then key:=#0;

end; 

Procedure rcCheckStrInt(sender:Tobject; var Key: Char); 
// allow only international alphabet and numbers when typing (and some dummy proof characters)
// OnKeyPress
Var
  Valid_Char:set of char;
Begin
  Valid_Char:=[ '0','1','2','3','4','5','6','7','8','9','*','/',
                'a','b','c','d','e','f','g','h','i','j','k','l','m',
                'n','o','p','q','r','s','t','u','v','w','x','y','z',
                'A','B','C','D','E','F','G','H','I','J','K','L','M',
                'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                '!','#','$','%','&','(',')','=','+','-','@','�',' ','.','_',#8];
  if not (key in Valid_Char) then key:=#0;

end; 