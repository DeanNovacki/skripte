// rcCurves /////////////////////////////

Procedure rcPointAdd(rc:TRubneTocke;ind:integer;px,py,pz:single;S:Integer);
// add point with index in in curve rc to position px, py, pz with Side S
var
   vec:Tvector3f;
Begin
   vec[0]:=px; vec[1]:=py; vec[2]:=pz;
   rc.AddNewTocka(ind,vec);
   rc.MainTocke[ind].xf:='';
   rc.MainTocke[ind].yf:='';
   rc.MainTocke[ind].zf:='';
   rc.MainTocke[ind].Fif:='';
   rc.MainTocke[ind].x:=py/1000;
   rc.MainTocke[ind].z:=px/1000;
   rc.MainTocke[ind].depth:=pz/1000;
   rc.MainTocke[ind].S:=S;
end;

Procedure rcPointPos(rc:TRubneTocke;ind:integer;px,py,pz:single);
// move point with index ind in curve rc to position px, py, pz
Begin
   //vec[0]:=px;vec[1]:=py;vec[2]:=pz;
   rc.MainTocke[ind].xf:='';
   rc.MainTocke[ind].yf:='';
   rc.MainTocke[ind].zf:='';
   rc.MainTocke[ind].Fif:='';
   rc.MainTocke[ind].x:=py/1000;
   rc.MainTocke[ind].z:=px/1000;
   rc.MainTocke[ind].depth:=pz/1000;
end;

Procedure rcPointPosF(rc:TRubneTocke;ind:integer;px,py,pz,pr:string);
// move point with index ind in curve rc to position by formula
// if string iz asterisk, value of formula wil not change
Begin
   if px<>'*' then rc.MainTocke[ind].xf:=py;
   if py<>'*' then rc.MainTocke[ind].yf:=px;
   if pz<>'*' then rc.MainTocke[ind].zf:=pz;
   if pr<>'*' then rc.MainTocke[ind].Fif:=pr;   // radius formula
end;

Procedure rcPointStyle(rc:TRubneTocke;ind:integer;st:TALineStyle);
// change style of Point Line
// Styles: rArc=radius,  rLine=line, rCurve=bezier
Begin
   rc.MainTocke[ind].AStyle:=st;
end;

Procedure rcPointArcData(rc            :TRubneTocke;     // curve
                         ind           :integer;         // index
                         rcStyle       :TArcStyle;       // ARplus, Arminus   (vanjski ili unutarnji krug)
                         rcArcCentar   :boolean);        // side
// change style of Arc
Begin
   rc.MainTocke[ind].ArcStyle:=rcStyle;
   rc.MainTocke[ind].ArcCentar:=rcArcCentar;
end;

Function rcPointReadX(rc:TRubneTocke;ind:integer):single;
// read X of Point
Begin
   result:=rc.MainTocke[ind].z*1000;
end;

Function rcPointReadY(rc:TRubneTocke;ind:integer):single;
// read Y of Point
Begin
   result:=rc.MainTocke[ind].x*1000;
end;

Procedure rcPointSegDens(rc:TRubneTocke;ind:integer;SegDens:Integer);
// Select number of segments by SegDens (Segments frequency per cm)
var
   
   count:Integer;
   x1,y1,x2,y2:Single;
   Distance:Single;
   Radius:Single;
   Angle:Single;
   Len:Single;
Begin
   // rc.owner.parent.recalcFormula;
   // if e<>nil then e.RecalcFormula(e);
   // find next point in curve
   count:=rc.Count;
   // ShowN('Nasao tocaka: '+IntToStr(Count));
   x1:=rcPointReadX(rc,ind);
   y1:=rcPointReadY(rc,ind);
   
   if (count-1)>ind then Begin                              // not last point
                              x2:= rcPointReadX(rc,ind+1);
                              y2:= rcPointReady(rc,ind+1)
                         end else begin                     // last point
                              x2:= rcPointReadX(rc,0);
                              y2:= rcPointReady(rc,0)
                         end; 
   radius:=rc.MainTocke[ind].ArcRadius*1000;
   //ShowN('Radius='+FloatToStr(Radius));
   Distance:=sqrt((x2-x1)*(x2-x1)+((y2-y1)*(y2-y1))); // distance between two points
   //ShowN('Distance='+FloatToStr(Distance));
   // Angle:=arcsin((Distance/2)/radius)*2;   // NE RADI DOBRO ARCSIN!!!
   Angle:=45;
   //ShowN('Angle='+FloatToStr(Angle));
   Len:=(3.14*radius*Angle/180);
   //ShowN('Length='+FloatToStr(Len));
   // ShowN('kut=45, sin(kut)='+FloatToStr(sin(45))+' kut='+FloatToStr(arcsin(sin(45))));  //  ??????????
   rc.MainTocke[ind].Segments:=Round(len*SegDens/10);

   //Showmessage('*');

end;


Function rcClearMainCurve(cur:TRubneTocke):TRubneTocke;
// delete all extra points in main curve of plate
// and move Main points to corners
var i,j:integer;
    ttn, tto:TTocka;   // new point, old point
    td:Tdaska;
Begin

   for i:=cur.count-1 downto 0 do begin    // backward count (deleting index)
      ttn:=cur.MainTocke[i];
      // ShowD('T('+IntToStr(i)+'/'+IntToStr(cur.count-1)+').S='+IntToStr(ttn.S));
      if i<cur.count-1 then begin          // if not 1st pass
             if abs(ttn.S-tto.S)<2 then begin  cur.ObrisiTocku(i+1);     // if equal delete old
             
                                 end;
         end else begin
             // 1st pass, tto not exist, nothing to compare  
      end; // if i<cur.count-1
      tto:=ttn;
   end;
   td:=cur.parentd;
   rcPointPos(cur,0,0,0,0);
   rcPointPos(cur,1,0,td.visina,0);
   rcPointPos(cur,2,td.dubina,td.visina,0);
   rcPointPos(cur,3,td.dubina,0,0);   

End;

Function rcCurVarEx(Curve:TTockeSystem;VarName:String):Boolean;
// check if variable exist
Begin
   If Curve.varijable.isvar(VarName) then result:=true
                                     else result:=false;
End;

Function rcCurVarVal(Curve:TTockeSystem;VarName:String):String;
// return Varijable value in curve system
Begin
   If Curve.varijable.isvar(VarName) then begin
               result:=Curve.varijable.Values[VarName]
            end else Showmessage('Variable not exist in this Curve System');
End;

Procedure rcCurVarAdd(curve:TTockeSystem;VarName,VarValue:String);
// If Variable exist then change value, if not then add variable name and value
Begin
   If Curve.varijable.isvar(VarName) then Curve.varijable.setvarvalue(VarName,VarValue)
                                     else Curve.varijable.add(VarName+'='+VarValue);
End;



