Program Pogled;   


// MAIN ////////////////////////////////////////////////////////////////////////
var
   // Startni prozor
	Main:TForm;
	Pan_cap,
   Pan_top, Pan_svi,
	Pan_pod, Pan_vis, Pan_rad, 
	Pan_cok, Pan_kut, Pan_str,
	Pan_zid, Pan_pla, Pan_res, 
	Pan_txt : TPanel;
	
			  SL_svi_V0, SL_svi_V1, SL_svi_S0, SL_svi_S1, 
	SL_pod, SL_pod_V0, SL_pod_V1, SL_pod_S0, SL_pod_S1, 
	SL_vis, SL_vis_V0, SL_vis_V1, SL_vis_S0, SL_vis_S1, 
	SL_rad, SL_rad_V0, SL_rad_V1, SL_rad_S0, SL_rad_S1, 
	SL_cok, SL_cok_V0, SL_cok_V1, SL_cok_S0, SL_cok_S1, 
	SL_kut, SL_kut_V0, SL_kut_V1, SL_kut_S0, SL_kut_S1, 
	SL_str, SL_str_V0, SL_str_V1, SL_str_S0, SL_str_S1, 
	SL_zid, SL_zid_V0, SL_zid_V1, SL_zid_S0, SL_zid_S1, 
	SL_pla, SL_pla_V0, SL_pla_V1, SL_pla_S0, SL_pla_S1, 
	SL_res, SL_res_V0, SL_res_V1, SL_res_S0, SL_res_S1,
	SL_pomoc, SL_vidi_sve, SL_Sakrij_sve	: TImage;
	

	Opis : TLabel;
	bx, Pomoc : TButton;
	desni_rub : Integer;	// gdje po�inje rc toolbar
	put : string; // folder skripte
	
	// shield
	Shield:TForm;
	
{$I ..\..\include\rcMessages.cps}
{$I ..\..\include\rcPrint.cps}
{$I ..\..\include\rcStrings.cps}
{$I ..\..\include\rcControls.cps}
{$I ..\..\include\rcObjects.cps}
{$I ..\..\include\rcEvents.cps}
{$I ..\..\include\rcCurves.cps}
{$I ..\..\include\rcMath.cps}


Procedure Sakrij2(tip1, tip2 :integer);
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc Sakrij2 started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]);   
		// Print ('i: '+IntToStr(i)+' E: '+elm.naziv);
		// elm.selected:=false;
      
		if ( elm.visible ) then begin 
			if ( integer(elm.tipelementa) = tip1 ) or ( integer(elm.tipelementa) = tip2 ) then begin
            // Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + 
				// 		' tip: ' + IntToStr (integer(elm.tipelementa)) );
				elm.visible:=False;
				// promjena varijable za sakrivanje
				if elm.fvarijable.isvar('Hide') then elm.fvarijable.setvarvalue('Hide','1+0');
				// dodavanje varijable za oznaku sakrivanja ako ne postoji
				if not elm.fvarijable.isvar('Hide') then elm.fvarijable.add('Hide=1+0');
				
				rcRefreshElm(elm);
				
			end; 	// if ( integer(elm.tipelementa) = ... 		
      end;  // if elm.visible...
   
	end; // for
		
End;

Procedure SakrijSveZajedno;
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc SakrijSveZajedno started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]);   
		// Print ('i: '+IntToStr(i)+' E: '+elm.naziv);
		// elm.selected:=false;
      
		if ( elm.visible ) then begin 
			// if ( integer(elm.tipelementa) = tip1 ) or ( integer(elm.tipelementa) = tip2 ) then begin
            // Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + 
				// 		' tip: ' + IntToStr (integer(elm.tipelementa)) );
				elm.visible:=False;
				// promjena varijable za sakrivanje
				if elm.fvarijable.isvar('Hide') then elm.fvarijable.setvarvalue('Hide','1+0');
				// dodavanje varijable za oznaku sakrivanja ako ne postoji
				if not elm.fvarijable.isvar('Hide') then elm.fvarijable.add('Hide=1+0');
				
				rcRefreshElm(elm);
				
			// end; 	// if ( integer(elm.tipelementa) = ... 		
      end;  // if elm.visible...
   
	end; // for
		
End;


Procedure SakrijLampeVar;
// Sakriva elemente koji imaju varijablu Light
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc Sakrij2 started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]);   
		// Print ('i: '+IntToStr(i)+' E: '+elm.naziv);
		// elm.selected:=false;
      
		if ( elm.visible ) then begin 
			if elm.fvarijable.isvar('Light')  then begin
            // Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + 
				// 		' tip: ' + IntToStr (integer(elm.tipelementa)) );
				elm.visible:=False;
				// promjena varijable za sakrivanje
				if elm.fvarijable.isvar('Hide') then elm.fvarijable.setvarvalue('Hide','1+0');
				// dodavanje varijable za oznaku sakrivanja ako ne postoji
				if not elm.fvarijable.isvar('Hide') then elm.fvarijable.add('Hide=1+0');
				
				rcRefreshElm(elm);
				
			end; 	// if ( integer(elm.tipelementa) = ... 		
      end;  // if elm.visible...
   
	end; // for
		
End;

Procedure SakrijPlafon;
// Sakriva Plafon (Zid koji ima z ve�i od 1000)
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc SakrijPlafon started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]);   
		// Print ('i: '+IntToStr(i)+' E: '+elm.naziv);
		// elm.selected:=false;
      
		if ( elm.visible ) then begin 
			if ( elm.YPos>900 ) and ( integer(elm.TipElementa)=12 ) then begin
            // Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + 
				// 		' tip: ' + IntToStr (integer(elm.tipelementa)) );
				elm.visible:=False;
				// promjena varijable za sakrivanje
				if elm.fvarijable.isvar('Hide') then elm.fvarijable.setvarvalue('Hide','1+0');
				// dodavanje varijable za oznaku sakrivanja ako ne postoji
				if not elm.fvarijable.isvar('Hide') then elm.fvarijable.add('Hide=1+0');
				
				rcRefreshElm(elm);
				
			end; 	// if ( integer(elm.tipelementa) = ... 		
      end;  // if elm.visible...
   
	end; // for
		
End;

Procedure SakrijZid;
// Sakriva Plafon (Zid koji ima z ve�i od 1000)
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc SakrijPlafon started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]);   
		// Print ('i: '+IntToStr(i)+' E: '+elm.naziv);
		// elm.selected:=false;
      
		if ( elm.visible ) then begin 
			if ( elm.YPos<901 ) and ( integer(elm.TipElementa)=12 ) then begin
            // Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + 
				// 		' tip: ' + IntToStr (integer(elm.tipelementa)) );
				elm.visible:=False;
				// promjena varijable za sakrivanje
				if elm.fvarijable.isvar('Hide') then elm.fvarijable.setvarvalue('Hide','1+0');
				// dodavanje varijable za oznaku sakrivanja ako ne postoji
				if not elm.fvarijable.isvar('Hide') then elm.fvarijable.add('Hide=1+0');
				
				rcRefreshElm(elm);
				
			end; 	// if ( integer(elm.tipelementa) = ... 		
      end;  // if elm.visible...
   
	end; // for
		
End;


Procedure Pokazi2(tip1, tip2 :integer);
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc Pokazi2 started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]); 
		if 	( elm.fvarijable.isvar('Hide') ) and 
				( elm.fvarijable.Parsevar('Hide') = 1 ) then Begin
				if ( elm.visible=false ) and 
					( ( integer(elm.TipElementa) = tip1 ) or ( integer(elm.tipelementa) = tip2 ) ) then begin
						// Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + ' tip: ' + IntToStr (integer(elm.tipelementa)) );
						elm.visible:=True;
						// brisanje varijable za oznaku sakrivanja

						rcVarDelete(elm,'Hide');
						
						rcRefreshElm(elm);
				end; 	// if ( elm.visible=false ) and ...
      end;  // ( elm.fvarijable.isvar('Hide') ) and ...
   
	end; // for
	// rcRefresh;	
End;

Procedure PokaziSve;
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc Pokazi2 started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]); 
		if 	( elm.fvarijable.isvar('Hide') ) and 
				( elm.fvarijable.Parsevar('Hide') = 1 ) then Begin
				if ( elm.visible=false ) // and 
					// ( ( integer(elm.TipElementa) = tip1 ) or ( integer(elm.tipelementa) = tip2 ) ) 
					then begin
						// Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + ' tip: ' + IntToStr (integer(elm.tipelementa)) );
						elm.visible:=True;
						// brisanje varijable za oznaku sakrivanja

						rcVarDelete(elm,'Hide');
						
						rcRefreshElm(elm);
				end; 	// if ( elm.visible=false ) and ...
      end;  // ( elm.fvarijable.isvar('Hide') ) and ...
   
	end; // for
	// rcRefresh;	
End;


Procedure PokaziPlafon;
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc Pokazi2 started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]); 
		if 	( elm.fvarijable.isvar('Hide') ) and 
				( elm.fvarijable.Parsevar('Hide') = 1 ) then Begin
				if ( elm.Ypos>900 ) and ( integer(elm.TipElementa) = 12 ) then begin
						// Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + ' tip: ' + IntToStr (integer(elm.tipelementa)) );
						elm.visible:=True;
						// brisanje varijable za oznaku sakrivanja

						rcVarDelete(elm,'Hide');
						
						rcRefreshElm(elm);
				end; 	// if ( elm.visible=false ) and ...
      end;  // ( elm.fvarijable.isvar('Hide') ) and ...
   
	end; // for
	// rcRefresh;	
End;

Procedure PokaziZid;
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc Pokazi2 started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]); 
		if 	( elm.fvarijable.isvar('Hide') ) and 
				( elm.fvarijable.Parsevar('Hide') = 1 ) then Begin
				if ( elm.Ypos<901 ) and ( integer(elm.TipElementa) = 12 ) then begin
						// Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + ' tip: ' + IntToStr (integer(elm.tipelementa)) );
						elm.visible:=True;
						// brisanje varijable za oznaku sakrivanja

						rcVarDelete(elm,'Hide');
						
						rcRefreshElm(elm);
				end; 	// if ( elm.visible=false ) and ...
      end;  // ( elm.fvarijable.isvar('Hide') ) and ...
   
	end; // for
	// rcRefresh;	
End;

Procedure PokaziLampeVar;
Var
	i:Integer;
	elm:Telement;
Begin
	// Print ('Proc PokaziLampeVar started')
	for i:=0 to ElmHolder.elementi.count-1 do Begin 
      elm:=telement(ElmHolder.elementi.items[i]); 
		if 	( elm.fvarijable.isvar('Hide') ) and 
				( elm.fvarijable.Parsevar('Hide') = 1 ) then Begin
				if ( elm.visible=false ) and ( elm.fvarijable.isvar('Light') ) then begin
						// Print ('i:'+IntToStr(i)+'; E: '+elm.naziv + ' tip: ' + IntToStr (integer(elm.tipelementa)) );
						elm.visible:=True;
						// brisanje varijable za oznaku sakrivanja

						rcVarDelete(elm,'Hide');
						
						rcRefreshElm(elm);
				end; 	// if ( elm.visible=false ) and ...
      end;  // ( elm.fvarijable.isvar('Hide') ) and ...
   
	end; // for
	// rcRefresh;	
End;

procedure SL_OnClickHide(Sender: TObject);
// Event Sakrij On Click 
begin

	If TMImage(sender).tag=10 then Begin // Sakrij sve

		SakrijSveZajedno;		

		SL_pod_V0.visible:=True;
		SL_vis_V0.visible:=True;
		SL_rad_V0.visible:=True;
		SL_cok_V0.visible:=True;
		SL_kut_V0.visible:=True;
		SL_str_V0.visible:=True;
		SL_zid_V0.visible:=True;
		SL_pla_V0.visible:=True;
		SL_res_V0.visible:=True;
		
		SL_pod_S0.visible:=False;
		SL_vis_S0.visible:=False;
		SL_rad_S0.visible:=False;
		SL_cok_S0.visible:=False;
		SL_kut_S0.visible:=False;
		SL_str_S0.visible:=False;
		SL_zid_S0.visible:=False;
		SL_pla_S0.visible:=False;
		SL_res_S0.visible:=False;
		
	End;


	If TMImage(sender).tag=11 then Begin // Sakrij Podne, Podne kutne, kolone
		Sakrij2(2,3);		
		Sakrij2(15,15);		
		SL_pod_S0.visible:=False;
		SL_pod_V0.visible:=True;
	End;
	
	If TMImage(sender).tag=12 then Begin // Sakrij Visece, Visece kutne
		Sakrij2(4,5);		
		SL_vis_S0.visible:=False;
		SL_vis_V0.visible:=True;
	End;
	
	If TMImage(sender).tag=13 then Begin // Sakrij Visece, Visece kutne
		Sakrij2(8,8);		
		SL_rad_S0.visible:=False;
		SL_rad_V0.visible:=True;
	End;
	
	If TMImage(sender).tag=14 then Begin // Sakrij Cokle
		Sakrij2(19,19);		
		SL_cok_S0.visible:=False;
		SL_cok_V0.visible:=True;
	End;
	
	If TMImage(sender).tag=15 then Begin // Kutne Lajsne
		Sakrij2(20,20);		
		SL_kut_S0.visible:=False;
		SL_kut_V0.visible:=True;
	End;
	
	If TMImage(sender).tag=16 then Begin // Stropna ploha
		Sakrij2(22,22);		
		SL_str_S0.visible:=False;
		SL_str_V0.visible:=True;
	End;
	
	If TMImage(sender).tag=17 then Begin // Zidovi i lampe
		Sakrij2(11,11);   // Ako je tip Lampa
		SakrijZid;
		SakrijLampeVar;
		SL_zid_S0.visible:=False;
		SL_zid_V0.visible:=True;
	End;

	If TMImage(sender).tag=18 then Begin // Plafon
		SakrijPlafon;
		SL_pla_S0.visible:=False;
		SL_pla_V0.visible:=True;
	End;	

	If TMImage(sender).tag=19 then Begin // Ostalo
		Sakrij2(0,1);	 	// Moj element?, Import? 
		Sakrij2(9,10);		// Vrata, Prozor, 
		Sakrij2(13,21);	// Corpus Aparat,  Bo�na ploha
		Sakrij2(23,24);	// Podna ploha, Dekoracija
		SL_res_S0.visible:=False;
		SL_res_V0.visible:=True;
	End;	
	
	
end;    

procedure SL_OnClickShow(Sender: TObject);
// Event Poka�i On Click 
begin
	
	If TMImage(sender).tag=20 then Begin // Poka�i Sve

		PokaziSve;		

		SL_pod_V0.visible:=False;
		SL_vis_V0.visible:=False;
		SL_rad_V0.visible:=False;
		SL_cok_V0.visible:=False;
		SL_kut_V0.visible:=False;
		SL_str_V0.visible:=False;
		SL_zid_V0.visible:=False;
		SL_pla_V0.visible:=False;
		SL_res_V0.visible:=False;
		
		SL_pod_S0.visible:=True;
		SL_vis_S0.visible:=True;
		SL_rad_S0.visible:=True;
		SL_cok_S0.visible:=True;
		SL_kut_S0.visible:=True;
		SL_str_S0.visible:=True;
		SL_zid_S0.visible:=True;
		SL_pla_S0.visible:=True;
		SL_res_S0.visible:=True;
		
	End;

	If TMImage(sender).tag=21 then Begin // Poka�i Podne, Podne kutne, Kolone
		Pokazi2(2,3);		
		Pokazi2(15,15);		
		SL_pod_S0.visible:=True;
		SL_pod_V0.visible:=False;
	End;
	
	If TMImage(sender).tag=22 then Begin // Poka�i Visece, visece kutne
		Pokazi2(4,5);		
		SL_vis_S0.visible:=True;
		SL_vis_V0.visible:=False;
	End;
	
	If TMImage(sender).tag=23 then Begin // Poka�i Visece, visece kutne
		Pokazi2(8,8);		
		SL_rad_S0.visible:=True;
		SL_rad_V0.visible:=False;
	End;
	
	If TMImage(sender).tag=24 then Begin // Poka�i Visece, visece kutne
		Pokazi2(19,19);		
		SL_cok_S0.visible:=True;
		SL_cok_V0.visible:=False;
	End;
	
	If TMImage(sender).tag=25 then Begin // Poka�i Kutne letve
		Pokazi2(20,20);		
		SL_kut_S0.visible:=True;
		SL_kut_V0.visible:=False;
	End;
	
	If TMImage(sender).tag=26 then Begin // Poka�i Stropne plohe
		Pokazi2(22,22);		
		SL_str_S0.visible:=True;
		SL_str_V0.visible:=False;
	End;
	
	If TMImage(sender).tag=27 then Begin // Zidovi i Lampe
		Pokazi2(11,11);  	// AKo je tip Lampa	
		PokaziLampeVar;
		PokaziZid;
		SL_zid_S0.visible:=True;
		SL_zid_V0.visible:=False;
	End;
	
	If TMImage(sender).tag=28 then Begin // Plafon
		PokaziPlafon;		
		SL_pla_S0.visible:=True;
		SL_pla_V0.visible:=False;
	End;
	
	If TMImage(sender).tag=29 then Begin // Ostalo
		Pokazi2(0,1);	 	// Moj element?, Import? 
		Pokazi2(9,10);		// Vrata, Prozor, 
		Pokazi2(13,21);	// Corpus Aparat,  Bo�na ploha
		Pokazi2(23,24);	// Podna ploha, Dekoracija
		SL_res_S0.visible:=True;
		SL_res_V0.visible:=False;
	End;	
		
	
	
end; 

procedure SL_OnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
// Captioni
begin 
	If TMImage(sender).tag=1 then Opis.caption:='Podni elementi'; 
	If TMImage(sender).tag=2 then Opis.caption:='Vise�i elementi'; 
	If TMImage(sender).tag=3 then Opis.caption:='Radne plo�e'; 
	If TMImage(sender).tag=4 then Opis.caption:='Cokle'; 
	If TMImage(sender).tag=5 then Opis.caption:='Kutne lajsne'; 
	If TMImage(sender).tag=6 then Opis.caption:='Stropne plohe'; 
	If TMImage(sender).tag=7 then Opis.caption:='Zidovi'; 
	If TMImage(sender).tag=8 then Opis.caption:='Strop'; 
	If TMImage(sender).tag=9 then Opis.caption:='Sve ostalo'; 
	
	If TMImage(sender).tag=10 then Opis.caption:='Sakrij sve'; 
	If TMImage(sender).tag=20 then Opis.caption:='Poka�i sve'; 
	
	If TMImage(sender).tag=11 then Opis.caption:='Sakrij podne'; 
	If TMImage(sender).tag=12 then Opis.caption:='Sakrij vise�e'; 
	If TMImage(sender).tag=13 then Opis.caption:='Sakrij radne plo�e'; 
	If TMImage(sender).tag=14 then Opis.caption:='Sakrij cokle'; 
	If TMImage(sender).tag=15 then Opis.caption:='Sakrij lajsne'; 
	If TMImage(sender).tag=16 then Opis.caption:='Sakrij stopne'; 
	If TMImage(sender).tag=17 then Opis.caption:='Sakrij zidove i lampe'; 
	If TMImage(sender).tag=18 then Opis.caption:='Sakrij strop'; 
	If TMImage(sender).tag=19 then Opis.caption:='Sakrij ostalo'; 
	
	If TMImage(sender).tag=21 then Opis.caption:='Poka�i podne'; 
	If TMImage(sender).tag=22 then Opis.caption:='Poka�i vise�e'; 
	If TMImage(sender).tag=23 then Opis.caption:='Poka�i radne plo�e'; 
	If TMImage(sender).tag=24 then Opis.caption:='Poka�i cokle'; 
	If TMImage(sender).tag=25 then Opis.caption:='Poka�i kutne lajsne'; 
	If TMImage(sender).tag=26 then Opis.caption:='Poka�i stopne'; 
	If TMImage(sender).tag=27 then Opis.caption:='Poka�i zidove i lampe'; 
	If TMImage(sender).tag=28 then Opis.caption:='Poka�i strop'; 
	If TMImage(sender).tag=29 then Opis.caption:='Poka�i ostalo'; 
	
end; 

procedure OnCorpusClose(Sender: TObject; var CanClose: boolean); 
// aktivira se netom prije zatvaranja prozora, a mo�da i ne :-)   
begin
	// Print ('OnCorpusClose');
	Main.visible:=False;   // ako je Show
	
	// Main.free;

end;

procedure OnCorpusClose2(Sender: TObject; var CloseAction: TCloseAction);
begin

	Main.visible:=False;   // ako je Show

end; 

procedure bxOnClick(Sender: TObject);
begin
	
	Main.visible:=False;
	
	{
	print ('Izlaz? :-)');
	Main.width:=0;
	Main.Height:=0;
	}
end; 



	
procedure OnCorpusResize(Sender: TObject);
begin
	shield.left:=application.MainForm.Left+application.MainForm.width-shield.width-7;
	shield.Top:=application.MainForm.Top;
end;	

procedure ShieldOnClick(Sender: TObject);
begin
	
	ShowMessage('Prije ga�enja Corpusa ili' + #13#10+
					'otvaranja novog projekta' + #13#10+
					'obavezno ugasite skriptu "Sakrivanje"');
end; 
	
procedure make_shield;
// ne dozvoljava ga�enje Corpusa ili resize
var
	slikica:TImage;
	
Begin
	// Print('procedura make_shield start');
	shield:=rcTool('Pogled');
	shield.BorderStyle:=bsNone;
	shield.Position:=poDesigned; 		   // sad radi TF.Top i TF.Left
	shield.FormStyle:=fsStayOnTop;
	shield.Width:=140; 
	shield.Height:=50;
	shield.left:=application.MainForm.Left+application.MainForm.width-shield.width-7;
	shield.Top:=application.MainForm.Top;
	Shield.color:=clWindow;
	Shield.OnClick:=@ShieldOnClick;
	
	If not FileExists(put+'shield.jpg') then Showmessage('nema slike "'+put+'shield.jpg')
													else Slikica:=rcLoadImage('svikica', put+'shield.jpg', Shield, 0, 0);

	Slikica.hint:='Prije ga�enja Corpusa ili' + #13#10+
					  'otvaranja novog projekta' + #13#10+
					  'obavezno ugasite skriptu "Sakrivanje"'
					
	Slikica.OnClick:=@ShieldOnClick;				
		
	// Print('procedura make_shield end');
End;	
	
	
	
Procedure kreiraj_formu;

Begin
	Main:=rcTool('Pogled');
	Main.caption:='Sakrivanje';
	Main.BorderStyle:=bsDialog;
	// Main.BorderStyle:=bsNone;
	Main.FormStyle:=fsStayOnTop;
	Main.Position:=poDesigned; 		   // sad radi TF.Top i TF.Left
	Main.Width:=170; Main.Height:=612;
	Main.left:=Desni_rub-Main.width+application.MainForm.left;
	Main.Top:=application.MainForm.Top+230;
	
	
	// Caption Panel
	Pan_cap:=rcPanel('Pogled',main);
	Pan_cap.Top:=2000;
	Pan_cap.height:=25;
	Pan_cap.align:=alTop;
	
	
	
	bx:=rcButton('x','x',Main, 145, 2);
	bx.width:=20;
	bx.Height:=20;
	
	bx.OnClick:=@bxOnClick;
	
	Pan_cap.visible:=False;   // ***************************************
	
	// Text Panel
	Pan_txt:=rcPanel('ptop',main);
	Pan_txt.Top:=2000;
	Pan_txt.height:=25;
	Pan_txt.align:=alTop;
	
	Opis:=rcLabel('opis', 'Sakrivanje elemenata', Pan_txt,3,4,taLeftJustify);
	Opis.font.size:=11;
	
	// Panel svi
	Pan_svi:=rcPanel('psvi',main);
	Pan_svi.Top:=2000;
	Pan_svi.height:=18;
	Pan_svi.align:=alTop;
	
		// Slike za sve
		SL_pomoc:=rcLoadImage('pomoc', put+'pomoc.jpg', Pan_svi, 2, 2);
		
		// Slike za sve
		SL_sakrij_sve:=rcLoadImage('sve1', put+'odaberi_sve.jpg', Pan_svi, 62, 2);
		SL_vidi_sve:=rcLoadImage('sve2', put+'odaberi_sve.jpg', Pan_svi, 112, 2);
	
	// Panel Podni
	Pan_pod:=rcPanel('ppod',main);
	Pan_pod.Top:=2000;
	Pan_pod.height:=60;
	Pan_pod.align:=alTop;
	
		// Slike
		If not FileExists(put+'podni.jpg') then Showmessage('nema slike "'+put+'podni.jpg');
		SL_pod:=rcLoadImage('podni', put+'podni.jpg', Pan_pod, 1, 1);
		SL_pod_S1:=rcLoadImage('podni_v1', put+'skriven.jpg', Pan_pod, 58, 1);
		SL_pod_V1:=rcLoadImage('podni_s1', put+'vidi_se.jpg', Pan_pod, 118, 1);
		SL_pod_S0:=rcLoadImage('podni_v0', put+'nije_skriven.jpg', Pan_pod, 58, 1);
		SL_pod_V0:=rcLoadImage('podni_s0', put+'ne_vidi_se.jpg', Pan_pod, 118, 1);

	
	// Panel vise�i
	Pan_vis:=rcPanel('pvis',main);
	Pan_vis.Top:=2000;
	Pan_vis.height:=60;
	Pan_vis.align:=alTop;
	
		// Slike
		If not FileExists(put+'viseci.jpg') then Showmessage('nema slike "'+put+'viseci.jpg');
		SL_vis:=rcLoadImage('viseci', put+'viseci.jpg', 				Pan_vis, 1, 1);
		SL_vis_S1:=rcLoadImage('viseci_v1', put+'skriven.jpg', 		Pan_vis, 58, 1);
		SL_vis_V1:=rcLoadImage('viseci_s1', put+'vidi_se.jpg', 		Pan_vis, 118, 1);
		SL_vis_S0:=rcLoadImage('viseci_v0', put+'nije_skriven.jpg', Pan_vis, 58, 1);
		SL_vis_V0:=rcLoadImage('viseci_s0', put+'ne_vidi_se.jpg', 	Pan_vis, 118, 1);
	      
	// Panel radne plo�e
	Pan_rad:=rcPanel('prad',main);
	Pan_rad.Top:=2000;
	Pan_rad.height:=60;
	Pan_rad.align:=alTop;
	
		// Slike
		If not FileExists(put+'radna_ploca.jpg') then Showmessage('nema slike "'+put+'radna_ploca.jpg');
		SL_rad:=rcLoadImage('radplo', put+'radna_ploca.jpg', 			Pan_rad, 1, 1);
		SL_rad_S1:=rcLoadImage('radnap_v1', put+'skriven.jpg', 		Pan_rad, 58, 1);
		SL_rad_V1:=rcLoadImage('radnap_s1', put+'vidi_se.jpg', 		Pan_rad, 118, 1);
		SL_rad_S0:=rcLoadImage('radnap_v0', put+'nije_skriven.jpg', Pan_rad, 58, 1);
		SL_rad_V0:=rcLoadImage('radnap_s0', put+'ne_vidi_se.jpg', 	Pan_rad, 118, 1);

	// Panel cokle
	Pan_cok:=rcPanel('pcok',main);
	Pan_cok.Top:=2000;
	Pan_cok.height:=60;
	Pan_cok.align:=alTop;
	
		// Slike
		If not FileExists(put+'cokla.jpg') then Showmessage('nema slike "'+put+'cokla.jpg');
		SL_cok:=rcLoadImage	 ('cok1', put+'cokla.jpg', 		Pan_cok, 1, 1);
		SL_cok_S1:=rcLoadImage('cok1_v1', put+'skriven.jpg', 			Pan_cok, 58, 1);
		SL_cok_V1:=rcLoadImage('cok1_s1', put+'vidi_se.jpg', 			Pan_cok, 118, 1);
		SL_cok_S0:=rcLoadImage('cok1_v0', put+'nije_skriven.jpg', 	Pan_cok, 58, 1);
		SL_cok_V0:=rcLoadImage('cok1_s0', put+'ne_vidi_se.jpg', 		Pan_cok, 118, 1);	
		
	// Panel kutne lajsne
	Pan_kut:=rcPanel('pkut',main);
	Pan_kut.Top:=2000;
	Pan_kut.height:=60;
	Pan_kut.align:=alTop;
	
		// Slike
		If not FileExists(put+'rubna_letva.jpg') then Showmessage('nema slike "'+put+'rubna_letva.jpg');
		SL_kut:=rcLoadImage	 ('kut1', put+'rubna_letva.jpg', 		Pan_kut, 1, 1);
		SL_kut_S1:=rcLoadImage('kut1_v1', put+'skriven.jpg', 			Pan_kut, 58, 1);
		SL_kut_V1:=rcLoadImage('kut1_s1', put+'vidi_se.jpg', 			Pan_kut, 118, 1);
		SL_kut_S0:=rcLoadImage('kut1_v0', put+'nije_skriven.jpg', 	Pan_kut, 58, 1);
		SL_kut_V0:=rcLoadImage('kut1_s0', put+'ne_vidi_se.jpg', 		Pan_kut, 118, 1);		
		
	// Panel stropne plohe
	Pan_str:=rcPanel('pstr',main);
	Pan_str.Top:=2000;
	Pan_str.height:=60;
	Pan_str.align:=alTop;
	
		// Slike
		If not FileExists(put+'stropna_ploha.jpg') then Showmessage('nema slike "'+put+'stropna_ploha.jpg');
		SL_str:=rcLoadImage	 ('str1', put+'stropna_ploha.jpg', 		Pan_str, 1, 1);
		SL_str_S1:=rcLoadImage('str1_v1', put+'skriven.jpg', 			Pan_str, 58, 1);
		SL_str_V1:=rcLoadImage('str1_s1', put+'vidi_se.jpg', 			Pan_str, 118, 1);
		SL_str_S0:=rcLoadImage('str1_v0', put+'nije_skriven.jpg', 	Pan_str, 58, 1);
		SL_str_V0:=rcLoadImage('str1_s0', put+'ne_vidi_se.jpg', 		Pan_str, 118, 1);			
	
	// Panel zidovi
	Pan_zid:=rcPanel('pzid',main);
	Pan_zid.Top:=2000;
	Pan_zid.height:=60;
	Pan_zid.align:=alTop;
	
		// Slike
		If not FileExists(put+'zidovi.jpg') then Showmessage('nema slike "'+put+'zidovi.jpg');
		SL_zid:=rcLoadImage	 ('zid1', put+'zidovi.jpg', 				Pan_zid, 1, 1);
		SL_zid_S1:=rcLoadImage('zid1_v1', put+'skriven.jpg', 			Pan_zid, 58, 1);
		SL_zid_V1:=rcLoadImage('zid1_s1', put+'vidi_se.jpg', 			Pan_zid, 118, 1);
		SL_zid_S0:=rcLoadImage('zid1_v0', put+'nije_skriven.jpg', 	Pan_zid, 58, 1);
		SL_zid_V0:=rcLoadImage('zid1_s0', put+'ne_vidi_se.jpg', 		Pan_zid, 118, 1);
		
	// Panel plafon
	Pan_pla:=rcPanel('pplafon',main);
	Pan_pla.Top:=2000;
	Pan_pla.height:=60;
	Pan_pla.align:=alTop;
	
		// Slike
		If not FileExists(put+'plafon.jpg') then Showmessage('nema slike "'+put+'plafon.jpg');
		SL_pla:=rcLoadImage	 ('pla1', put+'plafon.jpg', 				Pan_pla, 1, 1);
		SL_pla_S1:=rcLoadImage('pla1_v1', put+'skriven.jpg', 			Pan_pla, 58, 1);
		SL_pla_V1:=rcLoadImage('pla1_s1', put+'vidi_se.jpg', 			Pan_pla, 118, 1);
		SL_pla_S0:=rcLoadImage('pla1_v0', put+'nije_skriven.jpg', 	Pan_pla, 58, 1);
		SL_pla_V0:=rcLoadImage('pla1_s0', put+'ne_vidi_se.jpg', 		Pan_pla, 118, 1);	
		
	// Panel ostalo
	Pan_res:=rcPanel('pres',main);
	Pan_res.Top:=2000;
	Pan_res.height:=60;
	Pan_res.align:=alTop;
	
		// Slike
		If not FileExists(put+'ostalo.jpg') then Showmessage('nema slike "'+put+'ostalo.jpg');
		SL_res:=rcLoadImage	 ('res1', put+'ostalo.jpg', 				Pan_res, 1, 1);
		SL_res_S1:=rcLoadImage('res1_v1', put+'skriven.jpg', 			Pan_res, 58, 1);
		SL_res_V1:=rcLoadImage('res1_s1', put+'vidi_se.jpg', 			Pan_res, 118, 1);
		SL_res_S0:=rcLoadImage('res1_v0', put+'nije_skriven.jpg', 	Pan_res, 58, 1);
		SL_res_V0:=rcLoadImage('res1_s0', put+'ne_vidi_se.jpg', 		Pan_res, 118, 1);	
		
	// Default slike za kolonu poka�i
	
	SL_pod_V0.visible:=False;
	SL_vis_V0.visible:=False;
	SL_rad_V0.visible:=False;
	SL_cok_V0.visible:=False;
	SL_kut_V0.visible:=False;
	SL_str_V0.visible:=False;
	SL_zid_V0.visible:=False;
	SL_pla_V0.visible:=False;
	SL_res_V0.visible:=False;
		
	
	// On Mouse over
	
	SL_pod.tag:=1; SL_pod.OnMouseMove:=@SL_OnMouseMove;
	SL_vis.tag:=2;	SL_vis.OnMouseMove:=@SL_OnMouseMove;
	SL_rad.tag:=3;	SL_rad.OnMouseMove:=@SL_OnMouseMove;
	SL_cok.tag:=4;	SL_cok.OnMouseMove:=@SL_OnMouseMove;
	SL_kut.tag:=5;	SL_kut.OnMouseMove:=@SL_OnMouseMove;
	SL_str.tag:=6;	SL_str.OnMouseMove:=@SL_OnMouseMove;
	SL_zid.tag:=7;	SL_zid.OnMouseMove:=@SL_OnMouseMove;
	SL_pla.tag:=8;	SL_pla.OnMouseMove:=@SL_OnMouseMove;
	SL_res.tag:=9;	SL_res.OnMouseMove:=@SL_OnMouseMove;
	
	SL_sakrij_sve.tag:=10;	SL_sakrij_sve.OnMouseMove:=@SL_OnMouseMove;
	SL_vidi_sve.tag:=20;		SL_vidi_sve.OnMouseMove:=@SL_OnMouseMove;
	
	
	SL_pod_V0.tag:=21; SL_pod_V0.OnMouseMove:=@SL_OnMouseMove;
	SL_vis_V0.tag:=22; SL_vis_V0.OnMouseMove:=@SL_OnMouseMove;
   SL_rad_V0.tag:=23; SL_rad_V0.OnMouseMove:=@SL_OnMouseMove;
   SL_cok_V0.tag:=24; SL_cok_V0.OnMouseMove:=@SL_OnMouseMove;
   SL_kut_V0.tag:=25; SL_kut_V0.OnMouseMove:=@SL_OnMouseMove;
   SL_str_V0.tag:=26; SL_str_V0.OnMouseMove:=@SL_OnMouseMove;
   SL_zid_V0.tag:=27; SL_zid_V0.OnMouseMove:=@SL_OnMouseMove;
	SL_pla_V0.tag:=28; SL_pla_V0.OnMouseMove:=@SL_OnMouseMove;
   SL_res_V0.tag:=29; SL_res_V0.OnMouseMove:=@SL_OnMouseMove;
	
	SL_pod_V1.tag:=21; SL_pod_V1.OnMouseMove:=@SL_OnMouseMove;
	SL_vis_V1.tag:=22; SL_vis_V1.OnMouseMove:=@SL_OnMouseMove;
   SL_rad_V1.tag:=23; SL_rad_V1.OnMouseMove:=@SL_OnMouseMove;
   SL_cok_V1.tag:=24; SL_cok_V1.OnMouseMove:=@SL_OnMouseMove;
   SL_kut_V1.tag:=25; SL_kut_V1.OnMouseMove:=@SL_OnMouseMove;
   SL_str_V1.tag:=26; SL_str_V1.OnMouseMove:=@SL_OnMouseMove;
   SL_zid_V1.tag:=27; SL_zid_V1.OnMouseMove:=@SL_OnMouseMove;
   SL_pla_V1.tag:=28; SL_pla_V1.OnMouseMove:=@SL_OnMouseMove;
	SL_res_V1.tag:=29; SL_res_V1.OnMouseMove:=@SL_OnMouseMove;
	
	SL_pod_S0.tag:=11; SL_pod_S0.OnMouseMove:=@SL_OnMouseMove;
	SL_vis_S0.tag:=12; SL_vis_S0.OnMouseMove:=@SL_OnMouseMove;
   SL_rad_S0.tag:=13; SL_rad_S0.OnMouseMove:=@SL_OnMouseMove;
   SL_cok_S0.tag:=14; SL_cok_S0.OnMouseMove:=@SL_OnMouseMove;
   SL_kut_S0.tag:=15; SL_kut_S0.OnMouseMove:=@SL_OnMouseMove;
   SL_str_S0.tag:=16; SL_str_S0.OnMouseMove:=@SL_OnMouseMove;
   SL_zid_S0.tag:=17; SL_zid_S0.OnMouseMove:=@SL_OnMouseMove;
   SL_pla_S0.tag:=18; SL_pla_S0.OnMouseMove:=@SL_OnMouseMove;
	SL_res_S0.tag:=19; SL_res_S0.OnMouseMove:=@SL_OnMouseMove;

	SL_pod_S1.tag:=11; SL_pod_S1.OnMouseMove:=@SL_OnMouseMove;
	SL_vis_S1.tag:=12; SL_vis_S1.OnMouseMove:=@SL_OnMouseMove;
   SL_rad_S1.tag:=13; SL_rad_S1.OnMouseMove:=@SL_OnMouseMove;
   SL_cok_S1.tag:=14; SL_cok_S1.OnMouseMove:=@SL_OnMouseMove;
   SL_kut_S1.tag:=15; SL_kut_S1.OnMouseMove:=@SL_OnMouseMove;
   SL_str_S1.tag:=16; SL_str_S1.OnMouseMove:=@SL_OnMouseMove;
   SL_zid_S1.tag:=17; SL_zid_S1.OnMouseMove:=@SL_OnMouseMove;
   SL_pla_S1.tag:=18; SL_pla_S1.OnMouseMove:=@SL_OnMouseMove;
	SL_res_S1.tag:=19; SL_res_S1.OnMouseMove:=@SL_OnMouseMove;

	// Selektiraj sve
	
	SL_vidi_sve.OnClick:=@SL_OnClickShow;
	SL_sakrij_sve.OnClick:=@SL_OnClickHide;
	
	// On Mouse Click Hide 
	
	SL_pod_S0.OnClick:=@SL_OnClickHide;
	SL_vis_S0.OnClick:=@SL_OnClickHide;
	SL_rad_S0.OnClick:=@SL_OnClickHide;
	SL_cok_S0.OnClick:=@SL_OnClickHide;
	SL_kut_S0.OnClick:=@SL_OnClickHide;
	SL_str_S0.OnClick:=@SL_OnClickHide;
	SL_zid_S0.OnClick:=@SL_OnClickHide;
   SL_pla_S0.OnClick:=@SL_OnClickHide;
	SL_res_S0.OnClick:=@SL_OnClickHide;
	
	// On Mouse Click Show 
	
	SL_pod_V0.OnClick:=@SL_OnClickShow;
	SL_vis_V0.OnClick:=@SL_OnClickShow;
	SL_rad_V0.OnClick:=@SL_OnClickShow;
	SL_cok_V0.OnClick:=@SL_OnClickShow;
	SL_kut_V0.OnClick:=@SL_OnClickShow;
	SL_str_V0.OnClick:=@SL_OnClickShow;
	SL_zid_V0.OnClick:=@SL_OnClickShow;
   SL_pla_V0.OnClick:=@SL_OnClickShow;
	SL_res_V0.OnClick:=@SL_OnClickShow;
	
	// U slu�aju zatvaranja Corpusa!
	// application.MainForm.OnCloseQuery:=@OnCorpusClose;
	// RU�I SE!!!!!!!!!
	
	// application.MainForm.OnClose:=@OnCorpusClose2;
	// RU�I SE!!!!!!!!!
	
	// application.MainForm.OnDeactivate:=@OnCorpusDeactivate;   // Ne zatvara prozor skripte
	// Ne ru�i se, ali ne zatvara skriptu
	
	// application.MainForm.OnResize:=@OnCorpusResize;
	// RU�I SE!!!!!!!!!
	
	
End;

{$I ..\mSvi.cps}

// BEGIN	je u mSvi.cps

	SakrijSve;	
	// PrintTime('Skripta Pogled start');

	put:=ScriptFolder+'RC_Tools\Right\mPogled\';
	desni_rub:=Fright.left;	
	kreiraj_formu;
	make_shield;
	PokaziSve;
	
	
	
	
   
   // Main.ShowModal;
	
	Shield.show;
	Main.Show;
	
	 while Main.visible do begin
	// while 1=1 do begin
      application.processmessages;
   end;
   
	Main.close;
   Shield.close;
	// PrintTime('Napravljen close');
	
	Shield.free;
	Main.free; 
	// PrintTime('Napravljen free');
	
	
	
	PokaziSve;
	
   // PrintTime('Skripta Pogled end');
End.