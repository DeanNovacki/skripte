
{$I ..\..\..\Include\rcMessages.cps}
//{$I ..\..\include\rcStrings.cps}
{$I ..\..\..\Include\rcControls.cps}
//{$I ..\..\include\rcObjects.cps}
//{$I ..\..\include\rcEvents.cps}
//{$I ..\..\include\rcCurves.cps}
//{$I ..\..\include\rcMath.cps}

var
	PDF:TpdfPrinter;
	dato:string;
	Memko:TMemo;
	Forko:TForm;
	{$I ..\..\..\include\rcPrint.cps}
	{$I ..\..\mSvi.cps}	
	
	
{	
BEGIN	// begin je u includu
}

   // dato:=PrgFolder+'skripte\RC_Tools\podsjetnik.rtf';
	dato:=PrgFolder+'skripte\RC_Tools\tekst.rtf';
	// Showmessage(dato);
   
	
	if fileexists(dato) then begin 
			PDF:=TpdfPrinter.create(1250);
			PDF.FileName := dato;
			PDF.ShowPdf;
			PDF.Free
		end else begin
			// function rcMemo(mName:string; mParent: TWinControl; mX, mY, mS, mV: integer):TMemo;
			// Showmessage('Nema podsjetnika');
			Forko:=rcTool('Red_Cat');
			// Showmessage('20');
			Memko:=rcMemo('mm',Forko,1,1,50,50);
			// Showmessage('30');
			Memko.Lines.SaveToFile(dato);
			PDF:=TpdfPrinter.create(1250);
			PDF.FileName := dato;
			PDF.ShowPdf;
			PDF.Free;
	end;
	
	SakrijSve;
	
End.
