﻿
// Out 2******************************************
Procedure Test_dasaka;
var
	i, broj_bokova, Broj_horizontalnih:integer;
	ss:string;
	td:Tdaska;
Begin
	// brojanje količine
	broj_bokova:=0;
	Broj_horizontalnih:=0;
	for i:=0 to e.childdaska.CountObj-1 do Begin  // broji daske u editoru
		td:=e.childdaska.Daska[i];   
		// tražim bok
		if td.selected and (td.smjer in [VertBok]) then begin				// VertBok, VertFront, Horiz
			broj_bokova:=broj_bokova+1;
			daska_1:=td;
			// ShowMessage('Našao sam frontalnu broj: '+IntToStr(broj_fronti));
		end; // if td.selected
		// tražim horizontalnu
		if td.selected and (td.smjer in [Horiz]) then begin			// VertBok, VertFront, Horiz
			broj_horizontalnih:=broj_horizontalnih+1;
			// ShowMessage('Našao sam horizontalnu broj: '+IntToStr(broj_horizontalnih));
			daska_2:=td;
		end; //if td.selected
	end;  // for i:=0 to e.childdaska.CountObj-1 


	// provjera količine
	If (broj_bokova=1) and (Broj_horizontalnih=1) Then Begin
		// provjera kuteva
		If (Daska_1.kut<>0) or (Daska_1.xkut<>0) or (Daska_1.zkut<>0) or 
			(Daska_2.kut<>0) or (Daska_2.xkut<>0) or (Daska_2.zkut<>0) Then Begin 
					Showmessage('Neke selektirane daska su rotirane,' +#13#10+
									'a ne smiju biti za ovaj spoj.' );
					ButOK.Enabled:=False;			 
			End else begin
				// kutevi su ok
				// provjera dodira
				// visina,dubina,debljina,xPos,yPos,zPos
				If (daska_1.ZPos-daska_1.dubina>daska_2.ZPos)			// 1 ispred 2
						or (daska_1.ZPos < daska_2.ZPos-daska_2.dubina)					// 1 iza 2
						or (daska_1.xPos<daska_2.XPos+daska_2.visina) 					// 1 je lijevo od 2
						or (daska_1.xPos>daska_2.XPos+daska_2.visina)  						// 1 je desno od 2
						or (daska_1.yPos+daska_1.visina<daska_2.Ypos)						// 1 je ispod 2
						or (daska_1.yPos>daska_2.ypos+daska_2.debljina) 						// 1 je iznad 2
					Then begin
						Showmessage('Izgleda da se daske ne dodiruju.'+#13#10+
										'Makro se može postaviti, ali dobro provjerite što radite!');
				End;  // 	If (daska_1.ZPos<>daska_2.ZPos) 
		end;	// If (Daska_1.kut<>0) 
	end else begin  // If (broj_fronti=1) ;	
		ButOK.Enabled:=False;
		Showmessage('Moraju biti selektirane dvije daske:' +#13#10+
						'Bok s desne strane i horizontalna s lijeve.');
	end;  // If (broj_bokova=1)
	
		
End;
// Out 2******************************************
