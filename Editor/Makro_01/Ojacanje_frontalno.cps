program spoj;
// 
var
   sel:boolean;
   pogresna, i:integer;
   td:Tdaska;
   

begin

	
  sel:=false;
  for i:=0 to e.childdaska.CountObj-1 do Begin     //el.childdaska.CountObj = broj dasaka u lementu
		td:=e.childdaska.Daska[i];                    //jedna daska iz elementa (s rednim brojem i)
		If td.selected then begin 
			pogresna:=0;
			// provjera smijera
			If not (td.smjer in [VertFront]) Then Begin 			//( VertBok, VertFront, Horiz, Gore )
				Showmessage('Selektirana daska "'+td.naziv+'" nema smijer "Frontalno",' +#13#10+
								 'a trebala bi za ovaj spoj.' );
				pogresna:=pogresna+1;				 
			End;
			// provjera kuta
			If (td.kut<>0) or (td.xkut<>0) or (td.zkut<>0) Then Begin 
				Showmessage('Selektirana daska "'+td.naziv+'" je rotirana",' +#13#10+
								'a ne smije biti za ovaj spoj.' );
				pogresna:=pogresna+1;				 
			End;
			// provjera debljine
			If td.debljina<16 Then Begin 
				Showmessage('Selektirana daska "'+td.naziv+'" je pretanka",' +#13#10+
								'a treba biti debela minimalno 16mm za ovaj spoj.' );
				pogresna:=pogresna+1;				 
			End;
			if Pogresna=0	Then Begin
            e.MakeSpoj(td,nil,'..\skripte\Editor\Makro_01\ojacanje_frontalno.cmk','');
            sel:=true;
			End;
     end;
  end;  
 
  
  e.recalcformula(e);     // ponovo iscrtava - ne treba klik na ekran
 
  if not sel then ShowMessage('Odaberite barem jednu dasku ');

end.