Program i;
// starter for some scripts (notes, offer, tender, orders)


	{$I ..\..\..\include\rcPrint.cps}
	{$I ..\..\..\include\rcControls.cps}
// {$I ..\..\..\_include\rcObjects.cps}
// {$I ..\..\..\_include\rcEvents.cps}
// {$I ..\..\..\_include\rcCurves.cps}
	{$I ..\..\..\include\rcStrings.cps}
	{$I ..\..\..\include\rcPDF.cps}
	{$I Memo.cps}
var
   F_Choice:TForm;
   Choice: Integer;
   Button_Choice:TButton;
   starter:Boolean;
	{$I notes.cps}    
//{$I offer.cps}  
//{$I tender.cps} 
//{$I orders.cps} 

procedure ButClick(Sender:TObject);
Begin
   Choice:=TButton(Sender).Tag;
   Print('ButClick(Sender:TObject)'+'Choice:='+IntToStr(Choice));
   Case Choice of
        1 : notes;   // napomene 
   //   2 : offer;   // ponuda
   //   3 : tender;  // troškovnik
   //   4 : orders;  // radni nalozi
   end;  // case
   
   // If you want that choice windows remain open after executing script comment next line
   F_Choice.Close;   
   
   
End;   

// MAIN
Begin
   PrintTime ('Skripta Napomene Start');  
   starter:=false;                                    // enable to start choise window
   // 
   if starter=true then begin
      F_Choice:=RcTool('Izaberi'); 
      Print('Choice Window created');
      {1} Button_Choice:=rcButton('rcb1','Napomene'    ,F_Choice,10, 10) ; Button_Choice.Tag:=1; Button_Choice.width:=290; Button_Choice.OnClick:=@ButClick;
      {2} Button_Choice:=rcButton('rcb2','Ponuda'      ,F_Choice,10, 40) ; Button_Choice.Tag:=2; Button_Choice.width:=290; Button_Choice.OnClick:=@ButClick;
      {3} Button_Choice:=rcButton('rcb3','Troškovnik'  ,F_Choice,10, 70) ; Button_Choice.Tag:=3; Button_Choice.width:=290; Button_Choice.OnClick:=@ButClick;
      {4} Button_Choice:=rcButton('rcb4','Radni nalozi',F_Choice,10, 100); Button_Choice.Tag:=4; Button_Choice.width:=290; Button_Choice.OnClick:=@ButClick;
      Button_Choice.OnClick:=@ButClick;
      F_Choice.ShowModal
     end else begin
      notes;
      Print ('Notes call finished');
   end; // if starter=true

   PrintTime ('Skripta Napomene End'); 
	PrintTime ('===================='); 
   // Showmessage('Script Ended');
   Print('');
end.