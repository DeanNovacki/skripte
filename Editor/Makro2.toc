[10]
Scriptname=Fronta-Vertikala 
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Fronta_vertikala\Fronta_Vertikala.cps
Hint=Fronta - Vertikala
Icon=\Makro_02\Fronta_Vertikala\Fronta_Vertikala.bmp
Ver=1
[20]
Scriptname=Fronta-Horizontalna
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Fronta_Horizontalna\Fronta_Horizontalna.cps
Hint=Fronta-Horizontalna
Icon=\Makro_02\Fronta_Horizontalna\Fronta_Horizontalna.bmp
Ver=1
[30]
Scriptname=Lijevi_bok_fronta
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Lijevi_bok_fronta\Lijevi_bok_fronta.cps
Hint=Lijevi_bok-Vertikala
Icon=\Makro_02\Lijevi_bok_fronta\Lijevi_bok_fronta.bmp
Ver=1
[40]
Scriptname=Lijevi_bok_Horizontalna
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Lijevi_bok_Horizontalna\Lijevi_bok_Horizontalna.cps
Hint=Lijevi_bok-Horizontalna
Icon=\Makro_02\Lijevi_bok_Horizontalna\Lijevi_bok_Horizontalna.bmp
Ver=1
[50]
Scriptname=Desni_bok_Fronta
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Desni_bok_Fronta\Desni_bok_Fronta.cps
Hint=Desni_bok-Fronta
Icon=\Makro_02\Desni_bok_Fronta\Desni_bok_Fronta.bmp
Ver=1
[60]
Scriptname=Desni_bok_Horizontalna
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Desni_bok_Horizontalna\Desni_bok_Horizontalna.cps
Hint=Desni_bok-Horizontalna
Icon=\Makro_02\Desni_bok_Horizontalna\Desni_bok_Horizontalna.bmp
Ver=1
[70]
Scriptname=Ledja_Vertikala
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Ledja_Vertikala\Ledja_Vertikala.cps
Hint=Ledja-Vertikala
Icon=\Makro_02\Ledja_Vertikala\Ledja_Vertikala.bmp
Ver=1
[80]
Scriptname=Ledja-Horizontalna
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Ledja_Horizontalna\Ledja_Horizontalna.cps
Hint=Ledja-Horizontalna
Icon=\Makro_02\Ledja_Horizontalna\Ledja_Horizontalna.bmp
Ver=1
[90]
Scriptname=Pod_Fronta
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Pod_Fronta\Pod_Fronta.cps
Hint=Pod-Fronta
Icon=\Makro_02\Pod_Fronta\Pod_Fronta.bmp
Ver=1
[100]
Scriptname=Pod_Vertikala
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Pod_Vertikala\Pod_Vertikala.cps
Hint=Pod-Vertikala
Icon=\Makro_02\Pod_Vertikala\Pod_Vertikala.bmp
Ver=1
[110]
Scriptname=Strop_Vertikala
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Strop_Vertikala\Strop_Vertikala.cps
Hint=Strop-Vertikala
Icon=\Makro_02\Strop_Vertikala\Strop_Vertikala.bmp
Ver=1
[120]
Scriptname=Strop_Fronta
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Strop_Fronta\Strop_Fronta.cps
Hint=Strop-Fronta
Icon=\Makro_02\Strop_Fronta\Strop_Fronta.bmp
Ver=1
[1000]
Scriptname=Brisanje
TOOLNNAME=Spojevi_2
Scfile=\Makro_01\Ukloni.cps
Hint=Ukloni sve spojeve
Icon=\Makro_01\Ukloni.bmp
Ver=1
[1010]
Scriptname=Upute
TOOLNNAME=Spojevi_2
Scfile=\Makro_02\Upute.cps
Hint=Upute
Icon=\Makro_02\Upute.bmp
Ver=1





