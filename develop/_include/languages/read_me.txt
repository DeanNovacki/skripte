Instructions for translation

File 'active.lng' set actual language for translation.
First uncomented row in this file set dictionary.
For example, if file 'active.lng' have 'active_language=EnglishUK.dict' then translation will be found in dictionary named 'EnglishUK.dict'. If file 'active.lng' have 'active_language=CroatianHR.dict' then translation will be found in dictionary named 'CroatianHR.dict'. 


Dictionaries contain words and phrases used in scripts.

*.dict FILES

Before sign '=' is original phrase. Don't change it. Never!
After  sign '=' is phrase which appear in the script. You can change this.

