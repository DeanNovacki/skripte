var
   win:tForm; 
   OpenB:TButton;



function find_control2(root_object:TWinControl; vc_name:String):TObject;
// find_control(root_object:TWinControl; c_type, c_name:string):TObject;
var
   i:integer;
   xo:TObject;
begin
  // for i:=0 to root_object.ControlCount-1 do begin
  for i:=0 to root_object.ControlCount-1 do begin
     // writeln ('Analising object number '+IntToStr(i));
     xo:=root_object.Controls[i];
     if xo is Tlabel  then if UpperCase(vc_name)=UpperCase(TLabel(xo).name)  then result:=xo;
     if xo is Tpanel  then if UpperCase(vc_name)=UpperCase(TPanel(xo).name)  then result:=xo;
     if xo is TButton then if UpperCase(vc_name)=UpperCase(TButton(xo).name) then result:=xo;
     if xo is TEdit   then if UpperCase(vc_name)=UpperCase(TEdit(xo).name)   then result:=xo;
     if xo is TMImage then if UpperCase(vc_name)=UpperCase(TMImage(xo).name) then result:=xo;
     if xo is TShape  then if UpperCase(vc_name)=UpperCase(TShape(xo).name)  then result:=xo;
     if (result=nil) and (xo is TWinControl)  then result:=find_control2(TWinControl(xo),vc_name); 
  end;  // for 
  // writeln ('end for i');
end;  // function

function rButton(name, caption:string; parent: TWinControl; x,y:Integer):TButton;
// make button
var  b1:TButton; ro:TObject;
begin
   b1:=TButton.create(parent); 
   b1.parent:=parent; 
   ro:=Find_Control2(parent,name);
   if  ro<>nil then Showmessage ('Button "'+name+'" already exist'); 
                   
   
   b1.name:=name;
   b1.caption:=caption;
   b1.Left:=x; b1.Top:=y; 
   result:=b1;
end;


begin
   // create form  
   win:=TForm.create(nil);
   win.width:=930; win.height:=1000;
   win.name:='Script_window';
   OpenB:=rButton('Open', 'Open file',Win, 10,10);
   // OpenB:=rButton('Open', 'Open file',Win, 10,10);  
 


   win.showmodal;
   // win.show;
   // while win.visible do begin
   //      application.processmessages;
   //      end;

   // destroy form
   win.free; 
end.