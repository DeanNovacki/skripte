Procedure cbDebug_OnClick(Sender:TObject);	
Begin
	// Application.ProcessMessages;
	
	If cbDebug.checked=True then begin
			Panel_right.visible:=true;
			Panel_right.width:=400;
			Main.Width:=Sirina_prozora+400;
			// Main.Left:=Main.Left-200;
			ShowN('Poruke se pokazuju.');
			Debug:=true;
		end else begin
			Panel_right.visible:=false;
			Main.Width:=Sirina_Prozora;
			// Main.Left:=Main.Left+200;
			ShowN('Poruke se ne pokazuju.');
			Debug:=False;
	end;   // if
	// Main.Left:=screenwidth-Main.Width-20;
	If Main.Left+Main.Width>ScreenWidth then Main.Left:=ScreenWidth-Main.Width;
End;

Procedure ButOK_OnClick(Sender:TObject);
Begin
	ShowN('Pritisnuta je tipka "OK (Izlaz)"');
						

End;

Procedure ButPomoc_OnClick(Sender:TObject);
Begin
	ShowN('Pritisnuta je tipka "Pomo�"');
	Pomoc;
End;

Procedure postavi_slike;
// postavlja slike za elemente u SB1:=TScrollBox
var
	i, x_pos, y_pos:Integer;   // koordinate slika
	broj_slika, sirina_slike, visina_slike, x_razmak, y_razmak:integer;
	putanja, Slike_putanja, elementi_putanja, opis:String;
	slika:TImage;
Begin 
	ShowN('Procedure postavi_slike START');
	ShowN ('Configuracijska datoteka je "'+conf_path+'"');
	broj_slika:=find_value_int(conf_path,'Broj_Slika');
	ShowN ('Broj slika: '+IntToStr(Broj_slika));
	Slike_putanja:=prgfolder+find_value_str(conf_path,'Slike_putanja');
	ShowN ('Folder za slike je: '+Slike_Putanja);
	Broj_kolona:=find_value_int(conf_path,'Broj_Kolona');
	ShowN('Broj_kolona je pode�ena na '+IntToStr(Broj_kolona));
	Sirina_slike:=find_value_int(conf_path,'Sirina_slike');
	ShowN('Sirina_slike pode�ena na '+IntToStr(Sirina_slike));
	Visina_slike:=find_value_int(conf_path,'Visina_slike');
	ShowN('Visina_slike pode�ena na '+IntToStr(Visina_slike));
	// sirina_slike:=100; visina_slike:=150; 
	x_razmak:=10; y_razmak:=10;
	x_pos:=-sirina_slike; y_pos:=10
	for i:=1 to broj_slika do begin
		// prora�un pozicije slike
		if x_pos>(Sirina_slike*(Broj_kolona-1)) then begin 
									y_pos:=y_pos+visina_slike+y_razmak;
									x_pos:=10;
								end else begin 
									x_pos:=x_pos+x_razmak+sirina_slike;
		end;
		
		// u�itavanje slike
		Putanja:=Slike_putanja+'\'+find_value_str(conf_path,'Slika'+IntToStr(i));
		// ShowN ('Putanja za sliku je "'+Putanja+'"');
		if rcFileCheck(Putanja) then Begin
				Slika:=rcLoadImage('slika'+IntToStr(i),Putanja,SB1,x_pos,y_pos);
				ShowN ('U�itao sam sliku broj '+IntToStr(i)+' na poziciju '+ IntToStr(x_pos)+' ; '+IntToStr(y_pos));
			end else begin
				// ako nema slike, postavlja RedCat logo
				Slika:=rcLoadImage('slika'+IntToStr(i),Slike_putanja+'\'+'rc.jpg',SB1,x_pos,y_pos);
				ShowN('Nema slike '+putanja);
		end;
		// ShowN ('Slika postavljena na X: '+IntToStr(X_pos)+', Y: '+IntToStr(X_pos));
		Slika.tag:=i;
		opis:=find_value_str(conf_path,'hint'+IntToStr(i));
		Slika.hint:=IntToStr(i)+'. '+opis;
		Slika.AutoSize:=false;
		Slika.Stretch:=True;
		Slika.width:=Sirina_slike;
		Slika.Height:=Visina_slike;
		Slika.OnClick:=@Slika_OnClick;
		
		
	end;
	ShowN('Procedure postavi_slike END');
end;
	
Procedure Create_Main_Window;
var
	slika_put,elem_put:TStringlist;
	sput:string;
	i:integer;
Begin
	// Startni prozor
   Main:=rcTool('CRC'); 
	Main.caption:='Corpus RC - Segmenti ormara'
	//Main.Width:=600;
	
	// Main.Height:=420;
	// Main.Left:=200;
	Main.BorderStyle:=bsSizeToolWin;
	//Main.BorderIcons:=[];
	Main.Position:=poDesigned;
	

	// Donji Panel za tipke
	Panel_Down:= rcPanel ( '' ,Main ); 
	Panel_Down.ALign:=alBottom; 
   Panel_Down.Height:=40;       // Visina donjeg panela!!!!
   Panel_Down.BorderWidth:=5;
	
   // desni panel za memo 
   Panel_Right:= rcPanel ( '' ,Main ); Panel_Right.ALign:=alRight; 
   Panel_Right.Width:=2;       // Širina desnog panela!!!!
   Panel_Right.BorderWidth:=5;
	
   // lijevi panel za slikice
   Panel_Left:= rcPanel ( '' ,Main ); 
	Panel_Left.Left:=10;
	Panel_Left.Top:=10;
	Panel_Left.ALign:=alClient;

   // tipke OK i Cancel
	
	ButOK:=rcButton('bOK','X',Panel_Down,1000,1);ButOK.Align:=alRight; ButOK.width:=40;
	ButOK.visible:=false;
	rcSpaceR(Panel_Down,800);
	// ButCancel:=rcButton('bCancel','Odustajem',Panel_Down,1,1);ButCancel.Align:=alRight;ButCancel.width:=100;
   // rcSpaceR(Panel_Down,600);
	ButPomoc:=rcButton('bPomoc','Upute',Panel_Down,1,1);ButPomoc.Align:=alLeft;
	ButPomoc.width:=75; ButPomoc.Enabled:=False;
	rcSpaceL(Panel_Down,400);
		
	ButOK.OnClick:=@ButOK_OnClick;
	// ButCancel.OnClick:=@ButCancel_OnClick;
	ButPomoc.OnClick:=@ButPomoc_OnClick;
		
	ButOK.default:=True;
	// ButCancel.ModalResult:=mrCancel;
	
	// Main.OnResize:=@Main_OnResize;
	// debug checkbox
	
	rcSpaceR(Panel_Down,300);
	cbDebug:=rcCheckBox('cbDB','',Panel_Down, 1,20);cbDebug.Align:=alRight; 
	cbDebug.Font.Size:=7;
	cbDebug.Width:=20;
	cbDebug.hint:='Prika�i sistemske poruke';
	rcSpaceR(Panel_Down,1);rcSpaceR(Panel_Down,1);rcSpaceR(Panel_Down,1);
	
	if debug then cbDebug.checked:=true
				else cbDebug.checked:=false;
				
	cbDebug.OnClick:=@cbDebug_OnClick;
	
	// Poruke
	// function rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
	// Talignment: taLeftJustify, taCenter, taRightJustify 
	PorukaV:=rcLabel('LBL1','Poruka',Panel_Down,90,4,taLeftJustify);
	PorukaV.Font.Size:=11;
	PorukaV.Font.Color:=clMaroon;
	PorukaV.Caption:='Corpus RC';
	PorukaM:=rcLabel('LBL2','Poruka',Panel_Down,90,20,taLeftJustify);
	PorukaM.Font.Size:=9;
	PorukaM.Font.Color:=clGray;
	PorukaM.Caption:='Segmenti ormara';
	
	
	// MemoJ
   Panel_Memo:=rcPanel ( '' ,Panel_Right ); Panel_memo.Top:=18;Panel_Memo.ALign:=alClient; 
   
   MemoJ:=rcMemo('Memo_debug', Panel_Memo, 1,18,1,1);MemoJ.Align:=alClient;
   MemoJ.Color:=clBTNFace;MemoJ.Font.Size:=8;MemoJ.Ctl3D:=False;MemoJ.Font.Color:=clMaroon;
	
	// ---------------------------------------------------------------------------------------
	// Tek nakon ovoga mogu i�i debug poruke jer one idu u memo!!!
	// ---------------------------------------------------------------------------------------
	
	ShowN('skripta: Segmenti ormara v1.0');
	ShowN('07.06.2017.');
	
	ShowN ('Configuracijska datoteka je "'+conf_path+'"'); 
	if not rcFileCheck(conf_path) then begin
		ShowMessage('Nema konfiguracijske datoteke:'+#13#10+
						+ conf_path + #13#10+ 'Dovi�enja!');
						Exit;
	end;



	
	// Application.ProcessMessages;
	If cbDebug.checked=True then begin
			Panel_right.visible:=true;
			Panel_right.width:=200;
			Main.Width:=700;
			// ShowD('Main.Width='+IntToStr(Main.Width));
		end else begin
			Panel_right.visible:=false;
			Main.Width:=700;
	end;   // if
	Main.Left:=screenwidth-Main.Width-20;	
	
	// scroll box
	SB1:=TScrollBox.create(Panel_Left);
	SB1.parent:=Panel_Left;
   SB1.Ctl3D:=False;
   SB1.left:=1;
   SB1.top:=1;
   // SB1.width:=550;
   SB1.Height:=595;
	SB1.Align:=alClient;
   // SB1.Color:=clBlue;
   SB1.VertScrollBar.Visible:=True;
   SB1.VertScrollBar.Tracking:=True;   
   // Adj_SB.VertScrollBar.Smooth:=True;  ne radi!!
   SB1.HorzScrollBar.Visible:=False;
	
	// postavi slike

	postavi_slike;
	
End;
