// Primjer kako se koristi array u funkciji
// MORA BITI TYPE ARRAY, A ONDA VARIJABLA

Type 
	TSingleArray = array [0..1] of single;
	neka_polja: array of Tedit;   // vidi kasnije odre�ivanje du�ine
Var
	x1,y1:single;
	xc,yc:single;
	x2,y2:single;
	t2:TSingleArray;

Function rotate2D(t1:TSingleArray; alpha:single; c1:TSingleArray): TSingleArray;
// Rotacija to�ke T1 u 2D oko to�ke C1 za kut ALPHA

Begin
	// citanje ulaznog niza 
	x1:=t1[0]; 	y1:=t1[1];
	xc:=c1[0]; 	yc:=c1[1];
	// translacija svega tako da centar rotacije bude u ishodi�tu
	x1:=x1-xc;	
	y1:=y1-yc;
	// rotacija
	x2:=x1*cosD(alpha)-y1*sinD(alpha);
	y2:=y1*cosD(alpha)-x1*sinD(alpha);
	// translacija natrag 
	x2:=x2+xc;	
	y2:=y2+yc;
	// rezultat
	result[0]:=x2;
	result[1]:=y2;
End;

var
	poc,cen,zav: TSingleArray;
	
Begin
	// uzmi prvi element u projektu
	elem:=TElement(ElmList.Items[0]);
	// izmi oznaku kruga
	krug:=TElement(ElmList.Items[1]);
	// zapamti po�etni x i z
	sx:=elem.Xpos;
	sz:=elem.Zpos;
	
	// odredi centar rotacije
	// centar mora biti izracunat i za element pod kutem
	// p2:=rotate(p1,alpha,centar);
	poc[0]:=elem.Xpos+elem.sirina/2;
	poc[1]:=elem.Zpos-elem.dubina/2;
	cen[0]:=elem.Xpos;
	cen[1]:=elem.Zpos;
	
	SetLength(Neka_Polja, 10);
	Neka_polja[1].text:='gfgfsd';
	
	kut:=elem.kut;
	// Showmessage('1');
	zav:=rotate2D(poc,kut,cen);
	
	...