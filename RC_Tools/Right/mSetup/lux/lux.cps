Program Pogled;   


// MAIN ////////////////////////////////////////////////////////////////////////
var
   // Startni prozor
	put:String;
	Main:TForm;
	Pan_Title, Pan_Q, Pan_Size, Pan_xPU, Pan_But:TPanel;
	SL_Naslov, Slika : Array [1..5] of TMImage;
	Lab_Q:TLabel;
	SL_CPU0, SL_CPU1, SL_GPU0, SL_GPU1 : TMImage; 
	
	But : Array [1..5] of TSpeedButton;
	
	Bt_Ok, BT_Cancel, BT_Pomoc : TButton;
	
	Q_Scroll:TScrollBar;
	
	
	SetupDat:TStringList;		//	privremeni setup.Lux
	
	// Postavke
	kvaliteta, velicina, render : Integer;
	
{$I ..\..\..\include\rcMessages.cps}
{$I ..\..\..\include\rcPrint.cps}
{$I ..\..\..\include\rcStrings.cps}
{$I ..\..\..\include\rcControls.cps}
{$I ..\..\..\include\rcObjects.cps}
{$I ..\..\..\include\rcEvents.cps}
{$I ..\..\..\include\rcConfig.cps}
{$I ..\..\..\include\rcCurves.cps}
{$I ..\..\..\include\rcMath.cps}


procedure Q_Scroll_OnChange(Sender: TObject);
var
	i:Integer;
begin
	Print ( ' Proc Q_Scroll_OnChange Start');
	For i:=1 to 5 do begin
		If Q_Scroll.Position=i  then Slika[i].visible:=true
										else Slika[i].visible:=False;
	
	end;
	Q_Scroll.Hint:=IntToStr(Q_Scroll.Position);
	Kvaliteta:=Q_Scroll.Position;
	Print ('Kvaliteta je promijenjena u '+IntToStr(kvaliteta));
	
	Print ( ' Proc Q_Scroll_OnChange end');
end;

procedure But_OnClick(Sender: TObject);
var
	i:Integer;

begin
	print ('procedure But_OnClick start');
	For i:=1 to 5 do begin
		If TSpeedButton(Sender).Tag=i then But[i].font.style:=[fsBold]
												else But[i].font.style:=[];
	
	end;
	Velicina:=TSpeedButton(Sender).Tag;
	Print ('Velicina je promijenjena u '+IntToStr(velicina));
	
	print ('procedure But_OnClick end');
end;

procedure SL_render_OnClick(sender: TObject);
Begin 
	print ('procedure SL_render_OnClick start');
	if TImage(Sender).tag=1 then begin
			SL_CPU1.visible:=true;
			SL_GPU1.visible:=false;
			render:=1;
			Print ('Render: CPU');
		end else begin
			SL_CPU1.visible:=False;
			SL_GPU1.visible:=True;
			render:=2;
			Print ('Render: GPU');
	end;
	Print ('Render je promijenjen u '+IntToStr(Render));
	print ('procedure SL_render_OnClick end');
End;

Procedure Ucitaj_setup_file;
var
	putanja:string;
Begin
	Print ('proc Ucitaj_setup_file start');
	if render=1 then putanja:=put+'setupCPU.lux'
					else putanja:=put+'setupGPU.lux';
	// Print ('A');
	if FileExists(putanja) then begin
		// Print ('B');
		SetupDat:=TStringList.Create;
		SetupDat.LoadFromFile(putanja);
		Print ('u�itao sam ' + putanja);
	end else begin
		Showmessage ('Nedostaje ' + putanja + #13#10+ 'Probleme slutim')	;
	end;
	
	Print ('Kvaliteta: '+IntToStr(kvaliteta));
	Print ('Velcina: '+IntToStr(velicina));
	Print ('Render: '+IntToStr(render));
	
	Print ('proc Ucitaj_setup_file End');
End;


Procedure podesi_setup_CPU;
var
	i:integer;

	finkve,	 		// find eyedepth
	finkvl, 			// find Lightdepth

	repkve,	 		// replace eyedepth
	repkvl 			// replace Lightdepth

	: string;
Begin 
	
	Print ('podesi_setup_CPU start');
	
	// Kvaliteta
	finkve := 'eyedepth","value":2';				// find eyedepth
	finkvl := 'lightdepth","value":1';				// find Lightdepth
	
	if kvaliteta=1 then begin
		Print ('')
		repkve := 'eyedepth","value":2';
		repkvl := 'lightdepth","value":1';
		print ('Nova kvaliteta je '+ (repkve) );
	end; 						
	if kvaliteta=2 then begin
		repkve := 'eyedepth","value":3';
		repkvl := 'lightdepth","value":1';
		print ('Nova kvaliteta je '+ (repkve) );
	end; 						
	if kvaliteta=3 then begin
		repkve := 'eyedepth","value":4';
		repkvl := 'lightdepth","value":1';
		print ('Nova kvaliteta je '+ (repkve) );
	end; 						
	if kvaliteta=4 then begin
		repkve := 'eyedepth","value":5';
		repkvl := 'lightdepth","value":2';
		print ('Nova kvaliteta je '+ (repkve) );
	end; 						
	if kvaliteta=5 then begin
		repkve := 'eyedepth","value":6';
		repkvl := 'lightdepth","value":3';
		print ('Nova kvaliteta je '+ (repkvl) );
	end; 						
			
	
		
	for i:=0 to SetupDat.count-1 do begin  
		
		// eyedepth
		If Contain(SetupDat[i],finkve) then Begin
			Print ('IN : ' + IntToStr(i+1) + ' ' + SetupDat[i] );
			SetupDat[i]:=rcReplaceText(SetupDat[i],finkve,repkve);
			Print ('OUT: ' + IntToStr(i+1) + ' ' + SetupDat[i] );
		End; // If Contain(SetupDat[i],finkve)
		
		// lightdepth
		If Contain(SetupDat[i],finkvl) then Begin
			Print ('IN : ' + IntToStr(i+1) + ' ' + SetupDat[i] );
			SetupDat[i]:=rcReplaceText(SetupDat[i],finkvl,repkvl);
			Print ('OUT: ' + IntToStr(i+1) + ' ' + SetupDat[i] );
		End; // If Contain(SetupDat[i],finkvl)
		
		// 
		
	end; // for i:=0 to SetupDat.count-1

	Print ('podesi_setup_CPU end');	
End;

Procedure podesi_setup_GPU;
var
	i:integer;

	finkv,	 		// find maxdepth
	repkv	 		// replace maxdepth

	: string;
Begin 
	Print ('podesi_setup_GPU start');
	
	// Kvaliteta
	finkv := 'maxdepth","value":2';		// find maxdepth
	repkv := 'maxdepth","value":'+IntToStr(kvaliteta);
		
	for i:=0 to SetupDat.count-1 do begin  
		If Contain(SetupDat[i],finkv) then Begin
			Print ('IN : ' + IntToStr(i+1) + ' ' + SetupDat[i] );
			SetupDat[i]:=rcReplaceText(SetupDat[i],finkv,repkv);
			Print ('OUT: ' + IntToStr(i+1) + ' ' + SetupDat[i] );
		End; // If Contain(SetupDat[i],finkv)
	end; // for i:=0 to SetupDat.count-1
	
	Print ('podesi_setup_GPU start');
End;

Procedure promijeni_velicinu;
var
	i:integer;
	finx, finy, repX, repY : string;
Begin 
	
	Print ('podesi_velicinu start');
	
	finX := 'xresolution","value":640';
	finY := 'yresolution","value":320';
	
	if velicina=1 then begin
		repx := 'xresolution","value":320';
		repy := 'yresolution","value":180';
	end; 						
	if velicina=2 then begin
		repx := 'xresolution","value":640';
		repy := 'yresolution","value":360';
	end; 						
	if velicina=3 then begin
		repx := 'xresolution","value":768';
		repy := 'yresolution","value":432';
	end; 						
	if velicina=4 then begin
		repx := 'xresolution","value":1280';
		repy := 'yresolution","value":720';
	end; 						
	if velicina=5 then begin
		repx := 'xresolution","value":1920';
		repy := 'yresolution","value":1080';
	end; 						
	
		
	for i:=0 to SetupDat.count-1 do begin  
		
		// X
		If Contain(SetupDat[i],finX) then Begin
			Print ('IN : ' + IntToStr(i+1) + ' ' + SetupDat[i] );
			SetupDat[i]:=rcReplaceText(SetupDat[i],finX,repX);
			Print ('OUT: ' + IntToStr(i+1) + ' ' + SetupDat[i] );
		End; // If Contain(SetupDat[i],finX)

		// Y
		If Contain(SetupDat[i],finY) then Begin
			Print ('IN : ' + IntToStr(i+1) + ' ' + SetupDat[i] );
			SetupDat[i]:=rcReplaceText(SetupDat[i],finY,repY);
			Print ('OUT: ' + IntToStr(i+1) + ' ' + SetupDat[i] );
		End; // If Contain(SetupDat[i],finY)
		
	end; // for i:=0 to SetupDat.count-1
	
	Print ('podesi_velicinu end');	
End;

Procedure Save_ini;
// spremanje kvalitete, velicine, rendera u ini file
Begin
	print ('procedura save_ini start');
	if FileExists(put+'lux.ini') then begin
			set_value_str(put+'lux.ini','quality',IntToStr(kvaliteta));
			set_value_str(put+'lux.ini','size',IntToStr(velicina));
			set_value_str(put+'lux.ini','engine',IntToStr(render))	
		end else begin
			Showmessage ('Ne mogu pisati u ini datoteku' +#13#10+ 'Probleme slutim')	;
	end;
	print ('procedura Load_ini end');
End;

procedure napravi(sender:TObject);
var
	i:Integer;
	PutOut:string;
begin
	PrintTime ('Proc Napravi Start');
	Print ('Kvaliteta: '+IntToStr(kvaliteta));
	Print ('Velcina: '+IntToStr(velicina));
	Print ('Render: '+IntToStr(render));
	
	PutOut:=PrgFolder+'system\Media\lux\setup.lux';
	
	// Pozivanje setup datoteke
	ucitaj_Setup_file;
	
	// Promjena kvalitete
	if render=1 then podesi_setup_CPU
					else podesi_setup_GPU;
					
	// Promjena velicine slike
	promijeni_velicinu;				
	
	
	// Spremanje setup datoteke
	if fileExists(Putout) then SetupDat.SaveToFile(PutOut)
								 else print('Nisam zapisao u '+ PutOut);
	
	Save_ini;
	Print ('Kvaliteta: '+IntToStr(kvaliteta));
	Print ('Velcina: '+IntToStr(velicina));
	Print ('Render: '+IntToStr(render));
	
	PrintTime ('Proc Napravi End;');
	
End;

procedure Izadji(sender:TObject);
begin
	// 
End;

	
Procedure kreiraj_formu;
var
	i:Integer;
Begin
	Main:=rcTool('LuxRender');

	Main.Position:=poScreenCenter;
	Main.Width:=526; Main.Height:=768;
	
	// Naslov
	
	Pan_Title:=rcPanel( '', Main);
	Pan_Title.Height:=88; Pan_Title.Top:=2000; Pan_Title.Align:=AlTop;
	rcLoadImage('naslov',put+'LuxTitle.jpg', Pan_Title, 10, 15);
	
	// Kvaliteta Pan_Size, Pan_But:TPanel;
	
	Pan_Q:=rcPanel( '', Main);
	Pan_Q.Height:=320; Pan_Q.Top:=2000; Pan_Q.Align:=AlTop;
	
	Lab_Q:=rcLabel('', 'Kvaliteta renderinga',Pan_Q, 10, 8, TALeftJustify);
	Lab_Q.Font.Size:=10;
	
	
	for i:=1 to 5 do begin
		Slika[i]:=rcLoadImage('',put+'LS'+IntToStr(i)+'.jpg',Pan_Q, 10, 30);
	end;
	
	Q_Scroll:=rcScroll('',Pan_Q, 153, 288);
	Q_Scroll.Height:=20;
	Q_Scroll.Width:=200;
	Q_Scroll.Min:=1;
	Q_Scroll.Max:=5;
	 
	
	Q_Scroll.OnChange:=@Q_Scroll_OnChange;
	
	Lab_Q:=rcLabel('', 'Brzo', Pan_Q, 143, 290, TARightJustify);
	Lab_Q.Font.Size:=10;
	Lab_Q:=rcLabel('', 'Kvalitetno', Pan_Q, 360, 290, TALeftJustify);
	Lab_Q.Font.Size:=10;

	// Veli�ina slike Pan_But:TPanel;
	
	Pan_Size:=rcPanel( '', Main);
	Pan_Size.Height:=180; Pan_Size.Top:=2000; Pan_Size.Align:=AlTop;
	
	Lab_Q:=rcLabel('', 'Veli�ina slike',Pan_Size, 10, 8, TALeftJustify);
	Lab_Q.Font.Size:=10;
	
	But[5]:=rcSpeedButton('', 'FHD   1920 x 1080',Pan_Size, 10, 25);
	But[5].Width:=500;
	But[5].Font.Style:=[FsBold];
	
	
	But[4]:=rcSpeedButton('', 'HD   1280 x 720',Pan_Size, 40, 55);
	But[4].Width:=440;
	But[4].Font.Style:=[];

	But[3]:=rcSpeedButton('', 'SD   768 x 432',Pan_Size, 70, 85);
	But[3].Width:=380;
	But[3].Font.Style:=[];

	But[2]:=rcSpeedButton('', 'M   640 x 360',Pan_Size, 100, 115);
	But[2].Width:=320;
	But[2].Font.Style:=[];
	
	But[1]:=rcSpeedButton('', 'S   320 x 180',Pan_Size, 130, 145);
	But[1].Width:=260;
	But[1].Font.Style:=[];
	
	for i:=1 to 5 do begin
		But[i].tag:=i;
		// But[i].hint:=intToStr(i);
	end;	
	
	for i:=1 to 5 do begin
		But[i].OnClick:=@But_OnClick;
	end;	
	
	// Na�in renderinga Pan_xPU
	
	Pan_xPU:=rcPanel( '', Main);
	Pan_xPU.Height:=105; Pan_xPU.Top:=2000; Pan_xPU.Align:=AlTop;
	
	Lab_Q:=rcLabel('', 'Na�in renderinga',Pan_xPU, 10, 8, TALeftJustify);
	Lab_Q.Font.Size:=10;
	
	SL_CPU0:=rcLoadImage('cpu0',put+'CPU0.jpg', Pan_xPU, 10, 30);
	SL_CPU1:=rcLoadImage('cpu1',put+'CPU1.jpg', Pan_xPU, 10, 30);
	SL_CPU0.tag:=1;
	
	Lab_Q:=rcLabel('', 'Glavni procesor (CPU)', Pan_xPU, 80, 30, TALeftJustify);
	Lab_Q.Font.Size:=9;
	
	SL_GPU0:=rcLoadImage('gpu0',put+'GPU0.jpg', Pan_xPU, 255, 30);
	SL_GPU1:=rcLoadImage('gpu1',put+'GPU1.jpg', Pan_xPU, 255, 30);
	SL_GPU0.tag:=2;
	
	Lab_Q:=rcLabel('', 'Grafi�ka kartica (GPU)', Pan_xPU, 325, 30, TALeftJustify);
	Lab_Q.Font.Size:=9;
	
	SL_CPU0.OnClick:=@SL_render_OnClick;
	SL_GPU0.OnClick:=@SL_render_OnClick;
	
	// Pomoc, Cancel, OK
	
	Pan_But:=rcPanel( '', Main);
	Pan_But.Height:=105; Pan_But.Top:=2000; Pan_But.Align:=AlTop;
			
	Bt_Pomoc:=rcButton('', 'Pomo�',  Pan_But, 10, 10);
	Bt_Pomoc.enabled:=False;
	
	Bt_Cancel:=rcButton('', 'Odustajem',  Pan_But, 200, 10);
	Bt_Cancel.OnClick:=@Izadji; 
	Bt_Cancel.ModalResult:=mrCancel;  
	
	
	Bt_Ok:=rcButton('', 'OK',  Pan_But, 390, 10);
	Bt_Ok.OnClick:=@Napravi; 
	Bt_Ok.ModalResult:=mrOk;
	
	
End;

Procedure podesi_kvalitetu;
// poka�i adekvatnu sliku
var
	i:integer;
Begin
	print ('Proc podesi_kvalitetu start');
	For i:=1 to 5 do begin
		if i=kvaliteta then slika[i].visible:=true
							else slika[i].visible:=false;
		Q_Scroll.position:=kvaliteta;
		Q_Scroll.Hint:=IntToStr(Q_Scroll.Position);
	End;
	print ('kvaliteta: '+IntToStr(kvaliteta));
	print ('Proc podesi_kvalitetu end');
End;

Procedure podesi_velicinu;
// poboldaj adekvatni font
var
	i:integer;
Begin
	print ('Proc podesi_velicinu start');
	For i:=1 to 5 do begin
		if i=velicina then But[i].font.style:=[fsBold]
						  else But[i].font.style:=[];
	End;
	print ('velicina: '+IntToStr(velicina));
	print ('Proc podesi_velicinu end');
End;

Procedure podesi_render;
Begin
	print ('Proc podesi_render start');

	if render=1 then begin 
			SL_CPU1.visible:=true;
			SL_GPU1.visible:=false
		end else begin
			SL_CPU1.visible:=false;
			SL_GPU1.visible:=true
	end;
	print ('render: '+IntToStr(render));
	print ('Proc podesi_render end');
End;

Procedure Load_ini;
// �itanje kvaliteta, velicina, render : Integer;
Begin
	print ('procedura Load_ini start');
	if FileExists(put+'lux.ini') then begin
		kvaliteta:=find_value_int(put+'lux.ini','quality');
		velicina:=find_value_int(put+'lux.ini','size');
		render:=find_value_int(put+'lux.ini','engine');
	end else begin
		Showmessage ('Nedostaje ini datoteka' +#13#10+ 'Probleme slutim')	;
	end;
	
	Print ('Kvaliteta: '+IntToStr(kvaliteta));
	Print ('Velcina: '+IntToStr(velicina));
	Print ('Render: '+IntToStr(render));

	print ('procedura Load_ini end');
End;



{$I ..\..\mSvi.cps}

// BEGIN	je u mSvi.cps

	SakrijSve;	
	PrintTime('----------------------------');
	PrintTime('Skripta Lux start');

	put:=ScriptFolder+'RC_Tools\Right\mSetup\lux\';
	
	kreiraj_formu;
	load_ini;					// citanje zadnjeg pode�avanje
	podesi_kvalitetu;			// pode�avanje na temelju zadnjeg 
	podesi_velicinu;			// - II - 
	podesi_render;				// - II -
   
   Main.ShowModal;
	
{
	Main.Show;
	while Main.visible do begin
      application.processmessages;
   end;
}
	
   Main.free; 
   

	
   PrintTime('Skripta Lux end');
	PrintTime('----------------------------');
End.