program find_controls;
var
   win:TForm; lab:TLabel; but:TButton; pan:TPanel;
   s_panel,s_label,s_button:Tbutton; 
   p_edi,l_edi,b_edi:TEdit;
   res_name, res_x, res_y:TLabel;

// functions for creating controls       
function rPanel(name:string; parent: TWinControl; x,y,w,h:Integer):TPanel;
// make panel
var p1:Tpanel;
begin
   p1:=Tpanel.create(parent); p1.parent:=parent; p1.name:=name;
   p1.Left:=x; p1.Top:=y; p1.width:=w; p1.height:=h;
   result:=p1;
end;
function rButton(name:string; parent: TWinControl; x,y:Integer):TButton;
// make button
var  b1:TButton;
begin
   b1:=TButton.create(parent); b1.parent:=parent; b1.name:=name;
   b1.Left:=x; b1.Top:=y; 
   result:=b1;
end;
function rLabel(lname:string; lparent: TWinControl; lx,ly:Integer):TLabel;
// make label
var  L1:TLabel;
begin
   L1:=TLabel.create(lparent); L1.parent:=lparent; L1.name:=lname;
   L1.Left:=lx; L1.Top:=ly;
   result:=L1;
end;
function rEdit(name:string; parent: TWinControl; x,y:Integer):TEdit;
// make edit
var  E1:TEdit;
begin
   E1:=TEdit.create(parent); E1.parent:=parent; E1.text:=name;
   E1.Left:=x; E1.Top:=y;
   result:=E1;
end;

// functions for search controls
function find_control(root_object:TWinControl; c_type, c_name:string):TObject;
var
   i:integer;
   xo:TObject;
begin
  // writeln ('Trying to found control: '+c_name);
  if (c_type<>'label') and (c_type<>'panel') and (c_type<>'button') and 
     (c_type<>'edit')  and (c_type<>'image') and (c_type<>'shape') then begin
            showmessage( 'Function:' +#13#10+
                         'find_control(root_object:TWinControl; c_type, c_name:string):TObject'+#13#10+
                         #13#10+
                         'Unknow control type: "'+c_type+'"'+#13#10+
                         'Valid types: label, panel, button, edit, image, shape'+#13#10+
                         #13#10+
                         'result: NIL');
            exit;
  end;   
  for i:=0 to root_object.ControlCount-1 do begin
     // writeln ('Analising object number '+IntToStr(i));
     xo:=root_object.Controls[i];
     if (UpperCase(c_type)='LABEL')  and (xo is Tlabel)  then if UpperCase(c_name)=UpperCase(TLabel(xo).name)  then result:=xo;
     if (UpperCase(c_type)='PANEL')  and (xo is Tpanel)  then if UpperCase(c_name)=UpperCase(TPanel(xo).name)  then result:=xo;
     if (UpperCase(c_type)='BUTTON') and (xo is TButton) then if UpperCase(c_name)=UpperCase(TButton(xo).name) then result:=xo;
     if (UpperCase(c_type)='EDIT')   and (xo is TEdit)   then if UpperCase(c_name)=UpperCase(TEdit(xo).name)   then result:=xo;
     if (UpperCase(c_type)='IMAGE')  and (xo is TMImage) then if UpperCase(c_name)=UpperCase(TMImage(xo).name) then result:=xo;
     if (UpperCase(c_type)='SHAPE')  and (xo is TShape)  then if UpperCase(c_name)=UpperCase(TShape(xo).name)  then result:=xo;     
     if (result=nil) and (xo is TWinControl)  then result:=find_control(TWinControl(xo),c_type,c_name); 
  end;  // for 
end;  // function

function find_panel(root:TWinControl; c_name:string):TPanel;
begin result:=TPanel(find_control(root, 'panel', c_name));end;

function find_label(root:TWinControl; c_name:string):TLabel;
begin result:=TLabel(find_control(root, 'label', c_name));end;

function find_button(root:TWinControl; c_name:string):TButton;
begin result:=TButton(find_control(root, 'button', c_name));end;

function find_edit(root:TWinControl; c_name:string):TEdit;
begin result:=TEdit(find_control(root, 'edit', c_name));end;

function find_image(root:TWinControl; c_name:string):TMImage;
begin result:=TMImage(find_control(root, 'image', c_name));end;

function find_shape(root:TWinControl; c_name:string):TShape;
begin result:=TShape(find_control(root, 'shape', c_name));end;


// event for buttons click
procedure find_click(sender:TObject);
var my_panel:TPanel;
    my_label:TLabel;
    my_Button:TButton;
begin
    res_name.caption:='not found';
    res_x.caption:='not exist';
    res_y.caption:='not exist';
     
    if TButton(sender).name='Find_panel' then my_panel:=find_panel(win,p_edi.text);
    if TButton(sender).name='Find_label' then my_label:=find_label(win,l_edi.text);
    if TButton(sender).name='Find_button' then my_button:=find_button(win,b_edi.text);
    
    if my_panel<>nil then begin res_name.caption:=my_panel.name;
                                res_x.caption:=IntToStr(my_panel.left);
                                res_y.caption:=IntToStr(my_panel.top); end;      
        
    if my_label<>nil then begin res_name.caption:=my_label.name;
                                res_x.caption:=IntToStr(my_label.left);
                                res_y.caption:=IntToStr(my_label.top); end; 
                                
    if my_button<>nil then begin res_name.caption:=my_button.name;
                                 res_x.caption:=IntToStr(my_button.left);
                                 res_y.caption:=IntToStr(my_button.top); end;                                 
end;

// main ==============================================
         
begin
   // create form  
   win:=TForm.create(nil);
   win.width:=600; win.height:=600;
   
   // create some controls on form
   lab:=rLabel ('Label_1',  win,  20, 20);
   but:=rButton('Button_1', win,  10, 40);
   
   pan:=rPanel ('Panel_10',  win, 100, 10, 200, 500);
   lab:=rLabel ('Label_11',  pan,  10, 10);
   but:=rButton('Button_12', pan,  10, 30);
   pan:=rPanel ('Panel_13',  pan,  10, 70, 180, 150);
   
   lab:=rLabel ('Label_100',  pan,  10, 10);
   but:=rButton('Button_101', pan,  10, 30);
 
   // search controls 
   s_panel:=rButton('Find_panel', win,  320, 10);
   s_panel.OnClick:=@find_click;
   p_edi:=rEdit('panel_10',win,400,10);
    
   s_button:=rButton('Find_button', win,  320, 40);
   s_button.OnClick:=@find_click;
   b_edi:=rEdit('button_1',win,400,40);
   
   s_label:=rButton('Find_label', win,  320, 70);
   s_label.OnClick:=@find_click;
   l_edi:=rEdit('label_1',win,400,70);
  
   res_name:=rLabel ('control_name',  win,  320, 100);
   res_x:=rLabel ('control_x',  win,  320, 130);
   res_y:=rLabel ('control_y',  win,  320, 160);
     
   // show form and wait 
   win.showmodal;

   // destroy form
   win.free; 
end.