Program Gerung;   
// Red Cat Ltd 
// v1.0 15.06.2015. 
// v1.1 22.02.2017.
// - promijenjena po�etna pozicija prozora na gore-lijevo
// - izmijenjen naslov prozora u Red Cat
// v1.2 09.02.2019.
// - dodaje se primjedba u dasku u slu�aju postavljanja gerunga
// - bri�e se primjedba ako gerunga nema
// - mogu� je problem s postoje�im primjedbama koje �e biti obrisane 


{$I ..\include\rcPrint.cps}
{$I ..\include\rcMessages.cps}
{$I ..\include\rcStrings.cps}
{$I ..\include\rcControls.cps}
{$I ..\include\rcObjects.cps}
{$I ..\include\rcEvents.cps}
{$I ..\include\rcCurves.cps}
{$I ..\include\rcMath.cps}


var

   
   // ===============================================================================
	// Osnovni dio forme - uvijek isti
   Main:TForm;
   Panel_Left, Panel_right, Panel_Title, Panel_Memo : TPanel;
	ButOK,ButTest,ButCancel:TButton;
   // cbDebug:TCheckBox; MemoJ:TMemo;  definirani na vrhu glavne skripte!!!
   LabTemp:TLabel;      // stati�ne labele
	Debug:boolean;    // za debug poruke u Journalu
	// ===============================================================================
	// Dio koji je specifican za skriptu
	Panel_Side0 : TPanel;
		PPR0 :TPanel;
			PEdit0 : TEdit;
		PPL0 :TPanel;	
			PNaziv0 : TPanel;
			TNaziv0 : TLabel;
			PScroll0 : TScrollBar;
			Bminus0, Bnula0, Bplus0 : TButton;
	Razmak0:TPanel;		
	Panel_Side1 : TPanel;
		PPR1 :TPanel;
			PEdit1 : TEdit;
		PPL1 :TPanel;	
			PNaziv1 : TPanel;
			TNaziv1 : TLabel;
			PScroll1 : TScrollBar;
			Bminus1, Bnula1, Bplus1 : TButton;		
	Razmak1:TPanel;
	Panel_Side2 : TPanel;
		PPR2 :TPanel;
			PEdit2 : TEdit;
		PPL2 :TPanel;	
			PNaziv2 : TPanel;
			TNaziv2 : TLabel;
			PScroll2 : TScrollBar;
			Bminus2, Bnula2, Bplus2 : TButton;		
	Razmak2:TPanel;		
	Panel_Side3 : TPanel;
		PPR3 :TPanel;
			PEdit3 : TEdit;
		PPL3 :TPanel;	
			PNaziv3 : TPanel;
			TNaziv3 : TLabel;
			PScroll3 : TScrollBar;
			Bminus3, Bnula3, Bplus3 : TButton;					
	
	
	
	
	
	td:Tdaska;
	tc:TTockeSystem;
	
Procedure CreateForm;

Begin
	{$I GerungMainForm.cps}    // crtanje forme
				
   ShowN('Gerung v1.1 22.02.2017. ');
	
	
	
	Main.Height:=360; Main.Width:=480;  Main.top:=300; 
	// Main.Position:=poDesigned; // poScreenCenter;
	Main.Top:=105;
	Main.Left:=5;
	Main.BorderStyle:=bsDialog; // bsNone;
	Main.Caption:='Red Cat';
	// Main.Top:=100;
	// Main.Left:=10;
   Main.show;           // poka�i formu
End;	

Procedure ScrollChange(sender:TObject);


Begin
	If TScrollBar(Sender).tag=0 then PEdit0.Text:=IntToStr(TScrollBar(Sender).position);
	If TScrollBar(Sender).tag=1 then PEdit1.Text:=IntToStr(TScrollBar(Sender).position);
	If TScrollBar(Sender).tag=2 then PEdit2.Text:=IntToStr(TScrollBar(Sender).position);
	If TScrollBar(Sender).tag=3 then PEdit3.Text:=IntToStr(TScrollBar(Sender).position);

End;

procedure CheckIfReal(Sender: TObject);    // On Change
// provjera da li je realni broj u vrijeme upisa u edit polje
var
   rr:single;
   pass:boolean;
   i,j,tt:Integer;
	pt,zt:string;
begin
   // Print('procedure CheckIfReal(Sender: TObject);');
   pass:=True;
	// Provjera da li je prazno polje. Ako je upisuje se 0 i selektira se
	if Length(Tedit(Sender).text)<1 then Begin 
														Tedit(Sender).text:='0';
														TEdit(Sender).SelectAll;
													 end;
	// Provjera da li je upisan samo znak minus. Ako je dodaje se "0" i selektira se "0"
	if (Length(Tedit(Sender).text)=1) and (Tedit(Sender).text='-') then 
													Begin 
														Tedit(Sender).text:='-0';
														TEdit(Sender).SelStart:=1;
														TEdit(Sender).SelLength:=1;
													end;
	// Provjera da li je upisan znak minus koji nije na po�etku. Tada se bri�e iz stringa.
	// If (Contain(Tedit(Sender).text,'-')) and (Copy(Tedit(Sender).text,1,1)<>'-') then
	If (Contain(Copy(Tedit(Sender).text,2,100),'-')) then
							// kopiraj znak po znak ali presko�i "-"
							Begin
								pt:=Tedit(Sender).text;		// pocetni tekst
								zt:=copy(pt,1,1);				// zavrsni tekst kojem je odmah dodijeljen prvi znak
								For i:=2 to Length(pt) do begin
									if copy(pt,i,1)<>'-' then zt:=zt+copy(pt,i,1);
								End;
								// vrati kursor na kraj polja
								Tedit(Sender).text:=zt;   // vrati text u edit
								TEdit(Sender).SelStart:=10;  //Length(Tedit(Sender).text);
								TEdit(Sender).SelLength:=1;
								
								
							End;
													
													
   try
       rr:=StrToFloat(Tedit(Sender).text);
   except
       pass:=False; 
		 // Print('Nije broj!');
   end;
   
	if (pass=true) and (StrToFloat(Tedit(Sender).text)<-85) then pass:=false;
	if (pass=true) and (StrToFloat(Tedit(Sender).text)>85) then pass:=false;
	
   if pass=true then begin
         Tedit(Sender).font.color:=clWindowText;
         Tedit(Sender).font.style:=[];
         Tedit(Sender).color:=clWindow;
         ButOk.enabled:=True;
         
      end else begin
         Tedit(Sender).font.color:=clRed;
         Tedit(Sender).font.style:=[fsBold];
         Tedit(Sender).color:=clYellow;
         ButOk.enabled:=False;
   end;
	
	// promjena varijabli u krivuljama
	
	if pass=true then begin
		
		If Tedit(Sender).Tag=0 then rcCurVarAdd(tc,'G_down',Tedit(Sender).Text);
		If Tedit(Sender).Tag=1 then rcCurVarAdd(tc,'G_right',Tedit(Sender).Text);
		If Tedit(Sender).Tag=2 then rcCurVarAdd(tc,'G_up',Tedit(Sender).Text);
		If Tedit(Sender).Tag=3 then rcCurVarAdd(tc,'G_left',Tedit(Sender).Text);
	
		
		
		
		// Print('Mijenjam Gerung!'+' Daska '+td.naziv+' Tag: '+IntToStr(Tedit(Sender).Tag));
		rcRefresh;
	End;
	

end;

Procedure BOnClick(Sender:TObject);
Begin

// rje�ava tipke koje odre�uju stupnjeve
   if TButton(Sender).tag=0 then PScroll0.position:=-45;
	if TButton(Sender).tag=1 then PScroll0.position:=0;
	if TButton(Sender).tag=2 then PScroll0.position:=45;
	
	if TButton(Sender).tag=10 then PScroll1.position:=-45;
	if TButton(Sender).tag=11 then PScroll1.position:=0;
	if TButton(Sender).tag=12 then PScroll1.position:=45;
	
	if TButton(Sender).tag=20 then PScroll2.position:=-45;
	if TButton(Sender).tag=21 then PScroll2.position:=0;
	if TButton(Sender).tag=22 then PScroll2.position:=45;
	
	if TButton(Sender).tag=30 then PScroll3.position:=-45;
	if TButton(Sender).tag=31 then PScroll3.position:=0;
	if TButton(Sender).tag=32 then PScroll3.position:=45;

End;	


Procedure rcClearPlate(pDaska:TDaska);
var
	i:integer;
	AllCurve:TTockeSystem;
	curve : TRubneTocke;
Begin
	Print('rcClearPlate START');
	// bri�i ekstra krivulje
	
	AllCurve:=pDaska.TockeiRupe;
	for i:=allCurve.count downto 1 do begin
	   // Print ('Obrisat cu krivulju krivulju: '+IntToStr(i));
		AllCurve.RemoveCurve(i);
		// Print ('Obrisao sam krivulju: '+IntToStr(i));
	End;

	// postavi rubne to�ke na po�etna mjesta
	curve:=pDaska.TockeiRupe.rubne;
	curve:=rcClearMainCurve(curve);
	
	// izbri�i sve varijable iz krivulja
	For i:=AllCurve.varijable.count-1 downto 0  do begin;
		AllCurve.varijable.delete(i);
		// Print('Bri�em var '+IntToStr(i));
	End;
	
	// stavi sve vrijednosti PEdita na 0 jer se na temelju njih rade ili bri�u primjedbe
	Pedit0.text:='0';
	Pedit1.text:='0';	
	Pedit2.text:='0';	
	Pedit3.text:='0';

	rcRefresh;
	
	Print('rcClearPlate END');
End;


	
Procedure Zatvori(Sender:Tobject);
Begin
	rcClearPlate(td);
	// Main.Close;   
	// Main.free; 
End;
	
Procedure ChangeGerung(pDaska:tDaska);
var
	AllCurve:TTockeSystem;
Begin
	Print('ChangeGerung START');
	
	// Postavljanje natpisa ovisno o orjentaciji daske
	If pDaska.smjer = Horiz then Begin 
												TNaziv0.Caption:='L I J E V O';
												TNaziv1.Caption:='O T R A G A';
												TNaziv2.Caption:='D E S N O';
												TNaziv3.Caption:='N A P R I J E D';
									end;
									
	If pDaska.smjer = VertBok then Begin 
												TNaziv0.Caption:='D O L J E';
												TNaziv1.Caption:='O T R A G A';
												TNaziv2.Caption:='G O R E';
												TNaziv3.Caption:='N A P R I J E D';
									end;
	
	If pDaska.smjer = VertFront then Begin 
												TNaziv0.Caption:='D O L J E';
												TNaziv1.Caption:='D E S N O';
												TNaziv2.Caption:='G O R E';
												TNaziv3.Caption:='L I J E V O';
									end;
	
	AllCurve:=pDaska.TockeiRupe;
	
   // citanje varijabli iz krivulja i punjenje komponenti
	
	PEdit0.Text:=rcCurVarVal(AllCurve,'G_down');
	PEdit0.Tag:=0;
	PScroll0.Min:=-80; PScroll0.Max:=80; PScroll0.Tag:=0;
	PScroll0.Position:=StrToInt(PEdit0.Text); 
	PEdit0.OnKeyPress:=@rcCheckReal;
	PEdit0.OnChange:=@CheckIfReal;
	
	PEdit1.Text:=rcCurVarVal(AllCurve,'G_right');
	PEdit1.Tag:=1;
	PScroll1.Min:=-80; PScroll1.Max:=80; PScroll1.Tag:=1;
	PScroll1.Position:=StrToInt(PEdit1.Text); 
	PEdit1.OnKeyPress:=@rcCheckReal;
	PEdit1.OnChange:=@CheckIfReal;
	
	PEdit2.Text:=rcCurVarVal(AllCurve,'G_up');
	PEdit2.Tag:=2;
	PScroll2.Min:=-80; PScroll2.Max:=80; PScroll2.Tag:=2;
	PScroll2.Position:=StrToInt(PEdit2.Text); 
	PEdit2.OnKeyPress:=@rcCheckReal;
	PEdit2.OnChange:=@CheckIfReal;
	
	PEdit3.Text:=rcCurVarVal(AllCurve,'G_left');
	PEdit3.Tag:=3;
	PScroll3.Min:=-80; PScroll3.Max:=80; PScroll3.Tag:=3;
	PScroll3.Position:=StrToInt(PEdit3.Text); 
	PEdit3.OnKeyPress:=@rcCheckReal;
	PEdit3.OnChange:=@CheckIfReal;
	
	// eventi od scrollbara mijenjaju edite, a editi mijenjaju varijable u dasci
	
	PScroll0.OnChange:=@ScrollChange;
	PScroll1.OnChange:=@ScrollChange;
	PScroll2.OnChange:=@ScrollChange;
	PScroll3.OnChange:=@ScrollChange;
	
	// eventi od tipki za kuteve
	
	Bminus0.tag:=0; 	Bminus0.OnClick:=@BOnClick;
	BNula0.tag:=1;		BNula0.OnClick:=@BOnClick;
	BPlus0.tag:=2;		BPlus0.OnClick:=@BOnClick;
	
	Bminus1.tag:=10; 	Bminus1.OnClick:=@BOnClick;
	BNula1.tag:=11;	BNula1.OnClick:=@BOnClick;
	BPlus1.tag:=12;	BPlus1.OnClick:=@BOnClick;
	
	Bminus2.tag:=20; 	Bminus2.OnClick:=@BOnClick;
	BNula2.tag:=21;	BNula2.OnClick:=@BOnClick;
	BPlus2.tag:=22;	BPlus2.OnClick:=@BOnClick;
	
	Bminus3.tag:=30; 	Bminus3.OnClick:=@BOnClick;
	BNula3.tag:=31;	BNula3.OnClick:=@BOnClick;
	BPlus3.tag:=32;	BPlus3.OnClick:=@BOnClick;
            
   // ButOk.OnClick:=@Napravi; // ne mora ni�ta raditi
	
   ButCancel.OnClick:=@Zatvori; 
      
   ButOk.ModalResult:=mrOk;
   ButCancel.ModalResult:=mrCancel;  
   
   if e<>nil then e.RecalcFormula(e); 
   // Print('==========');
   // Print('SKRIPT END');
   // Print('==========');
	
	
	
	Print('ChangeGerung END');
   Main.Hide;     
   Main.ShowModal;   
   Main.free; 

End;


Function GerungVer(pDaska:Tdaska):integer;
// Provjera koja je verzija gerunga na dasci (ili ga uop�e nema)
var
	AllCurve:TTockeSystem;				// sve krivulje u dasci
	ver:integer;
Begin
	Print('GerungVer START');
	result:=0;
	AllCurve:=pDaska.TockeiRupe
	If rcCurVarEx(AllCurve,'GerungVer') then    // ako postoji varijablja GerungVer
		   if rcCurVarVal(AllCurve,'GerungVer')='1' then result := 1;	
	// Print('result: '+IntToStr(result));
	Print('GerungVer END');
End;

Function noCurves(pDaska:Tdaska):boolean;
// Provjera da li je daska bez ikakvih pomaknutih to�aka ili dodatnih krivulja
var
	AllCurve:TTockeSystem;
	curve : TRubneTocke;
	c1,c2,c3:integer;
	td:tdaska;
	x0,y0,z0:single;
Begin
	Print('noCurves START');
	// koliko ima krivulja?
	AllCurve:=pDaska.TockeiRupe;
	c1:=allCurve.count+1;
	// Print('Broj krivulja: '+IntToStr(c1)); 
	
	// koliko to�aka ima vanjska?
	curve:=pDaska.TockeiRupe.rubne;
	c2:=curve.count;
	// Print('Broj to�aka u vanjskoj: '+IntToStr(c2));  
	
	// jesu li sve to�ke na svojim mjestima?
	td:=curve.parentd;
	if 	(rcPointReadX(curve,0)=0) and 
			(rcPointReadY(curve,0)=0) and 
			(rcPointReadZ(curve,0)=0) and
			
			(rcPointReadX(curve,1)=0) and 
			(rcPointReadY(curve,1)=td.visina) and 
			(rcPointReadZ(curve,1)=0) and
			
			(rcPointReadX(curve,2)=td.sirina) and 
			(rcPointReadY(curve,2)=td.visina) and 
			(rcPointReadZ(curve,2)=0) and
			
			(rcPointReadX(curve,3)=td.sirina) and 
			(rcPointReadY(curve,3)=0) and 
			(rcPointReadZ(curve,3)=0) 
			then c3:=0
		else c3:=1;
	
	
	//x0:=rcPointReadX(curve,0);
	// Print('x0='+FloatToStrC1(rcPointReadX(curve,0),8,2));
	// Print('y0='+FloatToStrC1(rcPointReadY(curve,0),8,2));
	// Print('Z0='+FloatToStrC1(rcPointReadZ(curve,0),8,2));
	
	// Print('x1='+FloatToStrC1(rcPointReadX(curve,1),8,2));
	// Print('y1='+FloatToStrC1(rcPointReadY(curve,1),8,2));
	// Print('Z1='+FloatToStrC1(rcPointReadZ(curve,1),8,2));
	
	// Print('x2='+FloatToStrC1(rcPointReadX(curve,2),8,2));
	// Print('y2='+FloatToStrC1(rcPointReadY(curve,2),8,2));
	// Print('Z2='+FloatToStrC1(rcPointReadZ(curve,2),8,2));
	
	// Print('x3='+FloatToStrC1(rcPointReadX(curve,3),8,2));
	// Print('y3='+FloatToStrC1(rcPointReadY(curve,3),8,2));
	// Print('Z3='+FloatToStrC1(rcPointReadZ(curve,3),8,2));
	
	
	
	
	
	// Print('Broj krivulja (c1): '+IntToStr(c1));
	// Print('Broj to�aka u vanjskoj (c2): '+IntToStr(c2));
	// Print('To�ke na dobrim mjestima (c3): '+IntToStr(c3));
	
	
	if (c1>1) or (c2>4) or (c3>0)	then result:=false
											else result:=true;
	// Showmessage('2');
	// Print('result: '+ rcBoolToStr(result));
	Print('noCurves END');
End;

Procedure CreateGerung(pDaska:Tdaska);
var
	AllCurve:TTockeSystem;
	curve, nCurve : TRubneTocke;
	i:integer;
begin
	Print('CreateGerung START');
	AllCurve:=pDaska.TockeiRupe;
	Curve:=AllCurve.Rubne;			// ili curve:=pDaska.TockeiRupe.rubne;
	
	// izbri�i postoje�e varijable
	For i:=AllCurve.varijable.count-1 downto 0  do begin;
		AllCurve.varijable.delete(i);
		// Print('Bri�em var '+IntToStr(i));
	End;

	
	// prebaci varijable
	AllCurve.varijable.add('G_up=0');
	AllCurve.varijable.add('G_down=0');
	AllCurve.varijable.add('G_left=0');
	AllCurve.varijable.add('G_right=0');
	AllCurve.varijable.add('//YLDB=Y Left Down Back');
	AllCurve.varijable.add('YLDB=if(G_down>0;de*tan(G_down);0)');
	AllCurve.varijable.add('//YRDB=Y Right Down Back');
	AllCurve.varijable.add('YRDB=YLDB');
	AllCurve.varijable.add('//YLDF=Y Left Down Front');
	AllCurve.varijable.add('YLDF=if(G_down<0;0-de*tan(G_down);0)');
	AllCurve.varijable.add('//YRDF=Y Right Down Front');
	AllCurve.varijable.add('YRDF=YLDF');
	AllCurve.varijable.add('//ZLDF=Z Left Down Front');
	AllCurve.varijable.add('ZLDF=DE');
	AllCurve.varijable.add('ZRDF=DE');
	AllCurve.varijable.add('ZLUF=DE');
	AllCurve.varijable.add('ZRUF=DE');
	AllCurve.varijable.add('ZLDB=0');
	AllCurve.varijable.add('ZRDB=0');
	AllCurve.varijable.add('ZLUB=0');
	AllCurve.varijable.add('ZRUB=0');
	AllCurve.varijable.add('// YLUF=Y Left Up Front');
	AllCurve.varijable.add('YLUF=IF(G_up<0;V+de*tan(G_up);V)');
	AllCurve.varijable.add('// YRUF=Y Right Up Front');
	AllCurve.varijable.add('YRUF=YLUF');
	AllCurve.varijable.add('// YLUB=Y Left Up Back');
	AllCurve.varijable.add('YLUB=IF(G_Up>0;V-de*tan(G_up);V)');
	AllCurve.varijable.add('// YRUB=Y Right Up Back');
	AllCurve.varijable.add('YRUB=YLUB');
	AllCurve.varijable.add('// XLDF=X left Down Front');
	AllCurve.varijable.add('XLDF=IF(G_Left<0;-de*tan(G_left);0)');
	AllCurve.varijable.add('// XLUF=X Left Up Front');
	AllCurve.varijable.add('XLUF=XLDF');
	AllCurve.varijable.add('// XLDB=X left Down Back');
	AllCurve.varijable.add('XLDB=IF(G_Left>0;de*tan(G_left);0)');
	AllCurve.varijable.add('// XLUB=X left UP Back');
	AllCurve.varijable.add('XLUB=XLDB');
	AllCurve.varijable.add('// XRDF=X Right Down Front');
	AllCurve.varijable.add('XRDF=IF(G_right<0;S+de*tan(G_right);S)');
	AllCurve.varijable.add('// XRUF=X Right Up Front');
	AllCurve.varijable.add('XRUF=XRDF');
	AllCurve.varijable.add('// XRDB=X Right Down Back');
	AllCurve.varijable.add('XRDB=IF(G_right>0;S-de*tan(G_right);S)');
	AllCurve.varijable.add('// XRUF=X Right Up Front');
	AllCurve.varijable.add('XRUF=XRDF');
	AllCurve.varijable.add('GerungVer=1');

	// napravi kopiju krivulje
	nCurve:=TrubneTocke.createParented(pDaska, AllCurve);
	nCurve.kopiraj(Curve);
	AllCurve.addLine(nCurve);
	

	
	// dodijeli vrijednosti to�kama
	curve.MainTocke[0].AStyle:=rLine;
	curve.MainTocke[0].yf:='XLDF';     // Yf je zapravo Xf !!!
	curve.MainTocke[0].xf:='YRDF';
	curve.MainTocke[0].zf:='ZRDF';
	curve.MainTocke[1].AStyle:=rLine;
	curve.MainTocke[1].yf:='XLUF';
	curve.MainTocke[1].xf:='YLUF';
	curve.MainTocke[1].zf:='ZRUF';
	curve.MainTocke[2].AStyle:=rLine;
	curve.MainTocke[2].yf:='XLUB';
	curve.MainTocke[2].xf:='YLUB';
	curve.MainTocke[2].zf:='ZRUB';
	curve.MainTocke[3].AStyle:=rLine;
	curve.MainTocke[3].yf:='XLDB';
	curve.MainTocke[3].xf:='YLDB';
	curve.MainTocke[3].zf:='ZRDB';
	
	ncurve.MainTocke[0].AStyle:=rLine;
	ncurve.MainTocke[0].yf:='XRDF';
	ncurve.MainTocke[0].xf:='YLDF';
	ncurve.MainTocke[0].zf:='ZLDF';
	ncurve.MainTocke[1].AStyle:=rLine;
	ncurve.MainTocke[1].yf:='XRUF';
	ncurve.MainTocke[1].xf:='YLUF';
	ncurve.MainTocke[1].zf:='ZLUF';
	ncurve.MainTocke[2].AStyle:=rLine;
	ncurve.MainTocke[2].yf:='XRDB';
	ncurve.MainTocke[2].xf:='YLUB';
	ncurve.MainTocke[2].zf:='ZLUB';
	ncurve.MainTocke[3].AStyle:=rLine;
	ncurve.MainTocke[3].yf:='XRDB';
	ncurve.MainTocke[3].xf:='YLDB';
	ncurve.MainTocke[3].zf:='ZLDB';
	
	// napravi povr�inu
	AllCurve.SideType:=TSOpen;
	AllCurve.Mode:=RTSurface;
	
	// Zatvori krajeve
	// zatvoreni su po defaultu. na svu sre�u :-)


	rcRefresh;
	Print('CreateGerung END');
End;


Procedure DoGerung(pDaska:tDaska);
var
	i:Integer;
	tekst1:string;
Begin	
	Print('DoGerung START');
	
	tekst1:=	'UPOZORENJE!'+#13#10+#13#10+
				'Ovo nije obi�na daska. U sebi ima dodatne krivulje.'+#13#10+#13#10+
				'Ako odaberete "NASTAVI" sve krivulje �e biti obrisane!'+#13#10+#13#10+
				'Odaberite "ODUSTANI" ako �elite odustati od izrade gerunga.';
	
	// postavljanje globalnih varijabli za dasku i krivulju jer ih ne mogu passat u event
	td:=pDaska;
	tc:=Pdaska.TockeiRupe;
	
	// test da li ve� ima gerung
	If GerungVer(pDaska)=1 	
		then ChangeGerung(pDaska)		// ima gerung!
		else if noCurves(pDaska) 	
					then begin 
								CreateGerung(pDaska);    // ako je �ista daska
								ChangeGerung(pDaska);
					end	else 	begin
								if MessageYesNo(	400,250,'Upozorenje', tekst1,'NASTAVI','ODUSTANI') 
										then begin // odgovor je YES
													rcClearPlate(pDaska);
													CreateGerung(pDaska);
													ChangeGerung(pDaska);
										end else begin;  // if messageYesNo
													// odgovor je NO
													Main.Hide;     
													// Main.ShowModal;   
													Main.Close;   
													Main.free; 
										
								end;
					End;  // else
					
	// upis ili brisanje napomene

	
	
	// Print ('1');
	// pdaska.PrimjedbaList[1]:='primjedba za gerung';
	// print('Daska "'+pdaska.naziv+'" ima '+IntToStr(pdaska.PrimjedbaList.count)+' redova primjedbi/');
	
	print ('PEdit(0)='+Pedit0.text);
	If ( PScroll0.Position=0 ) and
		( PScroll1.Position=0 ) and
		( PScroll2.Position=0 ) and
		( PScroll3.Position=0 ) then begin
			// nema gerunga i treba ga obrisati ako je bio upisan
		   if Contain (pdaska.Primjedba, 'Gerung:') then pdaska.Primjedba:='';
			// Print('Obrisana primjedba jer nema gerunga');
		end else begin
			// ima gerunga i treba ga upisati	
			pdaska.Primjedba:='Gerung: ';
			if Pedit0.text<>'0' then pdaska.Primjedba:=pdaska.Primjedba+'�d( '+Pedit0.text+' st.) ';
			if Pedit1.text<>'0' then pdaska.Primjedba:=pdaska.Primjedba+'Vd( '+Pedit1.text+' st.) ';
			if Pedit2.text<>'0' then pdaska.Primjedba:=pdaska.Primjedba+'�g( '+Pedit2.text+' st.) ';
			if Pedit3.text<>'0' then pdaska.Primjedba:=pdaska.Primjedba+'Vl( '+Pedit3.text+' st.) ';
			// Print('Upisana primjedba zbog gerunga');
		
	end;
	 
	
	
	
	// Print ('2');
					
					
	// Print('DoGerung END');
End;
	
var
   i,j:Integer;
	gDaska:tDaska;
Begin
   PrintTime('Procedure  GERUNG START');
	Debug:=false;         // debuger ispis on/off
   
   // Na�i selektirane objekte
   j:=0;
   if e<>nil then for i:=0 to e.childdaska.CountObj-1 do begin
      if e.childdaska.Daska[i].selected = true then j:=j+1;
   end;
	
   if NOT (j=1) then showmessage('Nije selektira jedna daska!') 
                else begin
								CreateForm;
									
								gDaska:=el_find_sel_plate;
								gDaska.Selected:=False;
								DoGerung(gDaska);
								gDaska.Selected:=True;
					 End;
					 


            
            

   Print('Procedure GERUNG END');
End.

