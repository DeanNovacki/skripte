

{$I ..\include\rcPrint.cps}
{$I ..\include\rcMessages.cps}
{$I ..\include\rcStrings.cps}
{$I ..\include\rcControls.cps}
{$I ..\include\rcObjects.cps}
{$I ..\include\rcConfig.cps}
{$I ..\include\rcEvents.cps}
{$I ..\include\rcCurves.cps}
{$I ..\include\rcMath.cps}
{$I ..\include\rcOut.cps}


var
	versiontxt:string;
	main:TForm;
	izvrseno:boolean;
	ButOK, ButCancel, ButPomoc : TButton;
	Panel_Memo:TScrollBox;
	Memo1,Memo2,Memo3:TMemo;
	LL:TLabel;
	//////////////////////////////////////////
	Polje: Array of String;		// Naziv polja
	Lab : Array of TLabel;			// Polja za nazive
	Mem : Array of TMemo;			// Polja za vrijednosti
	Vis : Array of Boolean;			// prikazuj ili ne
	Imm : Array of TImage;			// Image minus
	Imp : Array of TImage;			// Image plus
	Imh : Array of TImage;			// Image help
	Min : Array of Single;
	Max : Array of Single;
	Step: Array of Single;
	LabH: Array of TLabel;			// Hint
	// --------------------
	
	////////////////////////////////////////////
	
Procedure Kraj_skripte;
Begin
   Print('ZATVARANJE PROGRAMA');
	Main.close;
End;

Procedure MainOnResize(sender:Tobject);
var
	i:integer;
Begin
	IF Main.Width<100 then begin
			Main.Width:=100;	
		end else begin 
			for i:=0 to 8 do begin
				if Main.Width<150  then begin 
							Imm[i].Autosize:=False;	
							Imp[i].Autosize:=False;	
							Imm[i].width:=-1;
							Imp[i].width:=-1;
						end else begin
							Imm[i].Autosize:=True;	
							Imp[i].Autosize:=true;	
					
				end; // if Main.Width<150 
				if (i=0) or (i=8) then Begin
					Imm[i].autosize:=False;
					Imp[i].autosize:=False;
					Imm[i].width:=-1;
					Imp[i].width:=-1;
				End;
				Imp[i].left:=Panel_Memo.ClientWidth-Imp[i].Width;		
				Mem[i].Left:=Imm[i].Left+Imm[i].Width+1;
				Mem[i].Width:=Imp[i].Left-Imm[i].Left-Imm[i].Width-2;
				
				LabH[i].Width:=Panel_Memo.ClientWidth-5;
			end;  // for
	End;  // If Main


End;


	
Procedure OnKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState  );
// Key Down se doga�a prije KeyPressa pa se mo�e pr�itati koja je tipka stisnuta iako 
// ne�e biti detektirana za upis u polje (ili memo)
var
	i:Integer;
	bb,gg,rr:Integer;
Begin
	// Print('Key Down '+FloatToStr(Key));
	If Key=13 then Begin
		// Print ('ENTER');
		TMemo(Sender).Color:=ClGreen;
		WaitMiliSec(100);
		TMemo(Sender).Color:=ClWhite;
		
	End;
	If Key=112 then Print ('F1 HELP');
	If Key=16 then begin   // Shift
		TMemo(Sender).Color:=ClGreen;
		WaitMiliSec(300);
		TMemo(Sender).Color:=ClWhite;
	end; 

end;


Procedure OnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);  

var
	i:Integer;
	bb,gg,rr:Integer;
Begin
	// Print('Mouse Move start');

	For i:=0 to 8 do begin
		// Print('Mouse Move i:='+IntToStr(i));
		
		
		if TMemo(Sender).tag=i then LabH[i].visible:=True
									  else LabH[i].visible:=False;
										
		// Print('Mouse Move i:='+IntToStr(i));
	end;
	
End;
	
Procedure OnMouseDown(Sender: TObject; Button: TMouseButton;  Shift: TShiftState; X, Y: Integer);

var
	i:Integer;
	bb,gg,rr:Integer;
Begin
	// Print('Mouse Down ');
	
// {	
	If ( ssCtrl in Shift ) and ( Button = mbLeft )  then Begin 
			Print('CTRL + Left Click');
			Print (IntToStr(X));
			TMemo(Sender).Cursor:=crSizeNS;
			IF X>100 then x:=10;
			application.processmessages;
		End else begin
			TMemo(Sender).Cursor:=crDefault;
			application.processmessages;
	end;
// }	
	
	
End;

Procedure OnMouseUp(Sender: TObject; Button: TMouseButton;  Shift: TShiftState; X, Y: Integer);

var
	i:Integer;
	bb,gg,rr:Integer;
Begin
	// Print('Mouse Up ');
	// If ssCtrl in Shift then Print('CTRL + Up');
	// TMemo(Sender).Cursor:=crHSplit;
	
	
End;

Procedure OnKeyPress(sender:Tobject; var Key: Char);
// ima globalni OnKeyPress
Begin
	Print('Key Press');
	
	
End;
	
Procedure DodjeliPolja;
// var
// 	i:integer;
Begin
	SetLength(Polje,9);
	Polje[0]:='Naziv';
	Polje[1]:='X';
	Polje[2]:='Y';
	Polje[3]:='Z';
	Polje[4]:='�ir';
	Polje[5]:='Vis';
	Polje[6]:='Dub';
	Polje[7]:='Kut';
	Polje[8]:='Nap';

End;
	
Procedure Create_Main_Window;
var
	i:Integer;
	lv:Integer;			// visina linije
	fs:Integer;			// font size
	SPath:String;
Begin
	sPath:=PrgFolder+'Skripte\RC_Tools\Semafor2\';
	DodjeliPolja;
	lv:=17;
	fs:=9;
	// Startni prozor
   Main:=rcTool('Zamijeni_element'); 
	Main.Width:=400;
	Main.Height:=400;
 	Main.top:=300;
	Main.Left:=800;
	Main.BorderStyle:=bsSizeToolWin;
	Main.BorderIcons:=[biSystemMenu];
	Main.FormStyle:=fsStayOnTop;	

	
	// Main.Left:=200;
	// Main.Position:=poDesigned;
	
	Panel_Memo:=rcScrollBox ( '' ,Main ); 
	Panel_memo.Top:=1;
	Panel_memo.Left:=1;
	Panel_memo.Height:=250;
	Panel_Memo.align:=AlClient;

	
	for i:=0 to 8 do begin
		
		// Labela
		SetLength(Lab,i+1);
		Lab[i]:=rcLabel('','',panel_Memo,40,2,taRightJustify);
		Lab[i].Top:=4+i*lv-1;
		Lab[i].Font.Name:='Tahoma';
		Lab[i].Font.Size:=fs;
		Lab[i].Caption:=Polje[i];
		Lab[i].Font.Color:=clDefault;
		Lab[i].tag:=i;
		if i=1 then begin Lab[i].Font.Color:=clRed;   Lab[i].Font.Style:=[fsBold] end;
		if i=2 then begin Lab[i].Font.Color:=clGreen; Lab[i].Font.Style:=[fsBold] end;
		if i=3 then begin Lab[i].Font.Color:=clBlue;  Lab[i].Font.Style:=[fsBold] end;
		
		
		
		// Image minus
		SetLength(Imm,i+1);
		Imm[i]:=rcLoadImage('',sPath+'minus.bmp',panel_Memo,1,1);
		Imm[i].top:=2+i*lv
		Imm[i].left:=Lab[i].Left+Lab[i].width+3
		Imm[i].tag:=i;
		
		// Image plus
		SetLength(Imp,i+1);
		Imp[i]:=rcLoadImage('',sPath+'plus.bmp',panel_Memo,1,1);
		Imp[i].top:=2+i*lv
		Imp[i].left:=Panel_Memo.Width-Imp[i].Width-3;
		Imp[i].tag:=i;
		Imm[i].width:=-1;    // zbog autosize �e biti ipak dobro prikazane
		
		// Print( 'Pan.width:'+IntToStr(Panel_memo.width));
		
		if (i=0) or (i=8) then Begin
			Imm[i].autosize:=False;
			Imp[i].autosize:=False;
			Imm[i].width:=-1;
			Imp[i].width:=-1;
		End;
		Print('Imm('+IntToStr(i)+').width:='+IntToStr(Imm[i].width));
		
		// Memo
		SetLength(Mem,i+1);
		Mem[i]:=rcMemo('', Panel_Memo, 1,1,20,20);
		Mem[i].top:=1+i*lv;
		Mem[i].Left:=Imm[i].Left+Imm[i].Width+1;
		Mem[i].Width:=Imp[i].Left-Imm[i].Left-Imm[i].Width-2;
		Mem[i].Font.Name:='Tahoma';
		Mem[i].Font.Size:=fs;
		Mem[i].Height:=18;
		Mem[i].ScrollBars:=ssNone;
		// Mem[i].Lines.Add('1234567890.12345678909876543210');
		Mem[i].OnKeyPress:=@rcCheckReal;
		Mem[i].OnKeyDown:=@OnKeyDown;
		Mem[i].OnMouseDown:=@OnMouseDown;
		Mem[i].OnMouseUp:=@OnMouseUp;
		Mem[i].OnMouseMove:=@OnMouseMove;
		Mem[i].tag:=i;
		// OnMouseEnter NE RADI Mem[i].OnMouseEnter:=@OnMouseEnter;
		
		// Hintovi
		SetLength(LabH,i+1);
		LabH[i]:=rcLabel('','',panel_Memo,3,lv*9+5,taLeftJustify);
		LabH[i].Font.Name:='Tahoma';
		LabH[i].Font.Size:=8;
		LabH[i].Font.Color:=clGray;
		LabH[i].visible:=false;
		LabH[i].tag:=i;
	End;
	
	Main.OnResize:=@MainOnResize;
	
	LabH[0].Caption:= 'Naziv selektiranog elementa ili elemenata' + #13#10 +
							'Nakon promjene pritisni ENTER' + #13#10 +
							'Podaci za vi�e elemenata odvojeni su znakom ;';
							
	LabH[1].Caption:= 'X pozicija (lijevo-desno) selektiranog elementa ili elemenata' + #13#10 +
							'Za novu poziciju upi�i novi broj i pritisni ENTER' + #13#10 + 
							'Za relativni pomak u desno ili lijevo dodaj predznak + ili -';
							
	LabH[2].Caption:= 'Y pozicija (gore-dolje) selektiranog elementa ili elemenata' + #13#10 +
							'Za novu poziciju upi�i novi broj i pritisni ENTER' + #13#10 + 
							'Za relativni pomak prema gore ili dolje dodaj predznak + ili -';
	
	LabH[3].Caption:= 'Z pozicija (naprijed-natrag) selektiranog elementa ili elemenata' + #13#10 +
							'Za novu poziciju upi�i novi broj i pritisni ENTER' + #13#10 + 
							'Za relativni pomak prema naprijed ili natrag dodaj predznak + ili -';
							
	LabH[4].Caption:= '�irina selektiranog elementa ili elemenata' + #13#10 +
							'Za novu �irinu upi�i novi broj i pritisni ENTER' + #13#10 + 
							'Za relativnu promjenu dodaj predznak + ili -';
							
	LabH[5].Caption:= 'Visina selektiranog elementa ili elemenata' + #13#10 +
							'Za novu visinu upi�i novi broj i pritisni ENTER' + #13#10 + 
							'Za relativnu promjenu dodaj predznak + ili -';
							
	LabH[6].Caption:= 'Dubina selektiranog elementa ili elemenata' + #13#10 +
							'Za novu dubinu upi�i novi broj i pritisni ENTER' + #13#10 + 
							'Za relativnu promjenu dodaj predznak + ili -';
							
	LabH[7].Caption:= 'Kut selektiranog elementa ili elemenata' + #13#10 +
							'Za novi kut upi�i novi broj i pritisni ENTER' + #13#10 + 
							'Tipke mijenjaju kut za 90 stupnjeva';						
							
	LabH[8].Caption:= 'Napomena elementa ili elemenata' + #13#10 +
							'Za novu napomenu promijeni tekst i pritisni ENTER' + #13#10 + 
							'Podaci za vi�e elemenata odvojeni su znakom ;';

	
	
	{ 	
   ButOK:=rcButton('bOK','OK',main,10,300);
	ButCancel:=rcButton('bCancel','Minus',Main,130,300);
	ButPomoc:=rcButton('bPomoc','Plus',Main,250,300);
   
  
   Memo1:=rcMemo('Memo1', Panel_Memo, 1,18,300,20);
	Memo1.WordWrap:=False;
	Memo1.ScrollBars:=ssNone;
	Memo1.Alignment:=taRightJustify;
	Memo2:=rcMemo('Memo2', Panel_Memo, 1,18,300,40);
	Memo3:=rcMemo('Memo3', Panel_Memo, 1,18,300,60);
	Memo2.Alignment:=taRightJustify;
	Memo3.Alignment:=taRightJustify;
	Memo2.ScrollBars:=ssNone;
	Memo3.ScrollBars:=ssNone;
	// Memo1.Font.Name:='System';
	
	
	
	// 20 je minimalna visina memoa na kojoj se prikazuje kursor, ako je tekst visine 10
	// 19 je minimalna visina memoa na kojoj se prikazuje kursor, ako je tekst visine 9
	Memo1.Height:=20	;
	Memo1.Font.Size:=10;
	Memo1.Lines.Add('123.456');
	Memo2.Height:=20	;
	Memo2.Font.Size:=10;
	Memo2.Lines.Add('123.456');
	Memo3.Height:=20	;
	Memo3.Font.Size:=10;
	Memo3.Lines.Add('123.456');
	
	LL:=rcLabel('','',panel_Memo,1,1,taLeftJustify);
	LL.caption:=Memo1.Font.Name;
	}
End;

// Glavni   
Begin
   versiontxt:='2.0b';	
	Create_Main_Window;
	Main.Caption:='Semafor '+versiontxt;
	Panel_Memo.width:=Panel_Memo.width+1;  // da bi se pokrenuo resize i sve poravnalo :-)
	PrintTime ('==============');
	// eventi
	
	// ButOK.OnClick:=@ButOK_OnClick;
	// ButCancel.OnClick:=@ButCancel_OnClick;
	// ButPomoc.OnClick:=@ButPomoc_OnClick;
	
	// ButOK.default:=True;
	// ButCancel.ModalResult:=mrCancel;
	
   // Naslov
   
   // zaglavlje
   
   // tablica


	Main.Show;   

	while Main.visible do begin
      application.processmessages;
		
		{
		if e=nil then begin
		
			// PROJEKT
			// print('1');
			ie:=0; // broja� elemenata
			id:=0; // broja� dasaka
			rbs:=0;
			SetLength(lista_elementi,ie);
			SetLength(lista_daske,ie);
			// print('2');
			// selektiranih:=0;
			For i:=ElmList.Count-1 DownTo 0 Do begin
					elm2:=tElement(ElmList.items[i]);
					If elm2.Selected Then begin 
						rbs:=i;		// redni broj selektiranog elementa
						SetLength(lista_elementi,ie+1);
						lista_elementi[ie]:=elm2;
						ie:=ie+1;
					end;	// If elm.Selected
			end;
			// print('3');
			broj_el:=ie;
			// broj_das:=id;
			
			if (stari_broj_el<>broj_el) or (stari_rbs<>rbs) then begin
				Label_broj.caption:=IntToStr(broj_el)+ ' el.';
				provjeri_selektirano;
			end;
			stari_broj_el:=broj_el;
			stari_rbs:=rbs;
			// print('4');
			
		
		end;	// if e=nil  
		
		
		
				{
			
			// ShowD('U projektu sam');
			Print('U Projektu sam' );
			for i:=0 to ElmHolder.elementi.count-1 do Begin  // broji elemente u PROJEKTU!!!!!
				elm:=telement(ElmHolder.elementi.items[i]);   
				if elm.selected then begin
						// Elist.addObject(elm.naziv,elm);
				end;  // if elm.selected 
			End; // for i
			
		}
		
		
   end;	// while Main.visible
	print('nakon "While Main visible" petlje ');
	// set_value_int(put_skript+'Semafor.ini','work',0);  // u ini se upisuje da je forma prirodno zatvorena
	
	Main.Close;			// ovo �alje u sFormOnCloseQuery 
	

	PrintTime(' Skripta: "semaforP" '+versiontxt+ // #13#10+ 
								 ' end [P]');
	Print('____________________');
	// Print('Skripta SemaforP END (Projekt)');
   
   
End.