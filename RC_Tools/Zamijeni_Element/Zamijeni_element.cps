Program Zamijeni_element;   
// Red Cat Ltd 
// v1.0 07.06.2017.

// - 
// - 
// - 

// 
var
// progress bar
   progress_panel,bar_out, bar_in:TPanel;
   progress_text:TLabel;
   start_ok:boolean;
	conf_path:string;
	i,Sirina_prozora:integer;


{$I ..\include\rcMessages.cps}
{$I ..\include\rcPrint.cps}
{$I ..\include\rcStrings.cps}
{$I ..\include\rcControls.cps}
{$I ..\include\rcObjects.cps}
{$I ..\include\rcEvents.cps}
{$I ..\include\rcProgress.cps}
{$I ..\include\rcCurves.cps}
{$I ..\include\rcMath.cps}
{$I ..\include\rcConfig.cps}

    

// MAIN ////////////////////////////////////////////////////////////////////////
var
   // Startni prozor
	Main:TForm;
   Panel_Left, Panel_right, Panel_Down, Panel_Title, Panel_Info, Panel_Table, Panel_Memo, Panel_MemoO :TPanel;
   Pan_var, Pan_val, Pan_for, Pan_cb, Pan_hint : array of TPanel;
   ButOK,ButPomoc,ButCancel:TButton;

	MozeZatvaranje, Izvrseno :boolean;    // za debug poruke u Journalu
	PorukaV, PorukaM :TLabel;
	
	// slikice
	SB1:TScrollBox;
	// SL1,SL2,SL3,SL4,SL5,SL6,SL7,SL8:TImage;
	//Slika:TAImageList;
	slika_put:String;
	selektiranih:Integer;
	elm_stari:Telement;
	
Procedure End_Program(Sender:TObject);
// Ovdje se ne smije zvati niti jedna komponenta jer su sve ve� zatvorene

var
	dummy:boolean;
Begin
	dummy:=set_value_int(conf_path,'Lijevo',Main.left);
	dummy:=set_value_int(conf_path,'Gore', Main.Top);
	dummy:=set_value_int(conf_path,'Visina',Main.Height);
	

End;	

Procedure Kopiraj_propertije(es,en:TElement);
begin
	// prebaci poziciju
	en.xPos:=es.xPos;
	en.yPos:=es.yPos;
	en.zPos:=es.zPos;
	en.xFormula:=es.xFormula;
	en.yFormula:=es.yFormula;
	en.zFormula:=es.zFormula;
	ShowN('Pozicija promijenjena');
	
	// prebaci kuteve
	en.Kut:=es.Kut;
	en.xKut:=es.xKut;
	en.zKut:=es.zKut;
	en.KXFormula:=es.KXFormula;
	en.KYFormula:=es.KYFormula;
	en.KZFormula:=es.KZFormula;
	ShowN('Kutevi promijenjeni');
	
	// prebaci dimenzije
	en.Visina:=es.Visina;
	en.Dubina:=es.Dubina;
	en.Sirina:=es.Sirina;
	en.HFormula:=es.HFormula;
	en.SFormula:=es.SFormula;
	en.DFormula:=es.DFormula;
	ShowN('Dimenzije promijenjene')
end;

Procedure Pomoc;
var
	PDF:TpdfPrinter;
Begin
	ShowN('Pomo�');
	PDF:=TpdfPrinter.create(1250);
	// PDF.FileName := 'K:\_Corpus 4 RC dev 3\Corpus 4 RC\skripte\Test_polygon\Japin Corpus\skripte\Ponuda\ponuda.pdf';
   // PDF.FileName := 'https://www.youtube.com/watch?v=YRBH87-SKL4&feature=youtu.be';
	// PDF.FileName := 'K:\Zrc\web\PaleMoonRedCat\Moon RC.exe';
	//	PDF.FileName := prgfolder + 'MaterialEditor_4.1.2.54.exe';
	// PDF.FileName := prgfolder + 'skripte\RC_Tools\Gerung\gerung.cps'; // Otvara skriptu u editoru :-)
	// PDF.ShowPdf;
	ShowN('PDF.ShowPdf - DONE');
	PDF.Free;
End;


Procedure Zamijeni_OnClick(Sender:TObject);
var
   elm_stari, elm_novi:telement;
	Elementi_putanja, putanja:string;
	i,ii:integer;
	
	novi:string;
	dijalog:TOpenDialog;
	dummy:boolean;
	tempel:TElement;
Begin
	//ii:=Timage(Sender).Tag;
	ShowN('Pritisnuta je tipka Zamijeni');
	

	main.visible:=false;   // da ne smeta jer je "on top"
	putanja:=find_value_str(conf_path,'Elementi_putanja');
	novi:='';
	dijalog := TOpenDialog.Create(nil);
	dijalog.InitialDir := putanja;
	dijalog.Filter := 'Corpus elementi (*.e3d)|*.e3d';
	if dijalog.Execute then begin
		novi := dijalog.FileName
		dummy:=set_value_str(conf_path,'Elementi_putanja', novi);
	end;	// if
   main.visible:=true;

	// ShowMessage (novi);
	
	// provjera da li je i�ta selektirano
	
	
	
	// u�itavanje novog elementa
	// Elementi_putanja:=prgfolder+find_value_str(conf_path,'Elementi_putanja');
	// Putanja:=Elementi_putanja+'\'+find_value_str(conf_path,'Element'+IntToStr(ii));
	Putanja:=Novi;
	ShowN ('Putanja za element je "'+Putanja+'"');
	
	if novi<> '' then begin
		For i:=E.ElmList.Count-1 DownTo 0 Do begin
			elm_stari:=TElement(E.ElmList.items[i]);
			ShowN(IntToStr(i)+'. Gledam element "'+elm_stari.Naziv+'"');
			If elm_stari.Selected Then Begin
				selektiranih:=selektiranih+1;
				// ShowN('   SELEKTIRAN je element "'+elm_stari.Naziv+'"');
				// u�itavanje novog
				elm_novi:=LoadParentedElement(E,Putanja);
				ShowN('   U�itan je element "'+elm_novi.Naziv+'"');
				// kopiranje propertija
				Kopiraj_propertije(elm_stari,elm_novi);
				// promjena selekcije
				elm_stari.selected:=false;
				// elm_novi.selected:=True;
				// brisanje starog elementa
				E.Elmlist.Obrisi(i);
				ShowN('   DONE! Postavljen je element "'+elm_novi.Naziv+'"');
			end else begin   // Nije ni�ta selektirano pa mijenjam roditelja

				// ShowN('   Nije selektiran element "'+elm_stari.Naziv+'"');
			end;
		end;
	
	end;
	
	// ShowN('Selektiranih='+IntToStr(selektiranih));
	// If selektiranih=0 then begin
		// PorukaV.Caption:=IntToStr(selektiranih);
		// PorukaM.Caption:='Odaberite jedan ili vi�e elemenata unutar postoje�eg elementa';
		
		
		{
		tempel:=TElement.Create(nil);
		tempel.selected:=true;
		tempel:=e;
		// E:=LoadParentedElement(nil,Putanja);
		// E:=LoadParentedElement(E,Putanja);
		tempel:=LoadParentedElement(nil,Putanja);
		
		E.kopiraj(tempel);
		
		application.processmessages;
		E.RecalcFormula(nil);
		// win.width:=1
		}
	// end;

	
End;

Procedure postavi_tekst;
var
	PorukaT1, PorukaT2:TLabel;
Begin
	PorukaV:=rcLabel('LBL1','0',Panel_Down,150,10,taCenter);
	PorukaV.Font.Size:=14;
	PorukaV.Font.Color:=clRed;
	PorukaV.Font.Style:=[fsBold];
	
	PorukaT1:=rcLabel('LBL3','Poruka',SB1,10,10,taLeftJustify);
	PorukaT1.Font.Size:=11;
	PorukaT1.Font.Color:=clBlack;
	PorukaT1.Font.Style:=[fsBold];
	PorukaT1.Caption:='Zamjena elementa';
	
	PorukaT2:=rcLabel('LBL4','Skripta mijenja selektirane elemente.',SB1,10,45,taLeftJustify);
	PorukaT2.Font.Size:=9;
	
	PorukaT2:=rcLabel('LBL5','Novi elementi �e od starih preuzeti polo�aj i dimenzije.',SB1,10,70,taLeftJustify);
	PorukaT2.Font.Size:=9;
	
	// PorukaT2:=rcLabel('LBL6','elementi.',SB1,10,90,taLeftJustify);
	// PorukaT2.Font.Size:=9;
	
	// PorukaT2:=rcLabel('LBL7','Novi elementi �e od starih preuzeti polo�aj i dimenzije.',SB1,10,115,taLeftJustify);
	// PorukaT2.Font.Size:=9;
	
	// PorukaT2:=rcLabel('LBL6','elementi.',SB1,10,90,taLeftJustify);
	// PorukaT2.Font.Size:=9;
	
End;


Procedure Create_Main_Window;
var
	slika_put,elem_put:TStringlist;
	sput:string;
	i:integer;
Begin
	// Startni prozor
   Main:=rcTool('Corpus_RC_Tool'); 
	Main.Caption:='Corpus RC';
	Main.Width:=400;
	
	Main.Height:=320;
	// Main.Left:=200;
	Main.BorderStyle:=bsSizeToolWin;
	Main.FormStyle:=fsStayOnTop;
	//Main.BorderIcons:=[];
	Main.Position:=poDesigned;
	
	
	
	// Main.Top:=20;


	
	// Donji Panel za tipke
	Panel_Down:= rcPanel ( '' ,Main ); 
	Panel_Down.ALign:=alBottom; 
   Panel_Down.Height:=40;       // Visina donjeg panela!!!!
   Panel_Down.BorderWidth:=5;
	
   // desni panel za memo 
   Panel_Right:= rcPanel ( '' ,Main ); Panel_Right.ALign:=alRight; 
   Panel_Right.Width:=2;       // Širina desnog panela!!!!
   Panel_Right.BorderWidth:=5;
	
   // lijevi panel za slikice
   Panel_Left:= rcPanel ( '' ,Main ); 
	Panel_Left.Left:=10;
	Panel_Left.Top:=10;
	Panel_Left.ALign:=alClient;

   // tipke OK i Cancel
	
	ButOK:=rcButton('bOK','Zamijeni',Panel_Down,1,1);ButOK.Align:=alLeft; ButOK.width:=100;
	ButOK.visible:=True;
	// ButOK.
	rcSpaceL(Panel_Down,150);
	// ButCancel:=rcButton('bCancel','Odustajem',Panel_Down,10,1);ButCancel.Align:=alRight;ButCancel.width:=100;
   // rcSpaceR(Panel_Down,600);
	// ButPomoc:=rcButton('bPomoc','Upute',Panel_Down,300,1);ButPomoc.Align:=alLeft;ButPomoc.width:=75;
	// rcSpaceL(Panel_Down,400);
		
	ButOK.OnClick:=@Zamijeni_OnClick;
	// 
	// ButPomoc.OnClick:=@ButPomoc_OnClick;
		
	ButOK.default:=True;
	// ButCancel.ModalResult:=mrCancel;
	// ButCancel.OnClick:=@End_Program;
	
	// MemoJ
   Panel_Memo:=rcPanel ( '' ,Panel_Right ); Panel_memo.Top:=18;Panel_Memo.ALign:=alClient; 
   
   MemoJ:=rcMemo('Memo_debug', Panel_Memo, 1,18,1,1);MemoJ.Align:=alClient;
   MemoJ.Color:=clBTNFace;MemoJ.Font.Size:=8;MemoJ.Ctl3D:=False;MemoJ.Font.Color:=clMaroon;

	
	Main.Left:=screenwidth-Main.Width-20;	
	
	// scroll box
	SB1:=TScrollBox.create(Panel_Left);
	SB1.parent:=Panel_Left;
   SB1.Ctl3D:=False;
   SB1.left:=1;
   SB1.top:=1;
   // SB1.width:=550;
   SB1.Height:=595;
	SB1.Align:=alClient;
   // SB1.Color:=clBlue;
   SB1.VertScrollBar.Visible:=True;
   SB1.VertScrollBar.Tracking:=True;   
   // Adj_SB.VertScrollBar.Smooth:=True;  ne radi!!
   SB1.HorzScrollBar.Visible:=False;
	
	// postavi slike
	postavi_tekst;
	
End;

// ------------------------------------------------	
// ------------------------------------------------

// Glavni  

Begin
   
	Izvrseno:=false;
	
	Create_Main_Window;
	
	PrintTime('skripta: ZAMIJENI ELEMENT v1.1 started');
	
		
	// Da li postoji 'Zamijeni.ini'
	conf_path:=prgfolder+'skripte\RC_Tools\Zamijeni_Element\Zamijeni.ini';
	if not rcFileCheck(conf_path) then Exit;

	// Pozicioniranje glavnog prozora
	Sirina_Prozora:=360; Main.Width:=Sirina_Prozora; 
	ShowN('�irina je pode�ena na '+IntToStr(Main.Width));
	Main.Left:=find_value_int(conf_path,'Lijevo');
	ShowN('Pozicija X pode�ena na '+IntToStr(Main.Left));
	Main.Top:=find_value_int(conf_path,'Gore');
	ShowN('Pozicija Y je pode�ena na '+IntToStr(Main.Top));
	Main.Height:=find_value_int(conf_path,'Visina');
	ShowN('Visina je pode�ena na '+IntToStr(Main.Height));
	
	Main.onDeactivate:=@End_program;
  //ButPomoc.OnClick:=@ButPomoc_OnClick;
	// Main.OnResize:=@Main_OnResize;
	
   // cbDebug_OnClick(cbDebug);  // poziva "cbDebug_OnClick" bez da je i�ta kliknuto
	
   // Main.ShowModal;   // ekskluzivno radi
	Main.Show;  // KAd je ovo, onda ne radi tipka Cancel
	while Main.visible do begin
      application.processmessages;
		selektiranih:=0;
		For i:=E.ElmList.Count-1 DownTo 0 Do begin
				elm_stari:=TElement(E.ElmList.items[i]);
				If elm_stari.Selected Then selektiranih:=selektiranih+1;
		end;
		If selektiranih=0 then ButOK.enabled:=false
								else ButOK.enabled:=true;
		PorukaV.Caption:=IntToStr(selektiranih);
		{
		If selektiranih=0 then begin
			PorukaV.Caption:=IntToStr(selektiranih);
			// PorukaM.Caption:='Odaberite jedan ili vi�e segmenata u postoje�em elementu';
		end else begin
			if selektiranih=1 then PorukaV.Caption:='Odabran je '+IntToStr(selektiranih)+' segment.';
			if (selektiranih>1) and (selektiranih<5) then PorukaV.Caption:='Odabrana su '+IntToStr(selektiranih)+' segmenta.';
			if (selektiranih>4) and (selektiranih<21) then PorukaV.Caption:='Odabrano je '+IntToStr(selektiranih)+' segmenata.';
			PorukaM.Caption:='Odaberite s �ime ih treba zamijeniti.';
		end;
		}
   end;
   
	
   Main.free; 
	// Main.close; 
	Print('skripta: ZAMIJENI ELEMENT v1.1 end');
   
End.