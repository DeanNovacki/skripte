// Base
Procedure ok(sender:Tobject);
var
   i:integer;
   tx, ty, tz:single;
   new_element, path:string;
   Selem:Telement;
   Read_only_variables: boolean;
   tt: string;   // '0+' or ''         - for hidding variable in element
   plate:TDaska;
   el_leg:Telement;
   leg_FL_X, leg_FL_Y, leg_FL_Z,
   leg_FR_X, leg_FR_Y, leg_FR_Z,  
   leg_BL_X, leg_BL_Y, leg_BL_Z,leg_BR_X, leg_BR_Y, leg_BR_Z : string; 
   npsw:string;  
   nvc:TStringList;
  
   
   el_door:Telement;
   door_X,door_Y,door_Z,door_H,door_W,door_D,
   door_L_X,door_L_Y,door_L_Z,door_L_H,door_L_W,door_L_D,
   door_R_X,door_R_Y,door_R_Z,door_R_H,door_R_W,door_R_D :string;
Begin

	// Showmessage ('Procedure "OK" Started');
    progress_change(0,'Start!');

    //W_Window.cursor:=crHourglass;
    Button_ok.cursor:=crHourglass;
    Button_ok.enabled:=false;
    Read_only_variables:=TRUE;
    
    // Showmessage('OK');
    message ('Procedure ok(sender:Tobject);');
     
    // save preset to last used
    progress_change(5,'Saving to last preset');
    Save_Preset('Last used');
    
    // save apx heights to ini file
    for i:=1 to 9 do set_value_int(ininame,'AP['+IntToStr(i)+']', APS[i].Tag); 
    
    // save scrolBoxx sroller bar position :-)
    SBSP:=Adj_SB.VertScrollBar.Position;
    set_value_int(ininame,'SBSP', SBSP);

	 // Showmessage ('1');
	 
   if e=nil then begin    // in project   
   //    Showmessage ('12');
		selem:=TElement.Create(nil);    // created but still not in project or editor
	//	Showmessage ('13');
		selem.naziv:=el_name;
	//	Showmessage ('14');
      if elmHolder<>nil then begin
			
			elmHolder.DodajElement(selem);    // add element to project
		end;
		
		//	Showmessage ('15');
         selem.selected:=true;             // select new element
		//	Showmessage ('16');
      end else begin  // in editor
		//	Showmessage ('17');
         selem:=e;   // element in editor will be changed
      end;
                 
   // replace element
	// Showmessage ('2');
   progress_change(15,'Replace original');
   load_element:=PrgFolder+'elmsav\rce\Type_10'+IntToStr(el_shape)+'.e3d';
	if FileExists(load_element) then new_element:=load_element
								else new_element:=script_path+'rce\Type_10'+IntToStr(el_shape)+'.rce';
    
	 el_replace(selem,new_element);        //  **********************************************************************************
	 
	 // selem:=el_replaceF(selem,new_element);
    
	 progress_change(20,'Set basic properties');
    // set name 
    el_set_name(selem,el_name);  // TO SE MORA OPET iako je ve� prije zadano kod kreiranja

    // set dimensions
	 // Showmessage('�irina elementa �e biti: '+FloatToStrC1(el_width,8,2));
	 // Showmessage('�irina elementa je: '+FloatToStrC1(selem.sirina,8,2));
	 // application.processmessages;
	 // selem.RecalcFormula(selem);
    el_set_width(selem,el_width);
	 // selem.sirina:=el_width;
	 // selem.sirina:=1000.00;
	 //Showmessage('Naziv elementa je: '+selem.naziv);
	 // Showmessage('�irina elementa je sada: '+FloatToStrC1(selem.sirina,8,2));
    el_set_height(selem,el_height);
    el_set_depth(selem,el_depth);
    
    selem.MinSirina:=el_width_min;
    selem.MaxSirina:=el_width_max;
    selem.MinVisina:=el_height_min;
    selem.MaxVisina:=el_height_max;
    selem.MinDubina:=el_depth_min;
    selem.MaxDubina:=el_depth_max;
    
    selem.locked:=false; npsw:=#114;
    for i:=101 downto 99 do begin npsw:=npsw+chr(i); end;
    npsw:=npsw+#97+chr(StrToInt(IntToStr(6+10*11)));
    for i:=101 to 103 do begin npsw:=npsw+IntToStr(i-100); end;
    //selem.SetNewEditPassword('',npsw);

    //selem.SetNewEditPassword('','dex');
    
    // read only variables?
    if Read_only_variables then tt:='0+'
                           else tt:='';
    
    // change variable values
    
	// Showmessage ('3');
	 
    // top
    progress_change(25,'Set top');
    var_set(selem, 'top_type', tt+IntToStr(top_type));
    plate:=plate_find_by_name(selem,'Top_F');             // top_thick
    plate.debljina:=top_thick;
    plate:=plate_find_by_name(selem,'Top_B');
    if plate<>nil then plate.debljina:=top_thick;
    var_set(selem, 'top_width_back', tt+IntToStr(top_width_back));    
    var_set(selem, 'top_width_front', tt+IntToStr(top_width_front));    
    var_set(selem, 'top_dist_front', tt+IntToStr(top_dist_front));
    var_set(selem, 'top_dist_back_fix', '0+'+IntToStr(top_dist_back_fix));    

    // bottom
    progress_change(30,'Set bottom');
    plate:=plate_find_by_name(selem,'Bottom');             // top_thick
    plate.debljina:=Bottom_thick;
    var_set(selem, 'back_dist', tt+IntToStr(back_dist));
    var_set(selem, 'back_ins', tt+IntToStr(back_ins));    
    var_set(selem, 'bottom_dist_front', tt+IntToStr(bottom_dist_front));
    var_set(selem, 'bottom_type', '0+'+IntToStr(bottom_type));
    var_set(selem, 'bottom_dist_back_fix', '0+'+ IntToStr(bottom_dist_back));
  
    
    // back
    
    progress_change(35,'Set back');
    
    plate:=plate_find_by_name(selem,'Back');
    if back_on=1 then begin 
                      var_set(selem, 'back_on', '0+'+'1');
                      plate.visible:=true;
                 end else begin 
                      var_set(selem, 'Back_on', '0+'+'0');
                      plate.visible:=false;
                      plate:=plate_find_by_name(selem,'Back_top');
                      message('0');
                      if plate<> nil then plate.visible:=false; 
                      message('1');
    end; // if 

    var_set(selem, 'back_dist', tt+IntToStr(back_dist));

    plate:=plate_find_by_name(selem,'Back');
    if plate<> nil then plate.debljina:=back_thick;

    plate:=plate_find_by_name(selem,'Back_top');
    if plate<> nil then plate.debljina:=back_thick; 
    
    
    var_set(selem, 'back_ins', tt+IntToStr(back_ins));
    var_set(selem, 'back_top_width', tt+IntToStr(back_top_width));
    var_set(selem, 'back_down_width', tt+IntToStr(back_down_width));
    
    
    
    // sides
    progress_change(40,'Set sides');
    plate:=plate_find_by_name(selem,'Side_L');
    plate.debljina:=side_thick;
    plate:=plate_find_by_name(selem,'Side_R');
    plate.debljina:=side_thick;
    
    // shelves
    progress_change(45,'Set shelves');
    var_set(selem, 'shelf_qnt', '0+'+IntToStr(shelf_qnt-1));
    message('varijable name: "shelf_qnt"'+'value= '+IntToStr(shelf_qnt));
    case shelf_qnt of           // number of shelves
         1: begin
               plate:=plate_find_by_name(selem,'Shelf_1');plate.visible:=false;
               plate:=plate_find_by_name(selem,'Shelf_2');plate.visible:=false;
               plate:=plate_find_by_name(selem,'Shelf_3');plate.visible:=false;
            end;
         2: begin
               plate:=plate_find_by_name(selem,'Shelf_1');plate.visible:=true;
               plate:=plate_find_by_name(selem,'Shelf_2');plate.visible:=false;
               plate:=plate_find_by_name(selem,'Shelf_3');plate.visible:=false;
            end;
         3: begin
               plate:=plate_find_by_name(selem,'Shelf_1');plate.visible:=true;
               plate:=plate_find_by_name(selem,'Shelf_2');plate.visible:=true;
               plate:=plate_find_by_name(selem,'Shelf_3');plate.visible:=false;
            end;   
     end;
     for i:=1 to 3 do begin
        plate:=plate_find_by_name(selem,'Shelf_'+IntToStr(i));
        plate.debljina:=shelf_thick // shelf thickness
     end;    
     var_set(selem, 'shelf_dist_front', tt+IntToStr(shelf_front_dist));   
     if shelf_mov=1 then var_set(selem, 'shelf_move', tt+'1')
                    else var_set(selem, 'shelf_move', tt+'0');
                    
     // legs
     progress_change(50,'Set Legs1');
     el_delete_legs(selem);              // don't like it ;-)
     
     leg_FL_X:=IntToStr(leg_dist_side);
     leg_FL_Y:='0';
     leg_FL_Z:='0-'+IntToStr(leg_dist_front);
     
     leg_FR_X:='width-'+IntToStr(leg_dist_side);
     leg_FR_Y:='0';
     leg_FR_Z:='0-'+IntToStr(leg_dist_front);
        
     leg_BL_X:=IntToStr(leg_dist_side);
     leg_BL_Y:='0';
     leg_BL_Z:='0-depth+'+IntToStr(leg_dist_back);
     
     leg_BR_X:='width-'+IntToStr(leg_dist_side);
     leg_BR_Y:='0';
     leg_BR_Z:='0-depth+'+IntToStr(leg_dist_back); 
     
	  progress_change(51,'Set Legs2');
	  
     if (el_shape<>5) and (el_shape<>6) then begin  
        if leg_height=1 then begin
                          var_set(selem, 'foot_height', tt+'100');
                          path:=script_path+'rce\Foot_simple_100_FL.rce'; 
                                        
                          el_leg:=element_add_F( path,'leg_FL',selem,leg_FL_X,leg_FL_Y,leg_FL_Z);
                          el_leg:=element_add_F( path,'leg_FR',selem,leg_FR_X,leg_FR_Y,leg_FR_Z);
                          el_leg:=element_add_F( path,'leg_BL',selem,leg_BL_X,leg_BL_Y,leg_BL_Z);
                          el_leg:=element_add_F( path,'leg_BR',selem,leg_BR_X,leg_BR_Y,leg_BR_Z);
                          
                          
                     end else begin 
                          var_set(selem, 'foot_height', tt+'150');
                          path:=script_path+'rce\Foot_simple_150_FL.rce';  
                                       
                          el_leg:=element_add_F( path,'leg_FL',selem,leg_FL_X,leg_FL_Y,leg_FL_Z);
                          el_leg:=element_add_F( path,'leg_FR',selem,leg_FR_X,leg_FR_Y,leg_FR_Z);
                          el_leg:=element_add_F( path,'leg_BL',selem,leg_BL_X,leg_BL_Y,leg_BL_Z);
                          el_leg:=element_add_F( path,'leg_BR',selem,leg_BR_X,leg_BR_Y,leg_BR_Z);
        end;   //   if leg_height=
    end;  //  if (el_shape<>5) and 
    
	 progress_change(52,'Set Legs3');
	 
    // hidding some legs variable for element viewers
    if var_exist(selem, 'foot_dist_front') then var_set(selem, 'foot_dist_front', tt+IntToStr(leg_dist_front));
    if var_exist(selem, 'foot_dist_SIDE') then var_set(selem, 'foot_dist_side', tt+IntToStr(leg_dist_side));
    if var_exist(selem, 'foot_dist_back') then var_set(selem, 'foot_dist_back', tt+IntToStr(leg_dist_back));
    
    
    // doors 
    progress_change(55,'Set doors');  
  
    { read from presets
    left_offset:='2';
    right_offset:='2';
    up_offset:='2';
    down_offset:='2';
    }
    // Showmessage('left_offset='+left_offset);
    door_X:=left_offset;                                      // '0+2'
    door_Y:='foot_height+'+down_offset;                         // 'foot_height+2'
    door_Z:='door.depth';                                       // '0-door.depth'
    door_H:='height-foot_height-'+up_offset+'-'+down_offset;    // 'height-foot_height-2-2'
    door_W:='width-'+left_offset+'-'+right_offset;              // 'width-2-2
    door_D:='door.depth';                                       // 'door.depth'
    
    door_L_X:=door_X;
    door_L_Y:=door_Y;
    door_L_Z:='door_L.depth';
    door_L_H:=door_H;
    door_L_W:='width/2-'+left_offset+'-'+right_offset;               // 'width/2-2-2'
    door_L_D:='door_L.depth';
    
    door_R_X:='width/2+'+left_offset;                            // 'width/2+2'
    door_R_Y:=door_Y;
    door_R_Z:='door_R.depth';
    door_R_H:=door_H;
    door_R_W:='width/2-'+left_offset+'-'+right_offset;             // 'width/2-2-2'
    door_R_D:='door_R.depth';
    
    case door_qnt of
         1 : begin
                // no doors, do nothing
             end;
         2 : begin     // left door
                // add variables for postion and size of door

                if var_exist(selem, 'door_X') then var_set(selem, 'door_X', '0+'+door_X)
                                              else var_add(selem, 'door_X', '0+'+door_X);
                if var_exist(selem, 'door_Y') then var_set(selem, 'door_Y', '0+'+door_Y)
                                              else var_add(selem, 'door_Y', '0+'+door_Y);
                if var_exist(selem, 'door_Z') then var_set(selem, 'door_Z', '0+'+door_Z)
                                              else var_add(selem, 'door_Z', '0+'+door_Z);
                if var_exist(selem, 'door_W') then var_set(selem, 'door_W', '0+'+door_W)
                                              else var_add(selem, 'door_W', '0+'+door_W);
                if var_exist(selem, 'door_H') then var_set(selem, 'door_H', '0+'+door_H)
                                              else var_add(selem, 'door_H', '0+'+door_H);
                if var_exist(selem, 'door_D') then var_set(selem, 'door_D', '0+'+door_D)
                                              else var_add(selem, 'door_D', '0+'+door_D);
                                                                                           
                // add door
                progress_change(60,'Loading doors');
                path:=script_path+'rce\front01.rce';
                el_door:=element_add_f( path,'door',selem,door_X,door_Y,door_Z);   
                // add position and dimensions in new element

                progress_change(70,'Changing doors');
                el_door.Sformula:=door_W;
                el_door.Hformula:=door_H;
                el_door.Dformula:=door_D;
                
                el_door.TipElementa:=VRE;    // element type is front   
                plate:=plate_find_by_name(el_door,'front');
                plate.TipDaske:=Fronta;                 // plate type: front  
                plate.TipFronte:=TTipFronte(1);         // front type: 1=left door, 2:right door
                
             end;
             
         3 : begin     // right door              
                 // add variables for postion and size of door
                if var_exist(selem, 'door_X') then var_set(selem, 'door_X', '0+'+door_X)
                                              else var_add(selem, 'door_X', '0+'+door_X);
                if var_exist(selem, 'door_Y') then var_set(selem, 'door_Y', '0+'+door_Y)
                                              else var_add(selem, 'door_Y', '0+'+door_Y);
                if var_exist(selem, 'door_Z') then var_set(selem, 'door_Z', '0+'+door_Z)
                                              else var_add(selem, 'door_Z', '0+'+door_Z);
                if var_exist(selem, 'door_W') then var_set(selem, 'door_W', '0+'+door_W)
                                              else var_add(selem, 'door_W', '0+'+door_W);                                              
                if var_exist(selem, 'door_H') then var_set(selem, 'door_H', '0+'+door_H)
                                              else var_add(selem, 'door_H', '0+'+door_H);                                              
                if var_exist(selem, 'door_D') then var_set(selem, 'door_D', '0+'+door_D)
                                              else var_add(selem, 'door_D', '0+'+door_D);
                                                                                            
                // add door
                progress_change(60,'Loading doors');
                path:=script_path+'rce\front01.rce';
                el_door:=element_add_f( path,'door',selem,door_X,door_Y,door_Z);   
                // add dimensions in new element
                progress_change(70,'Changing doors');
                el_door.Sformula:=door_W;
                el_door.Hformula:=door_H;
                el_door.Dformula:=door_D;
                
                el_door.TipElementa:=VRE;    // element type is front   
                plate:=plate_find_by_name(el_door,'front');
                plate.TipDaske:=Fronta;                 // plate type: front  
                plate.TipFronte:=TTipFronte(2);         // front type: 1=left door, 2:right door
                
             end;

         4 : begin     // double doors
         
                // left door       
                // add variables for postion and size of door
         
                if var_exist(selem, 'door_L_X') then var_set(selem, 'door_L_X', '0+'+door_L_X)
                                                else var_add(selem, 'door_L_X', '0+'+door_L_X);
                if var_exist(selem, 'door_L_Y') then var_set(selem, 'door_L_Y', '0+'+door_L_Y)
                                                else var_add(selem, 'door_L_Y', '0+'+door_L_Y);
                if var_exist(selem, 'door_L_Z') then var_set(selem, 'door_L_Z', '0+'+door_L_Z)
                                                else var_add(selem, 'door_L_Z', '0+'+door_L_Z);
                if var_exist(selem, 'door_L_W') then var_set(selem, 'door_L_W', '0+'+door_L_W)
                                                else var_add(selem, 'door_L_W', '0+'+door_L_W);                                              
                if var_exist(selem, 'door_L_H') then var_set(selem, 'door_L_H', '0+'+door_L_H)
                                                else var_add(selem, 'door_L_H', '0+'+door_L_H);                                              
                if var_exist(selem, 'door_L_D') then var_set(selem, 'door_L_D', '0+'+door_L_D)
                                                else var_add(selem, 'door_L_D', '0+'+door_L_D);
                                                                                            
                // add left door
                progress_change(60,'Loading left door');
                path:=script_path+'rce\front01.rce';
                el_door:=element_add_f( path,'door_L',selem,door_L_X,door_L_Y,door_L_Z); 
                // add dimensions in new element
                progress_change(70,'Changing left door');
                el_door.Sformula:=door_L_W;
                el_door.Hformula:=door_L_H;
                el_door.Dformula:=door_L_D;
                
                el_door.TipElementa:=VRE;    // element type is front   
                plate:=plate_find_by_name(el_door,'front');
                plate.TipDaske:=Fronta;                 // plate type: front  
                plate.TipFronte:=TTipFronte(1);         // front type: 1=left door, 2:right door
                
                // right door       
                // add variables for postion and size of door
         
                if var_exist(selem, 'door_R_X') then var_set(selem, 'door_R_X', '0+'+door_R_X)
                                                else var_add(selem, 'door_R_X', '0+'+door_R_X);
                if var_exist(selem, 'door_R_Y') then var_set(selem, 'door_R_Y', '0+'+door_R_Y)
                                                else var_add(selem, 'door_R_Y', '0+'+door_R_Y);
                if var_exist(selem, 'door_R_Z') then var_set(selem, 'door_R_Z', '0+'+door_R_Z)
                                                else var_add(selem, 'door_R_Z', '0+'+door_R_Z);
                if var_exist(selem, 'door_R_W') then var_set(selem, 'door_R_W', '0+'+door_R_W)
                                                else var_add(selem, 'door_R_W', '0+'+door_R_W);                                              
                if var_exist(selem, 'door_R_H') then var_set(selem, 'door_R_H', '0+'+door_R_H)
                                                else var_add(selem, 'door_R_H', '0+'+door_R_H);                                              
                if var_exist(selem, 'door_R_D') then var_set(selem, 'door_R_D', '0+'+door_R_D)
                                                else var_add(selem, 'door_R_D', '0+'+door_R_D);
                                                                                            
                // add door
                progress_change(80,'Loading right door');
                path:=script_path+'rce\front01.rce';
                el_door:=element_add_f( path,'door_R',selem,door_R_X,door_R_Y,door_R_Z);  
                // add position and dimensions in new element
                
                progress_change(90,'Changing right door');
                el_door.Xformula:=door_R_X;
                el_door.Yformula:=door_R_Y;
                el_door.Zformula:=door_R_Z;
                el_door.Sformula:=door_R_W;
                el_door.Hformula:=door_R_H;
                el_door.Dformula:=door_R_D;
                
                el_door.TipElementa:=VRE;    // element type is front   
                plate:=plate_find_by_name(el_door,'front');
                plate.TipDaske:=Fronta;                 // plate type: front  
                plate.TipFronte:=TTipFronte(2);         // front type: 1=left door, 2:right door
                
                
                
             end;

             
    end; // case         

    ////
    // set position
    tx:=el_xp;
    ty:=el_yp;
    tz:=el_zp; 
    
    tx:=el_xp;
    ty:=el_yp;
    tz:=el_zp;
    
    selem.Xpos:=tx;
    selem.Ypos:=ty; 
    if selem.Dubina>tz then selem.Zpos:=selem.Dubina
                       else selem.Zpos:=tz;
    selem.selected:=false; 
    // add extra variables 
    
    
    // ************************************************************************** 
    nvc:=TStringList.Create;
    try nvc.LoadFromFile(script_path+'extra_variables.txt');
        except ShowMessage( 'Cannot load "extra_variables.txt"!');
    end; 
    for i:=0 to nvc.count-1 do begin
        // Showmessage('nvc.Names['+IntToStr(i)+']='+nvc.Names[i]);
        if var_exist(selem,nvc.Names[i]) then begin 
                                                   // Showmessage ('Exist, so changing');
                                                   selem.fvarijable.strings[i]:=nvc.strings[i]
                                         end else begin
                                                  // Showmessage ('Not exist, so adding '+nvc.strings[i]); 
                                                  selem.fvarijable.add(nvc.strings[i])
                                         end; 
    end;
    
    
    
    progress_change(95,'Recalculate...');
    selem.RecalcFormula(selem);
    
    progress_change(100,'Done...');
	// Showmessage ('Procedure "OK" Started');
End;