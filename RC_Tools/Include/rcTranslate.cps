﻿{

// Jezik prijevoda
CurrentLang=Croatian

// 0=Sistemski nazivi / 1=Language datoteka
System=1

Primjer upotrebe

ShowMessage (rct':Ger:Gdje se nalazi datoteka'+' '+FileName+'?')
1. Idi u Corpus.ini i provjeri da li je System=1, 
	- ako nije, u result stavi 'Gdje se nalazi datoteka'
	- ako je, u Corpus.ini pročitaj CurrentLang (recimo da je "English") i idi na 2
2. Idi u Folder System\LNG\English i traži datoteku "ScriptGer.lng". Script + Lijevi Dio + .LNG
	- ako je nema, kreiraj je i upiši izraz za prevođenje 'Gdje se nalazi datoteka=', Result:='*Gdje se nalazi datoteka'
	- ako je ima, traži izraz 'Gdje se nalazi datoteka'
		- ako ga ima pokupi vrijednost prijevoda i zapamti kao result
		- ako ga nema dopiši izraz za prevođenje 'Gdje se nalazi datoteka', Result:='*Gdje se nalazi datoteka'

ShowMessage (rct'Gdje se nalazi datoteka'+' '+FileName+'?')		
1. Sve isto kao gore, ali se traži u datoteci ScriptsAll


}

Function Language:string;
// daje vrijednosti varijable "CurrentLanguage" iz "Corpus.ini"
var
	path:string;
	jezik:string;
Begin
	path:=prgFolder+'system\corpus.ini';
	
	//rcFindSS(FileName,SectionName,ValueName:string):string;
	jezik:=rcFindSS(path,'Main','CurrentLang');
	// print('Jezik: '+jezik);
	if Length(jezik)>1 then result:=jezik
							 else result:='';
End;


Function xrcFileCheckx(file_name:string):boolean;
// provjerava da li postoji datoteka
// ako ne postoji, daje poruku 
Begin
	// Showmessage ('fc1' +#13#10+ file_name)	;
  	if not (FileExists(file_name)) then begin
			// Showmessage ('fc2 NE POSTOJI' +#13#10+ file_name)	;
			// Showmessage ('Nema datoteke:' +#13#10+file_name+#13#10+#13#10
			//				+'Prekidam izvođenje skripte!'+#13#10+#13#10
			//				+'Doviđenja!');
			result:=false;
		end else begin
			// Showmessage ('fc2 POSTOJI' +#13#10+ file_name)	;
			// Showmessage ('Postoji datoteka:' +#13#10+ file_name);
			result:=true;
	
	end;
	// Showmessage ('fc3 IZLAZ' +#13#10+ file_name)	;
end;


function xfind_value_int(FileName,ValueName:string):integer;
// Find property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if ValueName not found or result is not integer, return 0 (zero)
// if ValueName found, but without value, ValueName will be deleted, and file will be saved
var
   list:TStringList;
   ind:integer;
   Value,ResultValue:string;
Begin
   
   list:=TStringList.Create;
   try list.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_int'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   value:=list.Values[ValueName];          // BUT, in this case value will be ''
   if (ind>=0) and (value='') then begin                      //  Name exist but no value
                List.Delete(List.IndexOfName(ValueName));     // delete name from list
                list.SaveToFile(FileName);                    // save new list to file
                // search again!!
                ind:=List.IndexOfName(ValueName);             // search again :-)
                value:=list.Values[ValueName];
   end; // if 
	
   if (ind>=0) and (not(value='')) then ResultValue:=value
                                   else begin  
                                      // ShowMessage( 'Function: Find_value_int'+#13#10+'Cannot find name "'+ ValueName+'" in file "'+FileName+'"');
                                      ResultValue:='0';
                                   end;
   // try to convert to integer   
   try 
       Result:=StrToInt(ResultValue); 
   except 
       Result:=0; 
       Showmessage( 'Value "'+ValueName+'" in file "'+FileName+'" cannot be converted to integer number!'+#10#13+'Assigning zero');   
   end;

   
end;   // function find_value_int 
 
function xfind_value_str(FileName,ValueName:string):string;
// Find property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if ValueName not found return ' '
// if ValueName found, but without value, ValueName will be deleted, and file will be saved
var
   list:TStringList;
   ind:integer;
   Value,ResultValue:string;
Begin
	// print('f(x): find_value_str Start');
	// print('ulaz: '+FileName+' | '+ValueName);
   list:=TStringList.Create;
   try list.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   value:=list.Values[ValueName];          // BUT, in this case value will be ''
   if (ind>=0) and (value='') then begin                      //  Name exist but no value
                List.Delete(List.IndexOfName(ValueName));     // delete name from list
                list.SaveToFile(FileName);                    // save new list to file
                // search again!!
                ind:=List.IndexOfName(ValueName);             // search again :-)
                value:=list.Values[ValueName];
   end; // if 
   if (ind>=0) and (not(value='')) then ResultValue:=value
                                   else begin  
                                      // ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find name "'+ ValueName+'" in file "'+FileName+'"');
                                      ResultValue:=' ';
                                   end;



   Result:=ResultValue; 
   
	// print('result: '+result);
   // print('f(x): find_value_str End');   
end;   // function find_value_int 



