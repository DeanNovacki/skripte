var
   win:TForm; labx, laby:TLabel;pan:TPanel;
       
function rPanel(name:string; parent: TWinControl):TPanel;
// make panel
var p1:Tpanel;
begin
   p1:=Tpanel.create(parent); p1.parent:=parent; p1.name:=name;
   // p1.Left:=x; p1.Top:=y; p1.width:=w; p1.height:=h;
   result:=p1;
end;
function rButton(name:string; parent: TWinControl; x,y:Integer):TButton;
// make button
var  b1:TButton;
begin
   b1:=TButton.create(parent); b1.parent:=parent; b1.name:=name;
   b1.Left:=x; b1.Top:=y; 
   result:=b1;
end;
function rLabel(lname:string; lparent: TWinControl; lx,ly:Integer):TLabel;
// make label
var  L1:TLabel;
begin
   L1:=TLabel.create(lparent); L1.parent:=lparent; L1.name:=lname;
   L1.Left:=lx; L1.Top:=ly;
   result:=L1;
end;


// main ==============================================
         
begin
   // create form  
   win:=TForm.create(nil);
   win.width:=600; win.height:=600;
   
   
   pan:=rPanel ('Panel_10',  win);
   labx:=rLabel ('',  pan,  10, 10);
   laby:=rLabel ('',  pan,  10, 30);

   // application.MainForm

   labx.caption:='Label caption �Ɗ�';
     
   // show form and wait 
   win.showmodal;

   // destroy form
   win.free; 
end.