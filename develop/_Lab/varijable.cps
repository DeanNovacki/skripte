program varijable;

var
   i:integer;
   elm:TElement;
   strFormula:String; 
        

Begin

   // TESTIRATI U EDITORU
   elm:=e;

   // Provjera da li postoji neka varijabla u elementu elm
   if elm.fvarijable.isvar('Polica_uza') then Writeln ('Postoji varijabla "Polica_uza"');
   
   // �itanje izra�unate vrijednosti varijable FLOAT
   if elm.fvarijable.isvar('Polica_uza') then Writeln ( 'Float vrijednost joj je ' + FloatToStr(elm.fvarijable.Parsevar('Polica_uza')));

   // �itanje IZRA�UNATE vrijednosti varijable u STRING obliku
   if elm.fvarijable.isvar('Polica_uza') then Writeln ( 'String vrijednost joj je ' + elm.fvarijable.ParsevarAsString('Polica_uza')); 
   
   // �itanje STRING vrijednosti (formule) varijable u STRING obliku
   if elm.fvarijable.isvar('Polica_uza') then Begin
      For i:=0 to elm.Fvarijable.Count-1  
          if elm.fvarijable.isvar('Polica_uza') then Begin
             // cijeli red je elm.Fvarijable[i] (u ovom slu�aju 'Polica_uza=2+1')
             // desna strana iza znaka = je ono �to se tra�i
             strFormula:=rcRightSide(elm.Fvarijable[i])
          end;   
      End;
   End;        
   Writeln ('String vrijednost varijable se dobiva uz kori�tenje rcRightSide iz rcEvents');   
     
   // Dodavanje varijable u element
   if not elm.fvarijable.isvar('Metla') then elm.fvarijable.add('Metla=1');
   Writeln ('Dodao sam varijablu "Metla" i dao joj vrijednost "1"');  
     
   // promjena vrijednosti postoje�e varijable
   if elm.fvarijable.isvar('Metla') then  elm.fvarijable.setvarvalue('Metla','2');
   Writeln ('Varijabli "Metla" sam vrijednost promijenio u "1"');
   
End.
