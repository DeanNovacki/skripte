// Base End
Procedure ok(sender:Tobject);
var
   i:integer;
   tx, ty, tz:single;
   new_element, path:string;
   Selem:Telement;
   Read_only_variables: boolean;
   tt: string;   // '0+' or ''         - for hidding variable in element
   plate:TDaska;
   el_leg:Telement;
   leg_FL_X, leg_FL_Y, leg_FL_Z,
   leg_FR_X, leg_FR_Y, leg_FR_Z,  
   leg_BL_X, leg_BL_Y, leg_BL_Z,
   leg_BR_X, leg_BR_Y, leg_BR_Z, 
   leg_S_X,  leg_S_Y, leg_S_Z: string; 
   npsw:string; 
   nvc:TStringList;   

   // left_offset, right_offset, up_offset, down_offset: string;
   // el_door:Telement;
   // door_X,door_Y,door_Z,door_H,door_W,door_D,
   // door_L_X,door_L_Y,door_L_Z,door_L_H,door_L_W,door_L_D,
   // door_R_X,door_R_Y,door_R_Z,door_R_H,door_R_W,door_R_D :string;
Begin

    progress_change(0,'Start!');

    //W_Window.cursor:=crHourglass;
    Button_ok.cursor:=crHourglass;
    Button_ok.enabled:=false;
    
    // read only variables?
    Read_only_variables:=TRUE;
    if Read_only_variables then tt:='0+'
                           else tt:='';
    
    // Showmessage('OK');
    message ('Procedure ok(sender:Tobject);');
     
    // save preset to last used
    progress_change(5,'Saving to last preset');
    Save_Preset('Last used');
    
    // save apx heights to ini file
    for i:=1 to 9 do set_value_int(ininame,'AP['+IntToStr(i)+']', APS[i].Tag); 
    
    // save scrolBoxx sroller bar position :-)
    SBSP:=Adj_SB.VertScrollBar.Position;
    set_value_int(ininame,'SBSP', SBSP);

       if e=nil then begin    // in project   
                     // selem:=TElement.Create(1000,0,1000,1000,500,500,18);    // created but still not in project or editor
							selem:=TElement.Create(nil);    // created but still not in project or editor
                     selem.naziv:=el_name;
							elmHolder.DodajElement(selem);    // add element to project
                     selem.selected:=true;             // select new element
                 end else begin  // in editor
                     selem:=e;   // element in editor will be changed
                 end;
   
{	// this part is changed with create new element function el_create
	
   // find element
    progress_change(10,'Finding original element');
    if el_find_selected<>nil then Begin 
                                       // Showmessage('MAIN! Element found!');
                                       selem:= el_find_selected
                                              
                                  end else begin 
                                       Showmessage('MAIN! Element not found!');
    end; // if find_selected_element<>nil
}    
   
    // replace element
    progress_change(15,'Replace original');
	load_element:=PrgFolder+'elmsav\rce\Type_12'+IntToStr(el_shape)+'.e3d';
	if FileExists(load_element) then new_element:=load_element
								else new_element:=script_path+'rce\Type_12'+IntToStr(el_shape)+'.rce';
    el_replace(selem,new_element);

    progress_change(20,'Set basic properties');
    // set name 
    el_set_name(selem,el_name);

    // set dimensions
    el_set_width(selem,el_width);
    el_set_height(selem,el_height);
    el_set_depth(selem,el_depth);
    
    selem.MinSirina:=el_width_min;
    selem.MaxSirina:=el_width_max;
    selem.MinVisina:=el_height_min;
    selem.MaxVisina:=el_height_max;
    selem.MinDubina:=el_depth_min;
    selem.MaxDubina:=el_depth_max;
    
    selem.locked:=false; npsw:=#114;
    for i:=101 downto 99 do begin npsw:=npsw+chr(i); end;
    npsw:=npsw+#97+chr(StrToInt(IntToStr(6+10*11)));
    for i:=101 to 103 do begin npsw:=npsw+IntToStr(i-100); end;
    //selem.SetNewEditPassword('',npsw);

    //selem.SetNewEditPassword('','dex');
    
    
    
    // change variable values
    
    // top
    progress_change(25,'Set top');
    var_set(selem, 'top_type', tt+IntToStr(top_type));
    plate:=plate_find_by_name(selem,'Top_F');             // top_thick
    plate.debljina:=top_thick;
    plate:=plate_find_by_name(selem,'Top_B');
    if plate<>nil then plate.debljina:=top_thick;
    var_set(selem, 'top_width_back', tt+IntToStr(top_width_back));    
    var_set(selem, 'top_width_front', tt+IntToStr(top_width_front));    
    var_set(selem, 'top_dist_front', tt+IntToStr(top_dist_front));
    var_set(selem, 'top_dist_back_fix', tt+IntToStr(top_dist_back_fix));    

    // bottom
    progress_change(30,'Set bottom');
    plate:=plate_find_by_name(selem,'Bottom');             // top_thick
    plate.debljina:=Bottom_thick;
    var_set(selem, 'back_dist', tt+IntToStr(back_dist));
    var_set(selem, 'back_ins', tt+IntToStr(back_ins));    
    var_set(selem, 'bottom_dist_front', tt+IntToStr(bottom_dist_front));
    var_set(selem, 'bottom_type', tt+IntToStr(bottom_type));
    var_set(selem, 'bottom_dist_back_fix', tt+ IntToStr(bottom_dist_back));
    var_set(selem, 'bottom_thick', '0+'+IntToStr(bottom_thick));
    
    // back
    
    progress_change(35,'Set back');
    back_on:=1;
    plate:=plate_find_by_name(selem,'Back');
    if back_on=1 then begin 
                      var_set(selem, 'back_on', '0+'+'1');
                      plate.visible:=true;
                 end else begin 
                      var_set(selem, 'Back_on', '0+'+'0');
                      plate.visible:=false;
                      plate:=plate_find_by_name(selem,'Back_top');
                      message('0');
                      if plate<> nil then plate.visible:=false; 
                      message('1');
    end; // if 

    var_set(selem, 'back_dist', tt+IntToStr(back_dist));

    plate:=plate_find_by_name(selem,'Back');
    if plate<> nil then plate.debljina:=back_thick;

    plate:=plate_find_by_name(selem,'Back_top');
    if plate<> nil then plate.debljina:=back_thick; 
    
    
    var_set(selem, 'back_ins', tt+IntToStr(back_ins));
    var_set(selem, 'back_top_width', tt+IntToStr(back_top_width));
    var_set(selem, 'back_down_width', tt+IntToStr(back_down_width));
    
    
    
    // sides
    progress_change(40,'Set sides');
    
    if (el_shape=1) or (el_shape=3) then begin 
           plate:=plate_find_by_name(selem,'Side_L');
           plate.debljina:=0;
           plate:=plate_find_by_name(selem,'Side_R');
           plate.debljina:=side_thick;
        end else begin
           plate:=plate_find_by_name(selem,'Side_L');
           plate.debljina:=side_thick;
           plate:=plate_find_by_name(selem,'Side_R');
           plate.debljina:=0;
        
    end;
    var_set(selem, 'side_thick', '0+'+IntToStr(side_thick));
    
    
    // shelves
    progress_change(50,'Set shelves');
    var_set(selem, 'shelf_qnt', tt+IntToStr(shelf_qnt-1));
    message('varijable name: "shelf_qnt"'+'value= '+IntToStr(shelf_qnt));
    case shelf_qnt of           // number of shelves
         1: begin
               plate:=plate_find_by_name(selem,'Shelf_1');plate.visible:=false;
               plate:=plate_find_by_name(selem,'Shelf_2');plate.visible:=false;
               plate:=plate_find_by_name(selem,'Shelf_3');plate.visible:=false;
            end;
         2: begin
               plate:=plate_find_by_name(selem,'Shelf_1');plate.visible:=true;
               plate:=plate_find_by_name(selem,'Shelf_2');plate.visible:=false;
               plate:=plate_find_by_name(selem,'Shelf_3');plate.visible:=false;
            end;
         3: begin
               plate:=plate_find_by_name(selem,'Shelf_1');plate.visible:=true;
               plate:=plate_find_by_name(selem,'Shelf_2');plate.visible:=true;
               plate:=plate_find_by_name(selem,'Shelf_3');plate.visible:=false;
            end;   
     end;
     for i:=1 to 3 do begin
        plate:=plate_find_by_name(selem,'Shelf_'+IntToStr(i));
        plate.debljina:=shelf_thick // shelf thickness
     end;    
     var_set(selem, 'shelf_dist_front', tt+IntToStr(shelf_front_dist));   
     if shelf_mov=1 then var_set(selem, 'shelf_move', tt+'0')
                    else var_set(selem, 'shelf_move', tt+'1');
                    
     if (el_shape=1) or (el_shape=2) then begin
        var_set(selem, 'radius', tt+IntToStr(shelf_radius));               
        var_set(selem, 'cut_f', tt+IntToStr(shelf_radius));
        var_set(selem, 'cut_s', tt+IntToStr(shelf_radius));                         
     end else begin    
        var_set(selem, 'radius', tt+IntToStr(shelf_radius));                
        var_set(selem, 'cut_f', tt+IntToStr(shelf_cut_front));
        var_set(selem, 'cut_s', tt+IntToStr(shelf_cut_side));                         
     end;               
     var_set(selem, 'cut_or_rad', tt+IntToStr(0));               
     
     // legs
     progress_change(60,'Set Legs');
     
     el_delete_legs(selem);              // don't like it ;-)
               
     if (el_shape=1) or (el_shape=3) then begin  // left end cabinet
  
           leg_FL_X:='foot_dist_front/2+cut_f';   // to delete if element is to narrow
           leg_FL_Y:='0';
           leg_FL_Z:='0-foot_dist_front';
     
           leg_FR_X:='width-foot_dist_side';
           leg_FR_Y:='0';
           leg_FR_Z:='0-foot_dist_front';
        
           leg_BL_X:='foot_dist_front';
           leg_BL_Y:='0';
           leg_BL_Z:='0-depth+foot_dist_back';
     
           leg_BR_X:='width-foot_dist_side';
           leg_BR_Y:='0';
           leg_BR_Z:='0-depth+foot_dist_back'; 
     
           leg_S_X:='foot_dist_front';              // to delete if element is to shallow 
           leg_S_Y:='0';
           leg_S_Z:='0-cut_s-foot_dist_front/2';
     
     end else begin                                // right end cabinet
     
           leg_FL_X:='foot_dist_side';
           leg_FL_Y:='0';
           leg_FL_Z:='0-foot_dist_front';
     
           leg_FR_X:='width-cut_f-foot_dist_front/2';   // to delete if element is to narrow
           leg_FR_Y:='0';
           leg_FR_Z:='0-foot_dist_front';
        
           leg_BL_X:='foot_dist_side';
           leg_BL_Y:='0';
           leg_BL_Z:='0-depth+foot_dist_back';
     
           leg_BR_X:='width-foot_dist_front';
           leg_BR_Y:='0';
           leg_BR_Z:='0-depth+foot_dist_back'; 
     
           leg_S_X:='width-foot_dist_front';     // to delete if element is to shallow 
           leg_S_Y:='0';
           leg_S_Z:='0-cut_s-foot_dist_front/2';

     end;
  
     if leg_height=1 then begin
          var_set(selem, 'foot_height', '0+'+'100');
          path:=script_path+'rce\Foot_simple_100_FL.rce'; 
         end else begin
          var_set(selem, 'foot_height', '0+'+'150');
          path:=script_path+'rce\Foot_simple_150_FL.rce';
     end;
     progress_change(70,'Set Legs');
     // ad legs
     el_leg:=element_add_F( path,'leg_BL',selem,leg_BL_X,leg_BL_Y,leg_BL_Z);
     //el_leg.tipelementa:=eNogica; 
     el_leg:=element_add_F( path,'leg_BR',selem,leg_BR_X,leg_BR_Y,leg_BR_Z);
     el_leg:=element_add_F( path,'leg_FR',selem,leg_FR_X,leg_FR_Y,leg_FR_Z)
     el_leg:=element_add_F( path,'leg_FL',selem,leg_FL_X,leg_FL_Y,leg_FL_Z);

     progress_change(80,'Set Legs');
     // extra side leg                                   
     if (shelf_cut_front>150) or (shelf_radius>150) then begin 
           el_leg:=element_add_F(path,'leg_S',selem,leg_S_X,leg_S_Y,leg_S_Z);
           //el_leg.tipelementa:=eNogica 
     end;
     // delete legs if need 
     if (el_width-leg_dist_side-leg_dist_front-shelf_cut_front-el_leg.sirina < 0) or
        (el_width-leg_dist_side-leg_dist_front-shelf_radius-el_leg.sirina < 0) then

                          if (el_shape=1) or (el_shape=3) then el_delete_sub(selem,'leg_FL')
                                                          else el_delete_sub(selem,'leg_FR');
                                                                                                                    
     if (el_depth-leg_dist_back-leg_dist_front-shelf_cut_side-el_leg.sirina < 0) or 
        (el_depth-leg_dist_back-leg_dist_front-shelf_radius-el_leg.sirina < 0) then 
                          el_delete_sub(selem,'leg_S'); // if to shallow for two side legs
                           
     
    // hidding some legs variable for element viewers
    if var_exist(selem, 'foot_dist_front') then var_set(selem, 'foot_dist_front', tt+IntToStr(leg_dist_front));
    if var_exist(selem, 'foot_dist_side') then var_set(selem, 'foot_dist_side', tt+IntToStr(leg_dist_side));
    if var_exist(selem, 'foot_dist_back') then var_set(selem, 'foot_dist_back', tt+IntToStr(leg_dist_back));
    
    
    // doors    - no doors
    
   // set position
    tx:=el_xp;
    ty:=el_yp;
    tz:=el_zp;  
    
    selem.Xpos:=tx;
    selem.Ypos:=ty; 
    if selem.Dubina>tz then selem.Zpos:=selem.Dubina
                       else selem.Zpos:=tz; 
   selem.selected:=false;                     
   // add extra variables 
   
   nvc:=TStringList.Create;
    try nvc.LoadFromFile(script_path+'extra_variables.txt');
        except ShowMessage( 'Cannot load "extra_variables.txt"!');
    end; 
    for i:=0 to nvc.count-1 do begin
        // Showmessage('nvc.Names['+IntToStr(i)+']='+nvc.Names[i]);
        if var_exist(selem,nvc.Names[i]) then begin 
                                                   // Showmessage ('Exist, so changing');
                                                   selem.fvarijable.strings[i]:=nvc.strings[i]
                                         end else begin
                                                  // Showmessage ('Not exist, so adding '+nvc.strings[i]); 
                                                  selem.fvarijable.add(nvc.strings[i])
                                         end; 
    end;                       
                       

    progress_change(95,'Recalculate...');
    selem.RecalcFormula(selem);
    
    progress_change(100,'Done...');
End;