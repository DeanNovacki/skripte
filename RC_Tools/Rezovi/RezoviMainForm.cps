// design
   Main:=rcTool('Rezovi'); Main.Width:=800;
   Main.Height:=430;
   // Main.position:=poMainFormCenter;
   // desni panel
   Panel_Right:= rcPanel ( '' ,Main ); Panel_Right.ALign:=alRight; 
   Panel_Right.Width:=250;       // Sirina desnog panela!!!!
   Panel_Right.BorderWidth:=5;
   // lijevi panel
   Panel_Left:= rcPanel ( '' ,Main ); Panel_Left.ALign:=alClient;
   // tipke 
   ButOK:=rcButton('bOK','OK',Panel_Right,1,1);ButOK.Align:=alTop;
   ButOK.Default:=True;
   rcSpaceT(Panel_Right,2);
   ButTest:=rcButton('bTest','Test',Panel_Right,3,3);ButTest.Align:=alTop;
   ButTest.visible:=False;
   rcSpaceT(Panel_Right,4);
   ButCancel:=rcButton('bCancel','Cancel',Panel_Right,3,20);ButCancel.Align:=alBottom;
   rcSpaceB(Panel_Right,19);
   // MemoJ
   Panel_Memo:=rcPanel ( '' ,Panel_Right ); Panel_memo.Top:=18;Panel_Memo.ALign:=alClient; 
   cbDebug:=rcCheckBox('cbDB','Debug',Panel_Memo, 1,20);cbDebug.Align:=alBottom; 
   cbDebug.Font.Size:=7; cbDebug.visible:=False;
   If debug then cbDebug.checked:=True;
   MemoJ:=rcMemo('Memo_debug', Panel_Memo, 1,18,1,1);MemoJ.Align:=alClient;
   MemoJ.Color:=clBTNFace;MemoJ.Font.Size:=8;MemoJ.Ctl3D:=False;MemoJ.Font.Color:=clMaroon;
   
   // Main.show;    // prikazi formu da se vidi crtanje ostalih componenti
   // Main.FormStyle:=fsStayOnTop;
      
   // Naslov
   Panel_Up:=rcPanel ( '' ,Panel_Left ); Panel_Up.ALign:=alTop;   // dr�i UpR i UpL
   Panel_Up.Height:=40;
   PAnel_upR:=rcPanel ( '' ,Panel_Up ); Panel_UpR.ALign:=alRight;   // dr�i No
   Panel_UpR.width:=40;
   PAnel_upL:=rcPanel ( '' ,Panel_Up ); Panel_UpL.ALign:=alClient;   // dr�i Title, Info i No
   Panel_No:=rcPanel ( '' ,Panel_UpR); Panel_No.ALign:=alClient;
   Panel_No.Width:=60;
   Panel_Title:=rcPanel ( '' ,Panel_UpL ); Panel_Title.ALign:=alTop;
      Panel_Title.Height:=20;
      LabTemp:=rcLabel( '', 'Izrezivanje selektiranih dasaka  ',Panel_Title, 1,1,taLeftJustify);
      LabTemp.Font.Size:=9; LabTemp.Font.Style:=[fsBold];
      // ShowD('Panel_title je postavljen');
   
   // Info
   Panel_Info:=rcPanel ( '' ,Panel_UpL ); Panel_Info.ALign:=alClient;
      Panel_Info.Height:=20;
     // Label "selektirano"
      LabTemp:=rcLabel( '', 'Selektirano:',Panel_Info, 1,1,taLeftJustify);LabTemp.Align:=alLeft;
      LabTemp.Font.Size:=8; LabTemp.Font.Style:=[];
     // razmak
      rcSpaceL(Panel_Info,100);
     // broj selektiranih
      LabSelQnt:=rcLabel( '', '0',Panel_No, 200,1,taCenter);LabSelQnt.Align:=alClient;
      LabSelQnt.Font.Size:=20; LabSelQnt.Font.Style:=[fsBold];
     // razmak
      rcSpaceL(Panel_Info,300);
     // popis selektiranih
      SelTxt:='(Nista)';
      LabSelTxt:=rcLabel( '', SelTxt,Panel_Info, 400,1,taLeftJustify);LabSelTxt.Align:=alClient;
      LabSelTxt.Font.Size:=8; // LabSelTxt.Font.Style:=[fsBold];LabSelTxt.Enabled:=False;
      // ShowD('Panel_Info je postavljen');

   // PROPERTIES
   
   Pan_Prop:=rcPanel ( '' ,Panel_Left ); Pan_Prop.ALign:=alTop;
   // Pan_Prop.Height:=197;
   Pan_Prop.Height:=257;
   hss:='::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::';
   Pan_PropH:=rcPanel ( '' ,Pan_Prop ); Pan_PropH.ALign:=alTop;
   Pan_PropH.color:=clBtnShadow;
   Pan_propH.Height:=16; Pan_PropH.Caption:=hss+'  Osobine  '+hss;
   //Pan_propH.Font.style:=[fsBold];
   Pan_propH.Font.Size:=8; 
   Pan_propH.Hint:='Klik za pokazivanje ili skrivanje';
   Pan_propH.ShowHint:=True;
   // zaglavlje tablice
   Pan_redH:=rcPanel ( '' ,Pan_Prop ); Pan_RedH.ALign:=alTop;
   Pan_redH.caption:='zaglavlje tablice';Pan_redH.Height:=20;Pan_redH.Top:=100;
      // naslovi kolona u zaglavlju tablice
      // naziv propertija
      Pan_VarH:=rcPanel ( '' ,Pan_RedH ); Pan_VarH.ALign:=alLeft;
      Pan_VarH.Caption:='Osobina ';Pan_VarH.Width:=60;Pan_VarH.Left:=1;
      Pan_VarH.Alignment:=taRightJustify; Pan_VarH.Font.Size:=8;
      // Vrijednost varijable
      Pan_ValH:=rcPanel ( '' ,Pan_RedH ); Pan_ValH.ALign:=alLeft;
      Pan_ValH.Caption:='rezult. ';Pan_ValH.Width:=50;Pan_ValH.Left:=100;
      Pan_ValH.Alignment:=taRightJustify; Pan_ValH.Font.Size:=8;
      // Formula
      Pan_ForH:=rcPanel ( '' ,Pan_RedH ); Pan_ForH.ALign:=alClient;
      Pan_ForH.Caption:='formula ';Pan_ForH.Width:=5;Pan_ForH.Left:=100;
      Pan_ForH.Alignment:=taLeftJustify; Pan_ForH.Font.Size:=8;
      // CheckBox
      // CB_SelH:=rcCheckBox ( '' ,'',Pan_RedH,1000,1 ); CB_SelH.ALign:=alRight;
      // CB_SelH.Width:=14; CB_SelH.Font.Size:=8;
      
      // Hint
      Lab_HintH:=rcPanel ( '',Pan_RedH); Lab_HintH.Align:=alRight;
      Lab_HintH.Caption:='promijeni';Lab_HintH.Width:=50;Lab_HintH.Left:=1100;
      Lab_HintH.Alignment:=taCenter; Lab_HintH.Font.Size:=8;

      
   // redovi tablice
   For i:=0 to 10 do begin
      SetArrayLength(Pan_Red, i+1);
      // prazni redovi
      Pan_red[i]:=rcPanel ( '' ,Pan_Prop ); Pan_Red[i].ALign:=alTop;
      // Pan_red[i].caption:='p'+IntToStr(i);
      Pan_red[i].Height:=20;Pan_red[i].Top:=200+i*100;
         // naziv propertija
         SetArrayLength(Pan_Var, i+1);
         Pan_Var[i]:=rcPanel ( '' ,Pan_Red[i] ); Pan_Var[i].ALign:=alLeft;
         Pan_Var[i].Width:=80;Pan_Var[i].Left:=100;
         Pan_Var[i].Alignment:=taRightJustify; Pan_Var[i].Font.Size:=9;
         Case i of
            0:Pan_Var[i].caption:='X ';
            1:Pan_Var[i].caption:='Y ';
            2:Pan_Var[i].caption:='Z ';
            3:Pan_Var[i].caption:='Visina ';
            4:Pan_Var[i].caption:='�irina ';
            5:Pan_Var[i].caption:='Debljina ';
            // 6:Pan_Var[i].caption:='Dubina ';
            6:Pan_Var[i].caption:='Tip ';
            7:Pan_Var[i].caption:='Primjedba ';
				8:Pan_Var[i].caption:='X Kut ';
				9:Pan_Var[i].caption:='Y Kut ';
				10:Pan_Var[i].caption:='Z Kut ';
         end;  // case
         
         // vrijednost propertija
         SetArrayLength(Pan_Val, i+1);
         Pan_Val[i]:=rcEdit ( '' ,Pan_Red[i],1,1 ); Pan_Val[i].ALign:=alLeft;
         Pan_Val[i].Width:=50;Pan_Val[i].Left:=100;
         Pan_Val[i].Font.Size:=9;Pan_Val[i].ShowHint:=True;
         // Formula
         SetArrayLength(Pan_For, i+1);
         // ShowD('i='+IntToStr(i));
         if i=6 then begin
            // ShowD('Ovdje se kreira Combo Box');
            CoB_tip:=rcBoardType(Pan_Red[i],200,1);
            // ShowD('Kreiran je Combo Box');
            CoB_tip.Align:=alClient;
            CoB_tip.ItemIndex:=-1;
            // Integer(TipDaske)
         end else begin
            // ShowD('Kreiram rcEdit');
            Pan_For[i]:=rcEdit ('' ,Pan_Red[i],1,1 ); Pan_For[i].ALign:=alClient;
            Pan_For[i].Width:=50;Pan_For[i].Left:=200;
            Pan_For[i].Font.Size:=8;Pan_For[i].Font.Name:='Courier New';
            Pan_For[i].ShowHint:=True;
         end;
         if i=5 then begin  // nema formule za debljinu
                           Pan_For[i].Visible:=False; 
                           // Pan_For[i].Color:=clGray; 
                     end;
         if i=7 then Pan_Val[i].visible:=False;  // nema vrijednosti za napomene            
         // Selection CheckBox
         // ShowD('Ovdje se kreira CheckBox');
         SetArrayLength(CB_Sel, i+1);
         CB_Sel[i]:=rcCheckBox ( '' ,'',Pan_Red[i],1000,1 ); CB_Sel[i].ALign:=alRight;
         CB_Sel[i].Width:=14; CB_Sel[i].Font.Size:=8; 
         CB_Sel[i].checked:=true;
         
         if i=6 then Pan_Val[i].ShowHint:=True;Pan_Val[i].font.color:=clSilver;
         // Hint
         // ShowD('Ovdje se kreira Hint');
         {
         SetArrayLength(Lab_Hint, i+1);
         Lab_Hint[i]:=rcPanel ( '',Pan_Red[i]); Lab_Hint[i].Align:=alRight;
         Lab_Hint[i].Caption:='???';Lab_Hint[i].Width:=30;Lab_Hint[i].Left:=1100;
         Lab_Hint[i].Alignment:=taCenter; Lab_Hint[i].Font.Size:=8;
         Lab_Hint[i].ShowHint:=True;
         } 
    
   end;  // for i
   ShowD('Design properties zavrsen');  
   
   // CUTS - REZOVI
   
   Pan_Cuts:=rcPanel ( '' ,Panel_Left ); Pan_Cuts.ALign:=alTop;
   Pan_Cuts.Height:=18; 
   // Pan_Cuts.Color:=clRed;
   
   // header button
   hss:='::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::';
   Pan_CutsH:=rcPanel ( '' ,Pan_Cuts ); 
   Pan_CutsH.ALign:=alTop;
   Pan_CutsH.color:=clBtnShadow;
   Pan_CutsH.Height:=16; 
   Pan_CutsH.Caption:=hss+'  Rezovi  '+hss;
   // Pan_CutsH.Font.Color:=clGrayText;
   Pan_CutsH.Font.Size:=8; 
   Pan_CutsH.Hint:='Klik za pokazivanje ili skrivanje';
   Pan_CutsH.ShowHint:=True;
   
   // images holder
   Pan_CutsI:=rcPanel ( '' ,Pan_Cuts ); 
   Pan_CutsI.ALign:=alLeft;
   Pan_CutsI.Width:=310;
   
   // Horizontal orientation
   Pan_PicH:=rcPanel ('', Pan_CutsI);
   Pan_PicH.Left:=4; Pan_PicH.Top:=4; Pan_PicH.Width:=299; Pan_PicH.Height:=299;
   Pan_PicH.BorderStyle:=bsNone;
   Pan_PicH.BorderWidth:=0;
   Pan_PicH.BevelInner:=bvNone;
   Pan_PicH.BevelOuter:=bvNone;
   Pan_PicH.BevelWidth:=0;
   Pan_PicH.color:=clWhite;
   ipath:=PrgFolder+'skripte\RC_Tools\Rezovi\slike\';
	ShowD('ShowD Putanja je "'+ipath);
	ShowN('ShowN Putanja je "'+ipath);
   // Load Images
   HCent:=rcLoadImage('',ipath+'HCent.jpg',Pan_picH,88, 87);
   // Load normal
   //{
   H1N_0:=rcLoadImage('',ipath+'H1N_0.jpg',Pan_picH, 20,211);
   H1T_0:=rcLoadImage('',ipath+'H1T_0.jpg',Pan_picH, 20, 87);
   H2N_0:=rcLoadImage('',ipath+'H2N_0.jpg',Pan_picH, 20, 20);
   H2T_0:=rcLoadImage('',ipath+'H2T_0.jpg',Pan_picH, 88, 20);
   H3N_0:=rcLoadImage('',ipath+'H3N_0.jpg',Pan_picH,211, 20);
   H3T_0:=rcLoadImage('',ipath+'H3T_0.jpg',Pan_picH,211, 87);
   H4N_0:=rcLoadImage('',ipath+'H4N_0.jpg',Pan_picH,211,211);
   H4T_0:=rcLoadImage('',ipath+'H4T_0.jpg',Pan_picH,88, 211);
   // }
   // Load radius 0
   // {
   H1R_0:=rcLoadImage('',ipath+'H1R_0.jpg',Pan_picH, 20,211);
   H2R_0:=rcLoadImage('',ipath+'H2R_0.jpg',Pan_picH, 20, 20);
   H3R_0:=rcLoadImage('',ipath+'H3R_0.jpg',Pan_picH,211, 20);
   H4R_0:=rcLoadImage('',ipath+'H4R_0.jpg',Pan_picH,211,211);
   // }
   // Load radius 1
   // {
   H1R_1:=rcLoadImage('',ipath+'H1R_1.jpg',Pan_picH, 20,211);
   H2R_1:=rcLoadImage('',ipath+'H2R_1.jpg',Pan_picH, 20, 20);
   H3R_1:=rcLoadImage('',ipath+'H3R_1.jpg',Pan_picH,211, 20);
   H4R_1:=rcLoadImage('',ipath+'H4R_1.jpg',Pan_picH,211,211);
   // }
   // Load cutIN (/) 0
   // {
   H1I_0:=rcLoadImage('',ipath+'H1I_0.jpg',Pan_picH, 20,211);
   H2I_0:=rcLoadImage('',ipath+'H2I_0.jpg',Pan_picH, 20, 20);
   H3I_0:=rcLoadImage('',ipath+'H3I_0.jpg',Pan_picH,211, 20);
   H4I_0:=rcLoadImage('',ipath+'H4I_0.jpg',Pan_picH,211,211);
   // }
   // Load cutIN (/) 1
   // {
   H1I_1:=rcLoadImage('',ipath+'H1I_1.jpg',Pan_picH, 20,211);
   H2I_1:=rcLoadImage('',ipath+'H2I_1.jpg',Pan_picH, 20, 20);
   H3I_1:=rcLoadImage('',ipath+'H3I_1.jpg',Pan_picH,211, 20);
   H4I_1:=rcLoadImage('',ipath+'H4I_1.jpg',Pan_picH,211,211);
   // }
   // Load cut OUT (L) 0
   // {
   H1U_0:=rcLoadImage('',ipath+'H1U_0.jpg',Pan_picH, 20,211);
   H2U_0:=rcLoadImage('',ipath+'H2U_0.jpg',Pan_picH, 20, 20);
   H3U_0:=rcLoadImage('',ipath+'H3U_0.jpg',Pan_picH,211, 20);
   H4U_0:=rcLoadImage('',ipath+'H4U_0.jpg',Pan_picH,211,211);
   // }
   // Load cut OUT (L) 1
   // {
   H1U_1:=rcLoadImage('',ipath+'H1U_1.jpg',Pan_picH, 20,211);
   H2U_1:=rcLoadImage('',ipath+'H2U_1.jpg',Pan_picH, 20, 20);
   H3U_1:=rcLoadImage('',ipath+'H3U_1.jpg',Pan_picH,211, 20);
   H4U_1:=rcLoadImage('',ipath+'H4U_1.jpg',Pan_picH,211,211);
   // }
   // Load sides 1
   // {
   H1T_1:=rcLoadImage('',ipath+'H1T_1.jpg',Pan_picH, 20, 87);
   H2T_1:=rcLoadImage('',ipath+'H2T_1.jpg',Pan_picH, 88, 20);
   H3T_1:=rcLoadImage('',ipath+'H3T_1.jpg',Pan_picH,211, 87);
   H4T_1:=rcLoadImage('',ipath+'H4T_1.jpg',Pan_picH,88, 211);
   // }
   // Load sides 2
   // {
   H1T_2:=rcLoadImage('',ipath+'H1T_2.jpg',Pan_picH, 20, 87);
   H2T_2:=rcLoadImage('',ipath+'H2T_2.jpg',Pan_picH, 88, 20);
   H3T_2:=rcLoadImage('',ipath+'H3T_2.jpg',Pan_picH,211, 87);
   H4T_2:=rcLoadImage('',ipath+'H4T_2.jpg',Pan_picH,88, 211);
   // }
   // Load sides 3
   // {
   H1T_3:=rcLoadImage('',ipath+'VH1T_3.jpg',Pan_picH,1,20);
   H2T_3:=rcLoadImage('',ipath+'VH2T_3.jpg',Pan_picH,20,1);
   H3T_3:=rcLoadImage('',ipath+'VH3T_3.jpg',Pan_picH,279,20);
   H4T_3:=rcLoadImage('',ipath+'VH4T_3.jpg',Pan_picH,20,279);
   // }
   // Vertikal -Front orientation
   Pan_PicV:=rcPanel ('', Pan_CutsI);
   Pan_PicV.Left:=4; Pan_PicV.Top:=4; Pan_PicV.Width:=299; Pan_PicV.Height:=299;
   Pan_PicV.BorderStyle:=bsNone;
   Pan_PicV.BorderWidth:=0;
   Pan_PicV.BevelInner:=bvNone;
   Pan_PicV.BevelOuter:=bvNone;
   Pan_PicV.BevelWidth:=0;
   Pan_PicV.color:=clWhite;
   //  ZA BRISATI - Ima ve� gore! ipath:=PrgFolder+'skripte\Japa\Pictures\osobine\';
   // Load Images
   VCent:=rcLoadImage('',ipath+'VCent.jpg',Pan_picV,88, 87);
   VCent.visible:=False;
   FCent:=rcLoadImage('',ipath+'FCent.jpg',Pan_picV,88, 87);
   FCent.visible:=False;
   // Load normal
   //{
   V1N_0:=rcLoadImage('',ipath+'V1N_0.jpg',Pan_picV, 20,211);
   V1T_0:=rcLoadImage('',ipath+'V1T_0.jpg',Pan_picV, 20, 87);
   V2N_0:=rcLoadImage('',ipath+'V2N_0.jpg',Pan_picV, 20, 20);
   V2T_0:=rcLoadImage('',ipath+'V2T_0.jpg',Pan_picV, 88, 20);
   V3N_0:=rcLoadImage('',ipath+'V3N_0.jpg',Pan_picV,211, 20);
   V3T_0:=rcLoadImage('',ipath+'V3T_0.jpg',Pan_picV,211, 87);
   V4N_0:=rcLoadImage('',ipath+'V4N_0.jpg',Pan_picV,211,211);
   V4T_0:=rcLoadImage('',ipath+'V4T_0.jpg',Pan_picV,88, 211);
   // }
   // Load radius 0
   // {
   V1R_0:=rcLoadImage('',ipath+'V1R_0.jpg',Pan_picV, 20,211);
   V2R_0:=rcLoadImage('',ipath+'V2R_0.jpg',Pan_picV, 20, 20);
   V3R_0:=rcLoadImage('',ipath+'V3R_0.jpg',Pan_picV,211, 20);
   V4R_0:=rcLoadImage('',ipath+'V4R_0.jpg',Pan_picV,211,211);
   // }
   // Load radius 1
   // {
   V1R_1:=rcLoadImage('',ipath+'V1R_1.jpg',Pan_picV, 20,211);
   V2R_1:=rcLoadImage('',ipath+'V2R_1.jpg',Pan_picV, 20, 20);
   V3R_1:=rcLoadImage('',ipath+'V3R_1.jpg',Pan_picV,211, 20);
   V4R_1:=rcLoadImage('',ipath+'V4R_1.jpg',Pan_picV,211,211);
   // }
   // Load cutIN (/) 0
   // {
   V1I_0:=rcLoadImage('',ipath+'V1I_0.jpg',Pan_picV, 20,211);
   V2I_0:=rcLoadImage('',ipath+'V2I_0.jpg',Pan_picV, 20, 20);
   V3I_0:=rcLoadImage('',ipath+'V3I_0.jpg',Pan_picV,211, 20);
   V4I_0:=rcLoadImage('',ipath+'V4I_0.jpg',Pan_picV,211,211);
   // }
   // Load cutIN (/) 1
   //  {
   V1I_1:=rcLoadImage('',ipath+'V1I_1.jpg',Pan_picV, 20,211);
   V2I_1:=rcLoadImage('',ipath+'V2I_1.jpg',Pan_picV, 20, 20);
   V3I_1:=rcLoadImage('',ipath+'V3I_1.jpg',Pan_picV,211, 20);
   V4I_1:=rcLoadImage('',ipath+'V4I_1.jpg',Pan_picV,211,211);
   // }
   // Load cut OUT (L) 0
   // {
   V1U_0:=rcLoadImage('',ipath+'V1U_0.jpg',Pan_picV, 20,211);
   V2U_0:=rcLoadImage('',ipath+'V2U_0.jpg',Pan_picV, 20, 20);
   V3U_0:=rcLoadImage('',ipath+'V3U_0.jpg',Pan_picV,211, 20);
   V4U_0:=rcLoadImage('',ipath+'V4U_0.jpg',Pan_picV,211,211);
   // }
   // Load cut OUT (L) 1
   // {
   V1U_1:=rcLoadImage('',ipath+'V1U_1.jpg',Pan_picV, 20,211);
   V2U_1:=rcLoadImage('',ipath+'V2U_1.jpg',Pan_picV, 20, 20);
   V3U_1:=rcLoadImage('',ipath+'V3U_1.jpg',Pan_picV,211, 20);
   V4U_1:=rcLoadImage('',ipath+'V4U_1.jpg',Pan_picV,211,211);
   // }
   // Load sides 1
   // {
   V1T_1:=rcLoadImage('',ipath+'V1T_1.jpg',Pan_picV, 20, 87);
   V2T_1:=rcLoadImage('',ipath+'V2T_1.jpg',Pan_picV, 88, 20);
   V3T_1:=rcLoadImage('',ipath+'V3T_1.jpg',Pan_picV,211, 87);
   V4T_1:=rcLoadImage('',ipath+'V4T_1.jpg',Pan_picV,88, 211);
   // }
   // Load sides 2
   // {
   V1T_2:=rcLoadImage('',ipath+'V1T_2.jpg',Pan_picV, 20, 87);
   V2T_2:=rcLoadImage('',ipath+'V2T_2.jpg',Pan_picV, 88, 20);
   V3T_2:=rcLoadImage('',ipath+'V3T_2.jpg',Pan_picV,211, 87);
   V4T_2:=rcLoadImage('',ipath+'V4T_2.jpg',Pan_picV,88, 211);
   // }
   // Load sides 3
   // {
   V1T_3:=rcLoadImage('',ipath+'VH1T_3.jpg',Pan_picV,1,20);
   V2T_3:=rcLoadImage('',ipath+'VH2T_3.jpg',Pan_picV,20,1);
   V3T_3:=rcLoadImage('',ipath+'VH3T_3.jpg',Pan_picV,279,20);
   V4T_3:=rcLoadImage('',ipath+'VH4T_3.jpg',Pan_picV,20,279);
   // }
   
   
     
   // cuts properties holder
   Pan_CutsP:=rcPanel ( '' ,Pan_Cuts ); 
   Pan_CutsP.Align:=alClient;
   
   // Pan_CutsP.Color:=clGreen;
   
   // cuts properties
   Pan_use:=rcPanel ( '' ,Pan_CutsP ); Pan_Use.top:=0; Pan_Use.Align:=alTop;
   Pan_Use.Height:=18; Pan_Use.BorderWidth:=1;
   CB_Cuse:=rcCheckBox('', 'Promijeni rezove',pan_CutsH,pan_CutsH.clientWidth-15,1);
   Cb_Cuse.Height:=15;
   Cb_Cuse.Width:=12;
   Cb_Cuse.Hint:='Ozna�i da napravi promjenu';
   // cb_Cuse.ALign:=alClient;
   
   Pan_Sm:=rcPanel ( '' ,Pan_CutsP ); Pan_Sm.top:=18; Pan_Sm.Align:=alTop;
   Pan_Sm.Height:=18; Pan_Sm.BorderWidth:=1;
   Lab_sm:=rcLabel('',' Smijer: ',Pan_sm, 5,1,taRightJustify); Lab_Sm.ALign:=alLeft;Lab_Sm.width:=37;
   Lab_Vsm:=rcLabel('','nepoznat ',Pan_sm, 100,1,taLeftJustify); Lab_Vsm.ALign:=alClient;
   Lab_Vsm.Font.Style:=[fsBold];
   
   Pan_N:=rcPanel ( '' ,Pan_CutsP ); Pan_N.top:=100; Pan_N.Align:=alTop;Pan_N.Height:=36; Pan_N.BorderWidth:=1;
   Lab_N:=rcLabel('','Nema reza.'+#13#10+'Klikni ponovo za promjenu!',Pan_N, 1,1,taLeftJustify); 
   Lab_N.ALign:=alLeft; Lab_N.Font.Size:=8;
   Pan_N.Visible:=false;
   
   Pan_A:=rcPanel ( '' ,Pan_CutsP ); Pan_A.top:=100; Pan_A.Align:=alTop;Pan_A.Height:=18; Pan_A.BorderWidth:=1;
   Lab_A:=rcLabel('','     A: ',Pan_A, 1,1,taRightJustify); Lab_A.ALign:=alLeft;Lab_A.width:=30;
   Sl_A:=rcScroll('',Pan_A, 50,1);Sl_A.ALign:=alClient;
   Sl_A.Min:=0;Sl_A.Max:=300;
   rcSpaceR( Pan_A,100);
   Ed_A:=rcEdit('',Pan_A, 300,1);Ed_A.ALign:=alRight; Ed_A.Width:=40;
   rcSpaceR( Pan_A,800);
   
   Pan_B:=rcPanel ( '' ,Pan_CutsP ); Pan_B.top:=200; Pan_B.Align:=alTop;Pan_B.Height:=18; Pan_B.BorderWidth:=1;
   cb_B:=rcCheckBox('', '',pan_B,0,0);cb_B.Width:=12;cb_B.ALign:=alLeft;
   cb_B.hint:='Uklju�i da bude razli�ito od A';
   Lab_B:=rcLabel('','B: ',Pan_B, 50,1,taRightJustify); Lab_B.ALign:=alLeft;Lab_B.width:=18;
   Sl_B:=rcScroll('',Pan_B, 100,1);Sl_B.ALign:=alClient;
   Sl_B.Min:=0;Sl_B.Max:=300;
   rcSpaceR( Pan_B,300);
   Ed_B:=rcEdit('',Pan_B, 600,1);Ed_B.ALign:=alRight; Ed_B.Width:=40;
   rcSpaceR( Pan_B,1000);
   Lab_B.Enabled:=false; SL_B.enabled:=False; Ed_B.Enabled:=False;
   
   Pan_C:=rcPanel ( '' ,Pan_CutsP ); Pan_C.top:=300; Pan_C.Align:=alTop;Pan_C.Height:=18; Pan_C.BorderWidth:=1;
   rb_C:=rcRadioButton('', '',pan_C,0,0);rb_C.Width:=12;rb_C.ALign:=alLeft;rb_C.Left:=0;
   rb_C.hint:='Ovdje je po�etak?';rb_C.checked:=true;
   Lab_C:=rcLabel('','     C: ',Pan_C, 1,1,taRightJustify); Lab_C.ALign:=alLeft;Lab_C.width:=18;Lab_C.Left:=20;
   Sl_C:=rcScroll('',Pan_C, 50,1);Sl_C.ALign:=alClient; Sl_C.Min:=0;Sl_C.Max:=300;
   rcSpaceR( Pan_C,100);
   Ed_C:=rcEdit('',Pan_C, 300,1);Ed_C.ALign:=alRight; Ed_C.Width:=40;
   rcSpaceR( Pan_C,800);

   Pan_D:=rcPanel ( '' ,Pan_CutsP ); Pan_D.TOP:=400; Pan_D.Align:=alTop;Pan_D.Height:=18; Pan_D.BorderWidth:=1;
   Lab_D:=rcLabel('','     D: ',Pan_D, 1,1,taRightJustify); Lab_D.ALign:=alLeft;Lab_D.width:=30;
   Sl_D:=rcScroll('',Pan_D, 50,1);Sl_D.ALign:=alClient;Sl_D.Min:=0;Sl_D.Max:=300;
   rcSpaceR( Pan_D,100);
   Ed_D:=rcEdit('',Pan_D, 300,1);Ed_D.ALign:=alRight; Ed_D.Width:=40;
   rcSpaceR( Pan_D,800);   

   Pan_F:=rcPanel ( '' ,Pan_CutsP ); Pan_F.top:=500; Pan_F.Align:=alTop;Pan_F.Height:=18; Pan_F.BorderWidth:=1;
   cb_F:=rcCheckBox('', '',pan_F,0,0);cb_F.Width:=12;cb_F.ALign:=alLeft;
   cb_F.hint:='Uklju�i da bude razli�ito od D';
   Lab_F:=rcLabel('','     F: ',Pan_F, 50,1,taRightJustify); Lab_F.ALign:=alLeft;Lab_F.width:=18;
   Sl_F:=rcScroll('',Pan_F, 100,1);Sl_F.ALign:=alClient;Sl_F.Min:=0;Sl_F.Max:=300;
   rcSpaceR( Pan_F,300);
   Ed_F:=rcEdit('',Pan_F, 500,1);Ed_F.ALign:=alRight; Ed_F.Width:=40;
   rcSpaceR( Pan_F,800); 
   Lab_F.Enabled:=false; SL_F.enabled:=False; Ed_F.Enabled:=False;   
   
   Pan_E:=rcPanel ( '' ,Pan_CutsP ); Pan_E.top:=600; Pan_E.Align:=alTop;Pan_E.Height:=18; Pan_E.BorderWidth:=1;
   Lab_E:=rcLabel('','     E: ',Pan_E, 1,1,taRightJustify); Lab_E.ALign:=alLeft;Lab_E.width:=30;
   Sl_E:=rcScroll('',Pan_E, 50,1);Sl_E.ALign:=alClient;Sl_E.Min:=0;Sl_E.Max:=300;
   rcSpaceR( Pan_E,100);
   Ed_E:=rcEdit('',Pan_E, 300,1);Ed_E.ALign:=alRight; Ed_E.Width:=40;
   rcSpaceR( Pan_E,800);    

   Pan_G:=rcPanel ( '' ,Pan_CutsP ); Pan_G.top:=700; Pan_G.Align:=alTop;Pan_G.Height:=18; Pan_G.BorderWidth:=1;
   rb_G:=rcRadioButton('', '',pan_G,0,0);rb_G.Width:=12;rb_G.ALign:=alLeft;rb_G.Left:=0;
   rb_G.hint:='Ovdje je po�etak?';
   Lab_G:=rcLabel('','     G: ',Pan_G, 1,1,taRightJustify); Lab_G.ALign:=alLeft;Lab_G.width:=18;Lab_G.Left:=20;
   Sl_G:=rcScroll('',Pan_G, 50,1);Sl_G.ALign:=alClient;Sl_G.Min:=0;Sl_G.Max:=300;
   rcSpaceR( Pan_G,100);
   Ed_G:=rcEdit('',Pan_G, 300,1);Ed_G.ALign:=alRight; Ed_G.Width:=40;
   rcSpaceR( Pan_G,800); 
   Lab_G.Enabled:=false; SL_G.enabled:=False; Ed_G.Enabled:=False;    
   
   Pan_R:=rcPanel ( '' ,Pan_CutsP ); Pan_R.top:=800; Pan_R.Align:=alTop;Pan_R.Height:=18; Pan_R.BorderWidth:=1;
   Lab_R:=rcLabel('','     R: ',Pan_R, 1,1,taRightJustify); Lab_R.ALign:=alLeft;Lab_R.width:=30;
   Sl_R:=rcScroll('',Pan_R, 50,1);Sl_R.ALign:=alClient;Sl_R.Min:=0;Sl_R.Max:=300;
   rcSpaceR( Pan_R,100);
   Ed_R:=rcEdit('',Pan_R, 300,1);Ed_R.ALign:=alRight; Ed_R.Width:=40;
   rcSpaceR( Pan_R,800);  
   
  // LabSelTxt:=rcLabel( '', SelTxt,Panel_Info, 400,1,taLeftJustify)
   
   ShowD('Design rezovi zavrsen');  
 // Design end *****************************************************************  
 
    
 // CUTS - RUBNE TRAKE
   ShowD('Design rubne trake');  
   Pan_edge:=rcPanel ( '' ,Panel_Left ); 
   Pan_Edge.Top:=1000;
   Pan_Edge.ALign:=alTop;
   Pan_edge.Height:=18; 
   Pan_edge.Font.Name:='Arial';
   Pan_edge.Font.Size:=8;
   // Pan_edge.Height:=200; 
   // Pan_edge.Color:=clRed;
   
   // header button
   hss:='::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::';
   Pan_edgeH:=rcPanel ( '' ,Pan_Edge ); 
   Pan_edgeH.ALign:=alTop;
   Pan_edgeH.color:=clBtnShadow;
   Pan_edgeH.Height:=16; 
   Pan_edgeH.Caption:=hss+'  Rubne trake  '+hss;
   // Pan_CutsH.Font.Color:=clGrayText;
   Pan_edgeH.Font.Size:=8; 
   Pan_edgeH.Hint:='Klik za pokazivanje ili skrivanje';
   Pan_edgeH.ShowHint:=True; 
   CB_edge_use:=rcCheckBox('', '',Pan_edgeH,Pan_edgeH.clientWidth-15,1);
   Cb_edge_use.Width:=12; //cb_Cuse.ALign:=alClient;
   Cb_edge_use.Height:=15;
   Cb_edge_use.Hint:='Ozna�i da napravi promjenu';
//   CB_Cuse:=rcCheckBox('', 'Promijeni rezove',pan_Use,0,0);Cb_Cuse.Width:=12;cb_Cuse.ALign:=alClient;
   
   CMB_E1:=rcEdge(Pan_Edge, 100, 20);
   CMB_E2:=rcEdge(Pan_Edge, 5, 55);
   Img_PH:=rcLoadImage('',ipath+'plate_horizontal.jpg',Pan_Edge,155, 42);
   Img_PF:=rcLoadImage('',ipath+'plate_frontal.jpg'   ,Pan_Edge,160, 42);
   Img_PV:=rcLoadImage('',ipath+'plate_vertical.jpg'  ,Pan_Edge,180, 42);
   Img_PM:=rcLoadImage('',ipath+'plate_mix.jpg'  ,Pan_Edge,180, 42);
   

   
   CMB_E3:=rcEdge(Pan_Edge, 235, 55);
   CMB_E4:=rcEdge(Pan_Edge, 100, 110);
   RB_EdgeOne:=rcRadioButton('','Postavi pojedina�no',Pan_Edge, 3, 135);
   RB_EdgeOne.checked:=true;
   RB_EdgeAll:=rcRadioButton('','Postavi sve zajedno',Pan_Edge, 3, 155);
   RB_EdgeChange:=rcRadioButton('','Promijeni',Pan_Edge, 3, 175);
   RB_EdgeAll.Alignment:=taRightJustify;
   RB_EdgeOne.Alignment:=taRightJustify;
   RB_EdgeChange.Alignment:=taRightJustify;
   RB_EdgeAll.Width:=120;
   RB_EdgeOne.Width:=120;
   RB_EdgeChange.Width:=60;
   CMB_E5:=rcEdge(Pan_Edge, 85, 154);
   CMB_E5.visible:=false;
   Lab_U:=rcLabel('','u: ',Pan_Edge, 300,175,taLeftJustify);
   Lab_U.visible:=false;
   CMB_E6:=rcEdge(Pan_Edge, 320, 173);
   CMB_E6.visible:=false;
   CMB_E7:=rcEdge(Pan_Edge, 130, 173);
   CMB_E7.visible:=false;
   edges_arrange;   // poslo�i ovisno o �irini prozora
 

