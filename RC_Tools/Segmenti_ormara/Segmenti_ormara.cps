Program Segmenti_ormara;   
// Red Cat Ltd 
// v1.0 07.06.2017.

// - 
// - 
// - 

// 
var
// progress bar
   progress_panel,bar_out, bar_in:TPanel;
   progress_text:TLabel;
   start_ok:boolean;
	conf_path:string;
	Sirina_prozora:integer;

{$I ..\include\rcPrint.cps}
{$I ..\include\rcMessages.cps}

{$I ..\include\rcStrings.cps}
{$I ..\include\rcControls.cps}
{$I ..\include\rcObjects.cps}
{$I ..\include\rcEvents.cps}
{$I ..\include\rcProgress.cps}
{$I ..\include\rcCurves.cps}
{$I ..\include\rcMath.cps}
{$I ..\include\rcConfig.cps}

    

// MAIN ////////////////////////////////////////////////////////////////////////
var
   // Startni prozor
	Main:TForm;
   Panel_Left, Panel_right, Panel_Down, Panel_Title, Panel_Info, Panel_Table, Panel_Memo, Panel_MemoO :TPanel;
   Pan_var, Pan_val, Pan_for, Pan_cb, Pan_hint : array of TPanel;
   ButOK,ButPomoc,ButCancel:TButton;

	Debug, MozeZatvaranje, Izvrseno :boolean;    // za debug poruke u Journalu
	PorukaV, PorukaM :TLabel;
	
	// slikice
	SB1:TScrollBox;
	SL1,SL2,SL3,SL4,SL5,SL6,SL7,SL8:TImage;
	//Slika:TAImageList;
	slika_put:String;
	Sirina_slike, Visina_slike, Broj_kolona:Integer;

Procedure End_Program(Sender:TObject);
// Ovdje se ne smije zvati niti jedna komponenta jer su sve ve� zatvorene

var
	dummy:boolean;
Begin
	dummy:=set_value_int(conf_path,'Lijevo',Main.left);
	dummy:=set_value_int(conf_path,'Gore', Main.Top);
	dummy:=set_value_int(conf_path,'Visina',Main.Height);
	If Debug then set_value_int(conf_path,'Debug',1)
				else set_value_int(conf_path,'Debug',0);

End;	

Procedure Kopiraj_propertije(es,en:TElement);
begin
	// prebaci poziciju
	en.xPos:=es.xPos;
	en.yPos:=es.yPos;
	en.zPos:=es.zPos;
	en.xFormula:=es.xFormula;
	en.yFormula:=es.yFormula;
	en.zFormula:=es.zFormula;
	ShowN('Pozicija promijenjena');
	
	// prebaci kuteve
	en.Kut:=es.Kut;
	en.xKut:=es.xKut;
	en.zKut:=es.zKut;
	en.KXFormula:=es.KXFormula;
	en.KYFormula:=es.KYFormula;
	en.KZFormula:=es.KZFormula;
	ShowN('Kutevi promijenjeni');
	
	// prebaci dimenzije
	en.Visina:=es.Visina;
	en.Dubina:=es.Dubina;
	en.Sirina:=es.Sirina;
	en.HFormula:=es.HFormula;
	en.SFormula:=es.SFormula;
	en.DFormula:=es.DFormula;
	ShowN('Dimenzije promijenjene')
	
	
	// en.selected:=true;
	// es.selected:=false;
end;

Procedure Pomoc;
var
	PDF:TpdfPrinter;
Begin
	ShowN('Pomo�');
	PDF:=TpdfPrinter.create(1250);

   // PDF.FileName := 'https://www.youtube.com/watch?v=YRBH87-SKL4&feature=youtu.be';
	
	// PDF.ShowPdf;
	ShowN('PDF.ShowPdf - DONE');
	PDF.Free;
End;

Procedure Slika_OnClick(Sender:TObject);
var
   elm_stari, elm_novi:telement;
	Elementi_putanja, putanja:string;
	i,ii,selektiranih:integer;
Begin
	ii:=Timage(Sender).Tag;
	ShowN('Pritisnuta je Slika'+IntToStr(ii));
	
	// u�itavanje novog elementa
	Elementi_putanja:=prgfolder+find_value_str(conf_path,'Elementi_putanja');
	Putanja:=Elementi_putanja+'\'+find_value_str(conf_path,'Element'+IntToStr(ii));
	
	ShowN ('Potreban je element: "'+Putanja+'"'); 
	if not rcFileCheck(Putanja) then begin
		ShowN('Nema takvog elementa pa u�itavam defaultni!');
		Putanja:=Elementi_putanja+'\rc.e3d';
		if not rcFileCheck(Putanja) then begin
			ShowMessage('Nema defaultnog elementa "'+putanja+#13#10+'Izlazim van!');
			ShowN('Nema defaultnog elementa "'+putanja+#13#10+'Izlazim van!');
			Exit
		end;
	end;

	ShowN ('U�itan je element "'+Putanja+'"');
	
	For i:=E.ElmList.Count-1 DownTo 0 Do begin
		elm_stari:=TElement(E.ElmList.items[i]);
		// ShowN(IntToStr(i)+'. Gledam element "'+elm_stari.Naziv+'"');
		If elm_stari.Selected Then Begin
			selektiranih:=selektiranih+1;
			// ShowN('   SELEKTIRAN je element "'+elm_stari.Naziv+'"');
			// u�itavanje novog
			elm_novi:=LoadParentedElement(E,Putanja);
			ShowN('   U�itan je element "'+elm_novi.Naziv+'"');
			// kopiranje propertija
			Kopiraj_propertije(elm_stari,elm_novi);
			// promjena selekcije
	//		elm_stari.selected:=false;
	//		elm_novi.selected:=True;
			// brisanje starog elementa

			E.Elmlist.Obrisi(i);
			ShowN('   DONE! Postavljen je element "'+elm_novi.Naziv+'"');
			
		end else begin
			
			// ShowN('   Nije selektiran element "'+elm_stari.Naziv+'"');
		end;
	end;
	// if elm_novi <> nil then elm_novi.selected:=True;
	// ShowN('Selektiranih='+IntToStr(selektiranih));
	If selektiranih=0 then begin
		PorukaV.Caption:='Nije odabran niti jedan element!';
		PorukaM.Caption:='Odaberite jedan ili vi�e elemenata unutar postoje�eg elementa';
		ShowN('   Nije selektiran niti jedan element!');
		application.processmessages;
	end;
	
End;
// ------------------------------------------------	
{$I Segmenti_ormara_servis.cps}
// ------------------------------------------------

// Glavni  
var
	i, selektiranih:integer;
	elm_stari:tElement;
Begin
   // Debug:=true;         // debuger ispis on/off
	Debug:=false;
	Izvrseno:=false;
	
	// Da li postoji 'Segmenti.ini'
	conf_path:=prgfolder+'skripte\RC_Tools\Segmenti_ormara\Segmenti.ini';
	
	
	Create_Main_Window;
	
	
		

	
	
	// Pozicioniranje glavnog prozora
	Sirina_Prozora:=485; Main.Width:=Sirina_Prozora; 
	ShowN('�irina prozora je pode�ena na '+IntToStr(Main.Width));
	Main.Left:=find_value_int(conf_path,'Lijevo');
	ShowN('Pozicija X pode�ena na '+IntToStr(Main.Left));
	Main.Top:=find_value_int(conf_path,'Gore');
	ShowN('Pozicija Y je pode�ena na '+IntToStr(Main.Top));
	Main.Height:=find_value_int(conf_path,'Visina');
	ShowN('Visina prozora je pode�ena na '+IntToStr(Main.Height));
	
	If find_value_int(conf_path,'Debug')=1 then cbDebug.checked:=True 
														else cbDebug.checked:=False;
	Main.onDeactivate:=@End_program;
  //ButPomoc.OnClick:=@ButPomoc_OnClick;
	// Main.OnResize:=@Main_OnResize;
	
   cbDebug_OnClick(cbDebug);  // poziva "cbDebug_OnClick" bez da je i�ta kliknuto
	
   // Main.ShowModal;   // ekskluzivno radi
	Main.Show;  // KAd je ovo, onda ne radi tipka Cancel

	while Main.visible do begin
      application.processmessages;
		selektiranih:=0;
		For i:=E.ElmList.Count-1 DownTo 0 Do begin
				elm_stari:=TElement(E.ElmList.items[i]);
				If elm_stari.Selected Then selektiranih:=selektiranih+1;
		end;
		If selektiranih=0 then begin
			PorukaV.Caption:='Nije odabran niti jedan segment!';
			PorukaM.Caption:='Odaberite jedan ili vi�e segmenata u postoje�em elementu';
		end else begin
			if selektiranih=1 then PorukaV.Caption:='Odabran je '+IntToStr(selektiranih)+' segment.';
			if (selektiranih>1) and (selektiranih<5) then PorukaV.Caption:='Odabrana su '+IntToStr(selektiranih)+' segmenta.';
			if (selektiranih>4) and (selektiranih<21) then PorukaV.Caption:='Odabrano je '+IntToStr(selektiranih)+' segmenata.';
			PorukaM.Caption:='Odaberite s �ime ih treba zamijeniti.';
		end;
   end;
   
	
   Main.free; 
	// Main.close; 

   
End.