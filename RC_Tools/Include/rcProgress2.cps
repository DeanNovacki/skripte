
// *********************************************************************************
// Prokazuje prozor s tekstom i dva broja
// ako su dva broja jednaka, prozor se automatski gasi
// 
// U�itavam listu
// 23/2500
//



procedure rcProgress(title, mess:string; n1, n2:integer);
var
	// deklarirati u glavnoj skripti:
	// MessForm: TForm;
	// messLab1, messLab2 : tLabel;
	// box1, box2 : tShape;
	dummy:boolean;


begin
	// Print('rcProgress START');	
	If messform=nil then begin
		Print('kreiram');
		MessForm := TForm.Create(nil);
		// MessForm.Parent := MessForm;
		MessForm.Caption:=Title;
		
		MessForm.BorderStyle := bsDialog;
		
		MessForm.FormStyle:=fsStayOnTop;
		// MessForm.FormStyle:=fsMDIChild;  // ???
		// MessForm.FormStyle:=fsMDIForm;  // ???
		// MessForm.FormStyle:=fsNormal;  // ???
		// MessForm.FormStyle:=fsSplash;  // NE RADI
		// MessForm.FormStyle:=fsSystemStayOnTop;  // NE RADI
		
		MessForm.Position:=poScreenCenter;
		MessForm.Width:=300;
		MessForm.Height:=110;
		
		messLab1:=TLabel.create(MessForm);
		messLab1.Parent:=MessForm;
		messLab1.Left:=15;
		messLab1.Top:=10;
	
		messLab2:=TLabel.create(MessForm);
		messLab2.Parent:=MessForm;
		messLab2.Left:=15;
		messLab2.Top:=30;
		
		box1:=tShape.create(MessForm);
		box1.Parent:=MessForm;
		Box1.Left:=10;
		Box1.Top:=50;
		Box1.Width:=MessForm.ClientWidth-20;
		Box1.Height:=20;
		Box1.Brush.Color:=clSilver;
		
		box2:=tShape.create(MessForm);
		box2.Parent:=MessForm;
		Box2.Left:=10;
		Box2.Top:=50;
		Box2.Width:=MessForm.ClientWidth-20-100;
		Box2.Height:=20;
		Box2.Brush.Color:=clRed;
	
		MessForm.Show;
	
	End else begin
		// Print('Bila kreirana');
	
	End;
	
	// MessForm.FormStyle:=fsMDICHILD;
	MessForm.SetFocus;
	
	messLab1.Caption:=mess;
	messLab2.Caption:=IntToStr(N1)+'/'+IntToStr(N2);
	Box2.Width:=round((n1/n2)*Box1.width);
	
	application.processmessages;
	
	
	if n1=n2 then Begin 
		MessForm.Close;
		MessForm.Free;
		MessForm:=nil;
		// MessForm.Destroy;
		// MessForm.Terminate;
		
		// print('Progress uklonjen')	;
	end;
   
	// Print('rcProgress END');	
end;