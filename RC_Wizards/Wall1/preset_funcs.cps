{  
   Procedure Save_Preset(preset_name:string);
   Procedure Save_new_preset_show(sender:TObject);
   Procedure Save_new_preset_hide(sender:TObject);
   Procedure Save_new_preset(sender:TObject);
   Procedure Update_preset_show(sender:TObject);
   Procedure Update_preset_hide(sender:TObject);
   Procedure Update_preset(sender:TObject);
   Procedure Load_Preset(preset_name:string);
   Procedure Apply_preset;
   Procedure Presets_Click(sender:Tobject);
   procedure load_preset_list;
   Procedure Delete_preset_show(sender:TObject);
   Procedure Delete_preset_hide(sender:TObject);
   Procedure Delete_preset(sender:TObject);
}

Procedure Save_Preset(preset_name:string);

begin
   message('Procedure Save_Preset(preset_name:string);') 
   progress_change(0,'Saving preset...'); 
   if preset_name='Last used' then preset_name:=script_path+'Presets\default\'+preset_name+'.preset'
                             else preset_name:=script_path+'Presets\user\'+preset_name+'.preset';
   // end if
   
      progress_change(0,'Saving preset...'); 
      set_value_str(preset_name, 'el_name', el_name) 
      set_value_int(preset_name,'el_type', el_type)
      set_value_str(preset_name,'el_date',el_date) ;
      set_value_int(preset_name,'el_shape',el_shape) ;
      
      progress_change(10,'Saving preset...'); 
      set_value_int(preset_name,'el_width',el_width) ;
      set_value_int(preset_name,'el_width_min',el_width_min) ;
      set_value_int(preset_name,'el_width_max',el_width_max) ;
      set_value_int(preset_name,'el_height', el_height) ;
      set_value_int(preset_name,'el_height_min',el_height_min) ;
      set_value_int(preset_name,'el_height_max',el_height_max) ;
      set_value_int(preset_name,'el_depth',el_depth) ;
      set_value_int(preset_name,'el_depth_min',el_depth_min)  ;
      set_value_int(preset_name,'el_depth_max',el_depth_max)  ;
      
      set_value_int(preset_name,'el_xp',el_xp)  ;
      set_value_int(preset_name,'el_yp',el_yp)  ;
      set_value_int(preset_name,'el_zp',el_zp)  ;

      progress_change(20,'Saving preset...'); 
      set_value_int(preset_name,'top_type',top_type) ;
      set_value_int(preset_name,'top_thick',top_thick) ;
      set_value_int(preset_name,'top_thick_min',top_thick_min) ;
      set_value_int(preset_name,'top_thick_max',top_thick_max) ;
      set_value_int(preset_name,'top_dist_front',top_dist_front) ;
      set_value_int(preset_name,'top_dist_front_min',top_dist_front_min) ;      
      set_value_int(preset_name,'top_dist_front_max',top_dist_front_max) ;   
      
      set_value_int(preset_name,'top_dist_back_fix',top_dist_back_fix) ;
      set_value_int(preset_name,'top_dist_back_min',top_dist_back_min) ;
      set_value_int(preset_name,'top_dist_back_max',top_dist_back_max) ;

      progress_change(30,'Saving preset...'); 
      set_value_int(preset_name,'top_width_front',top_width_front) ;
      set_value_int(preset_name,'top_width_front_min',top_width_front_min) ;
      set_value_int(preset_name,'top_width_front_max',top_width_front_max) ;
      set_value_int(preset_name,'top_width_back',top_width_back) ;                                                  
      set_value_int(preset_name,'top_width_back_min',top_width_back_min) ;
      set_value_int(preset_name,'top_width_back_max',top_width_back_max) ;

      
      set_value_int(preset_name,'bottom_type',bottom_type) ;
      set_value_int(preset_name,'bottom_thick',bottom_thick) ;
      set_value_int(preset_name,'bottom_thick_min',bottom_thick_min) ;
      set_value_int(preset_name,'bottom_thick_max',bottom_thick_max) ;

      progress_change(50,'Saving preset...'); 
      set_value_int(preset_name,'bottom_dist_front',bottom_dist_front) ;
      set_value_int(preset_name,'bottom_dist_front_min',bottom_dist_front_min) ;
      set_value_int(preset_name,'bottom_dist_front_max',bottom_dist_front_max) ;
      set_value_int(preset_name,'bottom_dist_back',bottom_dist_back) ;
      set_value_int(preset_name,'bottom_dist_back_min',bottom_dist_back_min) ;
      set_value_int(preset_name,'bottom_dist_back_max',bottom_dist_back_max) ;

      progress_change(60,'Saving preset...'); 
      set_value_int(preset_name,'back_on',back_on) ;
      set_value_int(preset_name,'back_dist',back_dist) ;
      set_value_int(preset_name,'back_dist_min',back_dist_min) ;
      set_value_int(preset_name,'back_dist_max',back_dist_max) ;   
      set_value_int(preset_name,'back_thick',back_thick) ;
      set_value_int(preset_name,'back_thick_min',back_thick_min) ;
      set_value_int(preset_name,'back_thick_max',back_thick_max) ;
      set_value_int(preset_name,'back_ins',back_ins) ;
      set_value_int(preset_name,'back_ins_min',back_ins_min) ;
      set_value_int(preset_name,'back_ins_max',back_ins_max) ;
      set_value_int(preset_name,'back_top_width',back_top_width) ;
      set_value_int(preset_name,'back_top_width_min',back_top_width_min) ;
      set_value_int(preset_name,'back_top_width_max',back_top_width_max) ;
      set_value_int(preset_name,'back_down_width',back_down_width) ;
      set_value_int(preset_name,'back_down_width_min',back_down_width_min) ;
      set_value_int(preset_name,'back_down_width_max',back_down_width_max) ;

      progress_change(70,'Saving preset...'); 
      set_value_int(preset_name,'side_thick',side_thick) ;
      set_value_int(preset_name,'side_thick_min',side_thick_min) ;
      set_value_int(preset_name,'side_thick_max',side_thick_max) ;
      
      progress_change(80,'Saving preset...'); 
      set_value_int(preset_name,'shelf_thick',shelf_thick) ;
      set_value_int(preset_name,'shelf_thick_min',shelf_thick_min) ;
      set_value_int(preset_name,'shelf_thick_max',shelf_thick_max) ;   
      set_value_int(preset_name,'shelf_qnt',shelf_qnt) ;
      set_value_int(preset_name,'shelf_mov',shelf_mov) ;
      set_value_int(preset_name,'shelf_mov_min',shelf_mov_min) ;
      set_value_int(preset_name,'shelf_mov_max',shelf_mov_max) ;
      set_value_int(preset_name,'shelf_front_dist',shelf_front_dist) ;
      set_value_int(preset_name,'shelf_front_dist_min',shelf_front_dist_min) ;
      set_value_int(preset_name,'shelf_front_dist_max',shelf_front_dist_max) ;

      progress_change(90,'Saving preset...'); 
      set_value_int(preset_name,'leg_height',leg_height) ;
      set_value_int(preset_name,'leg_dist_front',leg_dist_front) ;
      set_value_int(preset_name,'leg_dist_front_min',leg_dist_front_min) ;
      set_value_int(preset_name,'leg_dist_front_max',leg_dist_front_max) ;
      set_value_int(preset_name,'leg_dist_side',leg_dist_side) ;
      set_value_int(preset_name,'leg_dist_side_min',leg_dist_side_min) ;      
      set_value_int(preset_name,'leg_dist_side_max',leg_dist_side_max) ;
      set_value_int(preset_name,'leg_dist_back',leg_dist_back) ;
      set_value_int(preset_name,'leg_dist_back_min',leg_dist_back_min) ;
      set_value_int(preset_name,'leg_dist_back_max',leg_dist_back_max) ;

      set_value_int(preset_name,'door_qnt',door_qnt) ;
      set_value_str(preset_name,'door_orient',door_orient);
      set_value_str(preset_name,'left_offset',left_offset);
      set_value_str(preset_name,'right_offset',right_offset);
      set_value_str(preset_name,'up_offset',up_offset);
      set_value_str(preset_name,'down_offset',down_offset);      
      
      {
      info[0]                 :=save_value_str(preset_name,'info0');
      info[1]                 :=save_value_str(preset_name,'info1');
      info[2]                 :=save_value_str(preset_name,'info2');
      info[3]                 :=save_value_str(preset_name,'info3');
      info[4]                 :=save_value_str(preset_name,'info4');
      info[5]                 :=save_value_str(preset_name,'info5');
      info[6]                 :=save_value_str(preset_name,'info6');
      info[7]                 :=save_value_str(preset_name,'info7');
      info[8]                 :=save_value_str(preset_name,'info8');
      info[9]                 :=save_value_str(preset_name,'info9');
      }

     progress_change(100,'Done'); 
end;


Procedure Save_new_preset_show(sender:TObject);
Begin
  Save_panel.visible:=true;
  Save_Field.Text:='new_preset';
  Save_Field.SelectAll;
  // Save_Field.AutoSelect := FALSE;
  Save_Field.SelStart := length(Save_Field.Text);
  Save_Field.SetFocus();
  
end;

Procedure Save_new_preset_hide(sender:TObject);
Begin
  Save_panel.visible:=false;
  Save_Field.font.color:=clWindowText;
  warning_off;
  lock_main_buttons;
  // turn_on_ok;
  
  
end;


Procedure Save_new_preset(sender:TObject);
var
   new_preset,save_name, str:string;
   i:integer;
   exist:boolean;
   temp_list:TStringList;
Begin
    // read new name for preset
    new_preset:=Save_Field.text;
    
    // check if name exist in preset list
    if Preset_Listbox.Items.IndexOf(new_preset) > -1 then exist := true
                                                     else exist := false;
 
    if exist then  warning_name_exist
              else begin
                   warning_off;
                   
                   // create empty preset file on disc    
                   temp_list:= TStringlist.create;
                   try

                        temp_list.Add('User preset: '+new_preset);                               // first row in new file
                        save_name:=script_path+'Presets\user\'+new_preset+'.preset';
                        // if not (FileExists(save_name)) then temp_list.SaveToFile(save_name);     // create if not exist
                        temp_list.SaveToFile(save_name);     // save file
                     except
                        Showmessage ( 'Cannot create file "'+save_name+'"');
                     finally
                        temp_list.Free;
                   end; // try
                 
                   // save preset to disc
                   Save_Preset(new_preset);

                   // add new preset to preset list
//                   Preset_ListBox.Items.Add(new_preset);       Strange :-)  Work without this command
                 
                   // add new preset to ini file
                   for i:=1 to 16 do begin
                      str:='up'+IntToStr(i)     // up1, up2, up3...
                      if find_value_str(ininame,str)=' ' then       // up(i) not found
                         begin
                             Preset_ListBox.Items.Add(new_preset);                  // ad new preset to list of presets on screen
                             if not set_value_str(ininame,str, new_preset)then   // ad new preset to ini file
                                 Showmessage ( 'Cannot write new preset to ini file'); 
                             break;
                      end; //  if find_value_str
                   end; // for i
                   // make new preset as actual
                   Preset_ListBox.ItemIndex:=Preset_Listbox.Items.IndexOf(new_preset);
                   
                   // mark new preset as user                   
                   preset_index[Preset_Listbox.Items.IndexOf(new_preset)]:='U';
                   
                   // hide save panel
                   Save_panel.visible:=false;
                   
    end;  // if exist;
    lock_main_buttons; 
    if reach_preset_limit then Button_save.enabled:=false;

End;


Procedure Update_preset_show(sender:TObject);
Begin
  message('Procedure Update_preset_show(sender:TObject);');
  Update_panel.visible:=true;
  Update_field.text:=Preset_ListBox.Items.Strings[Preset_ListBox.ItemIndex];
  
end;

Procedure Update_preset_hide(sender:TObject);
Begin
  Update_panel.visible:=false;
  
end;





Procedure Update_preset(sender:TObject);
// save adjustments from form to active preset 
var
  sp:string;                            // selected item text
  si:Integer;                           // selected item index
begin
  message('Procedure Update_preset(sender:TObject);');
  Update_OK.enabled:=false;
  Update_Cancel.enabled:=false;
  Update_panel.cursor:=crHourglass;
  sp:=Preset_ListBox.Items.Strings[Preset_ListBox.ItemIndex];    // current preset
  si:=Preset_ListBox.ItemIndex;
  //Showmessage ( 'Update preset');
  Save_Preset(sp);
  Update_panel.visible:=false;
  
  Update_OK.enabled:=true;
  Update_Cancel.enabled:=true;  
  Update_panel.Cursor := crDefault;
 

End;    // update preset




Procedure Load_Preset(preset_name:string);
// Load preset from disc (after click on it in preset list)

begin
  
  preset_name:=script_path+'Presets\'+preset_name+'.preset';
   try
      progress_change(0,'Loading preset...'); 
      el_name                 :=find_value_str(preset_name,'el_name');
      el_type                 :=find_value_int(preset_name,'el_type');
      el_date                 :=find_value_str(preset_name,'el_date');
      el_shape                :=find_value_int(preset_name,'el_shape');

      el_width                :=find_value_int(preset_name,'el_width');
      el_width_min            :=find_value_int(preset_name,'el_width_min');
      el_width_max            :=find_value_int(preset_name,'el_width_max');

      progress_change(10,'Loading preset...'); 
      el_height               :=find_value_int(preset_name,'el_height');
      el_height_min           :=find_value_int(preset_name,'el_height_min');
      el_height_max           :=find_value_int(preset_name,'el_height_max');

      el_depth                :=find_value_int(preset_name,'el_depth');
      el_depth_min            :=find_value_int(preset_name,'el_depth_min');
      el_depth_max            :=find_value_int(preset_name,'el_depth_max');
      
      el_xp                   :=find_value_int(preset_name,'el_xp');
      el_yp                   :=find_value_int(preset_name,'el_yp');
      el_zp                   :=find_value_int(preset_name,'el_zp');

      progress_change(20,'Loading preset...'); 
      top_type                :=find_value_int(preset_name,'top_type');
      top_thick               :=find_value_int(preset_name,'top_thick');
      top_thick_min           :=find_value_int(preset_name,'top_thick_min');
      top_thick_max           :=find_value_int(preset_name,'top_thick_max');
      top_dist_front          :=find_value_int(preset_name,'top_dist_front');
      top_dist_front_min      :=find_value_int(preset_name,'top_dist_front_min');      
      top_dist_front_max      :=find_value_int(preset_name,'top_dist_front_max');      
      
      progress_change(30,'Loading preset...');
      top_dist_back_fix       :=find_value_int(preset_name,'top_dist_back_fix');
      top_dist_back_min       :=find_value_int(preset_name,'top_dist_back_min');
      top_dist_back_max       :=find_value_int(preset_name,'top_dist_back_max');

      top_width_front         :=find_value_int(preset_name,'top_width_front');
      top_width_front_min     :=find_value_int(preset_name,'top_width_front_min');
      top_width_front_max     :=find_value_int(preset_name,'top_width_front_max');
      top_width_back          :=find_value_int(preset_name,'top_width_back');                                                  
      top_width_back_min      :=find_value_int(preset_name,'top_width_back_min');
      top_width_back_max      :=find_value_int(preset_name,'top_width_back_max');

      progress_change(40,'Loading preset...');
      bottom_type             :=find_value_int(preset_name,'bottom_type');
      bottom_thick            :=find_value_int(preset_name,'bottom_thick');
      bottom_thick_min        :=find_value_int(preset_name,'bottom_thick_min');
      bottom_thick_max        :=find_value_int(preset_name,'bottom_thick_max');

      progress_change(50,'Loading preset...');
      bottom_dist_front       :=find_value_int(preset_name,'bottom_dist_front');
      bottom_dist_front_min   :=find_value_int(preset_name,'bottom_dist_front_min');
      bottom_dist_front_max   :=find_value_int(preset_name,'bottom_dist_front_max');
      bottom_dist_back        :=find_value_int(preset_name,'bottom_dist_back');
      bottom_dist_back_min    :=find_value_int(preset_name,'bottom_dist_back_min');
      bottom_dist_back_max    :=find_value_int(preset_name,'bottom_dist_back_max');

      progress_change(60,'Loading preset...');
      back_on                 :=find_value_int(preset_name,'back_on');
      back_dist               :=find_value_int(preset_name,'back_dist');
      back_dist_min           :=find_value_int(preset_name,'back_dist_min');
      back_dist_max           :=find_value_int(preset_name,'back_dist_max');      
      back_thick              :=find_value_int(preset_name,'back_thick');
      back_thick_min          :=find_value_int(preset_name,'back_thick_min');
      back_thick_max          :=find_value_int(preset_name,'back_thick_max');
      back_ins                :=find_value_int(preset_name,'back_ins');
      back_ins_min            :=find_value_int(preset_name,'back_ins_min');
      back_ins_max            :=find_value_int(preset_name,'back_ins_max');
      back_top_width          :=find_value_int(preset_name,'back_top_width');
      back_top_width_min      :=find_value_int(preset_name,'back_top_width_min');
      back_top_width_max      :=find_value_int(preset_name,'back_top_width_max');
      back_down_width         :=find_value_int(preset_name,'back_down_width');
      back_down_width_min     :=find_value_int(preset_name,'back_down_width_min');
      back_down_width_max     :=find_value_int(preset_name,'back_down_width_max');

      progress_change(70,'Loading preset...');
      side_thick              :=find_value_int(preset_name,'side_thick');
      side_thick_min          :=find_value_int(preset_name,'side_thick_min');
      side_thick_max          :=find_value_int(preset_name,'side_thick_max');
      
      progress_change(80,'Loading preset...');
      shelf_thick             :=find_value_int(preset_name,'shelf_thick');
      shelf_thick_min         :=find_value_int(preset_name,'shelf_thick_min');
      shelf_thick_max         :=find_value_int(preset_name,'shelf_thick_max');      
      shelf_qnt               :=find_value_int(preset_name,'shelf_qnt');
      shelf_mov               :=find_value_int(preset_name,'shelf_mov');
      shelf_mov_min           :=find_value_int(preset_name,'shelf_mov_min');
      shelf_mov_max           :=find_value_int(preset_name,'shelf_mov_max');
      shelf_front_dist        :=find_value_int(preset_name,'shelf_front_dist');                                      
      shelf_front_dist_min    :=find_value_int(preset_name,'shelf_front_dist_min');
      shelf_front_dist_max    :=find_value_int(preset_name,'shelf_front_dist_max');

      progress_change(90,'Loading preset...');
      leg_height              :=find_value_int(preset_name,'leg_height');
      leg_dist_front          :=find_value_int(preset_name,'leg_dist_front');
      leg_dist_front_min      :=find_value_int(preset_name,'leg_dist_front_min');      
      leg_dist_front_max      :=find_value_int(preset_name,'leg_dist_front_max');
      leg_dist_side           :=find_value_int(preset_name,'leg_dist_side');
      leg_dist_side_min       :=find_value_int(preset_name,'leg_dist_side_min');      
      leg_dist_side_max       :=find_value_int(preset_name,'leg_dist_side_max');
      leg_dist_back           :=find_value_int(preset_name,'leg_dist_back');
      leg_dist_back_min       :=find_value_int(preset_name,'leg_dist_back_min');
      leg_dist_back_max       :=find_value_int(preset_name,'leg_dist_back_max');

      door_qnt               :=find_value_int(preset_name,'door_qnt');
      door_orient             :=find_value_str(preset_name,'door_orient');
      
      left_offset             :=find_value_str(preset_name,'left_offset');
      // Showmessage('LOAD PRESET: left_offset='+left_offset);
      right_offset            :=find_value_str(preset_name,'right_offset');
      up_offset               :=find_value_str(preset_name,'up_offset');
      down_offset             :=find_value_str(preset_name,'down_offset');
      
      {
      info[0]                 :=find_value_str(preset_name,'info0');
      info[1]                 :=find_value_str(preset_name,'info1');
      info[2]                 :=find_value_str(preset_name,'info2');
      info[3]                 :=find_value_str(preset_name,'info3');
      info[4]                 :=find_value_str(preset_name,'info4');
      info[5]                 :=find_value_str(preset_name,'info5');
      info[6]                 :=find_value_str(preset_name,'info6');
      info[7]                 :=find_value_str(preset_name,'info7');
      info[8]                 :=find_value_str(preset_name,'info8');
      info[9]                 :=find_value_str(preset_name,'info9');
      }
    except
     showmessage('Error reading preset:'+#13+#10+'"'+preset_name+'"');
     // close;
   end;
   lock_main_buttons;
   progress_change(100,'Loading preset...');
end;


Procedure Apply_preset;
// fill controls on form according preset
var
   i,LL, AP_count:Integer;
   findV,resultV:string;   
Begin
   // message('Change adjusments start!');

   // change preset info
   {
   PresetM.lines.clear;
   PresetM.ReadOnly:=false;
   for i:=0 to 9 do begin    // 10 lines of text
           // PresetM.lines.add(info[i]);
           PresetM.SelStart := 0;         // positioning cursor to start of memo
           PresetM.SelLength:= 0;
   end; //for i
   }
   // change image for type of element
   
   RC_Radio_Image_Change( AP[1],          // destination
                          101,            // start tag
                          106,            // end tag
                          100+el_shape);  // tag of selected image                                      
                          
   // change memo for type of element       
   // message('Change Info');                    
   AP1_memo.lines.clear;
   AP1_memo.readonly:=false;
   for i:=1 to 6 do begin    // six lines of text
           findV:='t10'+IntToStr(el_shape)+'_'+IntToStr(i);  // name of variable
           // message('Find: '+FindV+'');  
           resultV:=find_value_str(lang_file,findV);           // value of variable
           // message('Found: '+ResultV+'');
           Delete(ResultV,1,1);                             // delete sign ' on start of string
           LL:=Length(resultV);
           Delete(ResultV,LL-1,3);   // delete '; on the end of the string
           // message('Cuted: '+ResultV+'');
         
           AP1_memo.lines.add(ResultV);
           AP1_Memo.SelStart := 0;         // positioning cursor to start of memo
           AP1_Memo.SelLength:= 0;
   end; //for i    
   AP1_memo.readonly:=true;
   // FILL edit fields

   // assign element name
   edit_name.text:=el_name;
   
   // looking for ALL edit fields with right tags
   for AP_count:=1 to 9 do begin                                 // all panels
      for i:=0 to AP[AP_count].controlcount-1 do begin           // all objects
         if (AP[AP_count].controls[i] is TEdit) then             // if edit fields
            case AP[AP_count].controls[i].tag of 
              201 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(el_width);  
              202 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(el_height);
              203 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(el_depth);
              204 : TEdit(AP[AP_count].controls[i]).text:=el_name;
              
              304 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(top_thick);
              //305 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(top_width_back);
              //306 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(top_width_front);
              305 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(top_dist_back_fix);
              306 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(top_dist_front);
              
              404 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(bottom_thick);
              405 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(bottom_dist_front);
              406 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(bottom_dist_back);
              
              503 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(back_dist);
              504 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(back_thick);
              505 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(back_ins);
              506 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(back_top_width);
              507 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(back_down_width);
              
              601 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(side_thick);
              
              705 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(shelf_thick);              
              706 : TEdit(AP[AP_count].controls[i]).text:=IntToStr(shelf_front_dist);
              
              
              
                                                                                               // <<====================== NADOPUNITI
                                                                                               // <<====================== NADOPUNITI
                                                                                               // <<====================== NADOPUNITI                                                                                                             
              
            end;   // case 
         // end if 
      end;  // for i
   end; //  AP_count


   // Showmessage('W1');

   // ASSIGN values for Radio_Images

   // Assign image for top type
   RC_Radio_Image_Change( AP[3],          // destination
                          301,            // start tag
                          303,            // end tag
                          300+top_type);  // tag of selected image 
                          
   // Assign image for bottom of element
   RC_Radio_Image_Change( AP[4],          // destination
                          401,            // start tag
                          403,            // end tag
                          400+bottom_type);  // tag of selected image 
                           
   // Assign image for back existance
   RC_Radio_Image_Change( AP[5],          // destination
                          501,            // start tag
                          502,            // end tag
                          500+back_on);  // tag of selected image 

// Showmessage('W2');
   // Assign image for shelf quantity
   RC_Radio_Image_Change( AP[7],          // destination
                          701,            // start tag
                          703,            // end tag
                          700+shelf_qnt);  // tag of selected image 
 // Showmessage('W3');
   // Assign image for shelf_move
   RC_Radio_Image_Change( AP[7],          // destination
                          707,            // start tag
                          708,            // end tag
                          706+shelf_mov);  // tag of selected image 
 // Showmessage('W4');
 
 //  Assign image for Legs
  RC_Radio_Image_Change( AP[8],          // destination
                          801,            // start tag
                          802,            // end tag
                          800+leg_height);  // tag of selected image
                          
 //  Assign image for doors 
  RC_Radio_Image_Change( AP[9],          // destination
                          901,            // start tag
                          904,            // end tag
                          900+door_qnt);  // tag of selected image
 
   //  Change other adjustments ******************************************************************************************************
                                                                                            
                                                                                               // <<====================== NADOPUNITI
                                                                                               // <<====================== NADOPUNITI
                                                                                               // <<====================== NADOPUNITI  
                                                                                               
                                                                                               
                                                                                                                                                                                                          
   // turn off unusable properties for some options  


   // enable all before disabling
   Edit_enabled(305,true);
   Edit_enabled(306,true);
   Edit_enabled(307,true);
   Edit_enabled(406,true);
   Edit_enabled(503,true);
   Edit_enabled(504,true);
   Edit_enabled(505,true);
   Edit_enabled(506,true);
   Edit_enabled(507,true);
   Edit_enabled(705,true);
   Edit_enabled(706,true);
   Edit_enabled(707,true);
   Edit_enabled(708,true);




{
   // double top bars
   if (el_shape=2) or (el_shape=5) or (el_shape=6) then begin 
                        Edit_enabled(305,false);
                        Edit_enabled(306,false);     
   end;

   // back top width
   if (el_shape=3) then begin 
                        Edit_enabled(305,false);     
   end;
}
   // distance from top to end of element            
   if (top_type=1) or (top_type=2) then begin 
                        Edit_enabled(305,false);     
   end;
{
   // double back bar
   if (el_shape=1) or (el_shape=2) or (el_shape=5) or (el_shape=6) then begin 
                        Edit_enabled(506,false);
                        Edit_enabled(507,false);     
   end;
}
   // distance from back to end of element            
   if (bottom_type=1) or (bottom_type=2) then begin 
                        Edit_enabled(406,false);     
   end;

   // back_on
   if (back_on=2)  then begin 
                        Edit_enabled(503,false);
                        Edit_enabled(504,false);
                        Edit_enabled(505,false);
                        Edit_enabled(506,false);
                        Edit_enabled(507,false);     
   end;                                                                                           

   // back_on
   if (shelf_qnt=1) then begin 
                        Edit_enabled(705,false);
                        Edit_enabled(706,false);
                        Edit_enabled(707,false);
                        Edit_enabled(708,false);     
   end;                                                                                           

                                                                                               // <<====================== NADOPUNITI
                                                                                               // <<====================== NADOPUNITI
                                                                                               // <<====================== NADOPUNITI 


   // message('Apply_preset finished!');
   lock_main_buttons;
   
End;   // Apply_preset



Procedure Presets_Click(sender:Tobject);
// v1.07
var
  sp:string;                            // selected item text
  si:Integer;                           // selected item index
 
begin
  sp:=Preset_ListBox.Items.Strings[Preset_ListBox.ItemIndex];
  si:=Preset_ListBox.ItemIndex;
  // message('last preset is no: '+IntToStr(last_preset));
  if preset_index[si]='L' then begin Preset_ListBox.ItemIndex:=last_preset;
                                     // message('bad click!');   
                                     application.processmessages;

                          end else last_preset:=si;
  // message('Item no: '+IntToStr(si)+' "'+sp+'"');  
  if preset_index[si]='D' then load_preset('default\'+sp);
  if preset_index[si]='U' then load_preset('user\'+sp);


  if (preset_index[Preset_ListBox.ItemIndex]='U')                      
        then begin
           Button_delete.enabled:=true; 
         end else begin
           Button_delete.enabled:=false; 
  end;
  
  // if reach_preset_limit then Button_save.enabled:=false
  //                       else Button_save.enabled:=true;

  Apply_preset;
 
end;

procedure load_preset_list;
// v1.07
var
   i,pcn:integer;
   str,outstr:string;
   list:TStringList;
   // searchResult : TSearchRec;
begin
  // load default presets
  // load names of presets from ini file

  message('procedure load_preset_list;');

 
   list:=TStringList.Create;

   try list.LoadFromFile(IniName)
   except ShowMessage( 'Cannot find file"'+ IniName+'"!');
   end; 
  pcn:=0;      // presets counter;

  for i:=1 to 16 do begin
  
      str:='dp'+IntToStr(i)      // dp1, dp2, dp3...

      outstr:=list.Values[str];                    // ininame=.../base01.ini
      if (Length(Outstr)>0) and not (outstr=' ') then begin
         Preset_ListBox.Items.Add(outstr);
         // message('String "'+Outstr+'" found and added to default presets');
         preset_index[pcn]:='D';   pcn:=pcn+1; 
      end;
  end; // for
  
  Preset_ListBox.Items.Add('-------------------------------------------');
   preset_index[pcn]:='L';  pcn:=pcn+1;

  // load user presets
  i:=0;
  outstr:=' ';
  for i:=1 to 16 do begin
      str:='up'+IntToStr(i)     // up1, up2, up3...
      outstr:=list.Values[str];                    // ininame=.../base01.ini
      if (Length(Outstr)>0) and not (outstr=' ') then begin
         Preset_ListBox.Items.Add(outstr);
         // message('String "'+Outstr+'" found and added to user presets');
         preset_index[pcn]:='U';    pcn:=pcn+1;      
      end;
  end;  // for i

  if reach_preset_limit then Button_save.enabled:=false;

  // marking items in Preset_indexing table for marking items in Presets_listbox 
  // maximum of 33 items can be marked, included separation line

  Preset_ListBox.Selected[1]:= True;            // select second item in Presets (Last used)
  list.free;



  message('LOAD PRESET FINISHED!');

end; 


Procedure Delete_preset_show(sender:TObject);
var
   working_preset:string;
Begin
    // which preset is selected
    working_preset:=Preset_ListBox.Items.Strings[Preset_ListBox.ItemIndex];
    Delete_Field.text:=working_preset;
    
    // warning
    Delete_Panel.visible:=True;
end;    
    
Procedure Delete_preset_hide(sender:TObject);
Begin
    Delete_Panel.visible:=False;
end;    
    
Procedure Delete_preset(sender:TObject);
var
   working_preset, file_name, str, ini_preset_name:string;
   working_index, i, ind:Integer;
   sl:TStringList;
Begin
    message('Procedure Delete_preset(sender:TObject);');
    Update_panel.cursor:=crHourglass;
    working_preset:=Preset_ListBox.Items.Strings[Preset_ListBox.ItemIndex];  // which preset is selected
    working_index:=Preset_ListBox.ItemIndex;                                 // index
    
    // find preset in ini file
    for i:=1 to 16 do begin                                    // looking for name in ini file (upX) and user index
        str:='up'+IntToStr(i)     // up1, up2, up3...
        if find_value_str(ininame,str)=working_preset then begin
                             ini_preset_name:=str+'='+working_preset;
                             message('Preset string "'+ini_preset_name+'" found in "'+ininame+'"');
                             break;
        end; //  if find_value_str
    end; // for i
                   
    // delete preset name from ini file
    sl:=TStringList.Create;
    sl.LoadFromFile(ininame);
    ind:=sl.IndexOf(ini_preset_name); 
    // message('ind='+IntToStr(ind));
    if ind=-1 then message('cannot find ' +ini_preset_name+' in '+ininame)
              else begin 
                         sl.Delete(Ind);
                         message('preset "'+ini_preset_name+' deleted from ini file!')
    end; // if ind=-1
    sl.SaveToFile(ininame);
    sl.free;

    
    // delete preset file from disk not work
    file_name:=script_path+'\templates\user\'+working_preset+'.preset';
    message('Delete "'+file_name+'" not work :-("');
    {
    if DeleteFile(file_name) then message('file "'+File_name+' deleted!')
                               else ShowMessage(fileName+' not deleted');
    }
    
    // remove from preset list
    // search for index
    
    message('Selecting previous preset (with index: '+IntToStr(working_index-1)+') from Preset_ListBox');
    if preset_index[working_index-1]<>'L' then preset_listbox.Selected[working_index-1]:= True
                                          else preset_listbox.Selected[working_index-2]:= True;
    
    message('Starting to delete preset "'+working_preset+'" Preset_ListBox');
    preset_ListBox.Items.Delete(preset_listbox.Items.IndexOf(working_preset));
    message('Deleting preset "'+working_preset+'" from Preset_ListBox finished');
    

    
    // remove preset_index for this preset 
    {
    for i:=working_index to preset_ListBox.Items.count do begin
         //preset_index[i]:=preset_index[i+1];
         message('preset: "'+preset_listbox.Items.Strings[0] +'" mark: '+preset_index[0]);
         message('preset: "'+preset_listbox.Items.Strings[1] +'" mark: '+preset_index[1]);
         message('preset: "'+preset_listbox.Items.Strings[2] +'" mark: '+preset_index[2]);
         // message('preset: "'+preset_listbox.Items.Strings[3] +'" mark: '+preset_index[3]);
         message('----');
         // message('preset: "'+preset_listbox.Items.Strings[i] +'" mark: '+preset_index[i]);;
    end;
    }
    message('Hiding Delete_panel');
    Delete_Panel.visible:=False;
    lock_main_buttons;
    if reach_preset_limit then Button_save.enabled:=false;
    Update_panel.cursor:=crDefault;
    message('Procedure Delete_preset done');


    
End;    // delete_preset


