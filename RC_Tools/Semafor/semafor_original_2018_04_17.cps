var
   aform:Tform;
   edNaziv:tedit;
   track:tscrollbar;
   cnt:integer;
   btn:tspeedbutton;
   emptyDummyList:tlist;
   lb:tlistbox;
   memoJ:TMemo;
   
   
Procedure AddDummy(sender:tobject);   // dodaje dummy u objekt dummy
                                       // dummy postoji uvijek i ne treba ga deklarirati
var
   tmp:Tdummy;
   i:integer;
   parent,owner:tobject;
   s:string;
   v:tvector3f;
   v4:tvector;
   color:tvector;
begin
  cnt:=cnt+1;
  tmp:=dummy.AddNewDummy;
  tmp.owner:=e;
  {dummy.Position.x:=dummy.Position.x+100;
  dummy.Position.y:=100;
  dummy.Position.Z:=100;}
  {dummy.Move(10);
  dummy.Slide(10);
  dummy.Lift(10);}
  
  {dummy.Size.x:=500;
  dummy.Size.y:=500;
  dummy.Size.Z:=300;}
  tmp.position.x:= 300*sin(degtorad(cnt*15));
  tmp.position.z:= 300*cos(degtorad(cnt*15));
  tmp.position.y:=30*cnt;
  
  tmp.BlendMode:=0;
  
  
  tmp.Pickable:=false;
  tmp.Visible:=true;
  for i := 0 to dummy.count-1 do begin
      tmp:=dummy.dummy[i];
      ///////////////////////// boja pojedine komponente
      tmp.Color.R:=0.1*i;
      tmp.Color.G:=0.1;
      tmp.Color.B:=1/(i+1);
      tmp.Color.A:=0.3;    
      ////////////////////////// boja vektor (array[0..3])
      color:=tmp.Color.color;
      color[3]:=1/cnt;
      tmp.Color.color:=color;
      //////////////////////////////
  end;
  v4:=tmp.AbsolutePosition;
  v:=tmp.AbsoluteAffinePosition;
  s:='Absolutni polo�aj u elementu: ';
  s:=s+' X= '+floattostrc1(v[0],6,2);
  s:=s+' Y= '+floattostrc1(v[1],6,2);
  s:=s+' Z='+floattostrc1(v[2],6,2)+#13#10;
  v:=tmp.position.vektor;
  s:=s+'Lokalni polo�aj u parentu: ';
  s:=s+' X= '+floattostrc1(v[0],6,2);
  s:=s+' Y= '+floattostrc1(v[1],6,2);
  s:=s+' Z='+floattostrc1(v[2],6,2)+#13#10;
  owner:=tmp.owner;
  //  showmessage('Zadani Owner'+telement(owner).naziv);
  MemoJ.Lines.Add('Zadani Owner'+telement(owner).naziv);
  parent:= tmp.parent;
  if parent=dummy then MemoJ.Lines.Add('ovaj dummy je u osnovnom '+#13#10+s);
  // svi dodani su u osnovnom "DUMMY" a on je u editoru;
  
  // obri�i prvi child za test
  // netreba ga removat, to radi automatski
  if dummy.count>10 then begin    
    dummy.dummy[0].free;      // iz lb bi sad trebalo izbaciti ovog i sve njegove childove ali mi se neda
  end;
end;





procedure onBtnClick(sender:tobject);
var
   adummy,atmp:tdummy;
begin
  if emptyDummyList.count=0 then begin
     MemoJ.Lines.Add('lista je prazna');
     exit;
  end;   
  adummy:=tdummy(emptyDummyList.items[0]);
  emptyDummyList.remove(adummy);  
  atmp:=adummy.AddNewDummy; // dodaj u njega novi;
/////////////////////////////////  
  atmp.position.init(0,0,0);
  {atmp.position.x:=0;
  atmp.position.y:=0;        // svejedno koje se koristi
  atmp.position.z:=-0;}
/////////////////////////////  
  
  atmp.size.x:=tdummy(atmp.parent).size.x; // sirina parenta iako znam da je parent adummy, ovo je za demo da se vidi da i to radi
  atmp.size.z:=tdummy(atmp.parent).size.z;
  atmp.size.y:=tdummy(atmp.parent).size.y/2-9;
  atmp.color.init(0,0.9,0,0.9);
  {atmp.color.r:=0;
  atmp.color.g:=0.9;
  atmp.color.b:=0;}
  atmp.data.add(e.childdaska.daska[0]);
  atmp.pickable:=true;
  atmp.BlendMode:=1;
  atmp.Name := 'Bla Bla';
  atmp.tag:=89434;
  atmp.offset:=2;                               // dummy je ve�i za dva ne�ega
                                                // apsolutan, a ne relativan u odnosu na parenta
  emptyDummyList.add(atmp);
  lb.items.addobject(atmp.name,atmp);
  
  atmp:=adummy.AddNewDummy; // dodaj u njega novi;
  {atmp.position.x:=0;
  atmp.position.y:=tdummy(atmp.parent).size.y/2+9;
  atmp.position.z:=0;}
  atmp.position.init(0,tdummy(atmp.parent).size.y/2+9,0);  
  // atmp.size.init(x,y,z);
  atmp.size.x:=tdummy(atmp.parent).size.x; // sirina parenta iako znam da je parent adummy, ovo je za demo da se vidi da i to radi
  atmp.size.z:=tdummy(atmp.parent).size.z;
  atmp.size.y:=tdummy(atmp.parent).size.y/2-9;
  atmp.color.r:=0;
  atmp.color.g:=0.1;
  atmp.color.b:=1;
  atmp.color.init(0,0.1,1,0.1);
  atmp.BlendMode:=0;
  atmp.Name := 'ldfkgd�flgkd�f';
  atmp.tag:=22;
  atmp.offset:=-1;  // -1 je default  // dummy je manji za dva ne�ega
  atmp.data.add(e.childdaska.daska[1]);   // data je Tlist, na programeru je da ga puni i prazni po potrebi.
  
  emptyDummyList.add(atmp);
  lb.items.addobject(atmp.name,atmp);
  
  adummy.visible:=false;  // ako se ne sakriva onda childovi nesmiju dirati rubove jel ru�no izgleda, ili treba koristiti offset

  e.recalcformula(nil); // mali trik
end;

Procedure onLbClick(sender:tobject);   // listbox
var
   adummy:tdummy;
begin
   if lb.itemindex>-1 then begin
      adummy:=tdummy(lb.items.objects[lb.itemindex]);
      adummy.visible:= not adummy.visible;
      e.recalcformula(nil); // mali trik
   end;  
end;

procedure onTrackChange(sender:tobject);  // trackabr
begin
  dummy.position.x:=track.position;
  e.recalcformula(nil);
end;
   
Procedure NapraviFormu;
begin
  //aform:=CreateParentedForm(false);
  edNaziv:=tedit.create(aForm);
  edNaziv.parent:=aForm;
  if e<>nil then begin
     if e.parent<>nil then edNaziv.text:=telement(e.parent).naziv else
     edNaziv.text:='';
  end;
  track:=tscrollbar.create(aform);
  track.parent:=aform;
  track.top:=30;
  track.width:=aform.width-20;
  track.max:=1000;
  track.min:=-1000;
  track.onchange:=@onTrackChange;
  
  btn:=tspeedbutton.create(aform);
  btn.parent:=aform;
  btn.top:=2;
  btn.left:= edNaziv.width+5;
  btn.width:=100;
  btn.caption:='podjeli  dummy';
  btn.onclick:= @onBtnClick;
  
  btn:=tspeedbutton.create(aform);
  btn.parent:=aform;
  btn.top:=2;
  btn.left:= edNaziv.width+5+105;
  btn.width:=60;
  btn.caption:='dodaj dummy';
  btn.onclick:= @AddDummy;
  
  lb:=tlistbox.create(aform);
  lb.parent:=aform;
  lb.top:=60;
  lb.height:=200;
  lb.onclick:=@onLbClick;
  
  lb.items.addobject('MainDummy',Dummy);
  
   Memoj:=TMemo.create(aForm);
  MemoJ.Parent:=aForm;
  MemoJ.align:=alBottom;
  MemoJ.Height:=600;
  
  
  aform.show;
end;

function isDaskaOk(Param1: tdaska): boolean;   // izbaciti
begin
  MemoJ.Lines.Add('Param1: '+Param1.naziv );
  Result := Param1.naziv[1]='P';
end;

Procedure EngineStart(parent:tobject);   // mora se ovako zvati, parent joj je editor
begin     
  showmessage('EngineStart');   
	
	aform:=tform.create(twincontrol(parent));
	aform.align:=alLeft;
	aform.parent:=twincontrol(parent);
	aform.height:=400;
	NapraviFormu;

     dummy.visible:=true;
     dummy.size.x:=e.sirina;  // povecaj da nebude onaj mali
     dummy.position.x:=0;  // pomakni ga u nulu
     dummy.position.z:=0;  // pomakni ga u nulu
     dummy.position.y:=0;  // pomakni ga u nulu
     dummy.size.y:=e.visina 

  emptyDummyList:=tlist.create;
  emptyDummyList.add(dummy);
end;

Procedure EngineEnd;
begin
   // showmessage('EngineEnd');
   if aform<>nil then aform.free;
   emptyDummyList.free;             // neka lista, dummije ne treba freejat
                                    // roditeljski dummy se ne smije freejat
                                    // djeca dummiji ne nasljedjuju visible  
end;


Procedure EngineGetObject(sender:tobject);    // select, down, a ne klik
begin
     if sender is tdaska then MemoJ.Lines.Add('Klik na dasku '+tdaska(sender).naziv) 
     else
       if sender is telement then MemoJ.Lines.Add('Klik na Element '+telement(sender).naziv)
     else 
       if sender is tdummy then MemoJ.Lines.Add('Klik na dummy '+tdummy(sender).name)
     else
       if sender <> nil then MemoJ.Lines.Add('Klik na Ne�to ')
     else 
       MemoJ.Lines.Add('Klik na Prazno ');

end;

procedure EngineMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y, Z: Single);
var
   s:string;
   btn:array[0..2] of string; 
begin
     btn[0]:='mbLeft';
     btn[1]:='mbRight';
     btn[2]:='mbMiddle';
     if sender is tdaska then s:=tdaska(sender).naziv else
     if sender is telement then s:=telement(sender).naziv else
     if sender <> nil then s:='Ne�to' else s:= 'Ni�ta';
     s:=s+#13#10;
     s:=s+'X:= '+floattostrc1(x,8,2)+#13#10;
     s:=s+'Y:= '+floattostrc1(Y,8,2)+#13#10;
     s:=s+'Z:= '+floattostrc1(Z,8,2)+#13#10;
     s:=s+'Tipka '+btn[integer(Button)]++#13#10;
     s:=s+'SHIFT =[';
       if ssleft in shift then s:= s +'ssLeft,'; 
       if ssRight in shift then s:= s +'ssRight,';
       if ssMiddle in shift then s:= s +'ssMiddle,';
       if ssDouble in shift then s:= s +'ssDouble,';  // ovo ne�e biti nikad.
       if ssShift in shift then s:= s +'ssShift,';
       if ssAlt in shift then s:= s +'ssAlt,';
       if ssCtrl in shift then s:= s +'ssCtrl,';
     s:=s+']';
    
    MemoJ.Lines.Add(s);
    MemoJ.Lines.Add('  ');      
end;



begin
   
end.