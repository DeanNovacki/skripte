//  preset values

   el_name: String;
   el_type: Integer;
   el_date: String;    //2012_08_21
   el_shape, 
   el_width, el_width_min, el_width_max,
   el_height, el_height_min, el_height_max,
   el_depth, el_depth_min, el_depth_max,
   left_depth, left_depth_min, left_depth_max,
   right_depth, right_depth_min, right_depth_max,
   top_type, 
   top_thick, top_thick_min, top_thick_max, 
   top_dist_front, top_dist_front_min, top_dist_front_max, 
   top_dist_back_fix, top_dist_back_min, top_dist_back_max,
   top_width_front, top_width_front_min, top_width_front_max,
   top_width_back, top_width_back_min, top_width_back_max,   
   bottom_type, 
   bottom_thick, bottom_thick_min, bottom_thick_max,
   bottom_dist_front, bottom_dist_front_min, bottom_dist_front_max,   
   bottom_dist_back, bottom_dist_back_min, bottom_dist_back_max,
   back_on, 
   back_dist, back_dist_min, back_dist_max, 
   back_thick, back_thick_min, back_thick_max,
   back_ins, back_ins_min, back_ins_max,
   back_top_width, back_top_width_min, back_top_width_max,
   back_down_width, back_down_width_min, back_down_width_max,   
   side_thick, side_thick_min, side_thick_max,
   shelf_thick, shelf_thick_min, shelf_thick_max,
   shelf_qnt,
   shelf_mov, shelf_mov_min, shelf_mov_max, 
   shelf_front_dist, shelf_front_dist_min, shelf_front_dist_max,
   shelf_radius, shelf_radius_min, shelf_radius_max,
   shelf_cut_front, shelf_cut_front_min, shelf_cut_front_max,     
   shelf_cut_side, shelf_cut_side_min, shelf_cut_side_max,
   leg_height, 
   leg_dist_front, leg_dist_front_min, leg_dist_front_max, 
   leg_dist_side, leg_dist_side_min, leg_dist_side_max,
   leg_dist_back, leg_dist_back_min, leg_dist_back_max :Integer;
   door_qnt: integer;
   door_orient: string;    // 'left'
   left_offset, right_offset, up_offset, down_offset: string;
   // info :array[0..64] of String;

// 
   W_Window:TForm;
   Hello, PanelP1, Mini_panel: TPanel; 

   Adj_SB:TScrollBox;                      // container for adjusment panels
   AP: array[1..9] of TPanel;              // adjustment panels 
   AP_Height: array[1..9] of Integer;       // adjustment panels heights
   APH: array[1..9] of TPanel;             // adjustment panels headers   
   APS: array[1..9] of TPanel;             // adjustment panels minimize symbols
   SBSP:Integer;            // position od scrollBox scroller bar
   
   // Presets
   Preset_ListBox:TListBox;                // list box containing default and user presets
   // PresetM:TMemo;                          // info about each preset
   preset_index: array[0..64] of String;   // for marking presets as D(efault), U(ser) or (L)ine
   last_preset: integer;                   // index of last used preset;
   // new_preset_name: Tedit;                     // Tedit for name of new oreset
   // AP1       Type 
   
   AP1_Memo:TMemo;
   edit_name:Tedit;
   
   Button_OK, Button_Cancel, Button_Help, Button_About, 
   Button_update, Button_Save, Button_Delete:TButton;
   cc, Headline:TLabel;
   BLine:TBevel;
   
   Save_panel:Tpanel;
   Save_field:Tedit;
   Butt_ok, Butt_cancel:TButton;


   
   Update_panel:Tpanel;
   Update_Field:TEdit;
   Update_ok, Update_cancel:TButton;
   
   Delete_panel:Tpanel;
   Delete_Field:TEdit;
   Delete_ok, Delete_cancel:TButton;

   warning_header, warning_line1, warning_line2, warning_line3:TLabel;
   
   //ini_file:TStringList;
   script_path, IniName, lang_file:string;

   // progress bar
   progress_panel,bar_out, bar_in:TPanel;
   progress_text:TLabel;
   start_ok:boolean;
         
 	load_element:string;    