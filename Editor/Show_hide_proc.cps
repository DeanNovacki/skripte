// SHOW - HIDE SKUP PROCEDURA

// ShowD_ABC = 0 - skriva dasku ili element koji se zove ABC
// ShowD_ABC = 1 - pokazuje dasku ili element koji se zove ABC
// ShowC_ABC = b - pokazuje sve daske ili elemente naziva ABCxy kojima je xy<=b, a ostale skriva

// primjer za ShowC
// postoje daske pol1, pol2, pol3, pol4, pol5 i pol6
// ShowC_pol=4 �e pokazati daske pol1, pol2, pol3, pol4, a sakriti daske pol5 i pol6 
// vrijednost za ShowC mora biti izme�u 0 i 98
// daska smije imati maksimalno dvoznamenkasti broj u zavrsetku (pol34 je ok, pol367 nije ok)


Procedure Obrada_ShowC(VarName:String;VarValue:Extended);
var
   i:integer;
   VarDaskaName,nd, daskaroot:string;
   DaskaIndex,vv2:Integer;
   td:TDaska;
   ne:TElement;
Begin
   //Showmessage ('Obrada za ShowC');
   VarDaskaName:=copy(VarName,7,20);    // tra�i naziv daske iz varijable ShowC
   // daske
   for i:=0 to e.childdaska.CountObj-1 do Begin      //e.childdaska.CountObj = broj dasaka u elementu
      td:=e.childdaska.Daska[i];                    
      nd:=td.Naziv;
      If Copy(nd,1,Length(VarDaskaName))=VarDaskaName then begin    // da li je lijevi dio naziva daske jednak
                                                                    // onom iza ShowC varijable? 
         daskaroot:=copy(nd,1,Length(VarDaskaName));                // root ime je naziv iz ShowC varijable
         Try
            DaskaIndex:=StrToInt(Copy(nd,Length(VarDaskaName)+1,2));     // pokusaj tra�enja indexa na stvarnoj dasci
          except
            DaskaIndex:=-99;
         end;
         vv2:=Round(VarValue);
         if DaskaIndex>vv2 then td.visible:=false
                            else td.visible:=true;
      end; // if
   end; // for 
   // daske
   for i:=0 to e.ElmList.CountObj-1 do Begin      //e.ElmList.CountObj = Lista elemenata u elementu
      ne:=e.ElmList.Element[i];                    
      nd:=ne.Naziv;
      If Copy(nd,1,Length(VarDaskaName))=VarDaskaName then begin    // da li je lijevi dio naziva daske jednak
                                                                    // onom iza ShowC varijable? 
         daskaroot:=copy(nd,1,Length(VarDaskaName));                // root ime je naziv iz ShowC varijable
         Try
            DaskaIndex:=StrToInt(Copy(nd,Length(VarDaskaName)+1,2));     // pokusaj tra�enja indexa na stvarnoj dasci
          except
            DaskaIndex:=-99;
         end;
         vv2:=Round(VarValue);
         if DaskaIndex>vv2 then ne.visible:=false
                            else ne.visible:=true;
      end; // if
   end; // for   
End;  // proc


Procedure Obrada_ShowD(VarName:String;VarValue:Extended);
var
   i:integer;
   DaskaName,nd:string;
   td:TDaska;
   ne:TElement;
Begin
   DaskaName:=copy(VarName,7,20);
   // daske
   for i:=0 to e.childdaska.CountObj-1 do Begin      //e.childdaska.CountObj = broj dasaka u elementu
      td:=e.childdaska.Daska[i];                    //jedna daska iz elementa (s rednim brojem i)
      nd:=td.Naziv;
      if (td.Naziv=DaskaName) then 
         if VarValue=1  then td.visible:=true
         else if VarValue=0 then td.visible:=false;
   end;
   // elementi
  for i:=0 to e.ElmList.CountObj-1 do Begin        //el.ElmList.CountObj = broj elemenata u elementu
      ne:=e.ElmList.Element[i];                     //jedan element iz elementa (s rednim brojem i)
      nd:=ne.naziv;
      if (ne.Naziv=DaskaName) then 
         if VarValue=1  then ne.visible:=true
         else if VarValue=0 then ne.visible:=false;
   end; 
End;

Procedure Show_Hide_proc;

var
   i:integer;
   VarName:String;
   VarValue:extended;
   td:Tdaska;

begin

  For i:=0 to e.Fvarijable.Count-1 do begin      // citanje svih varijabli
      VarName:=e.Fvarijable.Names[i]    // Nazivi varijabli
      VarValue:=e.Fvarijable.ParseVar(e.Fvarijable.Names[i]);
      If copy (VarName,1,6)='ShowC_' then Obrada_ShowC(VarName,VarValue);
      If copy (VarName,1,6)='ShowD_' then Obrada_ShowD(VarName,varValue); 
  end;
 
end;