Procedure cbDebug_OnClick(Sender:TObject);	
Begin
	// Application.ProcessMessages;
	
	If cbDebug.checked=True then begin
			Panel_right.visible:=true;
			Panel_right.width:=200;
			Main.Width:=600;
			// Main.Left:=Main.Left-200;
			ShowN('Poruke se pokazuju.');
		end else begin
			Panel_right.visible:=false;
			Main.Width:=400;
			// Main.Left:=Main.Left+200;
			ShowN('Poruke se ne pokazuju.');
	end;   // if
	// Main.Left:=screenwidth-Main.Width-20;
End;

Procedure Kraj_skripte;
Begin
	// Main.Hide;     
   // Main.ShowModal;   
   ShowN('ZATVARANJE PROGRAMA');
	Main.close;
	
   // e.RecalcFormula(e); 
	// Main.free; 
End;



Procedure ButOK_OnClick(Sender:TObject);
Begin
	ShowN('Pritisnuta je tipka "OK"');
	
	If Izvrseno then begin
								Main.close;
								exit;
	end;
	If cbDebug.checked=True then begin 
												Napravi_posao;
												ShowN('Posao zavr�en');
												ButOk.Caption:='Izlaz';
												ButCancel.enabled:=False;
												ButPomoc.enabled:=False;
												Izvrseno:=true;
										  end else begin 
												Napravi_posao;
												Main.close;
	end;
									

End;

Procedure ButCancel_OnClick(Sender:TObject);
Begin
	ShowN('Pritisnuta je tipka "Odustajem"');
	
End;

Procedure ButPomoc_OnClick(Sender:TObject);
Begin
	ShowN('Pritisnuta je tipka "Pomo�"');
	Pomoc;
End;


	
Procedure Create_Main_Window;
Begin
	// Startni prozor
   Main:=rcTool('Prazna_skripta'); 
	Main.Width:=400;
	Main.Height:=400;
	// Main.Left:=200;
	Main.BorderStyle:=bsDialog;
	Main.Position:=poDesigned;
	
	Main.Top:=20;

	// Donji Panel
	Panel_Down:= rcPanel ( '' ,Main ); 
	Panel_Down.ALign:=alBottom; 
   Panel_Down.Height:=40;       // Visina donjeg panela!!!!
   Panel_Down.BorderWidth:=5;
	
   // desni panel
   Panel_Right:= rcPanel ( '' ,Main ); Panel_Right.ALign:=alRight; 
   Panel_Right.Width:=2;       // Širina desnog panela!!!!
   Panel_Right.BorderWidth:=5;
	
   // lijevi panel
   Panel_Left:= rcPanel ( '' ,Main ); Panel_Left.ALign:=alClient;
	
   // tipke OK i Cancel
   
	ButOK:=rcButton('bOK','OK',Panel_Down,1000,1);ButOK.Align:=alRight; ButOK.width:=100;
	rcSpaceR(Panel_Down,800);
	ButCancel:=rcButton('bCancel','Odustajem',Panel_Down,1,1);ButCancel.Align:=alRight;ButCancel.width:=100;
   rcSpaceR(Panel_Down,600);
	ButPomoc:=rcButton('bPomoc','Pomo�',Panel_Down,1,1);ButPomoc.Align:=alLeft;ButPomoc.width:=100;
	rcSpaceL(Panel_Down,400);
	
	// debug
	
	rcSpaceR(Panel_Down,300);
	cbDebug:=rcCheckBox('cbDB','',Panel_Down, 1,20);cbDebug.Align:=alClient; 
	cbDebug.Font.Size:=7;
	cbDebug.hint:='Prika�i poruke za vrijeme izvr�avanja skripte';
	rcSpaceR(Panel_Down,1);rcSpaceR(Panel_Down,1);rcSpaceR(Panel_Down,1);
	
	if debug then cbDebug.checked:=true
				else cbDebug.checked:=false;
				
	cbDebug.OnClick:=@cbDebug_OnClick;
	
	  
	
   // MemoJ
   Panel_Memo:=rcPanel ( '' ,Panel_Right ); Panel_memo.Top:=18;Panel_Memo.ALign:=alClient; 
   
   MemoJ:=rcMemo('Memo_debug', Panel_Memo, 1,18,1,1);MemoJ.Align:=alClient;
   MemoJ.Color:=clBTNFace;MemoJ.Font.Size:=8;MemoJ.Ctl3D:=False;MemoJ.Font.Color:=clMaroon;

	// Application.ProcessMessages;
	If cbDebug.checked=True then begin
			Panel_right.visible:=true;
			Panel_right.width:=200;
			Main.Width:=600;
			// ShowD('Main.Width='+IntToStr(Main.Width));
		end else begin
			Panel_right.visible:=false;
			Main.Width:=400;
	end;   // if
	Main.Left:=screenwidth-Main.Width-20;	
	
End;

// Glavni   
Begin
   Debug:=true;         // debuger ispis on/off
	Debug:=false;
	Izvrseno:=false;

	
	Create_Main_Window;
	ShowN('skripta: ZAMIJENI ELEMENT v1.0');
	ShowN('01.06.2017.');
	ShowN('==============');
	// eventi
	cbDebug_OnClick(cbDebug);  // poziva "cbDebug_OnClick" bez da je i�ta kliknuto
	ButOK.OnClick:=@ButOK_OnClick;
	ButCancel.OnClick:=@ButCancel_OnClick;
	ButPomoc.OnClick:=@ButPomoc_OnClick;
	
	ButOK.default:=True;
	ButCancel.ModalResult:=mrCancel;
	
   // Naslov
   
   // zaglavlje
   
   // tablica
   
   Main.ShowModal;
   // Panel_Title:=rcPanel ( 'dsafdasf' ,Panel_Left ); Panel_Title.ALign:=alTop;
   Main.free; 
   
   
End.