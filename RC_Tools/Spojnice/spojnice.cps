﻿
{$I ..\include\rcPrint.cps}

var // progress bar
	MessForm: TForm; messLab1, messLab2 : tLabel; box1, box2 : tShape;
{$I ..\include\rcProgress2.cps}

// {$I ..\include\rcMessages.cps}
{$I ..\include\rcStrings.cps}
{$I ..\include\rcControls.cps}
// {$I ..\include\rcObjects.cps}
{$I ..\include\rcConfig.cps}
// {$I ..\include\rcEvents.cps}
// {$I ..\include\rcCurves.cps}
// {$I ..\include\rcMath.cps}
// {$I ..\include\rcOut.cps}
{$I ..\include\rcTranslate.cps}
{$I ..\include\rcSystem.cps}
{$I ..\include\rcGetCodes.cps}

// ovo je test edita da vidim kako rado kraken i gitlab
// nema svrhe jer je komenta valjda

// test broj 2
// editirano u notepodu u clone folderu

var
	dummyvar : String // opet test da vidmio kako radi GIT
	versiontxt:string;
	jezik:string;
	brand:string;
	sPath:string;
	main:TForm;
	PanMain : TPanel;
		PanDown : TPAnel;
			ButOK, ButCancel, ButPomoc : TButton;
		
		PanLeft : TPanel;
			PanLeftHeader : TPanel;
				LabLeftTitle : TLabel;
			ScrollLeft : TScrollBox;
				PanSpoj : Array of TPanel;
				LabSpoj : Array of TLabel;
				PicSpojOFF : Array of TMImage;
				PicSpojON : Array of TMImage;
				LabRB : Array of TLabel;
			PanLeftFooter: TPanel;
				ButDelMakroActive 		: TButton;
				ButDelMakroAll 	: TButton;
				
		PanRight :TPanel;
			LabBrand : TLabel;
			ComboProizvodjac : TComboBox;
			LabNaziv : TEdit;
			ButOdaberi : TButton;
			
			ScrollRight : TScrollBox;
			
				Crta:TShape;
				
				// naslov
				Naslov:TLabel;
				Opis : TMemo;
				
				// Sifre: Spojnica, Vijak, Podlozna, Vijci podlozne
				SifraSpojnice :  tEdit;
				butSifraSpojnice : TButton ;
				
				SifraVijkaSpojnice : tEdit;
				butSifraVijkaSpojnice : tButton;
				
				SifraPodloznePlocice : tEdit;
				butSifraPodloznePlocice : tButton;
				
				SifraVijkaPlocice : tEdit;
				butSifraVijkaPlocice : tButton;
				
				// Sifra: Ublazivac zatvaranja
				SifraUblazivaca : tEdit;
				butSifraUblazivaca : tButton;
				
				// Sifra: Kapica
				SifraKapice : tEdit;
				butSifraKapice : tButton;
				
				// Sifra i kolicina: Extra1
				SifraExtra1 : tEdit;
				butSifraExtra1 : tButton;
				
				SifraExtra1Kol : tEdit;
				
				// Sifra i kolicina: Extra2
				SifraExtra2 : tEdit;
				butSifraExtra2 : tButton;
				
				SifraExtra2Kol,
				
				// Spojnica: polozaj
				OdGornjegRuba,
				OdDonjegRuba,
				MaxRazmak,
				
				// Spojnica: rupe
				CasicaOdRuba,
				CasicaFi,
				CasicaDubina,
				CasVijRaz,
				CasVijPom,
				CasVijDub,
				CasVijFi : tEdit;
				
				// podlozna plocica: vrsta 0/1 = krizna/ravna
				LabPodlozna	: tLabel;		// naslov za tComboBox
				VrstaPodlozne : tComboBox;
				
				// podlozna plocica: rupe
				KriznaX,
				KriznaRazmak,
				RavnaX,
				RavnaRazmak,
				PodloznaFi,
				PodloznaDubina,
				PodloznaRavnaSifra : tEdit;
				ButPodloznaRavnaSifra : tButton;
				
				// Silikonski ublaziva�: sifra i rupe
				SifraSilUblaz : tEdit;
				butSifraSilUblaz : tButton;
				LabRupaSil: tLabel;
				RupaSil : tComboBox;
				RupaSilX,
				RupaSilGoreY,
				RupaSilDoljeY,
				RupaSilFi,
				RupaSilDubina,
				
				Element3D : TEdit;
				ButOdaberiElem : TButton;
				
				MakroFile:TEdit;
								
				// JOINT polozaj
				LabPozO2:TLabel;		// naslov za tComboBox
				PozicijaObj2: TComboBox;
					o2x,o2y,o2z:integer;
	
				
	putanja_slike_ini,nazivSlikeON, NazivSlikeOFF,
	putanja_naziva,Naziv_slike,Text_naziva:string;
	
	Odabir:Integer;
	selektiran:boolean;
	ii:Integer;
	BrojSlotova:Integer;
	Obj2Udaljenost:Integer;
	tt:string;
	
Procedure SpremiDefiniciju(DefPath:string);
// Zapisuje definiciju na disk
// svaki put kreira novu, zapisuje je na disk i pregazi postoje�u
var
	lista:TStringList;
Begin
	PrintTime ('SpremiDefiniciju Start');
	
	Lista:=TStringList.create;
	Lista.Add('Opis='+Opis.text)
	
	// Sifre: Spojnica, Vijak, Podlozna, Vijci podlozne
	Lista.Add('SifraSpojnice='+			SifraSpojnice.text);
	Lista.Add('SifraVijkaSpojnice='+		SifraVijkaSpojnice.text);
	Lista.Add('SifraPodloznePlocice='+	SifraPodloznePlocice.text);
	Lista.Add('SifraVijkaPlocice='+		SifraVijkaPlocice.text);
	
	// Sifra: Ublazivac zatvaranja
	Lista.Add('SifraUblazivaca='+			SifraUblazivaca.text);
	
	// Sifra: Kapica
	Lista.Add('SifraKapice='+				SifraKapice.text);
	
	// Sifra i kolicina: Extra1
	Lista.Add('SifraExtra1='+				SifraExtra1.text);
	Lista.Add('SifraExtra1Kol='+			SifraExtra1Kol.text);
	
	// Sifra i kolicina: Extra2
	Lista.Add('SifraExtra2='+				SifraExtra2.text);
	Lista.Add('SifraExtra2Kol='+			SifraExtra2Kol.text);
	
	// Spojnica: polozaj
	Lista.Add('OdGornjegRuba='+			OdGornjegRuba.text);
	Lista.Add('OdDonjegRuba='+				OdDonjegRuba.text);
	Lista.Add('MaxRazmak='+					MaxRazmak.text);
	
	// Spojnica: rupe
	Lista.Add('CasicaOdRuba='+				CasicaOdRuba.text);
	Lista.Add('CasicaFi='+					CasicaFi.text);
	Lista.Add('CasicaDubina='+				CasicaDubina.text);
	Lista.Add('CasVijRaz='+					CasVijRaz.text);
	Lista.Add('CasVijPom='+					CasVijPom.text);
	Lista.Add('CasVijDub='+					CasVijDub.text);
	Lista.Add('CasVijFi='+					CasVijFi.text);
	
	// podlozna plocica: vrsta 0/1 = krizna/ravna
	Lista.Add('VrstaPodlozne='+			IntToStr(VrstaPodlozne.ItemIndex));
	
	// podlozna plocica: rupe
	Lista.Add('KriznaX='+					KriznaX.text);
	Lista.Add('KriznaRazmak='+				KriznaRazmak.text);
	Lista.Add('RavnaX='+						RavnaX.text);
	Lista.Add('RavnaRazmak='+				RavnaRazmak.text);
	Lista.Add('PodloznaFi='+				PodloznaFi.text);
	Lista.Add('PodloznaDubina='+			PodloznaDubina.text);
	
	// Silikonski ublaziva�: sifra i rupe
	Lista.Add('SifraSilUblaz='+			SifraSilUblaz.text);
	Lista.Add('RupaSil='+               IntToStr(RupaSil.ItemIndex));
	Lista.Add('RupaSilX='+              RupaSilX.text);
	Lista.Add('RupaSilGoreY='+          RupaSilGoreY.text);
	Lista.Add('RupaSilDoljeY='+         RupaSilDoljeY.text);
	Lista.Add('RupaSilFi='+             RupaSilFi.text);
	Lista.Add('RupaSilDubina='+         RupaSilDubina.text);
	Lista.Add('PodloznaRavnaSifra='+ 	PodloznaRavnaSifra.text);
	
	Lista.Add('Element3D='+					Element3D.text);
	Lista.Add('MakroFile='+					MakroFile.text);
	Lista.Add('PozicijaObj2='+				IntToStr(PozicijaObj2.ItemIndex));
	Lista.Add('o2x='+							IntToStr(o2x));
	Lista.Add('o2y='+							IntToStr(o2y));
	Lista.Add('o2z='+							IntToStr(o2z));
	
	
	try lista.SaveToFile(DefPath);
     except ShowMessage( 'Gre�ka kod spremanja predlo�ka!' + #13#10 + DefPath);
   end; 
	

	
	PrintTime ('SpremiDefiniciju End');
End;


Procedure PokreniMakro;
var
	i,j,k:integer;
	sel:boolean;
	te:tElement;
	td:tDaska;
	tlista:TStringList;
	ss:string;
	MakroPath:String;
Begin
	Print ('PokreniMakro start');
	sel:=false;
  
	MakroPath:=sPath+'Brand\'+ComboProizvodjac.Items[ComboProizvodjac.ItemIndex]+'\'+MakroFile.text;
	Print ('MakroPath:='+MakroPath);
	

  
  // probaj u selektiranom elementu
  
  for i:=0 to e.ElmList.CountObj-1 do Begin     
		te:=e.ElmList.Element[i];
		// Print ('na�ao sam element: "'+te.naziv+'"');										
		if (te.selected) and (te.TipElementa in [VRE]) then begin						// selektiran je i fronta je
			// Print ('u�ao sam u element: "'+te.naziv+'"');
			for j:=0 to te.childdaska.CountObj-1 do Begin									// brojim daske u njemu
				td:=te.childdaska.Daska[j];														//jedna daska iz elementa (s rednim brojem i)
				
				// Print ('na�ao sam dasku: "'+td.naziv+'" u elementu: "'+ te.naziv+'"');
				if (td.tipDaske in [Fronta]) and (td.visible) then begin
					// Print ('u�ao sam u dasku: "'+td.naziv+'"');
					
					// Obri�i postoje�e makroe u vratima
					// te.DestroyLinks;
					
					// Obri�i postoje�i makro u dasci
					ss:=Copy(MakroFile.text,1,Length(MakroFile.text)-4);   // bri�e se '.cmk' iz naziva makroa
					te.DeleteSpoj(ss,td);	
					//te.DeleteSpoj(MakroFile.text,td);
					print ('Obrisao sam makro "'+ss+'" u dasci "'+td.naziv+'" u elementu "'+te.naziv+'"');
					// ('Procedure DeleteSpoj(Macroname:string;KogaSpaja:tobject)');
					
					// * * * * V A � N O * * * * * * * 
					//
					// Putanja makroa mora biti u odnosu na putanju makro foldera
					
					MakroPath:='..\skripte\RC_Tools\Spojnice\Brands\'
									+ComboProizvodjac.Items[ComboProizvodjac.ItemIndex]+'\'
									+MakroFile.text;
										
					te.MakeSpoj(td,nil,MakroPath,'');
					Print ('Napravio sam "'+ MakroPath+'" na: "'+td.naziv+'"');
					// Print ('Makro: "'+MakroPath+'"');
					
					// ispis makroa za test
					// tlista:=TStringList.create;
					// tlista.LoadFromFile(MakroPath);
					// For k:=0 to tLista.count-1 Do Begin
					//	Print(tLista[k]);
					// End;
					
					sel:=true;
					// te.recalcformula(te);
					// application.processmessages;	
					sel:=true;
				
				
				end;
			end;
		end;
   end;  
   // showmessage ('Pro�ao sam kroz sve elemente');
	
	// probaj u selektiranoj dasci
	if not sel then begin
		for i:=0 to e.childdaska.CountObj-1 do Begin									// brojim daske u njemu
				td:=e.childdaska.Daska[i];														//jedna daska iz elementa (s rednim brojem i)
				
				// Print ('na�ao sam dasku: "'+td.naziv+'" u elementu: "'+ te.naziv+'"');
				if (td.tipDaske in [Fronta]) and (td.visible) and (td.selected) then begin
					// Print ('u�ao sam u selektiranu dasku: "'+td.naziv+'"');
					
					// Obri�i postoje�i makro u dasci
					e.DeleteSpoj('MakroFile.text',td);
					
					// ('Procedure DeleteSpoj(Macroname:string;KogaSpaja:tobject)');
					
					// * * * * V A � N O * * * * * * * 
					//
					// Putanja makroa mora biti u odnosu na putanju makro foldera
					
					MakroPath:='..\skripte\RC_Tools\Spojnice\Brands\'
									+ComboProizvodjac.Items[ComboProizvodjac.ItemIndex]+'\'
									+MakroFile.text;
										
					te.MakeSpoj(td,nil,MakroPath,'');
					// Print ('Napravio sam spoj na: "'+td.naziv+'"');
					// Print ('Makro: "'+MakroPath+'"');
					
					// ispis makroa za test
					// tlista:=TStringList.create;
					// tlista.LoadFromFile(MakroPath);
					// For k:=0 to tLista.count-1 Do Begin
					//	Print(tLista[k]);
					// End;
					
					sel:=true;
					te.recalcformula(te);
					application.processmessages;	
					sel:=true;
				
				
				end;
			end;
	end;
	
	
	
	
   if not sel then ShowMessage('	Nije selektirana niti jedna fronta!');
	e.recalcformula(e); 
	application.processmessages;
	Print ('PokreniMakro end');
End;

Procedure SpremiMakro;
var
	MakroPath:String;
	
Begin
	PrintTIME ('SpremiMakro Start');
	
	MakroPath:=sPath+'Brands\'+ComboProizvodjac.Items[ComboProizvodjac.ItemIndex]+'\'+MakroFile.text;
	// Print ('MakroPath:='+MakroPath);
	
	// a�uriranje makroa
	if FileExists(MakroPath) then begin
	
		// Spojnica: polozaj
		rcSetSS(MakroPath, 'VARIJABLE', 'OdGornjegRuba',	OdGornjegRuba.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'OdDonjegRuba',		OdDonjegRuba.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'MaxRazmak',			MaxRazmak.text);
		
		// Spojnica: rupe
		rcSetSS(MakroPath, 'VARIJABLE', 'CasicaOdRuba',		CasicaOdRuba.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'CasicaFi',			CasicaFi.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'CasicaDubina',		CasicaDubina.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'CasVijRaz',			CasVijRaz.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'CasVijPom',			CasVijPom.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'CasVijDub',			CasVijDub.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'CasVijFi',			CasVijFi.text);
		
		// podlozna plocica: rupe
		rcSetSS(MakroPath, 'VARIJABLE', 'VrstaPodlozne',	IntToStr(VrstaPodlozne.ItemIndex));
		rcSetSS(MakroPath, 'VARIJABLE', 'KriznaX',			KriznaX.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'KriznaRazmak',		KriznaRazmak.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'RavnaX',				RavnaX.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'RavnaRazmak',		RavnaRazmak.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'PodloznaFi',		PodloznaFi.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'PodloznaDubina',	PodloznaDubina.text);

		// Silikonski ublaziva�: sifra i rupe
		if RupaSil.ItemIndex=0 then rcSetSS(MakroPath, 'VARIJABLE', 'RupaSil','0')
									  else rcSetSS(MakroPath, 'VARIJABLE', 'RupaSil','1');
		
		rcSetSS(MakroPath, 'VARIJABLE', 'RupaSilX',			RupaSilX.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'RupaSilGoreY',		RupaSilGoreY.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'RupaSilDoljeY',	RupaSilDoljeY.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'RupaSilFi',			RupaSilFi.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'RupaSilDubina',	RupaSilDubina.text);
		
		// Sifre okova bez posebne koli�ine
		rcSetSS(MakroPath, 'POTROSNI1', 'PS1',		SifraSpojnice.text);
		rcSetSS(MakroPath, 'POTROSNI2', 'PS1',		SifraVijkaSpojnice.text);
		rcSetSS(MakroPath, 'POTROSNI3', 'PS1',		SifraPodloznePlocice.text);
		rcSetSS(MakroPath, 'POTROSNI4', 'PS1',		SifraVijkaPlocice.text);
		rcSetSS(MakroPath, 'POTROSNI5', 'PS1',		SifraUblazivaca.text);
		rcSetSS(MakroPath, 'POTROSNI6', 'PS1',		SifraKapice.text);
		
		// extra potro�ni 1
		rcSetSS(MakroPath, 'POTROSNI7', 'PS1',		SifraExtra1.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'SifraExtra1Kol',	SifraExtra1Kol.text);
		
		// extra potro�ni 2
		rcSetSS(MakroPath, 'POTROSNI8', 'PS1',		SifraExtra2.text);
		rcSetSS(MakroPath, 'VARIJABLE', 'SifraExtra2Kol',	SifraExtra2Kol.text);
		
		// sifra silikonskog ublazivaca
		rcSetSS(MakroPath, 'POTROSNI9', 'PS1',		SifraSilUblaz.text);
		
		// sifra ravne podlo�ne plo�ice
		Print ('�ifra ravne podlo�ne plo�ice: '+PodloznaRavnaSifra.text);
		rcSetSS(MakroPath, 'POTROSNI10', 'PS1',	PodloznaRavnaSifra.text);
		
		// JOINT - izra�un polozaja
		if PozicijaObj2.ItemIndex=0 then begin
			// Objekt 2 je IZA fronte
			o2x:=0;
			o2y:=100;
			o2z:=-obj2Udaljenost;
		end;
		if PozicijaObj2.ItemIndex=1 then begin
			// Objekt 2 je BO�NO (lijevo ili desno)
			o2x:=-obj2Udaljenost;
			o2y:=100;
			o2z:=+obj2Udaljenost;
		end;
		if PozicijaObj2.ItemIndex=2 then begin
			// Objekt 2 je DIJAGONALNO (lijevo ili desno)
			o2x:=-obj2Udaljenost;
			o2y:=100;
			o2z:=-obj2Udaljenost;
		end;
		if PozicijaObj2.ItemIndex=3 then begin
			// Objekt 2 je PREKLOP (lijevo ili desno - dijagonalno naprijed)
			o2x:=-obj2Udaljenost;
			o2y:=100;
			o2z:=obj2Udaljenost*6;
		end;
		
		// JOINT polozaj
		rcSetSS(MakroPath, 'VARIJABLE', 'o2x',	IntToStr(o2x));
		rcSetSS(MakroPath, 'VARIJABLE', 'o2y',	IntToStr(o2y));
		rcSetSS(MakroPath, 'VARIJABLE', 'o2z',	IntToStr(o2z));
		
		

		
		
	end else begin
		ShowMessage ('Ne postoji makro "'+MakroFile.text+'"' + #13#10+	
							'Provjerite rubriku "Makro datoteka" i upi�ite postoje�i makro.');
	end;

	PrintTime ('SpremiMakro End');
End;
	

procedure ButOK_OnClick(sender:TObject);
var
	sss:string;
	DefPath:string;
	slsl:tStringList;
Begin
	print ('Procedura ButOK_OnClick start')
	// function rcFindSS(FileName,SectionName,ValueName:string):string;	
	// sss:=rcFindSS(spath+'Titus Plus Glissando\vrata.CMK','JOINT','JX');
	// print(sss);
	
	
	// Zapisi def datoteku
	rcProgress('Aktiviranje makroa','A�uriranje definicije',5,100);
	DefPath:=sPath+'Brands\'+ComboProizvodjac.Items[ComboProizvodjac.ItemIndex]+'\'+LabNaziv.Text+'.def';
	SpremiDefiniciju(DefPath);
	
	// A�uriraj Slotovi.ini
	rcProgress('Aktiviranje makroa','A�uriranje slotova',10,100);
	DefPath:=sPath+'Brands\'+ComboProizvodjac.Items[ComboProizvodjac.ItemIndex]+'\Slotovi.ini';
	set_value_str(DefPath,IntToStr(Odabir),LabNaziv.text+'.def');
	
	// Spremi Makro
	// sss:=sPath+ComboProizvodjac.Items[ComboProizvodjac.ItemIndex]+'\Slotovi.ini';
	
	rcProgress('Aktiviranje makroa','Zapisivanje makroa',15,100);
	SpremiMakro;	// mora se spremiti jer se izvr�avanje radi pozivanjem s diska
	
	// spremi zadnji kori�teni Brand
	rcProgress('Aktiviranje makroa','A�uriranje aktivne grupe',80,100);
	slsl:=rcDirDirs(sPath+'Brands\');
	set_value_str(sPath+'user.ini','brand',ComboProizvodjac.Items[ComboProizvodjac.ItemIndex]);
	
	// izvrsavanje makroa
	// MakroPath:=sPath+ComboProizvodjac.Items[ComboProizvodjac.ItemIndex]+'\'+MakroFile.text;
	// Print ('MakroPath:='+MakroPath);
	rcProgress('Aktiviranje makroa','Pokretanje novog makroa',85,100);
	PokreniMakro;
	rcProgress('Aktiviranje makroa','Zavr�eno!',100,100);
	
End;	

Procedure ButCancel_OnClick(sender:TObject);
Begin
	Main.close
End;

Procedure OdaberiSpajanje;
var
	Select:Integer;
	ttt,Put:string;
	PutDef:string;
Begin
	// print ('Odaberi spajanje start');
	Select:=ComboProizvodjac.ItemIndex;
	Put:=Spath+'Brands\'+ComboProizvodjac.Items[Select]+'\';
	// Print (Put);
	
	// Tra�enje naziva definicije slota u spoju
	ttt:=find_value_str(put+'slotovi.ini',IntToStr(Odabir));
	
	// micanje nastavka '.def' iz naziva da bi normalno izgledalo na ekranu
	LabNaziv.Text:=copy(ttt,1,length(ttt)-4);
	
	PutDef:=Put+LabNaziv.Text+'.def';
	// Print (PutDef);
	If not FileExists(PutDef) then LabNaziv.Text:='';   // Ako nema definicije za odabrani slot
	
	If Length(LabNaziv.Text)<3 then begin 
			LabNaziv.Text:='';
			Opis.text 						:='';
			
			// Sifre: Spojnica, Vijak, Podlozna, Vijci podlozne
			SifraSpojnice.text			:='';
			SifraVijkaSpojnice.text    :='';
			SifraPodloznePlocice.text  :='';
			SifraVijkaPlocice.text     :='';
			
			// Sifra: Ublazivac zatvaranja
			SifraUblazivaca.text       :='';
			
			// Sifra: Kapica
			SifraKapice.text           :='';
			
			// Sifra i kolicina: Extra1
			SifraExtra1.text           :='';
			SifraExtra1Kol.text			:='0';
			
			// Sifra i kolicina: Extra2
			SifraExtra2.text           :='';
			SifraExtra2Kol.text			:='0';
			
			// Spojnica: polozaj
			OdGornjegRuba.text         :='';
			OdDonjegRuba.text          :='';
			MaxRazmak.text             :='';
			
			// Spojnica: rupe
			CasicaOdRuba.text          :='';
			CasicaFi.text              :='';
			CasicaDubina.text          :='';
			CasVijRaz.text             :='';
			CasVijPom.text             :='';
			CasVijDub.text             :='';
			CasVijFi.text              :='';
			
			// podlozna plocica: vrsta 0/1 = krizna/ravna
			VrstaPodlozne.ItemIndex		:=-1

			// podlozna plocica: rupe
			KriznaX.text					:='';
			KriznaRazmak.text          :='';
			RavnaX.text               	:='';
			RavnaRazmak.text           :='';
			PodloznaFi.text            :='';
			PodloznaDubina.text        :='';
			PodloznaRavnaSifra.text		:='';

			// Silikonski ublaziva�: sifra i rupe
			SifraSilUblaz.text			:='';
			RupaSil.ItemIndex				:=-1;
			RupaSilX.text					:='';
			RupaSilGoreY.text				:='';
			RupaSilDoljeY.text			:='';
			RupaSilFi.text					:='';
			RupaSilDubina.text			:='';
			
			Element3D.text             :='';
			MakroFile.text					:='';
			
			// Joint pozicija
			PozicijaObj2.ItemIndex		:=-1;
			
		end else begin
			
			// Sifre: Spojnica, Vijak, Podlozna, Vijci podlozne
			Opis.text 						:=find_value_str(PutDef,'Opis');
			SifraSpojnice.text			:=find_value_str(PutDef,'SifraSpojnice');
			SifraVijkaSpojnice.text    :=find_value_str(PutDef,'SifraVijkaSpojnice');
			SifraPodloznePlocice.text  :=find_value_str(PutDef,'SifraPodloznePlocice');
			SifraVijkaPlocice.text     :=find_value_str(PutDef,'SifraVijkaPlocice');
			
			// Sifra: Ublazivac zatvaranja
			SifraUblazivaca.text       :=find_value_str(PutDef,'SifraUblazivaca');
			
			// Sifra: Kapica
			SifraKapice.text           :=find_value_str(PutDef,'SifraKapice');
			
			// Sifra i kolicina: Extra1			
			SifraExtra1.text           :=find_value_str(PutDef,'SifraExtra1');
			SifraExtra1Kol.text			:=find_value_str(PutDef,'SifraExtra1Kol');
			
			// Sifra i kolicina: Extra2
			SifraExtra2.text           :=find_value_str(PutDef,'SifraExtra2');
			SifraExtra2Kol.text			:=find_value_str(PutDef,'SifraExtra2Kol');
			
			// Spojnica: polozaj
			OdGornjegRuba.text         :=find_value_str(PutDef,'OdGornjegRuba');
			OdDonjegRuba.text          :=find_value_str(PutDef,'OdDonjegRuba');
			MaxRazmak.text             :=find_value_str(PutDef,'MaxRazmak');
			
			// Spojnica: rupe
			CasicaOdRuba.text          :=find_value_str(PutDef,'CasicaOdRuba');
			CasicaFi.text              :=find_value_str(PutDef,'CasicaFi');
			CasicaDubina.text          :=find_value_str(PutDef,'CasicaDubina');
			CasVijRaz.text             :=find_value_str(PutDef,'CasVijRaz');
			CasVijPom.text             :=find_value_str(PutDef,'CasVijPom');
			CasVijDub.text             :=find_value_str(PutDef,'CasVijDub');
			CasVijFi.text              :=find_value_str(PutDef,'CasVijFi');
			
			// podlozna plocica: vrsta 
			VrstaPodlozne.ItemIndex		:=StrToInt(find_value_str(PutDef,'VrstaPodlozne'));
			
			// podlozna plocica: rupe
			KriznaX.text					:=find_value_str(PutDef,'KriznaX');
			KriznaRazmak.text          :=find_value_str(PutDef,'KriznaRazmak');
			RavnaX.text               	:=find_value_str(PutDef,'RavnaX');
			RavnaRazmak.text           :=find_value_str(PutDef,'RavnaRazmak');
			PodloznaFi.text            :=find_value_str(PutDef,'PodloznaFi');
			PodloznaDubina.text        :=find_value_str(PutDef,'PodloznaDubina');
			PodloznaRavnaSifra.text		:=find_value_str(PutDef,'PodloznaRavnaSifra');
			
			// Silikonski ublaziva�: sifra i rupe
			SifraSilUblaz.text			:=find_value_str(PutDef,'SifraSilUblaz');
			RupaSil.ItemIndex				:=StrToInt(find_value_str(PutDef,'RupaSil'));

			RupaSIlX.text					:=find_value_str(PutDef,'RupaSilX');
			RupaSilGoreY.text				:=find_value_str(PutDef,'RupaSilGoreY');
			RupaSilDoljeY.text			:=find_value_str(PutDef,'RupaSilDoljeY');
			RupaSilFi.text					:=find_value_str(PutDef,'RupaSilFi');
			RupaSilDubina.text			:=find_value_str(PutDef,'RupaSilDubina');
			
			Element3D.text             :=find_value_str(PutDef,'Element3D');
			MakroFile.text					:=find_value_str(PutDef,'MakroFile');	
			
			// JOINT polozaj
			PozicijaObj2.ItemIndex		:=StrToInt(find_value_str(PutDef,'PozicijaObj2'));
			o2x								:=StrToInt(find_value_str(PutDef,'o2x'));
			o2y								:=StrToInt(find_value_str(PutDef,'o2y'));
			o2z								:=StrToInt(find_value_str(PutDef,'o2z'));
			
	end;	// If Length(LabNaziv.Text)<3
	
	// Opis.text:=find_value_str(put+,ValueName:string):string;
	// print ('Odaberi spajanje end');
End;

Procedure TestPosible(sender:TObject);

Begin

	 if 		(LabNaziv.text='') 				
			or (OdGornjegRuba.text='') 	
			or (OdDonjegRuba.text='') 		
			or (MaxRazmak.text='') 			
			 
			or (CasicaOdRuba.text='') 		
			or (CasicaFi.text='') 			
			or (CasicaDubina.text='') 		
			or (CasVijRaz.text='') 			
			or (CasVijPom.text='') 			
			or (CasVijDub.text='') 			
			or (CasVijFi.text='') 			
			 
			or (VrstaPodlozne.ItemIndex=-1) 
			or (KriznaX.text='') 				
			or (KriznaRazmak.text='') 				
			or (RavnaX.text='') 				
			or (RavnaRazmak.text='') 	
			or (PodloznaFi.text='') 
			or (PodloznaDubina.text='')
			or (RupaSil.ItemIndex=-1)
			
			or (Length(MakroFile.text)<5) 
			or (PozicijaObj2.ItemIndex=-1) 
				
				then ButOk.Enabled:= false
				else ButOk.Enabled:= true;

End;


Procedure SpojClick (sender:TObject);
var
	i:integer;
Begin
	// Print('SpojClick start');
	if sender is TMImage then odabir:=TMImage(Sender).tag;
	if sender is TPanel then odabir:=TPanel(Sender).tag;
	if sender is TLabel then odabir:=TLabel(Sender).tag;
	
	// Print (IntToStr(Odabir));
	
	For i:=1 to BrojSlotova do begin
		PicSpojON[i].visible:=false;
	End;
	PicSpojON[odabir].visible:=true;
	
	OdaberiSpajanje;
	TestPosible(nil);
End;

Procedure ComboProizvodjacOnChange(Sender:TObject);
Begin
	SpojClick(nil);

End;

	
Procedure DelMakroActive(sender:TObject);
var
	te : tElement;
	td : tDaska;
	i,j : integer;
	ss : string;
Begin
	// print ('DelMakroActive start');
	

	
	if Length(MakroFile.text)<5 then begin
		Showmessage('Ne postoji aktivni makro. Odaberite funkcionalni spoj.');
		Exit;
	end;
	
	if rcMessage('Brisanje makroa', '�elite li selektiranom elementu ili dasci obrisati aktivni spoj?',
					 'Nisam siguran', 'Obri�i') then begin
			// Print('Da');
			
			// ako je unutar elementa
				
			te:=nil;
			for i:=0 to e.ElmList.CountObj-1 do Begin     
				te:=e.ElmList.Element[i];
				if (te.selected) then begin	
					td:=nil;
					for j:=0 to te.childdaska.CountObj-1 do Begin									// brojim daske u njemu
						td:=te.childdaska.Daska[j];														//jedna daska iz elementa (s rednim brojem i)
						if (td.tipDaske in [Fronta]) and (td.visible) then begin
							// te.DeleteSpoj(MakroFile.text,td);
							ss:=Copy(MakroFile.text,1,Length(MakroFile.text)-4);   // mi�e se '.cmk'
							te.DeleteSpoj(ss,td);
							// te.DeleteSpoj(MakroFile.text,td);
							print ('Obrisao sam makro "'+ss+'" u dasci "'+td.naziv+'" u elementu "'+te.naziv+'"');
						end;
						te.recalcformula(te);
						application.processmessages;	
					end;
				end;
			end;
			
			// ako je direkto selektiran
	
			for j:=0 to e.childdaska.CountObj-1 do Begin									// brojim daske u njemu
				td:=e.childdaska.Daska[j];														//jedna daska iz elementa (s rednim brojem i)
				if (td.tipDaske in [Fronta]) and (td.visible) and td.selected then begin
					// te.DeleteSpoj(MakroFile.text,td);
					ss:=Copy(MakroFile.text,1,Length(MakroFile.text)-4);   // mi�e se '.cmk'
					e.DeleteSpoj(ss,td);
					print ('Obrisao sam makro "'+ss+'" u dasci "'+td.naziv+'"');
				end;
				te.recalcformula(te);
				application.processmessages;	
			end;
			
			
		end else begin
			// Print('Ne');		 
	End;
	
	
	
	
	// print ('DelMakroActive end');
End;	

Procedure DelMakroAll(sender:TObject);
var
	te : tElement;
	td : tDaska;
	i,j : integer;
	ss : string;
Begin
	// print ('DelMakroAll All');
	
	
	
	if rcMessage('Brisanje makroa', '�elite li selektiranom elementu ili dasci obrisati sve spojeve?',
					 'Nisam siguran', 'Obri�i') then begin
			// Print('Da');
			
			// ako je unutar elementa
			
			te:=nil;
			for i:=0 to e.ElmList.CountObj-1 do Begin     
				te:=e.ElmList.Element[i];
				if (te.selected) then begin	
					td:=nil;
					for j:=0 to te.childdaska.CountObj-1 do Begin									// brojim daske u njemu
						td:=te.childdaska.Daska[j];														//jedna daska iz elementa (s rednim brojem i)
						if (td.tipDaske in [Fronta]) and (td.visible) then begin
							// te.DeleteSpoj(MakroFile.text,td);
							//ss:=Copy(MakroFile.text,1,Length(MakroFile.text)-4);   // mi�e se '.cmk'
							te.DeleteSpoj('',td);
							// te.DeleteSpoj(MakroFile.text,td);
							print ('Obrisao sam makro "'+ss+'" u dasci "'+td.naziv+'" u elementu "'+te.naziv+'"');
						end;
						te.recalcformula(te);
						application.processmessages;	
					end;
				end;
			end;
			
			
			
			// ako je direkto selektiran
	
			for j:=0 to e.childdaska.CountObj-1 do Begin									// brojim daske u njemu
				td:=e.childdaska.Daska[j];														//jedna daska iz elementa (s rednim brojem i)
				if (td.tipDaske in [Fronta]) and (td.visible) and td.selected then begin
					// te.DeleteSpoj(MakroFile.text,td);
					// ss:=Copy(MakroFile.text,1,Length(MakroFile.text)-4);   // mi�e se '.cmk'
					e.DeleteSpoj('',td);
					print ('Obrisao sam makro "'+ss+'" u dasci "'+td.naziv+'"');
				end;
				te.recalcformula(te);
				application.processmessages;	
			end;
	
			
		end else begin
			// Print('Ne');		 
	End;  // if rcMessage('Brisanje makroa
	
	
	
	// print ('DelMakroAll end');
End;	

Procedure napuni_polja(PutDef:string);
var
	ss1,ss2:string;
Begin
			ss1:=rcExtractName(PutDef);			// micanje foldera
			ss2:=copy(ss1,1,Length(ss1)-4);		// micanje extenzije
			
			// naslov
			LabNaziv.text					:=ss2;
			Opis.text 						:=find_value_str(PutDef,'Opis');
			
			// Sifre: Spojnica, Vijak, Podlozna, Vijci podlozne
			SifraSpojnice.text			:=find_value_str(PutDef,'SifraSpojnice');
			SifraVijkaSpojnice.text    :=find_value_str(PutDef,'SifraVijkaSpojnice');
			SifraPodloznePlocice.text  :=find_value_str(PutDef,'SifraPodloznePlocice');
			SifraVijkaPlocice.text     :=find_value_str(PutDef,'SifraVijkaPlocice');
			
			// Sifra: Ublazivac zatvaranja
			SifraUblazivaca.text       :=find_value_str(PutDef,'SifraUblazivaca');
			
			// Sifra: Kapica
			SifraKapice.text           :=find_value_str(PutDef,'SifraKapice');
			
			// Sifra i kolicina: Extra1
			SifraExtra1.text           :=find_value_str(PutDef,'SifraExtra1');
			SifraExtra1Kol.text			:=find_value_str(PutDef,'SifraExtra1Kol');
			
			// Sifra i kolicina: Extra2
			SifraExtra2.text           :=find_value_str(PutDef,'SifraExtra2');
			SifraExtra2Kol.text			:=find_value_str(PutDef,'SifraExtra2Kol');
			
			// Spojnica: polozaj
			OdGornjegRuba.text         :=find_value_str(PutDef,'OdGornjegRuba');
			OdDonjegRuba.text          :=find_value_str(PutDef,'OdDonjegRuba');
			MaxRazmak.text             :=find_value_str(PutDef,'MaxRazmak');
			
			// Spojnica: rupe
			CasicaOdRuba.text          :=find_value_str(PutDef,'CasicaOdRuba');
			CasicaFi.text              :=find_value_str(PutDef,'CasicaFi');
			CasicaDubina.text          :=find_value_str(PutDef,'CasicaDubina');
			CasVijRaz.text             :=find_value_str(PutDef,'CasVijRaz');
			CasVijPom.text             :=find_value_str(PutDef,'CasVijPom');
			CasVijDub.text             :=find_value_str(PutDef,'CasVijDub');
			CasVijFi.text              :=find_value_str(PutDef,'CasVijFi');
			
			// podlozna plocica: vrsta 0/1 = krizna/ravna
			VrstaPodlozne.ItemIndex		:=StrToInt(find_value_str(PutDef,'VrstaPodlozne'));
			
			// podlozna plocica: rupe
			KriznaX.text               :=find_value_str(PutDef,'KriznaX');
			KriznaRazmak.text          :=find_value_str(PutDef,'KriznaRazmak');
			RavnaX.text						:=find_value_str(PutDef,'RavnaX');
			RavnaRazmak.text				:=find_value_str(PutDef,'RavnaRazmak');
			PodloznaFi.text				:=find_value_str(PutDef,'PodloznaFi');
			PodloznaDubina.text			:=find_value_str(PutDef,'PodloznaDubina');
			PodloznaRavnaSifra.text		:=find_value_str(PutDef,'PodloznaRavnaSifra');
			
			// Silikonski ublaziva�: sifra i rupe
			SifraSilUblaz.text			:=find_value_str(PutDef,'SifraSilUblaz');
			RupaSil.ItemIndex				:=StrToInt(find_value_str(PutDef,'RupaSil'));
			RupaSilX.text					:=find_value_str(PutDef,'RupaSilX');
			RupaSilGoreY.text				:=find_value_str(PutDef,'RupaSilGoreY');
			RupaSilDoljeY.text			:=find_value_str(PutDef,'RupaSilDoljeY');
			RupaSilFi.text					:=find_value_str(PutDef,'RupaSilFi');
			RupaSilDubina.text			:=find_value_str(PutDef,'RupaSilDubina');
			
			Element3D.text             :=find_value_str(PutDef,'Element3D');
			MakroFile.text					:=find_value_str(PutDef,'MakroFile');	
			
			// JOINT polozaj
			PozicijaObj2.ItemIndex		:=StrToInt(find_value_str(PutDef,'PozicijaObj2'));
			o2x								:=StrToInt(find_value_str(PutDef,'o2x'));
			o2y								:=StrToInt(find_value_str(PutDef,'o2y'));
			o2z								:=StrToInt(find_value_str(PutDef,'o2z'));
			

End;

Procedure ButOdaberi_OnClick(sender: TObject);
var
	dijalog:TOpenDialog;
	novi:string;
	Select:Integer;
	PutDef, put :string;
Begin
	// Print ('ButOdaberi_OnClick Start');
	Select:=ComboProizvodjac.ItemIndex;
	Put:=Spath+'Brands\'+ComboProizvodjac.Items[Select]+'\';
	// print('putanja: '+put);
	
	dijalog := TOpenDialog.Create(nil);
	dijalog.InitialDir := Put;
	dijalog.Title:='Odaberi definiciju spoja';
	dijalog.Filter := 'Definicija spoja (*.def)|*.def';
	
	Main.FormStyle:=fsNormal;
	
	if dijalog.Execute then begin
		novi := dijalog.FileName
		// Print('Odabrano: "'+novi+'"');
		Napuni_polja(novi);
	end;
	
	Main.FormStyle:=fsStayOnTop;
	
	// Print ('ButOdaberi_OnClick End');
End;

Procedure SifraOnClick(Sender: TObject);

Begin
	// print('SPOC '+sifraSpojnice.text);
	if tButton(sender).tag=101 then SifraSpojnice.Text				:=UcitajPotrosni(SifraSpojnice.text,main.Left+main.width,main.top);
	if tButton(sender).tag=102 then SifraVijkaSpojnice.Text		:=UcitajPotrosni(SifraVijkaSpojnice.text,main.Left+main.width,main.top);
	if tButton(sender).tag=103 then SifraPodloznePlocice.Text	:=UcitajPotrosni(SifraPodloznePlocice.text,main.Left+main.width,main.top);
	if tButton(sender).tag=104 then SifraVijkaPlocice.Text		:=UcitajPotrosni(SifraVijkaPlocice.text,main.Left+main.width,main.top);
	if tButton(sender).tag=105 then SifraUblazivaca.Text			:=UcitajPotrosni(SifraUblazivaca.text,main.Left+main.width,main.top);
	if tButton(sender).tag=106 then SifraKapice.Text				:=UcitajPotrosni(SifraKapice.text,main.Left+main.width,main.top);
	if tButton(sender).tag=107 then SifraExtra1.Text				:=UcitajPotrosni(SifraExtra1.text,main.Left+main.width,main.top);
	if tButton(sender).tag=108 then SifraExtra2.Text				:=UcitajPotrosni(SifraExtra2.text,main.Left+main.width,main.top);
	if tButton(sender).tag=109 then PodloznaRavnaSifra.Text		:=UcitajPotrosni(PodloznaRavnaSifra.text,main.Left+main.width,main.top);
	if tButton(sender).tag=110 then SifraSilUblaz.Text				:=UcitajPotrosni(SifraSilUblaz.text,main.Left+main.width,main.top);

	
End;
	
Procedure Create_Main_Window;
var 

	i:Integer;
Begin
	// Print('Create_Main_Window start');


	// Startni prozor
   Main:=rcTool('Dodavanje_spojnica'); 
	Main.Width:=500;
	Main.Height:=500;
 	Main.top:=100;
	Main.Left:=120;
	// Main.BorderStyle:=bsSizeToolWin;
	Main.BorderIcons:=[biSystemMenu];
	// Main.FormStyle:=fsStayOnTop;	

	// Panel Down
	PanDown:=rcPanel('Pan_Down',Main);
	PanDown.Align:=alBottom;
	PanDown.Height:=40;
	
	ButOK:=rcButton('btok','Napravi',PanDown,main.width-120,8);
	ButOK.Width:=100;
	ButOK.OnClick:=@ButOK_OnClick;
	ButOK.enabled:=false;
	ButOk.Hint:='Treba popuniti obavezna polja (ozna�ena �uto)';	
	
	ButCancel:=rcButton('btCancel','Zatvori',PanDown,main.width-240,8);
	ButCancel.Width:=100;
	ButCancel.OnClick:=@ButCancel_OnClick;
	
	ButPomoc:=rcButton('btPomoc','Pomo�',PanDown,main.width-360,8);
	ButPomoc.Width:=100;
	ButPomoc.enabled:=False;
	// ButPomoc.OnClick:=@ButPomoc_OnClick;
	
	ButOK.default:=True;
	ButCancel.ModalResult:=mrCancel;
	
	// PanMain
	PanMain:=rcPanel('PanMain',Main);
	PanMain.Align:=alClient;
	
		// PanLeft
		PanLeft:=rcPanel('PanLeft',PanMain);
		PanLeft.Align:=alLeft;
		PanLeft.Width:=150;
	
		// PanLeftHeader
		PanLeftHeader:=rcPanel('PanLeftHeader',PanLeft);
		PanLeftHeader.Align:=alTop;
		PanLeftHeader.Height:=15;
		
			// PanLeftTitle
			LabLeftTitle:=rcLabel( '', 'VRSTA SPOJA',PanLeftHeader, 4, 1, taLeftJustify);
			LabLeftTitle.Font.Style:=[fsBold];
		
		// PanLeftFooter
		PanLeftFooter:=rcPanel('PanLeftFooter',PanLeft);
		PanLeftFooter.Align:=alBottom;
		PanLeftFooter.Height:=75;
			
			// Tipke za brisanje makroa
			ButDelMakroActive:=rcButton('btdeldaska','Ukloni aktivni spoj',PanLeftFooter,5,8);
			ButDelMakroActive.Width:=135;
			ButDelMakroActive.OnClick:=@DelMakroActive;
			
			ButDelMakroAll:=rcButton('btdelelem','Ukloni sve spojeve',PanLeftFooter,5,38);
			ButDelMakroAll.Width:=135;
			ButDelMakroAll.OnClick:=@DelMakroAll;
			
		
		// ScrollLeft
		ScrollLeft:=rcScrollBox('ScrollLeft',PanLeft);
		ScrollLeft.Align:=alClient;
		ScrollLeft.VertScrollBar.visible:=True;
		ScrollLeft.VertScrollBar.Tracking:=True;
		
		
			SetLength(PanSpoj,BrojSlotova+1);
			SetLength(LabSpoj,BrojSlotova+1);
			SetLength(PicSpojON,BrojSlotova+1);
			SetLength(PicSpojOFF,BrojSlotova+1);
			SetLength(LabRB,BrojSlotova+1);
			
			for i:=1 to BrojSlotova do begin 
				// print('i='+IntToStr(i));
				PanSpoj[i]:=rcPanel('',ScrollLeft);
				// PanSpoj[i].width:=68;
				PanSpoj[i].Height:=84;
				PanSpoj[i].Top:=10000;
				PanSpoj[i].Align:=alTop;
				
			
			
				// SLike
				
				// Putanja_slike:=spath+'Slike\';
				// Print('Putanja_slike: '+Putanja_slike);
				Naziv_slike:=find_value_str(sPath+'slike.ini',IntToStr(i));
				// Print('Naziv_slike: '+Naziv_slike);
				// Print('Slot: '+IntToStr(i)+' slika: '+Naziv_slike);
				
				if length(Naziv_slike)>1 then begin
						NazivSlikeOFF:=sPath+'slike\'+Naziv_slike+'_off.bmp';
						// Print(NazivSlikeOFF);
						NazivSlikeON:=sPath+'slike\'+Naziv_slike+'_on.bmp';
						if FileExists(NazivSlikeOFF) then PicSpojOFF[i]:=rcLoadImage('',NazivSlikeOFF ,PanSpoj[i],5,16)
															else 	PicSpojOFF[i]:=rcLoadImage('', sPath+'slike\nema_slike_off.bmp'	, PanSpoj[i] , 5, 16);
						
						if FileExists(NazivSlikeON) then PicSpojON[i]:=rcLoadImage('',NazivSlikeON,PanSpoj[i], 5, 16)
															 else PicSpojON[i]:=rcLoadImage('',sPath+'slike\nema_slike_on.bmp',PanSpoj[i], 5, 16);
						if i=odabir then PicSpojON[i].visible:=true
										else PicSpojON[i].visible:=false;
						
					End else begin
						PicSpojON[i]:=rcLoadImage('',sPath+'slike\prazno.bmp',PanSpoj[i], 5, 16);
						PicSpojOFF[i]:=rcLoadImage('',sPath+'slike\prazno.bmp',PanSpoj[i], 5, 16);
				End;

				
				// Nazivi
				
				// Print (IntToStr(i));
				
				LabSpoj[i]:=rcLabel( '','*', PanSpoj[i], 5, 1, taLeftJustify);
				LabSpoj[i].caption:=find_value_str(spath+'LNG\'+jezik+'\nazivi.ini',IntToStr(i));
				// LabSpoj[i].Alignment:=taCenter;
				
				LabRB[i]:=rcLabel( '',IntToStr(i), PanSpoj[i], 80, 32, taLeftJustify);
				LabRB[i].font.size:=20;
				LabRB[i].font.color:=clSilver;
				LabRB[i].font.style:=[fsBold];
				
				
				// TAGOVI
				PanSpoj[i].Tag:=i;
				
				LabSpoj[i].Tag:=i;
				PicSpojON[i].Tag:=i;
				PicSpojOFF[i].Tag:=i;
				LabRB[i].Tag:=i;
				
				// EVENTI
				PanSpoj[i].OnClick:=@SpojClick;
				LabSpoj[i].OnClick:=@SpojClick;
				PicSpojON[i].OnClick:=@SpojClick;
				PicSpojOFF[i].OnClick:=@SpojClick;
				LabRB[i].OnClick:=@SpojClick;
				
				
				
			End; // for i:=1 to BrojSlotova
	
	// PanRight
	PanRight:=rcPanel('PanRight',PanMain);
	PanRight.Align:=alClient;
	

		// ComboProizvodjaci
		
		LabBrand:=rcLabel( '','Grupa', PanRight, PanRight.Width-205, 14, taRightJustify);
		LabBrand.Font.Size:=9;
		
		
		ComboProizvodjac:=rcComboBox(PanRight, PanRight.Width-200, 10);
		
		// ComboProizvodjac.font.size:=15;
		// ComboProizvodjac.AutoSize:=True;
		// ComboProizvodjac.Height:=50;
		ComboProizvodjac.ItemHeight:=18;
		ComboProizvodjac.Font.Size:=10;
		ComboProizvodjac.Font.Style:=[fsBold];
		
		// ComboProizvodjac.Items.LoadFromFile(SPath+'Proizvodjaci.ini');
		// Print ('tra�im brandove');
		ComboProizvodjac.Items.Assign(rcDirDirs(spath+'Brands\'));
		// ComboProizvodjac.Items.Add('Titus Plus T-Type Glissando');
		ComboProizvodjac.Width:=PanRight.Width-105;;
		
		brand:=find_value_str(sPath+'user.ini','brand');
		ComboProizvodjac.ItemIndex:=0;
		
		ComboProizvodjac.Items[1]:='Nesto';
		for i:=0 to ComboProizvodjac.DropDownCount-1 do begin
			if ComboProizvodjac.Items[i]=brand then ComboProizvodjac.ItemIndex:=i;
		End;
		
		
		// Predlo�ak / definicija
		LabNaziv:=	rcEditL('Predlozak','',PanRight,PanRight.Width-189, 40, 'right');
		LabNaziv.Text:='';
		LabNaziv.Width:=PanRight.Width-140;
		LabNaziv.Color:=clInfoBK;
		
		ButOdaberi:=rcButton('', '...', PanRight, PanRight.Width-35, 40);
		ButOdaberi.width:=30;
		ButOdaberi.height:=23;
		ButOdaberi.OnClick:=@ButOdaberi_OnClick;
		
		// EVENT
		ComboProizvodjac.OnChange:=@ComboProizvodjacOnChange;
		
		
		// ScrollRight
		ScrollRight:=rcScrollBox('ScrollRight',PanRight);
		// ScrollRight.Align:=alClient;
		ScrollRight.VertScrollBar.visible:=True;
		ScrollRight.HorzScrollBar.visible:=False;
		ScrollRight.VertScrollBar.Tracking:=True;
		ScrollRight.Left:=0;
		ScrollRight.Top:=70;
		ScrollRight.Width:=PanRight.Width-0;
		ScrollRight.Height:=PanRight.Height-75;
	
			// Labele
			//rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
			Naslov:=rcLabel('','Opis',ScrollRight,5,2,taLeftJustify);
			Opis:=rcMemo('', ScrollRight, 4, 20, ScrollRight.Width-27, 38);
			
			Crta:=rcBox('',ScrollRight,4,65,ScrollRight.Width-27,1);
			Naslov:=rcLabel('','Potro�ni materijal',ScrollRight,5,70,taLeftJustify);
	
			// Sifra Spojnice
			
			SifraSpojnice:=		rcEditL('�ifra spojnice','',ScrollRight,ScrollRight.Width-115, 85, 'right');
			SifraSpojnice.width:=ScrollRight.Width-222;
			
			butSifraSpojnice:= rcButton('', '...', ScrollRight, ScrollRight.Width-45, 84);
			butSifraSpojnice.Width:=23;
			butSifraSpojnice.OnClick:=@SifraOnClick;
			butSifraSpojnice.Tag:=101;
			
			// Sifra vijka spojnice
			
			SifraVijkaSpojnice:=	rcEditL('�ifra vijaka spojnice','',ScrollRight,ScrollRight.Width-115, 110, 'right');
			SifraVijkaSpojnice.width:=ScrollRight.Width-222;
			
			butSifraVijkaSpojnice:= rcButton('', '...', ScrollRight, ScrollRight.Width-45, 109);
			butSifraVijkaSpojnice.Width:=23;
			butSifraVijkaSpojnice.Tag:=102;
			butSifraVijkaSpojnice.OnClick:=@SifraOnClick;
			
			// Sifra podlo�ne plo�ice
			
			SifraPodloznePlocice:=rcEditL('�ifra kri�ne pod. plo�ice','',ScrollRight,ScrollRight.Width-115, 135, 'right');
			SifraPodloznePlocice.width:=ScrollRight.Width-222;
			
			butSifraPodloznePlocice:= rcButton('', '...', ScrollRight, ScrollRight.Width-45, 134);
			butSifraPodloznePlocice.Width:=23;
			butSifraPodloznePlocice.Tag:=103;
			butSifraPodloznePlocice.OnClick:=@SifraOnClick;
			
			// Sifra vijka podlozne plo�ice
			
			SifraVijkaPlocice:=	rcEditL('�ifra vijaka podlo�ne plo�ice','',ScrollRight,ScrollRight.Width-115, 160, 'right');
			SifraVijkaPlocice.width:=ScrollRight.Width-222;
			
			butSifraVijkaPlocice:= rcButton('', '...', ScrollRight, ScrollRight.Width-45, 159);
			butSifraVijkaPlocice.Width:=23;
			butSifraVijkaPlocice.Tag:=104;
			butSifraVijkaPlocice.OnClick:=@SifraOnClick;
			
			// Sifra: Ublazivac zatvaranja
	
			SifraUblazivaca:=		rcEditL('�ifra ubla�iva�a zatvaranja','',ScrollRight,ScrollRight.Width-115, 185, 'right');
			SifraUblazivaca.width:=ScrollRight.Width-222;
			
			butSifraUblazivaca:= rcButton('', '...', ScrollRight, ScrollRight.Width-45, 184);
			butSifraUblazivaca.Width:=23;
			butSifraUblazivaca.Tag:=105;
			butSifraUblazivaca.OnClick:=@SifraOnClick;
			
			// Sifra: Kapica
			
			SifraKapice:=			rcEditL('�ifra kapice','',ScrollRight,ScrollRight.Width-115, 210, 'right');
			SifraKapice.width:=ScrollRight.Width-222;
			butSifraKapice:= rcButton('', '...', ScrollRight, ScrollRight.Width-45, 209);
			butSifraKapice.Width:=23;
			butSifraKapice.Tag:=106;
			butSifraKapice.OnClick:=@SifraOnClick;
			
			
			// Sifra i kolicina: Extra1
			
			SifraExtra1:=			rcEditL('�ifra 7','',ScrollRight,ScrollRight.Width-180, 240, 'right');
			SifraExtra1.width:=120;
			butSifraExtra1:= rcButton('', '...', ScrollRight, ScrollRight.Width-110, 239);
			butSifraExtra1.Width:=23;
			butSifraExtra1.Tag:=107;
			butSifraExtra1.OnClick:=@SifraOnClick;
			
			SifraExtra1Kol:=rcEditL('Kol:','',ScrollRight,ScrollRight.Width, 240, 'right');
			SifraExtra1Kol.width:=30;
			
			// Sifra i kolicina: Extra2
			
			SifraExtra2:=			rcEditL('�ifra 8','',ScrollRight,ScrollRight.Width-180, 265, 'right');
			SifraExtra2.width:=120;
			butSifraExtra2:= rcButton('', '...', ScrollRight, ScrollRight.Width-110, 264);
			butSifraExtra2.Width:=23;
			butSifraExtra2.Tag:=108;
			butSifraExtra2.OnClick:=@SifraOnClick;
			
			SifraExtra2Kol:=rcEditL('Kol:','',ScrollRight,ScrollRight.Width, 265, 'right');
			SifraExtra2Kol.width:=30;

			// Spojnica: polozaj
			
			Crta:=rcBox('',ScrollRight,4,295,ScrollRight.Width-27,1);
			Naslov:=rcLabel('','Polo�aj',ScrollRight,5,300,taLeftJustify);
			
			OdGornjegRuba:=		rcEditL('Od gornjeg ruba','',ScrollRight,ScrollRight.Width-20, 315, 'right');
			OdGornjegRuba.Color:=clInfoBK;
			OdDonjegRuba:=			rcEditL('Od donjeg ruba','',ScrollRight,ScrollRight.Width-20, 340, 'right');
			OdDonjegRuba.Color:=clInfoBK;
			MaxRazmak:=				rcEditL('Maximalni razmak','',ScrollRight,ScrollRight.Width-20, 365, 'right');
			MaxRazmak.Color:=clInfoBK;
			
			// Spojnica: rupe
			
			Crta:=rcBox('',ScrollRight,4,395,ScrollRight.Width-27,1);
			Naslov:=rcLabel('','Pozicije za bu�enje - Spojnica',ScrollRight,5,400,taLeftJustify);
			
			CasicaOdRuba:=			rcEditL('�a�ica - od ruba','',ScrollRight,ScrollRight.Width-20, 415, 'right');
			CasicaFi:=				rcEditL('�a�ica - promjer','',ScrollRight,ScrollRight.Width-20, 440, 'right');
			CasicaDubina:=			rcEditL('�a�ica - dubina','',ScrollRight,ScrollRight.Width-20, 465, 'right');
			CasVijRaz:=				rcEditL('Vijci - razmak','',ScrollRight,ScrollRight.Width-20, 490, 'right');
			CasVijPom:=				rcEditL('Vijci - pomak','',ScrollRight,ScrollRight.Width-20, 515, 'right');
			CasVijDub:=				rcEditL('Vijci - dubina','',ScrollRight,ScrollRight.Width-20, 540, 'right');
			CasVijFi:=				rcEditL('Vijci - promjer','',ScrollRight,ScrollRight.Width-20, 565, 'right');
	
			// podlozna plocica
	
			Crta:=rcBox('',ScrollRight,4,595,ScrollRight.Width-27,1);
			Naslov:=rcLabel('','Pozicije za bu�enje - Podlo�na plo�ica',ScrollRight,5,600,taLeftJustify);
			
			
			// podlozna plocica: vrsta 0/1 = krizna/ravna
			LabPodlozna:=			rcLabel( '','Podlozna plo�ica', ScrollRight, ScrollRight.Width-145, 617, taRightJustify);
			LabPodlozna.Font.Size:=9;
			VrstaPodlozne:=		rcComboBox( ScrollRight, ScrollRight.Width-140, 615 );
			VrstaPodlozne.ItemHeight:=16;
			VrstaPodlozne.Width:=ScrollRight.Width-225;
			VrstaPodlozne.Items.Add('Kri�na');				// 0
			VrstaPodlozne.Items.Add('Ravna');				// 1
			VrstaPodlozne.ItemIndex:=0;
			VrstaPodlozne.Color:=clInfoBK;
			
			// podlozna plocica: rupe
			KriznaX:=					rcEditL('Krizna pod. pl. - X rupa','',ScrollRight,ScrollRight.Width-20, 640, 'right');
			KriznaX.Color:=clInfoBK;
			KriznaRazmak:=				rcEditL('Kri�na pod. pl. - Razmak rupa','',ScrollRight,ScrollRight.Width-20, 665, 'right');
			KriznaRazmak.Color:=clInfoBK;
			RavnaX:=						rcEditL('Ravna pod. pl. - X bli�e rupe','',ScrollRight,ScrollRight.Width-20, 690, 'right');
			RavnaX.Color:=clInfoBK;
			RavnaRazmak:=				rcEditL('Ravna pod. pl. - Razmak rupa','',ScrollRight,ScrollRight.Width-20, 715, 'right');
			RavnaRazmak.Color:=clInfoBK;
			PodloznaFi:=				rcEditL('Promjer rupa','',ScrollRight,ScrollRight.Width-20, 740, 'right');
			PodloznaFi.Color:=clInfoBK;
			PodloznaDubina:=			rcEditL('Dubina rupa','',ScrollRight,ScrollRight.Width-20, 765, 'right');
			PodloznaDubina.Color:=clInfoBK;
			PodloznaRavnaSifra:=	rcEditL('�ifra ravne pod. plo�ice','',ScrollRight,ScrollRight.Width-115, 790, 'right');
			PodloznaRavnaSifra.width:=ScrollRight.Width-222;
			
			butPodloznaRavnaSifra:= rcButton('', '...', ScrollRight, ScrollRight.Width-45, 789);
			butPodloznaRavnaSifra.Width:=23;
			butPodloznaRavnaSifra.Tag:=109;
			butPodloznaRavnaSifra.OnClick:=@SifraOnClick;
			
			// Silikonski ublaziva�: sifra i rupe
			
			Crta:=rcBox('',ScrollRight,4,820,ScrollRight.Width-27,1);
			Naslov:=rcLabel('','Silikonski ubla�iva� ',ScrollRight,5,825,taLeftJustify);
			
			SifraSilUblaz:=		rcEditL('�ifra','',ScrollRight,ScrollRight.Width-115, 840, 'right');
			SifraSilUblaz.width:=ScrollRight.Width-222;
			butSifraSilUblaz:= rcButton('', '...', ScrollRight, ScrollRight.Width-45, 839);
			butSifraSilUblaz.Width:=23;
			butSifraSilUblaz.Tag:=110;
			butSifraSilUblaz.OnClick:=@SifraOnClick;
			
			
			
			// RupaSil:=				rcEditL('Bu�iti rupe? (1/0)','',ScrollRight,ScrollRight.Width-20, 865, 'right');
			LabRupaSIl:=	rcLabel( '','Busi Rupu', ScrollRight, ScrollRight.Width-78, 868, taRightJustify);
			LabRupaSIl.font.size:=9;
			
			RupaSil:=				rcComboBox( ScrollRight, ScrollRight.Width-73, 865 );
			RupaSil.Items.Add('NE');			// 0
			RupaSil.Items.Add('DA');			// 1
			RupaSil.ItemHeight:=16;
			RupaSil.Width:=50;
			RupaSIl.color:=clinfoBK;
			
			RupaSilX:=				rcEditL('Udaljenost od bo�nog ruba','',ScrollRight,ScrollRight.Width-20, 890, 'right');
			
			
			
			
			RupaSilGoreY:=			rcEditL('Udaljenost od gornjeg ruba','',ScrollRight,ScrollRight.Width-20, 915, 'right');
			RupaSilDoljeY:=		rcEditL('Udaljenost od donjeg ruba','',ScrollRight,ScrollRight.Width-20, 940, 'right');
			RupaSilFi:=				rcEditL('Promjer rupe','',ScrollRight,ScrollRight.Width-20, 965, 'right');
			RupaSilDubina:=		rcEditL('Dubina rupe','',ScrollRight,ScrollRight.Width-20, 990, 'right');

			// vizualizacija
			Crta:=rcBox('',ScrollRight,4,1020,ScrollRight.Width-27,1);
			Naslov:=rcLabel('','Vizualizacija',ScrollRight,5,1025,taLeftJustify);

			Element3D:=	rcEditL('Element','',ScrollRight,ScrollRight.Width-160-10, 1040, 'right');
			Element3D.width:=ScrollRight.Width-180;
			Element3D.enabled:=false;
			
			ButOdaberiElem:=rcButton('', '...', ScrollRight, ScrollRight.Width-55, 1040);
			ButOdaberiElem.width:=30;
			ButOdaberiElem.height:=23;
			//ButOdaberiPredlozak.OnClick:=@ButOdaberiPredlozak_OnClick;
			ButOdaberiElem.enabled:=false;
			
			
			Crta:=rcBox('',ScrollRight,4,1070,ScrollRight.Width-27,1);
			Naslov:=rcLabel('','Makro',ScrollRight,5,1075,taLeftJustify);
	
			MakroFile:=				rcEditL('Datoteka','',ScrollRight,ScrollRight.Width-187, 1090, 'right');
			MakroFile.Width:=		ScrollRight.Width-125;
			MakroFile.Color:=clInfoBK;
			
			// JOINT polozaj
	
			LabPozO2:=			rcLabel( '','Polo�aj Objekta 2', ScrollRight, ScrollRight.Width-145, 1117, taRightJustify);
			LabPozO2.Font.Size:=9;
			PozicijaObj2:=			rcComboBox( ScrollRight, ScrollRight.Width-140, 1115 );
			PozicijaObj2.Color:=clInfoBK;
	//		
	//		// PozicijaObj2.Font.Size:=8;
	//		// PozicijaObj2.Font.Height:=20;
			PozicijaObj2.ItemHeight:=16;
			PozicijaObj2.Width:=ScrollRight.Width-225;
			PozicijaObj2.Items.Add('Iza');					// 0
			PozicijaObj2.Items.Add('Bo�no');					// 1
			PozicijaObj2.Items.Add('Dijagonalno');			// 2
			PozicijaObj2.Items.Add('Preklop');				// 3

			
			PozicijaObj2.ItemIndex:=0;
			
		
	// GLOBAL EVENTS
	LabNaziv.OnChange					:= 	@TestPosible;
	Opis.OnChange						:= 	@TestPosible;
				
	// Sifre: Spojnica, Vijak, Podlozna, Vijci podlozne
	SifraSpojnice.OnChange			:= 	@TestPosible;
	SifraVijkaSpojnice.OnChange	:= 	@TestPosible;
	SifraPodloznePlocice.OnChange	:= 	@TestPosible;
	SifraVijkaPlocice.OnChange		:= 	@TestPosible;
	                                 
	// Sifra: Ublazivac zatvaranja   
	SifraUblazivaca.OnChange		:= 	@TestPosible;
	                             
	// Sifra: Kapica             
	SifraKapice.OnChange				:= 	@TestPosible;
	                               
	// Sifra i kolicina: Extra1    
	SifraExtra1.OnChange				:= 	@TestPosible;
	SifraExtra1Kol.OnChange			:= 	@TestPosible;
                                
	// Sifra i kolicina: Extra2  
	SifraExtra2.OnChange				:= 	@TestPosible;
	SifraExtra2Kol.OnChange			:= 	@TestPosible;
	                                   
	// Spojnica: polozaj               
	OdGornjegRuba.OnChange			:= 	@TestPosible;
	OdDonjegRuba.OnChange			:= 	@TestPosible;
	MaxRazmak.OnChange				:= 	@TestPosible;
	                                    
	// Spojnica: rupe                   
	CasicaOdRuba.OnChange			:= 	@TestPosible;
	CasicaFi.OnChange					:= 	@TestPosible;
	CasicaDubina.OnChange			:= 	@TestPosible;
	CasVijRaz.OnChange				:= 	@TestPosible;
	CasVijPom.OnChange				:= 	@TestPosible;
	CasVijDub.OnChange				:= 	@TestPosible;
	CasVijFi.OnChange					:= 	@TestPosible;
	
	// podlozna plocica: vrsta 0/1 = krizna/ravna
	VrstaPodlozne.OnChange			:= 	@TestPosible;

	// podlozna plocica: rupe 
	KriznaX.OnChange					:= 	@TestPosible;
	KriznaRazmak.OnChange			:= 	@TestPosible;
	RavnaX.OnChange					:= 	@TestPosible;
	RavnaRazmak.OnChange				:= 	@TestPosible;
	PodloznaFi.OnChange				:= 	@TestPosible;
	PodloznaDubina.OnChange			:= 	@TestPosible;

	// Silikonski ublaziva�: sifra i rupe TestPosible
	SifraSilUblaz.OnChange			:= 	@TestPosible;
	RupaSil.OnChange					:= 	@TestPosible;
	RupaSilX.OnChange					:= 	@TestPosible;
	RupaSilGoreY.OnChange			:= 	@TestPosible;
	RupaSilDoljeY.OnChange			:= 	@TestPosible;
	RupaSilFi.OnChange				:= 	@TestPosible;
	RupaSilDubina.OnChange			:= 	@TestPosible;

	Element3D.OnChange				:= 	@TestPosible;
	
	MakroFile.OnChange				:= 	@TestPosible;
	
	
		
	// Print('Create_Main_Window end');

End;

// Glavni   
Begin
	Print ('==============');
	
	// rcCreateFolder(PrgFolder+'Skripte','Dex1101');
	//	exit;
	
	
   versiontxt:='1.1';	
	Print ('==============');
	PrintTime(' Skripta: "Dodavanje spojnica" '+versiontxt+ // #13#10+ 
								 ' start');
	sPath:=PrgFolder+'Skripte\RC_Tools\Spojnice\';
	BrojSlotova:=30;
	obj2Udaljenost:=5;
	
	selektiran:=False;
	
	Jezik := Language;
	odabir:=1;
	
	
	// for ii:=1 to 5 do rcProgress('Naslov prozora','Tekst progresa',ii,5);
	
	// 
	// UcitajPotrosni;	
	
	
	
	
	
	
	
	
	
	
	for ii:=0 to e.ElmList.CountObj-1 do Begin 
		if (e.ElmList.Element[ii].selected) and 
			(e.ElmList.Element[ii].TipElementa in [VRE]) 
			then selektiran:=true;
	End; // for ii 
	
	
	
	// If selektiran=true then begin
			Create_Main_Window;
			SpojClick(nil);
			Main.Caption:='Dodavanje spojnica '+versiontxt;
			Main.Show; 
			Main.FormStyle:=fsStayOnTop; 	
			
			while Main.visible do begin
			   application.processmessages;
			end;	// while Main.visible
			// print('nakon "While Main visible" petlje ');
	
			Main.Close;			// ovo �alje u sFormOnCloseQuery 
	//	End else begin
			// ShowMessage('Nije selektirana niti jedna fronta!');
	// End;  // 
	
	
	

	PrintTime(' Skripta: "Dodavanje spojnica" '+versiontxt+ // #13#10+ 
								 ' end');
	Print('____________________');

   
   
End.