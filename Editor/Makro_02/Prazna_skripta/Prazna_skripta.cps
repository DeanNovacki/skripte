﻿Program Prazna_skripta;   
// Red Cat Ltd 
// v1.0 30.05.2017.

// - 
// - 
// - 

// 
{$I ..\..\..\RC_Tools\Include\rcPrint.cps}
{$I ..\..\..\RC_Tools\Include\rcMessages.cps}
{$I ..\..\..\RC_Tools\Include\rcStrings.cps}
{$I ..\..\..\RC_Tools\Include\rcControls.cps}
{$I ..\..\..\RC_Tools\Include\rcObjects.cps}
{$I ..\..\..\RC_Tools\Include\rcEvents.cps}
{$I ..\..\..\RC_Tools\Include\rcCurves.cps}
{$I ..\..\..\RC_Tools\Include\rcMath.cps}
{$I ..\..\..\RC_Tools\Include\rcConfig.cps}
    

// MAIN ////////////////////////////////////////////////////////////////////////
var
   // Startni prozor
	Main:TForm;
   Panel_Left, Panel_right, Panel_Down, Panel_Title, Panel_Info, Panel_Table, Panel_Memo, Panel_MemoO :TPanel;
   Panel_1, Panel_2, Panel_3, Panel_4 :TPanel;
	Pan_var, Pan_val, Pan_for, Pan_cb, Pan_hint : array of TPanel;
   ButOK,ButPomoc,ButCancel:TButton;
	
	Raster_1_Naslov: TLabel;
	PoljeR1, PoljeR2, PoljeE1, PoljeE2: Array of TEdit;
	NazivR1, NazivR2, NazivE1, NazivE2: Array of TLabel;

	CBR1:TCheckBox;
	
	makro_obj1, makro_obj2, makro1, makro2 :string;		// putanja
	panel_name:TPanel;
	sirpan:Integer;
	
	R1_Lista, R2_lista, E1_lista, E2_Lista: TStringList;
	

	Debug, MozeZatvaranje, Izvrseno :boolean;    // za debug poruke u Journalu

	
	i,Main_start_Width, Main_start_height :Integer;

Procedure Napravi_posao;
Begin
	ShowN('*************************');
	ShowN('Ovdje se izvršava program');
	ShowN('*************************');
	ShowMessage('POSAO');
End;

Procedure Pomoc;
var
	PDF:TpdfPrinter;
Begin
	ShowN('Pomoć');
	PDF:=TpdfPrinter.create(1250);
	// PDF.FileName := 'K:\_Corpus 4 RC dev 3\Corpus 4 RC\skripte\Test_polygon\Japin Corpus\skripte\Ponuda\ponuda.pdf';
   // PDF.FileName := 'https://www.youtube.com/watch?v=YRBH87-SKL4&feature=youtu.be';
	// PDF.FileName := 'K:\Zrc\web\PaleMoonRedCat\Moon RC.exe';
	//	PDF.FileName := prgfolder + 'MaterialEditor_4.1.2.54.exe';
	PDF.FileName := prgfolder + 'skripte\RC_Tools\Gerung\gerung.cps'; // Otvara skriptu u editoru :-)
	PDF.ShowPdf;
	ShowN('PDF.ShowPdf - DONE');
	PDF.Free;
End;

Procedure Call_makro (Makro_obj1, Makro_obj2:string; Panel_dest:Tpanel);
// poziv makroe i stavlja ih u panele;

var
	i,j,k:integer;
	naziv, vrijednost,znakic:string;
	temp_str, redak: string;
	naslov:string;
	
Begin
	//////////////////////////////////////////////////	
	// Puni raster_1
	// Raster_1_Makro1:='..\skripte\Editor\Makro_02\Fronta-Vertikala-1.CMK'
	// - Raster_1_Makro2:='..\skripte\Editor\Makro_02\Fronta-Vertikala-2.CMK'
	// - temp_str:=prgFolder+Copy(Raster_1_Makro1,4,200);
	temp_str:=prgFolder+Copy(Makro_obj1,4,200);
	if not (rcFileCheck(temp_str)) then 
		begin
			ShowMessage ('Nema datoteke:' +#13#10+temp_str+'"'+#13#10+#13#10
							+'Koja je dodijeljena makrou.');
		end else begin
			Makro_obj1:=temp_str;
			// ShowMessage ('Ima datoteke: "'+temp_str+'"');
		end;
	
	// Naslov
	/// function find_value_str(FileName,ValueName:string):string;
	naslov:=find_value_str(temp_str,'GN');
	Raster_1_Naslov:=rcLabel('',naslov,Panel_dest,20,3, taLeftJustify);
	Raster_1_Naslov.font.size:=10;
	Raster_1_Naslov.font.style:=[fsBold];
	// CheckBox
	cbr1:=rcCheckBox('cbr1','',Panel_dest,-80,3);
	
	// učitavanje varijabli iz makroa
	R1_Lista:=TStringList.Create;
	R1_Lista.LoadFromFile(Makro_obj1);
	
	i:=0;
	SetLength(PoljeR1,100);
	SetLength(NazivR1, 100);	// tekst ispred znaka "jednako"
	For i:=1 to 100 do begin							
		redak:=R1_Lista[i];						// čitanje pojedinačnih redova iz makroa
		ShowN('redak="'+redak+'"');
		// Showmessage('1 - i='+IntToStr(i)+' redak="'+redak+'"');
		if not (Contain(redak,'=')) then begin
			ShowN('Izlazim zbog toga što ovaj redak nema znaka =')
			Break;
		end;
		
		// Čitanje NazivR1[i] - lijevo od znaka jednakosti
		// prvo se kreira labela da imam amo pisati
		// function rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
		nazivR1[i]:=rcLabel( 'naz_'+IntToStr(i), '',Panel_1, sirpan-55,24+(i-1)*22,taRightJustify);  // taCenter ne radi
		
		for j:=1 to Length(redak) do begin
			if redak[j]='=' then begin
				ShowN('Izlazim zbog toga što sam došao do znaka =')
				Break;
			end;
			nazivR1[i].caption:=nazivR1[i].caption+redak[j];
			nazivR1[i].font.size:=9;
			ShowN('i='+IntToStr(i)+' j='+IntToStr(j));
			ShowN('redak="'+redak+'"');
		end;
		ShowN('Došao sam do znaka jednako, sad tražim vrjednost');
		// kreiranje polja jer inače sve ode kvragu
		// Function rcEdit(eName: string; eDest: TWinControl; eX, eY: integer):TEdit;
		// Showmessage('i='+IntToStr(i)+' mjesto: 4');
		PoljeR1[i]:=rcEdit('polje_'+IntToStr(i), Panel_1, sirpan-52,22+(i-1)*22);
		PoljeR1[i].font.size:=9;
		PoljeR1[i].font.style:=[fsBold];	
		// čitanje vrijednosti - desno od znaka jednakosto
		PoljeR1[i].text:=Copy(redak,j+1,200);
		PoljeR1[i].width:=48;
		// Showmessage('i='+IntToStr(i)+' mjesto: 5');
		// Showmessage(nazivR1[i]+' jednako '+PoljeR1[i].text);
	end;  // For i:=1
	Showmessage('A sad odabir potrošnog!');
	
	

End;

// ------------------------------------------------	
{$I Skripta_servis.cps}
// ------------------------------------------------

// Glavni   
Begin
   // Debug:=true;         // debuger ispis on/off
	Debug:=false;
	Izvrseno:=false;

	
	Main_start_Width:=850;
	Main_start_Height:=500;
	
	Create_Main_Window;
	ShowN('skripta: NAPRAVI SPOJ v1.0');
	ShowN('01.06.2017.');
	ShowN('==============');
	
	// Postavljanje panela
	sirpan:=round(Panel_Left.width/4);
	ShowD('Panel_Left.Width='+IntToStr(Panel_Left.Width));
	ShowD('Panel_Left.Width/4='+IntToStr(sirpan));
	
	// Panel Raster 1
	Panel_1:=rcPanel( 'pr1' ,Panel_Left ); 
	Panel_1.ALign:=alLeft; 
   Panel_1.Width:=sirpan;      
   Panel_1.BorderWidth:=5;
		
	// čitanje varijabli makroa
	makro1:='..\skripte\Editor\Makro_02\Fronta_Vertikala_Raster1_Obj1.CMK';
	makro2:='..\skripte\Editor\Makro_02\Fronta_Vertikala_Raster1_Obj2.CMK';
	call_makro(makro1, makro2, panel_1);
	
	
	// Panel Raster 2
	Panel_2:= rcPanel ( '' ,Panel_Left ); 
	Panel_2.ALign:=alLeft; 
   Panel_2.Width:=sirpan;      
   Panel_2.BorderWidth:=5;
	
	// Panel Ekscentar 1
	Panel_3:= rcPanel ( '' ,Panel_Left ); 
	Panel_3.ALign:=alLeft; 
   Panel_3.Width:=sirpan;      
   Panel_3.BorderWidth:=5;
	
	// Panel Ekscentar 2
	Panel_4:= rcPanel ( '' ,Panel_Left ); 
	Panel_4.ALign:=alLeft; 
   Panel_4.Width:=sirpan;      
   Panel_4.BorderWidth:=5;
	
	
	
	// eventi
	cbDebug_OnClick(cbDebug);  // poziva "cbDebug_OnClick" bez da je išta kliknuto
	ButOK.OnClick:=@ButOK_OnClick;
	ButCancel.OnClick:=@ButCancel_OnClick;
	ButPomoc.OnClick:=@ButPomoc_OnClick;
	
	ButOK.default:=True;
	ButCancel.ModalResult:=mrCancel;
	
	
   // Naslov
   
   // zaglavlje
   
   // tablica
   
   Main.ShowModal;
	// Main.Show;
	while Main.visible do begin
        application.processmessages;
   end;
   
   Main.free; 
   
   
End.