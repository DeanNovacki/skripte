Program test_nizova;
var
   a1: array of integer;  // dinami�ki niz
   b1: array [1..9] of integer;   // niz fiksne du�ine
   c1: array of array of integer;   // dvodimenzionalni dinami�ki niz
   d1: array [1..10 ]of array [1..10] of integer;  // dvodimenzionalni niz fiksne veli�ine
   duzina, i :integer;
    
Begin
     // Dinami�ki niz - odre�ivanje du�ine
     SetLength(a1,10);
     // Dinami�ki niz - pridru�ivanje vrijednosti     
     a1[3]:=15;
     // Dinami�ki niz - �itanje du�ine
     duzina:=GetArrayLength(a1);
     WriteLn(IntToStr(duzina));
     // Dinami�ki niz - �itanje vrijednosti     
     duzina:=a1[3];
     
     // Dvodimenzionalni dinami�ki niz
     
     // odre�ivanje veli�ine
     setlength(c1,10);
     For i := 0 to 9 do begin
         setLength(c1[i],10);
     end;
     // pridru�ivanje vrijednosti
     c1[2][2]:=14;
     // �itanje vrijednosti
     WriteLn(intToStr(c1[2][2]));
          
End.

