// Math Functions and Formating

Function Sqr(a:single):single;
// kvadriranje
begin
     result:=a*a;
end;

Function Tg(a:single):single;
// tangens
begin
     result:=sin(a)/cos(a);
end;

Function Ctg(a:single):single;
// kotangens
begin
     result:=cos(a)/sin(a);
end;

// Function random(a:integer):integer;


Function Zbroj(a,b:single):single;
// zbrajanje dva broja
begin
     result:=a+b;
end;   
