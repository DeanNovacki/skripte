// rcSystem

Procedure rcExecute(expath:string);
// pokre�e vanjsku datoteku
var
	PDF:TpdfPrinter;
Begin
	if fileexists(expath) then begin 
			PDF:=TpdfPrinter.create(1250);
			PDF.FileName := expath;
			PDF.ShowPdf;
			PDF.Free
		end else begin
			Showmessage('Ne mogu prona�i ' + #13#10 + expath);
	end;
End;

Procedure rcWaitFolderCreate(ppath:string);
// �ekanje da se kreira folder (je se kreira pomo�u Batch skripte koja je spora)
// Koristi se interno u naredbi za kreiranje foldera
Var
	i:integer;
	ok:boolean;
	ttt:tStringList;
Begin
	print ('Procedure rcWaitFolderCreate START');
	ttt:=tStringList.Create;
	ttt.Add('test of existance');
	repeat 
		i:=i+1;
		ok:=true;
		try ttt.SaveToFile(ppath+'\test.txt');
			except ok:=false;
		end;
		print ('Waiting '+intToStr(i));
	until ok or (i>1000);
	
	
	if ok then print ('Folder '+ppath+' created or already exist.')
			else Showmessage('neuspjelo kreiranje'+#13#10+ppath);
	
	print ('Procedure rcWaitFolderCreate END');	
End;

Procedure rcWaitFileCreate(put:string);
// �ekanje da se kreira file (je se kreira pomo�u Batch skripte koja je spora)
var
	i:integer;
Begin

End;
	
Procedure rcWaitFileDelete(put:string);
// �ekanje da se obri�e file (je se bri�e pomo�u Batch skripte koja je spora)
// koristi se interno u naredbi za brisanje datoteka
var
	i:integer;
Begin
	print ('Procedure rcWaitFileDelete START');
	// provjera da li je brisanje zavr�eno
	i:=0;
	repeat 
		i:=i+1;
		If not FileExists(put) then break;
		print ('Waiting '+intToStr(i));
	until i>2000;
	print ('File '+put+' finally deleted or not exist.');
	
	print ('Procedure rcWaitFileDelete END');
End;

Procedure rcCreateTemp;
// kreira temp folder unutar folder skripte 
// taj folder slu�i za privremeno spremanje raznih datoteka 

var
	tmpList:tStringList;
	ok:boolean;
	i:integer;
	putOut,
	putBatch:string;
Begin
	print ('Procedure rcCreateTemp START');
	
	putOut:=PrgFolder+'Skripte\Temp';
	
	if not fileExists(putOut+'\Test.txt') then begin
			tmpList:=tStringList.Create;
			tmpList.Add('@echo off');
			
			tmpList.Add('MD "'+putOut+'"');
			putBatch:=PrgFolder+'Skripte\kreiraj_temp.bat';
			tmpList.SaveToFile(putBatch);
			print ('Batch za kreiranje je spremljen u folder '+putBatch);
			
			// pokreni netom napravljenu batch skriptu
			rcExecute(putBatch);
			print ('Batch za kreiranje je pokrenut');
			
			// �ekanje da se folder kreira
			rcWaitFolderCreate(putOut);
			
			// obri�i temp bat iz foldera skripte
			// kreiraj novi Batch
			tmplist.clear;
			tmpList.Add('@echo off');
			tmpList.Add('DEL "'+putBatch+'"');
			// pokreni batch
			putBatch:=PrgFolder+'Skripte\Temp\brisanje.bat';
			print ('Sad �u Batch za brisanje spremiti kao '+PutBatch);
			
			
			tmpList.SaveToFile(PutBatch);
			
			
			print ('Batch za brisanje je spremljen kao '+PutBatch);
			
			// pokreni
			print ('Sad �u Batch za brisanje pokrenuti');
			rcExecute(putBatch);
			print ('Batch za brisanje je pokrenut');
			
			// provjera da li je brisanje zavr�eno
			putBatch:=PrgFolder+'Skripte\kreiraj_temp.bat';
			rcWaitFileDelete(putBatch)
		end else begin
			Print ('Tenp folder already exist');
	end;
	
	print ('Procedure rcCreateTemp END');
End;

Procedure rcFileDelete(put:string);
// brisanje datoteke 
var
	tmpList:tStringList;
	putBatch:string;
	i:integer;
Begin
	print ('Procedure rcFileDelete START');
	print ('Bri�em '+put);
	
	if fileExists(put) then begin
			rcCreateTemp;				// kreiraj temp folder (mo�da ga nema)
			
			// kreiraj novi Batch
			tmplist:=tStringList.create;
			tmpList.Add('@echo off');
			tmpList.Add('DEL "'+put+'"');
			// spremi novi batch
			putBatch:=PrgFolder+'Skripte\Temp\brisanje.bat';
			//print ('Sad �u Batch za brisanje spremiti kao '+PutBatch);
			tmpList.SaveToFile(PutBatch);
			//print ('Batch za brisanje je spremljen kao '+PutBatch);
			
			// pokreni
			//print ('Sad �u Batch za brisanje pokrenuti');
			rcExecute(putBatch);
			//print ('Batch za brisanje je pokrenut');
			
			// provjera da li je brisanje zavr�eno
			// put:=PrgFolder+'Skripte\Temp\CreateFolder.bat';
			i:=0;
			repeat 
				i:=i+1;
				If not FileExists(put) then break;
				print ('Waiting '+intToStr(i));
			until i>2000;
			print (Put+' is finnaly deleted');
			
			// pra�njenje datoteke "brisanje.bat'
			tmplist.clear;
			tmpList.SaveToFile(PutBatch)
		end else begin
			print ('Nothing to delete. File '+put+' not exist');
	
	end;

	print ('Procedure rcFileDelete END');
End;



Procedure rcCreateFolder(path, nname : string);
// kreiranje foldera nname unutar foldera path
var
	tmpList:tStringList;
	postoji:boolean;
	putskripte:String;
	put:string;
	i:integer;
	ok:boolean;
Begin
	print ('Procedure rcCreateFolder START ('+nname+')');
	// print ('path: '+path)
	// print ('nname: '+nname);
	
	// print ('create temp folder');
	rcCreateTemp;		// u slu�aju da temp ne postoji
	// print ('temp folder created');
	
	tmpList:=tStringList.Create;
	tmpList.Add('@echo off');
	put:=path+'\'+nname;
	tmpList.Add('MD "'+put+'"');
	
	put:=PrgFolder+'Skripte\Temp\CreateFolder.bat';
	// print ('Sad �u Batch za kreiranje spremiti kao '+Put);
	tmpList.SaveToFile(Put);
	// print ('Spremljeno');
	
	// pokreni netom napravljenu batch skriptu
	rcExecute(put);
	// print ('porenuto kreiranje');
	
	// testiranje postojanja novog foldera
	put:=path+'\'+nname;
	rcWaitFolderCreate(put);
	
	// brisanje Batcha za kreiranje
	put:=PrgFolder+'Skripte\Temp\CreateFolder.bat';
	rcFileDelete(put);
	print('1');
	
	print ('Procedure rcCreateFolder END ('+nname+')');
End;

Function rcDirFiles(path, filter: string):tStringList;
// popis datoteka iz nekog foldera stavlja u stringlistu
//
// nije najpouzdanija funkcija jer radi prebrzo i ne stigne pro�itat listu
// ako se koristi prvi put, pri�ekat �e da se kreira, ALI
// svaki idu�i put �e �itati listu kreiranu prethodni put!!!!
var
	tmpBat:string;
	tmpList:tStringList;
   i:integer;
	ok:boolean;
	putsave:string;
	putout:string;
Begin
	putsave:=prgFolder+'skripte\temp\';		// batch file
	
   // batch dir to list.txt
	tmpList:=tStringList.Create;
	tmpList.Add('@echo off');
	tmpList.Add('cd '+path);
	tmpList.Add('dir /b '+filter+'>rclist.txt');
	// tmpList.Add('timeout 2');
	
	tmpList.SaveToFile(putsave+'command.bat');
	print ('put. batcha: '+ putsave+'command.bat');
	
	
	
	
	// printTime ('Start command.bat');
	rcExecute(putsave+'command.bat');
	// printTime ('Done command.bat');
	
	tmpList.Clear;
	
	// load list from file
	i:=0
	repeat 
		i:=i+1;
		ok:=true;
		try tmpList.LoadFromFile(path+'\'+'rclist.txt');
			except ok:=false;
		end;
	until ok or (i>20000);
	// print ('i='+IntToStr(i));
	
	result:=tmpList;
	
	{
	for i:=0 to tmpList.count-1 do begin
		print (tmpList[i]);
	End;
	}
	
End;

Function rcDirDirs(path: string):tStringList;
// popis foldera iz nekog foldera stavlja u stringlistu
//
// nije najpouzdanija funkcija jer radi prebrzo i ne stigne pro�itat listu
// ako se koristi prvi put, pri�ekat �e da se kreira, ALI
// svaki idu�i put �e �itati listu kreiranu prethodni put!!!!

var
	tmpBat:string;
	tmpList:tStringList;
   i:integer;
	ok:boolean;
Begin
   // batch dir to list.txt
	tmpList:=tStringList.Create;
	tmpList.Add('@echo off');
	tmpList.Add('cd '+path);
	tmpList.Add('dir /a:D /b '+'>rclist.txt');
	// tmpList.Add('timeout 2');
	tmpList.SaveToFile(path+'\'+'command.bat');
	
	
	// printTime ('Start command.bat');
	rcExecute(path+'\'+'command.bat');
	// printTime ('Done command.bat');
	
	tmpList.Clear;
	
	// load list from file
	i:=0
	repeat 
		i:=i+1;
		ok:=true;
		try tmpList.LoadFromFile(path+'\'+'rclist.txt');
			except ok:=false;
		end;
	until ok or (i>20000);
	// print ('i='+IntToStr(i));
	
	result:=tmpList;
	
	 {
	for i:=0 to tmpList.count-1 do begin
		print (tmpList[i]);
	End;
	 }
	
End;