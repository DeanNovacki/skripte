var
   SystemToolbar:TPanel;   // Commands (tree) on right side of Corpus
   win,ScriptForm: TForm;      // Embeded form of script icons (if possible, without icons)
   ScriptPanel:Tpanel;     // backround for some script
   xo:TObject;
   work:boolean;
   i,ww:integer;
   tt:TButton;
 

{$I ..\include\rcMessages.cps}

{$I ..\include\rcStrings.cps}
{$I ..\include\rcControls.cps}
{$I ..\include\rcObjects.cps}
{$I ..\include\rcEvents.cps}
{$I ..\include\rcProgress.cps}
{$I ..\include\rcCurves.cps}
{$I ..\include\rcMath.cps}
{$I ..\include\rcConfig.cps}

 
Procedure CloseOnClick(sender:TObject);
begin 
   work:=false; 
end;  

Procedure ttShow_hide(sender:TObject);
begin 
   If SystemToolbar.visible=true then SystemToolbar.Visible:=False 
                                 else SystemToolbar.Visible:=True;
end;  


// ======================================================================================
begin
   work:=true;
  Showmessage ('Dobar dan!');
   
   
   // find forms
   // Caption of Form must be in Project.toc, In this case, this is 'Right'
   for i:=0 to application.MainForm.ControlCount-1 do begin
     xo:=application.MainForm.Controls[i];
     if (xo is Tpanel)  then if (TPanel(xo).name='stool') then 
			Begin
			   SystemToolbar:=TPanel(xo) ;      // Corpus panel on right side
				Showmessage ('Našao sam Corpusovu desnu alatnu traku!');
			end;
     if (xo is TForm)   then if (TForm(xo).caption='RCtools') then 
			Begin
				ScriptForm:=TForm(xo);         // Script form 
				Showmessage ('Našao sam RC desnu alatnu traku!');
			end;
   end; 
   
   // reveal form   
   ScriptForm.width:=400;                  // reveal form if width is 1 in project.toc
   ScriptForm.BorderStyle:=bsToolWindow;   // or bsSizeToolWin
   // ScriptForm.color:=clBlue;
   // ScriptForm.visible:=true;

   // create panel inside Script Form
   xo:=nil; xo:=find_control(ScriptForm, 'ToolPanel');
   ScriptPanel:=rcPanel('ToolPanel',ScriptForm);
   ScriptPanel.caption:='Super Panel';
   ScriptPanel.align:=alClient;
   ScriptPanel.color:=clWhite;
   
   // create some buttons
   xo:=nil; xo:=find_control(ScriptForm, 'Show_Hide');
   if xo=nil then begin 
                         tt:=rcButton('Show_Hide','show-hide',ScriptPanel, 10, 10);
                         tt.width:=90;
                         tt.caption:='Show/hide Tree';
                         tt.OnClick:=@ttShow_Hide;
                   end;
   
   xo:=nil; xo:=find_control(ScriptForm, 'Close_Button');
   if xo=nil then begin 
                         tt:=rcButton('Close_Button','close',ScriptPanel, 10, 40);
                         tt.width:=90;
                         tt.caption:='Close!';
                         tt.OnClick:=@CloseOnClick;
                   end;
   
    // create form for closing in editor
   win:=TForm.create(nil);
   win.left:=ScreenWidth-400; win.top:=50; win.width:=100; win.height:=50;
   win.name:='Close_tool'; win.caption:='Super Panel';
   win.FormStyle:=fsStayOnTop;
   win.BorderStyle:=bsToolWindow;
   win.BorderIcons:=[]; 
   xo:=nil; xo:=find_control(ScriptForm, 'Close_win');
   if xo=nil then begin 
                         tt:=rcButton('Close_win','close',win, 0, 0);
                         tt.width:=win.clientwidth; tt.height:=win.clientheight;
                         tt.caption:='Close';
                         tt.OnClick:=@CloseOnClick;
                   end;
	Showmessage ('Dobar dan!');
   win.show;
   
   
   while work do begin 
         application.processmessages;
   end;
   
   ScriptPanel.free;
   ScriptForm.width:=1;
   win.free;
 
end.