// Translation

var
   language:string;




Function rcFindTranslation(language,text:string):String;
var
   LangPath,LangFile,FileName:String;
   list:TStringList;
   ind:integer;
   Value:string;
Begin
   LangPath:='\..\_Include\languages\';
   LangPath:=PrgFolder+'skripte\RC_Tools\_include\languages\';
   LangFile:=language;
   FileName:=LangPath+LangFile;
   
   list:=TStringList.Create;

   try list.LoadFromFile(FileName);
     except ShowMessage( 'Function: rcFindTranslation'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
   
   ind:=List.IndexOfName(text);       // if text NOT EXIST, ind will be -1
                                      // if text DO EXIST but without value, ind will be real index 
   value:=list.Values[text];          // BUT, in this case value will be ''
   if ind>=0 then Result:=value
             else Result:='';
  
End;


Procedure language_init;
// Find active language. File: _include\languages\active.lng
Begin
   language:=rcFindTranslation('active.lng','active_language');
End;

Function tr(tt:String):String;
// translate input tekst tt

   
Begin
   result:=rcFindTranslation(language,tt);
   
   if (result='') or 
      (result=' ') or 
      (result='  ') then result:='?'+tt+'?';
   
End;

