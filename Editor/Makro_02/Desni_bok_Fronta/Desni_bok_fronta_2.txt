FRONTA ISPRED VERTIKALE
Pocetak - Pozicija prve rupe
Raster - Razmak izme�u rupa
Koli�ina - Broj rupa
Eks_Fi - Promjer Ekscentra
Eks_Dubina - Dubina rupe za ekscentar (Ako glasi "obj2.debljina/2+5" onda �e ekscentar biti postavljen na sredinu dubine daske)
Eks_strana - 0 = Lijeva strana, 1 = Desna strana vertikale
Eks_Pomak - Odmak centra ekscentra od ruba daske
Vijak_Dub_Vertikala (VDV) - Dubina rupe za vijak ekscentra u vertikali
Vijak_Dub_Fronta (VDV) - Dubina rupe za vijak ekscentra u fronti
Vijak_FI - Promjer vijska za ekscentar
Potro�ni 1 - �ifra prvog potro�nog materijala (ako postoji)
Potro�ni 1 - �ifra drugog potro�nog materijala (ako postoji)
Ako potro�nog materijala nema, trab polje ostaviti prazno. Skripa �e sama napisqati "*nema*" nakon izvr�avanja.
