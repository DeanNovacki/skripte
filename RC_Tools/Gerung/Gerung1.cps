Program Test;

var
   Main:TForm;
   Panel_Left, Panel_right, Panel_Title, Panel_Info, Panel_Table, Panel_Memo, Panel_MemoO :TPanel;
   Pan_var, Pan_val, Pan_for, Pan_cb, Pan_hint : array of TPanel;
   ButOK,ButTest,ButCancel:TButton;
   cbDebug:TCheckBox; MemoJ:TMemo;
   e1:Telement;


function find_control(root_object:TWinControl; vc_name:string):TObject;
//
// Recursive function for finding visual components in WinControl components (panel, window etc..)   
// Result is TObject (TPanel, TButton, TLabel, TEdit, TImage, TShape...)
// 
// root_object = parent object for searching (goes to subobjects also)
// c_name = name of object

var
   i:integer;
   xo:TObject;
   
   
begin 
  // writeln ('Trying to found control: '+vc_name);
  for i:=0 to root_object.ControlCount-1 do begin
     // writeln ('Analising object number '+IntToStr(i));
     xo:=root_object.Controls[i];
     if xo is Tlabel     then if UpperCase(vc_name)=UpperCase(TLabel(xo).name)    then result:=xo; 
     if xo is Tpanel     then if UpperCase(vc_name)=UpperCase(TPanel(xo).name)    then result:=xo;
     if xo is TButton    then if UpperCase(vc_name)=UpperCase(TButton(xo).name)   then result:=xo;
     if xo is TEdit      then if UpperCase(vc_name)=UpperCase(TEdit(xo).name)     then result:=xo;
     if xo is TMImage    then if UpperCase(vc_name)=UpperCase(TMImage(xo).name)   then result:=xo;
     if xo is TShape     then if UpperCase(vc_name)=UpperCase(TShape(xo).name)    then result:=xo;
     if xo is TForm      then if UpperCase(vc_name)=UpperCase(TForm(xo).name )    then result:=xo; 
     if xo is TScrollBox then if UpperCase(vc_name)=UpperCase(TScrollBox(xo).name)then result:=xo;
     if xo is TListBox   then if UpperCase(vc_name)=UpperCase(TListBox(xo).name)  then result:=xo;
     if xo is TGroupBox  then if UpperCase(vc_name)=UpperCase(TGroupBox(xo).name) then result:=xo;
     if xo is TCheckBox  then if UpperCase(vc_name)=UpperCase(TCheckBox(xo).name) then result:=xo;
     if (result=nil) and (xo is TWinControl) then result:=find_control(TWinControl(xo),vc_name);
 
  end;  // for 
end;  // function
//--------------------------------------------------------------------------------------
Function rcTool(tName:string):TForm;
// form with custom caption, after creation MUST use show or showmodal
var 
    TF:TForm;
Begin
    TF:=TForm.create(nil);
    // TF:=CreateParentedForm(true);
    //  TF:=CreateFreeForm(nil);
    
    // TF.Left:=x;TF.Top:=y;TF.Width:=w;TF.Height:=h;
    TF.FormStyle:=fsStayOnTop; 
    TF.BorderStyle:=bsSizeToolWin; // bsToolWindow; //bsSizeToolWin
    TF.Name:=tName;
    TF.Position:=poScreenCenter;
    TF.Font.Name:='Arial';
    TF.Font.Size:=10;
    // TF.Caption:=tCaption;
    result:=TF;
End; 
//------------------------------------------------------------------------------
function rcPanel( pName: string; pParent: TWinControl):TPanel;
var
   Panel_1:TPanel;
   xo:TObject;
 begin
     if pName<>'' then begin 
                       xo:=nil; 
                       find_control(pParent,pName);
                       if xo<>nil then Begin 
                          ShowMessage ('Panel with name: "'+pName+'" already exist!'); 
                          exit 
                       end;
                   end;    
     Panel_1:=Tpanel.create(pParent);
     if pName<>'' then Panel_1.name:=pName;
     //Panel_1.align:=alNone;    // alNone, alTop, alBottom, alLeft, alRight, alClient 
     Panel_1.parent:=pParent;
     //Panel_1.BevelInner:=bvNone;
     //Panel_1.BevelOuter:=bvRaised;
     //Panel_1.BorderStyle:=bsNone;
     //Panel_1.BorderWidth:=0;
     Panel_1.Caption:='';
     Panel_1.Left:=100;
     Panel_1.Top:=100;
     Panel_1.Width:=300;
     Panel_1.Height:=300;
     //Panel_1.BevelInner:=bvNone;
     //Panel_1.BevelOuter:=bvNone;
     Panel_1.BevelWidth:=1;
     //Panel_1.BorderStyle:=bsSingle;
     //Panel_1.BorderWidth:=3;
     //Panel_1.Ctl3D:=False;
     //PanelP1.Font.style:=[fsBold];
     // Panel_1.Font.Name:='Arial';
     // Panel_1.Font.Size:=10;
     // ne radi Panel_1.FullRepaint:=True;
     result:=Panel_1;
end;
//------------------------------------------------------------------------------
function rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
// Talignment: taLeftJustify, taCenter, taRightJustify 
 var
   Text_1:TLabel;
   xo:TObject;
 begin
     if lName<>'' then begin 
                       xo:=nil; 
                       find_control(LParent,LName);
                       if xo<>nil then Begin 
                          ShowMessage ('Label name: "'+LName+'" already exist!'); 
                          exit 
                       end;
                   end;    
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     if LName<>'' then Text_1.name:=LName;
     Text_1.caption:=LCaption; 
     // Text_1.font.name:='arial';
     // Text_1.font.size:=9;
     Text_1.Alignment:=LA;
     // if Align='right' then Text_1.left:=nX-text_1.width
     //                      else Text_1.left:=nX;
     Text_1.left:=LX;
     Text_1.top:=LY;
     result:=Text_1;
 end; 
 //------------------------------------------------------------------------
Procedure rcSpaceT( LParent: TWinControl; LY:integer);
// make spacing (top aligned)
 var Text_1:TLabel;
 begin
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     Text_1.caption:='  '; Text_1.font.size:=3; Text_1.top:=LY;
     Text_1.Align:=alTop;
 end; 
 Procedure rcSpaceB( LParent: TWinControl; LY:integer);
// make spacing (bottom aligned)
 var Text_1:TLabel;
 begin
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     Text_1.caption:='  '; Text_1.font.size:=3; Text_1.top:=LY;
     Text_1.Align:=alBottom;
 end; 
 Procedure rcSpaceL( LParent: TWinControl; LX:integer);
// make spacing (left aligned)
 var Text_1:TLabel;
 begin
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     Text_1.caption:='  '; Text_1.font.size:=3; Text_1.left:=LX;
     Text_1.Align:=alLeft;
 end; 
 Procedure rcSpaceR( LParent: TWinControl; LX:integer);
// make spacing (right aligned)
 var Text_1:TLabel;
 begin
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     Text_1.caption:='  '; Text_1.font.size:=3; Text_1.left:=LX;
     Text_1.Align:=alRight;
 end; 
 //------------------------------------------------------------------------
function rcButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TButton;
// Create Button
 var
   Button_1:TButton;
   xo:TObject;
 begin
     if bName<>'' then begin 
                       xo:=nil; 
                       find_control(bParent,bName);
                       if xo<>nil then Begin 
                          ShowMessage ('Button with name: "'+bName+'" already exist!'); 
                          exit 
                       end;
                   end;     
     Button_1:=TButton.create(bParent);
     Button_1.parent:=bParent;
     if bName<>'' then Button_1.name:=bName;  
     Button_1.caption:=bCaption;
     Button_1.Left:=bX;
     Button_1.Top:=bY;
     Button_1.Width:=120;
     Button_1.Height:=25
     Button_1.ShowHint:=true;
     // Button_1.Font.Name:='Arial';
     // Button_1.Font.Size:=9;
     result:=Button_1;
end;

function rcCheckBox(cbName, cbCaption: string; cbParent: TWinControl; cbX, cbY: integer):TCheckBox;
// Create CheckBox
 var
   CB1:TCheckBox;
   xo:TObject;
 begin
     if cbName<>'' then begin 
                       xo:=nil; 
                       find_control(cbParent,cbName);
                       if xo<>nil then Begin 
                          ShowMessage ('CheckBox with name: "'+cbName+'" already exist!'); 
                          exit 
                       end;
                   end;     
     CB1:=TCheckBox.create(cbParent);
     CB1.parent:=cbParent;
     if cbName<>'' then CB1.name:=cbName;  
     CB1.caption:=cbCaption;
     CB1.Left:=cbX;
     CB1.Top:=cbY;
     CB1.Alignment:=taLeftJustify;
     // CB1.Width:=120;
     // CB1.Height:=25
     CB1.ShowHint:=true;
     // CB1.Font.Name:='Arial';
     // CB1.Font.Size:=9;
     result:=CB1;
end;

function rcMemo(mName:string; mParent: TWinControl; mX, mY, mW, mH: integer):TMemo;
var
   Memo_1:TMemo;
   xo:TObject;
begin
   if mName<>'' then begin 
                       xo:=nil; 
                       find_control(mParent,mName);
                       if xo<>nil then Begin 
                          ShowMessage ('Memo name: "'+mName+'" already exist!'); 
                          exit 
                       end;
                   end;     
   Memo_1:=TMemo.create(mParent);
   Memo_1.parent:=mParent;
   if mName<>'' then Memo_1.name:=mName;
   Memo_1.Left  :=mX;
   Memo_1.Top   :=mY;
   Memo_1.Width :=mW;
   Memo_1.Height:=mH;
   result:=Memo_1;
end;

Function rcFindSelectedElement:TElement;
// Find first selected element
var i:integer;
    elm:TElement;
Begin
  if e=nil then begin   // search in project;
     for i:=0 to elmholder.Elementi.count-1 do begin
         elm:=Telement(ElmHolder.Elementi.items[i]);            
         if elm.selected=true then begin
                                 result:=elm; 
                                 exit;
         end; // if 
     end;   // for                                 
  end else begin   // search in editor
        for i:=0 to e.ElmList.CountObj-1 do Begin  
           elm:=e.ElmList.Element[i];
           if elm.selected=true then begin
                                 result:=elm;  
                                 exit;
           end;  // if
        end;   // for
  end; // if e<>nil
End;

Function BooleanToStr(bb:boolean):String;
Begin
   if bb then result:='T'
         else result:='F';
End;

Procedure Print_data(e2:Telement);
Begin
   MemoJ.Lines.add('Changed: '     +BooleanToStr(e2.Changed)+' | '

                  + 'Locked: '      + BooleanToStr(e2.Locked) +' | '
   //               + 'SerNo: '       + e2.SerNo +' | '
                  + 'ZidIndex: '    + IntToStr(e2.ZidIndex)+' | '
                 + 'SirinaOfset: ' + FloatToStr(e2.SirinaOfset)+' | '
                  + 'DubinaOfset: ' + FloatToStr(e2.DubinaOfset)+' | '
                  + 'IndexOnWall: ' + IntToStr(e2.ZidIndex)+' | '
                     );
End;


// MAIN ////////////////////////////////////////////////////////////////////////

   
Begin
   // design
   Main:=rcTool('Osobine'); Main.Width:=800;Main.Height:=400;
   
   // Main.FormStyle:=MDIForm;
   // desni panel
   Panel_Right:= rcPanel ( '' ,Main ); Panel_Right.ALign:=alRight; 
   Panel_Right.Width:=750;       // Širina desnog panela!!!!
   Panel_Right.BorderWidth:=5;
   // lijevi panel
   Panel_Left:= rcPanel ( '' ,Main ); Panel_Left.ALign:=alClient;
   // tipke 
   ButOK:=rcButton('bOK','OK',Panel_Right,1,1);ButOK.Align:=alTop;
   rcSpaceT(Panel_Right,2);
   ButTest:=rcButton('bTest','Test',Panel_Right,3,3);ButTest.Align:=alTop;
   rcSpaceT(Panel_Right,4);
   ButCancel:=rcButton('bCancel','Cancel',Panel_Right,3,20);ButCancel.Align:=alBottom;
   rcSpaceB(Panel_Right,19);
   // MemoJ
   Panel_Memo:=rcPanel ( '' ,Panel_Right ); Panel_memo.Top:=18;Panel_Memo.ALign:=alClient; 
   cbDebug:=rcCheckBox('cbDB','Debug',Panel_Memo, 1,20);cbDebug.Align:=alBottom; cbDebug.Font.Size:=7;
   MemoJ:=rcMemo('Memo_debug', Panel_Memo, 1,18,1,1);MemoJ.Align:=alClient;
   MemoJ.Color:=clBTNFace;MemoJ.Font.Size:=8;MemoJ.Ctl3D:=False;MemoJ.Font.Color:=clMaroon;
   
   MemoJ.Lines.add('Started');
   // Naslov
   
   // zaglavlje
   
   // tablica

   
   
    Main.Show;
   // Main.ShowModal;
   
   while main.visible do begin
      try
        e1:=rcFindSelectedElement;
      except;
      end;
        if e1<>nil then print_data(e1);
        application.processmessages;
    end;
   
   Main.free; 
   
   
End.