// rcMessages /////////////////////////////////////////////////////////////
// Write message to MemoJ
// MemoJ must be in Main form of script
//
// ShowN = write messsage to MemoJ
// ShowD = write messsage to MemoJ if cbDebug.checked=true
//
var
   MemoJ:TMemo;

   cbDebug:TCheckBox;
procedure ShowD(mess:String);
Begin
	// Showmessage('10');
   try 
		If cbDebug.checked=true then MemoJ.Lines.Add(mess);
	except 
		Showmessage(mess);
	end;
End;
procedure ShowN(mess:String);
Begin
	try 
		// If cbDebug.checked=true then MemoJ.Lines.Add(mess);
      MemoJ.Lines.Add(mess);
	except 
		Showmessage(mess);
	end;
End;


// *********************************************************************************
// Funkcija otvara poruku na vlastitom prozoru i dvije tipke.
// Jedna Tipka daje rezultat TRUE, a druga FALSE
//
//	if MessageYesNo(300,150,'Naslov prozora', 'Neki tekst poruke ','Može','Zaboravi',false) then do nesto;

//
function MessageYesNo(	const MWidth, MHeight:Integer;	// širina i visina prozora	
                        ACaption, APrompt: string;	// Naslov i tekst
                        TxtOK, TxtCancel:string	// buttons text
						//		pass:boolean
						//		var 		Value: string
							): Boolean;
var
  MForm: TForm;
  Prompt: TLabel;
  Edit: TEdit;
  DialogUnits: TPoint;
  ButtonTop, ButtonWidth, ButtonHeight: Integer;
begin
  Result := False;
  MForm := TForm.Create(Application);
  with MForm do
    try
//      Canvas.Font := Font;
      DialogUnits.x := 10;
      DialogUnits.y := 12;
      BorderStyle := bsDialog;
      Caption := ACaption;
      ClientWidth := MWidth;
      ClientHeight:=MHeight;      
      Position := poScreenCenter;
      Prompt := TLabel.Create(MForm);
      with Prompt do
      begin
        Parent := MForm;
        Caption := APrompt;
        Left:=15;
        Top:=15;
        Width:=MForm.ClientWidth-30;
        WordWrap := True;
      end;
      ButtonTop := Prompt.Top + Prompt.Height + 15;
      ButtonWidth := round((MForm.ClientWidth-3*30)/2)
      ButtonHeight := 20;
      with TButton.Create(MForm) do
      begin
        Parent := MForm;
        Caption := TxtOK;
        ModalResult := mrOk;
        Default := True;
        left:=30;
        top:=ButtonTop;
        Width:=ButtonWidth;
        Height:=ButtonHeight;
      end;
      with TButton.Create(MForm) do
      begin
        Parent := MForm;
        Caption := TxtCancel;
        ModalResult := mrCancel;
        Cancel := True;
        left:=60+ButtonWidth;
        top:=ButtonTop;
        Width:=ButtonWidth;
        Height:=ButtonHeight;
        MForm.ClientHeight := Top + Height + 13;          
      end;
      if ShowModal = mrOk then
      begin
        // Value := Edit.Text;
        Result := True;
      end;
    finally
      MForm.Free;
    end;
end;
