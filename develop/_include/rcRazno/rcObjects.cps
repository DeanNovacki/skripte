Type
   TBorder = Record
		Name : String;  		// nazivi objekata (redom: lijevi, desni, do, go, prednji, zadnji)
		Typ  : String;  		// tipovi objekata T/D
		Pos  : Single;  		// polozaji (xx,yy,zz)
		dir  : TSmjerDaske;  // VertFront, VertBok, Horiz
		Offset: Single; 		// offseti (lijevi, donji, prednji)
		soposf:String;  		// položaj svjetlog otvora (formula)
	End;
	
	TSvijetliOtvor = Record
      Left, Width, Bottom, Height, Front, Depth : TBorder; 
   end;
var
    so1:TSvijetliOtvor;



////////////////////////////////////////////////////////////////////
 // rcObjects
 //////////////////////////////////////////////////////////////////////
function el_find_sel_plates:TStringList;
var
   i:Integer;
   td:TDaska;
   Elist:TStringList;
Begin
   ShowD('function "el_find_sel_plates" started');
   Elist:=Tstringlist.create;
   // ShowD('Kreirao Elist:TStringList"');
   if e<>nil then begin  // U Editoru sam
      for i:=0 to e.childdaska.CountObj-1 do Begin  // broji daske u editoru
           td:=e.childdaska.Daska[i];   
           if td.selected then begin
               Elist.addObject(td.naziv,td);
           end;  // if td.selected 
      End; // for i
   end else begin
      ShowMessage('U projektu sam! Ovdje nema selektiranih dasaka');
      //
   end;  // if e<>nil     
   result:=Elist;    
	ShowD('function "el_find_sel_plates" ended');
End;

function el_find_sel_elements:TStringList;
var
   i:Integer;
   elm:TElement;
   Elist:TStringList;
Begin
   // ShowD('Function "el_find_sel_elements" started');
   Elist:=Tstringlist.create;
   // ShowD('Kreirao Elist:TStringList"');
   if e<>nil then begin  // U Editoru sam
      for i:=0 to ElmHolder.elementi.count-1 do Begin  // broji elemente u editoru
           elm:=telement(ElmHolder.elementi.items[i]);   
           if elm.selected then begin
               Elist.addObject(elm.naziv,elm);
           end;  // if elm.selected 
      End; // for i
   end else begin
      ShowMessage('U projektu sam! Ovdje nema selektiranih elemenata');
      //
   end;  // if e<>nil     
   result:=Elist;    
	// ShowD('Function "el_find_sel_elements" ended');
End;

function el_find_sel_objects:TStringList;
var
   i:Integer;
	td:Tdaska;
   elm:TElement;
   Elist:TStringList;
Begin
   // ShowD('Function "el_find_sel_objects" started');
   Elist:=Tstringlist.create;
   // ShowD('Kreirao Elist:TStringList"');
   if e<>nil then begin  // U Editoru sam
      //for i:=0 to ElmHolder.elementi.count-1 do Begin  // broji elemente u editoru
		for i:=0 to e.elmlist.countObj-1 do Begin  // broji elemente u editoru
           elm:=telement(e.elmlist.element[i]);   
           if elm.selected then begin
               Elist.addObject(elm.naziv,elm);
           end;  // if elm.selected 
      End; // for i
		for i:=0 to e.childdaska.CountObj-1 do Begin  // broji daske u editoru
           td:=e.childdaska.Daska[i];   
           if td.selected then begin
               Elist.addObject(td.naziv,td);
           end;  // if td.selected 
      End; // for i
   end else begin
      ShowMessage('U projektu sam! Prekidam!');
      //
   end;  // if e<>nil     
   result:=Elist;    
	// ShowD('Function "el_find_sel_elements" ended');
End;

function rcSO(elist:TstringList):TSvijetliOtvor;
// nalazi svjetli otvor
// dobiva niz objekata (elementi ili daske)
// izlaz je record koji mora biti definiran globalno
// izlalzni podaci su redom: x, sirina, y, visina, z, dubina

// rezultat na kraju izgleda: 
//		so1.left.name:=Lijevi_Bok
// 	so1.Left.Typ:='D'
// 	so1.Left.Pos:=100
// 	so1.Left.offset:=18
// 	so1.Left.dir:=vertbok
// 	so1.Left.soposf:='Lijevi_Bok.x+Lijevi_Bok.debljina';
var
{
Type
   TBorder = Record
		Name : String;  // nazivi objekata (redom: lijevi, desni, do, go, prednji, zadnji)
		Typ  : String;  // tipovi objekata E/D
		Pos  : Single;  // polozaji (xx,yy,zz)
		Offset: Single; // offseti (lijevi, donji, prednji)
		dir	: TSmjerDaske
		soposf:String;  // položaj svjetlog otvora (formula)
	End;
	
	TSvijetliOtvor = Record
      Left, Width, Bottom, Height, Front, Depth : TBorder; 
   end;
}
	i:integer;
	td:Tdaska;
	te:TElement;
	xt,yt,zt:single;					


Begin
	ShowD('procedure "rcSO" started');
	elist:=el_find_sel_objects;
	
	// pocetne vrijednosti
	xt := 1000000;
	// if TObject(elist.objects[1]) is tdaska then xt:=Tdaska(elist.objects[1]).xpos+Tdaska(elist.objects[1]).debljina;
	// if TObject(elist.objects[1]) is telement then xt:=Telement(elist.objects[1]).xpos+Telement(elist.objects[1]).sirina;
	yt := 1000000; 
	
	// ShowD('1');
	for i:=0 to elist.Count-1 do begin
		ShowD('Trazim lijevo ('+IntToStr(i)+')');
	   // find left object
	   if TObject(elist.objects[i]) is tdaska then begin
			td:=Tdaska(elist.objects[i]);

			if (td.smjer=vertbok) and ((td.xpos+td.debljina)<xt) then begin 
				result.left.Typ:='D';
				result.left.Name:=Td.naziv;
				result.left.Pos:=Td.xpos;
				result.left.dir:=Td.smjer;
				result.left.offset:=Td.debljina;
				result.left.soposf:=td.Naziv+'.x+'+td.naziv+'.de';
				xt:=td.xpos+td.debljina;
			end;	// if (td.smjer=vertbok)
			if (td.smjer=vertfront) and ((td.xpos+td.dubina)<xt) then begin 
				result.left.Typ:='D';
				result.left.Name:=Td.naziv;
				result.left.Pos:=Td.xpos;
				result.left.dir:=Td.smjer;
				result.left.offset:=Td.dubina;
				result.left.soposf:=td.Naziv+'.x+'+td.naziv+'.sirina';
				xt:=td.xpos+td.dubina;
			end;	// if (td.smjer=vertfront)
			if (td.smjer=horiz) and ((td.xpos+td.visina)<xt) then begin 
				result.left.Typ:='D';
				result.left.Name:=Td.naziv;
				result.left.Pos:=Td.xpos;
				result.left.dir:=Td.smjer;
				result.left.offset:=Td.visina;
				result.left.soposf:=td.Naziv+'.x+'+td.naziv+'.visina';
				xt:=td.xpos+td.visina;
			end;	// if (td.smjer=horizontal)
		end else begin  // //  if TObject(elist.objects[i]) is tdaska
		   
			te:=Telement(elist.objects[i]);
			if (te.xpos+te.sirina)<xt then begin
				result.left.Name:=Te.naziv;
				result.left.Typ:='E';
				result.left.Pos:=Te.xpos;
				result.left.offset:=Te.sirina;
				result.left.soposf:=te.Naziv+'.x+'+te.naziv+'.sirina';
				xt:=te.xpos+te.sirina;
			end;
		end;	// if TObject(elist.objects[i]) is tdaska
	end; // for i:=0 to elist.Count-1 	
		
	ShowD('Trazim desno ('+IntToStr(i)+')');
		// find object width
	for i:=0 to elist.Count-1 do begin 
	   if TObject(elist.objects[i]) is tdaska then begin
			ShowD('to width is daska('+IntToStr(i)+')');
			td:=Tdaska(elist.objects[i]);
			if td.xpos>=xt then begin 
				result.width.Name:=Td.naziv;
				result.width.Typ:='D';
				result.width.Pos:=Td.xpos;
				if result.left.Typ='D' then begin
						ShowD('Left is D');
						if result.left.dir=vertbok then result.width.soposf:=result.width.Name+'.x-'+result.left.name+'.x-'+result.left.name+'.debljina';
						if result.left.dir=vertfront then result.width.soposf:=result.width.Name+'.x-'+result.left.name+'.x-'+result.left.name+'.sirina';
						if result.left.dir=horiz then result.width.soposf:=result.width.Name+'.x-'+result.left.name+'.x-'+result.left.name+'.visina';
					end else begin ; // if result.left.Typ='D'
						ShowD('Left is E');
						result.width.soposf:=result.width.Name+'.x-'+result.left.name+'.x-'+result.left.name+'.sirina';
				end; //if result.left.Typ='D' 
				xt:=td.xpos;
			end;	// if td.xpos>xt
			
		end else begin  // //  if TObject(elist.objects[i]) is tdaska
			ShowD('to width is element('+IntToStr(i)+')');
			te:=Telement(elist.objects[i]);
			ShowD('ELEMENT: '+te.naziv+' se obrađuje');
			ShowD(te.naziv+'.x = '+FloatToStr(te.xpos));		
			ShowD('stari xt = '+FloatToStr(xt));		
			
			if te.xpos>=xt then begin 
			   ShowD('Element: '+te.naziv+' je desno!!!!!');
				result.width.Name:=Te.naziv;
				result.width.Typ:='E';
				result.width.Pos:=Te.xpos;
				if result.left.Typ='D' then begin
					ShowD('Left is D');
					if result.left.dir=vertbok then result.width.soposf:=result.width.Name+'.x-'+result.left.name+'.x-'+result.left.name+'.debljina';
					if result.left.dir=vertfront then result.width.soposf:=result.width.Name+'.x-'+result.left.name+'.x-'+result.left.name+'.sirina';
					if result.left.dir=horiz then result.width.soposf:=result.width.Name+'.x-'+result.left.name+'.x-'+result.left.name+'.visina';
				end else begin ; // if result.left.Typ='D'
					ShowD('Left is E');
					result.width.soposf:=result.width.Name+'.x-'+result.left.name+'.x-'+result.left.name+'.sirina';
				end;
				
				xt:=te.xpos;
			end;	// if td.xpos>xt
			
		end;	// if TObject(elist.objects[i]) is tdaska
	end; // for i:=0 to elist.Count-1 
	
		// --------------------------------------------------------------------------
	ShowD('Trazim dolje ('+IntToStr(i)+')');
	   // find bottom object
	for i:=0 to elist.Count-1 do begin	
	   if TObject(elist.objects[i]) is tdaska then begin
			td:=Tdaska(elist.objects[i]);

			if (td.smjer=vertbok) and ((td.ypos+td.visina)<yt) then begin 
				result.bottom.Typ:='D';
				result.bottom.Name:=Td.naziv;
				result.bottom.Pos:=Td.Ypos;
				result.bottom.dir:=Td.smjer;
				result.bottom.offset:=Td.visina;
				result.bottom.soposf:=td.Naziv+'.y+'+td.naziv+'.visina';
				yt:=td.ypos+td.visina;
			end;	// if (td.smjer=vertbok)
			if (td.smjer=vertfront) and ((td.ypos+td.visina)<yt) then begin 
				result.bottom.Typ:='D';
				result.bottom.Name:=Td.naziv;
				result.bottom.Pos:=Td.Ypos;
				result.bottom.dir:=Td.smjer;
				result.bottom.offset:=Td.visina;
				result.bottom.soposf:=td.Naziv+'.y+'+td.naziv+'.visina';
				yt:=td.ypos+td.visina;
			end;	// if (td.smjer=vertfront)
			if (td.smjer=horiz) and ((td.ypos+td.debljina)<yt) then begin 
				result.bottom.Typ:='D';
				result.bottom.Name:=Td.naziv;
				result.bottom.Pos:=Td.ypos;
				result.bottom.dir:=Td.smjer;
				result.bottom.offset:=Td.debljina;
				result.bottom.soposf:=td.Naziv+'.y+'+td.naziv+'.debljina';
				yt:=td.ypos+td.debljina;
			end;	// if (td.smjer=horizontal)
		end else begin  // //  if TObject(elist.objects[i]) is tdaska
		   
			te:=Telement(elist.objects[i]);
			if (te.xpos+te.visina)<yt then begin
				result.bottom.Name:=Te.naziv;
				result.bottom.Typ:='E';
				result.bottom.Pos:=Te.xpos;
				result.bottom.offset:=Te.visina;
				result.bottom.soposf:=te.Naziv+'.y+'+te.naziv+'.visina';
				yt:=te.ypos+te.visina;
			end;
		end;	// if TObject(elist.objects[i]) is tdaska
	end; // for i:=0 to elist.Count-1 
	
	ShowD('Trazim gore ('+IntToStr(i)+')');
		// find object with
	for i:=0 to elist.Count-1 do begin	
	   if TObject(elist.objects[i]) is tdaska then begin
			ShowD('to height is daska('+IntToStr(i)+')');
			td:=Tdaska(elist.objects[i]);
			if td.ypos>yt then begin 
				result.Height.Name:=Td.naziv;
				result.Height.Typ:='D';
				result.Height.Pos:=Td.ypos;
				if result.bottom.Typ='D' then begin
						ShowD('Down is D');
						if result.Bottom.dir=vertbok then result.Height.soposf:=result.Height.Name+'.y-'+result.bottom.name+'.y-'+result.bottom.name+'.visina';
						if result.Bottom.dir=vertfront then result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.visina';
						if result.Bottom.dir=horiz then result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.debljina';
					end else begin ; // if result.bottom.Typ='D'
						ShowD('Down is E');
						result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.visina';
				end; //if result.left.Typ='D' 
				yt:=td.ypos;
			end;	// if td.xpos>xt
			
		end else begin  // //  if TObject(elist.objects[i]) is tdaska
			ShowD('to width is element('+IntToStr(i)+')');
			te:=Telement(elist.objects[i]);

			if te.ypos>yt then begin 
				result.Height.Name:=Te.naziv;
				result.Height.Typ:='E';
				result.Height.Pos:=Te.ypos;
				if result.Height.Typ='D' then begin
					ShowD('Down is D');
					if result.Bottom.dir=vertbok then result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.visina';
					if result.Bottom.dir=vertfront then result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.visina';
					if result.Bottom.dir=horiz then result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.debljina';
				end else begin ; // if result.left.Typ='D'
					ShowD('Down is E');
					// result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.visinaR';
					
					if result.Bottom.dir=vertbok then result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.visina';
					if result.Bottom.dir=vertfront then result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.visina';
					if result.Bottom.dir=horiz then result.Height.soposf:=result.Height.Name+'.y-'+result.Bottom.name+'.y-'+result.Bottom.name+'.debljina';
					
					
					
				end;  // if result.Height.Typ='D' 
				
				yt:=te.ypos;
			end;	// if td.xpos>xt
			
		end;	// if TObject(elist.objects[i]) is tdaska
	end; // for i:=0 to elist.Count-1 	
		
	ShowD('lijeva daska je '+result.left.name);
	ShowD('desna daska je '+result.width.name);
	ShowD('doljnja daska je '+result.bottom.name);
	ShowD('gornja daska je '+result.height.name);
	
	ShowD('procedure "rcSO" ended');



End;  // function

procedure rcSO_x( var lista:TStringList;						// lista ulaznih dasaka i/ili elemenata
				      var 	N1, N2, N3, N4, N5, N6:String;	   // nazivi objekata (li, de, do, go, fr, le)
								T1, T2, T3, T4, T5, T6:String;	   // tipovi objekata T/D
								P1, P2, P3, P4, P5, P6:Single;	   // polozaji (xx,yy,zz)
								O1    , O3    , O5	 :Single);	   // ofset (debljina ili sirina, visina, dubina)	

// procedura za detektiranje svijetlog otvora
//
// ulazni TStringList je lista ulaznih objekata (daske ili elementi)
// ako nema selektiranih objekata onda pokušava sam naći prvi lijevi svijetli otvor
// ako ima 3 selektirana pokušava zamijeniti jedan run sa eubom elementa
// ako ima 4 selektirana to je svijetli otvor, a prednja i zadnja su prednji i zadnji rub elementa
// ako ima 5 selektiranih ...
// ako ima 6 selektiranih ...

// izlazni rezultat = objekt + tip + položaj + ofset
// ofset je debljina za lijevi, donji i prednji rub ili 
// širina za lijevi, visina za donji i dubina za prednji ako se radi o elementu

var
	elist:TStringList;

Begin
	ShowD('procedure "rcSO" started');
	elist:=el_find_sel_objects;
	

	
	ShowD('procedure "rcSO" ended');
End;






















procedure S_otvor(var Li_naziv, De_naziv, Go_naziv, Do_naziv, Naz_naziv: string;
                              var Li_x, De_x, Go_y, Do_y, Naz_z,
                                  Li_xx, De_xx, Go_yy, Do_yy, Naz_zz: Integer;
                              var OK:Boolean) ;


// Detektiranje svijetlog otvora i njegovih granica
// Ako nisu selektirane 4 daske, traži svijetli otvor elementa

// Li_naziv = naziv daske koja je lijeva granica otvora
// De_naziv = naziv daske koja je desna granica otvora
// Go_naziv = naziv daske koja je gornja granica otvora
// Do_naziv = naziv daske koja je donja granica otvora
// Naz_naziv = naziv daske koja je zadnja granica otvora

// Li_x = pozicija X lijeve daske
// De_x = pozicija X desne daske
// Go_y = pozicija Y gornje daske
// Do_y = pozicija Y gornje daske
// Naz_z = pozicija Z straznje daske

// Li_xx = debljina lijeve daske
// De_xx = debljina desne daske
// Go_yy = debljina gornje daske
// Do_yy = debljina donje daske
// Naz_zz = debljina lijeve daske

// OK = svijetli otvor nađen ili nije (Boolean) 

                              
  var
	b:tbox; 
        i,j:integer;
begin
        OK:=true;                        // procedura ispravno izvsena?
        // brojanje selektiranih dasaka
        j:=0; 
        for i:=0 to e.ChildDaska.CountObj-1 do begin
            if e.ChildDaska.Daska[i].selected then j:=j+1;
        end; 

        if (j<>4) and (j<>0) then begin
           ShowMessage('Selektirajte 4 daske ili niti jednu!');
           OK:=False;                  // procedura nije ispravno izvrsena!
           Exit;
        end;

	b:=inbox(e,0);                                    // čitanje svijetlog otvora

        Li_Naziv:='NEMA lijeve daske';Li_x:=100000;                   // početne vrijednosti 
        De_Naziv:='NEMA desne daske'; De_x:=-1;
        Go_Naziv:='NEMA gornje daske';Go_y:=-1;
        Do_Naziv:='NEMA donje daske'; Do_y:=100000;

      // pronalazenje naziva, dimenzija i pozicija ako su selektirane 4 daske
		
        if j=4 then begin
  
	   if (MinMaxObj.Left is tdaska) then begin
              Li_naziv:= Tdaska(MinMaxObj.Left).naziv;
              Li_x:=round(Tdaska(MinMaxObj.Left).xPos);                         // polozaj
              Li_xx:=round(Tdaska(MinMaxObj.Left).Debljina);                    // debljina
           end;
	   if (MinMaxObj.Right is tdaska) then begin 
              de_naziv:= Tdaska(MinMaxObj.Right).naziv;
              De_x:=round(Tdaska(MinMaxObj.Right).xPos);
              De_xx:=round(Tdaska(MinMaxObj.Right).Debljina);
           end;
	   if (MinMaxObj.Top is tdaska) then begin
              Go_naziv:= Tdaska(MinMaxObj.Top).naziv;
              Go_y:=round(Tdaska(MinMaxObj.Top).yPos);
              Go_yy:=round(Tdaska(MinMaxObj.Top).Debljina);
           end;
	   if (MinMaxObj.Bottom is tdaska) then begin
              Do_naziv:= Tdaska(MinMaxObj.Bottom).naziv;
              Do_y:=round(Tdaska(MinMaxObj.Bottom).yPos);
              Do_yy:=round(Tdaska(MinMaxObj.Bottom).Debljina);
           end;
        end 
        
        // pronalazenje naziva, dimenzija i debljina ako nije selektirano nista
          
        else begin      
                  for i:=0 to e.ChildDaska.CountObj-1 do                        // prođi kroz sve daske u elementu
                     if (e.ChildDaska.Daska[i].Visible) and (e.ChildDaska.Daska[i].TipDaske<>Nogica) then 
                     begin
                        if e.ChildDaska.Daska[i].smjer = horiz then             // ispitavanje da li je horizontalna
                            begin 
                            If e.ChildDaska.Daska[i].yPos < Do_y  then          // trazi najnizu
                               begin 
                                  Do_naziv:= e.ChildDaska.Daska[i].Naziv;
                                  Do_y:=round(e.ChildDaska.Daska[i].yPos);
                                  Do_yy:=round(e.ChildDaska.Daska[i].Debljina);
                               end;
                            If e.ChildDaska.Daska[i].yPos > Go_y  then          // trazi najvisu 
                               begin 
                                  Go_naziv:= e.ChildDaska.Daska[i].Naziv;
                                  Go_y:=round(e.ChildDaska.Daska[i].yPos);
                                  Go_yy:=round(e.ChildDaska.Daska[i].Debljina);
                               end;
                            End;
                        if e.ChildDaska.Daska[i].smjer = VertBok then           // ispitavanje da li je horizontalna
                            Begin
                            If e.ChildDaska.Daska[i].xPos < Li_x  then          // trazi najljeviju 
                               begin 
                                  Li_naziv:= e.ChildDaska.Daska[i].Naziv;
                                  Li_x:=round(e.ChildDaska.Daska[i].xPos);
                                  Li_xx:=round(e.ChildDaska.Daska[i].Debljina);
                               end;                            
                            If e.ChildDaska.Daska[i].xPos > De_x  then          // trazi najdesniju 
                               begin
                                  De_naziv:= e.ChildDaska.Daska[i].Naziv;
                                  De_x:=round(e.ChildDaska.Daska[i].xPos);
                                  De_xx:=round(e.ChildDaska.Daska[i].Debljina);
                               end;
                            End; 
                     end;
        end;
        // trazi ledja
        Naz_Naziv:='Nema zadnje daske'; Naz_z:=round(0-e.dubina-101);
        for i:=0 to e.ChildDaska.CountObj-1 do                        // prođi kroz sve daske u elementu
            if e.ChildDaska.Daska[i].Visible then                      // samo one koje su visible
               begin
                   if e.ChildDaska.Daska[i].smjer = VertFront then           // ispitavanje da li je fronta
                        If (e.ChildDaska.Daska[i].zPos > Naz_z) and (e.ChildDaska.Daska[i].zPos < 0)  then     // trazi najdalju za ledja 
                             begin 
                                  Naz_naziv:= e.ChildDaska.Daska[i].Naziv;
                                  Naz_z:=round(e.ChildDaska.Daska[i].zPos);
                                  Naz_zz:=round(e.ChildDaska.Daska[i].Debljina);
                             end;                            
               end;
        If Naz_z = 0-e.dubina-101 then          // ako je z ostao defaultni
                        Begin                             // znači da nema ledja
                             Naz_z:=round(e.dubina);             
                             Naz_zz:=0;
                        End; 
        If (Do_Naziv=Go_Naziv) or (Li_Naziv=De_Naziv) then
                Begin
                     ShowMessage('Nema dovoljno dasaka za odrediti svijetli otvor!.');
                     OK:=False;                  // procedura nije ispravno izvrsena!
                
                End;

        Writeln ('Dolje: '+Do_naziv+', y: '+ IntToStr(Do_y) + ', Debljina: '+ IntToStr(Do_yy));
        Writeln ('Gore: '+Go_naziv+', y: '+ IntToStr(Go_y) + ', Debljina: '+ IntToStr(Go_yy));

        
end;