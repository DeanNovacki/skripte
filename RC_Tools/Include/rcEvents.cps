Procedure rcCheckInt_Pos(sender:Tobject; var Key: Char); 
// allow only integer and positive when typing
// OnKeyPress
// var
  //// OldColor: TColor;
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key=#8)) then key:=#0;             

end; 

Procedure rcCheckInt(sender:Tobject; var Key: Char); 
// allow only integer typing
// OnKeyPress
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
          
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key='-') or
             (key=#45) or  (key=#8)) then key:=#0;              // 45=dash (minus), 8=backspace
end; 

Procedure rcCheckReal(sender:Tobject; var Key: Char); 
// allow only real number when typing
// OnKeyPress
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
          
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key='.') or                          
             (key='-') or (key=#8)) then key:=#0;              // 8=backspace
end; 

Procedure rcCheckStrPrg(sender:Tobject; var Key: Char); 
// allow only international alphabet and numbers when typing (and some dummy proof characters)
// OnKeyPress
Var
  Valid_Char:set of char;
Begin
  Valid_Char:=[ '0','1','2','3','4','5','6','7','8','9',
                'a','b','c','d','e','f','g','h','i','j','k','l','m',
                'n','o','p','q','r','s','t','u','v','w','x','y','z',
                'A','B','C','D','E','F','G','H','I','J','K','L','M',
                'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                '!','#','$','%','&','(',')','=','+','-','@','�',' ','.','_',#8];
  if not (key in Valid_Char) then key:=#0;

end; 

Procedure rcCheckStrInt(sender:Tobject; var Key: Char); 
// allow only international alphabet and numbers when typing (and some dummy proof characters)
// OnKeyPress
Var
  Valid_Char:set of char;
Begin
  Valid_Char:=[ '0','1','2','3','4','5','6','7','8','9','*','/',
                'a','b','c','d','e','f','g','h','i','j','k','l','m',
                'n','o','p','q','r','s','t','u','v','w','x','y','z',
                'A','B','C','D','E','F','G','H','I','J','K','L','M',
                'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                '!','#','$','%','&','(',')','=','+','-','@','�',' ','.','_',#8];
  if not (key in Valid_Char) then key:=#0;

end; 


Procedure Obrada_ShowV(VarName:String; VarValue:Extended);
// obra�uje varijable
// ovo se izvr�ava jer naziv jedne varijable po�inje sa "ShowV"
// VarName = VArijabla koja sadr�i nardbu ShowV
// VarValue = 0 ili 1 (sakrij ili poka�i)

var
   i:integer;
 	// nd,ss:string;
   // td:TDaska;
   // ne:TElement;
	// CommandString: String;	// string vrijednost 
	TargetName:string;    		// naziv varijable koju treba obraditi
	TargetValue:String;	 		// string vrjednost varijable koju treba obraditi
	TargetFormula:String;		// neizra�unati value
	TempName:string;      		// naziv varijable koja s trenutno testira
	value_str:string;			// matemati�ki izraz s desne strane
Begin
	printTime('proc Obrada_ShowV start');

	TargetName:=rcCaps(copy(VarName,7,50));
	Print ('TargetName je ' + TargetName );
   
	// varijable
	For i:=0 to e.Fvarijable.Count-1 do begin      // citanje svih varijabli
      // Puni naziv varijable
		TempName:=rcCaps(e.Fvarijable.Names[i]);    // Nazivi varijabli
		TArgetValue:= e.GetVarValueAsString(TempName);
		TargetFormula:=rcRightSide(e.Fvarijable[i]);
		print (IntToStr(i) + '. '+TempName+'='+TArgetValue+' ('+TargetFormula+')');
		
		// numeri�ka vrijednost varijable
		// VarValue:=e.Fvarijable.ParseVar(e.Fvarijable.Names[i]);
		// print (TempName + '='+FloatToStr(VarValue));
		
		// string vrijednosti varijable
		// TargetValue:=e.Fvarijable.ParseVarAsString(e.Fvarijable.Names[i]);
		// print (TempName+'='+TargetValue);
		
		// izraz
		// GetVarValueAsString( VarName : string; var Value : string; var VarFound : Boolean)');
		// TArgetValue:= e.GetVarValueAsString(TempName);
		// print (TempName+'='+TargetValue);
		
		// TempName:=e.Fvarijable[i];
		// print ('Cijeli naziv je '+TempName);
		// print ('Lijevi dio je '+rcLeftSide(TempName));
		// print ('Desni dio je '+rcRightSide(TempName));
		
		// ako je vrijednost ShowV-a = 0
		If ( TempName = TargetName ) and ( VarValue = 0 ) then begin 
			Print ('ShowC = 0, prva dva znaka su '  + Copy(TargetFormula,1,2));
			if not (Copy(TargetFormula,1,2)='0+') then begin			// ako prva dva znaka nisu '0+' 
				TargetFormula:='0+'+TargetFormula;						// stavi da budu
				e.Fvarijable[i]:=TempName+'='+TargetFormula;		// zapi�i u vrijednost
			end;
			// provjera
			// TargetFormula:=rcRightSide(e.Fvarijable[i]);
			Print ( 'Rez: '+ e.Fvarijable[i]);
		end;   // If ( TempName = 
		
		// ako je vrijednost ShowV-a = 1
		If ( TempName = TargetName ) and ( VarValue = 1 ) then begin 
			Print ('ShowC = 1, prva dva znaka su '  + Copy(TargetFormula,1,2));
			if Copy(TargetFormula,1,2)='0+' then begin				// ako su prva dva znaka '0+' 
				TargetFormula:=Copy(TargetFormula,3,150);				// makni ih
				e.Fvarijable[i]:=TempName+'='+TargetFormula;				// zapi�i u vrijednost
			end;
			
			// provjera
			// TargetFormula:=rcRightSide(e.Fvarijable[i]);
			Print ( 'Rez: '+ e.Fvarijable[i]);
			
		end; // If ( TempName = T
		
			
		
	end;  // For i:=0 to e.Fvarijable.Count-1
	
	// PrintTime('proc ShowD end____________');	
	// ShowMessage('');	
	// e.recalcformula(nil);
	rcRefresh;
	print ('refesh done');
	 
	rcRefreshVar;
	print ('refreshVAR done'); 
	
	application.processmessages;
	e.recalcformula(nil);
	
	if e=nil then print ('e je nil')
				else print ('e nije nil');
				 
	printTime('proc Obrada_ShowV end __________________');
	
End;
  
  
Procedure Obrada_ChangeO(VarName:String; VarValue:Extended);

// promjena TipaDaske koji odre�uje otvaranje vrata
// mogu�e je jedino promjena iz lijevog u desno otvaranje i obrnuto
// a to su tipovi 1 i 2

var
   i:integer;
   DaskaName,nd, TempName, TargetDaska:string;
   td:TDaska;
   ne:TElement;
Begin
	printTime('proc Obrada_ChangeO start IN: '+FloatToStr(VarValue));

	// PrintTime('proc ShowD start' + ' in: ' + VarName + ' | ' + FloatToStr(VarValue));
	
	
	TargetDaska:=rcCaps(copy(VarName,9,50));   // Desni dio od ChangeO_DaskaName
   // daske
   for i:=0 to e.childdaska.CountObj-1 do Begin      //e.childdaska.CountObj = broj dasaka u elementu
      td:=e.childdaska.Daska[i];                    //jedna daska iz elementa (s rednim brojem i)
      TempName:=rcCaps(td.Naziv);
      if TempName=TargetDaska then Begin
			
			If td.TipFronte=TTipFronte(1) then print(td.naziv+' je imala lijevo otvaranje');
			If td.TipFronte=TTipFronte(2) then print(td.naziv+' je imala desno otvaranje');
			
			if VarValue=1  then td.TipFronte := TTipFronte(1);   // postavljanje lijevih vrata
			if VarValue=2  then td.TipFronte := TTipFronte(2);   // postavljanje desnih vrata
		
			If td.TipFronte=TTipFronte(1) then print(td.naziv+' sada ima lijevo otvaranje');
			If td.TipFronte=TTipFronte(2) then print(td.naziv+' sada ima desno otvaranje');
			
		
		end;  // if TempName=TargetDaska
   end;  // for
   
	printTime('proc Obrada_ChangeO end');

End;

Procedure WaitMiliSec(mm:Integer);
var
	MyDate:tDateTime;
	nn, zz, ss: Integer;
	mili, milistart, milisec : Integer;
Begin
	MyDate:=Now;
	zz:=StrToInt(FormatDateTime('zzz', myDate));	
	ss:=StrToInt(FormatDateTime('ss', myDate));	
	nn:=StrToInt(FormatDateTime('nn', myDate));	
	mili:=zz+1000*ss+60*1000*nn;		// prera�unato u milisekunde
	miliStart:=mili;						// pam�enje poo�etnog vremena u milisekundama
	// Print('time: '+IntToStr(nn)+':'+IntToStr(ss)+':'+IntToStr(zz));
	// Print('mili: '+IntToStr(mili) );
	// Print('start: '+IntToStr(mili-miliStart) );
	application.processMessages;
	Repeat
		MyDate:=Now;
		zz:=StrToInt(FormatDateTime('zzz', myDate));	
		ss:=StrToInt(FormatDateTime('ss', myDate));	
		nn:=StrToInt(FormatDateTime('nn', myDate));	
		mili:=zz+1000*ss+60*1000*nn;
		milisec:=mili-miliStart;
		
		
		// Print('milisec: '+IntToStr(milisec) );
		// application.processMessages;
				
		// Print(ss);
	Until milisec > mm;
	application.processMessages;

End;