begin
    e.MinVisina:=100;
    e.MinSirina:=100;
    e.MinDubina:=100;
    e.MaxSirina:=1000;
    e.MaxVisina:=1000;
    e.MaxDubina:=1000;
    e.KorakV:=100;
    e.KorakS:=100;
    e.KorakD:=100;
    
    e.VKorakOn:=true;
    e.SKorakOn:=true;
    e.DKorakOn:=true;

    e.LockV:=true;
    e.LockS:=true;
    e.LockD:=true;
    e.LockN:=true;

    e.VString:='300,500,600';
    e.SString:='300,500,600';
    e.DString:='300,500,600';
    
    e.DefVisina:=1000;
    e.DefDubina:=1000;
    e.DefSirina:=1000;
    e.PolozajUcitanog:=0;
    e.PribrojiV_NStandardu:=False;
    e.PromjeniY:=False;
    e.PromjeniVisinu:=False;    
end.