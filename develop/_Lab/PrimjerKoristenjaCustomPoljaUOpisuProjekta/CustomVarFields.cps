
   
Function AddNewVar(aType:TVarDataType):TEditVar;
begin
   result:=TEditVar.create;
   result.VarDataType:=aType;
   elmholder.projectOpisData.CustomFieldList.add(result);
end;

Procedure DodajParVarijabli;
var
   aVar:TEditVar;
begin
  aVar:=AddNewVar(dtvInteger);
  aVar.MinValue:=-100;
  aVar.MaxValue:=100;
  aVar.VarValue:='50';
  aVar.Category:='TestKat';
  aVar.VarName := 'Var1';
  aVar.Caption := 'Moja prva varijabla';
  
  aVar:=AddNewVar(dtvCheck);  
  aVar.VarValue:='1';
  aVar.Category:='TestKat';
  aVar.VarName := 'Var2';
  aVar.Caption := 'Moja druga varijabla';
  aVar.ValuesList[0]:='Nije odabrano';
  aVar.ValuesList[1]:='Odabrano je';
  
  
  aVar:=AddNewVar(dtvCombo);    
  aVar.Category:='TestKat';
  aVar.VarName := 'Var3';
  aVar.Caption := 'Moja treca varijabla';
  aVar.ValuesList.Add('Vrijednost prva=1');
  aVar.ValuesList.Add('Vrijednost Druga=2');
  aVar.ValuesList.Add('Vrijednost ...=3');
  aVar.VarValue:='2';  
end;


Procedure PrintVars;
var
   i:integer;
   aVar:TEditVar;
begin
  //elmholder.projectOpisData.CustomFieldList.SortByCategory;
  for i := 0 to elmholder.projectOpisData.CustomFieldList.count-1 do begin
     aVar:=elmholder.projectOpisData.CustomFieldList.EditVar[i];
     ShowMessage(aVar.VarName+ ' , '+ aVar.Category);
  end;
end;

Procedure PrintTestKat;
var
   L:tlist;
   i:integer;
   aVar:TEditVar;
begin
  // ovo prvo sortira listu po kategorijama 
  L:=elmholder.projectOpisData.CustomFieldList.GetCategoryVar('ATestKat');
  if l<>nil then begin
  for i := 0 to L.count-1 do begin
    aVar:=TEditVar(L.items[i]);
    ShowMessage(aVar.VarName+ ' , '+ aVar.Category); 
  end;
  L.free;
  end;
end;

Procedure Test;
var
   s:string;
   i:integer;
begin
   s:=elmholder.projectOpisData.CustomFieldList.commatext;
   showmessage(s);
   s:='';
   for i := 0 to elmholder.projectOpisData.CustomFieldList.count-1 do
     s:=s+elmholder.projectOpisData.CustomFieldList[i]+#13#10;
end;
   
begin
  DodajParVarijabli;
  PrintVars;
  PrintTestKat;
  Test;
end.