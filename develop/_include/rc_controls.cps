
Procedure message(mess:string);

Begin
   try
      writeln(mess);
   except
      // i have no idea what to do!
   end;
   
end;

function find_control(root_object:TWinControl; vc_name:string):TObject;
//
// Recursive function for finding visual components on WinControl components (panel, window etc..)   
// Result is TObject (TPanel, TButton, TLabel, TEdit, TImage, TShape...)
// 
// root_object = parent object for searching (goes to subobjects also)
// c_name = name of object

var
   i:integer;
   xo:TObject;
begin
  // writeln ('Trying to found control: '+vc_name);
  for i:=0 to root_object.ControlCount-1 do begin
     // writeln ('Analising object number '+IntToStr(i));
     xo:=root_object.Controls[i];
     if xo is Tlabel       then if UpperCase(vc_name)=UpperCase(TLabel(xo).name)       then Begin result:=xo; break; end;
     if xo is Tpanel       then if UpperCase(vc_name)=UpperCase(TPanel(xo).name)       then Begin result:=xo; break; end;
     if xo is TButton      then if UpperCase(vc_name)=UpperCase(TButton(xo).name)      then Begin result:=xo; break; end;
     if xo is TEdit        then if UpperCase(vc_name)=UpperCase(TEdit(xo).name)        then Begin result:=xo; break; end;
     if xo is TMImage      then if UpperCase(vc_name)=UpperCase(TMImage(xo).name)      then Begin result:=xo; break; end;
     if xo is TShape       then if UpperCase(vc_name)=UpperCase(TShape(xo).name)       then Begin result:=xo; break; end;
     if xo is TForm        then if UpperCase(vc_name)=UpperCase(TForm(xo).name )       then Begin result:=xo; break; end;
     if xo is TScrollBox   then if UpperCase(vc_name)=UpperCase(TScrollBox(xo).name)   then Begin result:=xo; break; end;
     if xo is TListBox     then if UpperCase(vc_name)=UpperCase(TListBox(xo).name)     then Begin result:=xo; break; end;
     if xo is TGroupBox    then if UpperCase(vc_name)=UpperCase(TGroupBox(xo).name)    then Begin result:=xo; break; end;
     if xo is TScrollBar   then if UpperCase(vc_name)=UpperCase(TScrollBar(xo).name)   then Begin result:=xo; break; end;
     if xo is TRadioButton then if UpperCase(vc_name)=UpperCase(TRadioButton(xo).name) then Begin result:=xo; break; end;
     if xo is TComboBox    then if UpperCase(vc_name)=UpperCase(TComboBox(xo).name)    then Begin result:=xo; break; end;
     if xo is TCheckBox    then if UpperCase(vc_name)=UpperCase(TCheckBox(xo).name)    then Begin result:=xo; break; end;
     if (result=nil) and (xo is TWinControl) then result:=find_control(TWinControl(xo),vc_name);
 
  end;  // for 
end;  // function
{
function find_control(root_object:TWinControl; vc_name:string):TObject;
//
// Recursive function for finding visual components on WinControl components (panel, window etc..)   
// Result is TObject (TPanel, TButton, TLabel, TEdit, TImage, TShape...)
// 
// root_object = parent object for searching (goes to subobjects also)
// c_name = name of object

var
   i:integer;
   xo:TObject;
begin
  // writeln ('Trying to found control: '+vc_name);
  for i:=0 to root_object.ControlCount-1 do begin
     // writeln ('Analising object number '+IntToStr(i));
     xo:=root_object.Controls[i];
     if xo is Tlabel       then if UpperCase(vc_name)=UpperCase(TLabel(xo).name)       then result:=xo; 
     if xo is Tpanel       then if UpperCase(vc_name)=UpperCase(TPanel(xo).name)       then result:=xo;
     if xo is TButton      then if UpperCase(vc_name)=UpperCase(TButton(xo).name)      then result:=xo;
     if xo is TEdit        then if UpperCase(vc_name)=UpperCase(TEdit(xo).name)        then result:=xo;
     if xo is TMImage      then if UpperCase(vc_name)=UpperCase(TMImage(xo).name)      then result:=xo;
     if xo is TShape       then if UpperCase(vc_name)=UpperCase(TShape(xo).name)       then result:=xo;
     if xo is TForm        then if UpperCase(vc_name)=UpperCase(TForm(xo).name )       then result:=xo; 
     if xo is TScrollBox   then if UpperCase(vc_name)=UpperCase(TScrollBox(xo).name)   then result:=xo;
     if xo is TListBox     then if UpperCase(vc_name)=UpperCase(TListBox(xo).name)     then result:=xo;
     if xo is TGroupBox    then if UpperCase(vc_name)=UpperCase(TGroupBox(xo).name)    then result:=xo;
     if xo is TScrollBar   then if UpperCase(vc_name)=UpperCase(TScrollBar(xo).name)   then result:=xo;
     if xo is TRadioButton then if UpperCase(vc_name)=UpperCase(TRadioButton(xo).name) then result:=xo;
     if xo is TComboBox    then if UpperCase(vc_name)=UpperCase(TComboBox(xo).name)    then result:=xo;
     if xo is TCheckBox    then if UpperCase(vc_name)=UpperCase(TCheckBox(xo).name)    then result:=xo;
     if (result=nil) and (xo is TWinControl) then result:=find_control(TWinControl(xo),vc_name);
 
  end;  // for 
end;  // function
}


Function rcPanelHeight(root_object:TWinControl):Integer;
 // calculate necessary height for  panel
 // measure PANELS that are aligned alTop or AlBottom
var
   i,j:Integer;
   xo:TObject;
   xP:TPanel;
Begin
   j:=0;
   // ShowD('---------');
   // ShowD('rcPanelHeight('+TPanel(root_object).Name+') Started');
   for i:=0 to root_object.ControlCount-1 do begin
      xo:=root_object.Controls[i];
      if xo is Tpanel then Begin
         xP:=TPanel(xo);
         if (xP.align=alTop) or (xP.align=alBottom) then begin 
                                                            // ShowD(IntToStr(j)+'. Panel: '+xp.Name+' vis: '+IntToStr(xp.Height));
                                                            result:=result+xP.Height;
                                                            j:=j+1;
                                                         End;
      
      End;
   
   end;
   // ShowD('Panel "'+TPanel(root_object).name+'" ima '+IntToStr(j)+' panela u sebi.'); 
   // ShowD('rcPanelHeight('+TPanel(root_object).Name+') Ended');
End;


function xfind_control(root_object:TWinControl; vc_name:string):TObject;
// isto kao i Find_Control, ali ispisuje sve �to je prona�eno

// Recursive function for finding visual components on WinControl components (panel, window etc..)   
// Result is TObject (TPanel, TButton, TLabel, TEdit, TImage, TShape...)
// 
// root_object = parent object for searching (goes to subobjects also)
// c_name = name of object

var
   i:integer;
   xo:TObject;
   //
   xForm:TForm;
   xMemo:TMemo;
begin
  
  xForm:=TForm.create(nil);
  xForm.FormStyle:=fsStayOnTop;
  xMemo:=TMemo.create(xForm);
  xMemo.parent:=xForm;
  xMemo.Align:=alClient;
  
  
   
  // writeln ('Trying to found control: '+vc_name);
  for i:=0 to root_object.ControlCount-1 do begin
     // writeln ('Analising object number '+IntToStr(i));
     xo:=root_object.Controls[i];
     if xo is Tlabel       then Begin
               xMemo.Lines.Add('Label: '+TLabel(xo).name);   
               if UpperCase(vc_name)=UpperCase(TLabel(xo).name)       then result:=xo; 
               end;
     if xo is Tpanel       then begin 
               xMemo.Lines.Add('Panel: '+TPanel(xo).name);   
               if UpperCase(vc_name)=UpperCase(TPanel(xo).name)       then result:=xo;
               End;
     if xo is TButton      then begin
               xMemo.Lines.Add('Button: '+TButton(xo).name);   
               if UpperCase(vc_name)=UpperCase(TButton(xo).name)      then result:=xo;
               End;
     if xo is TEdit        then begin 
               xMemo.Lines.Add('Edit: '+TEdit(xo).name);   
               if UpperCase(vc_name)=UpperCase(TEdit(xo).name)        then result:=xo;
               end;
     if xo is TMImage      then if UpperCase(vc_name)=UpperCase(TMImage(xo).name)      then result:=xo;
     if xo is TShape       then if UpperCase(vc_name)=UpperCase(TShape(xo).name)       then result:=xo;
     if xo is TForm        then if UpperCase(vc_name)=UpperCase(TForm(xo).name )       then result:=xo; 
     if xo is TScrollBox   then if UpperCase(vc_name)=UpperCase(TScrollBox(xo).name)   then result:=xo;
     if xo is TListBox     then if UpperCase(vc_name)=UpperCase(TListBox(xo).name)     then result:=xo;
     if xo is TGroupBox    then if UpperCase(vc_name)=UpperCase(TGroupBox(xo).name)    then result:=xo;
     if xo is TScrollBar   then if UpperCase(vc_name)=UpperCase(TScrollBar(xo).name)   then result:=xo;
     if xo is TRadioButton then if UpperCase(vc_name)=UpperCase(TRadioButton(xo).name) then result:=xo;
     if xo is TComboBox    then if UpperCase(vc_name)=UpperCase(TComboBox(xo).name)    then result:=xo;
     if xo is TCheckBox    then if UpperCase(vc_name)=UpperCase(TCheckBox(xo).name)    then result:=xo;
     if (result=nil) and (xo is TWinControl) then result:=find_control(TWinControl(xo),vc_name);
 
  end;  // for 
  xForm.Show;
end;  // function

Function xFind_Edit(root_object:TWinControl; x_name:string):TEdit;
var
   xo:TObject;
Begin
   xo:=xFind_Control(root_object, x_name);
   if xo=nil then ShowMessage('Object not exist! (proc: Find_Edit)')
             else If xo is TEdit then result:=TEdit(xo)
                                  else ShowMessage('Edit "'+ TEdit(xo).name + '" not exist!');
End;




Function Find_Button(root_object:TWinControl; x_name:string):TButton;
var
   xo:TObject;
Begin
   xo:=Find_Control(root_object, x_name);
   if xo=nil then ShowMessage('Object not exist! (proc: Find_Button)')
             else If xo is TButton then result:=TButton(xo)
                                   else ShowMessage('Button "'+ TButton(xo).name + '" not exist!');
End;

Function Find_Edit(root_object:TWinControl; x_name:string):TEdit;
var
   xo:TObject;
Begin
   xo:=Find_Control(root_object, x_name);
   if xo=nil then ShowMessage('Object not exist! (proc: Find_Edit)')
             else If xo is TEdit then result:=TEdit(xo)
                                  else ShowMessage('Edit "'+ TEdit(xo).name + '" not exist!');
End;



Function Find_Panel(root_object:TWinControl; x_name:string):TPanel;
var
   xo:TObject;
   
Begin
   xo:=Find_Control(root_object, x_name);
   if xo=nil then ShowMessage('Object not exist! (proc: FindPanel)')
             else If xo is TPanel then result:=TPanel(xo)
                                  else ShowMessage('Panel "'+ TPanel(xo).name + '" not exist!');
End;

Function Find_Label(root_object:TWinControl; x_name:string):TLabel;
var
   xo:TObject;
   
Begin
   xo:=Find_Control(root_object, x_name);
   if xo=nil then ShowMessage('Object not exist! (proc: Find_Label)')
             else If xo is TLabel then result:=TLabel(xo)
                                  else ShowMessage('Label "'+ TLabel(xo).name + '" not exist!');
End;

Function xFind_Label(root_object:TWinControl; x_name:string):TLabel;
var
   xo:TObject;
   
Begin
   xo:=xFind_Control(root_object, x_name);
   if xo=nil then ShowMessage('Object not exist! (proc: Find_Label)')
             else If xo is TLabel then result:=TLabel(xo)
                                  else ShowMessage('Label "'+ TLabel(xo).name + '" not exist!');
End;

Function FindSystemName(aName:string):string;
var
   k:integer;
   s:string;
begin
   result:='';
   
   For k:=1 to Length(aName) do begin
      s:=Copy(aName,k,1);
      // ShowMessage(' Ime = "'+aName+'" Slovo broj '+IntToStr(k)+' je "' + S );
      if s='_'  then break
                else Result:=Result+s;
   
   end;  // For k:=1 
End;

Function Find_ScrollBar(root_object:TWinControl; x_name:string):TScrollBar;
var
   xo:TObject;
   
Begin
   xo:=Find_Control(root_object, x_name);
   if xo=nil then ShowMessage('Object not exist! (proc: FindScrollBar)')
             else If xo is TScrollBar then result:=TScrollBar(xo)
                                  else ShowMessage('ScrollBar "'+ TScrollBar(xo).name + '" not exist!');
End;

Function rcScrollBar(sName: string; sDest: TWinControl):TScrollBar;
var
     s1:TScrollBar;
     xo:TObject;
 begin
      if sDest=nil then showmessage('Parent for ScrollBar "'+sName+'" not exist!');
      if sName<>'' then begin 
                       xo:=nil; 
                       find_control(sDest,sName);
                       if xo<>nil then Begin 
                          ShowMessage ('Edit name: "'+sName+'" already exist!'); 
                          exit 
                       end;
                   end;              
     s1:=TScrollBar.create(sDest);
     s1.parent:=sDest;
     if sName<>'' then s1.name:=sName;
     // s1.Left:=sX; s1.Top:=sY;
     s1.Height:=10;
     s1.showhint:=true;
     result:=s1;
 end;
 
Procedure rcSetScrollBar(iScrollBar:TScrollBar;iMin,iMax,iPos:Integer);
Begin
   iScrollBar.min:=iMin;
   iScrollBar.max:=iMax;
   iScrollBar.position:=iPos;
End;
    
function rcBox(nParent:TWinControl;X,Y,W,H:Integer):TShape;
var B:TShape;
Begin
      // if LParent=nil then showmessage('Parent for Label "'+LName+'/'+LCaption+'" not exist!');
     B:=TShape.create(nParent);
     B.parent:=nParent;
     B.Shape:=stRectangle;
     B.Left:=x;B.Top:=y;B.width:=w;B.Height:=H;
     result:=B;
end;

function rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
// Talignment: taLeftJustify, taCenter, taRightJustify 
 var
   Text_1:TLabel;
   xo:TObject;
 begin
     if LParent=nil then showmessage('Parent for Label "'+LName+' with caption "'+lCaption+'" not exist!');
     if lName<>'' then begin 
                       xo:=nil; 
                       find_control(LParent,LName);
                       if xo<>nil then Begin 
                          ShowMessage ('Label name: "'+LName+' with caption "'+lCaption+'" already exist!'); 
                          exit 
                       end;
                   end;    
     Text_1:=TLabel.create(LParent);
     Text_1.parent:=LParent;
     if LName<>'' then Text_1.name:=LName;
     Text_1.caption:=LCaption; 
     Text_1.font.name:='arial';
     // Text_1.font.size:=9;
     Text_1.ParentFont:=True;
     Text_1.Alignment:=LA;
     // if Align='right' then Text_1.left:=nX-text_1.width
     //                      else Text_1.left:=nX;
     Text_1.left:=LX;
     Text_1.top:=LY;
     Text_1.ShowHint:=True;
     result:=Text_1;
     
 end; 

function rcWindow(wName, wCaption: string; wS, wV: integer):TForm;
// create window
 var
     wForm:TForm;
    // xo:TObject;
 begin
     wForm:=TForm.create(nil);           // creating
     if wName<>'' then wForm.name:=wName;
     wForm.caption:=wCaption;
     // wForm.Align:=alNone;              // alBottom, alCustom?, alLeft, alRight, alTop, alClient 
     // wForm.AlphaBlend:=True;             // not work
     // wForm.AutoScroll:=True;
     // wForm.AutoSize:=                    // not work
     // wForm.BorderIcons:=[biSystemMenu,biMinimize,biMaximize]; 
      
     wForm.BorderStyle:=bsSingle;           // bsNone = no border, no caption
                                            // bsSizeToolWin 
                                            // bsDialog
                                            // bsSingle
                                            // bsSizeable
                                            // bsToolWindow
     // wForm.BorderWith:=0;                // not work 
     // wForm.Color:=clDefault;             // clRed, $00bbggrr
     // wForm.Cursor:=crDefault;            // work but not always
     // wForm.enabled:=true;                
     wForm.font.name:='Arial';
     wForm.font.size:=10;
     // wForm.font.style:=[];              // ;[fsBold,fsItalic,fsStrikeOut,fsUnderline]
     wForm.FormStyle:=fsNormal;            // fsStayOnTop;      // on top in Corpus
                                           // fsMDIChild;     // replace Project window :-) but imposible to close
                                           // fsMDIForm       // normal form
                                           // fsSplash        // not work
                                           // fsStayOnTop
                                           
     wForm.Width:=wS;                      // Screen.Width; 
     wForm.Height:=wV;                     // Screen.Height;
     // wForm.HorzScrollBar.Range := 1000;
     // wForm.DefaultMonitor:=dmDesktop;   // not work
     wForm.position:=poScreenCenter;
     // wForm.Constraints.MaxHeight = 300; // not work
     
     wForm.WindowState:=wsNormal;          // wsMaximized, wsMinimized
     
     result:=wForm;
     
     // events
     // nForm.OnResize:=@fhdjks;
 end;


function rcProjectName:string;
var
   cap,str:string;
   i,ci:Integer;
Begin
   cap:=application.MainForm.caption;
   ci:=Length(cap);
   repeat 
	   str:=copy(cap,ci,1);
	   ci:=ci-1;
		
   until str='\';
	result:=copy(cap,ci+2,Length(cap)-ci-6);
	if length(result)<1 then result:='PROJEKT BEZ IMENA';
   // rcOut('na�ao sam backslash');
	// rcout('Naziv projekta je '+result);
End; 

function rcProjectLongName:string;
var
   cap,str:string;
   i,ci:Integer;
Begin
   cap:=application.MainForm.caption;
   ci:=Length(cap);
   repeat 
	   str:=copy(cap,ci,9);
	   ci:=ci-1;
		// rcOut(IntToStr(ci)+' '+str);
   until (UpperCase(str)=UpperCase('\Sobasav\')) or (ci<0);
	result:=copy(cap,ci+10,Length(cap)-ci-10-4);
	if length(result)<1 then result:='PROJEKT BEZ IMENA';
   // rcOut('na�ao sam "Sobasav"');
	// rcout('Dugi naziv projekta je '+result);
End;

function rcProjectPath:string;
var
   cap,str:string;
   i,ci:Integer;
Begin
   cap:=application.MainForm.caption;
   ci:=Length(cap);
   repeat 
	   str:=copy(cap,ci,1);
	   ci:=ci-1;
		// rcOut(IntToStr(ci)+' '+str);
   until (UpperCase(str)=UpperCase(':')) or (ci<0);
	result:=copy(cap,ci,Length(cap)-ci);
	if length(result)<1 then result:='PROJEKT BEZ IMENA';
   // rcOut('na�ao sam "Sobasav"');
	// rcout('Dugi naziv projekta je '+result);
End;

function rcDanas:String;
var
	dd:TDateTime;
   vv:string;
Begin
	dd:=now;
   // vv:=TimeToStr(dd);            NE RADI!!!
   // vv:=DateTimeToStr(dd);        NE RADI!!!
   // vv:=ShortDateFormat(dd);      NE RADI!!!
   vv:= FormatDateTime('hh nn ss',dd);
   
 
   
	result:=DateToStr(dd);
End;
 
Function ScreenWidth:integer;
// Find width of monitor screen
 var
     nForm:TForm;
 begin
     nForm:=TForm.create(nil);
     nForm.BorderStyle:=bsNone;        
     nForm.WindowState:=wsMaximized;
     nForm.show;
	 Application.ProcessMessages;
	 // showmessage('Function ScreenWidth');
	 result:=nForm.clientwidth;
   nForm.free;
   // nForm.ModalResult := 1;
 end; 

Function ScreenHeight:integer;
// Find width of monitor screen
 var
     nForm:TForm;
 begin
     nForm:=TForm.create(nil);
     nForm.BorderStyle:=bsNone;        
     nForm.WindowState:=wsMaximized;
     nForm.show;
	 Application.ProcessMessages;
	 // showmessage('Function ScreenHeight');
	 result:=nForm.clientheight;
	 nForm.free;
    // nForm.ModalResult := 1;
 end; 

Procedure rcRefreshFix;
// set Main form to normal state (vs FullScreen) to enable changing window size for rcRefresh
// 
var    x,y,w,h:integer;
Begin
   // check state
   if application.MainForm.WindowState=wsMaximized then begin
      // showmessage('MAX');
   //   x:=0;y:=0;w:=Screenwidth;h:=ScreenHeight;
      x:=application.MainForm.left;
      y:=application.MainForm.top;
      w:=application.MainForm.width;
      h:=application.MainForm.height;
      application.MainForm.WindowState:=wsNormal;
      application.MainForm.left:=x;
      application.MainForm.top:=y;
      application.MainForm.width:=w;
      application.MainForm.height:=h;
   end;
end;

 
Function rcCreateToolForm(tName:string;x,y,w,h:Integer):TForm;
// form with custom caption, after creation MUST use show or showmodal
 
var 
// L1,L2,L3,L4,L5:TShape;cap:TLabel;
TF:TForm;
Begin
    TF:=TForm.create(nil);
    TF.Left:=x;TF.Top:=y;TF.Width:=w;TF.Height:=h;
    TF.FormStyle:=fsStayOnTop; TF.BorderStyle:=bsToolWindow; //bsSizeToolWin
    TF.Name:=tName;
    // TF.Caption:=tCaption;
    result:=TF;
End; 
 
Procedure rcRefresh; // refresh main Aplication window
var F:TForm; refresh_type:integer;
Begin 
   refresh_type:=1;
   case refresh_type of 
   0 : Begin  
            // do nothing;
       End;
   1 : Begin   // change height of main Form
            application.MainForm.height:=application.MainForm.height+1;
            application.MainForm.height:=application.MainForm.height-1;
       End;
   2 : Begin   // change width of main Form
            application.MainForm.width:=application.MainForm.width+1;
            application.MainForm.width:=application.MainForm.width-1;
       End;
   3 : Begin   // show form at full screen then close it - slow and blinking
            F:=TForm.create(application);
            F.WindowState:=wsMaximized;
            F.BorderStyle:=bsNone;
            F.color:=clGray;
            F.Show;
            F.free;
            APPLICATION.processmessages;
       End;
   End;
   
{
   
}   
End; 


function rcScrollBox( sName: string; sParent: TWinControl):TScrollBox;

var
   ScrollBox_1:TScrollBox;
   xo:TObject;
begin
     if sParent=nil then showmessage('Parent for ScrollBox "'+sName+'" not exist!'); 
     if sName<>'' then begin 
                       xo:=nil; 
                       find_control(sParent,sName);
                       if xo<>nil then Begin 
                          ShowMessage ('ScrollBox name: "'+sName+'" already exist!'); 
                          exit 
                       end;
                   end;    
     ScrollBox_1:=TScrollBox.create(sParent);
     if sName<>'' then ScrollBox_1.name:=sName;
     ScrollBox_1.parent:=sParent;
     ScrollBox_1.VertScrollBar.Visible:=True;
     ScrollBox_1.VertScrollBar.Tracking:=True;   
     ScrollBox_1.HorzScrollBar.Visible:=False;
     // ScrollBox_1.VertScrollBar.size:=10; not work :-(
     
     //ScrollBox_1.align:=alNone;    // alNone, alTop, alBottom, alLeft, alRight, alClient
     //ScrollBox_1.caption:= ????????
     //ScrollBox_1.BevelInner:=bvNone;
     //ScrollBox_1.BevelOuter:=bvRaised;
     ScrollBox_1.BorderStyle:=bsNone;
     ScrollBox_1.ParentFont:=True;
     //ScrollBox_1.BorderWidth:=0;
     //ScrollBox_1.Caption:='';
     //ScrollBox_1.Left:=100;
     //ScrollBox_1.Top:=100;
     //ScrollBox_1.Width:=300;
     // ScrollBox_1.Height:=300;
     // ScrollBox_1.FullRepaint:=True;
     result:=ScrollBox_1;
end;

function rcListBox( sName: string; sParent: TWinControl):TListBox;

var
   ListBox_1:TListBox;
   xo:TObject;
begin
     if sParent=nil then showmessage('Parent for ListBox "'+sName+'" not exist!'); 
     if sName<>'' then begin 
                       xo:=nil; 
                       find_control(sParent,sName);
                       if xo<>nil then Begin 
                          ShowMessage ('ListBox name: "'+sName+'" already exist!'); 
                          exit 
                       end;
                   end;    
     ListBox_1:=TListBox.create(sParent);
     if sName<>'' then ListBox_1.name:=sName;
     ListBox_1.parent:=sParent;
     ListBox_1.ParentFont:=True;
     result:=ListBox_1;
end;


function rcPanel( pName: string; pParent: TWinControl):TPanel;
// procedura koja radi panel
// 
// 
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira labela
 var
   Panel_1:TPanel;
   xo:TObject;
 begin
     if pParent=nil then showmessage('Parent for Panel "'+pName+'" not exist!'); 
     if pName<>'' then begin 
                       xo:=nil; 
                       find_control(pParent,pName);
                       if xo<>nil then Begin 
                          ShowMessage ('Label name: "'+pName+'" already exist!'); 
                          exit 
                       end;
                   end;    
     Panel_1:=Tpanel.create(pParent);
     if pName<>'' then Panel_1.name:=pName;
     //Panel_1.align:=alNone;    // alNone, alTop, alBottom, alLeft, alRight, alClient 
     Panel_1.parent:=pParent;
     Panel_1.BevelInner:=bvNone;
     Panel_1.BevelOuter:=bvNone; // bvRaised;
     Panel_1.BevelWidth:=0;
     Panel_1.BorderStyle:=bsNone;
     Panel_1.BorderWidth:=0;
     Panel_1.Ctl3D:=False;
     
     Panel_1.Caption:='';
     Panel_1.Left:=100;
     Panel_1.Top:=100;
     Panel_1.Width:=300;
     Panel_1.Height:=300;
     //Panel_1.BevelInner:=bvNone;
     //Panel_1.BevelOuter:=bvNone;




     //PanelP1.Font.style:=[fsBold];
     Panel_1.Font.Name:='Arial';
     // Panel_1.Font.Size:=10;
     Panel_1.ParentFont:=True;
     // ne radi Panel_1.FullRepaint:=True;
     result:=Panel_1;
     Panel_1.ShowHint:=True;
end;

function rcButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TButton;
// Create Button
 var
   Button_1:TButton;
   xo:TObject;
 begin
     if bParent=nil then showmessage('Parent for Button "'+bName+'" not exist!'); 
     if bName<>'' then begin 
                       xo:=nil; 
                       find_control(bParent,bName);
                       if xo<>nil then Begin 
                          ShowMessage ('Label name: "'+bName+'" already exist!'); 
                          exit 
                       end;
                   end;     
     Button_1:=TButton.create(bParent);
     Button_1.parent:=bParent;
     if bName<>'' then Button_1.name:=bName;  
     Button_1.caption:=bCaption;
     Button_1.Left:=bX;
     Button_1.Top:=bY;
     Button_1.Width:=120;
     Button_1.Height:=25
     Button_1.ShowHint:=true;
     Button_1.Font.Name:='Arial';
     // Button_1.Font.Size:=9;
     Button_1.ParentFont:=True;
     result:=Button_1;
 end;



 
Procedure MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);                 
// var

begin
	writeln('Mouse move! X='+IntToStr(x)+' Y='+IntToStr(y));
	
	// if sender.parent=TPanel then showmessage('Panel!');
	
	{
        for i:=0 to AP[AP_count].controlcount-1 do begin           // all objects
         if (AP[AP_count].controls[i] is TEdit) then             // if edit fields
            case AP[AP_count].controls[i].tag of
            
         }    
End; 


// *******************************************************************************************

function rcTool(name,caption:string;parent:TWinControl;left,top,width,height:integer;open:boolean):TPanel;

// rcTool is panel for parenting controls 

var
   Panel_1, Panel_2:TPanel;
   header:TLabel;
   bord:TShape;
begin
     // icon_path:=PrgFolder+'skripte\RC_Wizards\Global\Icons\'; 
     if Parent=nil then showmessage('Parent for Panel "'+Name+'" not exist!'); 
     Panel_1:=Tpanel.create(parent);
     Panel_1.parent:=parent;
     Panel_1.Name:=Name;
     Panel_1.Left:=left;
     Panel_1.Top:=top;
     Panel_1.Width:=width;
     Panel_1.Height:=height;
     Panel_1.Font.Name:='Arial';
     Panel_1.Font.Size:=10;
	  Panel_1.BorderStyle:=bsSizeToolWin;
														  // bsNone = no border, no caption
                                            // bsSizeToolWin   // nema ruba,  ima resizea, nema icone
                                            // bsDialog  // nema ruba nema resizea
                                            // bsSingle - nema ruba,  nema resizea, ima ikone
                                            // bsSizeable - nema ruba,  ima resizea, ima ikone
                                            // bsToolWindow - nema ruba,  nema resizea, nema ikone	
	  
     Panel_2:=Tpanel.create(panel_1);
     Panel_2.parent:=panel_1;
     Panel_2.Name:='header_'+Name;
     Panel_2.tag:=1;
     Panel_2.Left:=0;
     Panel_2.Top:=0;
     Panel_2.Width:=Panel_1.clientwidth;
     Panel_2.Height:=24;
     Panel_2.color:=clSilver;
     
     // transparent rectangle for mouseClickControl
     bord:=Tshape.create(panel_2);
     bord.parent:=panel_2;
     bord.left:=0;
     Bord.top:=0;
     bord.width:=Panel_2.width;
     bord.height:=Panel_2.height;
     bord.brush.style:=bsClear;         
     bord.pen.style:=psClear;
     bord.name:='bar_'+Name;
     // RCP[i].bar:=bord;
     
     Header:=rcLabel( 'rcToolHeader', AnsiUpperCase(caption),panel_2,10,5,TaLeftJustify);
     header.font.style:=[fsBold];
     header.font.size:=9;
     
     // bord.OnMouseUp:=@caption_onClick;
     result:=Panel_1;
end;


procedure EditOnClick(sender:TObject);
// POPRAVLJA BUG!!!!!!
// Ako se ne mo�e selektirati u Edit onda treba pozvati ovu proceduru
begin
// Showmessage('Click to Edit');
 TEdit(sender).SetFocus;
 TEdit(sender).SelStart := 0;
 // TEdit(sender).SelLength := 0; // or for selecting al text: Length(Tedit(sender).Text);
 TEdit(sender).SelLength := Length(Tedit(sender).Text);

end;  

Function rcEdit(eName: String; eParent:TWinControl):TEdit;
var
   ee:Tedit;
   xo:TObject;
 begin
   if eParent=nil then showmessage('Parent for Edit "'+eName+'" not exist!');
   if eName<>'' then begin 
                       xo:=nil; 
                       find_control(eParent,eName);
                       if xo<>nil then Begin 
                          ShowMessage ('Label name: "'+eName+'" already exist!'); 
                          exit 
                       end;
   end;  
   ee:=TEdit.create(eParent);
   ee.parent:=eParent;
   if eName<>'' then ee.name:=eName;
   
   // ee.Text:=eText;
   // ee.Left:=eX;
   // ee.Top:=eY;
   // ee.font.name:='Arial';
   // ee.font.size:=9;
   ee.ParentFont:=True;
   ee.height:=20;
   ee.width:=150;
   ee.showhint:=true;
   // ee.BevelInner:=bvNone;  // NE RADI
   // ee.BorderStyle:=bsNone; // NE RADI
   ee.Ctl3D:=False;
   
   /////////////////////////////////////////////////////
   // ZBOG BUG-a U TEDIT-u 
   /////////////////////////////////////////////////////
   ee.OnClick:=@EditOnClick;                          //
   /////////////////////////////////////////////////////
   result:=ee;
end;  


Procedure rcCheckInt_Pos(sender:Tobject; var Key: Char); 
// allow only integer and positive when typing
// var
  //// OldColor: TColor;
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key=#8)) then key:=#0;             

end; 

Procedure rcCheckInt(sender:Tobject; var Key: Char); 
// allow only integer typing
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
          
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key='-') or
             (key=#45) or  (key=#8)) then key:=#0;              // 45=dash (minus), 8=backspace
end; 

Procedure rcCheckReal(sender:Tobject; var Key: Char); 
// allow only real number when typing
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
          
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key='.') or                          
             (key='-') or (key=#8)) then key:=#0;              // 8=backspace
 end; 

Procedure rcCheckStrPrg(sender:Tobject; var Key: Char); 
// allow only international alphabet and numbers when typing (and some dummy proof characters)

Var
  Valid_Char:set of char;
Begin
  Valid_Char:=[ '0','1','2','3','4','5','6','7','8','9',
                'a','b','c','d','e','f','g','h','i','j','k','l','m',
                'n','o','p','q','r','s','t','u','v','w','x','y','z',
                'A','B','C','D','E','F','G','H','I','J','K','L','M',
                'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                '!','#','$','%','&','(',')','=','+','-','@','�',' ','.','_',#8];
  if not (key in Valid_Char) then key:=#0;

end; 

   // ED_twf.OnKeyPress:=@Check_Str_Prg;
   // ED_twf.OnKeyPress:=@Check_Str_Eng;
   // ED_twf.OnKeyPress:=@Check_Str_Cro;  
 
 
 
function rcRadioBox(nTekst: string; nazivi:Array of String; def:integer; 
                          nProzor: TWinControl; nX, nY: integer):TRadioGroup;
// kreira Radio Grupu
// nTekst = Naslov Radio Grupe
// nazivi = niz koji odre�uje check boxove ['prvi','drugi', ...]
// def = defaultni check
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira edit i labela
// nX = udaljenost od lijevog ruba
// nY = udaljenost od vrha

 
 var
     RB_1:TRadioGroup;
     I:Integer;
 begin
     if nProzor=nil then showmessage('Parent for RadioBox "'+nTekst+'" not exist!'); 
     RB_1:=TRadioGroup.create(nProzor);
     RB_1.parent:=nProzor;
     RB_1.caption:=' '+nTekst+' ';
     For i:=0 to GetArrayLength(nazivi)-1 do 
         begin
              RB_1.Items.Add(nazivi[i]);
         end;
     If def>RB_1.Items.Count-1 then def:=RB_1.Items.Count-1;
     RB_1.Height:=10+RB_1.Items.Count*20;
     RB_1.ItemIndex:=def;
     RB_1.Left:=nX;
     RB_1.Top:=nY;
     RB_1.ParentFont:=True;
     result:=RB_1;
 end;

Function rcImage(iName,iPath,iFile:string; iParent: TWinControl; ix,iy:Integer):TMImage;
// load image from disk and place to parent at xx,yy position 
// e.g. 
// pic:=rcImage('pic_01','c:\Good_Pictures\','img1.jpg',panel6, 30,10);

var
   L_Image:TMImage;
   ok:boolean; 
   xo:TObject;  
begin
   OK:=true;
   if iParent=nil then ShowMessage ('rcImage! Parent not exist!');
   if iName<>'' then begin 
                     xo:=nil; 
                     find_control(iParent,iName);
                     if xo<>nil then Begin 
                                     ShowMessage ('Image name: "'+iName+'" already exist!'); 
                                     exit
                                end; 
                end;
   L_Image:=TMImage.create(iParent);
   L_Image.parent:=iParent;
   try 
      L_Image.loadFromFile(iPath+iFile);
   except
      showmessage('image: "'+iPath+iFile+'" not found!');
      OK:=false;
   end;
   if OK then begin 
       // L_Image.Stretch:=True;
       if iName<>'' then L_Image.Name:=iName;
       L_Image.Left:=ix;       
       L_Image.Top:=iy;
       //L_Image.Width:=iw; 
       //L_Image.Height:=ih;
       L_Image.Showhint:=true;  
       L_Image.AutoSize:=True;
       L_Image.Center:=True;
   end;
   result:=L_Image;
end;
 {
// RC_Image_Edit_Create
// Create image on position left, top 
// On the right side of image creat one, two or three edit fields
// Every edit field have label on right side 
// Start_tag is three digits integer (e.g. 701)
// Start tag is first number in array that uniquelly identify edit field for change event.   

Procedure RC_Image_Edit_Create    (
                  	destination         :TWinControl;  
                  	image_name          :string;      
                  	xx                  :integer;
                  	yy                  :integer;
					image_width         :integer;
                  	image_height        :integer;
					start_tag           :integer;
                  	quantity            :integer;
                    field_1_label	    :string;
					field_1_hint	    :string;
					field_2_label	    :string;
					field_2_hint	    :string;
                    field_3_label       :string;
					field_3_hint	    :string;
					units               :string
			);

var
   i  :integer;

   ced:array[1..3] of TEdit;
Begin
   
   rcLoadImage ('', image_name,    // image_path
              destination,  // parent
              xx,           // left
              yy,           // top
              image_width,  // image_width
              image_height  // image_height
              );
   
   for i:=1 to quantity do begin
         if i=1 Then ced[i]:=rcEditL(field_1_label, units, destination, xx+image_width+10, yy+(i-1)*50, 'top-left');
         if i=2 Then ced[i]:=rcEditL(field_2_label, units, destination, xx+image_width+10, yy+(i-1)*50, 'top-left');
         if i=3 Then ced[i]:=rcEditL(field_3_label, units, destination, xx+image_width+10, yy+(i-1)*50, 'top-left');
         ced[i].tag:=start_tag-1+i;
         case i of
              1   :  ced[i].hint:=field_1_hint;
              2   :  ced[i].hint:=field_2_hint;
              3   :  ced[i].hint:=field_3_hint;
         end;  // case
   end;   // for i        
end;    // proc
}

			
{
// RC_Radio_Image_Create
// First, create rectangular shape with caption. 
// Then, inside shape cretae pictures with captions. 
// Start_tag is three digits integer (e.g. 701)
// Start tag is first number in array that uniquelly identify pictures for click event.   
// A pair of true and false picture has same tag
// 'ON' image will be placed ower the 'OFF' image
// Visible property of 'ON' image mark if image is selected or not
// Finall image names structure:   tag + on/of + 'name' + '.jpg'
// Image descruptions are separated with '|' sign

Procedure RC_Radio_Image_Create  (
                  destination        :TWinControl;  
                  caption            :string;
                  start_tag          :integer;
                  quantity           :integer;
                  default            :integer;
                  left               :integer;
                  top                :integer;
                  width              :integer;
                  image_width        :integer;
                  image_height       :integer;
                  image_names        :string;                // tag+on/of+'name'+'.jpg'
                  image_descriptions :string;                // separated with '|' sign
                  script_path        :string
                  );     

var
   image_path_off, image_path_on :string;
   Box:TShape;
   h, Img_label:Tlabel; 
   img_on, img_off: TMimage;
   img_caption: Array of string; 
   i, ind,min_distance, x_number, x_distance,row,number_of_rows:Integer;
   Show_hint:boolean;     // showing tag in hint for testing 
   xx,yy:Integer;

Begin
    
    // Showmessage('RC_Radio_Image_Create');
    
    Show_hint:=false;
    
    // creating box 
    Box:=TShape.create(destination);
    Box.parent:=destination;
    Box.shape:=stRectangle;
    Box.Left:=left;
    Box.Top:=top;
    Box.Width:=width;
    Box.Brush.color:=clBtnFace; 
    H:= TLabel.create(destination);
    H.parent:=destination;
    H.Font.Name:='Arial';
    H.Font.Size:=10;
    H.caption:=' '+caption+' ';   
    H.Left:=left+10;
    H.Top:=top-10;
    
    // Showmessage('Box created');
    
    // unpack captions from image_descriptions which contain strings separated with '|' sign
    SetArrayLength(img_caption, quantity+1); 
    ind:=1;
    Img_caption[ind]:='';
    
    // if Start_Tag>700 then Showmessage ('Looking for Radio Image caption. Tag: '+IntToStr(Start_tag));
    
    for i:=1 to Length(image_descriptions) do begin
        try 
            if image_descriptions[i] <> '|' then Img_caption[ind]:=Img_caption[ind]+image_descriptions[i]    // add one string from description
                                        else ind:=ind+1;
        except
            showmessage( 'Wrong size of images descriptions for images '+
                         IntToStr(start_tag)+' to '+ IntToStr(start_tag+quantity-1)+
                         ' or wrong description passed!');
            break;
        end;
        // Showmessage('Images start tag: '+IntToStr(start_tag)+' Index: '+IntToStr(ind)+' '+Img_caption[ind]);
    end;
    
    
    
    min_distance:=10;  // minimum distance between two images (and borders)
    
    // calculate possible number of images for one row
    try
        x_number:=integer(   (width-(quantity+1)*min_distance))/(image_width);
        if x_number>quantity then x_number:=quantity;
    except
        x_number:=1;
         showmessage( '1. Division with zero!'+#10+#13+
                      'Images start tag: '+IntToStr(start_tag)+#10+#13+
                      'number of images for one row (x_number): '+IntToStr(x_number)+#10+#13+
                      'Image width: '+IntToStr(Image_width)+#10+#13+
                      'Box Width: '+IntToStr(width)); 
    end;
    

    
    // calculate distance between two images (and borders)
    x_distance:=integer( (width-(x_number*image_width))/(x_number+1));
    
}               
  {  showmessage( 'Images start tag: '+IntToStr(start_tag)+#10+#13+
                  'Image width: '+IntToStr(Image_width)+#10+#13+
                  'Box Width: '+IntToStr(width)+#10+#13+
                  'number of images for one row (x_number): '+IntToStr(x_number)+#10+#13+
                  'x distance: '+IntToStr(x_distance));                                  }
 {
   try            
       number_of_rows:=integer(quantity/x_number)+1;
    except
       number_of_rows:=1;
         showmessage( '2. Division with zero!'+#10+#13+
                      'Images start tag: '+IntToStr(start_tag)+#10+#13+
                      'number of images for one row (x_number): '+IntToStr(x_number)+#10+#13+
                      'Image width: '+IntToStr(Image_width)+#10+#13+
                      'Box Width: '+IntToStr(width)); 
    end;            

    // if Start_Tag>700 then Showmessage ('Creating Images for Radio Image will start. Tag: '+IntToStr(Start_tag));

    // creating images and captions
    
    // Showmessage('Loading Images start tag: '+IntToStr(start_tag));
    
    ind:=0;                                    // image tag
    for row:=1 to number_of_rows do begin
      for i:=1 to x_number do begin            // possition in row
        ind:=ind+1;
        xx:=left+x_distance+(x_distance+image_width)*(i-1);
        yy:=top+5+min_distance*row+(image_height+min_distance+15)*(row-1)

        // create 'off' image
        image_path_off:=script_path+IntToStr(start_tag+ind-1)+'_of_'+image_names+'.jpg';
        // showmessage('Start tag: '+IntToStr(Start_tag)+' row: '+IntToStr(row)+' i: '+IntToStr(i)+' '+image_path_off);
        Img_off:=rcLoadImage (  '',  image_path_off,    // image_path
                                destination,                              // parent
                                xx,                                       // left
                                yy,                                       // top
                                image_width,                              // image_width
                                image_height                              // image_height
                                );
        Img_off.name:='i'+IntToStr(start_tag+ind-1)+'of'+image_names;
        Img_off.visible:=true;
        Img_off.tag:=start_tag+ind-1;
        Img_off.hint:=IntToStr(Img_off.tag)+' '+Img_off.name+'   - for testing only! dex_unit: Show_hint:=true';
        if Show_hint=true then Img_off.showhint:=true else Img_off.showhint:=false;
        
        
         
        //create 'on' image
        image_path_on:=script_path+IntToStr(start_tag+ind-1)+'_on_'+image_names+'.jpg';
        // showmessage('Start tag: '+IntToStr(Start_tag)+' row: '+IntToStr(row)+' i: '+IntToStr(i)+' '+image_path_on);
        Img_on:=rcLoadImage (   '',  image_path_on,    // image_path
                                destination,                              // parent
                                xx,                                       // left
                                yy,                                       // top
                                image_width,                              // image_width
                                image_height                              // image_height
                                );
        Img_on.name:='i'+IntToStr(start_tag+ind-1)+'on'+image_names;
        If ind=default then Img_on.visible:=true 
                   else Img_on.visible:=false;
        Img_on.tag:=start_tag+ind-1;         
        Img_on.hint:=IntToStr(Img_on.tag)+' '+Img_on.name+'   - for testing only! dex_unit: Show_hint:=true';
        if Show_hint=true then Img_on.showhint:=true else Img_on.showhint:=false;


        
        Img_label:=rcLabel(  '',
                             Img_caption[ind],
                             destination,
                             left+x_distance+(x_distance+image_width)*(i-1),
                             top+min_distance*row+(image_height+min_distance+15)*(row-1)+image_height+5,
                              taLeftJustify);
        Img_label.Font.Name:='Arial';
        Img_label.Font.Size:=8;

        if ind=quantity then break;             // when quantity reached exit from for loop
      end; // for i
      if ind=quantity then break;               // when quantity reached exit from for loop
    end;   // for row
    
    // if Start_Tag>700 then Showmessage ('Creating Images for Radio Image Ended! Tag: '+IntToStr(Start_tag));
    
    Box.Height:=min_distance*(row+1)+(image_height+20)*(row)+3;

End;
}
{ 
Procedure RC_Radio_Image_Change(  
                                 destination     : TWinControl;
                                 //image_name      : string;
                                 start_tag       : integer;
                                 end_tag         : integer;
                                 selected_tag    : integer
                                );
                                
// This procedure process Radio_Image 'object'
// 
// 1. Create array for manipulation with imaged
// 2. Find all images on destination object: if their name star with "i"   (added when created Radio Image 'object')
//                                            if their name have ON state
// 1b. Put these images to array
// 



var
   i,ind,AL:integer;
   // real_image_name_on:string;
   images:array of TMImage;  // array for saving image objects of destination
   // Image_on:boolean;
Begin
  { 
    showmessage ( 'Radio Image Change'+#13+#10+#13+#10+
                 'Destination: '+ destination.controls[i].name+#13+#10+
                 'Start_tag: '+IntToStr(start_tag)+#13+#10+
                 'End_tag: '+IntToStr(end_tag)+#13+#10+
                 'Selected_tag: '+IntToStr(selected_tag) 
                 );
   }
 {  
   
   
   
   // looking for images and saving in array
   AL:= End_tag-Start_Tag+2
   SetArrayLength(images,AL);
   ind:=0;
   // showmessage ('Array Length is '+IntToStr(AL));
   for i:=0 to destination.controlcount-1 do begin        // count all objects in destination
   
      // showmessage ('Object number '+IntToStr(i)+'/'+IntToStr(destination.controlcount-1)+' is '+destination.controls[i].name);
   
      if (destination.controls[i] is TMImage)          and        // is it image?  
         (copy(destination.controls[i].name,1,1) ='i') and        // is it first char in name 'i'?  (all taged images names start with 'i')
         (copy(destination.controls[i].name,5,2) = 'on') and      // is it tag_ON_name image?
         (destination.controls[i].tag <= End_Tag)        and     // is it tag less then end tag?
         (destination.controls[i].tag >= Start_Tag)             // is it tag greater then start tag?
              then begin ind:=ind+1;
                         images[ind]:= TMImage(destination.controls[i]);

                         if ind>End_tag-Start_Tag+1 then showmessage ('To many images found in "'+destination.controls[i].name+'"!');
              end else begin

              
              end;
   end; // for i

   for i:=1 to Ind do begin
       {
       showmessage ('Changing visibility '+#13+#10+ 
                    'Name: '+images[i].name+#13+#10+
                    'Original tag: '+ IntToStr(images[i].tag)+#13+#10+
                    // 'Recalculated tag: '+ IntToStr(images[i].tag+start_tag-1)+#13+#10+
                    'Selected_tag: '+IntToStr(selected_tag));
       }
       {
       if images[i].tag=selected_tag then images[i].visible:=true
                                     else images[i].visible:=false; 
   end;
   



End;
}
 

 
 
function rcCheckBox(nName,nText: string; nParent: TWinControl; nX, nY: integer):TCheckBox;
// create CheckBox
// nText = CheckBox text
// nParent = parent of CheckBox
// nX = Left
// nY = Top
 var
     CBox_1:TCheckBox;
     xo:TObject;
 begin
     if nParent=nil then showmessage('Parent for CheckBox "'+nText+'" not exist!'); 
     if nName<>'' then begin 
                       xo:=nil; 
                       find_control(nParent,nName);
                       if xo<>nil then Begin 
                          ShowMessage ('CheckBox name: "'+nName+'" already exist!'); 
                          exit 
                       end;
                   end;     
     CBox_1:=TCheckBox.create(nParent);
     CBox_1.parent:=nParent;
     CBox_1.width:=300;
     CBox_1.caption:=nText;
     CBox_1.Left:=nX;
     CBox_1.Top:=nY;
     CBox_1.ParentFont:=True;
     result:=CBox_1;
 end;


 

 
function rcMemo(mName:string; mParent: TWinControl; mX, mY, mS, mV: integer):TMemo;

var
   Memo_1:TMemo;
   xo:TObject;
begin
   if mParent=nil then showmessage('Parent for Memo "'+mName+'" not exist!');
   if mName<>'' then begin 
                       xo:=nil; 
                       find_control(mParent,mName);
                       if xo<>nil then Begin 
                          ShowMessage ('Memo name: "'+mName+'" already exist!'); 
                          exit 
                       end;
                   end;     
   Memo_1:=TMemo.create(mParent);
   Memo_1.parent:=mParent;
   if mName<>'' then Memo_1.name:=mName;
   Memo_1.Left  :=mX;
   Memo_1.Top   :=mY;
   Memo_1.Width :=mS;
   Memo_1.Height:=mV;
   Memo_1.ParentFont:=True;
   result:=Memo_1;

 end;
 
Function rcComboBox(sName:String;mParent: TWinControl):TComboBox;
var
     box1:TComboBox;
     xo:TObject;
 begin
    if mParent=nil then showmessage('Parent for ComboBox "'+sName+'" not exist!');
    if sName<>'' then begin 
                       xo:=nil; 
                       find_control(mParent,sName);
                       if xo<>nil then Begin 
                          ShowMessage ('Combo Box: "'+sName+'" already exist!'); 
                          exit 
                       end;
                   end;     
     box1:=TComboBox.create(mParent);
     box1.parent:=mParent;
     if sName<>'' then box1.name:=sName;
     // bot1.Left:=bX; bot1.Top:=bY;
     box1.ParentFont:=True;
     // bot1.Font.Size:=9;
     box1.Style:=csDropDownList;
     box1.Style:=csOwnerDrawFixed;   // sad se moze mijenjati visina
     box1.ItemHeight:=13;
     // box1.font.size:=9;
     box1.ParentFont:=True;
     // bot1.ItemIndex:=-1;
     with box1 do begin
      showhint:=true;
      // hint:='With radi!!!!';
    End;
     // box1.showhint:=true;
     {
     box1.Items.Add('000 Korpus');
     box1.Items.Add('001 Fronta');
     box1.Items.Add('002 Polica');
     box1.Items.Add('003 Cokla');
     box1.Items.Add('004 Radna ploca');
     }
     
     result:=box1;
 end;
 
 
function find_value_int(FileName,ValueName:string):integer;
// Find property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if ValueName not found or result is not integer, return 0 (zero)
// if ValueName found, but without value, ValueName will be deleted, and file will be saved
var
   list:TStringList;
   ind:integer;
   Value,ResultValue:string;
Begin
   list:=TStringList.Create;
   try list.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_int'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   value:=list.Values[ValueName];          // BUT, in this case value will be ''
   if (ind>=0) and (value='') then begin                      //  Name exist but no value
                List.Delete(List.IndexOfName(ValueName));     // delete name from list
                list.SaveToFile(FileName);                    // save new list to file
                // search again!!
                ind:=List.IndexOfName(ValueName);             // search again :-)
                value:=list.Values[ValueName];
   end; // if 
   if (ind>=0) and (not(value='')) then ResultValue:=value
                                   else begin  
                                      ShowMessage( 'Function: Find_value_int'+#13#10+'Cannot find name "'+ ValueName+'" in file "'+FileName+'"');
                                      ResultValue:='0';
                                   end;
   // try to convert to integer   
   try 
       Result:=StrToInt(ResultValue); 
   except 
       Result:=0; 
       Showmessage( 'Value "'+ValueName+'in file "'+FileName+'" cannot be converted to integer number!'+#10#13+'Assigning zero');   
   end;

      
end;   // function find_value_int 
 
function find_value_str(FileName,ValueName:string):string;
// Find property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if ValueName not found return ' '
// if ValueName found, but without value, ValueName will be deleted, and file will be saved
var
   list:TStringList;
   ind:integer;
   Value,ResultValue:string;
Begin
   list:=TStringList.Create;
   try list.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   value:=list.Values[ValueName];          // BUT, in this case value will be ''
   if (ind>=0) and (value='') then begin                      //  Name exist but no value
                List.Delete(List.IndexOfName(ValueName));     // delete name from list
                list.SaveToFile(FileName);                    // save new list to file
                // search again!!
                ind:=List.IndexOfName(ValueName);             // search again :-)
                value:=list.Values[ValueName];
   end; // if 
   if (ind>=0) and (not(value='')) then ResultValue:=value
                                   else begin  
                                      // ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find name "'+ ValueName+'" in file "'+FileName+'"');
                                      ResultValue:=' ';
                                   end;



   Result:=ResultValue; 
      
end;   // function find_value_int 

function set_value_str(FileName,ValueName, Value :string):boolean;
// Set property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if not succed return false

var
   list:TStringList;
   ind:integer;

Begin
   list:=TStringList.Create;
   result:=true;
   try begin 
            list.LoadFromFile(FileName);
            ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
            if ind =-1 then list.Add(ValueName+'='+Value) else list[ind]:=ValueName+'='+Value;
            list.SaveToFile(FileName);
       end; 
   except begin 
               ShowMessage( 'Function: Set_value_str'+#13#10+'Error load/save file "'+ FileName+'"!');
               result:=False;
          end;
   end;   // try

      
end;   // function set_value_str 

function set_value_int(FileName,ValueName :string; Value:Integer):boolean;
// Set property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if not succed return false

var
   list:TStringList;
   ind:integer;
Begin
   list:=TStringList.Create;
   result:=true;
   try begin 
            list.LoadFromFile(FileName);
            ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
            if ind =-1 then list.Add(ValueName+'='+IntToStr(Value)) else list[ind]:=ValueName+'='+IntToStr(Value);
            list.SaveToFile(FileName);
       end; 
   except begin 
               ShowMessage( 'Function: Set_value_str'+#13#10+'Error load/save file "'+ FileName+'"!');
               result:=False;
          end;
   end;   // try

end;   // function set_value_int 

Function rcFindNamedElement:TElement;
// Find first selected element
var i:integer;
    elm:TElement;
Begin
  if e=nil then begin   // search in project;
     for i:=0 to elmholder.Elementi.count-1 do begin
         elm:=Telement(ElmHolder.Elementi.items[i]);            
         if elm.naziv='xtemp' then begin
                                 result:=elm; 
                                 exit;
         end; // if 
     end;   // for                                 
  end else begin   // search in editor
        for i:=0 to e.ElmList.CountObj-1 do Begin  
           elm:=e.ElmList.Element[i];
           if elm.naziv='xtemp' then begin
                                 result:=elm;  
                                 exit;
           end;  // if
        end;   // for
  end; // if e<>nil
End;


Function rcFindSelectedElement:TElement;
// Find first selected element
var i:integer;
    elm:TElement;
Begin
  if e=nil then begin   // search in project;
     for i:=0 to elmholder.Elementi.count-1 do begin
         elm:=Telement(ElmHolder.Elementi.items[i]);            
         if elm.selected=true then begin
                                 result:=elm; 
                                 exit;
         end; // if 
     end;   // for                                 
  end else begin   // search in editor
        for i:=0 to e.ElmList.CountObj-1 do Begin  
           elm:=e.ElmList.Element[i];
           if elm.selected=true then begin
                                 result:=elm;  
                                 exit;
           end;  // if
        end;   // for
  end; // if e<>nil
End;

var
   selems : array of TElement;    // selected elements
   
Function FindSelectedElements: array of Telement;
// Find all selected element
var i,c:integer;
    elm:TElement;
    xselems: array of TElement;
Begin
  c:=0;
   
  if e=nil then begin                               
     // search in project;
     for i:=0 to elmholder.Elementi.count-1 do begin
         elm:=Telement(ElmHolder.Elementi.items[i]);            
         if elm.selected=true then begin
               c:=c+1;
               SetArrayLength(xselems,c); 
               xselems[c-1]:=elm; 
         end; // if 
     end;   // for                                 
  end else begin                             
        // search in editor
        for i:=0 to e.ElmList.CountObj-1 do Begin  
           elm:=e.ElmList.Element[i];
           if elm.selected=true then begin
               c:=c+1;
               SetArrayLength(xselems,c); 
               xselems[c-1]:=elm;
           end;  // if
        end;   // for
        if GetArrayLength(selems)=0 then begin    // if nothing selcted in editor 
               SetArrayLength(xselems,1);
               xselems[0]:=e;                 // then RESULT IS EDITOR ELEMENT
        end;
  end; // if e<>nil
  result:=xselems;
End; 


Function rcCBX(BotDest: TWinControl; bX, bY: integer):TComboBox;
// Kreira ComboBox
var
     bot1:TComboBox;
     xo:TObject;
     dir,path,dat,poc_nule:string;
     tipovi2:TStringList;
     i:Integer;
     ok:boolean;     
 begin
     if BotDest=nil then showmessage('Parent for ComboBox not exist!');
     bot1:=TComboBox.create(BotDest);
     bot1.parent:=BotDest;
     bot1.Left:=bX; bot1.Top:=bY;
     bot1.ParentFont:=True;
     bot1.Font.Size:=9;
     // bot1.Style:=csDropDownList;
     bot1.Style:=csOwnerDrawFixed;   // sad se moze mijenjati visina
     bot1.ItemHeight:=13;
     // bot1.font.size:=9;
     bot1.ParentFont:=True;
     bot1.ItemIndex:=-1;
     with bot1 do begin
      showhint:=true;
      // hint:='With radi!!!!';
    End;
     // bot1.showhint:=true;
     
     bot1.Items.Add('000 Korpus');
     bot1.Items.Add('001 Fronta');
     bot1.Items.Add('002 Polica');
     bot1.Items.Add('003 Cokla');
     bot1.Items.Add('004 Radna ploca');
     bot1.Items.Add('005 Nogica');
     bot1.Items.Add('006 Sudoper');
     bot1.Items.Add('007 Ruckica');
     bot1.Items.Add('008 Leda');
     bot1.Items.Add('009 Zid donji');
     bot1.Items.Add('010 Zid gornji');
     bot1.Items.Add('011 Pod');
     bot1.Items.Add('012 Strop');
     bot1.Items.Add('013 Okvir fronte');
     bot1.Items.Add('014 Proizvoljni');
     bot1.Items.Add('015 Bok');
     bot1.Items.Add('016 Zidna letva');
     bot1.Items.Add('017 Stropna ploha');
     bot1.Items.Add('018 Podna ploha');
     
     // read user types
     dir:=PrgFolder;
     path:='System\';
     dat:='TipDaske.dat';
     
     tipovi2:=TStringList.Create;
     // ShowD('Ucitavam korisnicke tipove dasaka');
     OK:=True;
     try 
        tipovi2.LoadFromFile(dir+path+dat);  // Load from Testing.txt file
     except begin
        ShowMessage('Ne mogu pronaci datoteku "'+dir+path+dat+'"!!!'); 
        ok:=false;
            end;
     end;
     // ShowD(Tipovi2.text); 
     if ok then begin
         for i:=0 to tipovi2.count-1 do begin
            if i<10 then poc_nule:='00';
            if (i>9) and (i<100) then poc_nule:='0';
            if (i>99) and (i<1000) then poc_nule:=''; 
            // ShowD(poc_nule+IntToStr(99+i)+' '+tipovi2[i]);   
            bot1.Items.Add(IntToStr(100+i)+' '+tipovi2[i]); 
         end;  // for i
         // ShowD('Ucitani!');
     end;  // if ok
     
     
     
     result:=bot1;
 end;

// ----------------------------------------------------------------
// --------------------- CHANGER ----------------------------------
// ----------------------------------------------------------------

var  
   LOD:Integer;
   LB:TListBox;

   // all tops and heights MUST depends of other tops and heights!!!
   
   chP:Tpanel;                         // host for changer
      chNameB:TButton;                 // name of selected element
      chInfoP:TPanel;                  // hosting main info and warnings of changer
         chNameL:TLabel;               // Label "Name"
         chNameE:TEdit;                // Edit name
         chCodeL:TLabel;               // Label "code"
         chCodeE:TEdit;                // Edit code
         chDescL:TLabel;               // Label "Description"
         chDescE:TEdit;                // Edit "Description"
         chTypeI:Integer;              // Element Type Code 
         chTypeL:TLabel;               // Type number and type name
         
         // to do chWP:TPanel;                  // warning panel. visible when need warning
         // to do   chWPL1,chWPL2:TLabel;      // Warning tekst
      
      chSBP:TPanel;                    // host for LOD panel and scroll box
         chSBL:TPanel;                 // Panel for LOD buttons and LOD state
            chLodDown:TButton;         // LOD down button
            chLodInfo:TLabel;          // actual level of details
            chLodUp:TButton;           // LOD up button
         chSB:TScrollBox;              // container for panelHolders 
            chPH:Tpanel;               // Panel Holder for group of variables ( many of them! )
                                       // tag=0 closed, tag=1 open
            {                           
            chPHName:Tlabel;           // Name: 'chPH_01_Holder_Name', caption: 01 Holder name
            
            chPVar:TPanel;             // Panel for one variable, top:=chPHNAme.Height+5
                                       // name:=holder number + variable name 
                                       // tag:=type, 1=integer;
               chVName:TLabel;         // name=  xyz + |name, caption=screen name (title)
               chVro:TLabel;           // name=  xyz + 0 ili 
               chVType:TLabel;         // name=  xyz + type, visible:=false
               chVEdit:TEdit;          // edit fot fill
               chVUnit:TLabel;
               chVPic:TMIMage;
               chVPicPat:TLabel;
               chVDesc1:TLAbel;
               chVDesc2:TLabel;         // name=  xyz + desc, caption:=description
               
} 
Procedure Changer_header_click(sender:TObject);
begin 
  if chp.height=20 Then chp.height:=200
                   else chp.height:=20;
  chp.SetFocus;                           
end; 

Procedure LODup(sender:TObject);
begin 
  LOD:=LOD+1;
  If LOD>5 then LOD:=5;
  chLodInfo.caption:=IntToStr(LOD);
  chSBL.SetFocus;                           
end; 

Procedure LODdown(sender:TObject);
begin 
  LOD:=LOD-1;
  If LOD<1 then LOD:=1;
  chLodInfo.caption:=IntToStr(LOD);
  chSBL.SetFocus;                           
end;


{  
function changer(cparent:TWinControl;calign:TAlign;width,size:integer):TPanel;
var
   ss:string;
   // lab:TLabel;
   // hb:TButton;
Begin
   LOD:=size;        // Level Of Details
   //elm:=rcFindSelectedElement:
   
   // create holder for all components
   chp:=rcPanel('chp',cparent);  // ChangerHolderPanel
   chp.color:=clBlue;
   chp.align:=calign;
   
   // header button
   ss:=':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::';
   chNameB:=rcButton('chb',ss+ss+'   '+'Element name'+'   '+ss+ss,chp, 0, 0);
   chNameB.height:=16; 
   chNameB.align:=alTop;
   chNameB.font.size:=7;
   chNameB.OnClick:=@Changer_header_click;
   
   // info panel
   chInfoP:=rcPanel('chi',chp);  // Base info about element
   chInfoP.Align:=alTop;
   chInfoP.Height:=65;
                         
      // Name of element
      chNameL:=rcLabel('','Name'+':',chInfoP,100,5,taRightJustify);
      chNameE:=rcEdit('Element name',chInfoP,100,3);
      chNameE.width:=chInfoP.clientwidth-105;
      //chNameE.OnClick:=@EditOnClick;
      // STAVITI FUNKCIJU ZA DISABLE CTRL+C jer se tipe pokre�e kopiranje selektiranog elementa
      chNameL.Left:=chNameE.Left-chNameL.width-5;
      
      // Code of element
      chCodeL:=rcLabel('','Code'+':',chInfoP,100,25,taRightJustify);
      chCodeE:=rcEdit('','Element code',chInfoP,100,23);
      chCodeE.width:=chInfoP.clientwidth-105;
      chCodeE.OnClick:=@EditOnClick;
      chCodeL.Left:=chCodeE.Left-chCodeL.width-5;
      
      // Description of element
      chDescL:=rcLabel('','Description'+':',chInfoP,100,45,taRightJustify);
      chDescE:=rcEdit('','Element Description',chInfoP,100,43);
      chDescE.width:=chInfoP.clientwidth-105;
      chDescE.OnClick:=@EditOnClick;
      chDescL.Left:=chDescE.Left-chDescL.width-5;
      
   // Panel for host for LOD panel and scroll box
   chSBP:=rcPanel('',chp);    // panel for holding LOD
   chSBP.height:=300;
   chSBP.align:=alClient;
   chSBP.Color:=clYellow;
      
      // LOD panel - buttons and info for level of details for variables setting
      chSBL:=rcPanel('',chSBP);    // panel for holding LOD
      chSBL.height:=22;
      chSBL.align:=alTop;
      //chSBL.Color:=clRed;
             
         // LOD: up / info / down 
         chLodDown:=rcButton('','-',chSBL,3,3);             
         chLodDown.Hint:='Decrease level of details';
         chLodDown.width:=16; chLodDown.Height:=16;
         chLodDown.OnClick:=@LODdown;
         chLodInfo:=rcLabel('',IntToStr(LOD),chSBL,25,3,taCenter);
         chLodInfo.Hint:='Level of details';
         chLodUp:=rcButton('','+',chSBL,40,3);             
         chLodUp.width:=16; chLodUp.Height:=16;
         chLodUp.Hint:='Increase level of details';
         chLodUp.OnClick:=@LODup;

      // scroll box holder for variable holders   
      chSB:=rcScrollBox( '',chSBP);
      chSB.align:=alClient; 
      chSB.color:=clRed;
          
         // discover number of panels
         
        // showmessage(e.fvarijable.strings[0]);
        // showmessage(e.fvarijable.count);
 
         
         chPH:=rcPanel('Panel_01',chSB);
         chPH.height:=100;
         chPH.align:=alTop;
            
         chPH:=rcPanel('Panel_02',chSB);
         chPH.height:=100;
         chPH.align:=alTop;
            
         chPH:=rcPanel('Panel_03',chSB);
         chPH.height:=100;
         chPH.align:=alTop;
      {
    


            chPH:Tpanel;               // Panel Holder for group of variables ( many of them! )
                                       // tag=0 closed, tag=1 open
   
//    lab:=rcLabel('chp_h','ChangerHolderPanel_Header',pan,10,10,taLeftJustify); 
//      'top': begin pan.left:=0; pan.top:=0;pan.width:=cparent.clientwidth;

   result:=chp;

End;
}
{
Procedure update_changer;
// fill changer with data from selected element
// clean existing changer
// fill 
Begin
end;

Procedure apply_changer;
// change element with data from changer
// check box: change live, change on demand
begin
end;

// /////////////////////////////////////////////  TREE  ////////////////////////
var
   
   treep:Tpanel;
   treeB:TButton;
   mt:TMemo;
   

Procedure treeb_click(sender:TObject);
begin 
  if treep.height=20 Then treep.height:=200
                   else treep.height:=20;
  chp.SetFocus;                           
end; 

function create_tree(cparent:TWinControl;calign:TAlign;cheight:integer):TPanel;
var
   ss:string;
Begin

   // create holder for tree
   treep:=rcPanel('treep',cparent);  // ChangerHolderPanel
   treep.height:=cheight;
   treep.align:=calign;
   treep.font.name:='Arial';
   treep.font.size:=8;
 
   // header button
   ss:=':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::';
   treeB:=rcButton('treep',ss+ss+'   '+'Project'+'   '+ss+ss,treep, 0, 0);
   treeB.height:=16; 
   treeB.align:=alTop;
   treeB.font.size:=7;
   treeB.OnClick:=@treeb_click;
   
   // memo 
   mt:=rcMemo('',treep,0,5,200,50);
   mt.align:=alTop;
   mt.ScrollBars:=ssVertical;
   result:=treep;
   
   // ListBox
   LB:=TListBox.Create(treep);
   LB.Parent:=Treep;
   LB.Top:=10;
   LB.Multiselect:=true;
   LB.align:=alClient;
   
   
end;
}
{
Procedure refresh_tree;
var
   i,c:integer;
   elm:Telement;
   xselems:array of Telement;
Begin
   // show all elements
   selems:=FindSelectedElements;
   //elems:=FindElements;
   ns:=GetArrayLength(selems);
   if (ns=0) or (ns=oldns) then exit;
   {
   if qe=0 then begin LB.clear; exit; end;
   if ns=0 then exit;
   if ns=oldns then exit;
   if 
   
   LB.clear;
   if e=nil then begin                               
     // search in project;
     for i:=0 to elmholder.Elementi.count-1 do begin
         elm:=Telement(ElmHolder.Elementi.items[i]);
         LB.Items.add(elm.naziv);            
         if elm.selected=true then begin
               LB.ItemIndex:=i;
               c:=c+1;
               SetArrayLength(xselems,c); 
               xselems[c-1]:=elm; 
         end; // if 
     end;   // for                                 
  end else begin                             
        // search in editor
        for i:=0 to e.ElmList.CountObj-1 do Begin  
           elm:=e.ElmList.Element[i];
           LB.Items.add(elm.naziv);
           if elm.selected=true then begin
               LB.ItemIndex:=i;
               c:=c+1;
               SetArrayLength(xselems,c); 
               xselems[c-1]:=elm;
           end;  // if
        end;   // for
        if GetArrayLength(selems)=0 then begin    // if nothing selcted in editor 
               SetArrayLength(xselems,1);
               xselems[0]:=e;                 // then RESULT IS EDITOR ELEMENT
        end;
  end; // if e<>nil
   
   }
{   
   mt.clear;
   for i:=0 to GetArrayLength(selems)-1 do begin
       mt.lines.add('-'+selems[i].naziv);
   end;
   oldns:=ns;
   
End;
}





