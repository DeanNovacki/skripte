Program CNCSetup;   


// MAIN ////////////////////////////////////////////////////////////////////////
var
   // Startni prozor
	Main:TForm;
   Panel_Left, Panel_right, Panel_Down, Panel_Title, Panel_Info, Panel_Table, Panel_Memo, Panel_MemoO :TPanel;
   Pan_var, Pan_val, Pan_for, Pan_cb, Pan_hint : array of TPanel;
   ButOK,ButPomoc,ButCancel:TButton;
	datoteka:string;
	Debug, MozeZatvaranje, Izvrseno :boolean;    // za debug poruke u Journalu
	
	m1:TMemo;
	
	
{$I ..\..\..\include\rcMessages.cps}
{$I ..\..\..\include\rcPrint.cps}
{$I ..\..\..\include\rcStrings.cps}
{$I ..\..\..\include\rcControls.cps}
{$I ..\..\..\include\rcObjects.cps}
{$I ..\..\..\include\rcEvents.cps}
{$I ..\..\..\include\rcCurves.cps}
{$I ..\..\..\include\rcMath.cps}


Procedure Napravi_posao;
Begin
	// Print('*************************');
	// Print('Ovdje se izvr�ava program');
	// Print('*************************');
	// ShowMessage('POSAO');

End;


Procedure kreiraj_memo;
Begin
  // Showmessage('U�ao u Kreiraj memo');
  M1:=rcMemo('M10',Main,100,100,30,30);
  // Showmessage('Kreirao Memo');
  M1.align:=alClient; 
  M1.ScrollBars:=ssVertical;
  // M1.ReadOnly:=True;
  M1.Font.color:=clSilver;
  M1.Font.Name:='Consolas';
  M1.Color:=clMaroon;
  M1.WordWrap:=False;
End;

Procedure Napuni_memo;

Begin
	
	If FileExists(datoteka) then M1.Lines.LoadFromFile(datoteka)
							  else Showmessage ('Nije prona�ena datoteka '+datoteka);
end;

Procedure Spremi_ini;
var
	original:TStringList;
	PS,vr,god,mje,dan,sat,min,sek, sad:String;
	vrijeme:tDateTime;
Begin
	// Print ('proc: Spremi_ini start');
	
	// izrada Backupa
	original:=tStringList.Create;
	original.LoadFromFile(datoteka);	
	vrijeme:=Now;
	god:=FormatDateTime('yyyy', vrijeme);
	mje:=FormatDateTime('mm', vrijeme);
	dan:=FormatDateTime('dd', vrijeme);
	sat:=FormatDateTime('hh', vrijeme);
	min:=FormatDateTime('nn', vrijeme);
	sek:=FormatDateTime('ss', vrijeme);
	sad:=god+'_'+mje+'_'+dan+'_'+sat+'_'+min+'_'+sek;
	
	Try 
		original.SaveToFile(PrgFolder+'system\CncSetup_kopija_'+sad+'.ini')
		M1.Lines.SaveToFile(datoteka);	
		Showmessage(	'Datoteka je uspje�no spremljena.' + #13#10
							+ #13#10 +
					//		'Da bi neke od promjena postale funkcionalne potrebno je' + #13#10 +
					//		'zagasiti Corpus RC i ponovo ga pokrenuti.' + #13#10 +
					//		+ #13#10 +
							'Kreirana je i kopija prija�nje datoteke.');
	except
		Showmessage('Datoteka CncSetup.ini nije izmijenjena zbog gre�ke na disku');
	finally
		original.free;
	end;;
	
	

	// Print ('proc: Spremi_ini end');
End;
// ------------------------------------------------	
{$I Prazna_skripta_servis.cps}
// ------------------------------------------------

// Glavni   

{$I ..\..\mSvi.cps}	
	
{BEGIN je u includu}
	
	SakrijSve;

   PrintTime('Skripta CNC start');
	
	datoteka:=PrgFolder+'System\CncSetup.ini';

	
	Showmessage(	'UPOZORENJE!'+#13#10+
						#13#10+
						'CNCSetup.ini je vrlo va�na datoteka sustava.'+
						#13#10+
						'Ako niste sigurni �to radite, najbolje je da ni�ta ne dirate.'+#13#10
						+#13#10+
						'(Osim tipke "Odustajem")'
						+#13#10+
						+#13#10+
						'Hvala na razumijevanju');
	
	
	
	Create_Main_Window;
	Main.caption:='CNC Setup';
	// Application.processmessages;
	
	ButOK.OnClick:=@ButOK_OnClick;
	ButCancel.OnClick:=@ButCancel_OnClick;
	
	ButPomoc.OnClick:=@ButPomoc_OnClick;
	
	ButCancel.default:=True;
	ButCancel.ModalResult:=mrCancel;
	
	Panel_Title:=rcPanel ( 'dsafdasf' ,Panel_Left ); 
	Panel_Title.caption:='NASLOV';
	Panel_Title.ALign:=alTop;
	
	kreiraj_memo;
	napuni_memo;

	
	
   
   Main.ShowModal;
	
	
	
	{
	Main.Show;
		while Main.visible do begin
        application.processmessages;
    end;
   }
   Main.free; 
   
   PrintTime('Skripta CNC end');
End.