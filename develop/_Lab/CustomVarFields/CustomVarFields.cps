
var
   activeCustomFields:TEditableVarList;
   DataTypeName:array[0..8] of string; 
      
Function AddNewVar(aType:TVarDataType):TEditVar;
begin
   result:=TEditVar.create;
   result.VarDataType:=aType;
   activeCustomFields.add(result);
   
end;

Procedure DodajParVarijabli(aCatZaTest:string);
var
   aVar:TEditVar;
begin
  aVar:=AddNewVar(dtvInteger);
  aVar.MinValue:=-100;
  aVar.MaxValue:=100;
  aVar.VarValue:='50';
  aVar.Category:=aCatZaTest;
  aVar.VarName := 'Var1';
  aVar.Caption := 'Moja prva varijabla';
  aVar.tag:=11111;
  
  aVar:=AddNewVar(dtvCheck);  
  aVar.VarValue:='1';
  aVar.Category:=aCatZaTest;
  aVar.VarName := 'Var2';
  aVar.Caption := 'Moja druga varijabla';
  aVar.ValuesList[0]:='Nije odabrano';
  aVar.ValuesList[1]:='Odabrano je';
  
  
  aVar:=AddNewVar(dtvCombo);    
  aVar.Category:=aCatZaTest;
  aVar.VarName := 'Var3';
  aVar.Caption := 'Moja treca varijabla';
  aVar.ValuesList.Add('Vrijednost prva=1');
  aVar.ValuesList.Add('Vrijednost Druga=2');
  aVar.ValuesList.Add('Vrijednost ...=3');
  aVar.VarValue:='2';  
  
  aVar:=AddNewVar(dtvString);
  aVar.VarValue:='50';
  aVar.Category:='TestKat1';
  aVar.VarName := 'Var4';
  aVar.Caption := 'Var4';
  
  aVar:=AddNewVar(dtvString);
  aVar.VarValue:='';
  aVar.Category:='TestKat1';
  aVar.VarName := 'Var5';
  aVar.Caption := 'Var5';
  aVar.required:=true;
end;


Procedure PrintVars;
var
   i:integer;
   aVar:TEditVar;
begin
  //elmholder.projectOpisData.CustomFieldList.SortByCategory;
  for i := 0 to elmholder.projectOpisData.CustomFieldList.count-1 do begin
     aVar:=elmholder.projectOpisData.CustomFieldList.EditVar[i];
     ShowMessage(aVar.VarName+ ' , '+ aVar.Category);
  end;
end;

Procedure PrintTestKat(aCatZaTest:string);
var
   L:tlist;
   i:integer;
   aVar:TEditVar;
begin
  
  L:=elmholder.projectOpisData.CustomFieldList.GetCategoryVar(aCatZaTest);
  if l<>nil then begin
  for i := 0 to L.count-1 do begin
    aVar:=TEditVar(L.items[i]);
    ShowMessage(aVar.VarName+ ' , '+ aVar.Category); 
  end;
  L.free;
  end;
end;


   
Procedure StaviUProjekt;
begin
  activeCustomFields:=elmholder.projectOpisData.CustomFieldList;
  DodajParVarijabli('Ovo je opis projekta');
  //PrintVars;
  //PrintTestKat('Ovo je opis projekta');
  
  DoEditVarlist(activeCustomFields);
end;
 
Procedure StaviUPrviElement;
begin
  if not telement(elmlist.items[0]).HaveCustomFielsds then begin
    showMessage('CustomFields property jo� ne postoji'+#13#10+
                '�im mu se pritupi bude automatski kreiran'+#13#10+
                'Ako ne misli� dodavati varijable prvo provjeri'+#13#10+
                'dali postoje ako je "HaveCustomFielsds=TRUE"'+#13#10+
                'tek onda koristi CustomFields property;'+#13#10+
                'da se nebi bezveze kreirao objekt koji netreba.');
  end else begin
    showMessage('CustomFields property  kreiran od prije');
  end;
  activeCustomFields:=telement(elmlist.items[0]).CustomFields;  // ako nije postojao sad postoji, kreira se automatski kad mu se pristupi prvi put.
  DodajParVarijabli('Ovo je element '+telement(elmlist.items[0]).naziv);
  DoEditVarlist(activeCustomFields);
end;


Procedure StaviUPrvdasku;
begin
  activeCustomFields:=telement(elmlist.items[0]).Childdaska.Daska[0].CustomFields;
  DodajParVarijabli('Ovo je Daska '+telement(elmlist.items[0]).Childdaska.Daska[0].naziv);
  DoEditVarlist(activeCustomFields);
end;

function BoolToStr(Value:boolean):string;
begin
  if value then result:= 'True' else result:='False';
end; 

Procedure GetVarFromList(aVarName:string);
var
   aVar:TeditVar;
   list:TEditableVarList;   
begin
  
  list:=elmholder.projectOpisData.CustomFieldList;
  aVar:=list.getEditVarByName(aVarName');
  if aVar<>nil then begin
     showmessage('Varijabla "'+aVarName+'" postoji'+#13#10+
                 'Name: '+aVar.VarName +#13#10+
                 'Category: '+ aVar.Category +#13#10+
                 'DataTip: '+DataTypeName[integer(aVar.VarDataType)] +#13#10+
                 'Caption: '+aVar.Caption +#13#10+
                 'Color: '+ inttostr(aVar.Color) +#13#10+
                 'ReadOnly: '+BoolToStr(aVar.ReadOnly) +#13#10+
                 'Visible: '+BoolToStr(aVar.Visible) +#13#10+
                 'Required: '+BoolToStr(aVar.Required) +#13#10+
                 'Hint: '+ aVar.Hint +#13#10+
                 'Tag: '+ inttostr(aVar.Tag) );
                            
  end else begin
  end;
end;

   
begin
  DataTypeName[0]:= 'dtvNone'; DataTypeName[1]:= 'dtvInteger'; DataTypeName[2]:= 'dtvString';
  DataTypeName[3]:= 'dtvCombo'; DataTypeName[4]:= 'dtvColor'; DataTypeName[5]:= 'dtvExtended';
  DataTypeName[6]:= 'dtvButton'; DataTypeName[7]:= 'dtvDate'; DataTypeName[8]:= 'dtvCheck';
  StaviUProjekt;
  GetVarFromList('Var1');
  if elmlist.count >0 then begin
    StaviUPrviElement;
    StaviUPrvdasku;
  end else showmessage('Daj satvi element u projekt');
end.