Program StringList;
var
   sl:TStringList;
   i:Integer;
   searchString,str:string;
Begin
   writeln('');
   sl:=TStringList.Create;
   
   writeln('1. Add a few new rows');
   sl.add('Row_One=1');
   sl.add('Row_Two=2');
   sl.add('Row_Three=3');
   sl.add('Row_Four=14');
   writeln('');
   
   writeln('2. Count number of rows'); 
   i:=sl.count;
   writeln('This list has '+IntToStr(i)+' rows');
   writeln('');
   
   writeln('3. Read complete text from list:');
   str:=sl.text;
   writeln(str);
   writeln('');
   
   writeln('4. Write complete text to a list:');
   str:= '*Row_One=1'+#13#10+
         '*Row_Two=2'+#13#10+
         '*Row_Three=3'+#13#10+
         '*Row_Four=14';
         
   sl.SetText(str);
   
   writeln(sl.text);
   writeln('');
   
   writeln('5. Read string from index');
   for i:=0 to sl.count-1 do begin
      str:=sl[i]
      writeln(str);
   end;
   writeln('');
   
   
   writeln('6. Read name from position');
   for i:=0 to sl.count-1 do begin
      str:=sl.names[i]
      writeln(str);
   end;
   writeln('');
   
   writeln('7. Read index from name');
   i:=sl.IndexOfName('Row_Five');
   
   if i=-1 then writeln('row with index: '+IntToStr(i)+' "Row_Five" not exit')
           else writeln('row with index: '+IntToStr(i));
   
   writeln('');        
           
   writeln('8. Read value from name'); 
   str:=sl.Values['Row_Four'];         
   writeln('String value of "Row_Four" is '+str);        
   writeln('');
   
   writeln('9. Read complete string from index'); 
   str:=sl.Strings[3];          
   writeln('String value of row number 3 is '+str);        
   writeln('');
   
   writeln('10. Change string by index');           
   sl[3]:='Row_Four=4';      
   writeln('');
   
   
   writeln('11. Read index by string');          
   i:=sl.IndexOf('Row_Four=4') 
   writeln('"Row_Four=4" has index number '+IntToStr(i));
   writeln('');

   
   // find str and change value or add if not exist
   SearchString:='Row_FouR=4';
   i:=sl.IndexOf(SearchString);   // search for index of string
   If i>0 then Begin     
                    writeln ('String exist, change it!');
                    sl[i]:=SearchString
               end else begin                                 // if index is -1 then string is not exist in list
                    writeln ('String not exist, add it!');
                    sl.add(searchString);
   End;
   
	// brisanje string liste
	sl.clear;
	
   writeln(sl.text);      
   
   
   writeln('');
End.