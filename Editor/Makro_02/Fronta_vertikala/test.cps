﻿
// Out 2******************************************
Procedure Test_dasaka;
var
	i, broj_fronti, Broj_vertikala:integer;
	ss:string;
	td:Tdaska;
Begin
	// brojanje količine
	broj_fronti:=0;
	Broj_vertikala:=0;
	for i:=0 to e.childdaska.CountObj-1 do Begin  // broji daske u editoru
		td:=e.childdaska.Daska[i];   
		// tražim frontu
		if td.selected and (td.smjer in [VertFront]) then begin				// VertBok, VertFront, Horiz
			broj_fronti:=broj_fronti+1;
			daska_1:=td;
			// ShowMessage('Našao sam frontalnu broj: '+IntToStr(broj_fronti));
		end; // if td.selected
		// tražim vertikalu
		if td.selected and (td.smjer in [VertBok]) then begin				// VertBok, VertFront, Horiz
			broj_vertikala:=broj_vertikala+1;
			// ShowMessage('Našao sam vertikalu broj: '+IntToStr(broj_vertikala));
			daska_2:=td;
		end; //if td.selected
	end;  // for i:=0 to e.childdaska.CountObj-1 


	// provjera količine
	If (broj_fronti=1) and (Broj_vertikala=1) Then Begin
		// provjera kuteva
		If (Daska_1.kut<>0) or (Daska_1.xkut<>0) or (Daska_1.zkut<>0) or 
			(Daska_2.kut<>0) or (Daska_2.xkut<>0) or (Daska_2.zkut<>0) Then Begin 
					Showmessage('Neke selektirane daska su rotirane,' +#13#10+
									'a ne smije biti za ovaj spoj.' );
					ButOK.Enabled:=False;			 
			End else begin
				// kutevi su ok
				// provjera dodira
				// visina,dubina,debljina,xPos,yPos,zPos
				If (daska_1.ZPos<>daska_2.ZPos) or										// 2 nije točno iza 1
					(daska_2.xPos+daska_2.debljina<daska_1.XPos) or 				// 2 je lijevo od 1
					(daska_2.xPos>daska_1.XPos+daska_1.Dubina) or   				// 2 je desno od 1
					(daska_2.yPos+daska_2.visina<daska_1.Ypos) or 					// 2 je ispod 1
					(daska_2.yPos>daska_1.ypos+daska_1.visina) 						// 2 je iznad 1
					Then begin
						Showmessage('Izgleda da se daske ne dodiruju.'+#13#10+
										'Makro se može postaviti, ali dobro provjerite što radite!');
				End;  // 	If (daska_1.ZPos<>daska_2.ZPos) 
		end;	// If (Daska_1.kut<>0) 
	end else begin  // If (broj_fronti=1) ;	
		ButOK.Enabled:=False;
		Showmessage('Moraju biti selektirane dvije daske!' +#13#10+
						'Naprijed fronta i otraga vertikala');
	end;  // If (broj_fronti=1)
	
		
End;
// Out 2******************************************
