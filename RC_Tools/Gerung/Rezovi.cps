Program Rezovi;   // Ostalo je skriveno
// Japa d.o.o.
// v2.3 24.03.2015. kompatibilnost s Corpusom v4.2.2.2770
// v2.2 18.03.2014. Smanjen broj segmenata kod zaobljavanja zbog izbjegavanja gre�ke
// v2.1 14.01.2014. Sakrivene rubne trake i osobine, ostaju samo rezovi
// v2.0 24.05.2013. Dodani rezovi u skripti Osobine

{$I ..\include\rcMessages.cps}
{$I ..\include\rcControls.cps}
{$I ..\include\rcObjects.cps}
{$I ..\include\rcEvents.cps}
{$I ..\include\rcCurves.cps}

 

const 
   // segment_density=20;    // broj segmenata na 1cm duljine radijalnog reza
   segment_density=10;    // broj segmenata na 1cm duljine radijalnog reza

var

   
   // design vars start **************
   Main:TForm;
   Panel_Left, Panel_right, Panel_Up, Panel_UpL, Panel_UpR, Panel_No, Panel_Title, Panel_Info, 
   
   // properties
   Pan_prop:TPanel;
      Pan_PropH:TPanel;
      Panel_Table, Panel_Memo, Panel_MemoO :TPanel;
   Pan_RedH, // cijeli red zaglavlja
      Pan_varH, Pan_valH, Pan_forH, Lab_hintH : TPanel; CB_selH :TCheckBox; 
   Pan_red,  // redovi u tablici
      Pan_var: array of TPanel; 
      Pan_val, Pan_for : array of TEdit; 
      CB_sel :array of TCheckBox;
      Lab_hint : array of TPanel;
      CoB_tip:TComboBox;
   
   // cuts
   Pan_Cuts,
      Pan_CutsH,     // Header - tipka
      Pan_CutsI : TPanel;     // Panel sa slikama
         Lab_go, Lab_do, Lab_li, Lab_de :TLabel;   // gore, dolje, lijevo, deno, naprijed, natrag
         Pan_PicH, Pan_PicV : TPanel;    // holder za vertikalnu i horizontalnu dasku
         // PicH
         H1N_0,H1N_1,H1I_0,H1I_1,H1U_0,H1U_1,H1R_0,H1R_1,H1T_0,H1T_1,H1T_2,
         H2N_0,H2N_1,H2I_0,H2I_1,H2U_0,H2U_1,H2R_0,H2R_1,H2T_0,H2T_1,H2T_2,
         H3N_0,H3N_1,H3I_0,H3I_1,H3U_0,H3U_1,H3R_0,H3R_1,H3T_0,H3T_1,H3T_2,
         H4N_0,H4N_1,H4I_0,H4I_1,H4U_0,H4U_1,H4R_0,H4R_1,H4T_0,H4T_1,H4T_2,
         // PicV
         V1N_0,V1N_1,V1I_0,V1I_1,V1U_0,V1U_1,V1R_0,V1R_1,V1T_0,V1T_1,V1T_2,
         V2N_0,V2N_1,V2I_0,V2I_1,V2U_0,V2U_1,V2R_0,V2R_1,V2T_0,V2T_1,V2T_2,
         V3N_0,V3N_1,V3I_0,V3I_1,V3U_0,V3U_1,V3R_0,V3R_1,V3T_0,V3T_1,V3T_2,
         V4N_0,V4N_1,V4I_0,V4I_1,V4U_0,V4U_1,V4R_0,V4R_1,V4T_0,V4T_1,V4T_2,
         // common        
         HCent,VCent,FCent,V1T_3,V2T_3,V3T_3,V4T_3,H1T_3,H2T_3,H3T_3,H4T_3:TMImage;
         
         Shape_Sense: TShape; 
			Sh_C, Sh_T : Array [0..3] of TShape;  // c=curve, T=sa strane (potsje�a na slovo T)
			Sh_S : TShape;   // sredina;
			
      Pan_CutsP:TPanel;     // Panel s propertijima
         Pan_use, Pan_Sm : TPanel;
         Lab_Sm : TLabel;
         CB_Cuse : TCheckBox;
         Lab_Vsm : TLabel;
         
         Pan_N, Pan_A, Pan_B, Pan_C, Pan_D, Pan_E, Pan_F, Pan_G, Pan_R : TPanel;
         Lab_DirN, Lab_DirV,    // orijentation name and value
         Lab_N, Lab_A, Lab_B, Lab_cB, Lab_C, Lab_D, Lab_E, Lab_F, Lab_cF, Lab_G, Lab_R : TLabel;
         cb_B, Cb_F: TCheckBox;    // Check Box za isklju�ivanje
         rb_C, rb_G: TRadioButton;    // za uklju�ivanje/isklju�ivanje ishodi�ta
         Sl_A, Sl_B, Sl_C, Sl_D, Sl_E, Sl_F, Sl_G, Sl_R : TScrollBar;
         Ed_A, Ed_B, Ed_C, Ed_D, Ed_E, Ed_F, Ed_G, Ed_R : TEdit;
         Var_A, Var_B, Var_C, Var_D, Var_E, Var_F, Var_G, Var_R : Array [1..4] of Single;
         Ceo: Array [1..4] of Integer;   // ishodi�te reza na bridu, 0=na C, 1=na G 
			Var_X, Var_Y, Var_W, Var_H, Var_Rad : Single;
      // CCS, ECS, Cs, Co
      CCS, // corner cut status   0=none , 1=/ , 2=L , 3=R 
      ECS  { edge cut status     0-1  }   :Array [1..4] of Integer;
      Cs : Integer;  // trenutno selektiran
      Co :Integer;   // cut mouse over   11-14=corner, 21-24=edge
      
      // Rubne trake
      
      Pan_edge:TPanel;
         Pan_EdgeH:TPanel;
         CB_edge_use:TCheckBox;
         CMB_E1,CMB_E2,CMB_E3,CMB_E4,CMB_E5,CMB_E6,CMB_E7:TComboBox;
         Img_PH,Img_PF,Img_PV,Img_PM:TMIMage;
         RB_EdgeAll,RB_EdgeOne,RB_EdgeChange:TRadioButton;
         Lab_U:TLabel;
         
      // cca,ccb,ccc,ccd,cce,ccf,ccg,ccr : Array [1..3] of integer;

      
      // krivulje u dasci
      AllCurve:TTockeSystem;
      curve {, curve_save} : Array of TRubneTocke; // rubne krivulje u dasci
      // curve_var, curve_var_save : Array of TStringList;
      
      
      
   ButOK,ButTest,ButCancel:TButton;
   // cbDebug:TCheckBox; MemoJ:TMemo;  definirani na vrhu!!!
   LabTemp:TLabel;      // stati�ne labele
   LabSelQnt:TLabel;    // labela za prikaz broja selektiranih objekata
   LabSelTxt:TLabel;    // labela za prikaz popisa selektiranih objekata
   // design vars end **************
   
   Debug:boolean;    // za debug poruke u Journalu
   SelQnt:Integer;   // broj selektiranih objekata; 
   SelTxt:String;    // tekstualni popis selektiranih objekata
   SelPlates:TStringList;
   j:Integer;
   UX, UY, UZ, UH, UW, UT, UD:single;   // zajedni�ki x,y,z,visina,�irina,debljina,dubina
   UXf, UYf, UZf, UHf, UWf, utf : string;   // zajedni�ka form. x,y,z,visina,�irina,debljina,dubina
   sUXf, sUYf, sUZf, sUHf, sUWf, sutf : string;   // stara zajedni�ka formula
   nUX, nUY, nUZ, nUH, nUW, nUT, nUD:single;   // novi x,y,z,visina,�irina,debljina,dubina
   sUX, sUY, sUZ, sUH, sUW, sUT, sUD:single;   // stari x,y,z,visina,�irina,debljina,dubina
	// kutevi
	UXK, UYK, UZK, sUXK, sUYK, sUZK: single;    // kutevi stari i novi
	UXKf, UYKf, UZKf, sUXKf, sUYKf, sUZKf: string;    // formule kuteva stare i nove
	prim,sprim:string;  // nova i stara primjedba
   sb,nb:single;
   UTip,sutip:integer;
   UF:string;                                  // zajedni�ka formula
   td:Tdaska;
   razno:string;
   hss:String;
   
   ipath:String;   // image path

procedure check_enable;
var
   i:Integer;
   
begin
  For i:=0 to 10 do begin
      // vrijednost propertija
      // if (Pan_For[i].text=razno) then begin
      

		if (i<6) or (i>7) then begin	
			// ako nije checkirano zabrani bilo kakav upis                        
			if (cb_sel[i].checked=False) then begin
                                  Pan_For[i].Font.color:=clSilver;
                                  Pan_For[i].ReadOnly:=True;
                                  Pan_Val[i].Font.color:=clSilver;
                                  Pan_Val[i].ReadOnly:=True;
                              end else Begin
                                  Pan_For[i].ReadOnly:=False;
                                  Pan_For[i].Font.color:=clWindowText;
                                  Pan_Val[i].ReadOnly:=False;
                                  Pan_Val[i].Font.color:=clWindowText;
			end;
				//{
			// ako ima ne�to u formuli zabrani upis vrijednosti ******************************************
			if (Length(Pan_For[i].text)>0) or
         (cb_sel[i].checked=false) then begin
                                  Pan_Val[i].Font.color:=clSilver;
                                  Pan_Val[i].ReadOnly:=True;
                              end else Begin
                                  Pan_Val[i].ReadOnly:=False;
                                  Pan_Val[i].Font.color:=clWindowText;
			end;
      end;  // if (i>5) or (i<8) then begin	
  end;   
  
  
  
End;

Procedure Check_CB(Sender: TObject);
Begin
   check_enable;
End;
Procedure Prop_on_off(Sender: TObject);
Begin
   If Pan_prop.Height=18 then Pan_prop.Height:=177
                         else Pan_prop.Height:=18; 
End;

Procedure Cuts_on_off(Sender: TObject);
Begin
   If Pan_cuts.Height=18 then Begin
                              Pan_cuts.Height:=335;
                              CB_Cuse.checked:=true;
                         end else begin
                              Pan_cuts.Height:=18; 
                              CB_Cuse.checked:=false;
                         end;
End;

Procedure Edge_on_off(Sender: TObject);
Begin
   // Showmessage('Edge_on_off');
   If Pan_Edge.Height=18 then Begin
                              Pan_Edge.Height:=200;
                              CB_Edge_use.checked:=true;
                         end else begin
                              Pan_Edge.Height:=18; 
                              CB_Edge_use.checked:=false;
                         end;
End;

Procedure cuts_resize(Sender: TObject);
Begin
   CB_Cuse.Left:=Pan_CutsH.clientwidth-15;
End;
   

Procedure edges_arrange;
var
   cbs,cbl:Integer;
Begin
   CB_edge_use.Left:=Pan_EdgeH.clientwidth-15;
   
   if Pan_Edge.clientwidth<600 then begin
         cbs:=round((Pan_Edge.clientwidth-4*3-64)/2);
         cbl:=round(Pan_Edge.clientwidth/2-cbs/2); 
      end else begin
         cbs:=round((600-4*3-64)/2); 
         cbl:=round(600/2-cbs/2);         
   end;
   CMB_E1.width:=cbs;
   CMB_E2.width:=cbs;
   CMB_E3.width:=cbs;
   CMB_E4.width:=cbs;
   CMB_E1.left:=cbl;
   CMB_E2.left:=3;
   CMB_E3.left:=3+cbs+3+64+3;
   CMB_E4.left:=cbl;
   Img_PH.Left:=round(cbs+6);
   Img_PF.Left:=round(cbs+6);
   Img_PV.Left:=round(cbs+6);
   Img_PM.Left:=round(cbs+6);
   CMB_E5.width:=cbs-5;
   CMB_E5.left:=130;
   CMB_E6.width:=cbs-5;
   CMB_E6.left:=67;
   Lab_U.left:=CMB_E6.left+cbs-5+2;
   CMB_E7.width:=cbs-5;
   CMB_E7.left:=Lab_U.left+Lab_U.width+3-3;
End;

Procedure Pan_Edge_resize(Sender: TObject);
Begin
   edges_arrange;
{
   If Pan_Edge.Height=18 then Begin
                              Pan_cuts.Height:=335;
                              CB_Cuse.checked:=true;
                         end else begin
                              Pan_cuts.Height:=18; 
                              CB_Cuse.checked:=false;
                         end;
                         }
End;


Procedure Change_Tip(Sender: TObject);
begin 
   Pan_Val[6].text:=copy(Cob_tip.Text,1,3);
end;

Procedure Napravi_osobine;
var
i,j:Integer;
begin
    for i:=0 to SelPlates.Count-1 do Begin  // redom daske iz liste
       // x, f(x)
       if CB_Sel[0].checked=True then begin
          if Pan_For[0].text<>razno then TDaska(SelPlates.Objects[i]).xFormula:=Pan_For[0].text;
          if (Length(Pan_For[0].text)=0) and (Pan_Val[0].text<>razno) then 
                    TDaska(SelPlates.Objects[i]).xPos:=StrToFloat(Pan_Val[0].text);                           
       end;  // if CB_Sel[0].checked=True
       // y, f(y)
       if CB_Sel[1].checked=True then begin
          if Pan_For[1].text<>razno then TDaska(SelPlates.Objects[i]).yFormula:=Pan_For[1].text;
          if (Length(Pan_For[1].text)=0) and (Pan_Val[1].text<>razno) then 
                    TDaska(SelPlates.Objects[i]).yPos:=StrToFloat(Pan_Val[1].text);                           
       end;  // if CB_Sel[1].checked=True
       // z, f(z)
       if CB_Sel[2].checked=True then begin
          if Pan_For[2].text<>razno then TDaska(SelPlates.Objects[i]).zFormula:=Pan_For[2].text;
          if (Length(Pan_For[2].text)=0) and (Pan_Val[2].text<>razno) then 
                    TDaska(SelPlates.Objects[i]).zPos:=StrToFloat(Pan_Val[2].text);                           
       end;  // if CB_Sel[2].checked=True
       // v, f(visina)
       if CB_Sel[3].checked=True then begin
          if Pan_For[3].text<>razno then TDaska(SelPlates.Objects[i]).hFormula:=Pan_For[3].text;
          if (Length(Pan_For[3].text)=0) and (Pan_Val[3].text<>razno) then 
                    TDaska(SelPlates.Objects[i]).visina:=StrToFloat(Pan_Val[3].text);                           
       end;  // if CB_Sel[3].checked=True
       // s, f(sirina)
       if CB_Sel[4].checked=True then begin
          if Pan_For[4].text<>razno then TDaska(SelPlates.Objects[i]).sFormula:=Pan_For[4].text;
          if (Length(Pan_For[4].text)=0) and (Pan_Val[4].text<>razno) then 
                    TDaska(SelPlates.Objects[i]).dubina:=StrToFloat(Pan_Val[4].text);                           
       end;  // if CB_Sel[4].checked=True
       // debljina, f(debljina)
       if CB_Sel[5].checked=True then begin
          if Pan_For[5].text<>razno then TDaska(SelPlates.Objects[i]).dFormula:=Pan_For[5].text;
          if (Length(Pan_For[5].text)=0) and (Pan_Val[5].text<>razno) then 
                    TDaska(SelPlates.Objects[i]).debljina:=StrToFloat(Pan_Val[5].text);                           
       end;  // if CB_Sel[4].checked=True
       // tip
       if CB_Sel[6].checked=True then 
          if CoB_tip.ItemIndex>-1 then 
                  TDaska(SelPlates.Objects[i]).TipDaske:=TTipDaske(StrToInt(Pan_Val[6].text));
       // primjedba   
       // if CB_Sel[7].checked=True then TDaska(SelPlates.Objects[i]).primjedba:=Pan_For[7].Text;
       
		 if CB_Sel[7].checked=True then begin
          if Pan_For[7].text<>razno then TDaska(SelPlates.Objects[i]).primjedba:=Pan_For[7].text;
       end;  // if CB_Sel[7].checked=True
		 
		 // xkut, f(xkut)
       if CB_Sel[8].checked=True then begin
          if Pan_For[8].text<>razno then TDaska(SelPlates.Objects[i]).kxFormula:=Pan_For[8].text;
          if (Length(Pan_For[8].text)=0) and (Pan_Val[8].text<>razno) then 
                    TDaska(SelPlates.Objects[i]).xkut:=StrToFloat(Pan_Val[8].text);                           
       end;  // if CB_Sel[0].checked=True
		 
		 // ykut, f(ykut)
       if CB_Sel[9].checked=True then begin
          if Pan_For[9].text<>razno then TDaska(SelPlates.Objects[i]).kyFormula:=Pan_For[9].text;
          if (Length(Pan_For[9].text)=0) and (Pan_Val[9].text<>razno) then 
                    TDaska(SelPlates.Objects[i]).xkut:=StrToFloat(Pan_Val[9].text);                           
       end;  // if CB_Sel[0].checked=True
		
		// zkut, f(zkut)
       if CB_Sel[10].checked=True then begin
          if Pan_For[10].text<>razno then TDaska(SelPlates.Objects[i]).kzFormula:=Pan_For[10].text;
          if (Length(Pan_For[10].text)=0) and (Pan_Val[10].text<>razno) then 
                    TDaska(SelPlates.Objects[i]).zkut:=StrToFloat(Pan_Val[10].text);                           
       end;  // if CB_Sel[0].checked=True	
		 
    End; // for i:=0 to SelPlates.Count-1
    
    
end;

Procedure ispis_varijabli;
// za provjeru 
var
   i:Integer;
Begin
   ShowD('PROC - ispis_varijabli');
   ShowD('k    A     B     C      D     E     F     G     R   Ceo');
   For i:=1 to 4 do begin
      
      ShowD(IntToStr(i)+'.'+FloatToStrC1(Var_A[i],8,2)+'.'+FloatToStrC1(Var_B[i],8,2)+'.'+ FloatToStrC1(Var_C[i],8,2)+
            '.'+FloatToStrC1(Var_D[i],8,2)+'.'+FloatToStrC1(Var_E[i],8,2)+'.'+ FloatToStrC1(Var_F[i],8,2)+
            '.'+FloatToStrC1(Var_G[i],8,2)+'.'+FloatToStrC1(Var_R[i],8,2)+'.'+IntToStr(Ceo[i]));
   End;
   ShowD('END PROC - ispis_varijabli');
End;

Procedure dodaj_varijable_rezova(kriv:TTockeSystem);
// upisuje u krivulju varijable sa vrijednostima
Var
   i:integer;
Begin
   ShowD('PROC: dodaj_varijable_rezova');
   For i:=1 to 4 do begin
       rcCurVarAdd(kriv,'cc'+IntToStr(i),IntToStr(CCS[i])     );
       rcCurVarAdd(kriv,'ec'+IntToStr(i),IntToStr(ECS[i])     );
       rcCurVarAdd(kriv,'a' +IntToStr(i),floattostrC1(Var_A[i],8,2) );
       rcCurVarAdd(kriv,'b' +IntToStr(i),floattostrC1(Var_B[i],8,2) );
       rcCurVarAdd(kriv,'c' +IntToStr(i),floattostrC1(Var_C[i],8,2) );
       rcCurVarAdd(kriv,'d' +IntToStr(i),floattostrC1(Var_D[i],8,2) );
       rcCurVarAdd(kriv,'e' +IntToStr(i),floattostrC1(Var_E[i],8,2) );
       rcCurVarAdd(kriv,'f' +IntToStr(i),floattostrC1(Var_F[i],8,2) );
       rcCurVarAdd(kriv,'g' +IntToStr(i),floattostrC1(Var_G[i],8,2) );
       rcCurVarAdd(kriv,'r' +IntToStr(i),floattostrC1(Var_R[i],8,2) );
       rcCurVarAdd(kriv,'ceo' +IntToStr(i),IntToStr(Ceo[i])   );
       
   End;

End;


Procedure Napravi_Frontalne_rezove(kriv:TRubneTocke);
// koristi se i za vertikalne jer je sve isto
var
   td:TDaska;
   sts:single;
   stt:String;
   i,bk:Integer;
   Parent_kriv:TTockeSystem;
Begin
   ShowD('START Procedure Napravi_Frontalne_rezove(kriv:TRubneTocke);');
      // dodaj varijable krivulja
      Parent_kriv:=kriv.owner;
      dodaj_varijable_rezova(Parent_kriv);
      
      td:=kriv.parentd;
      // rasporedi to�ke
      
      // �o�ak dolje lijevo
         // nema krivulje
      if CCS[1]=0 then begin  
                              rcPointStyle(Kriv,27,rLine);
                              rcPointStyle(Kriv,0 ,rLine);
                              rcPointPosF(kriv,27,'0' ,'0' ,'0','0');
                              rcPointPosF(kriv,0, '0' ,'0' ,'0','0');
                              rcPointPosF(kriv,1, '0' ,'0' ,'0','0')  
                       end;
         // kosi rez
      if CCS[1]=1 then begin  
                              rcPointStyle(Kriv,27,rLine);
                              rcPointStyle(Kriv,0 ,rLine);
                              rcPointPosF(kriv,27,'a1','0' ,'0','0');
                              rcPointPosF(kriv,0, 'a1/2','b1/2','0','0');
                              rcPointPosF(kriv,1, '0' ,'b1','0','0')  
                       end;
         // L rez              
      if CCS[1]=2 then begin  
                              rcPointStyle(Kriv,27,rLine);
                              rcPointStyle(Kriv,0 ,rLine);
                              rcPointPosF(kriv,27,'a1'   ,'0'   ,'0','0');
                              rcPointPosF(kriv,0, 'a1'   ,'b1'  ,'0','0');
                              rcPointPosF(kriv,1, '0'    ,'b1'  ,'0','0')  
                       end;
         // radius
      if CCS[1]=3 then begin  
                              rcPointStyle(Kriv,27,rArc);
                              rcPointArcData(Kriv,27,ArPlus,true);
                              rcPointStyle(Kriv,0,rArc);
                              rcPointArcData(Kriv,0,ArPlus,true);
                              rcPointPosF(kriv,27,'r1'   ,'0'   ,'0','r1');
                              rcPointPosF(kriv,0, 'r1-(r1/sqrt(2))' ,'r1-(r1/sqrt(2))' ,'0','r1');
                              rcPointPosF(kriv,1, '0'    ,'r1'  ,'0','0')            
                              rcPointSegDens(Kriv,27,segment_density);
                              rcPointSegDens(Kriv,0,segment_density);
                       end;   
      
      // �o�ak gore lijevo
         // nema krivulje      
      if CCS[2]=0 then begin  
                              rcPointStyle(Kriv,6,rLine);
                              rcPointStyle(Kriv,7 ,rLine);
                              rcPointPosF(kriv,6, '0' ,'b' ,'0','0');
                              rcPointPosF(kriv,7, '0' ,'b' ,'0','0');
                              rcPointPosF(kriv,8, '0' ,'b' ,'0','0')  
                       end;
         // kosi rez
      if CCS[2]=1 then begin  
                              rcPointStyle(Kriv,6,rLine);
                              rcPointStyle(Kriv,7 ,rLine);
                              rcPointPosF(kriv,6,'0','b-a2' ,'0','0');
                              rcPointPosF(kriv,7, 'b2/2','b-a2/2','0','0');
                              rcPointPosF(kriv,8, 'b2' ,'b','0','0')  
                       end;
         // L rez              
      if CCS[2]=2 then begin  
                              rcPointStyle(Kriv,6,rLine);
                              rcPointStyle(Kriv,7 ,rLine);
                              rcPointPosF(kriv,6,'0','b-a2' ,'0','0');
                              rcPointPosF(kriv,7, 'b2','b-a2','0','0');
                              rcPointPosF(kriv,8, 'b2' ,'b','0','0') 
                       end;
          // radius             
      if CCS[2]=3 then begin  
                              rcPointStyle(Kriv,6,rArc);
                              rcPointArcData(Kriv,6,ArPlus,true);
                              rcPointStyle(Kriv,7,rArc);
                              rcPointArcData(Kriv,7,ArPlus,true);
                              rcPointPosF(kriv,6,'0'   ,'b-r2'   ,'0','r2');
                              rcPointPosF(kriv,7, 'r2-(r2/sqrt(2))' ,'b-r2+(r2/sqrt(2))' ,'0','r2');
                              rcPointPosF(kriv,8, 'r2'    ,'b'  ,'0','0');  
                              rcPointSegDens(Kriv,6,segment_density);
                              rcPointSegDens(Kriv,7,segment_density);
                       end;   
      
      // �o�ak gore desno
         // nema reza
      if CCS[3]=0 then begin  
                              rcPointStyle(Kriv,13,rLine);
                              rcPointStyle(Kriv,14 ,rLine);
                              rcPointPosF(kriv,13, 'l' ,'b' ,'0','0');
                              rcPointPosF(kriv,14, 'l' ,'b' ,'0','0');
                              rcPointPosF(kriv,15, 'l' ,'b' ,'0','0')  
                       end;
         // kosi rez
      if CCS[3]=1 then begin  
                              rcPointStyle(Kriv,13,rLine);
                              rcPointStyle(Kriv,14 ,rLine);
                              rcPointPosF(kriv,13,'l-a3'  ,'b'     ,'0','0');
                              rcPointPosF(kriv,14,'l-a3/2','b-b3/2','0','0');
                              rcPointPosF(kriv,15,'l'     ,'b-b3'  ,'0','0')  
                       end;
         // L rez              
      if CCS[3]=2 then begin  
                              rcPointStyle(Kriv,13,rLine);
                              rcPointStyle(Kriv,14 ,rLine);
                              rcPointPosF(kriv,13,'l-a3'  ,'b'     ,'0','0');
                              rcPointPosF(kriv,14,'l-a3','b-b3','0','0');
                              rcPointPosF(kriv,15,'l'     ,'b-b3'  ,'0','0')  
                       end;
         // radius
      if CCS[3]=3 then begin  
                              rcPointStyle(Kriv,13,rArc);
                              rcPointArcData(Kriv,13,ArPlus,true);
                              rcPointStyle(Kriv,14,rArc);
                              rcPointArcData(Kriv,14,ArPlus,true);
                              rcPointPosF(kriv,13,'l-r3'   ,'b'   ,'0','r3');
                              rcPointPosF(kriv,14,'l-r3+(r3/sqrt(2))' ,'b-r3+(r3/sqrt(2))' ,'0','r3');
                              rcPointPosF(kriv,15,'l'    ,'b-r3'  ,'0','0'); 
                              rcPointSegDens(Kriv,13,segment_density);
                              rcPointSegDens(Kriv,14,segment_density);                              
                       end;   
   
      // �o�ak dolje desno
         // nema reza
      if CCS[4]=0 then begin  
                              rcPointStyle(Kriv,20,rLine);
                              rcPointStyle(Kriv,21 ,rLine);
                              rcPointPosF(kriv,20, 'l' ,'0' ,'0','0');
                              rcPointPosF(kriv,21, 'l' ,'0' ,'0','0');
                              rcPointPosF(kriv,22, 'l' ,'0' ,'0','0')  
                       end;
         // kosi rez
      if CCS[4]=1 then begin  
                              rcPointStyle(Kriv,20,rLine);
                              rcPointStyle(Kriv,21 ,rLine);
                              rcPointPosF(kriv,20,'l'  ,'a4'     ,'0','0');
                              rcPointPosF(kriv,21,'l-b4/2','a4/2','0','0');
                              rcPointPosF(kriv,22,'l-b4'  ,'0'  ,'0','0')  
                       end;
         // L rez             
      if CCS[4]=2 then begin  
                              rcPointStyle(Kriv,20,rLine);
                              rcPointStyle(Kriv,21 ,rLine);
                              rcPointPosF(kriv,20,'l'  ,'a4'     ,'0','0');
                              rcPointPosF(kriv,21,'l-b4','a4','0','0');
                              rcPointPosF(kriv,22,'l-b4'  ,'0'  ,'0','0')   
                       end;
         // radius          
      if CCS[4]=3 then begin  
                              rcPointStyle(Kriv,20,rArc);
                              rcPointArcData(Kriv,20,ArPlus,true);
                              rcPointStyle(Kriv,21,rArc);
                              rcPointArcData(Kriv,21,ArPlus,true);
                              rcPointPosF(kriv,20,'l'   ,'r4'   ,'0','r4');
                              rcPointPosF(kriv,21,'l-r4+(r4/sqrt(2))' ,'r4-(r4/sqrt(2))' ,'0','r4');
                              rcPointPosF(kriv,22,'l-r4'    ,'0'  ,'0','0'); 
                              rcPointSegDens(Kriv,20,segment_density);
                              rcPointSegDens(Kriv,21,segment_density);      
                       end;      
      
      // brid lijevo
      if ECS[1]=0 then begin     
                                 stS:=rcPointReadY(kriv,1);   // zadnja to�ka iz �o�ka
                                 stt:=floattostrC1(sts,8,2);
                                 
                                 rcPointPosF(kriv,2,'0' ,stt   ,'0','0');
                                 rcPointPosF(kriv,3,'0' ,stt,'0','0');
                                 rcPointPosF(kriv,4,'0' ,stt  ,'0','0');
                                 rcPointPosF(kriv,5,'0' ,stt  ,'0','0')  
                       end;   
                       
      if ECS[1]=1 then begin  
                              if ceo[1]=0 then begin
                                 rcPointPosF(kriv,2,'0'  ,'c1'   ,'0','0');
                                 rcPointPosF(kriv,3,'d1' ,'c1' ,'0','0');
                                 rcPointPosF(kriv,4,'f1' ,'c1+e1'  ,'0','0');
                                 rcPointPosF(kriv,5,'0'  ,'c1+e1'  ,'0','0')  
                                    end else begin
                                 rcPointPosF(kriv,2,'0'  ,'b-g1-e1'   ,'0','0');
                                 rcPointPosF(kriv,3,'d1' ,'b-g1-e1' ,'0','0');
                                 rcPointPosF(kriv,4,'f1' ,'b-g1'  ,'0','0');
                                 rcPointPosF(kriv,5,'0'  ,'b-g1'  ,'0','0')     
                              end;
                       end;      
      
      // brid gore
      if ECS[2]=0 then begin     
                                 stS:=rcPointReadX(kriv,8);   // zadnja to�ka iz �o�ka
                                 stt:=floattostrC1(sts,8,2);
                                 
                                 rcPointPosF(kriv,9, stt ,'b'  ,'0','0');
                                 rcPointPosF(kriv,10,stt ,'b'  ,'0','0');
                                 rcPointPosF(kriv,11,stt ,'b'  ,'0','0');
                                 rcPointPosF(kriv,12,stt ,'b'  ,'0','0')  
                              
                       end;  

      if ECS[2]=1 then begin  
                              if ceo[2]=0 then begin
                                 rcPointPosF(kriv,9, 'c2'    ,'b'    ,'0','0');
                                 rcPointPosF(kriv,10,'c2'    ,'b-d2' ,'0','0');
                                 rcPointPosF(kriv,11,'c2+e2' ,'b-f2' ,'0','0');
                                 rcPointPosF(kriv,12,'c2+e2' ,'b'    ,'0','0')  
                                    end else begin
                                 rcPointPosF(kriv,9, 'l-g2-e2' ,'b'    ,'0','0');
                                 rcPointPosF(kriv,10,'l-g2-e2' ,'b-d2' ,'0','0');
                                 rcPointPosF(kriv,11,'l-g2'    ,'b-f2' ,'0','0');
                                 rcPointPosF(kriv,12,'l-g2'    ,'b'    ,'0','0')     
                              end;
                       end;                     
                       
      // brid desno
      if ECS[3]=0 then begin     
                                 stS:=rcPointReadY(kriv,15);   // zadnja to�ka iz �o�ka
                                 stt:=floattostrC1(sts,8,2);
                                 
                                 rcPointPosF(kriv,16,'l' ,stt  ,'0','0');
                                 rcPointPosF(kriv,17,'l' ,stt  ,'0','0');
                                 rcPointPosF(kriv,18,'l' ,stt  ,'0','0');
                                 rcPointPosF(kriv,19,'l' ,stt  ,'0','0')  
                              
                       end;  

      if ECS[3]=1 then begin  
                              if ceo[3]=0 then begin
                                 rcPointPosF(kriv,16,'l'    ,'b-c3'    ,'0','0');
                                 rcPointPosF(kriv,17,'l-d3' ,'b-c3'    ,'0','0');
                                 rcPointPosF(kriv,18,'l-f3' ,'b-c3-e3' ,'0','0');
                                 rcPointPosF(kriv,19,'l'    ,'b-c3-e3' ,'0','0')  
                                    end else begin
                                 rcPointPosF(kriv,16,'l'   ,'g3+e3' ,'0','0');
                                 rcPointPosF(kriv,17,'l-d3','g3+e3' ,'0','0');
                                 rcPointPosF(kriv,18,'l-f3','g3'    ,'0','0');
                                 rcPointPosF(kriv,19,'l'   ,'g3'    ,'0','0')     
                              end;
                       end;                                     
                       
      // brid dolje
      if ECS[4]=0 then begin     
                                 stS:=rcPointReadX(kriv,22);   // zadnja to�ka iz �o�ka
                                 stt:=floattostrC1(sts,8,2);
                                 
                                 rcPointPosF(kriv,23,stt ,'0' ,'0','0');
                                 rcPointPosF(kriv,24,stt ,'0' ,'0','0');
                                 rcPointPosF(kriv,25,stt ,'0' ,'0','0');
                                 rcPointPosF(kriv,26,stt ,'0' ,'0','0')  
                              
                       end;  

      if ECS[4]=1 then begin  
                              if ceo[4]=0 then begin
                                 rcPointPosF(kriv,23,'l-c4'   ,'0'    ,'0','0');
                                 rcPointPosF(kriv,24,'l-c4'   ,'d4'   ,'0','0');
                                 rcPointPosF(kriv,25,'l-c4-e4','f4'   ,'0','0');
                                 rcPointPosF(kriv,26,'l-c4-e4','0'    ,'0','0')  
                                    end else begin
                                 rcPointPosF(kriv,23,'g4+e4' ,'0'  ,'0','0');
                                 rcPointPosF(kriv,24,'g4+e4' ,'d4' ,'0','0');
                                 rcPointPosF(kriv,25,'g4'    ,'f4' ,'0','0');
                                 rcPointPosF(kriv,26,'g4'    ,'0'  ,'0','0')     
                              end;
                       end;                                     
   ShowD('END Procedure Napravi_Frontalne_rezove(kriv:TRubneTocke);');  
   // Showmessage('END Procedure Napravi_Frontalne_rezove(kriv:TRubneTocke);');
 
End;

Procedure Napravi_vertikalne_rezove(kriv:TRubneTocke);
var
   td:TDaska;
   vis,sir:single;
   i,bk:Integer;
Begin
{
   bk:=GetArrayLength(curve);
   for i:=0 to bk-1 do begin    // sakupljene krivulje
      td:=curve[i].parentd;
      vis:=td.Visina;
      sir:=td.Dubina;
      // rcPointPosF(curve[i],1,0,0+20,0,0);  
      if CCS[1]=0 then begin  rcPointPosF(curve[i],27,'0' ,'0' ,'0','0');
                              rcPointPosF(curve[i],0, '0' ,'0' ,'0','0');
                              rcPointPosF(curve[i],1, '0' ,'0' ,'0','0')  end;
      if CCS[1]=1 then begin  rcPointPosF(curve[i],27,'a1','0' ,'0','6');
                              rcPointPosF(curve[i],0, 'a1/2','b1/2','0','0');
                              rcPointPosF(curve[i],1, '0' ,'b1','0','0')  end;
      if CCS[1]=2 then begin  rcPointPosF(curve[i],27,'a1','0' ,'0','6');
                              rcPointPosF(curve[i],0, 'a1','b1','0','0');
                              rcPointPosF(curve[i],1, '0' ,'b1','0','0')  end;                        
      if CCS[1]=3 then begin  rcPointPosF(curve[i],27,'r1','0' ,'0','6');
                              rcPointPosF(curve[i],0, 'a1','b1','0','0');
                              rcPointPosF(curve[i],1, '0' ,'r1','0','0')  end;                        
                              
                              
   end;  // for i:=0 to bk-1
}
End;


Procedure Napravi_Horizontalne_rezove(kriv:TRubneTocke);

var
   td:TDaska;
   sts:single;
   stt:String;
   i,bk:Integer;
   Parent_kriv:TTockeSystem;
Begin
      // dodaj varijable krivulja
      Parent_kriv:=kriv.owner;
      dodaj_varijable_rezova(Parent_kriv);
      
      td:=kriv.parentd;
      // rasporedi to�ke
      
      // �o�ak naprijed lijevo
      if CCS[1]=0 then begin  
                              rcPointStyle(Kriv,27,rLine);
                              rcPointStyle(Kriv,0 ,rLine);
                              rcPointPosF(kriv,27,'0' ,'0' ,'0','0');
                              rcPointPosF(kriv,0, '0' ,'0' ,'0','0');
                              rcPointPosF(kriv,1, '0' ,'0' ,'0','0')  
                       end;
                              
      if CCS[1]=1 then begin  
                              rcPointStyle(Kriv,27,rLine);
                              rcPointStyle(Kriv,0 ,rLine);
                              rcPointPosF(kriv,27,'b1','0' ,'0','0');
                              rcPointPosF(kriv,0, 'b1/2','a1/2','0','0');
                              rcPointPosF(kriv,1, '0' ,'a1','0','0')  
                       end;
                       
      if CCS[1]=2 then begin  
                              rcPointStyle(Kriv,27,rLine);
                              rcPointStyle(Kriv,0 ,rLine);
                              rcPointPosF(kriv,27,'b1'   ,'0'   ,'0','0');
                              rcPointPosF(kriv,0, 'b1'   ,'a1'  ,'0','0');
                              rcPointPosF(kriv,1, '0'    ,'a1'  ,'0','0')  
                       end;
                       
      if CCS[1]=3 then begin  
                              rcPointStyle(Kriv,27,rArc);
                              rcPointArcData(Kriv,27,ArPlus,true);
                              rcPointStyle(Kriv,0,rArc);
                              rcPointArcData(Kriv,0,ArPlus,true);
                              rcPointPosF(kriv,27,'r1'   ,'0'   ,'0','r1');
                              rcPointPosF(kriv,0, 'r1-(r1/sqrt(2))' ,'r1-(r1/sqrt(2))' ,'0','r1');
                              rcPointPosF(kriv,1, '0'    ,'r1'  ,'0','0');
                              rcPointSegDens(Kriv,27,segment_density);
                              rcPointSegDens(Kriv,0,segment_density);   
                       end;   
      
      // �o�ak otraga lijevo
      if CCS[2]=0 then begin  
                              rcPointStyle(Kriv,20,rLine);
                              rcPointStyle(Kriv,21 ,rLine);
                              rcPointPosF(kriv,20, 'l' ,'0' ,'0','0');
                              rcPointPosF(kriv,21, 'l' ,'0' ,'0','0');
                              rcPointPosF(kriv,22, 'l' ,'0' ,'0','0')  
                       end;
      
      if CCS[2]=1 then begin  
                              rcPointStyle(Kriv,20,rLine);
                              rcPointStyle(Kriv,21 ,rLine);
                              rcPointPosF(kriv,20,'l'  ,'b2'     ,'0','0');
                              rcPointPosF(kriv,21,'l-a2/2','b2/2','0','0');
                              rcPointPosF(kriv,22,'l-a2'  ,'0'  ,'0','0') 
                       end;
                       
      if CCS[2]=2 then begin  
                              rcPointStyle(Kriv,20,rLine);
                              rcPointStyle(Kriv,21 ,rLine);
                              rcPointPosF(kriv,20,'l'  ,'b2'     ,'0','0');
                              rcPointPosF(kriv,21,'l-a2','b2','0','0');
                              rcPointPosF(kriv,22,'l-a2'  ,'0'  ,'0','0') 
                       end;
                      
      if CCS[2]=3 then begin  
                              rcPointStyle(Kriv,20,rArc);
                              rcPointArcData(Kriv,20,ArPlus,true);
                              rcPointStyle(Kriv,21,rArc);
                              rcPointArcData(Kriv,21,ArPlus,true);
                              rcPointPosF(kriv,20,'l'   ,'r2'   ,'0','r2');
                              rcPointPosF(kriv,21,'l-r2+(r2/sqrt(2))' ,'r2-(r2/sqrt(2))' ,'0','r2');
                              rcPointPosF(kriv,22,'l-r2'    ,'0'  ,'0','0');   
                              rcPointSegDens(Kriv,20,segment_density);
                              rcPointSegDens(Kriv,21,segment_density);
                       end;   
      
      // �o�ak otraga desno
      if CCS[3]=0 then begin  
                              rcPointStyle(Kriv,13,rLine);
                              rcPointStyle(Kriv,14 ,rLine);
                              rcPointPosF(kriv,13, 'l' ,'b' ,'0','0');
                              rcPointPosF(kriv,14, 'l' ,'b' ,'0','0');
                              rcPointPosF(kriv,15, 'l' ,'b' ,'0','0')  
                       end;
       
      if CCS[3]=1 then begin  
                              rcPointStyle(Kriv,13,rLine);
                              rcPointStyle(Kriv,14 ,rLine);
                              rcPointPosF(kriv,13,'l-b3'  ,'b'     ,'0','0');
                              rcPointPosF(kriv,14,'l-b3/2','b-a3/2','0','0');
                              rcPointPosF(kriv,15,'l'     ,'b-a3'  ,'0','0')  
                       end;
                       
      if CCS[3]=2 then begin  
                              rcPointStyle(Kriv,13,rLine);
                              rcPointStyle(Kriv,14 ,rLine);
                              rcPointPosF(kriv,13,'l-b3'  ,'b'     ,'0','0');
                              rcPointPosF(kriv,14,'l-b3','b-a3','0','0');
                              rcPointPosF(kriv,15,'l'     ,'b-a3'  ,'0','0')  
                       end;
                       
      if CCS[3]=3 then begin  
                              rcPointStyle(Kriv,13,rArc);
                              rcPointArcData(Kriv,13,ArPlus,true);
                              rcPointStyle(Kriv,14,rArc);
                              rcPointArcData(Kriv,14,ArPlus,true);
                              rcPointPosF(kriv,13,'l-r3'   ,'b'   ,'0','r3');
                              rcPointPosF(kriv,14,'l-r3+(r3/sqrt(2))' ,'b-r3+(r3/sqrt(2))' ,'0','r3');
                              rcPointPosF(kriv,15,'l'    ,'b-r3'  ,'0','0');  
                              rcPointSegDens(Kriv,13,segment_density);
                              rcPointSegDens(Kriv,14,segment_density);
                       end;   
      
   // �o�ak naprijed desno
      if CCS[4]=0 then begin  
                              rcPointStyle(Kriv,6,rLine);
                              rcPointStyle(Kriv,7 ,rLine);
                              rcPointPosF(kriv,6, '0' ,'b' ,'0','0');
                              rcPointPosF(kriv,7, '0' ,'b' ,'0','0');
                              rcPointPosF(kriv,8, '0' ,'b' ,'0','0')  
                       end;
      
      if CCS[4]=1 then begin  
                              rcPointStyle(Kriv,6,rLine);
                              rcPointStyle(Kriv,7 ,rLine);
                              rcPointPosF(kriv,6,'0'  ,'b-b4'     ,'0','0');
                              rcPointPosF(kriv,7,'a4/2','b-b4/2','0','0');
                              rcPointPosF(kriv,8,'a4'  ,'b'  ,'0','0')  
                       end;
                       
      if CCS[4]=2 then begin  
                              rcPointStyle(Kriv,6,rLine);
                              rcPointStyle(Kriv,7 ,rLine);
                              rcPointPosF(kriv,6,'0'  ,'b-b4'     ,'0','0');
                              rcPointPosF(kriv,7,'a4','b-b4','0','0');
                              rcPointPosF(kriv,8,'a4'  ,'b'  ,'0','0')     
                       end;
                       
      if CCS[4]=3 then begin  
                              rcPointStyle(Kriv,6,rArc);
                              rcPointArcData(Kriv,6,ArPlus,true);
                              rcPointStyle(Kriv,7,rArc);
                              rcPointArcData(Kriv,7,ArPlus,true);
                              rcPointPosF(kriv,6,'0'   ,'b-r4'   ,'0','r4');
                              rcPointPosF(kriv,7,'r4-(r4/sqrt(2))' ,'b-r4+(r4/sqrt(2))' ,'0','r4');
                              rcPointPosF(kriv,8,'r4'    ,'b'  ,'0','0')  ;
                              rcPointSegDens(Kriv,6,segment_density);
                              rcPointSegDens(Kriv,7,segment_density);
                       end;      
      
      // brid dolje
      if ECS[4]=0 then begin     
                                 stS:=rcPointReadY(kriv,1);   // zadnja to�ka iz �o�ka
                                 stt:=floattostrC1(sts,8,2);
                                 
                                 rcPointPosF(kriv,2,'0' ,stt   ,'0','0');
                                 rcPointPosF(kriv,3,'0' ,stt,'0','0');
                                 rcPointPosF(kriv,4,'0' ,stt  ,'0','0');
                                 rcPointPosF(kriv,5,'0' ,stt  ,'0','0')  
                       end;   
                       
      if ECS[4]=1 then begin  
                              if ceo[4]=0 then begin
                                 rcPointPosF(kriv,2,'0'  ,'b-c4-e4'  ,'0','0');
                                 rcPointPosF(kriv,3,'f4' ,'b-c4-e4'  ,'0','0');
                                 rcPointPosF(kriv,4,'d4' ,'b-c4'     ,'0','0');
                                 rcPointPosF(kriv,5,'0'  ,'b-c4'     ,'0','0')  
                                    end else begin
                                 rcPointPosF(kriv,2,'0'  ,'g4'     ,'0','0');
                                 rcPointPosF(kriv,3,'f4' ,'g4'     ,'0','0');
                                 rcPointPosF(kriv,4,'d4' ,'g4+e4'  ,'0','0');
                                 rcPointPosF(kriv,5,'0'  ,'g4+e4'  ,'0','0')     
                              end;
                       end;      
      
      // brid gore
      if ECS[2]=0 then begin     
                                 stS:=rcPointReadY(kriv,15);   // zadnja to�ka iz �o�ka
                                 stt:=floattostrC1(sts,8,2);
                                 
                                 rcPointPosF(kriv,16,'l',stt   ,'0','0');
                                 rcPointPosF(kriv,17,'l',stt   ,'0','0');
                                 rcPointPosF(kriv,18,'l',stt   ,'0','0');
                                 rcPointPosF(kriv,19,'l',stt   ,'0','0')  
                              
                       end;  
      
      if ECS[2]=1 then begin  
                              if ceo[2]=0 then begin
                                 rcPointPosF(kriv,16,'l'  ,'c2+e2' ,'0','0');
                                 rcPointPosF(kriv,17,'l-f2','c2+e2' ,'0','0');
                                 rcPointPosF(kriv,18,'l-d2','c2'    ,'0','0');
                                 rcPointPosF(kriv,19,'l'   ,'c2'    ,'0','0')  
                                    end else begin
                                 rcPointPosF(kriv,16,'l'    ,'b-g2'    ,'0','0');
                                 rcPointPosF(kriv,17,'l-f2' ,'b-g2'    ,'0','0');
                                 rcPointPosF(kriv,18,'l-d2' ,'b-g2-e2' ,'0','0');
                                 rcPointPosF(kriv,19,'l'    ,'b-g2-e2' ,'0','0')     
                              end;
                       end;                     
                       
      // brid desno
      if ECS[3]=0 then begin     
                                 stS:=rcPointReadX(kriv,8);   // zadnja to�ka iz �o�ka
                                 stt:=floattostrC1(sts,8,2);
                                 
                                 rcPointPosF(kriv, 9, stt, 'b'   ,'0','0');
                                 rcPointPosF(kriv,10, stt, 'b'   ,'0','0');
                                 rcPointPosF(kriv,11, stt, 'b'   ,'0','0');
                                 rcPointPosF(kriv,12, stt, 'b'   ,'0','0')  
                              
                       end;  
      
      if ECS[3]=1 then begin  
                              if ceo[3]=0 then begin
                                 rcPointPosF(kriv, 9,'l-c3-e3' ,'b'      ,'0','0');
                                 rcPointPosF(kriv,10,'l-c3-e3' ,'b-f3'   ,'0','0');
                                 rcPointPosF(kriv,11,'l-c3'    ,'b-d3'   ,'0','0');
                                 rcPointPosF(kriv,12,'l-c3'    ,'b'     ,'0','0')  
                                    end else begin
                                 rcPointPosF(kriv, 9,'g3'    ,'b'     ,'0','0');
                                 rcPointPosF(kriv,10,'g3'    ,'b-f3' ,'0','0');
                                 rcPointPosF(kriv,11,'g3+e3' ,'b-d3' ,'0','0');
                                 rcPointPosF(kriv,12,'g3+e3' ,'b'    ,'0','0')     
                              end;
                       end;                                     
                      
      // brid lijevo
      if ECS[1]=0 then begin     
                                 stS:=rcPointReadX(kriv,22);   // zadnja to�ka iz �o�ka
                                 stt:=floattostrC1(sts,8,2);
                                 
                                 rcPointPosF(kriv,23,stt ,'0' ,'0','0');
                                 rcPointPosF(kriv,24,stt ,'0' ,'0','0');
                                 rcPointPosF(kriv,25,stt ,'0' ,'0','0');
                                 rcPointPosF(kriv,26,stt ,'0' ,'0','0')  
                              
                       end;  
       
      if ECS[1]=1 then begin  
                              if ceo[1]=0 then begin
                                 rcPointPosF(kriv,23,'c1+e1','0'    ,'0','0');
                                 rcPointPosF(kriv,24,'c1+e1','f1'   ,'0','0');
                                 rcPointPosF(kriv,25,'c1'   ,'d1'   ,'0','0');
                                 rcPointPosF(kriv,26,'c1'   ,'0'    ,'0','0')  
                                    end else begin
                                 rcPointPosF(kriv,23,'l-g1'   ,'0'  ,'0','0');
                                 rcPointPosF(kriv,24,'l-g1'   ,'f1' ,'0','0');
                                 rcPointPosF(kriv,25,'l-g1-e1','d1' ,'0','0');
                                 rcPointPosF(kriv,26,'l-g1-e1','0'  ,'0','0')     
                              end;
                       end;                                     
      { }                
End;

Procedure Napravi_rezove;
var
   ttd:Tdaska; // temp daska
   Parent_kriv:TTockeSystem;   // sve krivulje u jednoj dasci
   Point: TRubneTocke;// tocke 
   i, j, bk:Integer;  // broj vanjskih krivulja
   td:Tdaska;
   
Begin
   // test krivulja
   ShowD('Trazim daske za obradu krivulja i punim u niz krivulja');
   bk:=0;
   for i:=0 to SelPlates.Count-1 do Begin
         if SelPlates.Objects[i] is Tdaska then begin 
            ttd:=TDaska(SelPlates.Objects[i]);
            ShowD('d('+IntToStr(i)+')= "'+ttd.naziv+'"');
            bk:=bk+1;
            SetArrayLength(curve,bk); 
            // curve[bk-1]:=ttd.TockeiRupe.Rubne;      // vanjska (osnovna) krivulja 
            curve[bk-1]:=ttd.TockeiRupe.Rubne;      // vanjska (osnovna) krivulja 
         end;
   end;
   ShowD('Prona�eno krivulja: '+IntToStr(bk));
   

   
   // �i��enje krivulja - makni sve to�ke osim osnovnih
   for i:=0 to bk-1 do begin    // sakupljene krivulje
      curve[i]:=rcClearMainCurve(curve[i]);
   end;   // for i
   ShowD('O�i��ene krivulje!');
   
   
   // postavljam nove to�ke na krivuljama - polo�aji nisu bitni
  
   for i:=0 to bk-1 do begin    // sakupljene krivulje
      td:=curve[i].parentd;
      Parent_kriv:=curve[i].owner;
      Parent_kriv.InitRubneTocke; 
      
      // rcPointAdd(curve[i],1,20,20,0);   ovo je bio test

      // na cosak 0 druga
      rcPointAdd(curve[i],1,0,0,0,0);  

      // na rub 0   
      rcPointAdd(curve[i],2,0,100,0,0);
      rcPointAdd(curve[i],3,0,120,0,0);
      rcPointAdd(curve[i],4,0,140,0,0);
      rcPointAdd(curve[i],5,0,160,0,0);

      // na cosak 1 prva
      rcPointAdd(curve[i],6,0,td.visina-20,0,0);

      // na cosak 1 druga
      rcPointAdd(curve[i],8,50,td.visina,0,2);

      // na rub 1   
      rcPointAdd(curve[i],9,100,td.visina,0,2);
      rcPointAdd(curve[i],10,130,td.visina,0,2);
      rcPointAdd(curve[i],11,160,td.visina,0,2);
      rcPointAdd(curve[i],12,190,td.visina,0,2);
      
      // na cosak 2 prva
      rcPointAdd(curve[i],13,td.dubina-20,td.visina,0,2);
      
      // na cosak 2 druga
      rcPointAdd(curve[i],15,td.dubina,td.visina-20,0,4);
      
      // na rub 2 
      rcPointAdd(curve[i],16,td.dubina,td.visina-100,0,4);
      rcPointAdd(curve[i],17,td.dubina,td.visina-130,0,4);
      rcPointAdd(curve[i],18,td.dubina,td.visina-160,0,4);
      rcPointAdd(curve[i],19,td.dubina,td.visina-190,0,4);
      
      // na cosak 3 prva
      rcPointAdd(curve[i],20,td.dubina,20,0,4);

      // na cosak 3 druga
      rcPointAdd(curve[i],22,td.dubina-20,0,0,6);
      
      // na rub 3 
      rcPointAdd(curve[i],23,td.dubina-100,0,0,6);
      rcPointAdd(curve[i],24,td.dubina-130,0,0,6);
      rcPointAdd(curve[i],25,td.dubina-160,0,0,6);
      rcPointAdd(curve[i],26,td.dubina-190,0,0,6);
      
      // na cosak 0 prva
      rcPointAdd(curve[i],27,20,0,0,6);
      curve[i].BildOutList;
   end;   // for i
   if e<>nil then e.RecalcFormula(e);     // refresh ekrana
   

   // napravi (kona�no) nove rezove
   
   for i:=0 to SelPlates.Count-1 do Begin   // selektirane daske
         if SelPlates.Objects[i] is Tdaska then begin 
            ttd:=TDaska(SelPlates.Objects[i]);
            bk:=bk+1;
            SetArrayLength(curve,bk); 
            curve[bk-1]:=ttd.TockeiRupe.Rubne;      // vanjska (osnovna) krivulja 
            
            if ttd.smjer=VertFront then napravi_frontalne_rezove(curve[bk-1]);
            if ttd.smjer=VertBok then napravi_frontalne_rezove(curve[bk-1]);
            if ttd.smjer=Horiz then napravi_horizontalne_rezove(curve[bk-1]);
            
            curve[bk-1].BildOutList;
            
            
         end;  // if SelPlates.Objects[i] is Tdaska
   end;  // for i:=0 to SelPlates.Count-1 

End;

Procedure napravi_rubne_trake;
var
   i,j:Integer;
   ttd:TDaska;
   pot:TPotrosni;
   traka:TDuzni;
Begin
  for i:=0 to SelPlates.Count-1 do Begin   // selektirane daske
         if SelPlates.Objects[i] is Tdaska then begin 
            ttd:=TDaska(SelPlates.Objects[i]);
            
            // napravi jednu po jednu
            if RB_EdgeOne.checked=true then begin 
                  if (ttd.smjer=VertFront) or (ttd.smjer=VertBok) then begin
                     if CMB_E4.ItemIndex>1 then begin
                                                   ShowD('postavljam (mijenjam) traku dolje'); 
                                                   // traka:=ttd.Potrosni.GetTraka(0);
                                                   // ShowD('1'); 
                                                   // traka.SetOnOf(0,true);
                                                   // if ttd.potrosni.prednja<>nil then ttd.potrosni.prednja.prednja:=true
                                                   //                             else ttd.potrosni.prednja.prednja:=true;
                                                   // ShowD('2'); 
                                                   ttd.potrosni.kantirajrub([0],CMB_E4.text);
                                                   // ShowD('3'); 
                                            end;
                     if CMB_E4.ItemIndex=0 then begin 
                                                   ShowD('bri�em traku dolje');
                                                   traka:=ttd.Potrosni.GetTraka(0);
                                                   if traka=nil then SHowD('Nema trake!')
                                                                else traka.SetOnOF(0,false); 
                                           end;
                    
                     if CMB_E1.ItemIndex>1 then begin
                                                   ShowD('postavljam (mijenjam) traku gore');     
                                                   ttd.potrosni.kantirajrub([1],CMB_E1.text);
                                            end;
                     if CMB_E1.ItemIndex=0 then begin 
                                                   ShowD('bri�em traku gore');
                                                   traka:=ttd.Potrosni.GetTraka(1);
                                                   if traka=nil then SHowD('Nema trake!')
                                                                else traka.SetOnOF(1,false); 
                                           end;    
                     
                     if CMB_E2.ItemIndex>1 then begin
                                                   ShowD('postavljam (mijenjam) traku lijevo'); 
                                                   //traka.SetOnOf(0,true);      
                                                   ttd.potrosni.kantirajrub([2],CMB_E2.text);
                                            end;
                     if CMB_E2.ItemIndex=0 then begin 
                                                   ShowD('bri�em traku lijevo');
                                                   traka:=ttd.Potrosni.GetTraka(2);
                                                    if traka=nil then SHowD('Nema trake!')
                                                                else traka.SetOnOF(2,false);
                                           end;     
                                           
                     if CMB_E3.ItemIndex>1 then begin
                                                   ShowD('postavljam (mijenjam) traku desno');     
                                                   ttd.potrosni.kantirajrub([3],CMB_E3.text);
                                            end;
                     if CMB_E3.ItemIndex=0 then begin 
                                                   ShowD('bri�em traku desno');
                                                   traka:=ttd.Potrosni.GetTraka(3);
                                                   if traka=nil then SHowD('Nema trake!')
                                                                else traka.SetOnOF(3,false);
                                           end;     
                                          
                                           
                  end else begin  // horizontal
                     if CMB_E2.ItemIndex>1 then begin
                                                   ShowD('postavljam (mijenjam) traku lijevo');     
                                                   ttd.potrosni.kantirajrub([0],CMB_E2.text);
                                                
                                            end;
                     if CMB_E2.ItemIndex=0 then begin 
                                                   ShowD('bri�em traku lijevo');
                                                   traka:=ttd.Potrosni.GetTraka(0);
                                                   if traka=nil then SHowD('Nema trake!')
                                                                else traka.SetOnOF(0,false);
                                           end;
                     
                     if CMB_E3.ItemIndex>1 then begin
                                                   ShowD('postavljam (mijenjam) traku desno');     
                                                   ttd.potrosni.kantirajrub([1],CMB_E3.text);
                                            end;
                     if CMB_E3.ItemIndex=0 then begin 
                                                   ShowD('bri�em traku desno');
                                                   traka:=ttd.Potrosni.GetTraka(1);
                                                   if traka=nil then SHowD('Nema trake!')
                                                                else traka.SetOnOF(1,false);
                                           end; 
                                             
                     if CMB_E4.ItemIndex>1 then begin
                                                   ShowD('postavljam (mijenjam) traku otraga');     
                                                   ttd.potrosni.kantirajrub([2],CMB_E4.text);
                                            end;
                     if CMB_E4.ItemIndex=0 then begin 
                                                   ShowD('bri�em traku otraga');
                                                   traka:=ttd.Potrosni.GetTraka(2);
                                                   if traka=nil then SHowD('Nema trake!')
                                                                else traka.SetOnOF(2,false);
                                           end;  
                     
                     if CMB_E1.ItemIndex>1 then begin
                                                   ShowD('postavljam (mijenjam) traku naprijed');     
                                                   ttd.potrosni.kantirajrub([3],CMB_E1.text);
                                            end;
                     if CMB_E1.ItemIndex=0 then begin 
                                                   ShowD('bri�em traku naprijed');
                                                   traka:=ttd.Potrosni.GetTraka(3);
                                                   if traka=nil then SHowD('Nema trake!')
                                                                else traka.SetOnOF(3,false);
                                           end;                               
                  {}
                  end;

            
            end; // if RB_EdgeOne.ckecked=true
            
            // sve zajedno 
            if RB_EdgeAll.checked=true then begin 
               
                  for j:=0 to 3 do begin
                      ShowD('Mijenjam sve zajedno. Trenutno rub: '+IntToStr(j));  
                      if CMB_E5.ItemIndex>1 then begin
                                                   ttd.potrosni.kantirajrub([j],CMB_E5.text);
                                            end;
                      if CMB_E5.ItemIndex=0 then begin 
                                                   traka:=ttd.Potrosni.GetTraka(j);
                                                   if traka=nil then SHowD('Nema trake!')
                                                                else traka.SetOnOF(j,false); 
                                            end;    
      
                  end;
            end; // if RB_EdgeAll.ckecked=true
  
            // zamjena 
            if RB_EdgeChange.checked=true then begin 
               for j:=0 to 3 do begin    // svi rubovi daske
                  ShowD('Zamjena. Trenutno rub: '+IntToStr(j));  
                  traka:=ttd.Potrosni.GetTraka(j);
                  if (traka.naziv=CMB_E6.Text) and (CMB_E7.ItemIndex > 1) then Begin 
                        ShowD('Zamijena ili kreiranje trake');
                        ttd.potrosni.kantirajrub([j],CMB_E7.text);
                  end;      
                  if (traka.naziv=CMB_E6.Text) and (CMB_E7.ItemIndex = 0) then Begin 
                        ShowD('Micanje trake');
                        if traka<>nil then traka.SetOnOF(j,false);
                  end;            
               end; // for j
            end; // if RB_EdgeChange.ckecked=true     
{            
       if (pt.naziv=Comb1.Text) then begin
          tek:=pt.naziv;                                // staro ime trake
          po.kantirajrub([i],Comb2.Text);               // postavlja rubnu traku pomocu naziva
          m1.lines.add ('   Strana broj: '+IntToStr(i)+' sa trakom "'+tek+'" zamijenjena je u traku "'+pt.naziv+'"!!! <=====');
       end else m1.lines.add ('   Strana broj: '+IntToStr(i)+' zadrzala je traku "'+pt.naziv+'".');
}
            
            
            
         end;  // if SelPlates.Objects[i] is Tdaska
   end;  // for i:=0 to SelPlates.Count-1  
End;


procedure Napravi(Sender: TObject);
// var

begin
                                      Napravi_osobine;
   if CB_Cuse.checked=true     then   Napravi_rezove;
   if CB_Edge_use.checked=true then   Napravi_rubne_trake;
   // Showmessage ('Done');
end;

Procedure Cut_Edit_OnChange(Sender:TObject);
var
   pass:Boolean;
   rr:Single;
   i:Integer;
Begin
 If TEdit(Sender).modified then begin   
    //if TEdit(Sender).modified 
   ShowD('PROC: Cut_Edit_OnChange');
   // check if is real number
   pass:=True;
   try
       rr:=StrToFloat(Tedit(Sender).text);
   except
       pass:=False; 
   end;
   
   if pass=False then Begin 
                        Tedit(Sender).text:='0';
                        Tedit(Sender).SelStart:=0;
                        Tedit(Sender).SelLength:=20;
                        rr:=StrToFloat(Tedit(Sender).text);
   End;
   
   For i:=1 to 4 do begin;
      if Sender = Ed_A then Begin 
                            if rr>Sl_A.Max then Sl_A.Max:=Round(rr);   // pove�ava max vrijednosr slide bara
                            Sl_A.Position:=Round(rr); 
                            IF TEdit(Sender).Tag=i then Var_A[i]:=rr;
                            If CB_B.checked=False then begin 
                                                            Ed_B.text:=TEdit(Sender).text;
                                                            Var_B[TEdit(Sender).Tag]:=rr; 
                                                            if rr>Sl_B.Max then Sl_B.Max:=Round(rr);
                                                            Sl_B.Position:=Round(rr);
                                                       end;  
                         end;  
      if Sender = Ed_B then Begin 
                            if rr>Sl_B.Max then Sl_B.Max:=Round(rr);   // pove�ava max vrijednosr slide bara
                            Sl_B.Position:=Round(rr); 
                            IF TEdit(Sender).Tag=i then Var_B[i]:=rr;                
                         end;  
                         
      if Sender = Ed_C then Begin 
                            if rr>Sl_C.Max then Sl_C.Max:=Round(rr);   // pove�ava max vrijednosr slide bara
                            Sl_B.Position:=Round(rr); 
                            IF TEdit(Sender).Tag=i then Var_C[i]:=rr;                  
                         end;  
   
      if Sender = Ed_D then Begin 
                            if rr>Sl_D.Max then Sl_D.Max:=Round(rr);   // pove�ava max vrijednosr slide bara
                            Sl_D.Position:=Round(rr); 
                            IF TEdit(Sender).Tag=i then Var_D[i]:=rr;
                            If CB_F.checked=False then begin 
                                                            Ed_F.text:=TEdit(Sender).text;
                                                            Var_F[TEdit(Sender).Tag]:=rr; 
                                                            if rr>Sl_F.Max then Sl_F.Max:=Round(rr);
                                                            Sl_F.Position:=Round(rr);
                                                       end;  
                         end;  
      if Sender = Ed_E then Begin 
                            if rr>Sl_E.Max then Sl_E.Max:=Round(rr);   // pove�ava max vrijednosr slide bara
                            Sl_E.Position:=Round(rr); 
                            IF TEdit(Sender).Tag=i then Var_E[i]:=rr;                  
                         end;  
      if Sender = Ed_F then Begin 
                            if rr>Sl_F.Max then Sl_F.Max:=Round(rr);   // pove�ava max vrijednosr slide bara
                            Sl_F.Position:=Round(rr); 
                            IF TEdit(Sender).Tag=i then Var_F[i]:=rr;                   
                         end;  
      if Sender = Ed_G then Begin 
                            if rr>Sl_G.Max then Sl_G.Max:=Round(rr);   // pove�ava max vrijednosr slide bara
                            Sl_G.Position:=Round(rr); 
                            IF TEdit(Sender).Tag=i then Var_G[i]:=rr;                  
                         end;  
      if Sender = Ed_R then Begin 
                            if rr>Sl_R.Max then Sl_R.Max:=Round(rr);   // pove�ava max vrijednosr slide bara
                            Sl_R.Position:=Round(rr); 
                            IF TEdit(Sender).Tag=i then Var_R[i]:=rr;                    
                         end;  
   End; // For i:=1 to 4

                         
 end; // If TEdit(Sender).modified
 ispis_varijabli;  
End;

procedure CheckIfReal(Sender: TObject);    // On Change
// poku�ava prebaciti string u real
var
   rr:single;
   pass:boolean;
   i,tt:Integer;
begin
   ShowD('procedure CheckIfReal(Sender: TObject);');
   pass:=True;
   try
       rr:=StrToFloat(Tedit(Sender).text);
   except
       pass:=False; 
   end;
   
   if pass=true then begin
         Tedit(Sender).font.color:=clWindowText;
         Tedit(Sender).font.style:=[];
         Tedit(Sender).color:=clWindow;
         ButOk.enabled:=True;
         
      end else begin
         Tedit(Sender).font.color:=clRed;
         Tedit(Sender).font.style:=[fsBold];
         Tedit(Sender).color:=clYellow;
         ButOk.enabled:=False;
   end;

end;

Procedure Sense_Over(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
   i,ofs,x1,x2,y1,y2:Integer;
begin
   Co:=0;
   ofs:=0;
   For i:=0 to 3 do begin
   
      x1:=ofs+Sh_C[i].left;
      x2:=ofs+Sh_C[i].left+Sh_C[i].width;
      y1:=ofs+Sh_C[i].top;
      y2:=ofs+Sh_C[i].top+Sh_C[i].height;
      if (x>x1) and (x<x2) and (y>y1) and (y<y2) then begin
            Sh_C[i].visible:=true;
            Co:=10+i;
         end else begin
            Sh_C[i].visible:=false; 
      end; 
      x1:=ofs+Sh_T[i].left;
      x2:=ofs+Sh_T[i].left+Sh_T[i].width;
      y1:=ofs+Sh_T[i].top;
      y2:=ofs+Sh_T[i].top+Sh_T[i].height;
      if (x>x1) and (x<x2) and (y>y1) and (y<y2) then begin
            Sh_T[i].visible:=true;
            Co:=20+i;
         end else begin
            Sh_T[i].visible:=false; 
      end;      
   end;   // For i:=0 to 3 
	x1:=ofs+Sh_S.left;
   x2:=ofs+Sh_S.left+Sh_S.width;
   y1:=ofs+Sh_S.top;
   y2:=ofs+Sh_S.top+Sh_S.height;
	if (x>x1) and (x<x2) and (y>y1) and (y<y2) then begin
			Sh_S.visible:=true;
			// Co:=20+i;
      end else begin
         Sh_S.visible:=false; 
   end;      
	
	
end;

procedure semafor;
VAR
   i, tt:Integer;
begin
// nevidljivo sve!
   H1N_0.visible:=false; H1T_0.visible:=false; H2N_0.visible:=false; H2T_0.visible:=false; 
   H3N_0.visible:=false; H3T_0.visible:=false; H4N_0.visible:=false; H4T_0.visible:=false;
   H1R_0.visible:=false; H2R_0.visible:=false; H3R_0.visible:=false; H4R_0.visible:=false;
   H1R_1.visible:=false; H2R_1.visible:=false; H3R_1.visible:=false; H4R_1.visible:=false;
   H1I_0.visible:=false; H2I_0.visible:=false; H3I_0.visible:=false; H4I_0.visible:=false;
   H1I_1.visible:=false; H2I_1.visible:=false; H3I_1.visible:=false; H4I_1.visible:=false;
   H1U_0.visible:=false; H2U_0.visible:=false; H3U_0.visible:=false; H4U_0.visible:=false;
   H1U_1.visible:=false; H2U_1.visible:=false; H3U_1.visible:=false; H4U_1.visible:=false;
   H1T_1.visible:=false; H2T_1.visible:=false; H3T_1.visible:=false; H4T_1.visible:=false;
   H1T_2.visible:=false; H2T_2.visible:=false; H3T_2.visible:=false; H4T_2.visible:=false;
   
   V1N_0.visible:=false; V1T_0.visible:=false; V2N_0.visible:=false; V2T_0.visible:=false; 
   V3N_0.visible:=false; V3T_0.visible:=false; V4N_0.visible:=false; V4T_0.visible:=false;
   V1R_0.visible:=false; V2R_0.visible:=false; V3R_0.visible:=false; V4R_0.visible:=false;
   V1R_1.visible:=false; V2R_1.visible:=false; V3R_1.visible:=false; V4R_1.visible:=false;
   V1I_0.visible:=false; V2I_0.visible:=false; V3I_0.visible:=false; V4I_0.visible:=false;
   V1I_1.visible:=false; V2I_1.visible:=false; V3I_1.visible:=false; V4I_1.visible:=false;
   V1U_0.visible:=false; V2U_0.visible:=false; V3U_0.visible:=false; V4U_0.visible:=false;
   V1U_1.visible:=false; V2U_1.visible:=false; V3U_1.visible:=false; V4U_1.visible:=false;
   V1T_1.visible:=false; V2T_1.visible:=false; V3T_1.visible:=false; V4T_1.visible:=false;
   V1T_2.visible:=false; V2T_2.visible:=false; V3T_2.visible:=false; V4T_2.visible:=false;
   
   V1T_3.visible:=false; V2T_3.visible:=false; V3T_3.visible:=false; V4T_3.visible:=false;
   H1T_3.visible:=false; H2T_3.visible:=false; H3T_3.visible:=false; H4T_3.visible:=false;


// odredi vidljive slike

   // horizontal
   case CCS[1] of 
      0 : H1N_0.visible:=true;
      1 : H1I_0.visible:=true;
      2 : H1U_0.visible:=true;
      3 : H1R_0.visible:=true;
   end;
   case CCS[2] of 
      0 : H2N_0.visible:=true;
      1 : H2I_0.visible:=true;
      2 : H2U_0.visible:=true;
      3 : H2R_0.visible:=true;
   end;
   case CCS[3] of 
      0 : H3N_0.visible:=true;
      1 : H3I_0.visible:=true;
      2 : H3U_0.visible:=true;
      3 : H3R_0.visible:=true;
   end;
   case CCS[4] of 
      0 : H4N_0.visible:=true;
      1 : H4I_0.visible:=true;
      2 : H4U_0.visible:=true;
      3 : H4R_0.visible:=true;
   end;
   
   case ECS[1] of 
      0 : H1T_0.visible:=true;
      1 : H1T_1.visible:=true;
   end;
   case ECS[2] of 
      0 : H2T_0.visible:=true;
      1 : H2T_1.visible:=true;
   end;
   case ECS[3] of 
      0 : H3T_0.visible:=true;
      1 : H3T_1.visible:=true;
   end;
   case ECS[4] of 
      0 : H4T_0.visible:=true;
      1 : H4T_1.visible:=true;
   end;
      
// odredi aktivnu sliku
   if (CCS[1]=1) and (cs=11) then  H1I_1.visible:=true;
   if (CCS[1]=2) and (cs=11) then  H1U_1.visible:=true;
   if (CCS[1]=3) and (cs=11) then  H1R_1.visible:=true;
   if (CCS[2]=1) and (cs=12) then  H2I_1.visible:=true;
   if (CCS[2]=2) and (cs=12) then  H2U_1.visible:=true;
   if (CCS[2]=3) and (cs=12) then  H2R_1.visible:=true;
   if (CCS[3]=1) and (cs=13) then  H3I_1.visible:=true;
   if (CCS[3]=2) and (cs=13) then  H3U_1.visible:=true;
   if (CCS[3]=3) and (cs=13) then  H3R_1.visible:=true;
   if (CCS[4]=1) and (cs=14) then  H4I_1.visible:=true;
   if (CCS[4]=2) and (cs=14) then  H4U_1.visible:=true;
   if (CCS[4]=3) and (cs=14) then  H4R_1.visible:=true;

   if (ECS[1]=1) and (cs=21) then  begin  H1T_2.visible:=true; H1T_3.visible:=true end;
   if (ECS[2]=1) and (cs=22) then  begin  H2T_2.visible:=true; H2T_3.visible:=true end;
   if (ECS[3]=1) and (cs=23) then  begin  H3T_2.visible:=true; H3T_3.visible:=true end;
   if (ECS[4]=1) and (cs=24) then  begin  H4T_2.visible:=true; H4T_3.visible:=true end;

   // frontal & vertikal
   case CCS[1] of 
      0 : V1N_0.visible:=true;
      1 : V1I_0.visible:=true;
      2 : V1U_0.visible:=true;
      3 : V1R_0.visible:=true;
   end;
   case CCS[2] of 
      0 : V2N_0.visible:=true;
      1 : V2I_0.visible:=true;
      2 : V2U_0.visible:=true;
      3 : V2R_0.visible:=true;
   end;
   case CCS[3] of 
      0 : V3N_0.visible:=true;
      1 : V3I_0.visible:=true;
      2 : V3U_0.visible:=true;
      3 : V3R_0.visible:=true;
   end;
   case CCS[4] of 
      0 : V4N_0.visible:=true;
      1 : V4I_0.visible:=true;
      2 : V4U_0.visible:=true;
      3 : V4R_0.visible:=true;
   end;
   
   case ECS[1] of 
      0 : V1T_0.visible:=true;
      1 : V1T_1.visible:=true;
   end;
   case ECS[2] of 
      0 : V2T_0.visible:=true;
      1 : V2T_1.visible:=true;
   end;
   case ECS[3] of 
      0 : V3T_0.visible:=true;
      1 : V3T_1.visible:=true;
   end;
   case ECS[4] of 
      0 : V4T_0.visible:=true;
      1 : V4T_1.visible:=true;
   end;
      
// odredi aktivnu sliku
   if (CCS[1]=1) and (cs=11) then  V1I_1.visible:=true;
   if (CCS[1]=2) and (cs=11) then  V1U_1.visible:=true;
   if (CCS[1]=3) and (cs=11) then  V1R_1.visible:=true;
   if (CCS[2]=1) and (cs=12) then  V2I_1.visible:=true;
   if (CCS[2]=2) and (cs=12) then  V2U_1.visible:=true;
   if (CCS[2]=3) and (cs=12) then  V2R_1.visible:=true;
   if (CCS[3]=1) and (cs=13) then  V3I_1.visible:=true;
   if (CCS[3]=2) and (cs=13) then  V3U_1.visible:=true;
   if (CCS[3]=3) and (cs=13) then  V3R_1.visible:=true;
   if (CCS[4]=1) and (cs=14) then  V4I_1.visible:=true;
   if (CCS[4]=2) and (cs=14) then  V4U_1.visible:=true;
   if (CCS[4]=3) and (cs=14) then  V4R_1.visible:=true;

   if (ECS[1]=1) and (cs=21) then  begin  V1T_2.visible:=true; V1T_3.visible:=true end;
   if (ECS[2]=1) and (cs=22) then  begin  V2T_2.visible:=true; V2T_3.visible:=true end;
   if (ECS[3]=1) and (cs=23) then  begin  V3T_2.visible:=true; V3T_3.visible:=true end;
   if (ECS[4]=1) and (cs=24) then  begin  V4T_2.visible:=true; V4T_3.visible:=true end;

   
   // ShowD('Odre�ena je aktivna slika');
   
   // poka�i parametre
   Pan_A.Visible:=false;Pan_B.Visible:=false;Pan_C.Visible:=false;Pan_D.Visible:=false;
   Pan_E.Visible:=false;Pan_F.Visible:=false;Pan_G.Visible:=false;Pan_R.Visible:=false;
   // Pan_N.Visible:=false;
   
   ShowD('Pokazujem i punim parametre');
   for i:=1 to 4 do begin
   // ShowD('i='+IntToStr(i));
      //

      
      // kosi rez                                        
      if (CCS[i]=1) and (cs=10+i)  then Begin Pan_A.Visible:=True; Pan_A.Top:=100;
                                              Pan_B.Visible:=True; Pan_B.Top:=200;
                                              Pan_N.Visible:=False; 
                                              Ed_A.Text:=floattostrC1(Var_A[i],8,2); 
                                              Ed_B.Text:=floattostrC1(Var_B[i],8,2);
                                              Sl_A.Position:=Round(Var_A[i]);
                                              Sl_B.Position:=Round(Var_B[i]);
                                              //Ed_A.Tag:=1;Ed_B.Tag:=1
                                              End;
      // L rez
      if (CCS[i]=2) and (cs=10+i)  then Begin Pan_A.Visible:=True; Pan_A.Top:=100;
                                              Pan_B.Visible:=True; Pan_B.Top:=200;
                                              Pan_N.Visible:=False; 
                                              Ed_A.Text:=floattostrC1(Var_A[i],8,2); 
                                              Ed_B.Text:=floattostrC1(Var_B[i],8,2);
                                              Sl_A.Position:=Round(Var_A[i]);
                                              Sl_B.Position:=Round(Var_B[i]);
                                              //Ed_A.Tag:=2;Ed_B.Tag:=2
                                              End;
      // radius
      if (CCS[i]=3) and (cs=10+i)  then Begin Pan_R.Visible:=True;
                                              Pan_N.Visible:=False; 
                                              Sl_R.Position:=Round(Var_R[i]);
                                              Ed_R.Text:=floattostrC1(Var_R[i],8,2)
                                              End;
      if (ECS[i]=1) and (cs=20+i)  then Begin Pan_C.Visible:=True; Pan_C.top:=100;
                                              Pan_D.Visible:=True; Pan_D.top:=200;
                                              Pan_F.Visible:=True; Pan_F.top:=300;
                                              Pan_E.Visible:=True; Pan_E.top:=400;
                                              Pan_G.Visible:=True; Pan_G.top:=500;
                                              Pan_N.Visible:=False; 
                                              Ed_C.Text:=floattostrC1(Var_C[i],8,2); 
                                              Ed_D.Text:=floattostrC1(Var_D[i],8,2);
                                              Ed_E.Text:=floattostrC1(Var_E[i],8,2); 
                                              Ed_F.Text:=floattostrC1(Var_F[i],8,2);
                                              Ed_G.Text:=floattostrC1(Var_G[i],8,2)
                                              Sl_C.Position:=Round(Var_C[i]);
                                              Sl_D.Position:=Round(Var_D[i]);
                                              Sl_E.Position:=Round(Var_E[i]);
                                              Sl_F.Position:=Round(Var_F[i]);
                                              Sl_G.Position:=Round(Var_G[i]);
                                              If ceo[i]=0 then begin
                                                                  rb_C.checked:=true;
                                                                  rb_G.checked:=false;
                                                               end else begin
                                                                  rb_C.checked:=false;
                                                                  rb_G.checked:=true;
                                                               end;;
                                              
                                              End;
   end;
   ispis_varijabli;
end;


Procedure Sense_Click(Sender: TObject; Button: TMouseButton;  Shift: TShiftState; X, Y: Integer);
var
   tt,i,ofs,x1,x2,y1,y2:Integer;
begin
// CCS, ECS, Cs, Co
    
    if cs=Co+1 then  begin
      // drugi klik na isto mjesto - slijedi promjena reza
      ShowD('Sense_click - Drugi Klik, CS='+IntToStr(cs));
      Pan_N.Visible:=False;
      case Cs of 
         11 : begin  CCS[1]:=CCS[1]+1; if CCS[1]>3 then CCS[1]:=0 end;	// corner cut status
         12 : begin  CCS[2]:=CCS[2]+1; if CCS[2]>3 then CCS[2]:=0 end;
         13 : begin  CCS[3]:=CCS[3]+1; if CCS[3]>3 then CCS[3]:=0 end;
         14 : begin  CCS[4]:=CCS[4]+1; if CCS[4]>3 then CCS[4]:=0 end;
         21 : begin  ECS[1]:=ECS[1]+1; if ECS[1]>1 then ECS[1]:=0 end;	// edge cut status
         22 : begin  ECS[2]:=ECS[2]+1; if ECS[2]>1 then ECS[2]:=0 end;
         23 : begin  ECS[3]:=ECS[3]+1; if ECS[3]>1 then ECS[3]:=0 end;
         24 : begin  ECS[4]:=ECS[4]+1; if ECS[4]>1 then ECS[4]:=0 end //;
		//	30 : begin  SCS	:=ECS+1; 	if SCS   >1 then SCS	  :=0 end;	// sredina cut status  0=NI�TA, 1=PRAVOKUTNIK, 2=KRUG
      end  // case
      // Postavi tagove
     
      
      
    end else begin 
         // prvi klik na neko mjesto - ne desi se ni�ta
         ShowD('Sense_click - Prvi Klik, CS='+IntToStr(cs));
         cs:=Co+1;
         Pan_N.Visible:=True;
   end; // if cs=Co+1
   ShowD('cs '+IntToStr(CS));
   
	// tt=?
	// tt je tag kojim se odre�uje na koji �o�ak se primjenjuje kontrola
	
    If (cs>=10) and (cs<20) then tt:=CS mod 10;
    If (cs>=20) and (cs<30) then tt:=CS mod 20;
	 // If (cs>=30) and (cs<40) then tt:=CS mod 30;   NEMA SMISLA ZA CRTANJE U SREDINI JER IMA SAMO JEDAN OTVOR
      Sl_A.Tag:=tt; // Sl_A.Hint:=IntToStr(tt);
      Sl_B.Tag:=tt; // Sl_B.Hint:=IntToStr(tt);
      Sl_C.Tag:=tt; // Sl_C.Hint:=IntToStr(tt);
      Sl_D.Tag:=tt; // Sl_D.Hint:=IntToStr(tt);
      Sl_E.Tag:=tt; // Sl_E.Hint:=IntToStr(tt);
      Sl_F.Tag:=tt; // Sl_F.Hint:=IntToStr(tt);
      Sl_G.Tag:=tt; // Sl_G.Hint:=IntToStr(tt);
      Sl_R.Tag:=tt; // Sl_R.Hint:=IntToStr(tt);
      Ed_A.Tag:=tt; // Ed_A.Hint:=IntToStr(tt);
      Ed_B.Tag:=tt; // Ed_B.Hint:=IntToStr(tt);
      Ed_C.Tag:=tt; // Ed_C.Hint:=IntToStr(tt);
      Ed_D.Tag:=tt; // Ed_D.Hint:=IntToStr(tt);
      Ed_E.Tag:=tt; // Ed_E.Hint:=IntToStr(tt);
      Ed_F.Tag:=tt; // Ed_F.Hint:=IntToStr(tt);
      Ed_G.Tag:=tt; // Ed_G.Hint:=IntToStr(tt);
      Ed_R.Tag:=tt; // Ed_R.Hint:=IntToStr(tt);
      rb_C.Tag:=tt; // rb_C.Hint:=IntToStr(tt);
      rb_G.Tag:=tt; // rb_G.Hint:=IntToStr(tt);
      
      
      ShowD('Sense_Click je dodijelio tag za �o�ak: '+IntToStr(tt));
   
   ispis_varijabli;
   semafor;
   

end;
procedure read_properties;
var
   i:integer;
begin
   razno:='*';   // oznaka za joker kod razli�itih vrijednosti
   SelPlates:=el_find_sel_plates;
   
   ShowN(SelPlates.text);

   // ispi�i selektirane daske u info
   SelTxt:=' (';
   LabSelQnt.Caption:=IntToStr(SelPlates.Count);
   for i:=0 to SelPlates.Count-1 do begin
      SelTxt:=SelTxt+SelPlates.Strings[i];
      if i<(SelPlates.Count-1) then SelTxt:=SelTxt+', ';   // dodaj zarez i razmak ako nije zadnji
   end;
   SelTxt:=SelTxt+')'
   LabSelTxt.Caption:=SelTxt;  // ispis selektiranih dasaka na info od forme

// Upis vrijednosti

for i:=0 to SelPlates.Count-1 do begin
   // td:=TDaska(SelPlates.Objects[i]);
   
   // X vrijednost
   ux:=TDaska(SelPlates.Objects[i]).xpos;
   if i=0 then sux:=ux;                      // prva vrijednost od X
   ShowD(SelPlates.Strings[i]+'.x='+floattostrC1(ux,8,2));
   if (ux=sux) and (Pan_Val[0].Text<>razno) then Pan_Val[0].Text:=floattostrC1(ux,8,2)   // ako su jednaki
                                          else Pan_Val[0].Text:=razno;             // ako je razli�ito
   
   // Y vrijednost
   uy:=TDaska(SelPlates.Objects[i]).ypos;
   if i=0 then suy:=uy;                      // prva vrijednost od Y
   ShowD(SelPlates.Strings[i]+'.y='+floattostrC1(uy,8,2));
   if (uy=suy) and (Pan_Val[1].Text<>razno) then Pan_Val[1].Text:=floattostrC1(uy,8,2)   // ako su jednaki
                                          else Pan_Val[1].Text:=razno;             // ako je razli�ito
 
   // Z vrijednost
   uz:=TDaska(SelPlates.Objects[i]).zpos;
   if i=0 then suz:=uz;                      // prva vrijednost od z
   ShowD(SelPlates.Strings[i]+'.z='+floattostrC1(uz,8,2));
   if (uz=suz) and (Pan_Val[2].Text<>razno) then Pan_Val[2].Text:=floattostrC1(uz,8,2)   // ako su jednaki
                                          else Pan_Val[2].Text:=razno;             // ako je razli�ito 

   // visina
   uh:=TDaska(SelPlates.Objects[i]).Visina;
   if i=0 then suh:=uh;                      // prva vrijednost od visine
   ShowD(SelPlates.Strings[i]+'.vis='+floattostrC1(uh,8,2));
   if (uh=suh) and (Pan_Val[3].Text<>razno) then Pan_Val[3].Text:=floattostrC1(uh,8,2)   // ako su jednaki
                                          else Pan_Val[3].Text:=razno;             // ako je razli�ito

   // �irina
   uw:=TDaska(SelPlates.Objects[i]).Dubina;
   if i=0 then suw:=uw;                      // prva vrijednost od sirine
   ShowD(SelPlates.Strings[i]+'.sir='+floattostrC1(uh,8,2));
   if (uw=suw) and (Pan_Val[4].Text<>razno) then Pan_Val[4].Text:=floattostrC1(uw,8,2)   // ako su jednaki
                                          else Pan_Val[4].Text:=razno;             // ako je razli�ito
   
   // debljina
   ut:=TDaska(SelPlates.Objects[i]).Debljina;
   if i=0 then sut:=ut;                      // prva vrijednost od debljine
   ShowD(SelPlates.Strings[i]+'.deb='+floattostrC1(ut,8,2));
   if (ut=sut) and (Pan_Val[5].Text<>razno) then Pan_Val[5].Text:=floattostrC1(ut,8,2)   // ako su jednaki
                                          else Pan_Val[5].Text:=razno;             // ako je razli�ito
                                          
   // tip
   utip:=Integer(TDaska(SelPlates.Objects[i]).TipDaske);
   if i=0 then sutip:=utip;                      // prva vrijednost od tipa
   ShowD(SelPlates.Strings[i]+'.tip='+floattostrC1(ut,8,2));
   if (utip=sutip) and (Pan_Val[6].Text<>razno) then Pan_Val[6].Text:=floattostrC1(utip,8,0) // ako su jednaki
                                          else Pan_Val[6].Text:=razno;             // ako je razli�ito

   // X formula
   uxf:=LowerCase(TDaska(SelPlates.Objects[i]).xFormula);
   if i=0 then suxf:=uxf;                      // prva vrijednost od X Formule
   ShowD(SelPlates.Strings[i]+'.f(x)='+uxf);
   if (uxf=suxf) and (Pan_for[0].Text<>razno) then Pan_for[0].Text:=uxf   // ako su jednaki
                                          else Pan_For[0].Text:=razno;             // ako je razli�ito
                                          
   // Y formula
   uyf:=LowerCase(TDaska(SelPlates.Objects[i]).yFormula);
   if i=0 then suyf:=uyf;                      // prva vrijednost od Y Formule
   ShowD(SelPlates.Strings[i]+'.f(y)='+uyf);
   if (uyf=suyf) and (Pan_for[1].Text<>razno) then Pan_for[1].Text:=uyf   // ako su jednaki
                                          else Pan_For[1].Text:=razno;             // ako je razli�ito

   // Z formula
   uzf:=LowerCase(TDaska(SelPlates.Objects[i]).zFormula);
   if i=0 then suzf:=uzf;                      // prva vrijednost od Z Formule
   ShowD(SelPlates.Strings[i]+'.f(z)='+uzf);
   if (uzf=suzf) and (Pan_for[2].Text<>razno) then Pan_for[2].Text:=uzf   // ako su jednaki
                                          else Pan_For[2].Text:=razno;             // ako je razli�ito
    
   // Visina formula
   uhf:=LowerCase(TDaska(SelPlates.Objects[i]).hFormula);
   if i=0 then suhf:=uhf;                      // prva vrijednost od Formule Visine
   ShowD(SelPlates.Strings[i]+'.f(vis)='+uhf);
   if (uhf=suhf) and (Pan_for[3].Text<>razno) then Pan_for[3].Text:=uhf   // ako su jednaki
                                          else Pan_For[3].Text:=razno;             // ako je razli�ito 

   // �irina formula
   uwf:=LowerCase(TDaska(SelPlates.Objects[i]).sFormula);
   if i=0 then suwf:=uwf;                      // prva vrijednost od Formule �irina
   ShowD(SelPlates.Strings[i]+'.f(�ir)='+uwf);
   if (uwf=suwf) and (Pan_for[4].Text<>razno) then Pan_for[4].Text:=uwf   // ako su jednaki
                                          else Pan_For[4].Text:=razno;    // ako je razli�ito 
                                          
   // Debljina formula
   utf:=LowerCase(TDaska(SelPlates.Objects[i]).dFormula);
   if i=0 then sutf:=utf;                      // prva vrijednost od Formule Debljina
   ShowD(SelPlates.Strings[i]+'.f(deb)='+utf);
   if (utf=sutf) and (Pan_for[5].Text<>razno) then Pan_for[5].Text:=utf   // ako su jednaki
                                          else Pan_For[5].Text:=razno;    // ako je razli�ito 
                                          
   // Tip Combo Box  
   if Pan_Val[6].Text=razno then CoB_tip.ItemIndex:=-1
                           else CoB_tip.ItemIndex:=StrToInt(Pan_Val[6].Text);
   
	// Primjedba
   prim:=TDaska(SelPlates.Objects[i]).primjedba;
	if i=0 then sprim:=prim;
	Pan_For[7].Text:=TDaska(SelPlates.Objects[i]).primjedba;
	if (prim=sprim) and (Pan_for[7].Text<>razno) then Pan_for[7].Text:=prim   		// ako su jednaki
																else Pan_For[7].Text:=razno;     // ako je razli�ito 
   
	 // X Kut vrijednost
   uxk:=TDaska(SelPlates.Objects[i]).xkut;
   if i=0 then suxk:=uxk;                      // prva vrijednost od Xkuta
   ShowD(SelPlates.Strings[i]+'.xkut='+floattostrC1(uxk,8,2));
   if (uxk=suxk) and (Pan_Val[8].Text<>razno) then Pan_Val[8].Text:=floattostrC1(uxk,8,2)   // ako su jednaki
                                          else Pan_Val[8].Text:=razno;             // ako je razli�ito
	
	// Y Kut vrijednost
   uyk:=TDaska(SelPlates.Objects[i]).kut;
   if i=0 then suyk:=uyk;                      // prva vrijednost od Xkuta
   ShowD(SelPlates.Strings[i]+'.ykut='+floattostrC1(uyk,8,2));
   if (uyk=suyk) and (Pan_Val[9].Text<>razno) then Pan_Val[9].Text:=floattostrC1(uyk,8,2)   // ako su jednaki
                                              else Pan_Val[9].Text:=razno;            // ako je razli�ito
	
	// Z Kut vrijednost
   uzk:=TDaska(SelPlates.Objects[i]).zkut;
   if i=0 then suzk:=uzk;                      // prva vrijednost od Z kuta
   ShowD(SelPlates.Strings[i]+'.zkut='+floattostrC1(uzk,8,2));
   if (uzk=suzk) and (Pan_Val[9].Text<>razno) then Pan_Val[10].Text:=floattostrC1(uzk,8,2)   // ako su jednaki
                                              else Pan_Val[10].Text:=razno;            // ako je razli�ito
	
	 // Xkut formula
   uxkf:=LowerCase(TDaska(SelPlates.Objects[i]).kxFormula);
   if i=0 then suxkf:=uxkf;                      // prva vrijednost od X Formule
   ShowD(SelPlates.Strings[i]+'.f(x)='+uxf);
   if (uxkf=suxkf) and (Pan_for[8].Text<>razno) then Pan_for[8].Text:=uxkf   // ako su jednaki
                                          else Pan_For[8].Text:=razno;             // ako je razli�ito
	
	// Ykut formula
   uykf:=LowerCase(TDaska(SelPlates.Objects[i]).kyFormula);
   if i=0 then suykf:=uykf;                      // prva vrijednost od y Formule
   ShowD(SelPlates.Strings[i]+'.f(x)='+uyf);
   if (uykf=suykf) and (Pan_for[9].Text<>razno) then Pan_for[9].Text:=uykf   // ako su jednaki
                                          else Pan_For[9].Text:=razno;             // ako je razli�ito
	
	// Zkut formula
   uzkf:=LowerCase(TDaska(SelPlates.Objects[i]).kzFormula);
   if i=0 then suzkf:=uzkf;                      // prva vrijednost od y Formule
   ShowD(SelPlates.Strings[i]+'.f(x)='+uzf);
   if (uzkf=suzkf) and (Pan_for[10].Text<>razno) then Pan_for[10].Text:=uzkf   // ako su jednaki
                                          else Pan_For[10].Text:=razno;             // ako je razli�ito
	
	
   // hintovi


   Pan_Val[0].Hint:=Pan_Val[0].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.x= '+floattostrC1(ux,8,2)+#13#10;
   Pan_Val[1].Hint:=Pan_Val[1].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.y= '+floattostrC1(uy,8,2)+#13#10; 
   Pan_Val[2].Hint:=Pan_Val[2].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.z= '+floattostrC1(uz,8,2)+#13#10; 
   Pan_Val[3].Hint:=Pan_Val[3].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.Visina= '+floattostrC1(uh,8,2)+#13#10; 
   Pan_Val[4].Hint:=Pan_Val[4].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.�irina= '+floattostrC1(uw,8,2)+#13#10; 
   Pan_Val[5].Hint:=Pan_Val[5].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.Debljina= '+floattostrC1(ut,8,2)+#13#10; 
   Pan_Val[6].Hint:=Pan_Val[6].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.tip='+IntToStr(utip)+#13#10; 
   
   Pan_For[0].Hint:=Pan_For[0].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.f(x)= '+(uxF)+#13#10;
   Pan_For[1].Hint:=Pan_For[1].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.f(y)= '+(uyF)+#13#10; 
   Pan_For[2].Hint:=Pan_For[2].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.f(z)= '+(uzF)+#13#10; 
   Pan_For[3].Hint:=Pan_For[3].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.f(Visina)= '+(uhF)+#13#10; 
   Pan_For[4].Hint:=Pan_For[4].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.f(�irina)= '+(uwF)+#13#10; 
   Pan_For[5].Hint:=Pan_For[5].Hint+TDaska(SelPlates.Objects[i]).Naziv+'.f(Debljina)= '+(utF)+#13#10; 
   ShowD(SelPlates.Strings[i]+'.tip='+IntToStr(utip)+' / '+CoB_tip.Items[utip]);
   // {
   if utip<19 then  CoB_tip.Hint:=CoB_tip.Hint
                   +TDaska(SelPlates.Objects[i]).Naziv
                   +'.tip= ' +CoB_tip.Items[utip]+#13#10
              else CoB_tip.Hint:=CoB_tip.Hint
                   +TDaska(SelPlates.Objects[i]).Naziv
                   +'.tip= korisni�ki tip broj '+IntToStr(utip)+#13#10;     
   // }
   
 end;   // for i:=0 to SelPlates.Count-1 

 
 
 // enable/disable raznih polja
 check_enable;

end;

procedure read_cuts;
// �itanje postoje�ih rezova i priprema prozora
var
   ttd:TDaska;
   i, bk:Integer;
   stari_smijer, novi_smijer:string;
Begin
   ShowD('procedure read_cuts');
   // provjera da li su svi selektirani objekti daske
   // ako nisu to treba napisati u prozor od rezova i onemogu�iti promjenu
   
   // provjera da li su sve selektiranje daske iste orjentacije
   // ako nisu to treba napisati u prozor od rezova i onemogu�iti promjenu

   // �itanje krivulja
   stari_smijer:='nepoznat';
   novi_smijer:='nepoznat';
   for i:=0 to SelPlates.Count-1 do Begin
         if SelPlates.Objects[i] is Tdaska then begin 
            ttd:=TDaska(SelPlates.Objects[i]);
            // read orientation
            // VertBok, VertFront, Horiz, Gore 
            
            if ttd.smjer=VertBok then novi_smijer:='Vertikalno';
            if ttd.smjer=VertFront then novi_smijer:='Frontalno';
            if ttd.smjer=Horiz then novi_smijer:='Horizontalno';
            if (i>0) and (stari_smijer<>novi_smijer) then Lab_Vsm.caption:='Razno'
                                                     else begin
                                                         Lab_Vsm.caption:=novi_smijer; 
                                                         stari_smijer:=novi_smijer;
            end;
            // Lab_Vsm.caption
            // ShowD('d('+IntToStr(i)+')= "'+ttd.naziv+'"');
            bk:=bk+1;
            SetArrayLength(curve,bk); 
            curve[bk-1]:=ttd.TockeiRupe.Rubne;      // vanjska (osnovna) krivulja 
         end;
   end;
   
   // Draw Shapes
   if Lab_Vsm.caption='Horizontalno' then begin
               Pan_PicH.Visible:=True;
               Pan_PicV.Visible:=False;
               Sh_C[1]:=rcBox('',Pan_PicH,20,20,68,68);
               Sh_C[2]:=rcBox('',Pan_PicH,211,20,68,68);
               Sh_C[0]:=rcBox('',Pan_PicH,20,211,68,68);
               Sh_C[3]:=rcBox('',Pan_PicH,211,211,68,68);
               Sh_T[1]:=rcBox('',Pan_PicH,88,20,123,68);
               Sh_T[0]:=rcBox('',Pan_PicH,20,88,68,123);
               Sh_T[2]:=rcBox('',Pan_PicH,211,88,68,123);
               Sh_T[3]:=rcBox('',Pan_PicH,88,211,123,68);
               Shape_Sense:=rcBox('',Pan_PicH,0,0,300,300);
					// za rupu u sredini
					Sh_S:=rcBox('',Pan_PicH,90,90,118,118);
					
         end else begin
               Pan_PicV.Visible:=True;
               Pan_PicH.Visible:=False;
               Sh_C[1]:=rcBox('',Pan_PicV,20,20,68,68);
               Sh_C[2]:=rcBox('',Pan_PicV,211,20,68,68);
               Sh_C[0]:=rcBox('',Pan_PicV,20,211,68,68);
               Sh_C[3]:=rcBox('',Pan_PicV,211,211,68,68);
               Sh_T[1]:=rcBox('',Pan_PicV,88,20,123,68);
               Sh_T[0]:=rcBox('',Pan_PicV,20,88,68,123);
               Sh_T[2]:=rcBox('',Pan_PicV,211,88,68,123);
               Sh_T[3]:=rcBox('',Pan_PicV,88,211,123,68);
               Shape_Sense:=rcBox('',Pan_PicV,0,0,300,300);
					// za rupu u sredini
					Sh_S:=rcBox('',Pan_PicH,90,90,118,118);
					
               If Lab_Vsm.caption='Vertikalno' then VCent.visible:=True
                                               else FCent.visible:=True;
         
   end;
  
   
   
   for i:=0 to 3 do begin
      Sh_C[i].Brush.Style:=bsClear;
      Sh_C[i].Pen.Style:=psSolid;
      Sh_C[i].Pen.Width:=1;
      Sh_C[i].Pen.Color:=clRed;
      Sh_C[i].Visible:=False;
   end;
   
   for i:=0 to 3 do begin
      Sh_T[i].Brush.Style:=bsClear;
      Sh_T[i].Pen.Style:=psSolid;
      Sh_T[i].Pen.Width:=1;
      Sh_T[i].Pen.Color:=clRed;
      Sh_T[i].Visible:=False;
   end;
    
	Sh_S.Brush.Style:=bsClear;
   Sh_S.Pen.Style:=psSolid;
   Sh_S.Pen.Width:=1;
   Sh_S.Pen.Color:=clRed;
   Sh_S.Visible:=False;	
   
   Shape_Sense.Brush.Style:=bsClear;
   Shape_Sense.Pen.Style:=psSolid;
   Shape_Sense.Pen.Width:=1;
   Shape_Sense.Pen.Color:=clGreen;
   
   // �itanje varijabli, ako postoje (iz prve daske)
   ttd:=TDaska(SelPlates.Objects[0]);
   AllCurve:=ttd.TockeiRupe;
   if rcCurVarEx(AllCurve,'cc1') then CCS[1]:=StrToInt(rcCurVarVal(AllCurve,'cc1')) else  CCS[1]:=0;
   if rcCurVarEx(AllCurve,'cc2') then CCS[2]:=StrToInt(rcCurVarVal(AllCurve,'cc2')) else  CCS[2]:=0;
   if rcCurVarEx(AllCurve,'cc3') then CCS[3]:=StrToInt(rcCurVarVal(AllCurve,'cc3')) else  CCS[3]:=0;
   if rcCurVarEx(AllCurve,'cc4') then CCS[4]:=StrToInt(rcCurVarVal(AllCurve,'cc4')) else  CCS[4]:=0;
   
   if rcCurVarEx(AllCurve,'ec1') then ECS[1]:=StrToInt(rcCurVarVal(AllCurve,'ec1')) else  ECS[1]:=0;
   if rcCurVarEx(AllCurve,'ec2') then ECS[2]:=StrToInt(rcCurVarVal(AllCurve,'ec2')) else  ECS[2]:=0;
   if rcCurVarEx(AllCurve,'ec3') then ECS[3]:=StrToInt(rcCurVarVal(AllCurve,'ec3')) else  ECS[3]:=0;
   if rcCurVarEx(AllCurve,'ec4') then ECS[4]:=StrToInt(rcCurVarVal(AllCurve,'ec4')) else  ECS[4]:=0;
   
   if rcCurVarEx(AllCurve,'a1') then Var_A[1]:=StrToFloat(rcCurVarVal(AllCurve,'a1')) else  Var_A[1]:=0;
   if rcCurVarEx(AllCurve,'a2') then Var_A[2]:=StrToFloat(rcCurVarVal(AllCurve,'a2')) else  Var_A[2]:=0;
   if rcCurVarEx(AllCurve,'a3') then Var_A[3]:=StrToFloat(rcCurVarVal(AllCurve,'a3')) else  Var_A[3]:=0;
   if rcCurVarEx(AllCurve,'a4') then Var_A[4]:=StrToFloat(rcCurVarVal(AllCurve,'a4')) else  Var_A[4]:=0;
   
   if rcCurVarEx(AllCurve,'b1') then Var_B[1]:=StrToFloat(rcCurVarVal(AllCurve,'b1')) else  Var_B[1]:=0;
   if rcCurVarEx(AllCurve,'b2') then Var_B[2]:=StrToFloat(rcCurVarVal(AllCurve,'b2')) else  Var_B[2]:=0;
   if rcCurVarEx(AllCurve,'b3') then Var_B[3]:=StrToFloat(rcCurVarVal(AllCurve,'b3')) else  Var_B[3]:=0;
   if rcCurVarEx(AllCurve,'b4') then Var_B[4]:=StrToFloat(rcCurVarVal(AllCurve,'b4')) else  Var_B[4]:=0;
   
   if rcCurVarEx(AllCurve,'c1') then Var_C[1]:=StrToFloat(rcCurVarVal(AllCurve,'c1')) else  Var_C[1]:=0;
   if rcCurVarEx(AllCurve,'c2') then Var_C[2]:=StrToFloat(rcCurVarVal(AllCurve,'c2')) else  Var_C[2]:=0;
   if rcCurVarEx(AllCurve,'c3') then Var_C[3]:=StrToFloat(rcCurVarVal(AllCurve,'c3')) else  Var_C[3]:=0;
   if rcCurVarEx(AllCurve,'c4') then Var_C[4]:=StrToFloat(rcCurVarVal(AllCurve,'c4')) else  Var_C[4]:=0;
   
   if rcCurVarEx(AllCurve,'d1') then Var_D[1]:=StrToFloat(rcCurVarVal(AllCurve,'d1')) else  Var_D[1]:=0;
   if rcCurVarEx(AllCurve,'d2') then Var_D[2]:=StrToFloat(rcCurVarVal(AllCurve,'d2')) else  Var_D[2]:=0;
   if rcCurVarEx(AllCurve,'d3') then Var_D[3]:=StrToFloat(rcCurVarVal(AllCurve,'d3')) else  Var_D[3]:=0;
   if rcCurVarEx(AllCurve,'d4') then Var_D[4]:=StrToFloat(rcCurVarVal(AllCurve,'d4')) else  Var_D[4]:=0;
   
   if rcCurVarEx(AllCurve,'e1') then Var_E[1]:=StrToFloat(rcCurVarVal(AllCurve,'e1')) else  Var_E[1]:=0;
   if rcCurVarEx(AllCurve,'e2') then Var_E[2]:=StrToFloat(rcCurVarVal(AllCurve,'e2')) else  Var_E[2]:=0;
   if rcCurVarEx(AllCurve,'e3') then Var_E[3]:=StrToFloat(rcCurVarVal(AllCurve,'e3')) else  Var_E[3]:=0;
   if rcCurVarEx(AllCurve,'e4') then Var_E[4]:=StrToFloat(rcCurVarVal(AllCurve,'e4')) else  Var_E[4]:=0;
   
   if rcCurVarEx(AllCurve,'f1') then Var_F[1]:=StrToFloat(rcCurVarVal(AllCurve,'f1')) else  Var_F[1]:=0;
   if rcCurVarEx(AllCurve,'f2') then Var_F[2]:=StrToFloat(rcCurVarVal(AllCurve,'f2')) else  Var_F[2]:=0;
   if rcCurVarEx(AllCurve,'f3') then Var_F[3]:=StrToFloat(rcCurVarVal(AllCurve,'f3')) else  Var_F[3]:=0;
   if rcCurVarEx(AllCurve,'f4') then Var_F[4]:=StrToFloat(rcCurVarVal(AllCurve,'f4')) else  Var_F[4]:=0;
   
   if rcCurVarEx(AllCurve,'g1') then Var_G[1]:=StrToFloat(rcCurVarVal(AllCurve,'g1')) else  Var_G[1]:=0;
   if rcCurVarEx(AllCurve,'g2') then Var_G[2]:=StrToFloat(rcCurVarVal(AllCurve,'g2')) else  Var_G[2]:=0;
   if rcCurVarEx(AllCurve,'g3') then Var_G[3]:=StrToFloat(rcCurVarVal(AllCurve,'g3')) else  Var_G[3]:=0;
   if rcCurVarEx(AllCurve,'g4') then Var_G[4]:=StrToFloat(rcCurVarVal(AllCurve,'g4')) else  Var_G[4]:=0;
   
   if rcCurVarEx(AllCurve,'r1') then Var_R[1]:=StrToFloat(rcCurVarVal(AllCurve,'r1')) else  Var_R[1]:=0;
   if rcCurVarEx(AllCurve,'r2') then Var_R[2]:=StrToFloat(rcCurVarVal(AllCurve,'r2')) else  Var_R[2]:=0;
   if rcCurVarEx(AllCurve,'r3') then Var_R[3]:=StrToFloat(rcCurVarVal(AllCurve,'r3')) else  Var_R[3]:=0;
   if rcCurVarEx(AllCurve,'r4') then Var_R[4]:=StrToFloat(rcCurVarVal(AllCurve,'r4')) else  Var_R[4]:=0;
   
   if rcCurVarEx(AllCurve,'ceo1') then Ceo[1]:=StrToInt(rcCurVarVal(AllCurve,'ceo1')) else  Ceo[1]:=0;
   if rcCurVarEx(AllCurve,'ceo2') then Ceo[2]:=StrToInt(rcCurVarVal(AllCurve,'ceo2')) else  Ceo[2]:=0;
   if rcCurVarEx(AllCurve,'ceo3') then Ceo[3]:=StrToInt(rcCurVarVal(AllCurve,'ceo3')) else  Ceo[3]:=0;
   if rcCurVarEx(AllCurve,'ceo4') then Ceo[4]:=StrToInt(rcCurVarVal(AllCurve,'ceo4')) else  Ceo[4]:=0;
      
   semafor;
   ispis_varijabli;
end;
Procedure read_edges;
// �itanje postoje�ih rubnih traka

{
teorija o rubnim trakama
==========================

Rubne trake u startu ne postoje. To zna�i da nemaju svoj du�ni potro�ni. kad se prvi put trake postave dobiju svoj du�ni potro�ni.
Kasnije se taj du�ni potro�ni vi�e nikad ne mo�e maknuti. Zato je ptrebno provjeravati da li je na tom rubu duzni potro�ni aktivan ili nije.
Ukratko, prvo se mora provjeriti da li je duzni = NIL, ako je zna�i da nema rubne trake. Ako nije nil, treba provjeriti da li je aktivan
Ako je onda traka postoji.

Da li duzni postoji provjerava se sa: 

traka:TDuzni;

traka:=neka_daska.potrosni.gettraka(index_ruba). 
Ako je traka =  NIL, onda nema trake. 
Ako je traka <> NIL, onda se jo� testira:  traka.GetOnOf(Index_ruba). Ako je i to false onda definitivno nema trake!



 }


var
   pot:TPotrosni;
   pot_duz:TDuzni;
   i,j,first_plate_edge1,first_plate_edge2,first_plate_edge3,first_plate_edge4:integer;
   edge_name:string;
   edge_index:integer;
   ttd:TDaska;

Begin 
      img_PM.visible:=false;img_PF.visible:=false;img_PV.visible:=false;img_PH.visible:=false;
      if Lab_Vsm.caption='Vertikalno' then Img_PV.visible:=true;
      if Lab_Vsm.caption='Horizontalno' then Img_PH.visible:=true;
      if Lab_Vsm.caption='Frontalno' then Img_PF.visible:=true;
      if Lab_Vsm.caption='Razno' then img_PM.visible:=true;
      for i:=0 to SelPlates.Count-1 do Begin
         if SelPlates.Objects[i] is Tdaska then begin 
            ttd:=TDaska(SelPlates.Objects[i]);
            for j:= 0 to 3 do begin  // rubovi daske 
               pot:=ttd.potrosni;
               pot_duz:=pot.gettraka(j);
               
               if (pot_duz<>nil) then begin  // ima trake
                                    if pot_duz.GetonOf(j)=true then begin  // traka je aktivna
                                       edge_name:=pot_duz.naziv;
                                       edge_index:=pot_duz.index;
                                       showD('traka['+IntToStr(j)+']='+edge_name+',i:'+IntToStr(edge_index));
                                       if (ttd.smjer=VertBok) or (ttd.smjer=VertFront) then begin //(2=lijevo, 1=gore, 3=desno, 0=dolje);
                                          if j=0 then CMB_E4.itemIndex:=edge_index+2;   
                                          if j=1 then CMB_E1.itemIndex:=edge_index+2;   
                                          if j=2 then CMB_E2.itemIndex:=edge_index+2;   
                                          if j=3 then CMB_E3.itemIndex:=edge_index+2;   
                                         end else begin
                                          if j=0 then CMB_E2.itemIndex:=edge_index+2;   
                                          if j=1 then CMB_E3.itemIndex:=edge_index+2;   
                                          if j=2 then CMB_E4.itemIndex:=edge_index+2;   
                                          if j=3 then CMB_E1.itemIndex:=edge_index+2;  
                                       end; //  if (ttd.smjer=VertBok) or (ttd.smjer=VertFront)
                                    end else Begin    // ima trake ali je neaktivna
                                       showD('traka['+IntToStr(j)+']='+'postoji, ali je neaktivna!');
                                       if (ttd.smjer=VertBok) or (ttd.smjer=VertFront) then begin
                                          if j=0 then CMB_E4.itemIndex:=0;   
                                          if j=1 then CMB_E1.itemIndex:=0;   
                                          if j=2 then CMB_E2.itemIndex:=0;   
                                          if j=3 then CMB_E3.itemIndex:=0;   
                                         end else begin
                                          if j=0 then CMB_E2.itemIndex:=0;   
                                          if j=1 then CMB_E3.itemIndex:=0;   
                                          if j=2 then CMB_E4.itemIndex:=0;   
                                          if j=3 then CMB_E1.itemIndex:=0;  
                                       end; //  if (ttd.smjer=VertBok) or (ttd.smjer=VertFront)
                                    end;
                      end else begin  // nema trake
                                    showD('traka['+IntToStr(j)+']='+'ne postoji');
                                    if (ttd.smjer=VertBok) or (ttd.smjer=VertFront) then begin
                                          if j=0 then CMB_E4.itemIndex:=0;   
                                          if j=1 then CMB_E1.itemIndex:=0;   
                                          if j=2 then CMB_E2.itemIndex:=0;   
                                          if j=3 then CMB_E3.itemIndex:=0;   
                                       end else begin
                                          if j=0 then CMB_E2.itemIndex:=0;   
                                          if j=1 then CMB_E3.itemIndex:=0;   
                                          if j=2 then CMB_E4.itemIndex:=0;   
                                          if j=3 then CMB_E1.itemIndex:=0;  
                                    end; //  if (ttd.smjer=VertBok) or (ttd.smjer=VertFront)
                                    
               end;  // if (pot_duz<>nil)
               if i=0 then begin   // prva daska
                  ShowD('prva daska');
                  first_plate_edge1:=CMB_E1.itemIndex;
                  first_plate_edge2:=CMB_E2.itemIndex;
                  first_plate_edge3:=CMB_E3.itemIndex;
                  first_plate_edge4:=CMB_E4.itemIndex;
               end else begin
                  ShowD('druga daska');
                  if CMB_E1.itemIndex<>first_plate_edge1 then begin first_plate_edge1:=1; CMB_E1.itemIndex:=1 end;
                  if CMB_E2.itemIndex<>first_plate_edge2 then begin first_plate_edge2:=1; CMB_E2.itemIndex:=1 end;
                  if CMB_E3.itemIndex<>first_plate_edge3 then begin first_plate_edge3:=1; CMB_E3.itemIndex:=1 end;
                  if CMB_E4.itemIndex<>first_plate_edge4 then begin first_plate_edge4:=1; CMB_E4.itemIndex:=1 end;
               end;

            end; // for j
         end;  // if SelPlates.Objects[i] is Tdaska
      end;  // for i
   ShowD('Rubne trake u�itane');
End;



Procedure Cut_Edit_Exit(Sender:TObject);
// mijenja vrijednost varijable nakon promjene vrijednosti na ekranu
var
   rr:single;
   pass:boolean;
   tt:Integer;  // tag
Begin
if TEdit(Sender).modified then begin
   ShowD('Procedure Cut_Edit_Exit(Sender:TObject);');
    pass:=True;
   try
       rr:=StrToFloat(Tedit(Sender).text);
   except
       pass:=False; 
   end;
   
   if pass=false then begin  // Tedit(Sender).text:='0';
                             ShowD(Tedit(Sender).text + ' nije broj!')
   end;
  { 
  if  (Length(TEdit(Sender).Text)>0) and (pass=TRUE) then begin   
      tt:=TEdit(Sender).Tag;
      if Sender = Ed_A then Var_A[tt]:=StrToFloat(Ed_A.Text);
      if Sender = Ed_B then Var_B[tt]:=StrToFloat(Ed_B.Text);
      if Sender = Ed_C then Var_C[tt]:=StrToFloat(Ed_C.Text);
      if Sender = Ed_D then Var_D[tt]:=StrToFloat(Ed_D.Text);
      if Sender = Ed_E then Var_E[tt]:=StrToFloat(Ed_E.Text);
      if Sender = Ed_F then Var_F[tt]:=StrToFloat(Ed_F.Text);
      if Sender = Ed_G then Var_G[tt]:=StrToFloat(Ed_G.Text);
      if Sender = Ed_R then Var_R[tt]:=StrToFloat(Ed_R.Text);
      
        
  End else begin
       // TEdit(Sender).text:='0'; 
       // TEdit(Sender).selStart:=0; 
       // TEdit(Sender).selLength:=20; 
  end;
  }
end;  // if TEdit(Sender).modified
  ShowD('END: Procedure Cut_Edit_Exit(Sender:TObject);');
  ispis_varijabli;
End;

Procedure cb_B_Onclick(Sender:TObject);
Begin
   If cb_B.Checked then begin
                              Lab_B.Enabled:=True;
                              Sl_B.Enabled:=True;
                              Ed_B.Enabled:=True;
                        end else begin
                              Sl_B.Enabled:=False;
                              Ed_B.Enabled:=False;
                              Lab_B.Enabled:=False;
   end;
End;

Procedure cb_F_Onclick(Sender:TObject);
Begin
   If cb_F.Checked then begin
                              Lab_F.Enabled:=True;
                              Sl_F.Enabled:=True;
                              Ed_F.Enabled:=True;
                        end else begin
                              Sl_F.Enabled:=False;
                              Ed_F.Enabled:=False;
                              Lab_F.Enabled:=False;
   end;
End;

Procedure rb_C_Onclick(Sender:TObject);
var i:Integer;
Begin
   Lab_C.Enabled:=True;
   Sl_C.Enabled:=True;
   Ed_C.Enabled:=True; 
   ceo[TradioButton(Sender).Tag]:=0;     // ishodi�te je 0 (po�inje od C)
   
   Lab_G.Enabled:=False;
   Sl_G.Enabled:=False;
   Ed_G.Enabled:=False;
   rb_G.checked:=False;
   
End;

Procedure rb_G_Onclick(Sender:TObject);
Begin
   Lab_G.Enabled:=True;
   Sl_G.Enabled:=True;
   Ed_G.Enabled:=True; 
   ceo[TradioButton(Sender).Tag]:=1;     // ishodi�te je 1 (po�inje od G)
   
   Lab_C.Enabled:=False;
   Sl_C.Enabled:=False;
   Ed_C.Enabled:=False;
   rb_C.checked:=False;
   
End;


Procedure Sl_Cut_OnScroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: Integer);
var
   tt:Integer;
Begin
 
      tt:=TScrollBar(Sender).Tag;
      ShowD('Sl_Cut_OnChange. Tag: '+IntToStr(Tt)+' Pos: '+IntToStr(TScrollBar(Sender).Position));
      if Sender = Sl_A then Begin 
                              Var_A[tt]:=Sl_A.Position; 
                              Ed_A.Text:=IntToStr(Sl_A.Position);
                              If Cb_B.checked=False then Begin
                                                               Sl_B.Position:=Sl_A.Position;;
                                                               Var_B[tt]:=Sl_A.Position;
                                                               Ed_B.Text:=IntToStr(Sl_A.Position)
                                                          end;         
                              End;
      if (Sender = Sl_B) and (Cb_B.checked=True) then Begin Var_B[tt]:=Sl_B.Position; Ed_B.Text:=IntToStr(Sl_B.Position) End;
      if Sender = Sl_C then Begin Var_C[tt]:=Sl_C.Position; Ed_C.Text:=IntToStr(Sl_C.Position) End;
      if Sender = Sl_D then Begin 
                              Var_D[tt]:=Sl_D.Position; 
                              Ed_D.Text:=IntToStr(Sl_D.Position) 
                              If Cb_F.checked=False then Begin
                                                               Sl_F.Position:=Sl_D.Position;;
                                                               Var_F[tt]:=Sl_D.Position;
                                                               Ed_F.Text:=IntToStr(Sl_D.Position)
                                                          end;         
                              End;
      if Sender = Sl_E then Begin Var_E[tt]:=Sl_E.Position; Ed_E.Text:=IntToStr(Sl_E.Position) End;
      if (Sender = Sl_F) and (CB_F.checked=true) then Begin Var_F[tt]:=Sl_F.Position; Ed_F.Text:=IntToStr(Sl_F.Position) End;
      if Sender = Sl_G then Begin Var_G[tt]:=Sl_G.Position; Ed_G.Text:=IntToStr(Sl_G.Position) End;
      if Sender = Sl_R then Begin Var_R[tt]:=Sl_R.Position; Ed_R.Text:=IntToStr(Sl_R.Position) End;
 
      ispis_varijabli;
      ShowD('*');
End; 

procedure RV_Edge_Change(sender:TObject);
Begin
   if sender=RB_EdgeOne then begin
         CMB_E1.enabled:=true;
         CMB_E2.enabled:=true;
         CMB_E3.enabled:=true;
         CMB_E4.enabled:=true;
         CMB_E5.visible:=false;
         CMB_E6.visible:=false;
         Lab_U.visible:=false;
         CMB_E7.visible:=false;
   end; 
   
   if sender=RB_EdgeAll then begin
         CMB_E1.enabled:=false;
         CMB_E2.enabled:=false;
         CMB_E3.enabled:=false;
         CMB_E4.enabled:=false;
         CMB_E5.visible:=true;
         CMB_E6.visible:=false;
         Lab_U.visible:=false;
         CMB_E7.visible:=false;
   end; 
   
   if sender=RB_EdgeChange then begin
         CMB_E1.enabled:=false;
         CMB_E2.enabled:=false;
         CMB_E3.enabled:=false;
         CMB_E4.enabled:=false;
         CMB_E5.visible:=false;
         CMB_E6.visible:=true;
         Lab_U.visible:=true;
         CMB_E7.visible:=true;
   end; 
End;




procedure set_events;
var
   i:INteger;
Begin

   Ed_A.OnKeyPress:=@rcCheckReal;
   Ed_B.OnKeyPress:=@rcCheckReal;
   Ed_C.OnKeyPress:=@rcCheckReal;
   Ed_D.OnKeyPress:=@rcCheckReal;
   Ed_E.OnKeyPress:=@rcCheckReal;
   Ed_F.OnKeyPress:=@rcCheckReal;
   Ed_G.OnKeyPress:=@rcCheckReal;
   Ed_R.OnKeyPress:=@rcCheckReal;

 for i:=0 to 10 do begin
   
	if (i<6) or (i>7) then begin
		
		// provjera ispravnosti znakova
		Pan_Val[i].OnKeyPress:=@rcCheckReal;
		Pan_For[i].OnKeyPress:=@rcCheckStrPrg;
      
		// provjera mogu�nosti upisa
		CB_sel[i].OnClick:=@check_CB;
		Pan_For[i].OnChange:=@check_CB;
   
		// provjera ispravnosti realnog broja
		Pan_Val[i].OnChange:=@CheckIfReal;
		
	end;   // if (i<6) or (i>7)
 end;
   // promjena tipa
   Cob_Tip.OnClick:=@Change_Tip;
   
   // promjena vrijednosti polja za rezove
   {
   Ed_A.OnChange:=@CheckIfReal;
   Ed_B.OnChange:=@CheckIfReal;
   Ed_C.OnChange:=@CheckIfReal;
   Ed_D.OnChange:=@CheckIfReal;
   Ed_E.OnChange:=@CheckIfReal;
   Ed_F.OnChange:=@CheckIfReal;
   Ed_G.OnChange:=@CheckIfReal;
   Ed_R.OnChange:=@CheckIfReal;
   }
   Ed_A.OnChange:=@Cut_Edit_OnChange;
   Ed_B.OnChange:=@Cut_Edit_OnChange;
   Ed_C.OnChange:=@Cut_Edit_OnChange;
   Ed_D.OnChange:=@Cut_Edit_OnChange;
   Ed_E.OnChange:=@Cut_Edit_OnChange;
   Ed_F.OnChange:=@Cut_Edit_OnChange;
   Ed_G.OnChange:=@Cut_Edit_OnChange;
   Ed_R.OnChange:=@Cut_Edit_OnChange;
   
   cb_B.OnClick:=@cb_B_Onclick;
   cb_F.OnClick:=@cb_F_Onclick;
   
   rb_C.OnClick:=@rb_C_Onclick;
   rb_G.OnClick:=@rb_G_Onclick;
   
   Ed_A.OnExit:=@Cut_Edit_Exit;
   Ed_B.OnExit:=@Cut_Edit_Exit;
   Ed_C.OnExit:=@Cut_Edit_Exit;
   Ed_D.OnExit:=@Cut_Edit_Exit;
   Ed_E.OnExit:=@Cut_Edit_Exit;
   Ed_F.OnExit:=@Cut_Edit_Exit;
   Ed_G.OnExit:=@Cut_Edit_Exit;
   Ed_R.OnExit:=@Cut_Edit_Exit;
   
   // Ed_A.OnEnter:=@Cut_Edit_Enter;
   // Ed_B.OnEnter:=@Cut_Edit_Enter;
   // Ed_C.OnEnter:=@Cut_Edit_Enter;
   // Ed_D.OnEnter:=@Cut_Edit_Enter;
   // Ed_E.OnEnter:=@Cut_Edit_Enter;
   // Ed_F.OnEnter:=@Cut_Edit_Enter;
   // Ed_R.OnEnter:=@Cut_Edit_Enter;
   
   Sl_A.OnScroll:=@Sl_Cut_OnScroll;
   Sl_B.OnScroll:=@Sl_Cut_OnScroll;
   Sl_C.OnScroll:=@Sl_Cut_OnScroll;
   Sl_D.OnScroll:=@Sl_Cut_OnScroll;
   Sl_E.OnScroll:=@Sl_Cut_OnScroll;
   Sl_F.OnScroll:=@Sl_Cut_OnScroll;
   Sl_G.OnScroll:=@Sl_Cut_OnScroll;
   Sl_R.OnScroll:=@Sl_Cut_OnScroll;
   
   // Sl_A.OnScroll:=@Sl_Cut_OnScroll;
                  
   
   Pan_propH.OnClick:=@Prop_on_off;
   Pan_CutsH.OnClick:=@Cuts_on_off;
   Pan_CutsH.OnResize:=@Cuts_resize;
   Shape_Sense.OnMouseMove:=@Sense_Over;
   Shape_Sense.OnMouseUp:=@Sense_Click;
   Pan_Edge.OnResize:=@Pan_Edge_Resize;
   Pan_EdgeH.OnClick:=@Edge_on_off; 
   
   RB_EdgeOne.OnClick:=@RV_Edge_Change;
   RB_EdgeAll.OnClick:=@RV_Edge_Change;
   RB_EdgeChange.OnClick:=@RV_Edge_Change;
end;
var
   i:Integer;
Begin
         
   
   Debug:=false;         // debuger ispis on/off
   
   // Na�i selektirane objekte
   j:=0;
   if e<>nil then for i:=0 to e.childdaska.CountObj-1 do begin
      if e.childdaska.Daska[i].selected = true then j:=j+1;
   end;
   
	// showmessage('Dobra dan!') 
	
   if j<1 then begin 
   
            showmessage('Nema selektiranih dasaka!') 
          
      end else begin 

            {$I RezoviMainForm.cps}    // crtanje forme
				
            ShowN('Rezovi v2.0 24.05.2013. Dodani rezovi u skripti Osobine');
				ShowN('Rezovi v2.1 14.01.2014. Sakrivene rubne trake i osobine, ostaju samo rezovi');
            ShowN('Rezovi v2.2 18.03.2014. Smanjen broj segmenata kod zaobljavanja zbog izbjegavanja gre�ke');
				ShowN('--------------');
            ShowN('Selektirano:');
            
				Pan_prop.visible:=false;   // sakrij porperties
				Pan_edge.visible:=false;	// sakrij rubne trake
				CB_Cuse.checked:=true;	   // uklju�i check box za rezove
			   Pan_CutsH.visible:=False;
				Pan_Cuts.height:=310;	   // otvori panel s rezovima
				// smanji visinu prozora
				Main.Height:=377;
              
            Main.top:=300; 
            Main.show;           // poka�i formu
            read_properties;  
            read_cuts;  
            read_edges; 
            set_events; 
            
            ButOk.OnClick:=@Napravi; 
         // ButCancel.OnClick:=@Zatvori; 
            
            ButOk.ModalResult:=mrOk;
            ButCancel.ModalResult:=mrCancel;  
            
            if e<>nil then e.RecalcFormula(e); 
            ShowD('==========');
            ShowD('SKRIPT END');
            ShowD('==========');
            Main.Hide;     
            Main.ShowModal;   
            Main.free; 
            
   end;   // if j<1
   
End.

