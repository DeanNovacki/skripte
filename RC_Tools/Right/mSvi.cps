// Ovo je include za sve skripte na ikonicama 
// koje otvaraju toolbarove s velikim ikonama
// 
// Pode�ava sve toolbarove po veli�ini i poziciji	


var
	Fright, 		// desni izbornik koji je stalno vidljiv
	Fmain,		// ma�ka
	Fkuh, 		// kuhinje
	Formar, 		// ormari
	// Fselect,		// selekcija
	Fbris, 		// brisanje
	Frazno,		// razne skripte (kota�i�)
	Fset, 		// Setup (klju�)
	Fdod,			// dodatne aplikacije
	Fsys : TForm;			// folderi itd
	ttop, lleft, visico:Integer;
	xo:TObject;

function find_controlx(root_object:TWinControl;ime:string):TObject;
var
   i:integer;
   xo:TObject;
begin
	for i:=root_object.ControlCount-1 downto 0 do begin
		xo:=root_object.Controls[i];
		if xo is TForm then 
		   if  TForm(xo).caption=ime Then result:=xo;
	end;  // for 
end;  // function	
	
Procedure SakrijSve;
Begin
	Fmain.visible:=false;
	Fkuh.visible:=false;
	Formar.visible:=false;
	// Fselect.visible:=false;
	Fbris.visible:=false;
	Frazno.visible:=false;
	Fset.visible:=false;
	Fdod.visible:=false;
	Fsys.visible:=false;
	Print('Sakrio sam sve');
	
End;

Begin
	ttop:=3;
	lleft:=5;
	visico:=38;

	// VA�NO!!! 
	//
	// svi forma.WIDTH moraju biti 0 u Project.toc 
	//
	
	
	// RC_main
	xo:=Find_Controlx(application.MainForm,'Right'); 
	if xo<>nil then Fright:=TForm(xo);

	// mMain 2x3
	xo:=Find_Controlx(application.MainForm,'Corpus RC');
	if xo<>nil then Fmain:=TForm(xo);
	if Fmain.width=0 then begin
		Fmain.visible:=false; 
		// Print('Sakrio sam Main u startu');
	end;
	Fmain.borderStyle:=bsDialog;
	Fmain.BorderIcons:=[biSystemMenu];
	// Fmain.borderStyle:=bsNone;
	Fmain.Align:=AlNone;
	Fmain.width:=212; Fmain.Height:=334; 
	Fmain.left:=Fright.left-Fmain.width-lleft; 
	Fmain.top:=Fright.top+ttop+visico*(1-1); 
	// FMain.OnCloseQuery:=@fMainClose;  // Samo probleme radi!
	
	// mKuhinja 3x2
	xo:=Find_Controlx(application.MainForm,'Kuhinja');
	if xo<>nil then Fkuh:=TForm(xo);
	if Fkuh.width=0 then Fkuh.visible:=false; 
	Fkuh.borderStyle:=bsDialog;
	Fkuh.BorderIcons:=[biSystemMenu];
	Fkuh.Align:=AlNone;
	Fkuh.width:=315; Fkuh.Height:=232; 
	Fkuh.left:=Fright.left-Fkuh.width-lleft; 
	Fkuh.top:=Fright.top+ttop+visico*(2-1); 
	
	// mOrmar 1x1
	xo:=Find_Controlx(application.MainForm,'Ormari');
	if xo<>nil then Formar:=TForm(xo)
				  else SHowmessage('Formar:=TForm(xo) je NIL!!!!!');	
	if Formar.width=0 then Formar.visible:=false; 
	Formar.borderStyle:=bsDialog;
	Formar.BorderIcons:=[biSystemMenu];
	Formar.Align:=AlNone;
	Formar.width:=109; Formar.Height:=130; 
	
	Formar.left:=Fright.left-Formar.width-lleft; 
	Formar.top:=Fright.top+ttop+visico*(3-1); 
	
	// mSelect 3x3
{
	xo:=Find_Controlx(application.MainForm,'Selekcija');
	if xo<>nil then Fselect:=TForm(xo)
				  else Showmessage('FSelelect:=TForm(xo) je NIL!!!!!');	
	if Fselect.width=0 then Fselect.visible:=false; 
	Fselect.borderStyle:=bsDialog;
	Fselect.BorderIcons:=[biSystemMenu];
	Fselect.Align:=AlNone;
	Fselect.width:=315; Fselect.Height:=335; 
	Fselect.left:=Fright.left-Fselect.width-lleft; 
	Fselect.top:=Fright.top+ttop+visico*(4-1); 
}

	// mBrisanje 2x4
	xo:=Find_Controlx(application.MainForm,'Brisanje');
	if xo<>nil then Fbris:=TForm(xo);
	if Fbris.width=0 then Fbris.visible:=false; 
	Fbris.borderStyle:=bsDialog;
	Fbris.BorderIcons:=[biSystemMenu];
	Fbris.Align:=AlNone;
	Fbris.width:=212; Fbris.Height:=436; 
	Fbris.left:=Fright.left-Fbris.width-lleft; 
	Fbris.top:=Fright.top+ttop+visico*(5-1); 
	
	
	// mRazno 1x1
	xo:=Find_Controlx(application.MainForm,'Razno');
	if xo<>nil then Frazno:=TForm(xo);
	if Frazno.width=0 then Frazno.visible:=false; 
	Frazno.borderStyle:=bsDialog;
	Frazno.BorderIcons:=[biSystemMenu];
	Frazno.Align:=AlNone;
	Frazno.width:=109; Frazno.Height:=130; 

	Frazno.left:=Fright.left-Frazno.width-lleft; 
	Frazno.top:=Fright.top+ttop+visico*(6-1); 

	
	// mSetup 1x5
	xo:=Find_Controlx(application.MainForm,'Podesavanje');
	if xo<>nil then Fset:=TForm(xo);
	if Fset.width=0 then Fset.visible:=false; 
	Fset.borderStyle:=bsDialog;
	Fset.BorderIcons:=[biSystemMenu];
	Fset.Align:=AlNone;
	Fset.width:=109; Fset.Height:=538; 
	Fset.left:=Fright.left-Fset.width-lleft; 
	Fset.top:=Fright.top+ttop+visico*(7-1); 
	
	// mDodatni 1x4
	xo:=Find_Controlx(application.MainForm,'Dodatno');
	if xo<>nil then Fdod:=TForm(xo);
	if Fdod.width=0 then Fdod.visible:=false; 
	Fdod.borderStyle:=bsDialog;  // ne resizea se, moderan je
	// Fdod.borderStyle:=bsSizeToolWin;
	Fdod.BorderIcons:=[biSystemMenu];
	// Fdod.AutoScroll:=True; ???
	Fdod.Align:=AlNone;
	Fdod.width:=109; Fdod.Height:=436; 
	Fdod.left:=Fright.left-Fdod.width-lleft; 
	Fdod.top:=Fright.top+ttop+visico*(8-1); 
	
	
	// mSustav 2x4
	xo:=Find_Controlx(application.MainForm,'Sustav');
	if xo<>nil then Fsys:=TForm(xo);
	if Fsys.width=0 then Fsys.visible:=false; 
	Fsys.borderStyle:=bsDialog;
	Fsys.BorderIcons:=[biSystemMenu];
	Fsys.Align:=AlNone;
	Fsys.width:=212; Fsys.Height:=436; 
	Fsys.left:=Fright.left-Fsys.width-lleft; 
	Fsys.top:=Fright.top+ttop+visico*(9-1); 
	
	
	
	Print('include {$I mSvi.cps u�itan i izvr�en!');	
