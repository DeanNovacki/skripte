
var
   tmp:string;


 function MulDiv(N, N1, Di:integer):integer;
 begin
      result:=round(n*n1/di);
 end;



function MessageYesNo(	
								const 	ACaption, APrompt: string;
											pass:boolean;
								var 		Value: string
							): Boolean;
var
  MForm: TForm;
  Prompt: TLabel;
  Edit: TEdit;
  DialogUnits: TPoint;
  ButtonTop, ButtonWidth, ButtonHeight: Integer;
begin
  Result := False;
  MForm := TForm.Create(Application);
  with MForm do
    try
//      Canvas.Font := Font;
      DialogUnits.x := 10;
      DialogUnits.y := 12;
      BorderStyle := bsDialog;
      Caption := ACaption;
      ClientWidth := MulDiv(180, DialogUnits.X, 4);      
      Position := poScreenCenter;
      Prompt := TLabel.Create(MForm);
      with Prompt do
      begin
        Parent := MForm;
        Caption := APrompt;
        Left := MulDiv(8, DialogUnits.X, 4);
        Top := MulDiv(8, DialogUnits.Y, 8);
        
        //Constraints.MaxWidth := MulDiv(164, DialogUnits.X, 4);
        WordWrap := True;
      end;
      Edit := TEdit.Create(MForm);
      with Edit do
      begin
        Parent := MForm;
        Left := Prompt.Left;
        Top := Prompt.Top + Prompt.Height + 5;
        Width := MulDiv(164, DialogUnits.X, 4);
        MaxLength := 255;
        if pass then   PasswordChar:='*';
        Text := Value;
        SelectAll;
      end;
      ButtonTop := Edit.Top + Edit.Height + 15;
      ButtonWidth := MulDiv(50, DialogUnits.X, 4);
      ButtonHeight := MulDiv(14, DialogUnits.Y, 8);
      with TButton.Create(MForm) do
      begin
        Parent := MForm;
        Caption := 'U redu';
        ModalResult := mrOk;
        Default := True;
        left:=MulDiv(38, DialogUnits.X, 4);
        top:=ButtonTop;
        Width:=ButtonWidth;
        Height:=ButtonHeight;
      end;
      with TButton.Create(MForm) do
      begin
        Parent := MForm;
        Caption := 'Odustani';
        ModalResult := mrCancel;
        Cancel := True;
        left:=MulDiv(92, DialogUnits.X, 4);
        top:=ButtonTop;
        Width:=ButtonWidth;
        Height:=ButtonHeight;
        MForm.ClientHeight := Top + Height + 13;          
      end;
      if ShowModal = mrOk then
      begin
        Value := Edit.Text;
        Result := True;
      end;
    finally
      MForm.Free;
    end;
end;

begin

if MessageYesNo('Naslov prozora', 'Neki tekst poruke gfdgdfs gfds gfd sg fds gfd g fd g fds g fds gf dsg  gfdsgfdsgfdsgfd   g fds g fd gfd s',false,tmp) then showmessage(tmp);

end.