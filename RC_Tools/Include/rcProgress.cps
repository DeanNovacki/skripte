

Procedure progress_create(nProzor: TWinControl);
// var
   // vars must be declared in main program
   // progress_panel,bar_out, bar_in:TPanel;
   // progress_text:TLabel;
Begin
   // ShowN('Procedure progress_create(nProzor: TWinControl)');
   Progress_panel:=rcPanel( '',  nProzor);
   Progress_panel.width:=650;
   Progress_panel.height:=60;
   Progress_panel.left:=round((nProzor.width-Progress_panel.width)/2);
   Progress_panel.top:=200;
   
   bar_out:=rcPanel( '',  Progress_panel);
   bar_out.width:=Progress_panel.width-30;
   bar_out.height:=20;
   bar_out.left:=round((Progress_panel.width-bar_out.width)/2);
   bar_out.top:=10;              
   
   bar_in:=rcPanel( '',  Progress_panel);
   bar_in.width:=bar_out.width-4;
   bar_in.height:=16;
   bar_in.left:=bar_out.left+2;
   bar_in.top:=bar_out.top+2;
   bar_in.color:=clRed;
   
	// function rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
   progress_text:=rclabel( 'Progress_bar','Progress bar', Progress_panel, 10,10, taLeftJustify);
   
   progress_text.left:=round((Progress_panel.width-progress_text.width)/2);
   progress_text.top:=35;
   
   Progress_panel.visible:=false;
   // ShowN('done: Procedure progress_create(nProzor: TWinControl)');                                      
End;    

Procedure progress_change(pb_perc:Integer;pb_mess:string);
var
   full_width, bar_width:integer;
Begin
   if pb_perc =100 then Progress_panel.visible:=false
                   else Progress_panel.visible:=true; 
   full_width:=bar_out.width-4;
   bar_width:=round(((pb_perc/100))*full_width);
   bar_in.width:=bar_width;
   // Application.ProcessMessages;
   progress_text.caption:=pb_mess;         
   Application.ProcessMessages;
end;

