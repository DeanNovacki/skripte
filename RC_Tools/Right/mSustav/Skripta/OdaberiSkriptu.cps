Program OdaberiSkriptu;   
// Odabir skripte za pokretanje iz Projekta
// Red Cat Ltd 
// v1.0 17.09.2018.

{$I ..\..\..\include\rcPrint.cps}    		// RC Console

{$I ..\..\..\include\rcMessages.cps}		// MessageYesNo

{$I ..\..\..\include\rcStrings.cps}

{$I ..\..\..\include\rcControls.cps}
// {$I ..\..\..\include\rcObjects.cps}
// {$I ..\..\..\include\rcEvents.cps}
// {$I ..\..\..\include\rcProgress.cps}
// {$I ..\..\..\include\rcCurves.cps}
// {$I ..\..\..\include\rcMath.cps}
{$I ..\..\..\include\rcConfig.cps}

var
	MyDate:tDateTime;	
	versionTxt: String;
	dList:tStringList;
	Prozor: tForm;
		LabelSkripta: tLabel;
		LabelPutanja: tLabel;
		Memo1:TMemo;
		TipkaLoad:tButton;
		TipkaEdit:tButton;
		TipkaIzlaz:tButton;
		
	iniFile, DefSkripta, LastSkripta:String;
	testSTR:String;
	
Procedure PripremiZaStart;
var
	SkriptaCode, tl :tStringList;
	PutanjaStart, PutanjaEnd, PS : String;

Begin
	Print ('proc: PripremiZaStart Start');
	PS:=PrgFolder+'skripte\RC_Tools\Right\mTest\Skripta\';
		
	If FileExists(PS+'scrname.txt') then Begin
	
		// �itanje putanje skripte
		
		tl:=tStringList.Create;
		tl.LoadFromFile(PS+'scrname.txt');	// u�itana putanja skripte
		PutanjaStart:=tl[0];
		Print (PutanjaStart);
		
		// u�itavanje skripte u listu
		
		SkriptaCode:=TStringList.Create;
		SkriptaCode.LoadFromFile(PutanjaStart);
		
		// spremanje skripte na mjesto 'Test.cps'
		
		PutanjaEnd:=PS+'Test.cps';		
		SkriptaCode.SaveToFile(PutanjaEnd);
		Print ('Spremljen je novi Test.cps')
	End else begin
		Print('Nema datoteke: scrname.txt');
	
	End;
		
	Print ('proc: PripremiZaStart End');
End;
	
	
Procedure TipkaLoadOnClick(Sender: TObject);
var
	dijalog:TOpenDialog;
	dummy:boolean;

Begin 
	Print ('proc: TipkaLoadOnClick Start');

	dijalog := TOpenDialog.Create(nil);
	dijalog.InitialDir := rcExtractPath(DefSkripta);
	dijalog.Filter := 'pascal skripta (*.cps)|*.cps';
	if dijalog.Execute then begin
		
		// punjenje memoa
		
		Memo1.Lines.LoadFromFile(Dijalog.FileName);
		
		// a�uriranje labela
		
		LabelSkripta.caption:=rcExtractName(dijalog.FileName);
		LabelSkripta.hint:=LabelSkripta.caption;
		LabelPutanja.caption:=rcExtractPath(dijalog.FileName);
		LabelPutanja.hint:=LabelPutanja.caption;
		
		// spremanje LastSkripta u INI file
		
		LastSkripta := rcRemovePrgFolder(dijalog.FileName);
		// TestStr:=rcExtract(LastSkripta);
		set_value_str(iniFile,'LastSkripta',LastSkripta);
		
		PripremiZaStart;
		
	end else begin
	   Print('Nisam ni�ta u�itao jer je Kanceliran OpenDialog!')
	
	
	end;	// if
	
	
	
	
	
	Print ('proc: TipkaLoadOnClick End');
End;
	
Procedure TipkaEditOnClick	(Sender: TObject);
var 
	PDF:TpdfPrinter;
	PutanjaSkripte, PutanjaScrName, PutanjaPrgName, PutanjaProgram, PutanjaBat:String;
	ll:TStringList;
Begin
	Print ('proc: TipkaEditOnClick Start');
	
	PutanjaScrName	:=	PrgFolder+'skripte\RC_Tools\Right\mTest\Skripta\scrname.txt';
	PutanjaSkripte	:=	LabelPutanja.caption+LabelSkripta.caption;
	PutanjaPrgName	:=	PrgFolder+'skripte\RC_Tools\Right\mTest\Skripta\prgname.txt';
	PutanjaProgram	:=	PrgFolder+'extra\npp\notepad++.exe';
	PutanjaBat		:=	PrgFolder+'skripte\RC_Tools\Right\mTest\Skripta\UrediSkriptu.bat';
	
	ll:=TStringList.Create;
	ll.Add(PutanjaSkripte);
	ll.SaveToFile(PutanjaScrName);
	
	ll.Clear;
	ll.Add(PutanjaProgram);
	ll.SaveToFile(PutanjaPrgName);
	
	// Showmessage('');
		
	// Print ('Putanja skripte = '+ PutanjaSkripte);
	// Print ('Mjesto zapisa = '	+ PutanjaScrName);	
	// Print ('Putanja bata = '	+ PutanjaBat);	
	
	if not FileExists(PutanjaBat)  	 then Showmessage('Ne postoji '+PutanjaBat);
	if not FileExists(PutanjaSkripte) then Showmessage('Ne postoji '+PutanjaSkripte);
	
	if FileExists(PutanjaBat) then begin
		
		PDF:=TpdfPrinter.create(1250);
		PDF.FileName := PutanjaBat;
		PDF.ShowPdf;
		PDF.Free
	
	end;
	
	
	
	Print ('proc: TipkaEditOnClick End'); 
End;
	
Begin

	versiontxt:='v1.0';
	MyDate:=Now;
	Print('____________________');
	PrintTime('Skripta: "OdaberiSkriptu" '+versiontxt+	#13#10+ ' START');
	iniFile:=prgfolder+'skripte\RC_Tools\Right\mTest\Skripta\OdaberiSkriptu.ini';
	defSkripta:='skripte\Test\test.cps';
	
	// postavljanje komponenti
	
	Prozor:=rcTool('Odaberi_Skriptu');
	Prozor.Position:=poScreenCenter;
	Prozor.Width:=450;Prozor.Height:=450;
	Prozor.BorderStyle:=bsSingle;
	Prozor.BorderIcons:=[biSystemMenu];
	
	LabelSkripta:=rcLabel('LabSkri','Skripta nije u�itana',Prozor,10,6,taLeftJustify);
	LabelSkripta.Hint:=LabelSkripta.Caption;
	LabelSkripta.Font.Size:=16;
	LabelSkripta.Font.Style:=[fsBold];

	LabelPutanja:=rcLabel('LabPut','C:\Red Cat\Corpus 4 RC\skripte\Testiranje',Prozor,10,33,taLeftJustify);
	LabelPutanja.Font.Size:=10;
	LabelPutanja.Hint:=LabelPutanja.caption;
	
	memo1:=rcMemo('',Prozor,10,55,420,260);
	Memo1.color:=clMaroon;
	Memo1.Font.Name:='Consolas';
	Memo1.Font.Size:=10;
	// Memo1.Font.Style:=[fsBold];
	Memo1.Font.Color:=clSilver;
	Memo1.WordWrap:=False;
	Memo1.ReadOnly:=True;
	
	TipkaLoad:=rcButton('TipLoad', 'U�itaj drugu skriptu', Prozor, 10,325);
	TipkaLoad.Width:=420;
	TipkaLoad.OnClick:=@TipkaLoadOnClick;
	
	TipkaEdit:=rcButton('TipEdit', 'Uredi skriptu', Prozor, 10,355);
	TipkaEdit.Width:=420;
	TipkaEdit.OnClick:=@TipkaEditOnClick;
	
	TipkaIzlaz:=rcButton('TipIzlaz', 'Izlaz', Prozor, 10,385);
	TipkaIzlaz.Width:=420;
	TipkaIzlaz.ModalResult:=mrCancel;
	
	// provjera postojanja ini datoteke
	
	if not (FileExists(inifile)) then begin
		
		Print ('Nema INI datoteke "'+iniFile+'"'+'. Kreiram novu.');
		dList:=Tstringlist.create;
		dList.SaveToFile(inifile);
		Print ('Kreirana je INI datoteka "'+iniFile+'"');
		
		// upisivanje vrijednosti u praznu ini datoteku
		set_value_str(iniFile,'DefaultSkripta', 'skripte\test\Test.cps');
		set_value_str(iniFile,'LastSkripta', 'skripte\test\Test.cps');
		
	end;
	
	// provjeravanje postojanja varijabli u ini datoteci
	
	
	if length(find_value_str(iniFile,'DefaultSkripta'))<5 then begin
		Print ('Kreiram "DefaultSkripta=..."');
		set_value_str(iniFile,'DefaultSkripta', DefSkripta);
	end;
	
	if length(find_value_str(iniFile,'LastSkripta'))<5 then begin
		Print ('Kreiram "LastSkripta=..."');
		set_value_str(iniFile,'LastSkripta', DefSkripta);
	end;
	
	// Print('5 INI obra�en');
		
	// punjenje komponenti 
	
	LabelSkripta.caption:=rcExtractName(find_value_str(iniFile,'LastSkripta'));
	LabelPutanja.caption:=PrgFolder+rcExtractPath(find_value_str(iniFile,'LastSkripta'));
	
	// Print('6 Labeli obra�eni');
	
	LastSkripta:=PrgFolder+find_value_str(iniFile,'LastSkripta');
	// Print('7 LastSkripta=' + LastSkripta);
	
	Memo1.Lines.Clear;
	Memo1.Lines.LoadFromFile(LastSkripta);
	
	// Print('7 Memo popunjen');
	
	Prozor.showmodal;
		// Note;
	Prozor.free;
	
	MyDate:=Now;
	PrintTime('Skripta: "OdaberiSkriptu" '+versiontxt+' End');
	Print('____________________');
End.
