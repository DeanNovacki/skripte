// dodavanje radnih ploca u projekt

// 

Program radne_ploce_01;

var
//   e.elmlist.element[i] - djeca osnovnog elementa u editoru     
//   e.elmlist.countobj=broj elemenata

   element1: TElement;
   kandidat: array of Boolean;       //na njega moze ici radna ploca
   kand_postoji:boolean;
   Radna_ploca:Tdaska;
   i1:integer;
{$I math_rot.cps}



// uvjeti za postavljanje radne ploce
// visina+y<1500; dubina>100; tip: podni ili podni kutni
// 

Procedure nadji_kandidate(Pelement:telement;var Pkand_postoji:boolean); 
//to su oni na koje moze ici radna ploca
var
   i:integer;
   Pe:Telement;
begin
   for i:=0 to Pelement.elmlist.countobj-1 do begin
       Pe:=Pelement.elmlist.element[i];
       if ((Pe.TipElementa=PC) or (Pe.TipElementa=PK)) 
          AND ((Pe.Visina+Pe.YPos<1500)
          AND (Pe.Dubina>100)) then 
          begin 
            // writeln('element '+ Pe.Naziv + ' je kandidat za radnu plo�u.');
            kandidat[i]:=true;
            Pkand_postoji:=true;     
          end;
   end;
   if Pe=Nil then ShowMessage ('Nema kandidata za radnu plo�u.');
                
end;

Function Kreiraj_radnu_plocu(Pelement:Telement):TDaska;
//  - kreiraj radnu_plocu prema poslanom elementu

var
   RP1:Tdaska;
   RP_vect:TVector4f;  // definiranje tipa potrebnog za rotaciju
   Z_pomak:single;
   RP_dubina:single;
Begin
   RP_dubina:=600;
   // kreiranje daske na istu poziciju na kojoj je i element prema kojem se ta daska kreira
   RP1:=tdaska.CreateDaska(e,Pelement.Xpos,Pelement.YPos+Pelement.Visina,Pelement.Zpos,Pelement.sirina,RP_dubina,38);
   RP1.naziv:='RP';
   RP1.kut:=Pelement.kut;
   RP1.tipdaske:=RadnaPloca;
   RP1.Smjer:=Horiz;
   RP1.Texindex:=38;
   
   // radnu plocu treba pomaknuti da viri naprijed
   // 
   
   If Pelement.dubina+20>RP_dubina then RP_dubina:=Pelement.dubina+30;  // prosirenje premale radne ploce
   Z_pomak:=RP_dubina-Pelement.Dubina;
      
   // punjenje vektora
   RP_Vect[0]:=0;
   RP_Vect[1]:=0;
   RP_Vect[2]:=Z_pomak;
   
   { writeln('prije rotacije');
   writeln('RP_Vect(0):'+FloatToStr(RP_Vect[0]));
   writeln('RP_Vect(1):'+FloatToStr(RP_Vect[1]));
   writeln('RP_Vect(2):'+FloatToStr(RP_Vect[2]));  }   
   
   // rotacija
   RP_Vect:=rotateVector(RP_Vect,RP1.kut,1); 
   
   { writeln('nakon rotacije');
   writeln('RP_Vect(0):'+FloatToStr(RP_Vect[0]));
   writeln('RP_Vect(1):'+FloatToStr(RP_Vect[1]));
   writeln('RP_Vect(2):'+FloatToStr(RP_Vect[2])); }   
   
   // nova pozicija   
   RP1.xPos:=RP1.xPos+RP_Vect[0];
   RP1.yPos:=RP1.yPos+RP_Vect[1];
   RP1.zPos:=RP1.zPos+RP_Vect[2];
   

   { writeln('RP1.xPos:'+FloatToStr(RP1.xPos));
   writeln('RP1.yPos:'+FloatToStr(RP1.yPos));
   writeln('RP1.zPos:'+FloatToStr(RP1.zPos));  }   
      
   
   e.adddaska(RP1);
   { writeln('Kreirana je radna plo�a '+ RP1.Naziv);
   writeln('Element: x='+FloatToStr(Pelement.Xpos)+'    Y='+FloatToStr(Pelement.Ypos)+'    Z='+FloatToStr(Pelement.Zpos)+'    k='+FloatToStr(Pelement.Kut));
   writeln('R. ploca: x='+FloatToStr(RP1.Xpos)+'    Y='+FloatToStr(RP1.Ypos)+'    Z='+FloatToStr(RP1.Zpos)+'    k='+FloatToStr(RP1.Kut)); }
   Result:=RP1;
End;

Function Nadji_kandidata:Telement;
// pronalazi prvi element koji je (jo� uvijek) kandidat
var
   i:integer;
begin
   for i:=0 to e.elmlist.countobj-1 do begin
      if kandidat[i]=true then begin
          result:=e.elmlist.element[i];
          // kandidat[i]=false;
          exit;
      end;
   end
   result:=nil;
end;

Procedure Izbaci_kandidata(Pelement:Telement);
// Ozna�ava da teku�i element vi�e nije kandidat
begin
   kandidat[e.elmlist.indexof(Pelement)]:=false;
end;

Function pase_uz_postojecu(Felement:Telement;Fradna_Ploca:TDaska):Boolean;
// provjerava da li element pase za nastavak postojece radne ploce
var
   RP_vect,El_vect:TVector4f;
   i,nacin:integer;
Begin
   RP_Vect[0]:=Fradna_Ploca.xPos;
   RP_Vect[1]:=Fradna_Ploca.yPos;
   RP_Vect[2]:=Fradna_Ploca.zPos;
   El_Vect[0]:=Felement.xPos;   
   El_Vect[1]:=Felement.yPos;
   El_Vect[2]:=Felement.zPos;
   
   // postavljanje u nulu
   for i:=0 to 2 do RP_Vect[i]:=RP_Vect[i]-El_Vect[i];
   
   // rotacija
   RP_Vect:=rotateVector(RP_Vect,360-Felement.Kut,1);            // rotacija   

   // vra�anje na poziciju
   for i:=0 to 2 do RP_Vect[i]:=RP_Vect[i]+El_Vect[i];
   
   // for i:=0 to 2 do writeln(fLOATtostr(RP_Vect[i])+' '+fLOATtostr(El_Vect[i]));
   
   
   
   If ABS(El_Vect[0]+Felement.Sirina-RP_Vect[0])<1 then nacin:=1                   // element je lijevo
      else if ABS(RP_Vect[0]+Fradna_ploca.Visina-El_Vect[0])<1  then nacin:=2       // element je desno
           else nacin:=0;                                                   // element je u kurcu
   //writeln('nacin: '+IntToStr(nacin));       
   Case nacin of 
       0: result:=false;
       1: begin
              // writeln('Nacin: 1. Prosirujem radnu plocu na lijevu stranu'); 
              RP_Vect[0]:=Felement.xPos;
              RP_Vect[2]:=Felement.zPos+50;
              
              for i:=0 to 2 do RP_Vect[i]:=RP_Vect[i]-El_Vect[i];
              RP_Vect:=rotateVector(RP_Vect,Felement.Kut,1); 
              for i:=0 to 2 do RP_Vect[i]:=RP_Vect[i]+El_Vect[i];
              
              Fradna_Ploca.xPos:=RP_Vect[0];
              Fradna_Ploca.zPos:=RP_Vect[2];
              
              Fradna_Ploca.Visina:=Fradna_Ploca.Visina+Felement.Sirina;
              izbaci_kandidata(Felement);
              result:=true;
          end;
       2: begin
              // writeln('Nacin: 2. Prosirujem radnu plocu na desnu stranu');  
              Fradna_Ploca.Visina:=Fradna_Ploca.Visina+Felement.Sirina;
              izbaci_kandidata(Felement); 
              result:=true;             
          end;
   
   end; 

   
         
end;


Begin

SetArrayLength(kandidat,e.elmlist.countobj);
nadji_kandidate(e,kand_postoji);
if kand_postoji=false then begin
                              ShowMessage ('Ne moze se postaviti radna ploca!');
                              exit;
                            end;

                            
repeat
element1:=nadji_kandidata;

 if element1<>nil then begin

   Radna_ploca:=Kreiraj_radnu_plocu(Element1);
   izbaci_kandidata(Element1);
   i1:=0;
   while i1<e.elmlist.countobj do begin
      element1:=e.elmlist.element[i1]; 
      if (kandidat[i1]=true) AND (element1.kut=Radna_ploca.kut) then begin
      
      
//      ====================================

         if pase_uz_postojecu(element1,Radna_ploca) then begin
            // ('Pa�e!');
            i1:=0;
         end;
      end;
      i1:=i1+1;
   end; //for

 end; // if
until element1=nil;
  

end.

  