{
Function RC_LoadImage(file_name:string; destination: TWinControl; xx,yy,ww,hh:Integer):TMImage;
Function Sqr(a:single):single;
Function Tg(a:single):single;
Function Ctg(a:single):single;
function Window(nTekst: string; nS, nV: integer):TForm;
function naslov( nTekst: string; nProzor: TWinControl):TLabel;
function ScrollBox( nTekst: string; nProzor: TWinControl):TScrollBox;
function RC_Panel( CText: string; nProzor: TWinControl):TPanel;
function natpis( nTekst: string; nProzor: TWinControl; nX, nY:integer; poravnanje:String):TLabel;
function RC_label( nText: string; nDestination: TWinControl; nX, nY:integer; Align:String):TLabel;
Function RC_Edit(cap, units: string; destination: TWinControl; eX, eY: integer; kind:string):TEdit;
Procedure Check_Int_Pos(sender:Tobject; var Key: Char); 
Procedure Check_Int(sender:Tobject; var Key: Char);
Procedure Check_Real(sender:Tobject; var Key: Char); 
Procedure Check_Str_Prg(sender:Tobject; var Key: Char); 
function RadioBox(nTekst: string; nazivi:Array of String; def:integer; 
                          nProzor: TWinControl; nX, nY: integer):TRadioGroup;
Procedure RC_Image_Edit_Create    (
                  	destination         :TWinControl;  
                  	image_name          :string;      
                  	xx                  :integer;
                  	yy                  :integer;
			image_width         :integer;
                  	image_height        :integer;
			start_tag           :integer;
                  	quantity            :integer;
                        field_1_label	    :string;
			field_1_hint	    :string;
			field_2_label	    :string;
			field_2_hint	    :string;
                        field_3_label       :string;
			field_3_hint	    :string;
			units               :string
			);
Procedure RC_Radio_Image_Create  (
                  destination        :TWinControl;  
                  caption            :string;
                  start_tag          :integer;
                  quantity           :integer;
                  default            :integer;
                  left               :integer;
                  top                :integer;
                  width              :integer;
                  image_width        :integer;
                  image_height       :integer;
                  image_names        :string;                // tag+on/of+'name'+'.jpg'
                  image_descriptions :string;                // separated with '|' sign
                  script_path        :string
                  );                               
Procedure RC_Radio_Image_Change(  
                                 destination     : TWinControl;
                                 //image_name      : string;
                                 start_tag       : integer;
                                 end_tag         : integer;
                                 selected_tag    : integer
                                );                          
function RC_CheckBox(nText: string; nParent: TWinControl; nX, nY: integer):TCheckBox;                          
function RC_Button(nText: string; nParent: TWinControl; nX, nY: integer):TButton;
function Memo(nProzor: TWinControl; nX, nY, nS, nV: integer):TMemo;
function find_value_int(FileName,ValueName:string):integer;
function find_value_str(FileName,ValueName:string):string;
function set_value_str(FileName,ValueName, Value :string):boolean;
function set_value_int(FileName,ValueName :string; Value:Integer):boolean;
Procedure message(mess:string);
Procedure progress_create(nProzor: TWinControl);

}


Function RC_LoadImage(file_name:string; destination: TWinControl; xx,yy,ww,hh:Integer):TMImage;
// load image from disk and place to parent at xx,yy position and ww,hh size
// e.g. 
// pic:=LoadImage('c:\Good_Pictures\img1.jpg';panel6; 30,10.256,128);

var
   L_Image:TMImage;
   ok:boolean;   
begin
   OK:=true;
   L_Image:=TMImage.create(destination);
   L_Image.parent:=destination;
   try 
      L_Image.loadFromFile(file_name);
   except
      showmessage('image: "'+file_name+'"not found!');
      OK:=false;
   end;
   if OK then begin 
       L_Image.Left:=xx;       
       L_Image.Top:=yy;
       L_Image.Width:=ww; 
       L_Image.Height:=hh;
       L_Image.Showhint:=true;      
   end;
   result:=L_Image;
end;
   
Function Sqr(a:single):single;
// kvadriranje
begin
     result:=a*a;
end;

Function Tg(a:single):single;
// tangens
begin
     result:=sin(a)/cos(a);
end;

Function Ctg(a:single):single;
// kotangens
begin
     result:=cos(a)/sin(a);
end;

// Function random(a:integer):integer;
     

function Window(nTekst: string; nS, nV: integer):TForm;
// kreira prozor
// nTekst = naziv prozora
// nS = Sirina prozora
// nV = Visina prozora
 var
     nProzor:TForm;
 begin
     nProzor:=TForm.create(nil);              // definiranje forme
     nProzor.borderIcons:=[];
     nProzor.borderStyle:=bsToolWindow;
     nProzor.position:=poScreenCenter;
     nProzor.Width:=nS;
     nProzor.Height:=nV;                       //top,width,height
     nProzor.caption:=nTekst; 
     result:=nProzor;
 end;

// function input(nTekst:string; 
 
function naslov( nTekst: string; nProzor: TWinControl):TLabel;
// procedura koja napravi label na gornjoj sredini prozora (panela)
// primjer: napisi('Ovo je naslov', Prozor)
// nTekst = Caption labele
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira labela
 var
   Tekst_1:TLabel;
 begin
     Tekst_1:=TLabel.create(nProzor);
     Tekst_1.parent:=nProzor;
     // Tekst_1.font.style:=[FsBold];
     Tekst_1.font.size:=14;
     Tekst_1.font.name:='Arial';
     Tekst_1.caption:=nTekst;
     Tekst_1.left:=round(nProzor.width/2-Tekst_1.width/2);
     Tekst_1.top:=10;
     result:=Tekst_1;
end;

function ScrollBox( nTekst: string; nProzor: TWinControl):TScrollBox;
// procedura koja radi ScrollBoxpanel
// 
// 
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira labela
 var
   ScrollBox_1:TScrollBox;
 begin
     ScrollBox_1:=TScrollBox.create(nProzor);
     //ScrollBox_1.align:=alNone;    // alNone, alTop, alBottom, alLeft, alRight, alClient 
     ScrollBox_1.parent:=nProzor;
     //ScrollBox_1.BevelInner:=bvNone;
     //ScrollBox_1.BevelOuter:=bvRaised;
     ScrollBox_1.BorderStyle:=bsNone;
     //ScrollBox_1.BorderWidth:=0;
     //ScrollBox_1.Caption:='';
     //Panel_1.Left:=100;
     //Panel_1.Top:=100;
     //Panel_1.Width:=300;
     ScrollBox_1.Height:=300;
     // ne radi Panel_1.FullRepaint:=True;
     result:=ScrollBox_1;
end;

function RC_Panel( CText: string; nProzor: TWinControl):TPanel;
// procedura koja radi panel
// 
// 
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira labela
 var
   Panel_1:TPanel;
 begin
     Panel_1:=Tpanel.create(nProzor);
     //Panel_1.align:=alNone;    // alNone, alTop, alBottom, alLeft, alRight, alClient 
     Panel_1.parent:=nProzor;
     //Panel_1.BevelInner:=bvNone;
     //Panel_1.BevelOuter:=bvRaised;
     //Panel_1.BorderStyle:=bsNone;
     //Panel_1.BorderWidth:=0;
     Panel_1.Caption:=CText;
     Panel_1.Left:=100;
     Panel_1.Top:=100;
     Panel_1.Width:=300;
     Panel_1.Height:=300;
     Panel_1.BevelInner:=bvNone;
     Panel_1.BevelOuter:=bvNone;
     Panel_1.BevelWidth:=1;
     Panel_1.BorderStyle:=bsSingle;
     Panel_1.BorderWidth:=3;
     Panel_1.Ctl3D:=False;
     //PanelP1.Font.style:=[fsBold];
     Panel_1.Font.Name:='Arial';
     Panel_1.Font.Size:=10;
     // ne radi Panel_1.FullRepaint:=True;
     result:=Panel_1;
end;



function RC_label( nText: string; nDestination: TWinControl; nX, nY:integer; Align:String):TLabel;
// make label on nDestination 
// example: 
// txt1:=RC_label('This is label', Window1, 100, 200, 'right')
// nText = Caption
// nDestination = parent of label (window, panel...)
// nX = distance from left edge
// nY = distance from top
// nAlign = pAlignment (left/right)
 var
   Text_1:TLabel;
 begin
     Text_1:=TLabel.create(nDestination);
     Text_1.parent:=nDestination;
     Text_1.caption:=nText; 
     Text_1.font.name:='arial';
     Text_1.font.size:=10;
     if Align='right' then Text_1.left:=nX-text_1.width
                           else Text_1.left:=nX;
     Text_1.top:=nY;
     result:=Text_1;
 end; 

 
 Function RC_Edit(cap, units: string; destination: TWinControl; eX, eY: integer; kind:string):TEdit;
// create field for entering data (TEdit) and caption
// units is text next to edit field
// placement of caption depend of kind string
// 'left' - caption is left of the field, origin is on the left side of the field
// 'right' - caption is right of the field, origin is on the right side of the field
// 'top-left' - caption is under field. left aligned
// 'top-right' - caption is under field. right aligned
// cap - caption
// destination - parent
// eX, eY - position
// eg.
// input:=RC_Edit('Insert Your name: ','mm', Panel2, 100, 20, 'left');
 var
   Txt1,u2:TLabel;
   ce1:TEdit;
 begin
     
     if cap<>'' then 
        begin             
            Txt1:=TLabel.create(destination);
            Txt1.parent:=destination;
            Txt1.font.style:=[];
            Txt1.font.name:='Arial';
            Txt1.font.size:=10;
            Txt1.caption:=cap;
        End;
     ce1:=TEdit.create(destination);
     ce1.parent:=destination;
     ce1.font.name:='Arial';
     ce1.font.size:=10;
     ce1.width:=50;
     ce1.showhint:=true;
     
     u2:=TLabel.create(destination);
     u2.parent:=destination;
     u2.font.style:=[];
     u2.font.name:='Arial';
     u2.font.size:=10;
     u2.caption:=units;
     
     Case kind of 
          'left'     : begin
                         txt1.left:=eX-5-txt1.width;
                         txt1.top:=eY+3;
                         txt1.alignment:=taRightJustify;
                         ce1.left:=eX;
                         ce1.top:=eY;
                         u2.left:=ce1.left+5;
                         u2.top:=eY;
                       end;
          'right'    : Begin
                         u2.left:=eX;
                         u2.top:=eY+3;
                         u2.alignment:=taRightJustify;
                         ce1.left:=ex-ce1.width-u2.width;
                         ce1.top:=eY;
                         txt1.alignment:=taLeftJustify;
                         txt1.left:=eX-ce1.width-txt1.width-5-u2.width;
                         txt1.top:=eY+3;
                         {
                         ce1.left:=ex-ce1.width;
                         ce1.top:=eY;
                         txt1.alignment:=taLeftJustify;
                         txt1.left:=eX-ce1.width-txt1.width-5;
                         txt1.top:=eY+3;
                         }
                         
                       end;
          'top-left' : Begin 
                         txt1.left:=eX;
                         txt1.top:=eY;                         
                         txt1.alignment:=taLeftJustify;
                         ce1.left:=ex;
                         ce1.top:=eY+20;
                         u2.left:=eX+ce1.width+5;
                         u2.top:=eY+3+20;
                         u2.alignment:=taLeftJustify;
                       end;            
          'top-right': Begin
                         txt1.left:=eX;
                         txt1.alignment:=taRightJustify;
                         txt1.top:=eY;                  
                         u2.left:=eX;
                         u2.alignment:=taLeftJustify;
                         u2.top:=eY+3+20;
                         ce1.left:=eX-u2.width-5;
                         ce1.top:=eY+20;
                       End;
     end;
     result:=ce1;
 end;

Procedure Check_Int_Pos(sender:Tobject; var Key: Char); 
// allow only integer and positive when typing
// var
  //// OldColor: TColor;
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key=#8)) then key:=#0;             

end; 

Procedure Check_Int(sender:Tobject; var Key: Char); 
// allow only integer typing
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
          
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key='-') or
             (key=#45) or  (key=#8)) then key:=#0;              // 45=dash (minus), 8=backspace
end; 

Procedure Check_Real(sender:Tobject; var Key: Char); 
// allow only real number when typing
begin
     // Showmessage ('Edit_Fields_OnKeyPress! Key is: '+ key);
          
     if not ((key='0') or (key='1') or (key='2') or 
             (key='3') or (key='4') or (key='5') or
             (key='6') or (key='7') or (key='8') or
             (key='9') or (key='.') or                          
             (key='-') or (key=#8)) then key:=#0;              // 8=backspace
 end; 

Procedure Check_Str_Prg(sender:Tobject; var Key: Char); 
// allow only international alphabet and numbers when typing (and some dummy proof characters)

Var
  Valid_Char:set of char;
Begin
  Valid_Char:=[ '0','1','2','3','4','5','6','7','8','9',
                'a','b','c','d','e','f','g','h','i','j','k','l','m',
                'n','o','p','q','r','s','t','u','v','w','x','y','z',
                'A','B','C','D','E','F','G','H','I','J','K','L','M',
                'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                '!','#','$','%','&','(',')','=','+','-','@','�',' ','.','_',#8];
  if not (key in Valid_Char) then key:=#0;

end; 

   // ED_twf.OnKeyPress:=@Check_Str_Prg;
   // ED_twf.OnKeyPress:=@Check_Str_Eng;
   // ED_twf.OnKeyPress:=@Check_Str_Cro;  
 
 
 
function RadioBox(nTekst: string; nazivi:Array of String; def:integer; 
                          nProzor: TWinControl; nX, nY: integer):TRadioGroup;
// kreira Radio Grupu
// nTekst = Naslov Radio Grupe
// nazivi = niz koji odre�uje check boxove ['prvi','drugi', ...]
// def = defaultni check
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira edit i labela
// nX = udaljenost od lijevog ruba
// nY = udaljenost od vrha

 
 var
     RB_1:TRadioGroup;
     I:Integer;
 begin
     RB_1:=TRadioGroup.create(nProzor);
     RB_1.parent:=nProzor;
     RB_1.caption:=' '+nTekst+' ';
     For i:=0 to GetArrayLength(nazivi)-1 do 
         begin
              RB_1.Items.Add(nazivi[i]);
         end;
     If def>RB_1.Items.Count-1 then def:=RB_1.Items.Count-1;
     RB_1.Height:=10+RB_1.Items.Count*20;
     RB_1.ItemIndex:=def;
     RB_1.Left:=nX;
     RB_1.Top:=nY;
     result:=RB_1;
 end;

// RC_Image_Edit_Create
// Create image on position left, top 
// On the right side of image creat one, two or three edit fields
// Every edit field have label on right side 
// Start_tag is three digits integer (e.g. 701)
// Start tag is first number in array that uniquelly identify edit field for change event.   

Procedure RC_Image_Edit_Create    (
                  	destination         :TWinControl;  
                  	image_name          :string;      
                  	xx                  :integer;
                  	yy                  :integer;
			image_width         :integer;
                  	image_height        :integer;
			start_tag           :integer;
                  	quantity            :integer;
                        field_1_label	    :string;
			field_1_hint	    :string;
			field_2_label	    :string;
			field_2_hint	    :string;
                        field_3_label       :string;
			field_3_hint	    :string;
			units               :string
			);

var
   i  :integer;

   ced:array[1..3] of TEdit;
Begin
   
   RC_LoadImage (image_name,    // image_path
              destination,  // parent
              xx,           // left
              yy,           // top
              image_width,  // image_width
              image_height  // image_height
              );
   
   for i:=1 to quantity do begin
         if i=1 Then ced[i]:=RC_Edit(field_1_label, units, destination, xx+image_width+10, yy+(i-1)*50, 'top-left');
         if i=2 Then ced[i]:=RC_Edit(field_2_label, units, destination, xx+image_width+10, yy+(i-1)*50, 'top-left');
         if i=3 Then ced[i]:=RC_Edit(field_3_label, units, destination, xx+image_width+10, yy+(i-1)*50, 'top-left');
         ced[i].tag:=start_tag-1+i;
         case i of
              1   :  ced[i].hint:=field_1_hint;
              2   :  ced[i].hint:=field_2_hint;
              3   :  ced[i].hint:=field_3_hint;
         end;  // case
   end;   // for i        
end;    // proc

			

// RC_Radio_Image_Create
// First, create rectangular shape with caption. 
// Then, inside shape cretae pictures with captions. 
// Start_tag is three digits integer (e.g. 701)
// Start tag is first number in array that uniquelly identify pictures for click event.   
// A pair of true and false picture has same tag
// 'ON' image will be placed ower the 'OFF' image
// Visible property of 'ON' image mark if image is selected or not
// Finall image names structure:   tag + on/of + 'name' + '.jpg'
// Image descruptions are separated with '|' sign

Procedure RC_Radio_Image_Create  (
                  destination        :TWinControl;  
                  caption            :string;
                  start_tag          :integer;
                  quantity           :integer;
                  default            :integer;
                  left               :integer;
                  top                :integer;
                  width              :integer;
                  image_width        :integer;
                  image_height       :integer;
                  image_names        :string;                // tag+on/of+'name'+'.jpg'
                  image_descriptions :string;                // separated with '|' sign
                  script_path        :string
                  );     

var
   image_path_off, image_path_on :string;
   Box:TShape;
   h, Img_label:Tlabel; 
   img_on, img_off: TMimage;
   img_caption: Array of string; 
   i, ind,min_distance, x_number, x_distance,row,number_of_rows:Integer;
   Show_hint:boolean;     // showing tag in hint for testing 
   xx,yy:Integer;

Begin
    
    // Showmessage('RC_Radio_Image_Create');
    
    Show_hint:=false;
    
    // creating box 
    Box:=TShape.create(destination);
    Box.parent:=destination;
    Box.shape:=stRectangle;
    Box.Left:=left;
    Box.Top:=top;
    Box.Width:=width;
    Box.Brush.color:=clBtnFace; 
    H:= TLabel.create(destination);
    H.parent:=destination;
    H.Font.Name:='Arial';
    H.Font.Size:=10;
    H.caption:=' '+caption+' ';   
    H.Left:=left+10;
    H.Top:=top-10;
    
    // Showmessage('Box created');
    
    // unpack captions from image_descriptions which contain strings separated with '|' sign
    SetArrayLength(img_caption, quantity+1); 
    ind:=1;
    Img_caption[ind]:='';
    
    // if Start_Tag>700 then Showmessage ('Looking for Radio Image caption. Tag: '+IntToStr(Start_tag));
    
    for i:=1 to Length(image_descriptions) do begin
        try 
            if image_descriptions[i] <> '|' then Img_caption[ind]:=Img_caption[ind]+image_descriptions[i]    // add one string from description
                                        else ind:=ind+1;
        except
            showmessage( 'Wrong size of images descriptions for images '+
                         IntToStr(start_tag)+' to '+ IntToStr(start_tag+quantity-1)+
                         ' or wrong description passed!');
            break;
        end;
        // Showmessage('Images start tag: '+IntToStr(start_tag)+' Index: '+IntToStr(ind)+' '+Img_caption[ind]);
    end;
    
    
    
    min_distance:=10;  // minimum distance between two images (and borders)
    
    // calculate possible number of images for one row
    try
        x_number:=round(( (width-(quantity+1)*min_distance))/(image_width));
        if x_number>quantity then x_number:=quantity;
    except
        x_number:=1;
         showmessage( '1. Division with zero!'+#10+#13+
                      'Images start tag: '+IntToStr(start_tag)+#10+#13+
                      'number of images for one row (x_number): '+IntToStr(x_number)+#10+#13+
                      'Image width: '+IntToStr(Image_width)+#10+#13+
                      'Box Width: '+IntToStr(width)); 
    end;
    

    
    // calculate distance between two images (and borders)
    x_distance:=round( (width-(x_number*image_width))/(x_number+1));
    
               
  {  showmessage( 'Images start tag: '+IntToStr(start_tag)+#10+#13+
                  'Image width: '+IntToStr(Image_width)+#10+#13+
                  'Box Width: '+IntToStr(width)+#10+#13+
                  'number of images for one row (x_number): '+IntToStr(x_number)+#10+#13+
                  'x distance: '+IntToStr(x_distance));                                  }
    try            
       number_of_rows:=round(quantity/x_number)+1;
    except
       number_of_rows:=1;
         showmessage( '2. Division with zero!'+#10+#13+
                      'Images start tag: '+IntToStr(start_tag)+#10+#13+
                      'number of images for one row (x_number): '+IntToStr(x_number)+#10+#13+
                      'Image width: '+IntToStr(Image_width)+#10+#13+
                      'Box Width: '+IntToStr(width)); 
    end;            

    // if Start_Tag>700 then Showmessage ('Creating Images for Radio Image will start. Tag: '+IntToStr(Start_tag));

    // creating images and captions
    
    // Showmessage('Loading Images start tag: '+IntToStr(start_tag));
    
    ind:=0;                                    // image tag
    for row:=1 to number_of_rows do begin
      for i:=1 to x_number do begin            // possition in row
        ind:=ind+1;
        xx:=left+x_distance+(x_distance+image_width)*(i-1);
        yy:=top+5+min_distance*row+(image_height+min_distance+15)*(row-1)

        // create 'off' image
        image_path_off:=script_path+IntToStr(start_tag+ind-1)+'_of_'+image_names+'.jpg';
        // showmessage('Start tag: '+IntToStr(Start_tag)+' row: '+IntToStr(row)+' i: '+IntToStr(i)+' '+image_path_off);
        Img_off:=RC_LoadImage (    image_path_off,    // image_path
                                destination,                              // parent
                                xx,                                       // left
                                yy,                                       // top
                                image_width,                              // image_width
                                image_height                              // image_height
                                );
        Img_off.name:='i'+IntToStr(start_tag+ind-1)+'of'+image_names;
        Img_off.visible:=true;
        Img_off.tag:=start_tag+ind-1;
        Img_off.hint:=IntToStr(Img_off.tag)+' '+Img_off.name+'   - for testing only! dex_unit: Show_hint:=true';
        if Show_hint=true then Img_off.showhint:=true else Img_off.showhint:=false;
        
        
         
        //create 'on' image
        image_path_on:=script_path+IntToStr(start_tag+ind-1)+'_on_'+image_names+'.jpg';
        // showmessage('Start tag: '+IntToStr(Start_tag)+' row: '+IntToStr(row)+' i: '+IntToStr(i)+' '+image_path_on);
        Img_on:=RC_LoadImage (     image_path_on,    // image_path
                                destination,                              // parent
                                xx,                                       // left
                                yy,                                       // top
                                image_width,                              // image_width
                                image_height                              // image_height
                                );
        Img_on.name:='i'+IntToStr(start_tag+ind-1)+'on'+image_names;
        If ind=default then Img_on.visible:=true 
                   else Img_on.visible:=false;
        Img_on.tag:=start_tag+ind-1;         
        Img_on.hint:=IntToStr(Img_on.tag)+' '+Img_on.name+'   - for testing only! dex_unit: Show_hint:=true';
        if Show_hint=true then Img_on.showhint:=true else Img_on.showhint:=false;


        
        Img_label:=RC_label(
                             Img_caption[ind],
                             destination,
                             left+x_distance+(x_distance+image_width)*(i-1),
                             top+min_distance*row+(image_height+min_distance+15)*(row-1)+image_height+5,
                              'left');
        Img_label.Font.Name:='Arial';
        Img_label.Font.Size:=8;

        if ind=quantity then break;             // when quantity reached exit from for loop
      end; // for i
      if ind=quantity then break;               // when quantity reached exit from for loop
    end;   // for row
    
    // if Start_Tag>700 then Showmessage ('Creating Images for Radio Image Ended! Tag: '+IntToStr(Start_tag));
    
    Box.Height:=min_distance*(row+1)+(image_height+20)*(row)+3;

End;
 
Procedure RC_Radio_Image_Change(  
                                 destination     : TWinControl;
                                 //image_name      : string;
                                 start_tag       : integer;
                                 end_tag         : integer;
                                 selected_tag    : integer
                                );
                                
// This procedure process Radio_Image 'object'
// 
// 1. Create array for manipulation with imaged
// 2. Find all images on destination object: if their name star with "i"   (added when created Radio Image 'object')
//                                            if their name have ON state
// 1b. Put these images to array
// 



var
   i,ind,AL:integer;
   // real_image_name_on:string;
   images:array of TMImage;  // array for saving image objects of destination
   // Image_on:boolean;
Begin
  { 
    showmessage ( 'Radio Image Change'+#13+#10+#13+#10+
                 'Destination: '+ destination.controls[i].name+#13+#10+
                 'Start_tag: '+IntToStr(start_tag)+#13+#10+
                 'End_tag: '+IntToStr(end_tag)+#13+#10+
                 'Selected_tag: '+IntToStr(selected_tag) 
                 );
   }
   
   
   
   
   // looking for images and saving in array
   AL:= End_tag-Start_Tag+2
   SetArrayLength(images,AL);
   ind:=0;
   // showmessage ('Array Length is '+IntToStr(AL));
   for i:=0 to destination.controlcount-1 do begin        // count all objects in destination
   
      // showmessage ('Object number '+IntToStr(i)+'/'+IntToStr(destination.controlcount-1)+' is '+destination.controls[i].name);
   
      if (destination.controls[i] is TMImage)          and        // is it image?  
         (copy(destination.controls[i].name,1,1) ='i') and        // is it first char in name 'i'?  (all taged images names start with 'i')
         (copy(destination.controls[i].name,5,2) = 'on') and      // is it tag_ON_name image?
         (destination.controls[i].tag <= End_Tag)        and     // is it tag less then end tag?
         (destination.controls[i].tag >= Start_Tag)             // is it tag greater then start tag?
              then begin ind:=ind+1;
                         images[ind]:= TMImage(destination.controls[i]);

                         if ind>End_tag-Start_Tag+1 then showmessage ('To many images found in "'+destination.controls[i].name+'"!');
              end else begin

              
              end;
   end; // for i

   for i:=1 to Ind do begin
       {
       showmessage ('Changing visibility '+#13+#10+ 
                    'Name: '+images[i].name+#13+#10+
                    'Original tag: '+ IntToStr(images[i].tag)+#13+#10+
                    // 'Recalculated tag: '+ IntToStr(images[i].tag+start_tag-1)+#13+#10+
                    'Selected_tag: '+IntToStr(selected_tag));
       }
       if images[i].tag=selected_tag then images[i].visible:=true
                                     else images[i].visible:=false; 
   end;
   



End;

 

 
 
function RC_CheckBox(nText: string; nParent: TWinControl; nX, nY: integer):TCheckBox;
// create CheckBox
// nText = CheckBox text
// nParent = parent of CheckBox
// nX = Left
// nY = Top
 var
     CBox_1:TCheckBox;
 begin
     CBox_1:=TCheckBox.create(nParent);
     CBox_1.parent:=nParent;
     CBox_1.width:=300;
     CBox_1.caption:=nText;
     CBox_1.Left:=nX;
     CBox_1.Top:=nY;
     result:=CBox_1;
 end;

function RC_Button(nText: string; nParent: TWinControl; nX, nY: integer):TButton;
// Create Button
// nText = Button Caption
// nParent = name fo parent object (Window, Panel...)
// nX = Left
// nY = Top
 var
   Button_1:TButton;
 begin
     Button_1:=TButton.create(nParent);
     Button_1.parent:=nParent;
     Button_1.caption:=nText;
     Button_1.Left:=nX;
     Button_1.Top:=nY;
     Button_1.Width:=180;
     Button_1.Height:=25
     Button_1.ShowHint:=true;
     Button_1.Font.Name:='Arial';
     Button_1.Font.Size:=9;
     result:=Button_1;
 end;
 

 
 function Memo(nProzor: TWinControl; nX, nY, nS, nV: integer):TMemo;
// kreira memo
// 
// nProzor = naziv prozora (ili ne�ega drugoga) u kojem se kreira memo
// nX = udaljenost od lijevog ruba
// nY = udaljenost od vrha
// nS = �irina
// nV = visina

 var
   Memo_1:TMemo;
 begin
     Memo_1:=TMemo.create(nProzor);
     Memo_1.parent:=nProzor;

     Memo_1.Left:=nX;
     Memo_1.Top:=nY;
     Memo_1.Width:=nS;
     Memo_1.Height:=nV;
     result:=Memo_1;

 end;
 
function find_value_int(FileName,ValueName:string):integer;
// Find property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if ValueName not found or result is not integer, return 0 (zero)
// if ValueName found, but without value, ValueName will be deleted, and file will be saved
var
   list:TStringList;
   ind:integer;
   Value,ResultValue:string;
Begin
   list:=TStringList.Create;
   try list.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_int'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   value:=list.Values[ValueName];          // BUT, in this case value will be ''
   if (ind>=0) and (value='') then begin                      //  Name exist but no value
                List.Delete(List.IndexOfName(ValueName));     // delete name from list
                list.SaveToFile(FileName);                    // save new list to file
                // search again!!
                ind:=List.IndexOfName(ValueName);             // search again :-)
                value:=list.Values[ValueName];
   end; // if 
   if (ind>=0) and (not(value='')) then ResultValue:=value
                                   else begin  
                                      ShowMessage( 'Function: Find_value_int'+#13#10+'Cannot find name "'+ ValueName+'" in file "'+FileName+'"');
                                      ResultValue:='0';
                                   end;
   // try to convert to integer   
   try 
       Result:=StrToInt(ResultValue); 
   except 
       Result:=0; 
       Showmessage( 'Value "'+ValueName+'" in file "'+FileName+'" cannot be converted to integer number!'+#10#13+'Assigning zero');   
   end;

      
end;   // function find_value_int 
 
function find_value_str(FileName,ValueName:string):string;
// Find property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if ValueName not found return ' '
// if ValueName found, but without value, ValueName will be deleted, and file will be saved
var
   list:TStringList;
   ind:integer;
   Value,ResultValue:string;
Begin
   list:=TStringList.Create;
   try list.LoadFromFile(FileName);
     except ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find file"'+ FileName+'"!');
   end; 
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   value:=list.Values[ValueName];          // BUT, in this case value will be ''
   if (ind>=0) and (value='') then begin                      //  Name exist but no value
                List.Delete(List.IndexOfName(ValueName));     // delete name from list
                list.SaveToFile(FileName);                    // save new list to file
                // search again!!
                ind:=List.IndexOfName(ValueName);             // search again :-)
                value:=list.Values[ValueName];
   end; // if 
   if (ind>=0) and (not(value='')) then ResultValue:=value
                                   else begin  
                                      // ShowMessage( 'Function: Find_value_str'+#13#10+'Cannot find name "'+ ValueName+'" in file "'+FileName+'"');
                                      ResultValue:=' ';
                                   end;



   Result:=ResultValue; 
      
end;   // function find_value_int 

function set_value_str(FileName,ValueName, Value :string):boolean;
// Set property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if not succed return false

var
   list:TStringList;
   ind:integer;

Begin
   list:=TStringList.Create;
   result:=true;
   try begin 
            list.LoadFromFile(FileName);
            ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
            if ind =-1 then list.Add(ValueName+'='+Value) else list[ind]:=ValueName+'='+Value;
            list.SaveToFile(FileName);
       end; 
   except begin 
               ShowMessage( 'Function: Set_value_str'+#13#10+'Error load/save file "'+ FileName+'"!');
               result:=False;
          end;
   end;   // try

      
end;   // function set_value_str 

function set_value_int(FileName,ValueName :string; Value:Integer):boolean;
// Set property value in external file
// FileName  = external file (with path)
// ValueName = variable name
// if not succed return false

var
   list:TStringList;
   ind:integer;
Begin
   list:=TStringList.Create;
   result:=true;
   try begin 
            list.LoadFromFile(FileName);
            ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
            if ind =-1 then list.Add(ValueName+'='+IntToStr(Value)) else list[ind]:=ValueName+'='+IntToStr(Value);
            list.SaveToFile(FileName);
       end; 
   except begin 
               ShowMessage( 'Function: Set_value_str'+#13#10+'Error load/save file "'+ FileName+'"!');
               result:=False;
          end;
   end;   // try

end;   // function set_value_int 

Procedure message(mess:string);

Begin
   try
      writeln(mess);
   except
      // i have no idea what to do!
   end;
   
end;
  
Procedure progress_create(nProzor: TWinControl);
// var
   // vars must be declared in main program
   // progress_panel,bar_out, bar_in:TPanel;
   // progress_text:TLabel;
Begin
   message('Procedure progress_create(nProzor: TWinControl)');
   Progress_panel:=RC_Panel( '',  nProzor);
   Progress_panel.width:=650;
   Progress_panel.height:=60;
   Progress_panel.left:=round((nProzor.width-Progress_panel.width)/2);
   Progress_panel.top:=200;
   
   bar_out:=RC_Panel( '',  Progress_panel);
   bar_out.width:=Progress_panel.width-30;
   bar_out.height:=20;
   bar_out.left:=round((Progress_panel.width-bar_out.width)/2);
   bar_out.top:=10;              
   
   bar_in:=RC_Panel( '',  Progress_panel);
   bar_in.width:=bar_out.width-4;
   bar_in.height:=16;
   bar_in.left:=bar_out.left+2;
   bar_in.top:=bar_out.top+2;
   bar_in.color:=clRed;
   

   progress_text:=RC_label( 'Progress bar', Progress_panel, 10,10, 'left');
   
   progress_text.left:=round((Progress_panel.width-progress_text.width)/2);
   progress_text.top:=35;
   
   Progress_panel.visible:=false;
   message('done: Procedure progress_create(nProzor: TWinControl)');                                      
End;    

Procedure progress_change(pb_perc:Integer;pb_mess:string);
var
   full_width, bar_width:integer;
Begin

   if pb_perc =100 then Progress_panel.visible:=false
                   else Progress_panel.visible:=true; 
   full_width:=bar_out.width-4;
   bar_width:=round(((pb_perc/100))*full_width);
   bar_in.width:=bar_width;
   // Application.ProcessMessages;
   progress_text.caption:=pb_mess;         
   Application.ProcessMessages;
   


end;





