Program upute;  
// v1.00 (19.08.2015)
// Skripta koja uklanja makroe

{$I ..\..\Include\rcMessages.cps}
//{$I ..\..\include\rcStrings.cps}
{$I ..\..\Include\rcControls.cps}
//{$I ..\..\include\rcObjects.cps}
//{$I ..\..\include\rcEvents.cps}
//{$I ..\..\include\rcCurves.cps}
//{$I ..\..\include\rcMath.cps}
         
var
   Prozor:TForm;
   M1:TMemo;
	sn, dato:string;
	



Begin
  // showmessage ('Upute');
  // Function rcTool(tName:string):TForm;
  Prozor:=rcTool('Red_Cat');
  Prozor.BorderStyle:=bsSizeToolWin;
  Prozor.Caption:='Corpus RC Info';
  Prozor.Left:=400;
  Prozor.Top:=400;
  Prozor.width:=200;
  Prozor.Height:=90;
  
  M1:=rcMemo('M10',Prozor,100,100,30,30);
  M1.align:=alClient; 
  M1.ScrollBars:=ssNone; // ssVertical;
  M1.ReadOnly:=True;
  M1.Font.color:=clMaroon;
  M1.Color:=clSilver;
  
   dato:=PrgFolder+'skripte\RC_Tools\SystemFile.sys';
	// Showmessage(dato);
   If FileExists(dato) then begin 
									M1.Lines.LoadFromFile(dato);
									sn:=getWibuSN;
									M1.Lines.Add('Wibu klju� broj: '+sn);
							  end else Showmessage ('Nepoznata verzija' + #13#10 + dato);
  
  prozor.showmodal;
  Prozor.free;
  
End.
