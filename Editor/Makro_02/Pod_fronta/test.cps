﻿
// Out 2******************************************
Procedure Test_dasaka;
var
	i, broj_fronti, broj_horizontalnih:integer;
	ss:string;
	td:Tdaska;
Begin
	// brojanje količine
	broj_fronti:=0;
	Broj_horizontalnih:=0;
	for i:=0 to e.childdaska.CountObj-1 do Begin  // broji daske u editoru
		td:=e.childdaska.Daska[i];   
		// tražim frontu
		if td.selected and (td.smjer in [VertFront]) then begin				// VertBok, VertFront, Horiz
			broj_fronti:=broj_fronti+1;
			daska_2:=td;
			// ShowMessage('Našao sam frontalnu broj: '+IntToStr(broj_fronti));
		end; // if td.selected
		// tražim vertikalu
		if td.selected and (td.smjer in [Horiz]) then begin				// VertBok, VertFront, Horiz
			broj_horizontalnih:=broj_horizontalnih+1;
			// ShowMessage('Našao sam vertikalu broj: '+IntToStr(broj_horizontalnih));
			daska_1:=td;
		end; //if td.selected
	end;  // for i:=0 to e.childdaska.CountObj-1 


	// provjera količine
	If (broj_fronti=1) and (Broj_horizontalnih=1) Then Begin
		// provjera kuteva
		If (Daska_1.kut<>0) or (Daska_1.xkut<>0) or (Daska_1.zkut<>0) or 
			(Daska_2.kut<>0) or (Daska_2.xkut<>0) or (Daska_2.zkut<>0) Then Begin 
					Showmessage('Neke selektirane daska su rotirane,' +#13#10+
									'a ne smije biti za ovaj spoj.' );
					ButOK.Enabled:=False;			 
			End else begin
				// kutevi su ok
				// provjera dodira
				// visina,dubina,debljina,xPos,yPos,zPos
				If (daska_1.ZPos-daska_1.dubina>daska_2.ZPos) 							// 2 je ospred 1
					 or	(daska_1.ZPos<daska_2.ZPos) 										// 2 je iza 1
					 or (daska_1.xPos+daska_1.visina<daska_2.XPos)  					// 2 je lijevo od 1
					 or (daska_1.xPos>daska_1.XPos+daska_2.Dubina)    					// 2 je desno od 1
					 or (daska_1.yPos+daska_1.debljina<>daska_2.Ypos)  				// 2 nije točno po Y
					Then begin
						Showmessage('Izgleda da se daske ne dodiruju.'+#13#10+
										'Makro se može postaviti, ali dobro provjerite što radite!');
						// Showmessage(FloatToStr(daska_1.ZPos+daska_1.debljina)+'<>'+FloatToStr(daska_2.ZPos-daska_2.dubina));
				End;  // 	If (daska_1.ZPos<>daska_2.ZPos) 
		end;	// If (Daska_1.kut<>0) 
	end else begin  // If (broj_fronti=1) ;	
		ButOK.Enabled:=False;
		Showmessage('Moraju biti selektirane dvije daske!' +#13#10+
						'Pod i fronta iznad njega');
	end;  // If (broj_fronti=1)
	
		
End;
// Out 2******************************************
