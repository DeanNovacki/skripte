
{$I ..\include\rcPrint.cps}
{$I ..\include\rcMessages.cps}
{$I ..\include\rcStrings.cps}
{$I ..\include\rcControls.cps}
{$I ..\include\rcObjects.cps}
{$I ..\include\rcConfig.cps}
{$I ..\include\rcEvents.cps}
{$I ..\include\rcCurves.cps}
{$I ..\include\rcMath.cps}
{$I ..\include\rcOut.cps}


var
   	
	eForm:tForm; // prozor editora
	
   put_skript:string;
	selektirani_objekti:TStringList;
	Das:TDaska;
	Elem:TElement;
	puta:String;
	
	lista_elementi:array of TElement;
	lista_daske:array of TDaska;
	

	sForm:tForm; // prozor skripte
		SemSlik:TmImage;				// kad je forma minimizirana;
		
		panel_caption: TPAnel;		// najgornji za pomicanje i naslov
			xSlik:TmImage;				// za minimiziranje
		
		panel_id:TPanel;				// koli�ina i nazivi selektiranih
			label_broj :TLabel;
				broj_el, broj_das:integer;
			but_podesi:TSpeedButton;
			but_prop:TSpeedButton;
			but_var:TSpeedButton;
		
		panel_setup:TScrollBox;
			SetNaziv:TCheckBox;
			SetSifra:TCheckBox;
			SetX:TCheckBox;
			SetFx:TCheckBox;
			SetY:TCheckBox;
			SetFy:TCheckBox;
			SetZ:TCheckBox;
			SetFz:TCheckBox;
			SetSirina:TCheckBox;
			SetVisina:TCheckBox;
			SetDebljina:TCheckBox;
			SetDubina:TCheckBox;
			SetPrimjedba:TCheckBox;
			SetNapomena:TCheckBox;
			SetProgram:TCheckBox;
			SetJournal:TCheckBox;
		panel_main2:TScrollBox;	

			panel_naziv : Tpanel;
				label_naziv :TLabel;
				edit_naziv:TEdit;
				OK_naziv:TSpeedButton;
				
			panel_sifra : Tpanel;
				label_sifra :TLabel;
				edit_sifra:TEdit;
				OK_sifra:TSpeedButton;	
			
			panel_x : Tpanel;
				label_x :TLabel;
				edit_x:TEdit;
				OK_x:TSpeedButton;
				
			panel_fx : Tpanel;
				label_fx :TLabel;
				edit_fx:TEdit;
				OK_fx:TSpeedButton;
				
							
			panel_y : Tpanel;
				label_y :TLabel;
				edit_y,edit_yf:TEdit;
				OK_y:TSpeedButton;				
				
			panel_fy : Tpanel;
				label_fy :TLabel;
				edit_fy:TEdit;
				OK_fy:TSpeedButton;
				
			panel_fz : Tpanel;
				label_fz :TLabel;
				edit_fz:TEdit;	
				OK_fz:TSpeedButton;				
				
			panel_z : Tpanel;
				label_z :TLabel;
				edit_z,edit_zf:TEdit;	
				OK_z:TSpeedButton;
				
			panel_sirina : Tpanel;
				label_sirina :TLabel;
				edit_sirina:TEdit;
				OK_sirina:TSpeedButton;

			panel_visina : Tpanel;
				label_visina :TLabel;
				edit_visina:TEdit;
				OK_visina:TSpeedButton;

			panel_dubina : Tpanel;
				label_dubina :TLabel;
				edit_dubina:TEdit;
				OK_dubina:TSpeedButton;
				
			panel_debljina : Tpanel;
				label_debljina :TLabel;
				edit_debljina :TEdit;
				OK_debljina:TSpeedButton;
				
			panel_primjedba : Tpanel;
				label_primjedba:TLabel;
				edit_primjedba :TEdit;
				OK_primjedba:TSpeedButton;
				
			panel_napomena : Tpanel;
				label_napomena :TLabel;
				edit_napomena :TEdit;
				OK_napomena:TSpeedButton;
			panel_program : Tpanel;
				label_program :TLabel;
				edit_program :TEdit;
				OK_program:TSpeedButton;
		

			
			


		
		panel_bottom: TPAnel;					// na dnu
			image_resize:TmImage;				// za resize
			image_up:TmImage;				// za resize
			image_down:TmImage;				// za resize

	but_x:TSpeedButton;

	sredina, velicina:integer;			
	
	
   sX, sY:Integer; // po�etna to�ka za pomicanje otvorenog semafora mi�em

{
function isDaskaOk(Param1: tdaska): boolean;   // izbaciti
begin
	// MemoD.Lines.Add('Param1: '+Param1.naziv );
  RCOutP('function isDaskaOk. Param1: '+Param1.naziv, eForm); 
  Result := Param1.naziv[1]='P';
end;
}


Procedure poslozi;
// poslo�i sve komponente nakon resizeanja 
Begin
	if sForm.Width<20 then begin 
		// mala slikica
		panel_Main2.visible:=false;
		panel_id.visible:=false;
		panel_Caption.visible:=false;
		panel_bottom.visible:=false;
		semslik.visible:=True;
		// read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_mali');
	end else begin
		// velika slika
		panel_Main2.visible:=True;
		panel_Caption.visible:=True;
		panel_id.visible:=True;
		panel_bottom.visible:=True;
		semslik.visible:=False;
		// read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki');
	end;
end;

procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); 
// radi na mouse down unutar prozora, ali ne i captiona    
begin
     RCOutP('Mouse down',eForm);  
	  // action:=cafree;
end;

procedure zapamti_pozicije;

Begin
	RCOutP('Zapamti pozicije START',eForm);
	// spremanje pozicije i veli�ine semafora
	// u semafor.ini �e i�i i neke druge stvari
	puta:=PrgFolder+'skripte\RC_Tools\Semafor\Semafor';
	save_form(sForm,puta);
	
	if sForm.Width<20 then begin 
			// spremanje pozicije i veli�ine semafora kad je minimiziran
			puta:=PrgFolder+'skripte\RC_Tools\Semafor\Semafor_mali';		
			save_form(sForm,puta)
		end else begin
			// spremanje pozicije i veli�ine semafora kad je normalan
			puta:=PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki';		
			save_form(sForm,puta);
	End;
	
	// spremanje pozicije journala
	if rc_TF<>nil then begin
			puta:=PrgFolder+'skripte\RC_Tools\Semafor\Journal';
			save_form(rc_TF,puta);
			// Showmessage('ZAPAMTIO SAM VISINU: "'+#13#10+IntToStr(rc_TF.Height));
	end;
	RCOutP('Zapamti pozicije END',eForm);
	
	{
	save_form(sForm,PrgFolder+'skripte\RC_Tools\Semafor\Semafor');
	if sForm.Width<20 then save_form(sForm,PrgFolder+'skripte\RC_Tools\Semafor\Semafor_mali')
							 	else save_form(sForm,PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki');
	// save_form(eForm,'Editor');   	// Editor
	if rc_TF<>nil then save_form(rc_TF,PrgFolder+'skripte\RC_Tools\Include\Journal');	// Journal
	RCOutP('Procedure ZAPAMTI POZICIJE End',eForm);
	}
End;



procedure SemSLikMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
// Ovo pomi�e prozor skripte pomo�u mi�a
begin
	// RCOutP('Semafor x: '+IntToStr(x)+' y: '+IntToStr(x),eForm);
	Timage(Sender).Cursor :=crSizeAll;
	if ssleft in shift then begin 
				// RCOutP('Klik',eForm);
				if x>12 then sForm.Left:=sForm.Left+4;
				if x<4  then sForm.Left:=sForm.Left-4;
				if y>20 then sForm.Top:=sForm.Top+4;
				if y<12 then sForm.Top:=sForm.Top-4;
				
	end;
	 if ssDouble in shift then RCOutP('Klik Klik',eForm);
	 if ssRight in shift then RCOutP('Desni Klik',eForm);
end;

procedure SemSlikMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
// OTVARANJE
// Ovo detektira dvostruki klik na sliku semafora
// treba staviti formu skripte u punu veli�inu
begin
	// if ssleft in shift then RCOutP('Left Down',eForm);
				
	if ssDouble in shift then begin 
				RCOutP('Otvaram prozor skripte',eForm);
				save_form(sForm,PrgFolder+'skripte\RC_Tools\Semafor\Semafor_mali');
				read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki'); 				
	end;
	poslozi;
end;

procedure XSlikMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
// 
begin
	// RCOutP('X move over',eForm);
	Timage(Sender).Cursor :=crArrow;
	// Timage(Sender).Cursor :=crNoDrop;

end;

procedure xSlikMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
// ZATVARANJE
// Ovo detektira dvostruki klik na sliku X-a (zatvaranja prozora)
// treba staviti formu skripte na minimalnu veli�inu
begin
	// if ssleft in shift then RCOutP('Left Down',eForm);
				
	if ssLeft in shift then begin 
				RCOutP('Zatvaram prozor skripte',eForm);
				save_form(sForm,PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki');
				read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_mali'); 
				sForm.Width:=16;
				sForm.Height:=32;
	end;
	poslozi;
end;

procedure panel_captionMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
// POMICANJE OTVORENOG SEMAFORA
begin
	TPanel(Sender).Cursor :=crSizeAll;
	//RCOutP('moove',eForm);
	if ssleft in shift then begin 
				if x>sX+4 then sForm.Left:=sForm.Left+4;
				if x<sX-4 then sForm.Left:=sForm.Left-4;
				if y>sY+4 then sForm.Top:=sForm.Top+4;
				if y<sY-4 then sForm.Top:=sForm.Top-4;
	end;
end;

procedure panel_captionMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
// PAMTI X i Y MI�A KAO NULTU TO�KU
// zbog pomicanja
begin
	if ssLeft in shift then begin 
				RCOutP('Pamtim sX i sY',eForm);
				sX:=X;
				sY:=Y;
	end;
	if (ssDouble in shift) and (sForm.Width>20) then begin 
		// Zatvaranje prozora skripte (zapravo minimizacija, ali nema veze	
		RCOutP('Zatvaram prozor skripte',eForm);
		save_form(sForm,PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki');
		read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_mali'); 
		sForm.Width:=16;
		sForm.Height:=32;	
		poslozi;		
	end;
end;

procedure Image_resizeMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
// RESIZE forme skripte
var
	k:integer;
begin
	//k:=velicina;
	k:=8;
	Timage(Sender).Cursor :=crSizeNWSE;					// http://wiki.freepascal.org/Cursor
	if ssleft in shift then begin 
				if x>sX+4 then sForm.Width:=sForm.Width+k;
				if x<sX-4 then sForm.Width:=sForm.Width-k;
				if y>sY+4 then sForm.Height:=sForm.Height+k;
				if y<sY-4 then sForm.Height:=sForm.Height-k;
	end;
end;

procedure Image_resizeMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
// PAMTI X i Y MI�A KAO NULTU TO�KU
// zbog promjene veli�ine forme skripte
begin
	if ssLeft in shift then begin 
				RCOutP('Pamtim sX i sY',eForm);
				sX:=X;
				sY:=Y;
	end;
end;




function Panel3(naziv:string):TPanel;
begin
	result:=rcPanel(naziv,panel_main2);
	// result.name:=naziv;
	// result.caption:='';
	result.Height:=velicina;
	result.top:=10000;
	result.align:=alTop;
	result.Ctl3D:=False;
	result.BevelWidth:=0;
	result.font.name:='System';
end;

Function Label3(cap:string;par:TWinControl):TLabel;
begin
	result:=rcLabel('','',par,sredina-3,1,taRightJustify);
	result.caption:=cap+' ';
	result.font.size:=Velicina-12;
	result.width:=80;
	result.align:=alLeft;
	// result.font.style:=[fsBold];
	
		// result.Height:=20;  NE DE�AVA SE NI�TA
end;

Function Edit3(par:TWinControl):TEdit;
Begin
	// rcEditL(cap, units: string; destination: TWinControl; eX, eY: integer; kind:string):TEdit;
	result:=rcEditL('','',par,sredina,0,'left');
	// result.left:=
	result.width:=100;
	result.font.name:='Arial';
	
	result.font.size:=9;
	// result.width:=60;
	result.align:=alClient;
	result.Ctl3D:=False;
	result.Ctl3D:=False;
	
	// result.font.style:=[fsBold];
		// result.BorderWidth:=0;  NE RADI
		// result.BorderStyle:=bsNone; NE RADI
end;

Function Button3(naslov, opis: string; par:TWinControl):TSpeedButton;
Begin
	
	result:=rcSpeedButton('',naslov,par,0,0);
	// function rcSpeedButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TSpeedButton;
	result.left:=20000;
	result.align:=alRight; 
	result.hint:=opis;
	result.font.name:='Arial'; 
	result.font.size:=7; 
	// result.width:=30;
	result.width:=0;
	
end;

function CheckBox3( cap: string; cbX, cbY: integer):TCheckBox;
// function rcCheckBox(cbName, cbCaption: string; cbParent: TWinControl; cbX, cbY: integer):TCheckBox;
// Create CheckBox
 
 begin
	result:=rcCheckBox('',cap,panel_setup,cbx, cby);
	result.font.size:=8;
	result.width:=150;
	result.Alignment:=taRightJustify;

end;

procedure but_podesiOnClik(Sender: TObject);
Begin
	but_podesi.visible:=false;
	panel_setup.visible:=true;
	
	but_prop.visible:=true;
	panel_main2.visible:=false;
End;

procedure but_propOnClik(Sender: TObject);
Begin

	but_podesi.visible:=true;
	panel_setup.visible:=false;
	
	but_prop.visible:=false;
	panel_main2.visible:=true;
End;




procedure ShowHideOsobine;
Begin
	RCOutP('Procedure ShowHideOsobine START',eForm);
	
	if SetNaziv.checked 	then panel_naziv.Height:=Velicina
								else panel_naziv.Height:=0;
	if SetSifra.checked 	then panel_Sifra.Height:=Velicina
								else panel_Sifra.Height:=0;
	if SetX.checked 		then panel_X.Height:=Velicina
								else panel_X.Height:=0;							
	if SetFX.checked 		then panel_FX.Height:=Velicina
								else panel_FX.Height:=0;							
	if SetY.checked 		then panel_Y.Height:=Velicina
								else panel_Y.Height:=0;							
	if SetFY.checked 		then panel_FY.Height:=Velicina
								else panel_FY.Height:=0;		
	if SetZ.checked 		then panel_Z.Height:=Velicina
								else panel_Z.Height:=0;							
	if SetFZ.checked 		then panel_FZ.Height:=Velicina
								else panel_FZ.Height:=0;
	if SetSirina.checked then panel_Sirina.Height:=Velicina
								else panel_Sirina.Height:=0;							
	if SetVisina.checked then panel_Visina.Height:=Velicina
								else panel_Visina.Height:=0;
	if SetDebljina.checked then panel_Debljina.Height:=Velicina
								else panel_Debljina.Height:=0;
	if SetDubina.checked then panel_Dubina.Height:=Velicina
								else panel_Dubina.Height:=0;
	if SetPrimjedba.checked then panel_Primjedba.Height:=Velicina
								else panel_Primjedba.Height:=0;
	if SetNapomena.checked then panel_Napomena.Height:=Velicina
								else panel_Napomena.Height:=0;
	if SetProgram.checked then panel_Program.Height:=Velicina
								else panel_Program.Height:=0;
	
	
	if rcOut_enabled=true then Begin 
		// Showmessage('rcOut_enabled=true');
		if SetJournal.checked then begin 
				rc_TF.Height:=500;
				puta:=PrgFolder+'skripte\RC_Tools\Semafor\Journal';
				read_form(rc_TF, Puta);  // postavljanje prija�nje pozicije, dimenzija i stanja journala
				rc_TF.Visible:=true;
			end else begin 
				// rc_TF.Height:=0;
				// rc_TF.Width:=0;
				rc_TF.Visible:=false;
		End; // if rcOut_enabled=true
	End;
					
	RCOutP('Procedure ShowHideOsobine END',eForm);				
					

	
End;

Procedure SetOsobineWriteToDisk;
var
	puta:string;
Begin
	RCOutP('SetOsobineWriteToDisk START',eForm);
	puta:=PrgFolder+'skripte\RC_Tools\Semafor\semafor.ini';
	If SetNaziv.checked 	then set_value_int(puta,'SetNaziv',1)
								else set_value_int(puta,'SetNaziv',0);
	If SetSifra.checked 	then set_value_int(puta,'SetSifra',1)
								else set_value_int(puta,'SetSifra',0);
	If SetX.checked 		then set_value_int(puta,'SetX',1)
								else set_value_int(puta,'SetX',0);
	If SetFX.checked 		then set_value_int(puta,'SetFX',1)
								else set_value_int(puta,'SetFX',0);
	If SetY.checked 		then set_value_int(puta,'SetY',1)
								else set_value_int(puta,'SetY',0);
	If SetFY.checked 		then set_value_int(puta,'SetFY',1)
								else set_value_int(puta,'SetFY',0);
	If SetZ.checked 		then set_value_int(puta,'SetZ',1)
								else set_value_int(puta,'SetZ',0);
	If SetFZ.checked 		then set_value_int(puta,'SetFZ',1)
								else set_value_int(puta,'SetFZ',0);
	If SetSirina.checked then set_value_int(puta,'SetSirina',1)
								else set_value_int(puta,'SetSirina',0);
	If SetVisina.checked then set_value_int(puta,'SetVisina',1)
								else set_value_int(puta,'SetVisina',0);							
	If SetDebljina.checked 		then set_value_int(puta,'SetDebljina',1)
								else set_value_int(puta,'SetDebljina',0);
	If SetDubina.checked then set_value_int(puta,'SetDubina',1)
								else set_value_int(puta,'SetDubina',0);
	If SetPrimjedba.checked 		then set_value_int(puta,'SetPrimjedba',1)
								else set_value_int(puta,'SetPrimjedba',0);
	If SetNapomena.checked 		then set_value_int(puta,'SetNapomena',1)
								else set_value_int(puta,'SetNapomena',0);							
	If SetProgram.checked 		then set_value_int(puta,'SetProgram',1)
								else set_value_int(puta,'SetProgram',0);		
	If SetJournal.checked 		then set_value_int(puta,'SetJournal',1)
								else set_value_int(puta,'SetJournal',0);
	RCOutP('SetOsobineWriteToDisk END',eForm);
End;

Procedure SetOsobineReadFromDisk;
var
	puta:string;
Begin
	RCOutP('SetOsobineReadFromDisk START',eForm);
	puta:=PrgFolder+'skripte\RC_Tools\Semafor\semafor.ini'; 

	if find_value_int(puta,'SetNaziv') = 1 		then SetNaziv.Checked:=True
																else SetNaziv.Checked:=False;
	if find_value_int(puta,'SetSifra') = 1 		then SetSifra.Checked:=True						
																else SetSifra.Checked:=False;
	if find_value_int(puta,'SetX') = 1 				then SetX.Checked:=True
																else SetX.Checked:=False;
	if find_value_int(puta,'SetFX') = 1 			then SetFX.Checked:=True
																else SetFX.Checked:=False;
	if find_value_int(puta,'SetY') = 1 				then SetY.Checked:=True
																else SetY.Checked:=False;
	if find_value_int(puta,'SetFY') = 1 			then SetFY.Checked:=True
																else SetFY.Checked:=False;
	if find_value_int(puta,'SetZ') = 1 				then SetZ.Checked:=True
																else SetZ.Checked:=False;
	if find_value_int(puta,'SetFZ') = 1				then SetFZ.Checked:=True
																else SetFZ.Checked:=False;
	if find_value_int(puta,'SetSirina') = 1 		then SetSirina.Checked:=True
																else SetSirina.Checked:=False;
	if find_value_int(puta,'SetVisina') = 1		then SetVisina.Checked:=True
																else SetVisina.Checked:=False;
	if find_value_int(puta,'SetDebljina') = 1		then SetDebljina.Checked:=True
																else SetDebljina.Checked:=False;
	if find_value_int(puta,'SetDubina') = 1		then SetDubina.Checked:=True
																else SetDubina.Checked:=False;
	if find_value_int(puta,'SetPrimjedba') = 1	then SetPrimjedba.Checked:=True
																else SetPrimjedba.Checked:=False;
	if find_value_int(puta,'SetNapomena') = 1		then SetNapomena.Checked:=True
																else SetNapomena.Checked:=False;
	if find_value_int(puta,'SetProgram') = 1		then SetProgram.Checked:=True
																else SetProgram.Checked:=False;
	if find_value_int(puta,'SetJournal') = 1		then SetJournal.Checked:=True
																else SetJournal.Checked:=False;
	
	ShowHideOsobine;
	RCOutP('SetOsobineReadFromDisk END',eForm);
End;

Procedure set_osobineOnClick(Sender: TObject);
Begin
	RCOutP('set_osobineOnClick START',eForm);
	ShowHideOsobine;
	RCOutP('set_osobineOnClick END',eForm);
End;

Procedure Napomena_change(Sender:TObject);
// kad se klikle "OK" tipka od napomena
var
	i:Integer;
Begin
	// SAMO ZA ELEMENTE VRIJEDI
	For i:=0 to Length(lista_elementi)-1 do begin
		lista_elementi[i].napomena:=edit_napomena.text;
	End;

End;

Procedure Primjedba_change(Sender:TObject);
// kad se klikle "OK" tipka od napomena
var
	i:Integer;
Begin
	// SAMO ZA DASKE VRIJEDI
	For i:=0 to Length(lista_daske)-1 do begin
		lista_daske[i].primjedba:=edit_primjedba.text;
	End;

End;

Procedure Program_change(Sender:TObject);
// kad se klikne "OK" tipka od program
var
	i:Integer;
Begin
	// SAMO ZA DASKE VRIJEDI
	For i:=0 to Length(lista_daske)-1 do begin
		lista_daske[i].Bprogram:=edit_program.text;
	End;

End;


procedure FormCloseQuery(Sender: TObject; var CanClose: boolean); 
// aktivira se netom prije zatvaranja prozora, a mo�da i ne :-)   
begin
	RCOutP('Samo �to nisam zatvorio formu!',eForm);  
	Showmessage('Samo �to nisam zatvorio!');  
	zapamti_pozicije;
	SetOsobineWriteToDisk;
end;

procedure EditorCloseQuery(Sender: TObject; var CanClose: boolean); 
// aktivira se netom prije zatvaranja EDITORA
begin
     RCOutP('Samo �to nisam zatvorio EDITOR!',eForm);  
	  // Showmessage('Samo �to nisam zatvorio EDITOR!');  
	  zapamti_pozicije;
	  SetOsobineWriteToDisk;
	  // action:=cafree;
end;


Procedure NapraviFormu;
var
	xo:TObject;
	xM1:TMemo;
begin
  RCOutP('NapraviFormu START', eForm);
  Showmessage('NapraviFormu START');
  // kreiranje forme skripte	
	sForm:=tform.create(eForm);
	RCOutP('NapraviFormu 1', eForm);
	sForm.parent:=eForm;
	RCOutP('NapraviFormu 2', eForm);
	sForm.align:=alNone;
	sForm.caption:='Semafor';
	// RCOut('Skriptina forma je kreirana i ima naziv "'+sForm.caption+'"');    
	// RCOut('Kre�em u�itavati ini za semafor ');    
	// RCOut('ini za semafor gotov');    
	sForm.BorderStyle:=bsNone; // 
	sForm.font.name:='Arial';
	// sForm.BorderStyle:=bsSizeable;
	// sForm.Icon:=fdsaf;    NE RADI
	// sForm.BorderWidth:=1;  NE RADI
	// sForm.BorderIcons:=[biSystemMenu];
	sForm.OnCloseQuery:=@FormCloseQuery;
	//sForm.onMouseMove:=@sFormMouseMove;
	// Showmessage ('4');

	// Journal data:
	RCOutP('Forma Semafor je postavljena. Ima naslov: '+sForm.caption,eForm);
	RCOutP('Left: '+IntToStr(sForm.Left)+' Top: '+IntToStr(sForm.Top),eForm);
	RCOutP('Width: '+IntToStr(sForm.Width)+' Height: '+IntToStr(sForm.Height),eForm);
	
	read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor');  // postavljanje prija�nje pozicije, dimenzija i stanja
	
	puta:=PrgFolder+'skripte\RC_Tools\Semafor\Journal';
	read_form(rc_TF, Puta);  // postavljanje prija�nje pozicije, dimenzija i stanja journala
 	// Showmessage('Pro�itao SAM VISINU: "'+#13#10+IntToStr(rc_TF.Height));
	
	
	RCOutP('U�itani podaci s diska!',eForm);
	// rcRefresh;
	RCOutP('Left: '+IntToStr(sForm.Left)+' Top: '+IntToStr(sForm.Top),eForm);
	RCOutP('Width: '+IntToStr(sForm.Width)+' Height: '+IntToStr(sForm.Height),eForm);
	// 
	panel_caption:=rcPanel('',sForm);
	panel_caption.Height:=12;
	panel_caption.Align:=alTop;
	panel_caption.onMouseDown:=@panel_captionMouseDown;
	panel_caption.onMouseMove:=@panel_captionMouseMove;
	panel_Caption.caption:='SEMAFOR';
	panel_Caption.font.size:=7;
	panel_Caption.font.color:=clGray;
	// function rcButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TButton;
	
	xSlik:=rcLoadImage('xsemy',put_skript+'x.jpg',Panel_Caption,0,0);
	//xSlik.Width:=10;
	xSlik.Align:=alRight;
	xSlik.onMouseDown:=@xSlikMouseDown;
	xSlik.onMouseMove:=@xSlikMouseMove;
	// But_x.Color:=clRed;

	
	panel_bottom:=rcPanel('',sForm);
	panel_bottom.color:=clSilver;
	panel_bottom.Height:=12;
	panel_bottom.Align:=alBottom;

	
	Image_resize:=rcLoadImage('xresize',put_skript+'resize.bmp',Panel_Bottom,0,0);
	// Image_resize.transparent:=true;    NE RADI!!!
	//Image_resize:=10;
	Image_resize.Align:=alRight;
	Image_resize.onMouseDown:=@Image_resizeMouseDown;
	Image_resize.onMouseMove:=@Image_resizeMouseMove;
	
	Image_up:=rcLoadImage('xup',put_skript+'up.jpg',Panel_Bottom,0,0);
	Image_up.Align:=alleft;
	
	Image_down:=rcLoadImage('xdown',put_skript+'down.jpg',Panel_Bottom,0,0);
	Image_down.Align:=alLeft;
	
	// xSlik.onMouseDown:=@ImageResizeMouseDown;
	// xSlik.onMouseMove:=@ImageResizeMouseMove;
	
	
	// slika semafora
	semslik:=rcLoadImage('semy',put_skript+'semafor.jpg',sForm,0,0);
	semslik.ShowHint:=True;
	semslik.Hint:='Dvostruki klik za otvaranje skripte "Semafor".';
	semslik.onMouseMove:=@SemSlikMouseMove;
	semslik.onMouseDown:=@SemSlikMouseDown;
	//
	
	
	
	panel_id:=rcPanel('',sForm);
	panel_id.Height:=20;
	panel_id.Align:=alTop;
	panel_Caption.font.color:=clBlack;
	
	
	panel_setup:=rcScrollBox('',sForm);
	panel_setup.Align:=alClient;
	panel_setup.visible:=false;
	
	panel_main2:=rcScrollBox('',sForm);
	panel_Main2.Top:=50;
	panel_Main2.Align:=alClient;
	panel_Main2.ParentColor:=False;
	panel_Main2.VertScrollBar.visible:=True;
	panel_Main2.VertScrollBar.Increment:=8;
	// panel_Main2.VertScrollBar.Page:=velicina;
	
	// privremeno se sklanja ovaj panel (zapravo scrollbox) dok se ne napuni
	panel_Main2.visible:=False;  
	
	
	
	// function rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
	label_broj:=rcLabel('','',panel_id,1,1,taLeftJustify);
	label_broj.align:=alLeft;
	label_broj.font.size:=12;
	label_broj.font.style:=[fsBold];

	//
	// ======================================================================================
	sredina:=100;
	velicina:=21;		// veli�ina slova u semaforu = velicina - 12
	// 
	// ======================================================================================
	//

	
	but_prop:=Button3('OK','Vrati pogled na osobine', Panel_id);
	but_prop.font.color:=clBlack;
	but_prop.width:=50;
	but_prop.visible:=false;
	but_prop.OnClick:=@but_propOnClik;
	
	// but_var:=Button3('VAR','Varijable selektiranog elementa', Panel_id);
	// but_var.font.color:=clGray;
	// but_var.OnClick:=@but_varOnClik;

	but_podesi:=Button3('Podesi','Pode�avanje prikaza', Panel_id);
	but_podesi.font.color:=clBlack;
	but_podesi.width:=50;
	but_podesi.OnClick:=@but_podesiOnClik;
	
	panel_naziv:=Panel3('Naziv');
	label_naziv:=Label3('Naziv',Panel_naziv);
	OK_naziv:=Button3('OK','Promjeni une�eni sadr�aj',Panel_naziv);
	edit_naziv:=Edit3(Panel_naziv);	
	
	panel_sifra:=Panel3('sifra');
	label_sifra:=Label3('�ifra',Panel_sifra);
	OK_sifra:=Button3('OK','Promjeni une�eni sadr�aj',Panel_sifra);
	edit_sifra:=Edit3(Panel_sifra);	
	
	panel_x:=Panel3('X');
	label_x:=Label3('X',Panel_x);
	OK_x:=Button3('OK','Promjeni une�eni sadr�aj',Panel_x);
	edit_x:=Edit3(Panel_x);	
	
	panel_fx:=Panel3('fX');
	label_fx:=Label3('F (x)',Panel_fx);
	OK_fx:=Button3('OK','Promjeni une�eni sadr�aj',Panel_fx);
	edit_fx:=Edit3(Panel_fx);	
		
	panel_y:=Panel3('Y');
	label_y:=Label3('Y',Panel_y);
	OK_y:=Button3('OK','Promjeni une�eni sadr�aj',Panel_y);
	edit_y:=Edit3(Panel_y);	
	
	panel_fy:=Panel3('fY');
	label_fy:=Label3('F (y)',Panel_fy);
	OK_fy:=Button3('OK','Promjeni une�eni sadr�aj',Panel_fy);
	edit_fy:=Edit3(Panel_fy);	

	panel_z:=Panel3('Z');
	label_z:=Label3('Z',Panel_z);
	OK_z:=Button3('OK','Promjeni une�eni sadr�aj',Panel_z);
	edit_z:=Edit3(Panel_z);
	
	panel_fz:=Panel3('fZ');
	label_fz:=Label3('F (z)',Panel_fz);
	OK_fz:=Button3('OK','Promjeni une�eni sadr�aj',Panel_fz);
	edit_fz:=Edit3(Panel_fz);	
	
	panel_sirina:=Panel3('Sirina');
	label_sirina:=Label3('�irina',Panel_sirina);
	OK_sirina:=Button3('OK','Promjeni une�eni sadr�aj',Panel_sirina);
	edit_sirina:=Edit3(Panel_sirina);	
	
	panel_visina:=Panel3('Visina');
	label_visina:=Label3('Visina',Panel_visina);
	OK_visina:=Button3('OK','Promjeni une�eni sadr�aj',Panel_visina);
	edit_visina:=Edit3(Panel_visina);
	
	panel_debljina:=Panel3('Debljina');
	label_debljina:=Label3('Debljina_d',Panel_debljina);
	OK_debljina:=Button3('OK','Promjeni une�eni sadr�aj',Panel_debljina);
	edit_debljina:=Edit3(Panel_debljina);
	
	panel_dubina:=Panel3('Dubina');
	label_dubina:=Label3('Dubina_e',Panel_dubina);
	OK_dubina:=Button3('OK','Promjeni une�eni sadr�aj',Panel_dubina);
	edit_dubina:=Edit3(Panel_dubina);
			
	panel_primjedba:=Panel3('Primjedba');
	label_primjedba:=Label3('Primjedba_d',Panel_primjedba);
	OK_primjedba:=Button3('OK','Promjeni une�eni sadr�aj',Panel_primjedba);
	OK_primjedba.width:=30;
	OK_primjedba.onClick:=@Primjedba_change;
	edit_primjedba:=Edit3(Panel_primjedba);
	edit_primjedba.font.color:=clRed;
	edit_primjedba.font.style:=[fsBold];
	
	panel_napomena:=Panel3('Napomena');
	label_napomena:=Label3('Napomena_e',Panel_napomena);
	OK_napomena:=Button3('OK','Aktiviraj novu napomenu', Panel_napomena);
	OK_napomena.width:=30;
	OK_napomena.onClick:=@Napomena_change;
	edit_napomena:=Edit3(Panel_napomena);
	edit_napomena.font.color:=clRed;
	edit_napomena.font.style:=[fsBold];
	
	panel_program:=Panel3('program');
	label_program:=Label3('Program_d',Panel_program);
	OK_program:=Button3('OK','Aktiviraj novi program', Panel_program);
	OK_program.width:=30;
	OK_program.onClick:=@Program_change;
	edit_program:=Edit3(Panel_program);
	// edit_program.font.color:=clRed;
	// edit_program.font.style:=[fsBold];
	

	
	// setup
	
	
	// function rcCheckBox(cbName, cbCaption: string; cbParent: TWinControl; cbX, cbY: integer):TCheckBox;
	SetNaziv:=CheckBox3('Naziv (E+D)'				,10,   5); SetNaziv.OnClick:=@set_osobineOnClick;
	SetSifra:=CheckBox3('�ifra (E+D)'				,10,  25); SetSifra.OnClick:=@set_osobineOnClick;
	SetX:=CheckBox3	 ('X (E+D)'						,10,  45); SetX.OnClick:=@set_osobineOnClick;
	SetFX:=CheckBox3	 ('X - formula (E+D)'		,10,  65); SetFX.OnClick:=@set_osobineOnClick;
	SetY:=CheckBox3	 ('Y (E+D)'						,10,  85); SetY.OnClick:=@set_osobineOnClick;
	SetFY:=CheckBox3	 ('Y - formula (E+D)'		,10, 105); SetFY.OnClick:=@set_osobineOnClick;
	SetZ:=CheckBox3	 ('Z (E+D)'						,10, 125); SetZ.OnClick:=@set_osobineOnClick;
	SetFZ:=CheckBox3	 ('Z - formula (E+D)'		,10, 145); SetFZ.OnClick:=@set_osobineOnClick;
	SetSirina:=CheckBox3	 ('Sirina (E+D)'	 		,10, 165); SetSirina.OnClick:=@set_osobineOnClick;
	SetVisina:=CheckBox3	 ('Visina (E+D)'	 		,10, 185); SetVisina.OnClick:=@set_osobineOnClick;
	SetDebljina:=CheckBox3	 ('Debljina (D)'		,10, 205); SetDebljina.OnClick:=@set_osobineOnClick;
	SetDubina:=CheckBox3	 ('Dubina (E)'		 		,10, 225); SetDubina.OnClick:=@set_osobineOnClick;
	SetPrimjedba:=CheckBox3	 ('Primjedba (D)'		,10, 245); SetPrimjedba.OnClick:=@set_osobineOnClick;
	SetNapomena:=CheckBox3	 ('Napomena (E)'	 	,10, 265); SetNapomena.OnClick:=@set_osobineOnClick;
	SetProgram:=CheckBox3	 ('Program (D)'	 	,10, 285); SetNapomena.OnClick:=@set_osobineOnClick;
	SetJournal:=CheckBox3 ('Journal - NE DIRAJ!'	,10, 305); SetJournal.OnClick:=@set_osobineOnClick;
	
	// function rcSpeedButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TSpeedButton;

	// sakrij nepotrebne propertije
	// Showmessage ('1. Provjeri semafor.ini');
	SetOsobineReadFromDisk;
	// Showmessage ('2. Provjeri semafor.ini');
	// ponovo se pokazuje ovaj panel (zapravo scrollbox) jer je sad napunjen
	panel_Main2.visible:=False;  
	
	
	
	
	sForm.show;
	read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor');
	if sForm.Width<20 then begin 
		read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_mali');
	end else begin
		read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki');
	end;
	poslozi;
	
	puta:=PrgFolder+'skripte\RC_Tools\Semafor\semafor.ini'; 
	// RCOutP('0. find_value_int(puta,"SetSifra")='+IntToStr(find_value_int(puta,'SetSifra')),eForm);	
	
	
	
end;

procedure Kreiraj_formu;
var 
	i:integer;
	par:TForm;
	xo:TObject;
Begin
	RCOutP('Kreiraj_formu START',eForm);   
	put_skript:=PrgFolder+'skripte\RC_Tools\Semafor\';
	// rcOut_enabled:=true;	   // omogu�avanje Journala
	// rcOut_enabled:=false;
	RCOutP('1',eForm);
	Showmessage('1');
	If E<>nil   then  // forma je kreirana u EngineStart jer je u editoru
					else	eForm:=rcTool('Glavna');    // u projektu!
					
	RCOutP('2',eForm);				
	Showmessage('2');
	// eForm.OnCloseQuery:=@EditorCloseQuery;

	//RCOut('Detektiran je prozor editora. Ima naziv "'+eForm.caption+'"'); 
	RCOutP('3',eForm);
	Showmessage('3');
	NapraviFormu;
	RCOutP('4',eForm);
	Showmessage('4');
	RCOutP('Kreiraj_formu END',eForm);   
	Showmessage('Kreiraj_formu END');
	rcOut_enabled:=true
End;

Procedure EngineStart(parent:tobject);   // mora se ovako zvati, parent joj je editor
// de�ava se kad se pozove editor

begin 
	// Showmessage('EngineStart START'); 
	// rcOut_enabled:=true;
	
	RCOutP('EngineStart START',eForm); 
	
	If E<>nil   then  begin 
								Kreiraj_formu;
					end else	Begin 
								RCOutP('Prije kreiranja Glavne forma u Editoru',eForm);
								eForm:=TForm(twincontrol(parent));    // u prozoru editora!
								RCOutP('Glavna forma kreirana u Editoru',eForm);
								Kreiraj_formu; 
	end;					
	RCOutP('EngineStart END',eForm); 
	Showmessage('EngineStart END'); 
end;



{
Procedure EngineStart(parent:tobject);   // mora se ovako zvati, parent joj je editor
// de�ava se kad se pozove editor
var 
	i:integer;
	par:TForm;
	xo:TObject;
begin 
	put_skript:=PrgFolder+'skripte\RC_Tools\Semafor\';
	// /////////////////////////////////////////////////////////////////////////////////////////////////////////
	rcOut_enabled:=true;	   // omogu�avanje Journala
	// rcOut_enabled:=false;
	// /////////////////////////////////////////////////////////////////////////////////////////////////////////
	// RCOut('Journal forma je kreirana');   
	// RCOut('RC_EngineStart'); 	
	eForm:=TForm(twincontrol(parent));    // u prozoru editora!
	eForm.OnCloseQuery:=@EditorCloseQuery;

	//RCOut('Detektiran je prozor editora. Ima naziv "'+eForm.caption+'"'); 
	NapraviFormu;
	
end;
}

Procedure EngineEnd;
// de�ava se kad se zatvori prozor od editora
// u tom su trenutku sve forme i skripte editora ve� zatvorene i ni�ta se iz njih ne mo�e �itati
begin
   // showmessage('EngineEnd');
	if sform<>nil then sform.free;
	
	
	
	
 
end;

procedure provjeri_selektirano;
var
	// lista_elementi:array of TElement;
	// lista_daske:array of TDaska;
	i:integer;
	// broj_el - broj selektiranih elemenata deklariran globalno
	// Broj_das - broj selektiranih deklariran deklariran globalno
Begin
	RCOutP('Provjeri_selektirano START',eForm);

	
	RCOutP('Length(lista_elementi)='+IntToStr(Length(lista_elementi)),eForm);
	
	// obri�i sva polja
	edit_naziv.text:='';
	edit_sifra.text:='';
	edit_x.text:='';
	edit_fx.text:='';
	edit_y.text:='';
	edit_fy.text:='';
	edit_z.text:='';
	edit_fz.text:='';
	edit_sirina.text :='';
	edit_visina.text :='';
	edit_debljina.text :='';
	edit_dubina.text :='';
	edit_primjedba.text :='';
	edit_napomena.text :='';
	edit_program.text :='';
	
	
	For i:=0 to Length(lista_elementi)-1 do begin
		RCOutP('�itam element: '+IntToStr(i)+' '+lista_elementi[i].Naziv,eForm);
		if edit_naziv.text =''  then edit_naziv.text:=lista_elementi[i].Naziv
								  else edit_naziv.text:=edit_naziv.text+';'+lista_elementi[i].naziv;
		if edit_sifra.text =''  then edit_sifra.text:=lista_elementi[i].Sifra
								  else edit_Sifra.text:=edit_Sifra.text+';'+lista_elementi[i].Sifra;						  
		if edit_x.text =''  then edit_x.text:=FloatToStrC1(lista_elementi[i].xPos,8,2)
								  else edit_x.text:=edit_x.text+';'+FloatToStrC1(lista_elementi[i].xPos,8,2);
		if edit_fx.text ='' then edit_fx.text:=lista_elementi[i].xF
								  else edit_fx.text:=edit_fx.text + ';'+lista_elementi[i].xF;
		if edit_y.text =''  then edit_y.text:=FloatToStrC1(lista_elementi[i].yPos,8,2)
								  else edit_y.text:=edit_y.text+';'+FloatToStrC1(lista_elementi[i].yPos,8,2);
		if edit_fy.text ='' then edit_fy.text:=lista_elementi[i].yF
								  else edit_fy.text:=edit_fy.text + ';'+lista_elementi[i].yF;						 
		if edit_z.text =''  then edit_z.text:=FloatToStrC1(lista_elementi[i].zPos,8,2)
								  else edit_z.text:=edit_z.text+';'+FloatToStrC1(lista_elementi[i].zPos,8,2);
		if edit_fz.text ='' then edit_fz.text:=lista_elementi[i].yF
								  else edit_fz.text:=edit_fz.text + ';'+lista_elementi[i].zF;	
		if edit_sirina.text =''  then edit_sirina.text:=FloatToStrC1(lista_elementi[i].Sirina,8,2)
								  else edit_sirina.text:=edit_sirina.text+';'+FloatToStrC1(lista_elementi[i].sirina,8,2);								  
		if edit_visina.text =''  then edit_visina.text:=FloatToStrC1(lista_elementi[i].visina,8,2)
								  else edit_visina.text:=edit_visina.text+';'+FloatToStrC1(lista_elementi[i].visina,8,2);	
		if edit_dubina.text =''  then edit_dubina.text:=FloatToStrC1(lista_elementi[i].dubina,8,2)
								  else edit_dubina.text:=edit_dubina.text+';'+FloatToStrC1(lista_elementi[i].dubina,8,2);	
		if edit_napomena.text =''  then edit_napomena.text:=lista_elementi[i].napomena
								  else edit_napomena.text:=edit_napomena.text+';'+lista_elementi[i].napomena;	
								 
		
	End;		// For i:=0 to Length(lista_elementi)-1
	

	
	RCOutP('Length(lista_daske)='+IntToStr(Length(lista_daske)),eForm);
	For i:=0 to Length(lista_daske)-1 do begin
		RCOutP('�itam dasku: '+IntToStr(i)+' '+lista_daske[i].Naziv,eForm);
		if edit_naziv.text =''  then edit_naziv.text:=lista_daske[i].Naziv
										else edit_naziv.text:=edit_naziv.text+';'+lista_daske[i].naziv;
		if edit_sifra.text =''  then edit_sifra.text:=lista_daske[i].Sifra
										else edit_sifra.text:=edit_sifra.text+';'+lista_daske[i].sifra;
		if edit_x.text =''  then edit_x.text:=FloatToStrC1(lista_daske[i].xPos,8,2)
								  else edit_x.text:=edit_x.text+';'+FloatToStrC1(lista_daske[i].xPos,8,2);
		if edit_fx.text ='' then edit_fx.text:=lista_daske[i].xF
								  else edit_fx.text:=edit_fx.text + ';'+lista_daske[i].xF;
		if edit_y.text =''  then edit_y.text:=FloatToStrC1(lista_daske[i].yPos,8,2)
								  else edit_y.text:=edit_y.text+';'+FloatToStrC1(lista_daske[i].yPos,8,2);
		if edit_fy.text ='' then edit_fy.text:=lista_daske[i].yF
								  else edit_fy.text:=edit_fy.text + ';'+lista_daske[i].yF;						 
		if edit_z.text =''  then edit_z.text:=FloatToStrC1(lista_daske[i].zPos,8,2)
								  else edit_z.text:=edit_z.text+';'+FloatToStrC1(lista_daske[i].zPos,8,2);
		if edit_fz.text ='' then edit_fz.text:=lista_daske[i].yF
								  else edit_fz.text:=edit_fz.text + ';'+lista_daske[i].zF;	
		if edit_sirina.text =''  then edit_sirina.text:=FloatToStrC1(lista_daske[i].Sirina,8,2)
								  else edit_sirina.text:=edit_sirina.text+';'+FloatToStrC1(lista_daske[i].sirina,8,2);								  
		if edit_visina.text =''  then edit_visina.text:=FloatToStrC1(lista_daske[i].visina,8,2)
								  else edit_visina.text:=edit_visina.text+';'+FloatToStrC1(lista_daske[i].visina,8,2);	
		if edit_debljina.text =''  then edit_debljina.text:=FloatToStrC1(lista_daske[i].debljina,8,2)
								  else edit_debljina.text:=edit_debljina.text+';'+FloatToStrC1(lista_daske[i].debljina,8,2);	
		if edit_primjedba.text =''  then edit_primjedba.text:=lista_daske[i].primjedba
								  else edit_primjedba.text:=edit_primjedba.text+';'+lista_daske[i].primjedba;	
		if edit_program.text =''  then edit_program.text:=lista_daske[i].Bprogram
								  else edit_program.text:=edit_program.text+';'+lista_daske[i].Bprogram;	
	End;

	
	RCOutP('Provjeri_selektirano END',eForm);
End;


Procedure EngineGetObject(sender:tobject);    // select, down, a ne klik
// de�ava se kad se pritisne tipka mi�a u editoru
var
	i,ie,id:integer;
	elm:TElement;
	td:TDaska;
	// lista_elementi:array [1..100] of TElement;

begin
	RCOutP('EngineGetObject START! -------- ',eForm);
	// ****************************************************************************************************
	// ****************************************************************************************************
	// ****************************************************************************************************	
	
	// tra�im selektirane elemente
		
	ie:=0; // broja� elemenata
	id:=0; // broja� dasaka
	SetLength(lista_elementi,ie);
	SetLength(lista_daske,ie);
	
	if e<>nil then 
		// U ELEMENTU SAM 
		begin  
			
						
			// --------------- U�itaj elemente --------------------
			RCOutP('Brojim selektirane elemente u Editoru',eForm);
			for i:=0 to E.ElmList.count-1 do Begin  // broji elemente u EDITORU!!!!!
				elm:=tElement(E.ElmList.items[i]);   
				if elm.selected then begin
					SetLength(lista_elementi,ie+1);
					// RCOutP('A�uriram listu "lista_elementi[ie]", ie=' + IntToStr(ie),eForm);
					lista_elementi[ie]:=elm;
					ie:=ie+1;
					// RCOutP('A�uriranje gotovo',eForm);
						
				end;  // if elm.selected 
			end; // for i:=0 to E.ElmList.count-1
			
			RCOutP('Selektirano je '+IntToStr(ie)+'/'+IntToStr(E.ElmList.count)+' elemenata',eForm);
			
			// --------------- U�itaj daske --------------------
			
			RCOutP('Brojim selektirane daske u Editoru',eForm);
			for i:=0 to e.childdaska.CountObj-1 do begin
				td:=tDaska(e.childdaska.Daska[i]);
				// if e.childdaska.Daska[i].selected = true then id:=id+1;
				if td.selected then begin
					
					SetLength(lista_daske,id+1);
					// RCOutP('A�uriram listu "lista_daske[id]", id=' + IntToStr(id),eForm);
					lista_daske[id]:=td;
					id:=id+1;
					// RCOutP('A�uriranje gotovo',eForm);
				end;  // if td.selected 
			end; // for i:=0 to e.childdaska.CountObj-1
			
			RCOutP('Selektirano je '+IntToStr(id)+'/'+IntToStr(e.childdaska.CountObj)+' dasaka',eForm);
			
			// RCOutP('U elementu postoji '+IntToStr(e.elmlist.countObj)+' objekata.',eForm);
			// RCOutP('Selektirano je '+IntToStr(e.elmlist.countObj)+' objekata.',eForm);
			
			
		end else begin     
		// U PROJEKTU SAM
			// ShowD('U projektu sam');
			RCOutP('U Projektu sam',eForm);
			for i:=0 to ElmHolder.elementi.count-1 do Begin  // broji elemente u PROJEKTU!!!!!
				elm:=telement(ElmHolder.elementi.items[i]);   
				if elm.selected then begin
						// Elist.addObject(elm.naziv,elm);
				end;  // if elm.selected 
			End; // for i
	
	end;  // if e<>nil     
	
	broj_el:=ie;
	broj_das:=id;
	
	Label_broj.caption:=IntToStr(broj_el)+ ' el. + '+	IntToStr(broj_das)+ ' das.';
	provjeri_selektirano;
	
	
	// selektirani_objekti.Free;
RCOutP('EngineGetObject END! ------------ ',eForm);
end;





begin
// ovo se prikazuje samo ako se skripta pokrene u projektu
// 
	


end.
