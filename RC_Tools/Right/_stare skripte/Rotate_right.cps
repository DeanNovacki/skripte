Program Rotate;
var
	elem:Telement;
	sx,sz,rx,rz,nx,nz,k,nk, xr,zr,zc, korak,
	xfactor,zfactor,xp,zp, pkut, rad, kut,
	r,f,t,a1,b1,a2,b2:single;
	i:single;
	win:TForm;
	e1,krug:tElement;
	path:string;

Function DegReset(kk:single):single;
// mi�e "vi�ak" stupnjeva od 360 (370 pretvara u 10)
Begin
	if kk>=360 then begin

		result:=kk-Trunc(kk/360) * 360;
	end;
End;
	
	
	
Procedure refresh;
begin
   win:=TForm.create(nil);
   // win.width:=1; win.height:=1;
	// win.show;
	application.processmessages;
	win.free;	
end;

Function tg(k1:single):single;
// tangens
Begin
	result:=sin(k1)/cos(k1);
End;

Function ctg(k1:single):single;
// tangens
Begin
	result:=cos(k1)/sin(k1);
End;

Function DegToRad(Deg:single):single;
// stupnjevi u radijane
Begin
	result:=Deg*3.1415926535897932384626433832795/180;
End;

Function RadToDeg(Rad:single):single;
// stupnjevi u radijane
Begin
	result:=Rad*180/3.1415926535897932384626433832795;
End;
	
Function SinD(deg:single):single;
// Sinus za stupnjeve
Begin
	result:=sin(deg*3.1415926535897932384626433832795/180);
End;
	
Function CosD(deg:single):single;
// Cosinus za stupnjeve
Begin
	result:=cos(deg*3.1415926535897932384626433832795/180);
End;	

Function sqr(nn:single):single;
// kvadriranje
Begin
	result:=nn*nn
End;

Type 
	TSingleArray = array [0..1] of single;
Var
	x1,y1:single;
	xc,yc:single;
	x2,y2:single;
	t2:TSingleArray;

Function rotate2D(t1:TSingleArray; alpha:single; c1:TSingleArray): TSingleArray;
// Rotacija to�ke T1 u 2D oko to�ke C1 za kut ALPHA

Begin
	// citanje ulaznog niza 
	x1:=t1[0]; 	y1:=t1[1];
	xc:=c1[0]; 	yc:=c1[1];
	// translacija svega tako da centar rotacije bude u ishodi�tu
	x1:=x1-xc;	
	y1:=y1-yc;
	// rotacija
	x2:=x1*cosD(alpha)+y1*sinD(alpha);
	y2:=y1*cosD(alpha)-x1*sinD(alpha);
	// translacija natrag 
	x2:=x2+xc;	
	y2:=y2+yc;
	// rezultat
	result[0]:=x2;
	result[1]:=y2;
End;

var
	poc,cen,zav: TSingleArray; 
	
Begin
	
	kut:=10;											//<< OVDJE SE ZADAJE KUT ROTACIJE!!!
	
	// uzmi prvi element u projektu
	elem:=TElement(ElmList.Items[0]);
	
	// elem.kut:=elem.kut+5;   // za testiranje
	
	// uzmi oznaku kruga
	krug:=TElement(ElmList.Items[1]);
	// zapamti po�etni x i z
	// sx:=elem.Xpos;
	// sz:=elem.Zpos;
	
	// na�i centar elementa
	poc[0]:=elem.Xpos+elem.sirina/2;
	poc[1]:=elem.Zpos-elem.dubina/2;
	cen[0]:=elem.Xpos;
	cen[1]:=elem.Zpos;
	// bitan je i kut elementa
	zav:=rotate2D(poc,elem.kut,cen);    
	
	// na�i radius po kojem mora i�i ishodi�te elementa kod okretanja
	rad:=Sqrt((sqr(elem.sirina/2)+sqr(elem.dubina/2)));  // pola dijagonale elementa
	xc:=zav[0]-rad;
	zc:=zav[1]+rad;
	// test
	krug.ypos:=10000;   // da se mo�e risajzat
	krug.sirina:=rad*2;
	krug.dubina:=rad*2;
	krug.ypos:=0;			// vrati posto je resize napravljen
	krug.xpos:=xc;
	krug.zpos:=zc;
	
	// odredi stvarni centar rotacije (corpusov)
	// xr:=elem.Xpos;
	// zr:=elem.Zpos;
	
	// zarotiraj elementovo ishodi�te oko novog centra rotacije
	poc[0]:=elem.Xpos;
	poc[1]:=elem.Zpos;
	cen[0]:=xc;
	cen[1]:=zc;
	
	zav:=rotate2D(poc,kut,cen);
	// dodijeli rezultate
	elem.Xpos:=zav[0];
	elem.Zpos:=zav[1];
	elem.kut:=elem.kut+kut;
	
{	
	// odredi stvarni centar rotacije
	xr:=elem.Xpos;
	zr:=elem.Zpos;
	// prona�i radius izme�u novog centra rotacije i ishodista
	rad:=Sqrt((sqr(xc-xr)+sqr(zc-zr)));
	// odredi sirinu elementa krug
	krug.ypos:=10000;
	krug.sirina:=rad*2;
	krug.dubina:=rad*2;
	krug.ypos:=0;
	// odredi poziciju elementa krug
	
	// odredi kut pod kojim se nalazi pocetak elementa u odnosu na centar rotacije
	pkut:=arcsin((xc-xr)/rad);
	pkut:=270-RadToDeg(pkut);
	// Showmessage('pocetni kut: '+FloatToStrC1(pkut,8,2));
	// postavi pocetni kut nove rotacije
	// iako element ima kut nula, kut rotacije ovisi o mjestu centra pa nije nula
	{
	// zadavanje kuta rotacije
	kut:=elem.kut+45;
	// zadavanje koraka animacije
	korak:=kut/100;
	i:=0;
	while elem.kut < kut do begin
		// i:=i+korak;
		pkut:=pkut+korak;
		// novi polozaj elementa
		// Showmessage('Zadani kut  : '+FloatToStrC1(kut,8,2)+#13#10+
		// 				'Trenutni kut: '+FloatToStrC1(elem.kut,8,2)+#13#10+
		// 				'pKut: '+FloatToStrC1(pkut,8,2));
		xfactor:=rad*cos(DegToRad(pkut));
		zfactor:=rad*sin(DegToRad(pkut));
		// Showmessage('X factor: '+FloatToStrC1(xfactor,8,2)+#13#10+
		// 				' Z factor: '+FloatToStrC1(zfactor,8,2));
		elem.Xpos:=xc+xfactor;
		elem.zpos:=zc-zfactor;
		elem.kut:=elem.kut+korak;
		refresh;
		elem.RecalcFormula(elem);
	End;
	
	// Elem.kut:=DegReset(Elem.kut);
	
	}
	
	elem:=TElement(ElmList.Items[0]);
	elem.RecalcFormula(elem);	
End.