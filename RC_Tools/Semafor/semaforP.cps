
{$I ..\include\rcPrint.cps}
{$I ..\include\rcMessages.cps}
{$I ..\include\rcStrings.cps}
{$I ..\include\rcControls.cps}
{$I ..\include\rcObjects.cps}
{$I ..\include\rcConfig.cps}
{$I ..\include\rcEvents.cps}
{$I ..\include\rcCurves.cps}
{$I ..\include\rcMath.cps}
{$I ..\include\rcOut.cps}


var
   MyDate:TDateTime;	
	eForm:tForm; // prozor editora
	
   put_skript, put_ini :string;
	selektirani_objekti:TStringList;
	Das:TDaska;
	Elem:TElement;
	elm2:TElement;
	
	lista_elementi:array of TElement;
	lista_daske:array of TDaska;
	
	versiontxt:String;

	sForm:tForm; // prozor skripte
		SemSlik:TmImage;				// kad je forma minimizirana;
		
		panel_caption: TPAnel;		// najgornji za pomicanje i naslov
			xSlik:TmImage;				// za minimiziranje
		
		panel_id:TPanel;				// koli�ina i nazivi selektiranih
			label_broj :TLabel;
				broj_el, stari_broj_el, broj_das:integer;
			but_podesi:TSpeedButton;
			but_prop:TSpeedButton;
			but_var:TSpeedButton;
		
		panel_setup:TScrollBox;
			SetNaziv:TCheckBox;
			SetSifra:TCheckBox;
			SetX:TCheckBox;
			SetFx:TCheckBox;
			SetY:TCheckBox;
			SetFy:TCheckBox;
			SetZ:TCheckBox;
			SetFz:TCheckBox;
			SetSirina:TCheckBox;
			SetVisina:TCheckBox;
			SetDebljina:TCheckBox;
			SetDubina:TCheckBox;
			SetPrimjedba:TCheckBox;
			SetNapomena:TCheckBox;
			SetProgram:TCheckBox;
			// SetJournal:TCheckBox;
		panel_main2:TScrollBox;	

			panel_naziv : Tpanel;
				label_naziv :TLabel;
				edit_naziv:TEdit;
				OK_naziv:TSpeedButton;
				
			panel_sifra : Tpanel;
				label_sifra :TLabel;
				edit_sifra:TEdit;
				OK_sifra:TSpeedButton;	
			
			panel_x : Tpanel;
				label_x :TLabel;
				edit_x:TEdit;
				OK_x:TSpeedButton;
				
			panel_fx : Tpanel;
				label_fx :TLabel;
				edit_fx:TEdit;
				OK_fx:TSpeedButton;
				
							
			panel_y : Tpanel;
				label_y :TLabel;
				edit_y,edit_yf:TEdit;
				OK_y:TSpeedButton;				
				
			panel_fy : Tpanel;
				label_fy :TLabel;
				edit_fy:TEdit;
				OK_fy:TSpeedButton;
				
			panel_fz : Tpanel;
				label_fz :TLabel;
				edit_fz:TEdit;	
				OK_fz:TSpeedButton;				
				
			panel_z : Tpanel;
				label_z :TLabel;
				edit_z,edit_zf:TEdit;	
				OK_z:TSpeedButton;
				
			panel_sirina : Tpanel;
				label_sirina :TLabel;
				edit_sirina:TEdit;
				OK_sirina:TSpeedButton;

			panel_visina : Tpanel;
				label_visina :TLabel;
				edit_visina:TEdit;
				OK_visina:TSpeedButton;

			panel_dubina : Tpanel;
				label_dubina :TLabel;
				edit_dubina:TEdit;
				OK_dubina:TSpeedButton;
				
			panel_debljina : Tpanel;
				label_debljina :TLabel;
				edit_debljina :TEdit;
				OK_debljina:TSpeedButton;
				
			panel_primjedba : Tpanel;
				label_primjedba:TLabel;
				edit_primjedba :TEdit;
				OK_primjedba:TSpeedButton;
				
			panel_napomena : Tpanel;
				label_napomena :TLabel;
				edit_napomena :TEdit;
				OK_napomena:TSpeedButton;
			panel_program : Tpanel;
				label_program :TLabel;
				edit_program :TEdit;
				OK_program:TSpeedButton;
		

			
			


		
		panel_bottom: TPAnel;					// na dnu
			image_resize:TmImage;				// za resize
			image_up:TmImage;				// za resize
			image_down:TmImage;				// za resize

	but_x:TSpeedButton;

	sredina, velicina, i, ie,id:integer;			
	
	
   sX, sY:Integer; // po�etna to�ka za pomicanje otvorenog semafora mi�em




Procedure poslozi;
// poslo�i sve komponente nakon resizeanja 
Begin
	// Print('Poslozi START');
	if sForm.Width<20 then begin 
		// mala slikica
		panel_Main2.visible:=false;
		panel_id.visible:=false;
		panel_Caption.visible:=false;
		panel_bottom.visible:=false;
		semslik.visible:=True;
		// read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_mali');
	end else begin
		// velika slika
		panel_Main2.visible:=True;
		panel_Caption.visible:=True;
		panel_id.visible:=True;
		panel_bottom.visible:=True;
		semslik.visible:=False;
		// read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki');
	end;
	// Print('Poslozi END');
end;

procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); 
// radi na mouse down unutar prozora, ali ne i captiona    
begin
     // Print('Mouse down');  
	  // action:=cafree;
end;

procedure zapamti_pozicije;

Begin
	Print('proc: zapamti_pozicije START');
	// ShowMessage('Zapamti pozicije START');
	// spremanje pozicije i veli�ine semafora
	// u semafor.ini �e i�i i neke druge stvari
	
	If e=nil then begin
	
	// U PROJEKTU SAM 
		
		// print('Pamtim veliku formu semafora u projektu');
		if not set_value_int(put_ini, 'pLeft', sForm.Left)
			 then Showmessage('Gre�ka kod spremanja pLeft u '+ put_ini );
		if not set_value_int(put_ini, 'pTop', sForm.Top) 
			 then Showmessage('Gre�ka kod spremanja pTop u '+  put_ini );
		if not set_value_int(put_ini, 'pWidth', sForm.Width) 
			 then Showmessage('Gre�ka kod spremanja pWidth u '+ put_ini );
		if not set_value_int(put_ini, 'pHeight', sForm.Height) 
			 then Showmessage('Gre�ka kod spremanja pHeight '+ put_ini );

   	
	end else begin
	
	// U EDITORU SAM

		if sForm.Width>20 then begin 
		
			// Semafor je normalan
			
			print('Pamtim veliku formu semafora u editoru');
			if not set_value_str(put_ini, 'eSize', 'normalan')
				then Showmessage('Gre�ka kod spremanja pLeft u '+  put_ini );
			if not set_value_int(put_ini, 'eLeft', sForm.Left)
				then Showmessage('Gre�ka kod spremanja pLeft u '+  put_ini );
			if not set_value_int(put_ini, 'eTop', sForm.Top) 
				then Showmessage('Gre�ka kod spremanja pTop u '+  put_ini );
			if not set_value_int(put_ini, 'eWidth', sForm.Width) 
				then Showmessage('Gre�ka kod spremanja pWidth u '+  put_ini );
			if not set_value_int(put_ini, 'eHeight', sForm.Height) 
				then Showmessage('Gre�ka kod spremanja pHeight '+  put_ini );
		
		end else begin
			
			// semafor je minimiziran
	
			print('Pamtim malu formu semafora u editoru');
			if not set_value_str(put_ini, 'eSize', 'minimiziran')
				then Showmessage('Gre�ka kod spremanja pLeft u '+  put_ini );
			if not set_value_int(put_ini, 'mLeft', sForm.Left)
				then Showmessage('Gre�ka kod spremanja pLeft u '+  put_ini );
			if not set_value_int(put_ini, 'mTop', sForm.Top) 
				then Showmessage('Gre�ka kod spremanja pTop u '+  put_ini );
		
		end; 	// if sForm.Width>20
			
	
	end;	// if e<>nil
	
	Print('proc: zapamti_pozicije END');
End;



procedure SemSLikMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
// Ovo pomi�e prozor skripte pomo�u mi�a
begin
	// Print('Semafor x: '+IntToStr(x)+' y: '+IntToStr(x) );
	Timage(Sender).Cursor :=crSizeAll;
	if ssleft in shift then begin 
				// Print('Klik' );
				if x>12 then sForm.Left:=sForm.Left+4;
				if x<4  then sForm.Left:=sForm.Left-4;
				if y>20 then sForm.Top:=sForm.Top+4;
				if y<12 then sForm.Top:=sForm.Top-4;
				
	end;
	//  if ssDouble in shift then Print('Klik Klik' );
	//  if ssRight in shift then Print('Desni Klik' );
end;

procedure SemSlikMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
// OTVARANJE
// Ovo detektira dvostruki klik na sliku semafora
// treba staviti formu skripte u punu veli�inu
begin
	// if ssleft in shift then Print('Left Down' );
				
	if ssDouble in shift then  
				if (E=nil) then begin 
					
					// u projektu sam iako je OVO NEMOGU�E!
					print ('Double klik na slikicu semafora u projektu!?!?!?!?!?!?!');	
						
					if sForm.width<20 then begin
								// nekim slu�ajem je ovdje mali semafor i treba ga pove�at
								// Print('Projekt: Pove�avam prozor skripte' );
								sForm.Width:=200;
								sForm.Height:=200;
								poslozi;
							end else begin
								// Print('Projekt: Ne smanjujem prozor skripte' );
								// semafor je ve� veliki
								// ne radi ni�ta
					end // if sForm.width<20   
					
				end else begin // if (E=nil)
				
					// EDITOR
					
					// print ('Double klik na slikicu semafora u editoru');	
					if sForm.width<20 then begin
						// u editoru sam
						// forma je mala i treba je uve�ati
								// Print('Editor: Pove�avam prozor skripte' );
								zapamti_pozicije;
								sForm.Left:=find_value_int(put_ini, 'eLeft');
								sForm.Top:=find_value_int(put_ini, 'eTop');
								sForm.Width:=find_value_int(put_ini, 'eWidth');
								sForm.Height:=find_value_int(put_ini, 'eHeight');
								poslozi;
							end else begin
								// Print('Editor: "Minimiziram" prozor skripte' );
								save_form(sForm,PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki');	
								read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_maki'); 	
								poslozi;
					end;   // if sForm.width<20
				end;	// if (E=nil)
	// end if ssDouble in shift
	
end;

procedure XSlikMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
// ON MOUSE OVER
begin
	// Print('X move over' );
	Timage(Sender).Cursor :=crArrow;
	// Timage(Sender).Cursor :=crNoDrop;

end;

Procedure SetOsobineWriteToDisk;

Begin
	// Print('proc: SetOsobineWriteToDisk START' );

	If SetNaziv.checked 	then set_value_int(put_ini,'SetNaziv',1)
								else set_value_int(put_ini,'SetNaziv',-1);
	If SetSifra.checked 	then set_value_int(put_ini,'SetSifra',1)
								else set_value_int(put_ini,'SetSifra',-1);
	If SetX.checked 		then set_value_int(put_ini,'SetX',1)
								else set_value_int(put_ini,'SetX',-1);
	If SetFX.checked 		then set_value_int(put_ini,'SetFX',1)
								else set_value_int(put_ini,'SetFX',-1);
	If SetY.checked 		then set_value_int(put_ini,'SetY',1)
								else set_value_int(put_ini,'SetY',-1);
	If SetFY.checked 		then set_value_int(put_ini,'SetFY',1)
								else set_value_int(put_ini,'SetFY',-1);
	If SetZ.checked 		then set_value_int(put_ini,'SetZ',1)
								else set_value_int(put_ini,'SetZ',-1);
	If SetFZ.checked 		then set_value_int(put_ini,'SetFZ',1)
								else set_value_int(put_ini,'SetFZ',-1);
	If SetSirina.checked then set_value_int(put_ini,'SetSirina',1)
								else set_value_int(put_ini,'SetSirina',-1);
	If SetVisina.checked then set_value_int(put_ini,'SetVisina',1)
								else set_value_int(put_ini,'SetVisina',-1);							
	If SetDebljina.checked 		then set_value_int(put_ini,'SetDebljina',1)
								else set_value_int(put_ini,'SetDebljina',-1);
	If SetDubina.checked then set_value_int(put_ini,'SetDubina',1)
								else set_value_int(put_ini,'SetDubina',-1);
	If SetPrimjedba.checked 		then set_value_int(put_ini,'SetPrimjedba',1)
								else set_value_int(put_ini,'SetPrimjedba',-1);
	If SetNapomena.checked 		then set_value_int(put_ini,'SetNapomena',1)
								else set_value_int(put_ini,'SetNapomena',-1);							
	If SetProgram.checked 		then set_value_int(put_ini,'SetProgram',1)
								else set_value_int(put_ini,'SetProgram',-1);		

	// Print('proc: SetOsobineWriteToDisk END' );
End;

procedure xSlikMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
// ZATVARANJE
// Ovo detektira klik na sliku X-a (zatvaranja prozora)
// treba staviti formu skripte na minimalnu veli�inu
begin
	// Print('xSlikMouseDown START' );
	
	if ssLeft in shift then begin 
				if E<>nil then begin
						// Print('Editor: Minimiziram prozor skripte' );
						
						// EDITOR
												
						zapamti_pozicije;    // da bi zapamtio ono �to treba
						
						// u�itavanje pozicije za mali semafor
						
						sForm.Left:=find_value_int(put_ini, 'mLeft');
						sForm.Top :=find_value_int(put_ini, 'mTop'); 
						sForm.Width:=16;
						sForm.Height:=32;	
						
					end else begin
						
						// PROJEKT

						zapamti_pozicije;

						sForm.visible:=False
				end;
	end;
	poslozi;
	// Print('xSlikMouseDown END' );
end;

procedure panel_captionMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
// POMICANJE OTVORENOG SEMAFORA
begin
	TPanel(Sender).Cursor :=crSizeAll;
	//Print('moove' );
	if ssleft in shift then begin 
				if x>sX+4 then sForm.Left:=sForm.Left+4;
				if x<sX-4 then sForm.Left:=sForm.Left-4;
				if y>sY+4 then sForm.Top:=sForm.Top+4;
				if y<sY-4 then sForm.Top:=sForm.Top-4;
	end;
end;

procedure panel_captionMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
// PAMTI X i Y MI�A KAO NULTU TO�KU
// zbog pomicanja
begin
	if ssLeft in shift then begin 
				// Print('Pamtim sX i sY' );
				sX:=X;
				sY:=Y;
	end;
	if (ssDouble in shift) and (sForm.Width>20) then begin 
		// Zatvaranje prozora skripte (zapravo minimizacija, ali nema veze)
		// ali samo ako se radi u editoru	
		if E<>nil then begin 
				// u editoru sam
				// Print('Zatvaram prozor skripte' );
				save_form(sForm,PrgFolder+'skripte\RC_Tools\Semafor\Semafor_veliki');
				read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor_mali'); 
				sForm.Width:=16;
				sForm.Height:=32;	
				poslozi;		
		end;
	end;
end;

procedure Image_resizeMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
// RESIZE forme skripte
var
	k:integer;
begin
	//k:=velicina;
	k:=8;
	Timage(Sender).Cursor :=crSizeNWSE;					// http://wiki.freepascal.org/Cursor
	if ssleft in shift then begin 
				if x>sX+4 then sForm.Width:=sForm.Width+k;
				if x<sX-4 then sForm.Width:=sForm.Width-k;
				if y>sY+4 then sForm.Height:=sForm.Height+k;
				if y<sY-4 then sForm.Height:=sForm.Height-k;
	end;
end;

procedure Image_resizeMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
// PAMTI X i Y MI�A KAO NULTU TO�KU
// zbog promjene veli�ine forme skripte
begin
	if ssLeft in shift then begin 
				// Print('Pamtim sX i sY' );
				sX:=X;
				sY:=Y;
	end;
end;




function Panel3(naziv:string):TPanel;
begin
	result:=rcPanel(naziv,panel_main2);
	// result.name:=naziv;
	// result.caption:='';
	result.Height:=velicina;
	result.top:=10000;
	result.align:=alTop;
	result.Ctl3D:=False;
	result.BevelWidth:=0;
	result.font.name:='System';
end;

Function Label3(cap:string;par:TWinControl):TLabel;
begin
	result:=rcLabel('','',par,sredina-3,1,taRightJustify);
	result.caption:=cap+' ';
	result.font.size:=Velicina-12;
	result.width:=80;
	result.align:=alLeft;
	// result.font.style:=[fsBold];
	
		// result.Height:=20;  NE DE�AVA SE NI�TA
end;

Function Edit3(par:TWinControl):TEdit;
Begin
	// rcEditL(cap, units: string; destination: TWinControl; eX, eY: integer; kind:string):TEdit;
	result:=rcEditL('','',par,sredina,0,'left');
	// result.left:=
	result.width:=100;
	result.font.name:='Arial';
	
	result.font.size:=9;
	// result.width:=60;
	result.align:=alClient;
	result.Ctl3D:=False;
	result.Ctl3D:=False;
	
	// result.font.style:=[fsBold];
		// result.BorderWidth:=0;  NE RADI
		// result.BorderStyle:=bsNone; NE RADI
end;

Function Button3(naslov, opis: string; par:TWinControl):TSpeedButton;
Begin
	
	result:=rcSpeedButton('',naslov,par,0,0);
	// function rcSpeedButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TSpeedButton;
	result.left:=20000;
	result.align:=alRight; 
	result.hint:=opis;
	result.font.name:='Arial'; 
	result.font.size:=7; 
	// result.width:=30;
	result.width:=0;
	
end;

function CheckBox3( cap: string; cbX, cbY: integer):TCheckBox;
// function rcCheckBox(cbName, cbCaption: string; cbParent: TWinControl; cbX, cbY: integer):TCheckBox;
// Create CheckBox
 
 begin
	result:=rcCheckBox('',cap,panel_setup,cbx, cby);
	result.font.size:=8;
	result.width:=150;
	result.Alignment:=taRightJustify;

end;

procedure but_podesiOnClik(Sender: TObject);
Begin
	but_podesi.visible:=false;
	panel_setup.visible:=true;
	
	but_prop.visible:=true;
	panel_main2.visible:=false;
End;

procedure but_propOnClik(Sender: TObject);
Begin

	but_podesi.visible:=true;
	panel_setup.visible:=false;
	
	but_prop.visible:=false;
	panel_main2.visible:=true;
End;




procedure ShowHideOsobine;
Begin
	// Print('Procedure ShowHideOsobine START' );
	
	if SetNaziv.checked 	then panel_naziv.Height:=Velicina
								else panel_naziv.Height:=0;
	if SetSifra.checked 	then panel_Sifra.Height:=Velicina
								else panel_Sifra.Height:=0;
	if SetX.checked 		then panel_X.Height:=Velicina
								else panel_X.Height:=0;							
	if SetFX.checked 		then panel_FX.Height:=Velicina
								else panel_FX.Height:=0;							
	if SetY.checked 		then panel_Y.Height:=Velicina
								else panel_Y.Height:=0;							
	if SetFY.checked 		then panel_FY.Height:=Velicina
								else panel_FY.Height:=0;		
	if SetZ.checked 		then panel_Z.Height:=Velicina
								else panel_Z.Height:=0;							
	if SetFZ.checked 		then panel_FZ.Height:=Velicina
								else panel_FZ.Height:=0;
	if SetSirina.checked then panel_Sirina.Height:=Velicina
								else panel_Sirina.Height:=0;							
	if SetVisina.checked then panel_Visina.Height:=Velicina
								else panel_Visina.Height:=0;
	if SetDebljina.checked and (E<>nil) then panel_Debljina.Height:=Velicina
								else panel_Debljina.Height:=0;
	if SetDubina.checked then panel_Dubina.Height:=Velicina
								else panel_Dubina.Height:=0;
	if SetPrimjedba.checked and (E<>nil) then panel_Primjedba.Height:=Velicina
								else panel_Primjedba.Height:=0;
	if SetNapomena.checked then panel_Napomena.Height:=Velicina
								else panel_Napomena.Height:=0;
	if SetProgram.checked and (E<>nil) then panel_Program.Height:=Velicina
								else panel_Program.Height:=0;
	
	
	// Print('Procedure ShowHideOsobine END' );				
					

	
End;



Procedure SetOsobineReadFromDisk;

Begin
	// Print('SetOsobineReadFromDisk START' );

	// Mogu�i rezultati od find_value_int(put_ini, ...
	// 0 = nisu na�eni u ini. Treba ih prikazati da bi se kasnije mogli spremiti
	// 1 = na�eni su i treba ih pokazati
	// -1 = na�eni su i ne treba ih pokazati
	

	if find_value_int(put_ini,'SetNaziv') = -1 	then SetNaziv.Checked:=False
																else SetNaziv.Checked:=True;
	if find_value_int(put_ini,'SetSifra') = -1 	then SetSifra.Checked:=False						
																else SetSifra.Checked:=True;
	if find_value_int(put_ini,'SetX') = -1 			then SetX.Checked:=False
																else SetX.Checked:=True;
	if find_value_int(put_ini,'SetFX') = -1 		then SetFX.Checked:=False
																else SetFX.Checked:=True;
	if find_value_int(put_ini,'SetY') = -1 			then SetY.Checked:=False
																else SetY.Checked:=True;
	if find_value_int(put_ini,'SetFY') = -1 		then SetFY.Checked:=False
																else SetFY.Checked:=True;
	if find_value_int(put_ini,'SetZ') = -1 			then SetZ.Checked:=False
																else SetZ.Checked:=True;
	if find_value_int(put_ini,'SetFZ') = -1			then SetFZ.Checked:=False
																else SetFZ.Checked:=True;
	if find_value_int(put_ini,'SetSirina') = -1	then SetSirina.Checked:=False
																else SetSirina.Checked:=True;
	if find_value_int(put_ini,'SetVisina') = -1	then SetVisina.Checked:=False
																else SetVisina.Checked:=True;
	if find_value_int(put_ini,'SetDebljina') = -1	then SetDebljina.Checked:=False
																else SetDebljina.Checked:=True;
	if find_value_int(put_ini,'SetDubina') = -1	then SetDubina.Checked:=False
																else SetDubina.Checked:=True;
	if find_value_int(put_ini,'SetPrimjedba') = -1	then SetPrimjedba.Checked:=False
																else SetPrimjedba.Checked:=True;
	if find_value_int(put_ini,'SetNapomena') = -1	then SetNapomena.Checked:=False
																else SetNapomena.Checked:=True;
	if find_value_int(put_ini,'SetProgram') = -1	then SetProgram.Checked:=False
																else SetProgram.Checked:=True;

	
	ShowHideOsobine;
	// Print('SetOsobineReadFromDisk END' );
End;

Procedure set_osobineOnClick(Sender: TObject);
Begin
	// Print('set_osobineOnClick START' );
	ShowHideOsobine;
	// Print('set_osobineOnClick END' );
End;

Procedure Napomena_change(Sender:TObject);
// kad se klikle "OK" tipka od napomena
var
	i:Integer;
Begin
	// SAMO ZA ELEMENTE VRIJEDI
	For i:=0 to Length(lista_elementi)-1 do begin
		lista_elementi[i].napomena:=edit_napomena.text;
	End;

End;

Procedure Primjedba_change(Sender:TObject);
// kad se klikle "OK" tipka od napomena
var
	i:Integer;
Begin
	// SAMO ZA DASKE VRIJEDI
	For i:=0 to Length(lista_daske)-1 do begin
		lista_daske[i].primjedba:=edit_primjedba.text;
	End;

End;

Procedure Program_change(Sender:TObject);
// kad se klikne "OK" tipka od program
var
	i:Integer;
Begin
	// SAMO ZA DASKE VRIJEDI
	For i:=0 to Length(lista_daske)-1 do begin
		lista_daske[i].Bprogram:=edit_program.text;
	End;

End;


procedure FormCloseQuery(Sender: TObject; var CanClose: boolean); 
// aktivira se netom prije zatvaranja prozora, a mo�da i ne :-)   
// U Editoru se ovo nikad ne aktivira zato �to se u Editoru forma nikad niti ne gasi
// U projektu iizgleda da ne radi "sForm.OnCloseQuery"
// 
begin
	Showmessage('Jel Gotovo 1 ????');
	Print('proc: FormCloseQuery START' );  
	
	// Showmessage('FormCloseQuery!');  
	zapamti_pozicije;
	SetOsobineWriteToDisk;
	Print('proc: FormCloseQuery END' );  
end;

procedure eFormOnCloseQuery(Sender: TObject; var CanClose: boolean); 

// E form

// aktivira se netom prije zatvaranja EDITORA
// radi i u projektu i aktivira se zatvaranjem glavne skripte

begin
     Print('proc: eFormOnCloseQuery START' );  
	  // Print('Zatvara se prozor editora!' );  
	  
	  zapamti_pozicije;
	  SetOsobineWriteToDisk;
	 
	  // action:=cafree;
	  // sForm.free;
	  if sform<>nil then sform.free;
	  
	  Print('proc: eFormOnCloseQuery END' );  
end;


procedure sFormOnCloseQuery(Sender: TObject; var CanClose: boolean); 

// S form

// aktivira se netom prije zatvaranja forme od semafora
// radi i u projektu i aktivira se zatvaranjem glavne skripte

begin
     Print('proc: sFormOnCloseQuery START' );  
	  Print('Zatvara se prozor "sForm" tj. Semafor' ); 
	  zapamti_pozicije;
	  SetOsobineWriteToDisk;
	  // action:=cafree;
	  // sForm.free;
	  if sform<>nil then sform.free;
	  Print('proc: sFormOnCloseQuery END' );  
end;


Procedure NapraviFormu;
var
	xo:TObject;
	xM1:TMemo;
begin
  Print('proc: NapraviFormu START' ); 
  // kreiranje forme skripte	
	if E=nil then begin
	
			// PROJECT

			sForm:=tform.create(eForm);
			sForm.parent:=eForm;
			sForm.FormStyle:=
								// fsNormal;					//	  	An ordinary (overlapping) form.
								// fsMDIChild;					//	  	The form is an MDI child.
								// fsMDIForm;					//	  	The form is an MDI parent form, containing MDI child forms.
								fsStayOnTop;				//	  	The form is in the foreground, on top of all other application forms.
								// fsSplash;					//	  	A splash form.
								// fsSystemStayOnTop;	 	//		The form stays system-wide on top.
			

			// ovo radi!
			// sForm:=rcTool('Semafor');
			
			// Print('Projekt: Kreirana je glavna forma sForm' );
			
		end else begin
		
			// EDITOR
			sForm:=tform.create(eForm);
			sForm.parent:=eForm;
			
			// Print('Editor: Kreirana je glavna forma sForm' );
	end;
	
	sForm.align:=alNone;
	sForm.caption:='Semafor '+versiontxt;
	sForm.BorderStyle:=bsNone; // 
	sForm.font.name:='Arial';
	sForm.OnCloseQuery:=@sFormOnCloseQuery; 
	
	
	

	
	

	panel_caption:=rcPanel('',sForm);
	panel_caption.Height:=12;
	panel_caption.Align:=alTop;
	panel_caption.onMouseDown:=@panel_captionMouseDown;
	panel_caption.onMouseMove:=@panel_captionMouseMove;
	panel_Caption.caption:='SEMAFOR' + ' ' + VersionTxt;
	panel_Caption.font.size:=7;
	panel_Caption.font.color:=clGray;
	// function rcButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TButton;
	
	xSlik:=rcLoadImage('xsemy',put_skript+'x.jpg',Panel_Caption,0,0);
	//xSlik.Width:=10;
	xSlik.Align:=alRight;
	xSlik.onMouseDown:=@xSlikMouseDown;
	xSlik.onMouseMove:=@xSlikMouseMove;
	// But_x.Color:=clRed;

	
	panel_bottom:=rcPanel('',sForm);
	panel_bottom.color:=clSilver;
	panel_bottom.Height:=12;
	panel_bottom.Align:=alBottom;

	
	Image_resize:=rcLoadImage('xresize',put_skript+'resize.bmp',Panel_Bottom,0,0);
	// Image_resize.transparent:=true;    NE RADI!!!
	//Image_resize:=10;
	Image_resize.Align:=alRight;
	Image_resize.onMouseDown:=@Image_resizeMouseDown;
	Image_resize.onMouseMove:=@Image_resizeMouseMove;
	
	// Image_up:=rcLoadImage('xup',put_skript+'up.jpg',Panel_Bottom,0,0);
	// Image_up.Align:=alleft;
	
	// Image_down:=rcLoadImage('xdown',put_skript+'down.jpg',Panel_Bottom,0,0);
	// Image_down.Align:=alLeft;
	
	// xSlik.onMouseDown:=@ImageResizeMouseDown;
	// xSlik.onMouseMove:=@ImageResizeMouseMove;
	
	
	// slika semafora
	semslik:=rcLoadImage('semy',put_skript+'semafor.jpg',sForm,0,0);
	semslik.ShowHint:=True;
	semslik.Hint:='Dvostruki klik za otvaranje skripte "Semafor".';
	semslik.onMouseMove:=@SemSlikMouseMove;
	semslik.onMouseDown:=@SemSlikMouseDown;
	semslik.visible:=false;
	//
	
	
	
	panel_id:=rcPanel('',sForm);
	panel_id.Height:=20;
	panel_id.Align:=alTop;
	panel_Caption.font.color:=clBlack;
	
	
	panel_setup:=rcScrollBox('',sForm);
	panel_setup.Align:=alClient;
	panel_setup.visible:=false;
	
	panel_main2:=rcScrollBox('',sForm);
	panel_Main2.Top:=50;
	panel_Main2.Align:=alClient;
	panel_Main2.ParentColor:=False;
	panel_Main2.VertScrollBar.visible:=True;
	panel_Main2.VertScrollBar.Increment:=8;
	// panel_Main2.VertScrollBar.Page:=velicina;
	
	// privremeno se sklanja ovaj panel (zapravo scrollbox) dok se ne napuni
	panel_Main2.visible:=False;  
	
	
	
	// function rcLabel( LName, LCaption: string; LParent: TWinControl; LX, LY:integer; LA:TAlignment):TLabel;
	label_broj:=rcLabel('','',panel_id,1,1,taLeftJustify);
	label_broj.align:=alLeft;
	label_broj.font.size:=12;
	label_broj.font.style:=[fsBold];

	//
	// ======================================================================================
	sredina:=100;
	velicina:=21;		// veli�ina slova u semaforu = velicina - 12
	// 
	// ======================================================================================
	//

	
	but_prop:=Button3('OK','Vrati pogled na osobine', Panel_id);
	but_prop.font.color:=clBlack;
	but_prop.width:=50;
	but_prop.visible:=false;
	but_prop.OnClick:=@but_propOnClik;
	
	// but_var:=Button3('VAR','Varijable selektiranog elementa', Panel_id);
	// but_var.font.color:=clGray;
	// but_var.OnClick:=@but_varOnClik;

	but_podesi:=Button3('Podesi','Pode�avanje prikaza', Panel_id);
	but_podesi.font.color:=clBlack;
	but_podesi.width:=50;
	but_podesi.OnClick:=@but_podesiOnClik;
	
	panel_naziv:=Panel3('Naziv');
	label_naziv:=Label3('Naziv',Panel_naziv);
	OK_naziv:=Button3('OK','Promjeni une�eni sadr�aj',Panel_naziv);
	edit_naziv:=Edit3(Panel_naziv);	edit_naziv.ReadOnly:=True;
	edit_naziv.font.Style:=[fsBold];
	
	panel_sifra:=Panel3('sifra');
	label_sifra:=Label3('�ifra',Panel_sifra);
	OK_sifra:=Button3('OK','Promjeni une�eni sadr�aj',Panel_sifra);
	edit_sifra:=Edit3(Panel_sifra);	edit_sifra.ReadOnly:=True;
	
	panel_x:=Panel3('X');
	label_x:=Label3('X',Panel_x);
	OK_x:=Button3('OK','Promjeni une�eni sadr�aj',Panel_x);
	edit_x:=Edit3(Panel_x);		edit_x.ReadOnly:=True;
	
	panel_fx:=Panel3('fX');
	label_fx:=Label3('F (x)',Panel_fx);
	OK_fx:=Button3('OK','Promjeni une�eni sadr�aj',Panel_fx);
	edit_fx:=Edit3(Panel_fx);	edit_fx.ReadOnly:=True;
		
	panel_y:=Panel3('Y');
	label_y:=Label3('Y',Panel_y);
	OK_y:=Button3('OK','Promjeni une�eni sadr�aj',Panel_y);
	edit_y:=Edit3(Panel_y);	edit_y.ReadOnly:=True;
	
	panel_fy:=Panel3('fY');
	label_fy:=Label3('F (y)',Panel_fy);
	OK_fy:=Button3('OK','Promjeni une�eni sadr�aj',Panel_fy);
	edit_fy:=Edit3(Panel_fy);	edit_fy.ReadOnly:=True;

	panel_z:=Panel3('Z');
	label_z:=Label3('Z',Panel_z);
	OK_z:=Button3('OK','Promjeni une�eni sadr�aj',Panel_z);
	edit_z:=Edit3(Panel_z);		edit_z.ReadOnly:=True;
	
	panel_fz:=Panel3('fZ');
	label_fz:=Label3('F (z)',Panel_fz);
	OK_fz:=Button3('OK','Promjeni une�eni sadr�aj',Panel_fz);
	edit_fz:=Edit3(Panel_fz);		edit_fz.ReadOnly:=True;
	
	panel_sirina:=Panel3('Sirina');
	label_sirina:=Label3('�irina',Panel_sirina);
	OK_sirina:=Button3('OK','Promjeni une�eni sadr�aj',Panel_sirina);
	edit_sirina:=Edit3(Panel_sirina);	edit_sirina.ReadOnly:=True;
	
	panel_visina:=Panel3('Visina');
	label_visina:=Label3('Visina',Panel_visina);
	OK_visina:=Button3('OK','Promjeni une�eni sadr�aj',Panel_visina);
	edit_visina:=Edit3(Panel_visina);	edit_visina.ReadOnly:=True;
	
	panel_debljina:=Panel3('Debljina');
	label_debljina:=Label3('Debljina_d',Panel_debljina);
	OK_debljina:=Button3('OK','Promjeni une�eni sadr�aj',Panel_debljina);
	edit_debljina:=Edit3(Panel_debljina);	edit_debljina.ReadOnly:=True;
	
	panel_dubina:=Panel3('Dubina');
	label_dubina:=Label3('Dubina_e',Panel_dubina);
	OK_dubina:=Button3('OK','Promjeni une�eni sadr�aj',Panel_dubina);
	edit_dubina:=Edit3(Panel_dubina);	edit_dubina.ReadOnly:=True;
			
	panel_primjedba:=Panel3('Primjedba');
	label_primjedba:=Label3('Primjedba_d',Panel_primjedba);
	OK_primjedba:=Button3('OK','Promjeni une�eni sadr�aj',Panel_primjedba);
	OK_primjedba.width:=30;
	OK_primjedba.onClick:=@Primjedba_change;
	edit_primjedba:=Edit3(Panel_primjedba);
	edit_primjedba.font.color:=clRed;
	edit_primjedba.font.style:=[fsBold];
	
	panel_napomena:=Panel3('Napomena');
	label_napomena:=Label3('Napomena_e',Panel_napomena);
	OK_napomena:=Button3('OK','Aktiviraj novu napomenu', Panel_napomena);
	OK_napomena.width:=30;
	OK_napomena.onClick:=@Napomena_change;
	edit_napomena:=Edit3(Panel_napomena);
	edit_napomena.font.color:=clRed;
	edit_napomena.font.style:=[fsBold];
	
	panel_program:=Panel3('program');
	label_program:=Label3('Program_d',Panel_program);
	OK_program:=Button3('OK','Aktiviraj novi program', Panel_program);
	OK_program.width:=30;
	OK_program.onClick:=@Program_change;
	edit_program:=Edit3(Panel_program);
	// edit_program.font.color:=clRed;
	// edit_program.font.style:=[fsBold];
	

	
	// setup
	
	
	// function rcCheckBox(cbName, cbCaption: string; cbParent: TWinControl; cbX, cbY: integer):TCheckBox;
	SetNaziv:=CheckBox3('Naziv (E+D)'				,10,   5); SetNaziv.OnClick:=@set_osobineOnClick;
	SetSifra:=CheckBox3('�ifra (E+D)'				,10,  25); SetSifra.OnClick:=@set_osobineOnClick;
	SetX:=CheckBox3	 ('X (E+D)'						,10,  45); SetX.OnClick:=@set_osobineOnClick;
	SetFX:=CheckBox3	 ('X - formula (E+D)'		,10,  65); SetFX.OnClick:=@set_osobineOnClick;
	SetY:=CheckBox3	 ('Y (E+D)'						,10,  85); SetY.OnClick:=@set_osobineOnClick;
	SetFY:=CheckBox3	 ('Y - formula (E+D)'		,10, 105); SetFY.OnClick:=@set_osobineOnClick;
	SetZ:=CheckBox3	 ('Z (E+D)'						,10, 125); SetZ.OnClick:=@set_osobineOnClick;
	SetFZ:=CheckBox3	 ('Z - formula (E+D)'		,10, 145); SetFZ.OnClick:=@set_osobineOnClick;
	SetSirina:=CheckBox3	 ('Sirina (E+D)'	 		,10, 165); SetSirina.OnClick:=@set_osobineOnClick;
	SetVisina:=CheckBox3	 ('Visina (E+D)'	 		,10, 185); SetVisina.OnClick:=@set_osobineOnClick;
	SetDebljina:=CheckBox3	 ('Debljina (D)'		,10, 205); SetDebljina.OnClick:=@set_osobineOnClick;
	SetDubina:=CheckBox3	 ('Dubina (E)'		 		,10, 225); SetDubina.OnClick:=@set_osobineOnClick;
	SetPrimjedba:=CheckBox3	 ('Primjedba (D)'		,10, 245); SetPrimjedba.OnClick:=@set_osobineOnClick;
	SetNapomena:=CheckBox3	 ('Napomena (E)'	 	,10, 265); SetNapomena.OnClick:=@set_osobineOnClick;
	SetProgram:=CheckBox3	 ('Program (D)'	 	,10, 285); SetProgram.OnClick:=@set_osobineOnClick;
	// SetJournal:=CheckBox3 ('Journal - NE DIRAJ!'	,10, 305); SetJournal.OnClick:=@set_osobineOnClick;
	
	// function rcSpeedButton(bName, bCaption: string; bParent: TWinControl; bX, bY: integer):TSpeedButton;

	// sakrij nepotrebne propertije
	// Showmessage ('1. Provjeri semafor.ini');
	SetOsobineReadFromDisk;
	// Showmessage ('2. Provjeri semafor.ini');
	// ponovo se pokazuje ovaj panel (zapravo scrollbox) jer je sad napunjen
	panel_Main2.visible:=False;  
	
	
	
	
	sForm.show;
	// read_form(sForm, PrgFolder+'skripte\RC_Tools\Semafor\Semafor');
	
	
	// �itanje pozicija
	if e=nil then begin 
	
		// PROJEKT - �itam pozicije
		
		// Print (' U projektu sam i �itam pozicije');
		sForm.Left:=find_value_int(put_ini, 'pLeft');
		if (sForm.Left>1200) or (sForm.Left<0) then sForm.Left:=200;
		sForm.Top:=find_value_int(put_ini, 'pTop');
		if (sForm.Top>1000) or (sForm.Top<0) then sForm.Top:=200;
		sForm.Width:=find_value_int(put_ini, 'pWidth');
		sForm.Height:=find_value_int(put_ini, 'pHeight');
		
		// Showmessage('');
	end else begin
	
		// EDITOR - �itanje pozicija 
		
		// jo� neznam jesam li veliki ili minimiziran
		// Print (' U editoru gledam jesam li mali');
		if find_value_str (put_ini, 'eSize')='minimiziran' then sForm.width:=16;
		// Print (find_value_str (put_ini, 'eSize'));
		
		if sForm.width<20 then begin
			
			// forma je minimizirana
			
			// Print (' U editoru sam mali i �itam pozicije');
			sForm.Left:=find_value_int(put_ini, 'mLeft');
			sForm.Top:=find_value_int(put_ini, 'mTop');
			sForm.Width:=16;
			sForm.Height:=32;

		end else begin

			// Print (' U editoru sam veliki i �itam pozicije');
			sForm.Left:=find_value_int(put_ini, 'eLeft');
			if ((sForm.Left>1920) or (sForm.Left<0)) then sForm.Left:=50;
			sForm.Top:=find_value_int(put_ini, 'eTop');
			if ((sForm.Top>1000) or (sForm.Top<0)) then sForm.Top:=50;
			sForm.Width:=find_value_int(put_ini, 'eWidth');
			sForm.Height:=find_value_int(put_ini, 'eHeight');
		
		end;   // if sForm.width<20
	
	
	end;
	
	poslozi;
	

	
	Print('proc: NapraviFormu END' ); 
end;

procedure Kreiraj_formu;
Begin

End;


Procedure EngineStart(parent:tobject);   // mora se ovako zvati, parent joj je editor
// de�ava se kad se pozove editor
var 
	i:integer;
	par:TForm;
	xo:TObject;
begin 
	MyDate:=Now;
	Print('____________________');
	Print(+FormatDateTime('tt', myDate)+' Skripta: "semaforP" '+versiontxt+		// #13#10+
								 'start [E]');
	
	put_skript:=PrgFolder+'skripte\RC_Tools\Semafor\';
	put_ini:=PrgFolder+'skripte\RC_Tools\Semafor\Semafor.ini';

	eForm:=TForm(twincontrol(parent));    // u prozoru editora!
	
	// ovo se ne mo�e desiti u editoru
	// jer se forma u editoru nikad ne zatvara
	// osim kod ga�enja prozora editora????
	eForm.OnCloseQuery:=@eFormOnCloseQuery; 

	//RCOut('Detektiran je prozor editora. Ima naziv "'+eForm.caption+'"'); 
	
	NapraviFormu;
	
	
	
	
	
	
	
	
end;

Procedure EngineEnd;
// de�ava se kad se zatvori prozor od editora
// u tom su trenutku sve forme i skripte editora ve� zatvorene i ni�ta se iz njih ne mo�e �itati
begin
   // showmessage('EngineEnd');
	// if sform<>nil then sform.free;
	
	
	
	
	
	Print('Skripta Semafor:  Engine End (Editor)');
end;

procedure provjeri_selektirano;
var
	// lista_elementi:array of TElement;
	// lista_daske:array of TDaska;
	i:integer;
	// broj_el - broj selektiranih elemenata deklariran globalno
	// Broj_das - broj selektiranih deklariran deklariran globalno
Begin
	Print('proc: Provjeri_selektirano START' );

	
	// Print('Length(lista_elementi)='+IntToStr(Length(lista_elementi)) );
	
	// obri�i sva polja
	edit_naziv.text:='';
	edit_sifra.text:='';
	edit_x.text:='';
	edit_fx.text:='';
	edit_y.text:='';
	edit_fy.text:='';
	edit_z.text:='';
	edit_fz.text:='';
	edit_sirina.text :='';
	edit_visina.text :='';
	edit_debljina.text :='';
	edit_dubina.text :='';
	edit_primjedba.text :='';
	edit_napomena.text :='';
	edit_program.text :='';
	
	
	For i:=0 to Length(lista_elementi)-1 do begin
		Print('E['+IntToStr(i)+']:'+lista_elementi[i].Naziv );
		if edit_naziv.text =''  then edit_naziv.text:=lista_elementi[i].Naziv
								  else edit_naziv.text:=edit_naziv.text+';'+lista_elementi[i].naziv;
		if edit_sifra.text =''  then edit_sifra.text:=lista_elementi[i].Sifra
								  else edit_Sifra.text:=edit_Sifra.text+';'+lista_elementi[i].Sifra;						  
		if edit_x.text =''  then edit_x.text:=FloatToStrC1(lista_elementi[i].xPos,8,2)
								  else edit_x.text:=edit_x.text+';'+FloatToStrC1(lista_elementi[i].xPos,8,2);
		if edit_fx.text ='' then edit_fx.text:=lista_elementi[i].xF
								  else edit_fx.text:=edit_fx.text + ';'+lista_elementi[i].xF;
		if edit_y.text =''  then edit_y.text:=FloatToStrC1(lista_elementi[i].yPos,8,2)
								  else edit_y.text:=edit_y.text+';'+FloatToStrC1(lista_elementi[i].yPos,8,2);
		if edit_fy.text ='' then edit_fy.text:=lista_elementi[i].yF
								  else edit_fy.text:=edit_fy.text + ';'+lista_elementi[i].yF;						 
		if edit_z.text =''  then edit_z.text:=FloatToStrC1(lista_elementi[i].zPos,8,2)
								  else edit_z.text:=edit_z.text+';'+FloatToStrC1(lista_elementi[i].zPos,8,2);
		if edit_fz.text ='' then edit_fz.text:=lista_elementi[i].yF
								  else edit_fz.text:=edit_fz.text + ';'+lista_elementi[i].zF;	
		if edit_sirina.text =''  then edit_sirina.text:=FloatToStrC1(lista_elementi[i].Sirina,8,2)
								  else edit_sirina.text:=edit_sirina.text+';'+FloatToStrC1(lista_elementi[i].sirina,8,2);								  
		if edit_visina.text =''  then edit_visina.text:=FloatToStrC1(lista_elementi[i].visina,8,2)
								  else edit_visina.text:=edit_visina.text+';'+FloatToStrC1(lista_elementi[i].visina,8,2);	
		if edit_dubina.text =''  then edit_dubina.text:=FloatToStrC1(lista_elementi[i].dubina,8,2)
								  else edit_dubina.text:=edit_dubina.text+';'+FloatToStrC1(lista_elementi[i].dubina,8,2);	
		if edit_napomena.text =''  then edit_napomena.text:=lista_elementi[i].napomena
								  else edit_napomena.text:=edit_napomena.text+';'+lista_elementi[i].napomena;	
								 
		
	End;		// For i:=0 to Length(lista_elementi)-1
	

	
	// Print('Length(lista_daske)='+IntToStr(Length(lista_daske)) );
	For i:=0 to Length(lista_daske)-1 do begin
		Print('D['+IntToStr(i)+']: '+lista_daske[i].Naziv );
		if edit_naziv.text =''  then edit_naziv.text:=lista_daske[i].Naziv
										else edit_naziv.text:=edit_naziv.text+';'+lista_daske[i].naziv;
		if edit_sifra.text =''  then edit_sifra.text:=lista_daske[i].Sifra
										else edit_sifra.text:=edit_sifra.text+';'+lista_daske[i].sifra;
		if edit_x.text =''  then edit_x.text:=FloatToStrC1(lista_daske[i].xPos,8,2)
								  else edit_x.text:=edit_x.text+';'+FloatToStrC1(lista_daske[i].xPos,8,2);
		if edit_fx.text ='' then edit_fx.text:=lista_daske[i].xF
								  else edit_fx.text:=edit_fx.text + ';'+lista_daske[i].xF;
		if edit_y.text =''  then edit_y.text:=FloatToStrC1(lista_daske[i].yPos,8,2)
								  else edit_y.text:=edit_y.text+';'+FloatToStrC1(lista_daske[i].yPos,8,2);
		if edit_fy.text ='' then edit_fy.text:=lista_daske[i].yF
								  else edit_fy.text:=edit_fy.text + ';'+lista_daske[i].yF;						 
		if edit_z.text =''  then edit_z.text:=FloatToStrC1(lista_daske[i].zPos,8,2)
								  else edit_z.text:=edit_z.text+';'+FloatToStrC1(lista_daske[i].zPos,8,2);
		if edit_fz.text ='' then edit_fz.text:=lista_daske[i].yF
								  else edit_fz.text:=edit_fz.text + ';'+lista_daske[i].zF;	
		if edit_sirina.text =''  then edit_sirina.text:=FloatToStrC1(lista_daske[i].Sirina,8,2)
								  else edit_sirina.text:=edit_sirina.text+';'+FloatToStrC1(lista_daske[i].sirina,8,2);								  
		if edit_visina.text =''  then edit_visina.text:=FloatToStrC1(lista_daske[i].visina,8,2)
								  else edit_visina.text:=edit_visina.text+';'+FloatToStrC1(lista_daske[i].visina,8,2);	
		if edit_debljina.text =''  then edit_debljina.text:=FloatToStrC1(lista_daske[i].debljina,8,2)
								  else edit_debljina.text:=edit_debljina.text+';'+FloatToStrC1(lista_daske[i].debljina,8,2);	
		if edit_primjedba.text =''  then edit_primjedba.text:=lista_daske[i].primjedba
								  else edit_primjedba.text:=edit_primjedba.text+';'+lista_daske[i].primjedba;	
		if edit_program.text =''  then edit_program.text:=lista_daske[i].Bprogram
								  else edit_program.text:=edit_program.text+';'+lista_daske[i].Bprogram;	
	End;

	
	Print('Proc: Provjeri_selektirano END' );
End;


Procedure EngineGetObject(sender:tobject);    // select, down, a ne klik
// de�ava se kad se pritisne tipka mi�a u editoru
var
	i:integer;
	elm:TElement;
	td:TDaska;
	// lista_elementi:array [1..100] of TElement;

begin
	Print('EngineGetObject START! -------- ' );
	// ****************************************************************************************************
	// ****************************************************************************************************
	// ****************************************************************************************************	
	
	// tra�im selektirane elemente
		
	ie:=0; // broja� elemenata
	id:=0; // broja� dasaka
	SetLength(lista_elementi,ie);
	SetLength(lista_daske,ie);
	
	if e<>nil then 
		// U ELEMENTU SAM 
		begin  
			
						
			// --------------- U�itaj elemente --------------------
			// Print('Brojim selektirane elemente u Editoru' );
			for i:=0 to E.ElmList.count-1 do Begin  // broji elemente u EDITORU!!!!!
				elm:=tElement(E.ElmList.items[i]);   
				if elm.selected then begin
					SetLength(lista_elementi,ie+1);
					// Print('A�uriram listu "lista_elementi[ie]", ie=' + IntToStr(ie) );
					lista_elementi[ie]:=elm;
					ie:=ie+1;
					// Print('A�uriranje gotovo' );
						
				end;  // if elm.selected 
			end; // for i:=0 to E.ElmList.count-1
			
			Print('Selektirano je '+IntToStr(ie)+'/'+IntToStr(E.ElmList.count)+' elemenata' );
			
			// --------------- U�itaj daske --------------------
			
			// Print('Brojim selektirane daske u Editoru' );
			for i:=0 to e.childdaska.CountObj-1 do begin
				td:=tDaska(e.childdaska.Daska[i]);
				// if e.childdaska.Daska[i].selected = true then id:=id+1;
				if td.selected then begin
					
					SetLength(lista_daske,id+1);
					// Print('A�uriram listu "lista_daske[id]", id=' + IntToStr(id) );
					lista_daske[id]:=td;
					id:=id+1;
					// Print('A�uriranje gotovo' );
				end;  // if td.selected 
			end; // for i:=0 to e.childdaska.CountObj-1
			
			Print('Selektirano je '+IntToStr(id)+'/'+IntToStr(e.childdaska.CountObj)+' dasaka' );
			
			// Print('U elementu postoji '+IntToStr(e.elmlist.countObj)+' objekata.' );
			// Print('Selektirano je '+IntToStr(e.elmlist.countObj)+' objekata.' );
			
			
		end else begin     
		
		// U PROJEKTU SAM PA OVO NE RADI
		

	
	end;  // if e<>nil     
	
	broj_el:=ie;
	broj_das:=id;
	
	Label_broj.caption:=IntToStr(broj_el)+ ' el. + '+	IntToStr(broj_das)+ ' das.';
	provjeri_selektirano;
	
	
	// selektirani_objekti.Free;
Print('EngineGetObject END! ------------ ' );
end;

Procedure CorpusResize(Sender: TObject); 
var
	i:Integer;
Begin
	// ako se koristi ovaj event Corpus radi 
	//	ACCEESS VIOLATION NAKON GA�ENJE SKRIPTE!!!
	rc_MD.Lines.BeginUpdate;
	Print('CorpusResize START! ------------ ' );

	Print('Sirina: '+IntToStr(application.MainForm.width) );
	rc_MD.Lines.EndUpdate;
End;

Procedure ProjektCloseQuery(Sender: TObject; var CanClose: boolean); 
// ZATVARANJE CORPUSA!
Begin
	// Showmessage('Projekt se �eli zatvoriti!');
	Print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' );
	Print('proc: ProjektCloseQuery START!' );
	// Showmessage ('1');
	// zapamti_pozicije;
	// Showmessage ('2');
	// SetOsobineWriteToDisk;
	// Showmessage ('3');
	// Showmessage ('4');
	// sForm.visible:=False;
// * sForm.Close;   // gasi skriptu?
	sForm.visible:=false;  // svoje �e odraditi skripta nakon izlaska iz vizibiliti loopa
	
	Print('ProjektCloseQuery END! ------------ ' );
	
	// EXIT;     // gasi projekt, ali ne i skriptu!
	// Showmessage ('EXIT je PRO�AO');  // NE DOGODI SE NIKAD!
	
End;




var
	rbs, stari_rbs : integer;
	

	
	
begin
	versiontxt:='v1.1';
	MyDate:=Now;
	Print('____________________');
	Print(+FormatDateTime('tt', myDate)+' Skripta: "semaforP" '+versiontxt+		// #13#10+
								 ' start [P]');
	

// ovo se prikazuje samo ako se skripta pokrene u projektu
// Showmessage('BEGIN');
// 

	// presretanje zatvaranja prozora Corpusa!
	application.MainForm.OnCloseQuery:=@ProjektCloseQuery;
	// application.MainForm.OnResize:=@CorpusResize;
	// application.MainForm.OnPaint:=@CorpusOnPaint;

	
	put_skript:=PrgFolder+'skripte\RC_Tools\Semafor\';
	put_ini:=PrgFolder+'skripte\RC_Tools\Semafor\Semafor.ini';
	
	
	// ----- provjera pokrenute skripte
	if find_value_int(put_ini,'work')=1 then begin
		Showmessage ('Skripta je ve� pokrenuta!');
		set_value_int(put_ini,'work',0);
		Print('Skripta je ve� pokrenuta ili je nepravilno isklju�ena');
		MyDate:=Now;
		Print(+FormatDateTime('tt', myDate)+' Skripta: "semaforP" '+versiontxt+ // #13#10+
									 'end [P]');
		Print('____________________');
		// rc_TF.free;
		Exit;
	end;
	
	// ----- upisivanje da je skripta pokrenuta
	set_value_int(put_skript+'Semafor.ini','work',1);
	
	
	
	NapraviFormu;
	
	sForm.Name:='Semafor';
	sForm.OnCloseQuery:=@sFormOnCloseQuery;
	
	// Provjeri_instancu

	
	
	
	
	
	sForm.Show;
	
	while sForm.visible do begin
      application.processmessages;
		
		
		if e=nil then begin
		
			// PROJEKT
			// print('1');
			ie:=0; // broja� elemenata
			id:=0; // broja� dasaka
			rbs:=0;
			SetLength(lista_elementi,ie);
			SetLength(lista_daske,ie);
			// print('2');
			// selektiranih:=0;
			For i:=ElmList.Count-1 DownTo 0 Do begin
					elm2:=tElement(ElmList.items[i]);
					If elm2.Selected Then begin 
						rbs:=i;		// redni broj selektiranog elementa
						SetLength(lista_elementi,ie+1);
						lista_elementi[ie]:=elm2;
						ie:=ie+1;
					end;	// If elm.Selected
			end;
			// print('3');
			broj_el:=ie;
			// broj_das:=id;
			
			if (stari_broj_el<>broj_el) or (stari_rbs<>rbs) then begin
				Label_broj.caption:=IntToStr(broj_el)+ ' el.';
				provjeri_selektirano;
			end;
			stari_broj_el:=broj_el;
			stari_rbs:=rbs;
			// print('4');
			
		
		end;	// if e=nil  
		
		
		
				{
			
			// ShowD('U projektu sam');
			Print('U Projektu sam' );
			for i:=0 to ElmHolder.elementi.count-1 do Begin  // broji elemente u PROJEKTU!!!!!
				elm:=telement(ElmHolder.elementi.items[i]);   
				if elm.selected then begin
						// Elist.addObject(elm.naziv,elm);
				end;  // if elm.selected 
			End; // for i
			
		}
		
		
   end;	// while sForm.visible
	print('nakon "While sForm visible" petlje ');
	set_value_int(put_skript+'Semafor.ini','work',0);  // u ini se upisuje da je forma prirodno zatvorena
	
	sForm.Close;			// ovo �alje u sFormOnCloseQuery 
	
	MyDate:=Now;
	Print(+FormatDateTime('tt', myDate)+' Skripta: "semaforP" '+versiontxt+ // #13#10+
								 'end [P]');
	Print('____________________');
	// Print('Skripta SemaforP END (Projekt)');
end.
