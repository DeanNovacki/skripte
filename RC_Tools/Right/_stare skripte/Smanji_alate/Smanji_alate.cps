Program Micanje_Corpus_Toolbara;   
// Red Cat Ltd 
// v1.0 30.05.2017.

// - 
// - 
// - 

// 

    

// MAIN ////////////////////////////////////////////////////////////////////////
var
   // Startni prozor
	Main:TForm;
   Panel_Left, Panel_right, Panel_Down, Panel_Title, Panel_Info, Panel_Table, Panel_Memo, Panel_MemoO :TPanel;
   Pan_var, Pan_val, Pan_for, Pan_cb, Pan_hint : array of TPanel;
   ButOK,ButPomoc,ButCancel:TButton;

	Debug, MozeZatvaranje, Izvrseno :boolean;    // za debug poruke u Journalu
	
	// ----------------------------------------------
	SystemToolbar:TPanel;   // Commands (tree) on right side of Corpus
   win,ScriptForm: TForm;      // Embeded form of script icons (if possible, without icons)
   ScriptPanel:Tpanel;     // backround for some script
   xo:TObject;
   work:boolean;
   i,ww:integer;
   tt:TButton;
	
	
	
	
	
{$I ..\..\include\rcMessages.cps}
{$I ..\..\include\rcStrings.cps}
{$I ..\..\include\rcControls.cps}
{$I ..\..\include\rcObjects.cps}
{$I ..\..\include\rcEvents.cps}
{$I ..\..\include\rcCurves.cps}
{$I ..\..\include\rcMath.cps}


Procedure CloseOnClick(sender:TObject);
begin 
	// ovo se izvr�ava kad se stisne tipka Close
	// work se stalno provjerava i vrti ukrug sve dok ne bude "true". Onda nastavlja do free-a
   work:=false; 
end;  

Procedure ttShow_hide(sender:TObject);
begin 
   If SystemToolbar.visible=true then SystemToolbar.Visible:=False 
                                 else SystemToolbar.Visible:=True;
end;  



Procedure Napravi_posao;
Begin
	ShowN('*************************');
	ShowN('Ovdje se izvr�ava program');
	ShowN('*************************');
	ShowMessage('POSAO');
	
	// find forms
   // Caption of Form must be in Project.toc, In this case, this is 'Right'
   for i:=0 to application.MainForm.ControlCount-1 do begin
     xo:=application.MainForm.Controls[i];
     if (xo is Tpanel)  then if (TPanel(xo).name='Right') then 
			Begin
			   SystemToolbar:=TPanel(xo) ;      // Corpus panel on right side
				Showmessage ('Na�ao sam traku za skripte!');
			end;
   ShowN ('Sad �u smanjiti izbornik sa skriptama');
	//SystemToolbar.visible:=false;
	application.processmessages;
	if (xo is TForm)   then if (TForm(xo).caption='RCtools') then 
			Begin
				ScriptForm:=TForm(xo);         // Script form 
				// Showmessage ('Na�ao sam RC desnu alatnu traku!');
			end;
   end; 
	
	// SystemToolbar.visible:=False;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
End;

Procedure Pomoc;
var
	PDF:TpdfPrinter;
Begin
	ShowN('Pomo�');
	PDF:=TpdfPrinter.create(1250);
	// PDF.FileName := 'K:\_Corpus 4 RC dev 3\Corpus 4 RC\skripte\Test_polygon\Japin Corpus\skripte\Ponuda\ponuda.pdf';
   // PDF.FileName := 'https://www.youtube.com/watch?v=YRBH87-SKL4&feature=youtu.be';
	// PDF.FileName := 'K:\Zrc\web\PaleMoonRedCat\Moon RC.exe';
	//	PDF.FileName := prgfolder + 'MaterialEditor_4.1.2.54.exe';
	PDF.FileName := prgfolder + 'skripte\RC_Tools\Gerung\gerung.cps'; // Otvara skriptu u editoru :-)
	PDF.ShowPdf;
	ShowN('PDF.ShowPdf - DONE');
	PDF.Free;
End;

// ------------------------------------------------	
{$I Smanji_alate_servis.cps}
// ------------------------------------------------

Procedure ttShow_hide(sender:TObject);
begin 
   If SystemToolbar.visible=true then SystemToolbar.Visible:=False 
                                 else SystemToolbar.Visible:=True;
end;  






// Glavni   
Begin
   Debug:=true;         // debuger ispis on/off
	// Debug:=false;
	Izvrseno:=false;

	
	Create_Main_Window;
	ShowN('skripta: Smanji alate v1.0');
	ShowN('01.06.2017.');
	ShowN('==============');
	// eventi
	cbDebug_OnClick(cbDebug);  // poziva "cbDebug_OnClick" bez da je i�ta kliknuto
	ButOK.OnClick:=@ButOK_OnClick;
	ButCancel.OnClick:=@ButCancel_OnClick;
	ButPomoc.OnClick:=@ButPomoc_OnClick;
	
	ButOK.default:=True;
	ButCancel.ModalResult:=mrCancel;
	
	Panel_Title:=rcPanel ( 'dsafdasf' ,Panel_Left ); 
	Panel_Title.caption:='NASLOV';
	Panel_Title.ALign:=alTop;
	

   // Naslov
   
   // zaglavlje
   
   // tablica
   
   // Main.ShowModal;
	Main.Show;
	
	Napravi_posao;
	
	while Main.visible do begin
        application.processmessages;
    end;
   
   Main.free; 
   
   
End.