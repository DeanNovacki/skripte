var
   win:TForm; but:TButton;
   labx,laby, labw, labh :TLabel;
   labcx,labcy, labcw, labch :TLabel;
   x,y,w,h,cw,ch:integer; 
function rLabel(lname:string; lparent: TWinControl; lx,ly:Integer):TLabel;
var  L1:TLabel;
begin
   L1:=TLabel.create(lparent); L1.parent:=lparent; L1.name:=lname;
   L1.Left:=lx; L1.Top:=ly;
   result:=L1;
end;

// main ==============================================
      
Procedure butClick(sender:TObject);
Begin
   x:=application.MainForm.left;
   y:=application.MainForm.top;
   w:=application.MainForm.width;
   h:=application.MainForm.height;

//   cx:=application.MainForm.clientleft;
//   cy:=application.MainForm.clienttop;
   cw:=application.MainForm.clientwidth;
   ch:=application.MainForm.clientheight;

   labx.caption:='x='+IntToStr(x);   
   laby.caption:='y='+IntToStr(y);    
   labw.caption:='w='+IntToStr(w);
   labh.caption:='h='+IntToStr(h);         
   // labcx.caption:='cx='+IntToStr(w);   // notwork
   //labcy.caption:='cy='+IntToStr(h);    
   labcw.caption:='cw='+IntToStr(cw);
   labch.caption:='ch='+IntToStr(ch);         

End;      
         
begin
   // create form  
   win:=TForm.create(nil);
   win.width:=600; win.height:=600; 
   labx:=rLabel ('x',  win,  10, 10);
   laby:=rLabel ('y',  win,  10, 40);   
   labw:=rLabel ('w',  win,  10, 70);
   labh:=rLabel ('h',  win,  10, 100);   
   labcx:=rLabel ('cx',  win,  70, 10);
   labcy:=rLabel ('cy',  win,  70, 40);   
   labcw:=rLabel ('cw',  win,  70, 70);
   labch:=rLabel ('ch',  win,  70, 100);  
   
   But:=TButton.create(win); But.parent:=win;
   But.caption:='REFRESH'; but.left:=150;  


   //writeln('1');


   //writeln('2');

   // show form and wait 
   // win.showmodal;
   // application.MainForm.onresize:=@resizeForm;
   
   but.onclick:=@butclick;
   win.show;
   win.FormStyle:=fsStayOnTop;
   while true do begin
      application.processmessages;
   end;
   

   // destroy form
   win.free; 
end.