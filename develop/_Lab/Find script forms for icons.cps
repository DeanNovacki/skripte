var
   SystemToolbar:TPanel;   // Commands (tree) on right side of Corpus
   ScriptForm: TForm;      // Embeded form of script icons (if possible, without icons)
   ScriptPanel:Tpanel;     // backround for some script
   xo:TObject;
   work:boolean;
   i:integer;
   tt:TButton;
   {$I \..\_include\rc_controls.cps}
   
Procedure CloseOnClick(sender:TObject);
begin 
   work:=false; 
end;  

Procedure ttShow_hide(sender:TObject);
begin 
   If SystemToolbar.Visible=True then SystemToolbar.Visible:=False 
                                 else SystemToolbar.Visible:=True;
end;  

begin
   work:=true;
   // find forms
   // Caption of Form must be in Project.toc, In this case, this is 'Right'
   for i:=0 to application.MainForm.ControlCount-1 do begin
     xo:=application.MainForm.Controls[i];
     if (xo is Tpanel)  and (TPanel(xo).name='Stool') then SystemToolbar:=TPanel(xo) ;      // Corpus panel on right side
     if (xo is TForm)   and (TForm(xo).caption='Right') then ScriptForm:=TForm(xo);         // Script form 
   end; 
   
   // reveal form   
   ScriptForm.width:=400;                  // reveal form if width is 1 in project.toc
   ScriptForm.BorderStyle:=bsToolWindow;   // or bsSizeToolWin
   // ScriptForm.color:=clBlue;
   // ScriptForm.visible:=true;

   // create panel inside Script Form
   ScriptPanel:=rcPanel('ToolPanel',ScriptForm);
   ScriptPanel.caption:='Super Panel';
   ScriptPanel.align:=alClient;
   ScriptPanel.color:=clWhite;
   
   // create some buttons
   xo:=nil; xo:=find_control(ScriptForm, 'Show_Hide');
   if xo=nil then begin 
                         tt:=rcButton('Show_Hide','Caption',ScriptPanel, 10, 10);
                         tt.width:=70;
                         tt.caption:='Show/hide Tree';
                         tt.OnClick:=@ttShow_Hide;
                   end;
   
   xo:=nil; xo:=find_control(ScriptForm, 'Close_Button');
   if xo=nil then begin 
                         tt:=rcButton('Close_Button','Caption',ScriptPanel, 10, 40);
                         tt.width:=70;
                         tt.caption:='Close!';
                         tt.OnClick:=@CloseOnClick;
                   end;
  
   while work do begin 
         application.processmessages;
   end;
   
   ScriptPanel.free;
   ScriptForm.width:=1;
 
end.