// wall_corner
Procedure ok(sender:Tobject);
var
   i:integer;
   tx, ty, tz, dt, mw: single;
   new_element, path:string;
   Selem:Telement;
   Read_only_variables: boolean;
   tt: string;   // '0+' or ''         - for hidding variable in element
   plate:TDaska;
   el_leg:Telement;
   leg_FL_X, leg_FL_Y, leg_FL_Z,
   leg_FR_X, leg_FR_Y, leg_FR_Z,  
   leg_BL_X, leg_BL_Y, leg_BL_Z,
   leg_BR_X, leg_BR_Y, leg_BR_Z,
   leg_FM_X, leg_FM_Y, leg_FM_Z,
   leg_BM_X, leg_BM_Y, leg_BM_Z,
   Leg_M_X,  leg_M_Y, leg_M_Z,   
   leg_S_X,  leg_S_Y, leg_S_Z: string; 
   npsw:string; 
   nvc:TStringList;   

   
   // left_offset, right_offset, up_offset, down_offset: string;
   // el_door:Telement;
   // door_X,door_Y,door_Z,door_H,door_W,door_D,
   // door_L_X,door_L_Y,door_L_Z,door_L_H,door_L_W,door_L_D,
   // door_R_X,door_R_Y,door_R_Z,door_R_H,door_R_W,door_R_D :string;
Begin

    progress_change(0,'Start!');

    //W_Window.cursor:=crHourglass;
    Button_ok.cursor:=crHourglass;
    Button_ok.enabled:=false;
    
    // read only variables?
    Read_only_variables:=TRUE;
    if Read_only_variables then tt:='0+'
                           else tt:='';    
    // Showmessage('OK');
    message ('Procedure ok(sender:Tobject);');
     
    // save preset to last used
    progress_change(5,'Saving to last preset');
    Save_Preset('Last used');
    
    // save apx heights to ini file
    for i:=1 to 9 do set_value_int(ininame,'AP['+IntToStr(i)+']', APS[i].Tag); 
    
    // save scrolBoxx sroller bar position :-)
    SBSP:=Adj_SB.VertScrollBar.Position;
    set_value_int(ininame,'SBSP', SBSP);
    
    if e=nil then begin    // in project   
                     // selem:=TElement.Create(1000,0,1000,1000,500,500,18);    // created but still not in project or editor
							selem:=TElement.Create(nil);    // created but still not in project or editor
                     selem.naziv:=el_name;
							elmHolder.DodajElement(selem);    // add element to project
                     selem.selected:=true;             // select new element
                 end else begin  // in editor
                     selem:=e;   // element in editor will be changed
    end;

{	// this part is changed with create new element function el_create
    // find element
    progress_change(10,'Finding original element');
    if el_find_selected<>nil then Begin 
                                       // Showmessage('MAIN! Element found!');
                                       selem:= el_find_selected
                                              
                                  end else begin 
                                       Showmessage('MAIN! Element not found!');
    end; // if find_selected_element<>nil
}    
    // replace element
    progress_change(15,'Replace original');
    load_element:=PrgFolder+'elmsav\rce\Type_23'+IntToStr(el_shape)+'.e3d';
    if FileExists(load_element) then new_element:=load_element
								else new_element:=script_path+'rce\Type_23'+IntToStr(el_shape)+'.rce';
    el_replace(selem,new_element);

    // restore position
    progress_change(20,'Set basic properties');
    // set name 
    el_set_name(selem,el_name);

    // set dimensions
    el_set_width(selem,el_width);
    el_set_height(selem,el_height);
    var_set(selem, 'front_distance', tt+IntToStr(32));		

    dt:=0; mw:=0;
    
    if (el_shape=1) or (el_shape=2) then begin
       plate:=plate_find_by_name(selem,'dummy_door');
       if plate<>nil then begin         
                             dt:=plate.debljina;
                             // showmessage('Dummy door thickness: '+FloatToStr(dt)); 
                          end;
       plate:=plate_find_by_name(selem,'mask');
       if plate<>nil then begin         
                             mw:=plate.dubina;
                             // showmessage('mask thickness: '+FloatToStr(mw)); 
                          end;
    end; 
    // showmessage('el_depth before: '+IntToStr(el_depth));                          
    el_depth:=el_depth+round(dt+mw);                  
    el_set_depth(selem,el_depth); 
    // showmessage('el_depth after: '+IntToStr(el_depth));  
	
    selem.MinSirina:=el_width_min;
    selem.MaxSirina:=el_width_max;
    selem.MinVisina:=el_height_min;
    selem.MaxVisina:=el_height_max;
    selem.MinDubina:=el_depth_min;
    selem.MaxDubina:=el_depth_max;
	
	// showmessage('El_shape='+IntToStr(El_shape));
	if el_shape=1 then var_set(selem, 'side_depth', tt+IntToStr(left_depth));
	if el_shape=2 then var_set(selem, 'side_depth', tt+IntToStr(right_depth));
	if el_shape>2 then begin
							// showmessage('El_shape>2 '+IntToStr(El_shape)+' element: '+selem.naziv);
							var_set(selem, 'left_depth', tt+IntToStr(left_depth));
							var_set(selem, 'right_depth', tt+IntToStr(right_depth));
					end;
	
	
    
    selem.locked:=false; npsw:=#114;
    for i:=101 downto 99 do begin npsw:=npsw+chr(i); end;
    npsw:=npsw+#97+chr(StrToInt(IntToStr(6+10*11)));
    for i:=101 to 103 do begin npsw:=npsw+IntToStr(i-100); end;
    //selem.SetNewEditPassword('',npsw);

    //selem.SetNewEditPassword('','dex');
    

    
    // change variable values
    
    // top
    progress_change(25,'Set top');
    var_set(selem, 'top_type', tt+IntToStr(top_type));
    plate:=plate_find_by_name(selem,'Top_F');             // top_thick
    plate.debljina:=top_thick;
    // plate:=plate_find_by_name(selem,'Top_B');
    // if plate<>nil then plate.debljina:=top_thick;
    // var_set(selem, 'top_width_back', tt+IntToStr(top_width_back));    
    // var_set(selem, 'top_width_front', tt+IntToStr(top_width_front));    
    var_set(selem, 'top_dist_front', tt+IntToStr(top_dist_front));
    var_set(selem, 'top_dist_back_fix', tt+IntToStr(top_dist_back_fix));    

    // bottom
    progress_change(30,'Set bottom');
    plate:=plate_find_by_name(selem,'Bottom');             // top_thick
    plate.debljina:=Bottom_thick;
    var_set(selem, 'back_dist', tt+IntToStr(back_dist));
    var_set(selem, 'back_ins', tt+IntToStr(back_ins));    
    var_set(selem, 'bottom_dist_front', tt+IntToStr(bottom_dist_front));
    var_set(selem, 'bottom_type', tt+IntToStr(bottom_type));
    var_set(selem, 'bottom_dist_back_fix', tt+ IntToStr(bottom_dist_back));
  
    
    // back
    
    progress_change(35,'Set back');
    
    back_on:=1;
    plate:=plate_find_by_name(selem,'Back');
    if back_on=1 then begin 
                      var_set(selem, 'back_on', tt+'1');
                      plate.visible:=true;
                 end else begin 
                      var_set(selem, 'Back_on', tt+'0');
                      plate.visible:=false;
                      plate:=plate_find_by_name(selem,'Back_top');
                      message('0');
                      if plate<> nil then plate.visible:=false; 
                      message('1');
    end; // if 

    var_set(selem, 'back_dist', tt+IntToStr(back_dist));

    plate:=plate_find_by_name(selem,'Back');
    if plate<> nil then plate.debljina:=back_thick;

    plate:=plate_find_by_name(selem,'Back_top');
    if plate<> nil then plate.debljina:=back_thick; 
    
    
    var_set(selem, 'back_ins', tt+IntToStr(back_ins));
    var_set(selem, 'back_top_width', tt+IntToStr(back_top_width));
    var_set(selem, 'back_down_width', tt+IntToStr(back_down_width));
    
    
    
    // sides
    progress_change(40,'Set sides');
    
    if not ((el_shape=1) or (el_shape=2)) then begin 
		plate:=plate_find_by_name(selem,'Side_F');
		plate.debljina:=side_thick;
    end;
	
	plate:=plate_find_by_name(selem,'Side_R');
    plate.debljina:=side_thick;
	plate:=plate_find_by_name(selem,'Side_L');
    plate.debljina:=side_thick;

	
	
    // shelves
    progress_change(50,'Set shelves');
    var_set(selem, 'shelf_qnt', tt+IntToStr(shelf_qnt-1));
    message('varijable name: "shelf_qnt"'+'value= '+IntToStr(shelf_qnt));
    case shelf_qnt of           // number of shelves
         1: begin
               plate:=plate_find_by_name(selem,'Shelf_1');plate.visible:=false;
               plate:=plate_find_by_name(selem,'Shelf_2');plate.visible:=false;
               plate:=plate_find_by_name(selem,'Shelf_3');plate.visible:=false;
            end;
         2: begin
               plate:=plate_find_by_name(selem,'Shelf_1');plate.visible:=true;
               plate:=plate_find_by_name(selem,'Shelf_2');plate.visible:=false;
               plate:=plate_find_by_name(selem,'Shelf_3');plate.visible:=false;
            end;
         3: begin
               plate:=plate_find_by_name(selem,'Shelf_1');plate.visible:=true;
               plate:=plate_find_by_name(selem,'Shelf_2');plate.visible:=true;
               plate:=plate_find_by_name(selem,'Shelf_3');plate.visible:=false;
            end;   
     end;
     for i:=1 to 3 do begin
        plate:=plate_find_by_name(selem,'Shelf_'+IntToStr(i));
        plate.debljina:=shelf_thick // shelf thickness
     end;    
     var_set(selem, 'shelf_dist_front', tt+IntToStr(shelf_front_dist));   
     if shelf_mov=1 then var_set(selem, 'shelf_move', tt+'0')
                    else var_set(selem, 'shelf_move', tt+'1');
                    
     if (el_shape=1) or (el_shape=2) then begin
        var_set(selem, 'radius', tt+IntToStr(shelf_radius));               
        var_set(selem, 'cut_f', tt+IntToStr(shelf_radius));
        var_set(selem, 'cut_s', tt+IntToStr(shelf_radius));                         
     end else begin               
        var_set(selem, 'cut_f', tt+IntToStr(shelf_cut_front));
        var_set(selem, 'cut_s', tt+IntToStr(shelf_cut_side));                         
     end;               
                    
     // legs
     progress_change(60,'Set Legs');
     
     el_delete_legs(selem);              // don't like it ;-)

	
    {
    // door   
    door_X:='(side_l.thickness)-(door.depth*cos(90-y_kut))+left_offset*cos(y_kut)';                  
    door_Y:='foot_height+down_offset';                       
    door_Z:='(0-depth+right_depth)+(door.depth*sin(90-y_kut))+left_offset*sin(y_kut)';               
    door_Ya:='beta';
    door_H:='height-foot_height-up_offset-down_offset';  
    door_W:='hipo-up_offset-down_offset';           
    door_D:='door.depth';                                  
    
   if var_exist(selem, 'door_X') then var_set(selem, 'door_X', '0+'+door_X)
                                 else var_add(selem, 'door_X', '0+'+door_X);
   if var_exist(selem, 'door_Y') then var_set(selem, 'door_Y', '0+'+door_Y)
                                 else var_add(selem, 'door_Y', '0+'+door_Y);
   if var_exist(selem, 'door_Z') then var_set(selem, 'door_Z', '0+'+door_Z)
                                 else var_add(selem, 'door_Z', '0+'+door_Z);
   if var_exist(selem, 'door_Ya')then var_set(selem, 'door_Ya','0+'+door_Ya)
                                 else var_add(selem, 'door_Ya','0+'+door_Ya);    
   if var_exist(selem, 'door_W') then var_set(selem, 'door_W', '0+'+door_W)
                                 else var_add(selem, 'door_W', '0+'+door_W);
   if var_exist(selem, 'door_H') then var_set(selem, 'door_H', '0+'+door_H)
                                 else var_add(selem, 'door_H', '0+'+door_H);
   if var_exist(selem, 'door_D') then var_set(selem, 'door_D', '0+'+door_D)
                                 else var_add(selem, 'door_D', '0+'+door_D);
                                                                                          
   // add door
   progress_change(70,'Loading doors');
   path:=script_path+'rce\front01.rce';
   el_door:=element_add_f( path,'door',selem,door_X,door_Y,door_Z);  
   el_door.Xformula:=door_X;
   el_door.Yformula:=door_Y;
   el_door.Zformula:=door_Z;
   //el_door.YkFormula:=door_Ya;
   el_door.Sformula:=door_W;
   el_door.Hformula:=door_H;
   el_door.Dformula:=door_D;
   // handle up			
	if var_exist(el_door, 'front_base') then var_set(el_door, 'front_base', tt+IntToStr(0));  
   }
        
        
        
        
        
        
       // set position
    tx:=el_xp;
    ty:=el_yp;
    tz:=el_zp;
    
    selem.Xpos:=tx;
    selem.Ypos:=ty;     
    if selem.Dubina>tz then selem.Zpos:=selem.Dubina
                       else selem.Zpos:=tz;
    selem.selected:=false;  
    

    
        // add extra variables 
    // ************************************************************************* 
    progress_change(90,'Looking for extra variables...');
    nvc:=TStringList.Create;
    try nvc.LoadFromFile(script_path+'extra_variables.txt');
        except ShowMessage( 'Cannot load "extra_variables.txt"!');
    end; 
    for i:=0 to nvc.count-1 do begin
        // Showmessage('nvc.Names['+IntToStr(i)+']='+nvc.Names[i]);
        if var_exist(selem,nvc.Names[i]) then begin 
                                                   // Showmessage ('Exist, so changing');
                                                   selem.fvarijable.strings[i]:=nvc.strings[i]
                                         end else begin
                                                  // Showmessage ('Not exist, so adding '+nvc.strings[i]); 
                                                  selem.fvarijable.add(nvc.strings[i])
                                         end; 
    end;    
    
    progress_change(95,'Recalculate...');
    selem.RecalcFormula(selem);
    
    progress_change(100,'Done...');
End;