

var
   MyDate:TDateTime;
Begin
   MyDate:=Now;
   Writeln('datum')
   Writeln('   DateToStr(MyDate)  = ' + DateToStr(MyDate));
   //  4.2.2014.
   
   Writeln('                d/m/y = ' + FormatDateTime('d/m/y', myDate));
   //  4.2.14
   
   Writeln('             dd/mm/yy = ' + FormatDateTime('dd/mm/yy', myDate));
   // 04.02.14
   
   Writeln('    ddd d of mmm yyyy = ' + FormatDateTime('ddd d of mmm yyyy', myDate));
   // uto 4 of vlj 2014 
                          
   Writeln('  dddd d of mmmm yyyy = ' + FormatDateTime('dddd d of mmmm yyyy', myDate));
   // utorak 4 of velja�a 2014
   
   Writeln('                ddddd = ' + FormatDateTime('ddddd', myDate));
   // 4.2.2014.
   
   Writeln('               dddddd = ' + FormatDateTime('dddddd', myDate));
   //  4. velja�a 2014.
   
   Writeln('                    c = ' + FormatDateTime('c', myDate));
   // 4.2.2014. 2:15:03 
   
   Writeln('vrijeme')
   
   Writeln('     h:n:s.z = '+FormatDateTime('h:n:s.z', myDate));
   // 2:15:3.327

   Writeln('hh:nn:ss.zzz = '+FormatDateTime('hh:nn:ss.zzz', myDate));
   // 02:15:03.327
   
   Writeln('           t = '+FormatDateTime('t', myDate));
   // 2:15
   
   Writeln('          tt = '+FormatDateTime('tt', myDate));
   // 2:15:03
   
   Writeln('           c = '+FormatDateTime('c', myDate));
   // 4.2.2014. 2:15:03
   
End.