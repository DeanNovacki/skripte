function MessageYesNo(	const MWidth, MHeight:Integer;	
                        ACaption, APrompt: string;
                        TxtOK, TxtCancel:string;	// buttons text
								pass:boolean
						//		var 		Value: string
							): Boolean;
var
  MForm: TForm;
  Prompt: TLabel;
  Edit: TEdit;
  DialogUnits: TPoint;
  ButtonTop, ButtonWidth, ButtonHeight: Integer;
begin
  Result := False;
  MForm := TForm.Create(Application);
  with MForm do
    try
//      Canvas.Font := Font;
      DialogUnits.x := 10;
      DialogUnits.y := 12;
      BorderStyle := bsDialog;
      Caption := ACaption;
      ClientWidth := MWidth;
      ClientHeight:=MHeight;      
      Position := poScreenCenter;
      Prompt := TLabel.Create(MForm);
      with Prompt do
      begin
        Parent := MForm;
        Caption := APrompt;
        Left:=15;
        Top:=15;
        Width:=MForm.ClientWidth-30;
        WordWrap := True;
      end;
      ButtonTop := Prompt.Top + Prompt.Height + 15;
      ButtonWidth := round((MForm.ClientWidth-3*30)/2)
      ButtonHeight := 20;
      with TButton.Create(MForm) do
      begin
        Parent := MForm;
        Caption := TxtOK;
        ModalResult := mrOk;
        Default := True;
        left:=30;
        top:=ButtonTop;
        Width:=ButtonWidth;
        Height:=ButtonHeight;
      end;
      with TButton.Create(MForm) do
      begin
        Parent := MForm;
        Caption := TxtCancel;
        ModalResult := mrCancel;
        Cancel := True;
        left:=60+ButtonWidth;
        top:=ButtonTop;
        Width:=ButtonWidth;
        Height:=ButtonHeight;
        MForm.ClientHeight := Top + Height + 13;          
      end;
      if ShowModal = mrOk then
      begin
        // Value := Edit.Text;
        Result := True;
      end;
    finally
      MForm.Free;
    end;
end;




begin

// tmp:='Tekst polja';  // varijabla tmp se mo�e promijeniti i �itati nakon funkcije

if MessageYesNo(300,150,'Naslov prozora', 'Neki tekst poruke ','Mo�e','Zaboravi',false) then Showmessage('Pristao je!');

end.