// kreira prozor s potro�nim i vra�a �ifru selektiranog potro�nog
// ulaz: �ifra potro�nog - da bi se lako provjerilo da li postoji
// izlaz: �ifra potro�nog




var
   rcBok : Boolean;
	
	// liste sa svim okovima
	rcListGrupa, 
	rcListNaziv,
	rcListSifra,
	rcListCijena,
	rcListSlika,
	
	// Sav okov (neovisno o sadr�ju Memoa)
	UpSavOkov : Tstringlist;


	UPpotwin:TForm;
		UPpotpan:TPanel;
			UPpanNaslov : TPanel;
				UPnasLabel : TLabel;
			UPpanLista : TPanel;
				UPListaOkov : tListBox;
				
			UPpanSearch : tPanel;
				// UPLabelSearch	: tLabel;
				UPeditSearch 	: tEdit;
				UpButtonSearch,
				UpButtonClear	: tButton;
				
				UPbutRefresh 	: tButton;
				UPlabRefresh : Tlabel;
				
				
			UPpanDetalji : tPanel;
				UPPanSlika : tPanel;
				UPslikaPot : tmImage;
				
				UPispGrupa : TEdit;
			   UPispIme : TMemo;
				UPispSifra,
				UPispCijena : TEdit;
				
				
				
				UPbutGet : tButton;
	
	
procedure upLoadLists;
// u�itava stare liste koje su (vjerojatno) nekad prije kreirane
// samo puni StringListe. Ne stavlja ni�ta u Memo
var
	tempPath:string;
	
Begin
	tempPath:=prgFolder+'skripte\Temp\';	
	// pripremi liste
	
	print('upLoadLists start');
	
	rcListGrupa :=TStringList.Create;
	rcListNaziv :=TStringList.Create;
	rcListSifra :=TStringList.Create;
	rcListCijena:=TStringList.Create;
	rcListSlika :=TStringList.Create;
	
	// u�itaj listu Grupa
	if FileExists(tempPath+'rcListGrupa.data') then begin
		try rcListGrupa.LoadFromFile(tempPath+'rcListGrupa.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListGrupa.data');
		end;  
	end else begin
		// lista ostaje prazna
	end;
	
	// u�itaj listu Naziv
	if FileExists(tempPath+'rcListNaziv.data') then begin
		try rcListNaziv.LoadFromFile(tempPath+'rcListNaziv.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListNaziv.data');
		end;  
	end  else begin
		// lista ostaje prazna
	End; 
	
	// u�itaj listu Sifra
	if FileExists(tempPath+'rcListSifra.data') then begin
		try rcListSifra.LoadFromFile(tempPath+'rcListSifra.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListSifra.data');
		end;  
	end  else begin
		// lista ostaje prazna
	End; 
	
	// u�itaj listu Cijena
	if FileExists(tempPath+'rcListCijena.data') then begin
		try rcListCijena.LoadFromFile(tempPath+'rcListCijena.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListCijena.data');
		end;  
	end  else begin
		// lista ostaje prazna
	End; 
	
	// u�itaj listu Slika
	if FileExists(tempPath+'rcListSlika.data') then begin
		try rcListSlika.LoadFromFile(tempPath+'rcListSlika.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListSlika.data');
		end;  
	end  else begin
		// lista ostaje prazna
	End; 

	// Print('Lista ima ' + IntToStr(rcListGrupa.Count)+' elemenata' );
	// Print('Lista ima ' + IntToStr(rcListNaziv.Count)+' elemenata' );
	// Print('Lista ima ' + IntToStr(rcListSifra.Count)+' elemenata' );
	// Print('Lista ima ' + IntToStr(rcListCijena.Count)+' elemenata' );
	// Print('Lista ima ' + IntToStr(rcListSlika.Count)+' elemenata' );

	
	print('upLoadLists end');
	
end;

Procedure upFillMemos;
var
	i:Integer;

Begin 
	print('upFillMemos start');

	UPListaOkov.Clear;
	
	// Lista:=TStringList.create;
	UpSavOkov:=TStringList.Create;
	
	UPListaOkov.Items.BeginUpdate;
	For i:=0 to rcListGrupa.Count-1 do begin
		// print(IntToStr(i));
		
		if (i mod 243) = 0 then rcProgress('U�itavam listu okova','Okov broj:',i,rcListGrupa.Count-1);
		
		UPListaOkov.Items.Add(  rcListGrupa[i] + ' | ' + 
										rcListSifra[i] + ' | ' + 
										rcListNaziv[i] + ' | '+ 
										'rb="' + IntToStr(i)+'"')  ;
		// UPmemoNaziv.Lines.Add(rcListNaziv[i]);
		// UPmemoSifra.Lines.Add(rcListSifra[i]);
		UpSavOkov.Add (UPListaOkov.Items[i]);
	
	End;
	UPListaOkov.Items.EndUpdate;
	
	rcProgress('','',10,10);
		
	
	print('upFillMemos end');
	
End;

	
procedure butRefreshOnClick(sender:tObject);	
// kreira ponovo sve liste i sprema ih u "...skripte/Temp"

var
	listFolder 	: TStringList;
	cxm 			: tStringList;
	red 			: string;
	grupa 		: String;
	i,j			: Integer;
	tempPath		: String;
Begin
	PrintTime ('upRefreshLists start')
	
	rcCreateTemp;  // kreira temp folder (ako ne postoji). Tu �e se spremiti skra�ene liste okova
	
	// otkrij grupe
	listFolder := TStringList.Create;
	listFolder := rcDirFiles(prgFolder+'Potrosni', '*.cxm');
	
	// test ListFolder-a
	// for i:=0 to ListFolder.count-1 do begin
	//		print (ListFolder[i]);
	// End;

	// o�isti formu
	UPeditSearch.text:='';
	UPispIme.Clear;
	UPispGrupa.text:='';
	UPispSifra.text:='';
	UPispCijena.text:='';
	
	// pripremi liste
	rcListGrupa .Clear;
	rcListNaziv .Clear;
	rcListSifra .Clear;
	rcListCijena.Clear;
	rcListSlika .Clear;
	
	// napuni liste
	cxm :=TStringList.Create;
	
	for i:=0 to ListFolder.Count-1 do begin   // pro�i kroz sve cxm-ove
	// for i:=0 to 1 do begin   // pro�i kroz sve cxm-ove
		if Contain(rcCaps(ListFolder[i]),'TRAKE.CXM') then Continue;  // PRESKO�I!!!
		// kreiranje cxm liste - ZAJEDNI�KI potro�ni 
		cxm.Clear;
		
		// Print ('Obra�ujem listu: '+ListFolder[i]); 
		// labRefresh.text:='U�itavam '+ListFolder[i];
		// PrintTime ('Load start: '+ListFolder[i]); 
		
		try cxm.LoadFromFile(prgFolder+'\potrosni\'+ListFolder[i]);
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+ListFolder[i]);
		end; 
		// PrintTime ('Load end: '+ListFolder[i]); 
		
		// pro�i sve redove cxm file-a
		// PrintTime ('Extract start: '+ListFolder[i]); 
		for j:=0 to cxm.count-1 do begin
		
			// rcProgress('U�itavam okov',ListFolder[i],i,upSavOkov.Count-1);
			
			if (j mod 243) = 0 then rcProgress('U�itavam grupu',ListFolder[i],j,cxm.count-1);

			red:=cxm[j];
			
			// Na�i naziv grupe grupe
			if Contain(red,'<Potrosni') then begin
				grupa := rcFindVar2(red,'FILE');

				j:=j+1; 					// idi u slijede�i red jer sam na�ao naziv grupe
				red:=cxm[j];
			end;	
				
			// puni Grupu, Naziv i �ifru
			if Contain(red,'<PotItem ')  then begin
				rcListGrupa.Add(grupa);
				rcListNaziv.Add(rcFindVar2(red,' Naziv'));
				rcListSifra.Add(rcFindVar2(red,' oznakaP'));
				j:=j+1; 					// idi u slijede�i red jer ovdje nema vi�e ni�ta zanimljivo
				red:=cxm[j];
			end;
			
			// puni Cijenu
			if Contain(red,'<Data ')  then begin	
				rcListCijena.Add(rcFindVar2(red,' Cijena'));
				j:=j+1; 					// idi u slijede�i red jer ovdje nema vi�e ni�ta zanimljivo
				red:=cxm[j];
			End;
			
			// puni putanju za sliku
			if Contain(red,'<Dodatno ')  then begin	
				rcListSlika.Add(rcFindVar2(red,' MainPic'));
				j:=j+1; 					// idi u slijede�i red jer ovdje nema vi�e ni�ta zanimljivo
			End;
			
			{
			ii:=rcListGrupa.count-1;
			print (	'RB:'+IntToStr(ii)+' ----------------' +#13#10+
						'GR:'+rcListGrupa[ii]+#13#10+
						'NA:'+rcListNaziv[ii]+#13#10+
						'SI:'+rcListSifra[ii]+#13#10+
						'CI:'+rcListCijena[ii]+#13#10+
						'SL:'+rcListSlika[ii]);
			}
			
		end; // for j - redovi u jednom cxmu
		
		// PrintTime ('Extract end: '+ListFolder[i]); 
		
		// rcProgress('','',1,1);
	
	end;  // for i - cxm-ovi
	
	rcProgress('Spremanje u temp','  ',10,20);
	
	// spremanje na disk
	tempPath:=prgFolder+'skripte\Temp\';	

	rcListGrupa.SaveToFile(tempPath+'rcListGrupa.data');
	rcListNaziv.SaveToFile(tempPath+'rcListNaziv.data');
	rcListSifra.SaveToFile(tempPath+'rcListSifra.data');
	rcListCijena.SaveToFile(tempPath+'rcListCijena.data');
	rcListSlika.SaveToFile(tempPath+'rcListSlika.data');

	// print ('nova baza spremljena na disc');
	
	rcProgress('Gotovo','  ',20,20);
	
	
	// 
	upLoadLists;
	print ('upLoadLists nakon ru�nog refresha');
	upFillMemos;
	print ('puni Memo nakon ru�nog refresha');
	
	PrintTime ('upRefreshLists end')
End;



Procedure UplistaOkovClick(Sender: TObject);		
var
	rb,ii : integer;
	// eee:TScreen;
Begin
	// Print('Klik');
	ii:=tListBox(Sender).ItemIndex;
	// Print('ItemIndex='+IntToStr(ii));
	try rb:=StrToInt(rcFindVar2(UPListaOkov.Items[ii],'rb'));
		except ShowMessage( 'rb Nije broj!');
   end; 
	// Print('RB='+IntToStr(rb));
	
	UPispIme.Clear;
	UPispIme.lines.add(rcListNaziv[rb]);
	UPispIme.SelStart:=0;
	UPispIme.SelLength:=0;
	
	UPispGrupa.text:=(rcListGrupa[rb]);
	
	UPispSifra.text:=(rcListSifra[rb]);
	
	UPispCijena.text:=(rcListCijena[rb]);
	
	if length(rcListSlika[rb])>5 
		then 	if FileExists(PrgFolder+'katalog\'+rcListSlika[rb])
					then UPslikaPot.LoadFromFile(PrgFolder+'katalog\'+rcListSlika[rb])
					else UPslikaPot.Picture:=nil
		else UPslikaPot.Picture:=nil;
	
End;

Procedure upFiltriranje;
VAR
	i:integer;
Begin
	print('Procedure upFiltriranje start');
	
	UPListaOkov.Items.BeginUpdate;
	For i:=0 to upSavOkov.Count-1 do begin
		if Contain(rcCaps(upSavOkov[i]),rcCaps(UpEditSearch.Text)) then Begin
			UPListaOkov.Items.Add(upSavOkov[i]);
		end;
		
		
		if (i mod 243) = 0 then rcProgress('Filtiranje okova','Provjeravam okov',i,upSavOkov.Count-1);
		
	End;
	UPListaOkov.Items.EndUpdate;
	
	rcProgress('Filtiranje okova','Gotovo',10,10);
	
	
	print('Procedure upFIltriranje End');

End;

Procedure ClearOnClick(Sender:TObject);
var
	i:integer;
Begin
	print ('ClearOnClick start');
	
	UPeditSearch.clear;
	
	UPListaOkov.Items.BeginUpdate;
	
	UPListaOkov.Items.Assign(upSavOkov);
	
	UPListaOkov.Items.EndUpdate;
	
	print ('ClearOnClick end');
End;

Procedure SearchOnClick(Sender:TObject);

Begin
	if Length(UpEditSearch.Text)>0  then begin
		// print('Procedure SearchOnChange');
		
		// o�isti formu
		// UPeditSearch.text:='';
		// UPispIme.Clear;
		// UPispGrupa.text:='';
		// UPispSifra.text:='';
		// UPispCijena.text:='';
		
		// o�isti ListBox
		UPListaOkov.clear;
		
		UpFiltriranje;
		
	end;

End;

Procedure UPbutGetOnClick(Sender:TObject);

Begin
	If UPispSifra.Text<>'' then UPpotwin.Close;
	rcBOK:=True;
End;
		
Function UcitajPotrosni(sifra:string; left, top:integer):string;
var
	// listFolder 	: TStringList;
	i, j, ii		: integer;
	
Begin
	rcBOK:=false;
	printTime ('UcitajPotrosni Start');
	
	print('Kreiram formu');
	// upKreirajFormu;
	// kreiraj formu
	UPpotWin:=rcTool('Potrosni');
	UPpotWin.FormStyle:=fsStayOnTop; 
	UPpotWin.Width:=600;
	UPpotWin.Height:=535;
	
	UPpotWin.Font.Size:=9;
	
		// UPpanNaslov:=rcPanel('',UPpotWin);
		
		// 	UPnasLabel:=rcLabel('', 'Grupa' + ' || ' + 'Sifra ' + ' || ' + 'Naziv' , UPpanNaslov, 5, 3, taLeftJustify);
		// 	UPnasLabel.font.size:=10;

		UPpanLista:=rcPanel('',UPpotWin);
		UPpanLista.Height:=250;
		UPpanLista.Align:=alTop;
		
			// UPmemoOkov:=rcMemo('', UPpanLista, 1, 1, 100, 10);
			UplistaOkov:=tListBox.create(UPpanLista);
			UplistaOkov.parent:=UPpanLista;
			UplistaOkov.Align:=alClient;
			// UplistaOkov.Sorted:=True;
			// UplistaOkov.font.name:='Courier New';
			UplistaOkov.OnClick:=@UplistaOkovClick;
			
			
		UPpanSearch:=rcPanel('',UPpotWin);
		UPpanSearch.Height:=30;
		UPpanSearch.Top:=1000;
		UPpanSearch.Align:=alTop;
		
			UPeditSearch:=rcEditL('Filter'+':','',UPpanSearch,100 ,3,'right');
			UPeditSearch.Width:=100;
			
			
			UpButtonSearch:=rcButton('', 'Tra�i', UPpanSearch, 150,2);
			UpButtonSearch.Width:=100;
			UpButtonSearch.OnClick:=@SearchOnClick;
			
			UpButtonClear:=rcButton('', 'O�isti', UPpanSearch, 255,2);
			UpButtonClear.Width:=100;
			UpButtonClear.OnClick:=@ClearOnClick;
			
			UPbutRefresh:=rcButton('', 'Obnovi', UPpanSearch, 500, 3);
			UPbutRefresh.Hint:='Pokrenuti ako je u Corpus dodavan novi okov';
			UPbutRefresh.Height:=21;
			UPbutRefresh.Width:=80;
			UPbutRefresh.OnClick:=@butRefreshOnClick;

		UPLabRefresh:=rcLabel('', 'status', UPpanDetalji, 180, 135, taLeftJustify);
		UPLabRefresh.font.color:=clSilver;
			
		
		UPpanDetalji:=rcPanel('',UPpotWin);
		UPpanDetalji.Height:=170;
		UPpanDetalji.Top:=1000;
		UPpanDetalji.Align:=alClient;
		
			UPpanSlika:=rcPanel('',UPpanDetalji);
			UPpanSlika.Width:=245;
			UPpanSlika.Height:=205;
			UPpanSlika.Top:=10;
			UPpanSlika.Left:=10;
			
			UPslikaPot:=TMImage.create(UPPanSlika);
			UPslikaPot.Parent:=UPPanSlika;
			UPslikaPot.AutoSize:=False;
			UPslikaPot.stretch:=true;
			// UPslikaPot.proportional:=True; NE RADI  :-(
			UPslikaPot.Width:=240;
			UPslikaPot.Height:=200;
			UPslikaPot.Left:=2;
			UPslikaPot.Top:=2;
			// Align:=alClient;
		
		UPispIme	:=rcMemo('', UPpanDetalji, 270, 10, 10, 90);
		
		
		UPispGrupa	:=rcEditL('Grupa' +':', '', UPpanDetalji, 400, 105, 'right');
		UPispSifra	:=rcEditL('Sifra' +':', '', UPpanDetalji, 400, 130, 'right');
		UPispCijena:=rcEditL('Cijena' +':', '', UPpanDetalji, 400, 155, 'right');
		
		// UPispIme.readonly:=true;
		UPispIme.color:=clWindow;
		UPispIme.Width:=UPpanDetalji.Width-280;
		
		UPispGrupa.readonly:=true;
		UPispGrupa.color:=clWindow;
		UPispGrupa.Width:=200;

		UPispSifra.readonly:=true;
		UPispSifra.color:=clWindow;
		UPispSifra.Width:=200;

		UPispCijena.readonly:=true;
		UPispCijena.color:=clWindow;
		UPispCijena.Width:=200;

		UPbutGet:=rcButton('', 'Preuzmi', UPpanDetalji, 270, 185);
		UPbutGet.width:=UPpanDetalji.Width-280;
		UPbutGet.Height:=30;
		
		UPbutGet.OnClick:=@UPbutGetOnClick;
		
	UPpotWin.Left:=Left;
	UPpotWin.Top:=Top;
	// UPpotWin.Show;
	
	// print (sifra);
	// UPeditSifra.Text:=sifra;
	UPeditSearch.Text:=sifra;
	
	application.processmessages;  // da ne izgleda ru�no dok se �eka
	
	print('Ucitavam liste');
	upLoadLists;			// u�itaj stare liste
	upFillMemos;			// napuni Memo-e

	
	UPpotWin.ShowModal;
	if rcBok then result:=UPispSifra.Text
				else result:=Sifra;
	
	printTime ('UcitajPotrosni End');
End;
