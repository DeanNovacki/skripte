Program upute;  
// v1.00 (19.08.2015)
// Skripta koja uklanja makroe

{$I ..\..\RC_Tools\Include\rcMessages.cps}
//{$I ..\..\include\rcStrings.cps}
{$I ..\..\RC_Tools\Include\rcControls.cps}
//{$I ..\..\include\rcObjects.cps}
//{$I ..\..\include\rcEvents.cps}
//{$I ..\..\include\rcCurves.cps}
//{$I ..\..\include\rcMath.cps}
         
Procedure upute;
var
	PDF:TpdfPrinter;
	dato:string;
	Memko:TMemo;
	Forko:TForm;
Begin

   // dato:=PrgFolder+'skripte\RC_Tools\podsjetnik.rtf';
	dato:=PrgFolder+'\Skripte\Editor\Makro_01\upute.pdf';
	// Showmessage(dato);
   
	
	if fileexists(dato) then begin 
			PDF:=TpdfPrinter.create(1250);
			PDF.FileName := dato;
			PDF.ShowPdf;
			PDF.Free
		end else begin
			// function rcMemo(mName:string; mParent: TWinControl; mX, mY, mS, mV: integer):TMemo;
			// Showmessage('Nema podsjetnika');
			Forko:=rcTool('Red_Cat');
			// Showmessage('20');
			Memko:=rcMemo('mm',Forko,1,1,50,50);
			// Showmessage('30');
			Memko.Lines.SaveToFile(dato);
			PDF:=TpdfPrinter.create(1250);
			PDF.FileName := dato;
			PDF.ShowPdf;
			PDF.Free;
	end;
	
End;

Begin
	Upute;

End.
{			
			var
   Prozor:TForm;
   M1:TMemo;
	putanja:string;


Begin
  // showmessage ('Upute');
  // Function rcTool(tName:string):TForm;
  Prozor:=rcTool('Red_Cat');
  Prozor.BorderStyle:=bsSizeToolWin;
  Prozor.Caption:='Upute za postavljanje spojeva';
  Prozor.Left:=20;
  Prozor.Top:=100;
  Prozor.width:=400;
  Prozor.Height:=600;
  
  M1:=rcMemo('M10',Prozor,100,100,30,30);
  M1.align:=alClient; 
  M1.ScrollBars:=ssVertical;
  M1.ReadOnly:=True;
  M1.Font.color:=clMaroon;
  M1.Color:=clSilver;
  
  putanja:=PrgFolder+'\Skripte\Editor\Makro_01\';
  M1.Lines.LoadFromFile(putanja+'Upute.txt');
  
  prozor.showmodal;
  Prozor.free;
  
End.
}