// Math Functions and Formating

Function Sqr(a:single):single;
// kvadriranje
begin
     result:=a*a;
end;

Function Tg(a:single):single;
// tangens
begin
     result:=sin(a)/cos(a);
end;

Function Ctg(a:single):single;
// kotangens
begin
     result:=cos(a)/sin(a);
end;

Function Contain(tekst,ss:String):Boolean;
// pretra�uje da li u nutar stringa tekst postoji string ss
Var
   i:Integer;
Begin
   result:=false;
   For i:=1 to Length(tekst) do begin
      if copy(tekst,i,Length(ss))=ss then begin
                                             result:=true;
                                             Break;
                                          end;   
   
   End;
End;

Function rcFloatToStr(num:single;dec:integer):string;
// Ptevara single u string i ispisuje sa dec brojem decimala
var
   i,j,k:integer;
   NumInt,DecPos:integer;
   NumStr,NumLeft,NumRight,zn,Predznak:String;
   LL,CC,DD:String;
   
Begin
   // ShowN('num='+FloatToStr(num));
   // NumInt:=Trunc(num);            ShowN('NumInt='+IntToStr(NumInt));
   NumStr:=FloatToStr(Num);      //  ShowN('FloatToStr(Num)='+FloatToStr(Num));
   NumLeft:='';   // Lijevo od decimalne to�ke
   NumRight:='';  // Desno od decimalne to�ke
   
   // Provjera predznaka
   // Ako je pozitivan dodaje se '+' da bude lak�e poslije brojati
   If Copy(NumStr,1,1)='-' Then Predznak:='-'
                           Else Begin Predznak:=' ';
                                      NumStr:=Predznak+NumStr;
                                End;
   
   // Provjera da li sadr�i slovo E ( 144.2E3 ili 144.5E-7 )
   if Contain(NumStr,'E') then begin
      // Kopiram lijevi dio - prije decimalne tocke
      For i:=1+1 to Length(NumStr) do Begin   // i+1 je zbog predznaka
         zn:=Copy(NumStr,i,1);
         If zn='.' then break
                   else Numleft:=NumLeft+zn;
      End;
      // ShowN('Lijevo: '+NumLeft);
      // Kopiram desni dio - nakon decimalne tocke PRIJE SLOVA E!!!
      For j:=i+1 to Length(NumStr) do Begin
         zn:=Copy(NumStr,j,1);
         If zn='E' then break
                   else NumRight:=NumRight+zn;
      End;
      // ShowN('Desno '+NumRight);
      
      // Znam da sada slijedi pomak decimalne to�ke u lijevu ili desnu stranu
      
      // Prvo treba saznati da li je idu�i znak minus '-'
      If Copy (NumStr,j+1,1)='-' then begin
            // decimalni zarez treba pomaknuti u lijevo za broj mjesta koji pi�e nakon znaka minus
            // Prvo treba otkriti koji broj pi�e
            DecPos:=StrToInt(Copy(NumStr,j+2,10));
            // ShowN('DecPos='+IntToStr(DecPos));
            // Dodavanje extra nula u lijevi dio
            NumLeft:='0000000000000000000000000000000000'+NumLeft;
            // ShowN('Lijevo start: '+NumLeft);
            // Pomicanje u lijevo
            For k:=1 To DecPos do begin
               LL:=Copy(NumLeft,1,Length(NumLeft)-k);
               DD:=Copy(NumLeft,Length(NumLeft)-k+1,50);
               // ShowN(+IntToStr(k)+' LL: '+LL+' DD '+DD);
               
            End;
            NumLeft:=LL+'.'+DD;
            // ShowN('1 NumLeft: '+NumLeft);
            // Micanje po�etnih nula
            For k:=1 To Length(NumLeft) do begin
               If Copy(NumLeft,k,1)='.' then Begin 
                                                NumLeft:=Copy(NumLeft,k-1,50);
                                                Break
                                             end;
            End;
            // ShowN('2 NumLeft: '+NumLeft);
            // Stavljanje predznaka
            NumLeft:=Predznak+NumLeft;
            NumStr:=NumLeft+NumRight;
            // ShowN('3 NumStr: '+NumStr);
      end Else Begin
            // decimalni zarez treba pomaknuti u desno za broj mjesta koji pi�e nakon E
            // Prvo treba otkriti koji broj pi�e
            DecPos:=StrToInt(Copy(NumStr,j+1,10));
            // ShowN('DecPos='+IntToStr(DecPos));
            // Dodavanje extra nula na kraj
            NumRight:=NumRight+'0000000000000000000000000000000000';
            // ShowN('Desno start: '+NumRight);
            // Pomicanje u desno
            For k:=1 To DecPos do begin
               LL:=Copy(NumRight,1,k);
               DD:=Copy(NumRight,k+1,50);
               // ShowN(+IntToStr(k)+' LL: '+LL+' DD '+DD);    
            End;
            NumRight:=LL+'.'+DD;
            // ShowN('1 NumRight: '+NumRight);
            // Micanje zavr�nih nula
            For k:=Length(NumRight) DownTo 1  do begin
               If Copy(NumRight,k,1)='.' then Begin 
                                                NumRight:=Copy(NumRight,1,k+1);
                                                Break
                                             end;
            End;
            // ShowN('2 NumRight: '+NumRight);
            NumStr:=Predznak+NumLeft+NumRight;
            // ShowN('3 NumStr: '+NumStr);
      End;
      // 

      
   End;  // if Contain(NumStr,'E')
   
   // Dobijen je normalni broj ako je imao E
   
   // Sad treba na�i decimalnu to�ku ako je uop�e ima
   If Contain(NumStr,'.') then begin
      // ima decimalnu to�ku
      // treba pozicionirati kursor na decimalnu to�ku
      For i:=1 to Length(NumStr) do begin
         If Copy(NumStr,i,1)='.' then break
      End;
      // ShowN('Tocka je na poziciji broj '+IntToStr(i));
      // sad je i na poziciji decimalne to�ke.
      // treba vidjeti koliko ima znamenki iza to�ke. 
      // Ako ih ima manje od dec treba dodati nule.
      If Length(NumStr)-i < Dec then NumStr:=NumStr+'00000000000';
      // ShowN('4 NumStr: '+NumStr);
      // Ispitati kakva je znamenka na poziciji dec+1
      // ako je ve�a od 4 onda treba prethodnu uve�ati za 1
      NumStr:=Copy(NumStr,1,i+dec);            // potrebno - uklju�uju�i zadnju tra�enu decimalu
   End else NumStr:=NumStr+'.0000';
      
    // ShowN('5 NumStr: '+NumStr); 
    Result:=NumStr;
End;

// Function random(a:integer):integer;
     
