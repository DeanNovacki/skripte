// rcMessages /////////////////////////////////////////////////////////////
var
   Debug_Memo:TMemo;
   Debug_Form:TForm;
   // cbDebug:TCheckBox;

Procedure debug_create;
// kreira debug prozor za ispis raznih varijabli tokom testiranja
Begin
  Debug_Form:=TForm.create(nil);
  Debug_Form.Top:=520;
  Debug_Form.wIDTH:=300;
  Debug_Form.Height:=600;
  Debug_Form.caption:='Debug message system';
  Debug_Form.FormStyle:=fsStayOnTop;
  Debug_Memo:=TMemo.create(Debug_Form);
  Debug_Memo.parent:=Debug_Form;
  Debug_Memo.Align:=alClient;
  Debug_Memo.ScrollBars:=ssBoth;
  Debug_Form.Show;
End;

  
procedure ShowD(mess:String);
Begin

   try 
      Debug_Memo.Lines.Add(mess);
	except 
		Showmessage('Debug_Form not exist! Execute Debug_create to work properly.' +#13#10+
                  +#13#10+
                  'Message text: '+mess);
	end;
End;

