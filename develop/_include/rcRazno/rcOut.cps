// Create Journal window with memo and display text with
// If Journal window is already created then add new line of text
var
   rcOut_enabled:Boolean;
   rc_TF:TForm;
   rc_MD:TMemo;
Procedure rcOut(ss:string);
Begin
   // try to write to memo
   if rcOut_enabled=true then begin 
      // Showmessage('rcOut procedure: rcOut_enabled=true');
      if rc_TF=nil then Begin  // if form not exist create it
         rc_TF:=TForm.create(nil);
         rc_TF.BorderStyle:=bsSizeToolWin;
         rc_TF.Show;  
         rc_TF.FormStyle:=fsStayOnTop;
         rc_TF.Caption:='Journal'; 
         rc_MD:=TMemo.Create(rc_TF);
         rc_MD.Parent:=rc_TF;  
         rc_MD.Color:=clBtnFace;
         rc_MD.Font.Name:='Arial';
         rc_MD.Font.Size:=7;
         rc_MD.Font.Color:=clMaroon;
         rc_MD.ScrollBars:=ssVertical;
         rc_MD.Align:=alClient;
         rc_TF.Width:=350;
         rc_TF.Height:=1000;
      end; // if rc_TF=nil
      rc_MD.Lines.BeginUpdate;
      rc_MD.Lines.Add(ss);
      rc_MD.Lines.EndUpdate;
      rc_MD.SelStart:=rc_MD.Lines.Count*1000;
      rc_MD.SetFocus;
      rc_MD.SelLength:=0;

   end else begin  // if rcOut_enabled=true
      // Showmessage('rcOut procedure: rcOut_enabled=False');
      // Showmessage('rcOut procedure: Test if not nil');
      if rc_TF<>nil then begin 
                           // Showmessage('rcOut procedure: Try to close');
                           rc_TF.free;  // release, close
                           //rc_TF.close;    // na free se ru�i, izgleda da je ovo bolje  
      end; // if rc_TF<>nil   
   end ; // if rcOut_enabled=true
end;

