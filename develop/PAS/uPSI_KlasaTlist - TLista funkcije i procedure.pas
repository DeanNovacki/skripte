unit uPSI_KlasaTlist;
{
This file has been generated by UnitParser v0.7, written by M. Knight
and updated by NP. v/d Spek and George Birbilis. 
Source Code from Carlo Kok has been used to implement various sections of
UnitParser. Components of ROPS are used in the construction of UnitParser,
code implementing the class wrapper is taken from Carlo Kok's conv utility

}
interface
 
uses
   SysUtils
  ,Classes
  ,uPSComponent
  ,uPSRuntime
  ,uPSCompiler
  ;
 
type 
(*----------------------------------------------------------------------------*)
  TPSImport_KlasaTlist = class(TPSPlugin)
  protected
    procedure CompileImport1(CompExec: TPSScript); override;
    procedure ExecImport1(CompExec: TPSScript; const ri: TPSRuntimeClassImporter); override;
  end;
 
 
{ compile-time registration functions }
procedure SIRegister_TList(CL: TPSPascalCompiler);
procedure SIRegister_KlasaTlist(CL: TPSPascalCompiler);

{ run-time registration functions }
procedure RIRegister_TList(CL: TPSRuntimeClassImporter);
procedure RIRegister_KlasaTlist(CL: TPSRuntimeClassImporter);

procedure Register;

implementation


 
procedure Register;
begin
  RegisterComponents('Pascal Script', [TPSImport_KlasaTlist]);
end;

(* === compile-time registration functions === *)
(*----------------------------------------------------------------------------*)
procedure SIRegister_TList(CL: TPSPascalCompiler);
begin
     
  //with RegClassS(CL,'TObject', 'TList') do
  with CL.AddClassN(CL.FindClass('TObject'),'TList') do
  begin
    RegisterMethod('Function Add( Item : Tobject) : Integer');
    RegisterMethod('Procedure Clear');
    RegisterMethod('Procedure Delete( Index : Integer)');
    RegisterMethod('Procedure Exchange( Index1, Index2 : Integer)');
    RegisterMethod('Function First : Tobject');
    RegisterMethod('Function IndexOf( Item :Tobject) : Integer');
    RegisterMethod('Procedure Insert( Index : Integer; Item : Tobject)');
    RegisterMethod('Function Last : Tobject');
    RegisterMethod('Procedure Move( CurIndex, NewIndex : Integer)');
    RegisterMethod('Function Remove( Item : Tobject) : Integer');
    RegisterProperty('Count', 'Integer', iptrw);
    RegisterProperty('Items', 'Tobject Integer', iptrw);

  end;
end;

(*----------------------------------------------------------------------------*)
procedure SIRegister_KlasaTlist(CL: TPSPascalCompiler);
begin
  SIRegister_TList(CL);
end;

(* === run-time registration functions === *)
(*----------------------------------------------------------------------------*)
procedure TListItems_W(Self: TList; const T: Tobject; const t1: Integer);
begin Self.Items[t1] := T; end;

(*----------------------------------------------------------------------------*)
procedure TListItems_R(Self: TList; var T: Tobject; const t1: Integer);
begin T := Self.Items[t1]; end;

(*----------------------------------------------------------------------------*)
procedure TListCount_W(Self: TList; const T: Integer);
begin Self.Count := T; end;

(*----------------------------------------------------------------------------*)
procedure TListCount_R(Self: TList; var T: Integer);
begin T := Self.Count; end;

(*----------------------------------------------------------------------------*)
procedure RIRegister_TList(CL: TPSRuntimeClassImporter);
begin
  with CL.Add(TList) do
  begin
    RegisterMethod(@TList.Add, 'Add');
    RegisterVirtualMethod(@TList.Clear, 'Clear');
    RegisterMethod(@TList.Delete, 'Delete');
    RegisterMethod(@TList.Exchange, 'Exchange');
    RegisterMethod(@TList.First, 'First');
    RegisterMethod(@TList.IndexOf, 'IndexOf');
    RegisterMethod(@TList.Insert, 'Insert');
    RegisterMethod(@TList.Last, 'Last');
    RegisterMethod(@TList.Move, 'Move');
    RegisterMethod(@TList.Remove, 'Remove');
    RegisterPropertyHelper(@TListCount_R,@TListCount_W,'Count');
    RegisterPropertyHelper(@TListItems_R,@TListItems_W,'Items');
  end;
end;

(*----------------------------------------------------------------------------*)
procedure RIRegister_KlasaTlist(CL: TPSRuntimeClassImporter);
begin
  RIRegister_TList(CL);
end;

 
 
{ TPSImport_KlasaTlist }
(*----------------------------------------------------------------------------*)
procedure TPSImport_KlasaTlist.CompileImport1(CompExec: TPSScript);
begin
  SIRegister_KlasaTlist(CompExec.Comp);
end;
(*----------------------------------------------------------------------------*)
procedure TPSImport_KlasaTlist.ExecImport1(CompExec: TPSScript; const ri: TPSRuntimeClassImporter);
begin
  RIRegister_KlasaTlist(ri);
end;
(*----------------------------------------------------------------------------*)
 
 
end.
