
function find_value_int(FileName,ValueName:string):integer;
var
   list:TStringList;
   ResultStr, value:string;
   ind:integer;
Begin
   list:=TStringList.Create;
   list.LoadFromFile(FileName);
   ind:=List.IndexOfName(ValueName);       // if ValueName NOT EXIST, ind will be -1
                                           // if ValueName DO EXIST but without value, ind will be real index 
   value:=list.Values[ValueName];          // BUT, in this case value will be ''
   
   if (ind>=0) and (value='') then begin
                List.Delete(List.IndexOfName(ValueName));     // delete value from list
                list.SaveToFile(FileName);                    // save new list
                // search again!!
                ind:=List.IndexOfName(ValueName);
                value:=list.Values[ValueName];
   end; // if 
   if (ind>=0) and (not(value='')) then resultStr:=value
                                   else begin  
                                      ShowMessage( 'Function: Find_value_int'+#13#10+
                                                   'Cannot find name "'+ ValueName+'" in file "'+FileName+'"'+#13#10+
                                                   'Value index is: '+IntToStr(List.IndexOfName(ValueName))+#13#10+
                                                   'Result is: 0');
                                                   ResultStr:='0';
                                   end;
   

   result:=StrToInt(ResultStr);   
   list.free;                                   
end;

var
  s1: integer;
begin

  
  s1:=find_value_int('Lista2.txt','treci');
  showmessage(IntToStr(s1));
//   lista.SaveToFile('Lista.txt');
end.
