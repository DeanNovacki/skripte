// Procedure notes with dependent procedures and functions




// ================================================================================================
// MAIN
// ================================================================================================




var
   Main:TForm;
   lab_sel_qnt:TLabel;
   RadioBut_All_Elements,
   RadioBut_Sel_Elements: TRadioButton;
   Lab_Headline,  Lab_SubHeadline, Label_Description:TLabel;
   Edit_Headline, Edit_SubHeadline: TEdit;
	Edit_Autor:TEdit;
	Edit_Index:TEdit;
	Edit_Datum:TEdit;
   Memo_Description:TMemo;
   But_Cancel, But_setup, But_ok:TButton;
   Name_Line:string;
   Notes_Line:String;
   H1_String, H2_String:String;
   
   PDF_Lines: array of string;



Procedure process_element(e1:Telement);
var
   nap,naziv2,temp_name,tx:string;
   i,j:Integer;
   d1:TDaska;
   el_ozn,das_ozn, nap_ozn, prim_ozn,sep_ozn:string;

Begin
   {
   el_ozn:='Element: ';
   das_ozn:='Daska: ';
   nap_ozn:='Napomena: ';
   prim_ozn:='Primjedba: ';
   Sep_ozn:=' / ';
   }
   el_ozn:='El: ';
   das_ozn:='Das: ';
   nap_ozn:='  :  ';
   prim_ozn:='  :  ';
   Sep_ozn:='  /  ';
   // Print('Procedure "process_element" started. Element: "'+e1.naziv);
   // ss:=e1.opis;  OVO DAJE COMPILE ERROR!!!
   Name_line:=Name_line+el_ozn+e1.naziv+sep_ozn;
   nap:=e1.napomena;
   
   // vr�ni elementi
   if (e1.visible=true) and (nap<>'') and (nap<>'|') then Begin Notes_line:=Notes_line+nap_ozn+nap;
                         tx:=Name_line+''+Notes_line;
                         SetArrayLength(PDF_Lines,GetArrayLength(PDF_Lines)+1);  
                         PDF_Lines[GetArrayLength(PDF_Lines)-1]:=tx;
                         Print(tx);
                         Notes_line:='';
   end;
   
   // idi u daske
   for i:=0 to e1.childdaska.count-1 do Begin
        d1:=e1.childdaska.daska[i];
		If d1.visible=true then begin
            // glavna primjedba
            nap:=d1.Primjedba;
            if nap<>'' then Begin
                        Notes_line:=Notes_line+prim_ozn+nap;
                        tx:=Name_Line+das_ozn+d1.Naziv+Notes_Line;
                        SetArrayLength(PDF_Lines,GetArrayLength(PDF_Lines)+1);  
                        PDF_Lines[GetArrayLength(PDF_Lines)-1]:=tx;
                        Print(tx);
                        Notes_line:='';
            end;
            // ostale primjedbe
            for j:=0 to d1.PrimjedbaList.Count-1 do begin
                  nap:=d1.PrimjedbaList[j];
                  if nap<>'' then Begin 
                        // Notes_line:=Notes_line+prim_ozn+IntToStr(j)+''+nap;
								Notes_line:=Notes_line+prim_ozn+''+nap;
                        tx:=Name_Line+das_ozn+d1.Naziv+''+Notes_Line;
                        SetArrayLength(PDF_Lines,GetArrayLength(PDF_Lines)+1);  
                        PDF_Lines[GetArrayLength(PDF_Lines)-1]:=tx;
                        Print(tx);
                        Notes_line:='';
                  end;
            
            end;
		end; //  If d1.visible=true     
   end;
   
   // idi u podelemente
   temp_name:=Name_line;
   for i:=0 to e1.elmlist.count-1 do begin 
               if e1.elmlist.element[i].visible=true then process_element(e1.elmlist.element[i]);
               Name_line:=temp_name;
               // temp_name:='';
   end;
   Name_line:='';
               
   // Print('Procedure "process_element" finished');
End;

Procedure Notes_do;
var
   i:integer;
   all:boolean;
   tel:TElement;
   
Begin
   Print('Procedure "notes_do" started');
   if RadioBut_All_Elements.checked then all:=True;

   // obradi elemente iz projekta
   name_line:='';    // linija teksta;
   SetArrayLength(PDF_Lines,0);  
   //PDF_Lines[GetArrayLength(PDF_Lines)-1]:=tx;
   if e=nil then for i:=0 to elmHolder.elementi.Count-1 do begin
      tel:=telement(ElmHolder.elementi.items[i]);
      if tel.selected or all then begin
											if tel.visible=true then process_element(tel)
										end;
   end;
	
   Print('Procedure "notes_do" finished');
End;




   
Procedure PrintEndPage;
var
   i:integer;
begin
     // PDF.LineWidth:=0.1;
     // pdf.SetRGBColor(0,0,0);     
     // CRtaj_Tablicu;
     // PrintFooter;
end;
   
Procedure CheckNewPage;
begin     
     {
	 if Ypos+20>ph-20 then begin        
        PrintEndPage;
        pdf.newpage;
        taby1:=20;        
        Ypos:=taby1+1+Vel_Font_Zagl_Tbl; 
     end;
	 }
end;



Procedure print_pdf;
var
   i:integer;
   
   PdfOK:Boolean;
   pdfy, bs, us:Integer;
   first_y, other_y:Integer;

   pdf_folder:String;
   pdf_file:String;
   first_page:boolean;
   PdfImg:TMIMage;
   rb:integer;srb:string;
   
Begin

	// --------------------------------------------------------------------------------------------!!!
   // xo:=Main.parent;
   // xp:=xo.parent;
   // if xo is TForm then Begin sst:=UpperCase(TForm(xo).caption);   //  <<<< ----------------------------!!!
   // 							 ShowMessage (sst);	
   // 	End;
   first_page:=True;
   // --------------------------------------------------------------------------------------------!!!

   first_y:=85;	// po�etak tablice na prvoj stranici
   other_y:=65;		// po�etak tablice na ostalim stranama
   Print('Procedure "print_pdf" started');
   
   // setup PDF
   PDF:=TpdfPrinter.create(1250);
   PDF.TITLE := 'PInv print';                     // ?
   pdf.newpage;  
   pw:=PDF.PageWidth;  // �irina stranice
   ph:=PDF.PageHeight; // visina stranice
   
	pdf_folder:=prgfolder+'Proizvodnja\';
   pdf_file:='test.pdf' ;
   Print('Preparing PDF.FileName: "'+pdf_folder+pdf_file);
	PDF.FileName :=pdf_folder+pdf_file;
   PDF.FileName :='C:\RedCat\Corpus 4 RC\sobasav\napomene.pdf' ;
   
   //PDF.LineWidth:=0.1;
   pdf.SetRGBColor(0,0,0);
   
	
   If GetArrayLength(PDF_Lines)<1 then Begin
			SetArrayLength(PDF_Lines,1);
			PDF_Lines[0]:='Nema napomena ili primjedbi!';
	end;
	
   
   Print('1');
	
   // memo_srednji_iso('Napomene',Edit_Autor.text,Edit_Index.text,Edit_Datum.text,' Firma d.o.o.');

   Print('2');
   
   pdfy:=first_y;
   
   // print kratki naslov projekta
   
	Print('3');	
	
	pdf.setfont('Arial',12);
   pdf.FontStyles:=[fsBold];
Print('3a');	
//	PDF.TextOut(30,pdfy,rcProjectName);
Print('3b');	
	pdfy:=pdfy+13;
   
Print('4');	
	
	{
	// print naslov projekta s folderom
	pdf.setfont('Arial',8);
   pdf.FontStyles:=[];
   PDF.TextOut(30,pdfy,rcProjectLongName);
	pdfy:=pdfy+10;
	}
	
	// print puna putanja projekta
	pdf.setfont('Arial',8);
   pdf.FontStyles:=[];
   PDF.TextOut(30,pdfy,rcProjectPath);
	pdfy:=pdfy+10;
	

	pdfy:=pdfy+4;
   pdf.FontStyles:=[];
   pdf.setfont('Arial',8);
   PDF.TextOut(30 ,pdfy-0.5,'Opis projekta:');
   
	pdf.setfont('Arial',9);
   
	// print project description
   
   pdf.FontStyles:=[];
   pdf.setfont('Arial',8);
   PDF.TextOut(30 ,pdfy-0.5,'Opis projekta:');
   pdf.setfont('Arial',9);
   

	  
   
	For i:=0 to Memo_Description.Lines.Count do begin
		Pdf.TextBox(81,pdfy,600,taLeftJustify,Memo_Description.Lines[i]);
		pdfy:=pdfy+13;
   End;
	// pdfy:=pdfy+10;
  
   // print body
   pdf.setfont('Arial',9);
   pdf.FontStyles:=[];
   PDF.setRgbcolor(0,0,0);  
   

	// print one line on PDF page (generirano ve� prije)
	
   bs:=0;
   for i:=0 to GetArrayLength(PDF_Lines)-1 do Begin
      // Print(PDF_Lines[i]);

	  if pdfy>810 then begin 
				// showmessage ('800!');
				Print('Nova strana!');
				bs:=bs+1;
				PrintFooter('Napomene - strana '+ IntToStr(bs));
				bs:=bs+1;
				pdf.newpage;
				pdfy:=other_y;
				// memo_mali_iso('Napomene',Edit_Autor.text,Edit_Index.text,Edit_Datum.text,' Firma d.o.o.');
	  end;
	  
	  Print('5');
	  
	  pdf.setfont('Arial',8);
	  pdf.FontStyles:=[];
	  srb:=IntToStr(i+1)+'.';
	  Pdf.DrawRectangle(30,pdfy-8,38,pdfy);				// kockica
	  Pdf.Textbox(10,pdfy,44,taRightJustify,srb);		// redni broj
	  PDF.TextOut(55,pdfy,PDF_Lines[i]);					// tekst
	  
	  pdfy:=pdfy+13;
   end;
   
	// potpisi na kraju
   if pdfy>760 then begin 
				// showmessage ('800!');
				Print('Nova strana!');
				bs:=bs+1;
				pdf.newpage;
				pdfy:=other_y;
				// memo_mali_iso('Napomene','Dean Nova�ki','1',rcDanas,' Firma d.o.o.');
	end;
	PrintSignature(30,PDFy+5);	
	
	// footer za kraj
	PrintFooter('Napomene - strana '+ IntToStr(bs) + '.');
   
   
     Print('4');
   
   
   Print('PDF.EndDoc;');
   
   Repeat
      try 
			Print('5');
         PDF.EndDoc;
         PdfOK:=True;
      except
			Print('6');
         PdfOK:=False;
         Showmessage( 'Ne mogu kreirati pfd dokument!'+#13#10+#13#10+
					  'Ili ne postoji folder '+pdf_folder+#13#10+
					  'ili je otvoren prethodni dokument s ispisom napomena!'+#13#10+#13#10+
                      'Ispravi i nakon toga stisni [ OK ]');
      end;  
   Until PdfOK;  

	Print('7');	
		
   Print('PDF.ShowPdf;');
   PDF.ShowPdf;
   Print('PDF.Free;');
   PDF.Free;
   
   
   
   //PrintPdf(prgfolder+'\skripte\ponuda\test2.pdf');         // prgfolder = polo�aj Corpus.exe
   // 
   // print Header
   // print body
   
   //PDF.Compress:=true; 
   
   

   
   
   
   
   
  
   Print('Procedure "print_pdf" finished');
End;


Procedure But_Cancel_Clicked(Sender:TObject);
Begin
   Print('Procedure "But_Cancel_Clicked" started');
   Main.close;
   
   Print('Procedure "But_Cancel_Clicked" Finished');
End;

Procedure But_Setup_Clicked(Sender:TObject);
Begin
   Print('Procedure "But_Setup_Clicked" started');
   
   
   Print('Procedure "But_Setup_Clicked" Finished');
End;

Procedure But_OK_Clicked(Sender:TObject);
Begin
   //Print('Procedure "But_OK_Clicked" started');
   Notes_do;
   print_pdf;
   Main.close;
   //Print('Procedure "But_OK_Clicked" Finished');
End;

// include notes_create_window;
Procedure notes_create_window;
var
   i,j:Integer;
Begin
   // Print('Procedure "notes_create_window" started');
   // Main Window
   Main:=rcTool('Napomene');
   Main.Height:=300;Main.Width:=350;
   Main.Font.Height:=14;
   
   // Quantity of selected elements
   lab_sel_qnt:=rcLabel('', '0/0', Main, 3, 0, taLeftJustify);
   lab_sel_qnt.font.size:=30;
   lab_sel_qnt.hint:='Broj selektiranih elemenata / Ukupni broj elemenata';
   j:=0;
   if e=nil then for i:=0 to elmHolder.elementi.Count-1 do begin
      //Print('i:='+IntToStr(i)+' Naziv Elementa: '+telement(ElmHolder.elementi.items[i]).Naziv);
      if telement(ElmHolder.elementi.items[i]).selected = true then j:=j+1;
   end;
   lab_sel_qnt.caption:=IntToStr(j)+'/'+IntToStr(i);
   
   // All/Selected elements?
   RadioBut_All_Elements:=rcRadioButton('rbae','Svi elementi',Main,120,4);
   RadioBut_All_Elements.Width:=200;
	RadioBut_All_Elements.Alignment:=alRight;
   RadioBut_Sel_Elements:=rcRadioButton('rbse','SAMO SELEKTIRANI elementi',Main,120,23);
   RadioBut_Sel_Elements.Width:=200;  
	RadioBut_Sel_Elements.Alignment:=alRight;
   if j=0 then RadioBut_All_Elements.checked:=True
          else RadioBut_Sel_Elements.checked:=True;
   
   {
	// Main Header
   Edit_Headline:=RcEditL('1','',Main, 20,43,'left');
   Edit_Headline.width:=295;
   Edit_Headline.text:='Napomene: '+rcProjectLongName;


	Edit_SubHeadline:=RcEditL('2','',Main, 20,65,'left');
   if RadioBut_All_Elements.checked then begin
			Edit_SubHeadline.Text:=rcProjectPath + '  |  datum ispisa: '+ rcDanas + '  |  ' + 'Svi elementi.';
		end else begin
		   Edit_SubHeadline.Text:=rcProjectPath + '  |  datum ispisa: '+ rcDanas + '  |  ' + 'SELEKTIRANI elementi.';
	end; // if RadioBut_All_Elements.checked
   Edit_SubHeadline.width:=295;
	}
	
	Edit_Autor:=RcEditL('Autor:','',Main, 100, 45, 'left'); 
	Edit_Autor.text:=ElmHolder.ProjectOpisData.TrgovacProjektIzradio;
	Edit_autor.width:=200;
	Edit_Index:=RcEditL('Index (verzija):','',Main, 100, 70, 'left'); Edit_Index.width:=200;
	Edit_Index.Text:='0';
	Edit_Datum:=RcEditL('Datum:','',Main, 100, 95, 'left'); Edit_Datum.width:=200;
	Edit_Datum.Text:=rcDanas;

   Label_description:=rcLabel('','Opis projekta',Main,7,120,taLeftJustify);
   Memo_Description:=rcMemo('',Main,7,135,310,080);
	Memo_Description.WordWrap:=False;
   Memo_description.Lines.CommaText:=ElmHolder.ProjectOpisData.Opis;
   // Memo_Description.Lines.Assign(ElmHolder.ProjectOpisData.Opis); // radi samo za StringList
   
   // Buttons
   But_Cancel:=rcButton('','Odustani',Main,20,220);
   But_Cancel.width:=100;
   But_Setup:=rcButton('','Podesi',Main,130,220);
   But_Setup.Height:=20;
   But_Setup.Width:=80;
   But_Setup.Enabled:=False;
   But_Setup.Visible:=False;
   But_OK:=rcButton('','PDF',Main,200,220);
   But_OK.Width:=100;
   But_OK.Default:=True;
   But_Cancel.OnClick:=@But_Cancel_clicked;
   But_Setup.OnClick:=@But_Setup_clicked;
   But_OK.OnClick:=@But_OK_clicked;
   
   Main.ShowModal;
   
   
   // Print('Procedure "notes_create_window" ended');
End;



Procedure Notes;
var
   i:Integer;

Begin
  Print('Procedure "Notes" started');
  //ShowMessage('g4');
  notes_create_window;
  
  
  
  
  
  
  Main.Free;
  Print('Procedure "Notes" ended');
End;


