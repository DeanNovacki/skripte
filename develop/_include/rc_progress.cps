// Progress bar
var
   Progress_Form:TForm;
   Bar_in,Bar_out:TPanel;
   tt:TLabel;
procedure Progress(pp:Integer;ss:string);
var
   full_width,bar_width:Integer;
Begin
   if (pp<1) or (pp>99) then progress_Form.visible:=false
                        else begin
      IF Progress_Form=nil then begin
         
         Progress_Form:=TForm.create(nil);
         progress_Form.FormStyle:=fsStayOnTop;
         progress_Form.BorderStyle:= bsNone;           // bsNone = no border, no caption
                                            // bsSizeToolWin 
                                            // bsDialog
                                            // bsSingle
                                            // bsSizeable
                                            // bsToolWindow
         progress_Form.caption:='RC Tools';
         progress_Form.width:=600;
         progress_Form.height:=14;
         //progress_Form.position:=poScreenCenter;
         progress_Form.Top:=120;
         progress_Form.Left:=120;
         bar_in:=RCPanel( '',  progress_Form);
         bar_in.left:=1;
         // bar_in.width:=1;
         bar_in.align:=alLeft;
         bar_in.color:=clRed;
         //bar_in.BevelInner:=bvNone;   // mi�e border s panela
         //bar_in.BevelOuter:=bvNone;   // mi�e border s panela
         
         bar_out:=RCPanel( '',  progress_Form);
         bar_out.left:=100;
         bar_out.align:=alClient; 
         bar_out.color:=clNavy;
         //bar_out.BevelInner:=bvNone;   // mi�e border s panela
         //bar_out.BevelOuter:=bvNone;   // mi�e border s panela
         // bar_out.OnClick:=@progress_close;
         // bar_in.OnClick:=@progress_close;
         
         // tt:=rcLabel( '','Please wait...',progress_Form,300,1,TaCenter);
         tt:=rcLabel( '','Please wait...',bar_in,3,1,TaLeftJustify);
         tt.font.size:=8;
         tt.font.style:=[fsBold];
         tt.font.color:=cl3DLight;
         //tt.BringToFront;
         
         progress_Form.show;
      end;
      progress_Form.visible:=true;
      full_width:=progress_Form.clientwidth;
      bar_width:=round(((pp/100))*full_width);                 
      bar_in.width:=bar_width;
      // progress_form.caption:=ss;
      tt.caption:=ss;
      Application.ProcessMessages;
   end;
End;

