// TstringList example
// StringList contain variables
 
var
   variables:TstringList;

function ReadInt(name:string):integer;
// read value by name and convert to integer
begin
   result:=StrToInt(variables.Values[name]);
end;

function ReadStr(name:string):string;
// read string value from name
begin
   result:=variables.Values[name];
end;

Begin
   variables:=TStringList.Create;
   variables.Add('El_name=cabinet 3');
   variables.Add('El_width=450');
   variables.Add('El_height=820');
   variables.Add('El_depth=560');
   // variables.LoadFromFile('default.preset');
   showmessage(ReadStr('El_name'));
   showmessage(IntToStr(ReadInt('El_width')));


end.