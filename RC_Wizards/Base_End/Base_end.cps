//Version=1.0 (III/2013)
// Base End
{
   Procedure warning_range(wmin, wmax:integer);
   Procedure warning_length(wmin, wmax:integer);
   Procedure warning_name_exist;
   Procedure warning_off;
   function error_exist:boolean;
   procedure Edit_enabled(tag:integer;on:boolean);
   function reach_preset_limit:boolean;
   procedure lock_main_buttons;
   Procedure Get_ini;
   Procedure Change_AP_Heights;
   Procedure Minimize_click_AP(sender:Tobject);
   Procedure Set_events;
   
}


program base_end;
// wizard for base elements
// author: Dean Nova�ki
// 31.08.2012.
// v.1.16 created 25.10.2012
// v.1.20 created 09.11.2012

           
var
                                                                                            //========================// declare
   {$I declare.cps}                                                                         
   // start_ok:boolean;
                                                                                            //========================// dex_unit
   {$I dex_unit.cps} 
   {$I element.cps}
   
const
                                                                                            //========================// def.lng
   {$I def.lng}

   {$I arc.cpr}
Procedure warning_range(wmin, wmax:integer);
Begin
    message('Procedure warning_range(wmin, wmax:integer)');
    warning_header.caption:=txt_warning_header;
    warning_line1.caption:=txt_w_incorrect_entry;
    warning_line2.caption:=txt_w_value_out_range;    
    warning_line3.caption:=txt_w_allowed_range+': '+IntToStr(wmin)+' - '+IntToStr(wmax);
    warning_header.visible:=true;
    warning_line1.visible:=true;
    warning_line2.visible:=true;    
    warning_line3.visible:=true;
    message('done: Procedure warning_range(wmin, wmax:integer)');
end;

Procedure warning_length(wmin, wmax:integer);
Begin
    message('Procedure warning_length(wmin, wmax:integer);');
    warning_header.caption:=txt_warning_header;
    warning_line1.caption:=txt_w_incorrect_entry;
    warning_line2.caption:=txt_w_incorrect_length;    
    warning_line3.caption:=txt_w_allowed_size+': '+IntToStr(wmin)+' - '+IntToStr(wmax);
    warning_header.visible:=true;
    warning_line1.visible:=true;
    warning_line2.visible:=true;    
    warning_line3.visible:=true;

end;

Procedure warning_name_exist;
Begin
    warning_header.caption:=txt_warning_header;
    warning_line1.caption:=txt_w_incorrect_entry;
    warning_line2.caption:=txt_w_name_exist;    
    warning_line3.caption:=txt_w_choose_another_name;
    warning_header.visible:=true;
    warning_line1.visible:=true;
    warning_line2.visible:=true;    
    warning_line3.visible:=true;
    
end;



Procedure warning_off;
Begin
    warning_header.visible:=false;
    warning_line1.visible:=false;
    warning_line2.visible:=false;    
    warning_line3.visible:=false;
    // Turn_on_ok;
end;

function error_exist:boolean;
// if any of edit fields is in red color, there is some error entry
// this function hase result true if is some wrong entry
var
   AP_Count, i:Integer;

begin

   result:=false;
   for AP_count:=1 to 9 do begin                                 // all panels
      for i:=0 to AP[AP_count].controlcount-1 do begin           // all objects 
         if (AP[AP_count].controls[i] is TEdit)  then            // edit fields
           if (TEdit(AP[AP_count].controls[i]).font.color = $0000FF) and
              (TEdit(AP[AP_count].controls[i]).enabled = true) then result:=true;   
      end;  // for i
   end; //  AP_count
   if Save_Field.font.color = $0000FF then result:=true;
 
   
end;




procedure Edit_enabled(tag:integer;on:boolean);
// some controls cannot be used all the time
// this procedure make them enable or desable
var
   AP_count, i:integer;
begin
   for AP_count:=1 to 9 do begin                                 // all panels
      for i:=0 to AP[AP_count].controlcount-1 do begin           // all objects
         if (AP[AP_count].controls[i] is TEdit) and              // if Tedit
            (AP[AP_count].controls[i].tag =tag) then             // if correct tag
              if on then TEdit(AP[AP_count].controls[i]).Enabled:=True
                    else TEdit(AP[AP_count].controls[i]).Enabled:=False; 
      end;  // for i
   end; //  AP_count
end;

function reach_preset_limit:boolean;
var
   i,counter:integer;
begin
   // message('Function reach_preset_limit : boolean');
   counter:=0;
   for i:=0 to Preset_Listbox.Items.Count-1 do begin 
      if preset_index[i]='U' then counter:=counter+1;
   end;
   if counter>15 then result:=true
                 else result:=false;
   // if result then message('true')
   //           else message('false'); 
end;



procedure lock_main_buttons;
// disable save, ok, update etc. buttons in case of errors or other reasons
var
   si:integer;
begin

  si:=Preset_ListBox.ItemIndex;
  
  if (preset_index[si] = 'D') or error_exist 
                      then Button_update.enabled:=false
                      else Button_update.enabled:=true;

   if error_exist then begin
                           Button_OK.enabled:=false;
                           Button_Save.enabled:=false;
                           Butt_ok.enabled:=false;
                  end else begin
                           Button_OK.enabled:=true;
                           Button_Save.enabled:=true;
                           Butt_ok.enabled:=true;
   end;  // if

   
   
end;


                                                                                            //========================// preset_funcs
   {$I preset_funcs.cps}   { Procedure Save_Preset(preset_name:string); 
                             Procedure Save_new_preset_show(sender:TObject);
                             Procedure Save_new_preset_hide(sender:TObject);
                             Procedure Save_new_preset(sender:TObject);
                             Procedure Update_preset_show(sender:TObject);
                             Procedure Update_preset_hide(sender:TObject);
                             Procedure Update_preset(sender:TObject);
                             Procedure Load_Preset(preset_name:string);
                             Procedure Apply_preset;
                             Procedure Presets_Click(sender:Tobject);
                             Procedure load_preset_list;
                             Procedure Delete_preset_show(sender:TObject);
                             Procedure Delete_preset_hide(sender:TObject);
                             Procedure Delete_preset(sender:TObject);
                                                                                                  
   }

                                                                                     //========================// design.cps
 {$I OK.cps}  // ok button

      




                                                                                         //========================// design.cps
 {$I design.cps}  // design form and controls



Procedure Get_ini;
// 1.07
// read which adjustments panel are minimized, 
// read default and user presets

var
   i:integer;
Begin
   // reading AP[1] to AP[9] from ini file and set tag of APS
   // this tag will be used for setting minimize state of each adjustment panel (AP[i])
   for i:=1 to 9 do begin
      APS[i].tag:=find_value_int(IniName,'AP['+IntToStr(i)+']');
   end;
   SBSP:=find_value_int(IniName,'SBSP');

end;

Procedure Change_AP_Heights;
// v1.07
var
   SB_Pos, i:integer;
begin

   message('Procedure Change_AP_Heights');
   SB_Pos:=Adj_SB.VertScrollBar.Position;
   Adj_SB.VertScrollBar.Visible:=False;
   for i:=1 to 9 do begin 
       if APS[i].Tag=1 then begin 
                           AP[i].Height:=AP_Height[i];
                           APS[i].Caption:=CHR(241);
                      end else begin 
                          AP[i].Height:=25;
                          APS[i].Caption:=CHR(242);
                      end;
       If i=1 then AP[i].Top:=5
              else AP[i].Top:=AP[i-1].Top+AP[i-1].Height+5;                
   end;   // for i
   
   Mini_Panel.Top:=AP[9].Top+AP[9].Height;
   
   Adj_SB.VertScrollBar.Visible:=True;
   Adj_SB.VertScrollBar.Position:=SB_Pos;

   message('done: Procedure Change_AP_Heights');
End;  // Procedure Change_AP_Heights;



Procedure Minimize_click_AP(sender:Tobject);
// 1.07
// Click on minimize 'button' of any adjusment panel

begin
   // message('Minimize_click_AP');
   // TEdit(Sender).text:
   if TPanel(sender).Tag=1 then TPanel(sender).Tag:=0 else TPanel(sender).Tag:=1;
   Change_AP_Heights;
end;   




                                                                                    
                                                                                         //========================// Events.cps
 {$I Events.cps}      {   Procedure Radio_Image_OnClick(sender:Tobject);
                          Procedure Fields_Name_OnChange(sender:Tobject);
                          Procedure Fields_OnChange(sender:Tobject);
                          

                       }
   
Procedure Set_events;
var 
    i,AP_count:integer;
//    DEdit:TEdit;
Begin
   Preset_ListBox.OnClick:=@Presets_Click;  

   // For minimize/maximize Adjustment Panels (1-9)
   for i:=1 to 9 do begin 
       APS[i].OnClick:=@Minimize_click_AP;
   end;
   
   edit_name.OnKeyPress:=@Check_Str_Prg;
   edit_name.OnChange:=@fields_name_OnChange;
   
   // For Radio Images
   for AP_count:=1 to 9 do begin                                 // all panels
      for i:=0 to AP[AP_count].controlcount-1 do begin           // all objects
         if (AP[AP_count].controls[i] is TMImage) and 
            ((copy(AP[AP_count].controls[i].name,1,1) ='i')) then             // if first character in name is 'i'
              TMImage(AP[AP_count].controls[i]).OnClick:=@Radio_Image_OnClick;
      end;  // for i
   end; //  AP_count

   // OnKeyPress for edit fields

   for AP_count:=1 to 9 do begin                                 // all panels
      for i:=0 to AP[AP_count].controlcount-1 do begin           // all objects 
         if (AP[AP_count].controls[i] is TEdit)  then            // edit fields
            // DEdit:=Tedit(AP[AP_count].controls[i]);                                    <===      ?????? compile ok but not working ???????
            // must be positive integer
            if ((AP[AP_count].controls[i].Tag>=201) and (AP[AP_count].controls[i].Tag<=203)) or
               ((AP[AP_count].controls[i].Tag>=304) and (AP[AP_count].controls[i].Tag<=308)) or
               ((AP[AP_count].controls[i].Tag>=404) and (AP[AP_count].controls[i].Tag<=406)) or
               ((AP[AP_count].controls[i].Tag>=504) and (AP[AP_count].controls[i].Tag<=507)) or
               ((AP[AP_count].controls[i].Tag=601)                                         ) or
               ((AP[AP_count].controls[i].Tag>=705) and (AP[AP_count].controls[i].Tag<=709))                           
               
               
                   
                                                                                                                            // <<====================== 
                                                                                                                            // <<====================== 
                                                                                                                            // <<======================  
               then begin
                      TEdit(AP[AP_count].controls[i]).OnKeyPress:=@Check_Int_Pos;       // check if positive integer
               end;
            // end if ((AP[AP_count]...
            
            // must be integer
            if AP[AP_count].controls[i].Tag=503 then TEdit(AP[AP_count].controls[i]).OnKeyPress:=@Check_Int;    // check if integer
           
           
      end;  // for i
   end; //  AP_count

  // OnChange for edit fields
   for AP_count:=1 to 9 do begin                                 // all panels
      for i:=0 to AP[AP_count].controlcount-1 do begin           // all objects 
         if (AP[AP_count].controls[i] is TEdit)  then            // edit fields
            // val:= StrToInt( Tedit( AP[AP_count].controls[i] ).text );
            
           // if AP[AP_count].controls[i].Tag=201 then TEdit(AP[AP_count].controls[i]).OnExit:=@el_width_exit;
           
           if ((AP[AP_count].controls[i].Tag>=201)  and (AP[AP_count].controls[i].Tag<=203)) or
               ((AP[AP_count].controls[i].Tag>=304) and (AP[AP_count].controls[i].Tag<=308)) or
               ((AP[AP_count].controls[i].Tag>=404) and (AP[AP_count].controls[i].Tag<=406)) or
               ((AP[AP_count].controls[i].Tag>=503) and (AP[AP_count].controls[i].Tag<=507)) or
               ((AP[AP_count].controls[i].Tag=601)                                         ) or
               ((AP[AP_count].controls[i].Tag>=705) and (AP[AP_count].controls[i].Tag<=709)) then begin

                      // message('3. This has to be checked! Tag is '+ IntToStr(Tedit(sender).Tag));
                      TEdit(AP[AP_count].controls[i]).OnChange:=@Fields_OnChange;
           end;
           
      end;  // for i
   end; //  AP_count

   
   Save_Field.OnKeyPress:= @Check_Str_Prg;
   Save_Field.OnChange:=@fields_name_OnChange;
   Button_Save.OnClick:=@Save_new_preset_show;
   Butt_cancel.OnClick:=@Save_new_preset_hide;
   Butt_ok.OnClick:=@Save_new_preset;
   
   Button_update.OnClick:=@Update_preset_show; 
   Update_ok.OnClick:=@Update_preset; 
   Update_cancel.OnClick:=@Update_preset_hide; 
   
   Button_Delete.OnClick:=@Delete_preset_show;
   Delete_ok.OnClick:=@Delete_preset; 
   Delete_cancel.OnClick:=@Delete_preset_hide;                                                              
                                                                                                      
              
end;
var
   selem:Telement;
  
BEGIN        // MAIN 


   
   // find selected element
    start_ok:=true;
    
{    
    if el_find_selected<>nil then Begin 
                                       // Showmessage('MAIN! Element found!');
                                       selem:= el_find_selected
                                              
                                  end else begin 
                                       start_ok:=false;
                                       Showmessage('ONE element must be selected!');
                                       Showmessage('Press Cancel, select ONE element and start again!');
    end; // if el_find_selected<>nil
}

   script_path:=PrgFolder+'skripte\RC_Wizards\Base_end\';
   IniName:=script_path+'Base_end.ini';
   lang_file:=script_path+'def.lng';
   Last_preset:=2;
   

  Create_Window;


  Get_ini;                          // read which adjustments panel are minimized, 

  load_preset_list;                 // load presets

  Change_AP_Heights;                // change minimization of adjustment panels  for the first time!
  Adj_SB.VertScrollBar.Position:=SBSP;  // set last scroll bar position 
  
  Load_Preset('default\Last used');         // Read_last_preset

  draw_AP1; draw_AP2; draw_AP3; draw_AP4; draw_AP5; draw_AP6; draw_AP7; draw_AP8; draw_AP9;
  // Set_AP_Heights;                // set minimised state for each AP from ini file 

  Apply_Preset;                     // for the first time!   

  Set_Events;                       // 




   // W_Window.OnResize:=@win_resize;
   Button_ok.OnClick:=@ok;
   Button_about.OnClick:=@arc;
   
   // Button_Help.OnClick:=@Temp_Start;   // temporary
   
   // T3.OnClick:=@Pomoc;  
   
   // EVENTS

  if not start_ok then button_ok.visible:=false;
  Button_ok.ModalResult:=mrOk;
  Button_Cancel.ModalResult:=mrCancel; 

   W_Window.showmodal;
   W_Window.free;             // close window
   
   
end.   // main         
