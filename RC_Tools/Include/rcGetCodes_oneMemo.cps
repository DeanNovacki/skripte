// kreira prozor s potro�nim i vra�a �ifru selektiranog potro�nog
// ulaz: �ifra potro�nog - da bi se lako provjerilo da li postoji
// izlaz: �ifra potro�nog


var
   // GLOBAL!!!

	// liste sa svim okovima
	rcListGrupa, 
	rcListNaziv,
	rcListSifra,
	rcListCijena,
	rcListSlika,
	
	// trenutno vidljivi okovi
	detGrupa, 
	detNaziv,
	detSifra,
	detCijena,
	detSlika : Tstringlist;


	UPpotwin:TForm;
		UPpotpan:TPanel;
			UPpanNaslov : TPanel;
				UPnasLabel : TLabel;
			UPpanLista : TPanel;
				UPListaOkov : tMemo;
			UPpanSearch : tPanel;
				UPeditSearch : tEdit;
				
			UPpanDetalji : tPanel;
				UPPanSlika : tPanel;
				UPslikaPot : tmImage;
				
				UPispGrupa,
			   UPispIme,
				UPispSifra,
				UPispCijena : TEdit;
				
				UPbutRefresh 	: tButton;
				UPlabRefresh : Tlabel;
				
				UPbutGet : tButton;
	
	
procedure upLoadLists;
// u�itava stare liste koje su (vjerojatno) nekad prije kreirane
var
	tempPath:string;
	
Begin
	tempPath:=prgFolder+'skripte\RC_Tools\Spojnice\Temp\';	
	// pripremi liste
	
	print('upLoadLists start');
	
	rcListGrupa :=TStringList.Create;
	rcListNaziv :=TStringList.Create;
	rcListSifra :=TStringList.Create;
	rcListCijena:=TStringList.Create;
	rcListSlika :=TStringList.Create;
	
	// u�itaj listu Grupa
	if FileExists(tempPath+'rcListGrupa.data') then begin
		try rcListGrupa.LoadFromFile(tempPath+'rcListGrupa.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListGrupa.data');
		end;  
	end else begin
		// lista ostaje prazna
	end;
	
	// u�itaj listu Naziv
	if FileExists(tempPath+'rcListNaziv.data') then begin
		try rcListNaziv.LoadFromFile(tempPath+'rcListNaziv.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListNaziv.data');
		end;  
	end  else begin
		// lista ostaje prazna
	End; 
	
	// u�itaj listu Sifra
	if FileExists(tempPath+'rcListSifra.data') then begin
		try rcListSifra.LoadFromFile(tempPath+'rcListSifra.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListSifra.data');
		end;  
	end  else begin
		// lista ostaje prazna
	End; 
	
	// u�itaj listu Cijena
	if FileExists(tempPath+'rcListCijena.data') then begin
		try rcListCijena.LoadFromFile(tempPath+'rcListCijena.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListCijena.data');
		end;  
	end  else begin
		// lista ostaje prazna
	End; 
	
	// u�itaj listu Slika
	if FileExists(tempPath+'rcListSlika.data') then begin
		try rcListSlika.LoadFromFile(tempPath+'rcListSlika.data');
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+tempPath+'rcListSlika.data');
		end;  
	end  else begin
		// lista ostaje prazna
	End; 

	// Print('Lista ima ' + IntToStr(rcListGrupa.Count)+' elemenata' );
	// Print('Lista ima ' + IntToStr(rcListNaziv.Count)+' elemenata' );
	// Print('Lista ima ' + IntToStr(rcListSifra.Count)+' elemenata' );
	// Print('Lista ima ' + IntToStr(rcListCijena.Count)+' elemenata' );
	// Print('Lista ima ' + IntToStr(rcListSlika.Count)+' elemenata' );

	
	print('upLoadLists end');
	
end;
	
procedure butRefreshOnClick(sender:tObject);	
var
	listFolder 	: TStringList;
	cxm 			: tStringList;
	red 			: string;
	grupa 		: String;
	i,j			: Integer;
	tempPath		: String;
Begin
	PrintTime ('upRefreshLists start')
	// otkrij grupe
	listFolder := TStringList.Create;
	listFolder := rcDirFiles(prgFolder+'Potrosni', '*.cxm');
	
	// test ListFolder-a
	// for i:=0 to ListFolder.count-1 do begin
	//		print (ListFolder[i]);
	// End;

	// pripremi liste
	rcListGrupa .Clear;
	rcListNaziv .Clear;
	rcListSifra .Clear;
	rcListCijena.Clear;
	rcListSlika .Clear;
	
	// napuni liste
	cxm :=TStringList.Create;
	// for i:=0 to ListFolder.Count-1 do begin   // pro�i kroz sve cxm-ove
	
	
	for i:=0 to 1 do begin   // pro�i kroz sve cxm-ove
		
		// kreiranje cxm liste - ZAJEDNI�KI potro�ni 
		cxm.Clear;
		
		Print ('Obra�ujem listu: '+ListFolder[i]); 
		// labRefresh.text:='U�itavam '+ListFolder[i];
		PrintTime ('Load start: '+ListFolder[i]); 
		try cxm.LoadFromFile(prgFolder+'\potrosni\'+ListFolder[i]);
			except ShowMessage( 'Ne mogu ucitati potrosni'+#13#10+ListFolder[i]);
		end; 
		PrintTime ('Load end: '+ListFolder[i]); 
		
		// pro�i sve redove cxm file-a
		PrintTime ('Extract start: '+ListFolder[i]); 
		for j:=0 to cxm.count-1 do begin
			red:=cxm[j];
			
			// Na�i naziv grupe grupe
			if Contain(red,'<Potrosni') then begin
				grupa := rcFindVar2(red,'FILE');

				j:=j+1; 					// idi u slijede�i red jer sam na�ao naziv grupe
				red:=cxm[j];
			end;	
				
			// puni Grupu, Naziv i �ifru
			if Contain(red,'<PotItem ')  then begin
				rcListGrupa.Add(grupa);
				rcListNaziv.Add(rcFindVar2(red,' Naziv'));
				rcListSifra.Add(rcFindVar2(red,' oznakaP'));
				j:=j+1; 					// idi u slijede�i red jer ovdje nema vi�e ni�ta zanimljivo
				red:=cxm[j];
			end;
			
			// puni Cijenu
			if Contain(red,'<Data ')  then begin	
				rcListCijena.Add(rcFindVar2(red,' Cijena'));
				j:=j+1; 					// idi u slijede�i red jer ovdje nema vi�e ni�ta zanimljivo
				red:=cxm[j];
			End;
			
			// puni putanju za sliku
			if Contain(red,'<Dodatno ')  then begin	
				rcListSlika.Add(rcFindVar2(red,' MainPic'));
				j:=j+1; 					// idi u slijede�i red jer ovdje nema vi�e ni�ta zanimljivo
			End;
			
			{
			ii:=rcListGrupa.count-1;
			print (	'RB:'+IntToStr(ii)+' ----------------' +#13#10+
						'GR:'+rcListGrupa[ii]+#13#10+
						'NA:'+rcListNaziv[ii]+#13#10+
						'SI:'+rcListSifra[ii]+#13#10+
						'CI:'+rcListCijena[ii]+#13#10+
						'SL:'+rcListSlika[ii]);
			}
			
		end; // for j - redovi u jednom cxmu
		PrintTime ('Extract end: '+ListFolder[i]); 
	
	end;  // for i - cxm-ovi
	
	// spremanje na disk
	tempPath:=prgFolder+'skripte\RC_Tools\Spojnice\Temp\';	
	

	rcListGrupa.SaveToFile(tempPath+'rcListGrupa.data');
	rcListNaziv.SaveToFile(tempPath+'rcListNaziv.data');
	rcListSifra.SaveToFile(tempPath+'rcListSifra.data');
	rcListCijena.SaveToFile(tempPath+'rcListCijena.data');
	rcListSlika.SaveToFile(tempPath+'rcListSlika.data');

	print ('nova baza spremljena na disc');
	
	
	PrintTime ('upRefreshLists end')
End;

Procedure upFillMemos;
var
	i:Integer;

Begin 
	print('upFillMemos start');

	UPListaOkov.Clear;
	
	For i:=0 to rcListGrupa.Count-1 do begin
		 // print(IntToStr(i));
		UPListaOkov.Lines.Add(rcListGrupa[i]+' | ' + rcListSifra[i]+ ' | ' + rcListNaziv[i]);
		// UPmemoNaziv.Lines.Add(rcListNaziv[i]);
		// UPmemoSifra.Lines.Add(rcListSifra[i]);
	
	
	End;

	print('upFillMemos end');
	
End;

// Procedure UPmemoSifraOnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);		
Procedure UPmemoSifraOnMouseDown(Sender: TObject);		
var
	ppp : integer;
	eee:TStringGrid;
Begin
	Print('Klik');
	// UPmemoSifra.SelStart:=3;
	//UPmemoSifra.SelLength:=10;
	// UPmemoSifra.lines.assign;
	UPListaOkov.SetFocus;

End;

		
Function UcitajPotrosni(sifra:string; left, top:integer):string;
var
	// listFolder 	: TStringList;
	i, j, ii		: integer;
	
Begin
	printTime ('UcitajPotrosni Start');
	
	print('Kreiram formu');
	// upKreirajFormu;
	// kreiraj formu
	UPpotWin:=rcTool('Potrosni');
	UPpotWin.FormStyle:=fsStayOnTop; 
	UPpotWin.Height:=500;
	UPpotWin.Width:=500;
	UPpotWin.Font.Size:=9;
	
		UPpanNaslov:=rcPanel('',UPpotWin);
		
			UPnasLabel:=rcLabel('', 'Grupa' + ' || ' + 'Sifra ' + ' || ' + 'Naziv' , UPpanNaslov, 5, 3, taLeftJustify);
			UPnasLabel.font.size:=10;

		UPpanLista:=rcPanel('',UPpotWin);
		UPpanLista.Height:=250;
		UPpanLista.Align:=alTop;
		
			// UPmemoOkov:=rcMemo('', UPpanLista, 1, 1, 100, 10);
			UplistaOkov:=tListBox.create(UPpanLista);
			UplistaOkov.parent:=UPpanLista;
			UplistaOkov.Align:=alClient;
			UplistaOkov.font.name:='Courier New';
			
			
		UPpanSearch:=rcPanel('',UPpotWin);
		UPpanSearch.Height:=22;
		UPpanSearch.Top:=1000;
		UPpanSearch.Align:=alTop;
		
			UPeditSearch:=rcEdit('',UPpanSearch, 1,1);
			UPeditSearch.Width:=100;
			UPeditSearch.Align:=alLeft;
		
		UPpanDetalji:=rcPanel('',UPpotWin);
		UPpanDetalji.Height:=170;
		UPpanDetalji.Top:=1000;
		UPpanDetalji.Align:=alClient;
		
			UPpanSlika:=rcPanel('',UPpanDetalji);
			UPpanSlika.Height:=150;
			UPpanSlika.Width:=150;
			UPpanSlika.Top:=10;
			UPpanSlika.Left:=10;
			// panSlika.Align:=alTop;
		
		UPispIme	:=rcEditL('Naziv' +':', '', UPpanDetalji, 280, 10, 'right');
		UPispGrupa	:=rcEditL('Grupa' +':', '', UPpanDetalji, 280, 30, 'right');
		UPispSifra	:=rcEditL('Sifra' +':', '', UPpanDetalji, 280, 50, 'right');
		UPispCijena:=rcEditL('Cijena'+':', '', UPpanDetalji, 280, 70, 'right');
		
		UPispIme.readonly:=true;
		UPispIme.color:=clWindow;
		UPispIme.Width:=UPpanDetalji.Width-240;
		
		UPispGrupa.readonly:=true;
		UPispGrupa.color:=clWindow;
		UPispGrupa.Width:=UPpanDetalji.Width-240;

		UPispSifra.readonly:=true;
		UPispSifra.color:=clWindow;
		UPispSifra.Width:=UPpanDetalji.Width-240;

		UPispCijena.readonly:=true;
		UPispCijena.color:=clWindow;
		UPispCijena.Width:=UPpanDetalji.Width-240;

		UPbutRefresh:=rcButton('', 'Obnovi', UPpanDetalji, 180, 105);
		UPbutRefresh.Height:=22;
		UPbutRefresh.Width:=100;
		UPbutRefresh.OnClick:=@butRefreshOnClick;

		UPLabRefresh:=rcLabel('', 'status', UPpanDetalji, 180, 135, taLeftJustify);
		UPLabRefresh.font.color:=clSilver;

		UPbutGet:=rcButton('', 'Preuzmi', UPpanDetalji, 360, 105);
		UPbutGet.width:=115;
		UPbutGet.Height:=55;
		
	UPpotWin.Left:=Left;
	UPpotWin.Top:=Top;
	UPpotWin.Show;
	
	// print (sifra);
	// UPeditSifra.Text:=sifra;
	
	
	
	
	print('Ucitavam liste');
	upLoadLists;			// u�itaj stare liste
	upFillMemos;			// napuni Memo-e

	
	
	printTime ('UcitajPotrosni End');
End;
