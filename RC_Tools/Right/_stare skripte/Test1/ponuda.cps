var
   Zagx1,ZagY1,ZagX2,ZagY2 : Integer;           // Zaglavlje
   kupx1, kupy1, kupx2,kupy2  : Integer;        // Adresa kupca
   podx1, pody1, podx2, pody2 : Integer;        // Podaci o kupcu
   nasx1, nasy1, nasx2, nasy2 : Integer;        // Naslov
   tabx1, taby1, tabx2, tabY2 : Integer;        // tablica
   k1, k2, k3, k4, k5, k6, k7, k8 :Integer;         // kolone
   kd1,kd2,kd3:integer;                             // doadatni podaci

   pw,ph:integer;            // sirina i visina stranice
   ypos:integer;             // trenutni Y
   Ukupno:single;            // ukupna cijena
   PDF:TpdfPrinter; 
  
   Vel_Font_Zagl_Tbl:integer;
   colarr:array[0..5] of integer;          // x vrijednosti za kolone
   topLine:integer;
   rb:integer;                             // redni broj 
   dfs:single; //veli�ina fonta  
   
Procedure crtaj_zone;
var
   my:integer;
Begin
     my:=Ypos+5;
     pdf.setRGBcolor(0,0,0); 
     pdf.bitmapout(30,25,pw/3,40,prgfolder+'\skripte\ponuda\logotip_red_cat.jpg');
     PDF.DrawLine(zagx1,70,zagx2,70);
     pdf.setfont('Arial',8);
     //pdf.setfont('Gothic',8);
     pdf.TextBox( ZagX2,33,0,taRightJustify,'�kolska 4, 10370 Dugo Selo');
     pdf.TextBox( ZagX2,43,0,taRightJustify,'Tel.: 01 410 2767, 095 89 111 44');
     pdf.TextBox( ZagX2,53,0,taRightJustify,'MB: 2883872, �iro ra�un: 2484008-1106154065');
     pdf.TextBox( ZagX2,63,0,taRightJustify,'OIB: 74191595828');
     {PDF.DrawRectangle(20,5,pw-20,ph-20);   
     PDF.DrawRectangle(Zagx1,ZagY1,ZagX2,ZagY2);     
     PDF.DrawRectangle(kupx1,kupy1,kupx2,kupy2);     
     PDF.DrawRectangle(podx1,pody1,podx2,pody2);}  
     //PDF.DrawRectangle(nasx1,nasy1,nasx2,nasy2);     //ponuda rect
     
end;

Procedure CRtaj_Tablicu;
var
   my,red:integer;
Begin
     my:=Ypos+5;     
     //PDF.DrawRectangle(20,5,pw-20,ph-20);           
     PDF.DrawRectangle(tabx1,taby1,tabx2,MY);  
     PDF.DrawLine(k1,taby1,k1,MY);
     PDF.DrawLine(k2,taby1,k2,MY);
     PDF.DrawLine(k3,taby1,k3,MY);
     PDF.DrawLine(k4,taby1,k4,MY);
     PDF.DrawLine(k5,taby1,k5,MY);
     PDF.DrawLine(k6,taby1,k6,MY);
     PDF.DrawLine(k7,taby1,k7,MY);
     PDF.DrawLine(k8,taby1,k8,MY);  
     
     // zaglavlje tablice
     pdf.setfont('Arial',Vel_Font_Zagl_Tbl);
     pdf.setrgbColor(0,0,0);
   
     red:=Vel_Font_Zagl_Tbl;         
     PDF.TextBox(k1,taby1+red*1,k2-k1,tacenter,'RB');
     PDF.TextBox(k2,taby1+red*1,k3-k2,tacenter,'�ifra');
     PDF.TextBox(k3,taby1+red*1,k4-k3,tacenter,'Opis');
     PDF.TextBox(k4,taby1+red*1,k5-k4,tacenter,'Kmj');
     PDF.TextBox(k5,taby1+red*1,k6-k5,tacenter,'Kol');
     PDF.TextBox(k6,taby1+red*1,k7-k6,tacenter,'Cijena');     
     PDF.TextBox(k7,taby1+red*1,tabx2-k7,tacenter,'Ukupno'); 
     PDF.DrawLine(tabx1,taby1+red*1+2,tabx2,taby1+red*1+2);     
   
end;

Procedure PrintFooter;
begin
     PDF.DrawLine(k1,ph-20,tabx2,ph-20);     
     pdf.setfont('Arial',5);
     PDF.Textbox(pw/2,ph-14,0,tacenter,'www.sven-namjestaj.hr  -  info@sven-namjestaj.hr');
end;

   
Procedure PrintEndPage;
var
   i:integer;
begin
     PDF.LineWidth:=0.1;
     pdf.SetRGBColor(0,0,0);     
     CRtaj_Tablicu;
     PrintFooter;
end;
   
Procedure CheckNewPage;
begin     
     if Ypos+20>ph-20 then begin        
        PrintEndPage;
        pdf.newpage;
        taby1:=20;        
        Ypos:=taby1+1+Vel_Font_Zagl_Tbl; 
     end;
end;

Function FindRuckica(felm:telement):telement;
   var
      j:integer;
      ae:telement;
      d:tdaska;
   begin
        result:=nil;
        for j := 0 to Felm.childdaska.count-1 do begin
            d:=Felm.childdaska.daska[j];
            if integer(d.tipdaske)=1 then begin
               if d.RuckeList.count>0 then begin
                  if d.RuckeList.items[0] is telement then begin
                     result:=telement(d.RuckeList.items[0]);
                     exit;
                  end;  
               end;               
            end;
        end;
        for j:= 0 to felm.elmlist.count-1 do begin
            ae:=felm.elmlist.element[j];
            if (integer(ae.tipelementa)=14)then begin
               result:=ae;
               exit;
            end else begin
                result:=FindRuckica(ae);
                if result<>nil then exit;
            end;
        end;
   end;

Function GetRuckicaName:string;
var
   i:integer;
   elm,ru:telement; 
   pt : TPotrosniOsnovni;
   po:Tpotrosni;     
begin
     result:='';
     for i := 0 to ElmHolder.elementi.count-1 do begin
         elm:=telement(ElmHolder.elementi.items[i]); 
         if (integer(elm.tipelementa)>=2)and (integer(elm.tipelementa)<=5)or (integer(elm.tipelementa)=15) then begin
            ru:= FindRuckica(elm);
            if ru <>nil then begin
               po:=ru.potrosni;
               if po.count>0 then begin
                  pt := TPotrosniOsnovni(po.Getitem(0));
                  result:=pt.Naziv;
                  exit;
               end;
            end;
         end;  
     end;
end;

Function GetBojaFronte:integer;
var
   j,i:integer;
   inE,elm:telement;
begin     
     result:=-1;     
     for i := 0 to ElmHolder.elementi.count-1 do begin         
         elm:=telement(ElmHolder.elementi.items[i]);
         if integer(elm.tipelementa)=2 then begin
            for j:= 0 to elm.childdaska.count-1 do begin
             if integer(elm.childdaska.daska[j].tipdaske)=1 then begin                
                result:=elm.childdaska.daska[j].texindex;
                exit;
             end; 
            end;    
            for j := 0 to elm.elmlist.count-1 do begin
             ine:=elm.elmlist.element[j];
             if (integer(ine.tipelementa)=7) then
             if integer(ine.childdaska.daska[0].tipdaske)=1 then begin                
                result:=ine.childdaska.daska[0].texindex;
                exit;
             end;
            end;
         end;
     end;
end;

Function GetBojaUklade:integer;
var
   j,i,k:integer;
   inE,elm:telement;
begin     
     result:=-1;     
     for i := 0 to ElmHolder.elementi.count-1 do begin         
         elm:=telement(ElmHolder.elementi.items[i]);
         if (integer(elm.tipelementa)>=2)and (integer(elm.tipelementa)<=5)or (integer(elm.tipelementa)=15) then begin            
            for j := 0 to elm.elmlist.count-1 do begin
                ine:=elm.elmlist.element[j];
                if (integer(ine.tipelementa)=7) then
                   for k:= 0 to ine.childdaska.count-1 do begin
                       if integer(ine.childdaska.daska[k].tipdaske)=1 then begin                
                          if integer(ine.childdaska.daska[k].tipfronte) =8 then begin 
                             result:=ine.childdaska.daska[k].texindex;
                             exit;
                          end;
                       end;
                   end;
            end;
         end;
     end;
end;


Function GetBojaTipa(TipE,TipD:integer):integer;
var
   j,i:integer;
   inE,elm:telement;
begin     
     result:=-1;     
     for i := 0 to ElmHolder.elementi.count-1 do begin         
         elm:=telement(ElmHolder.elementi.items[i]);
         if integer(elm.tipelementa)=TipE then begin
            for j:= 0 to elm.childdaska.count-1 do begin
             if integer(elm.childdaska.daska[j].tipdaske)=TipD then begin                
                result:=elm.childdaska.daska[j].texindex;
                exit;
             end; 
            end;
         end;             
     end;
end;

Function GetMatName(value:integer):string;
var
   tm:TMaterijalObj;
begin
     result:='';
     tm:=GetMaterijalData(value);
     if tm= nil then exit;
     result:=tm.naziv;
end;

Function ZbrojiTip(tip:integer;var Ucj:single):single;
var
   i:integer;
   elm:telement;
   L:single;
begin
     result:=0;
     ucj:=0;
     for i :=0 to  ElmHolder.elementi.count-1 do begin
         elm:=telement(ElmHolder.elementi.items[i]);
         if integer(elm.tipelementa)=tip then begin
            l:=elm.childdaska.daska[0].visina;
            if elm.childdaska.daska[0].smjer<>horiz then L:=elm.childdaska.daska[0].dubina; 
            result:=result+L;
            ucj:=ucj+elm.cijena;
         end;
     end;
     result:=result/1000;
     if result>0 then ucj:=ucj/result;
end;
   
   
Procedure PrintElmData(Rb:integer;sifra,naziv,JedMj:string;Kol,cjena:single);
begin
     if rb>0 then
        PDF.TextBox(k1,ypos,k2-k1-2,taRightJustify,inttostr(rb));        // redni broj elementa u ponudi
     PDF.TextOut(k2,ypos,sifra);           // sifra 
     PDF.TextOut(k3,ypos,naziv);           // naziv
     if dfs>8 then pdf.setfont('Arial',8);
     PDF.TextBox(k4,ypos,k5-k4,taCenter,JedMj);        // napomena
     if dfs>8 then pdf.setfont('Arial',10);
     PDF.TextBox(k5,ypos,k6-k5-2,taRightJustify,floattostrC1(Kol,8,2));                                         
     PDF.TextBox(k6,ypos,k7-k6-2,taRightJustify,floattostrC1(cjena,8,2));                       
     PDF.TextBox(k7,ypos,tabx2-k7-2,taRightJustify,floattostrC1(Kol*cjena,8,2));
end;
   
procedure PrintOstalo;
var
   i:integer;
   tk:tkomadni; 
   red:integer;
   cj:single;  
begin
     if (elmholder.fpotmat<>nil) then begin
        if elmholder.fpotmat.count>0 then begin
           Ypos:=Ypos+5;
           PDF.LineWidth:=1.5;     
           PDF.DrawLine(k1,Ypos,tabx2,Ypos);
           Ypos:=Ypos+10;
           PDF.TextOut(k3,ypos,'Oprema:');
           PDF.LineWidth:=0.1;
           Ypos:=Ypos+3; 
           PDF.DrawLine(k1,Ypos,tabx2,Ypos);
           Ypos:=Ypos+1;
           pdf.setfont('Arial',10);
           dfs:=10;
           //PDF.Font.Size:=10;
           red:=10;  
           for i := 0 to elmholder.fpotmat.count-1 do begin
               tk:=tkomadni(elmholder.Fpotmat.GetItem(i));
               if tk<>nil then begin
                  rb:=rb+1; 
                  ypos:=ypos+red;  
                  cj:=tk.cijena;
                  PrintElmData(rb,tk.oznaka,tk.naziv,'kom',tk.komada,cj);
                  ukupno:=ukupno+cj*tk.komada;
                  CheckNewPage; 
               end;
           end;
        end;
     end;
end;
   
Procedure PrintDodatnaProdaja;
var
   Dp:TdodatnaProdaja;
   RDp:TredDodatneProdaje;
   i:integer;
begin
     dp:=ElmHolder.DodatnaProdaja;     
     if dp.count=0 then exit;
     Ypos:=Ypos+5;
     PDF.LineWidth:=1.5;     
     PDF.DrawLine(k1,Ypos,tabx2,Ypos);
     Ypos:=Ypos+10;
     PDF.TextOut(k3,ypos,'Ostalo:');
     PDF.LineWidth:=0.1;
     Ypos:=Ypos+3; 
     PDF.DrawLine(k1,Ypos,tabx2,Ypos);
     Ypos:=Ypos+10;          
     dfs:=10;
     pdf.setfont('Arial',dfs);       
     for i := 0 to dp.count-1 do begin
         RDp:=dp.RedProdaje[i];
         PDF.TextOut(k1,ypos,inttostr(i+1));
            PDF.TextOut(k2,ypos,rdp.sifra);                                         
            PDF.TextOut(k3,ypos,rdp.opis); 
            pdf.setfont('Arial',8);
            PDF.Textbox(k4,ypos,k5-k4,tacenter,rdp.jmj);
            pdf.setfont('Arial',10);           
            PDF.TextBox(k5,ypos,k6-k5-2,taRightJustify,floattostrC1(rdp.kolicina,8,2));            
            PDF.TextBox(k6,ypos,k7-k6-2,taRightJustify,floattostrC1(rdp.cijena,8,2)); 
            PDF.TextBox(k7,ypos,tabx2-k7-2,taRightJustify,floattostrC1(rdp.cijena*rdp.kolicina,8,2));           
            ukupno:=ukupno+rdp.cijena*rdp.kolicina;
            if i<dp.count-1 then ypos:=ypos+round(dfs);
            CheckNewPage;
     end;
end;
   
Procedure PrintAccesories(tl:tlist);
var
   tk:tkomadni;
   i,red:integer;
   cj:single;
begin
     pdf.setfont('Arial',8);
     dfs:=8;
     red:=10;      
     for i := 0 to tl.count-1 do begin
         tk:=tkomadni(tl.Items[i]);
         if tk<>nil then begin                        
            ypos:=ypos+red;  
            cj:=tk.cijena;
            PrintElmData(-1,tk.oznaka,tk.naziv,'kom',tk.komada,cj);                                                                                                              
            ukupno:=ukupno+cj*tk.komada;            
            CheckNewPage;
         end;
     end;
     ypos:=ypos+2;
end;


Function PrintDodatniPodaci:integer;
var
   st:string;
   red,boja:integer;
   sy:integer;
begin
     sy:=ypos+15;
     pdf.setfont('Arial',10);
     dfs:=10;
     PDF.setRgbcolor(0,0,0);       
     red:=10;
     sy:=sy+red;
     boja:=GetBojaFronte;
     st:=GetMatName(boja);
     PDF.TextOut(kd1,sy,'Boja fronte: '+st);
     boja:=GetBojaTipa(2,0);
     st:=GetMatName(boja);
     PDF.TextOut(kd2,sy,'Boja korpusa: '+st);
     boja:=GetBojaTipa(8,4);
     st:=GetMatName(boja);
     PDF.TextOut(kd3,sy,'Radna Ploca: '+st);
     sy:=sy+red;
     boja:=GetBojaTipa(19,3);
     st:=GetMatName(boja);
     PDF.TextOut(kd1,sy,'Sokla: '+st);
     boja:=GetBojaUklade;
     st:=GetMatName(boja);
     PDF.TextOut(kd2,sy,'Uklada: '+st);
     st:=GetRuckicaName;        
     PDF.TextOut(kd3,sy,'Ru�kica: '+st);
     result:=sy;
end;

   
Procedure PrintElementLine;
var
   elm:telement;
   red,i,sy:integer;
   cj:single;
   tl:tlist;
   arb:integer;
   Tipkol,Tipcj:single;
   pdv:single;
begin
     rb:=0;
     tl:=nil;
     
     pdf.setfont('Arial',10);
     dfs:=10;
     PDF.setRgbcolor(0,0,0);       
     red:=10;
     ypos:=taby1+red;       
           
     if ElmHolder<>nil then begin                               // ima elemenata
        for i := 0 to elmholder.Elementi.count-1 do begin
          
            elm:=Telement(ElmHolder.Elementi.items[i]);            
            if (integer(elm.tipelementa)<>8)and (integer(elm.tipelementa)<>19)and (integer(elm.tipelementa)<>20) then begin
               rb:=rb+1; 
               ypos:=ypos+red;  
               cj:=elm.cijena;
               PrintElmData(rb,elm.sifra,elm.OpisElementa,'kom',1,cj);                                                                                         
               ukupno:=ukupno+cj;
                           
               CheckNewPage;
               elm.GetAccessoriesList(tl);
               if tl <>nil then begin               
                  PrintAccesories(tl);
                  tl.free;
                  tl:=nil;
                  pdf.setfont('Arial',10);
                  dfs:=10;
                  PDF.setRgbcolor(0,0,0);          
                  red:=10;
               end;            
            end;
        end;
        tipkol:= ZbrojiTip(8,tipcj); 
        if tipkol>0 then begin
           rb:=rb+1; 
           ypos:=ypos+red;        
           PrintElmData(rb,'','Radna plo�a','m',tipkol,tipcj);
           ukupno:=ukupno+tipkol*tipcj;
         end;
        tipkol:= ZbrojiTip(19,tipcj);  
        if tipkol>0 then begin            
           rb:=rb+1; 
           ypos:=ypos+red;        
           PrintElmData(rb,'','Cokla','m',tipkol,tipcj);
           ukupno:=ukupno+tipkol*tipcj;
        end;
        tipkol:= ZbrojiTip(20,tipcj);
        if tipkol>0 then begin             
           rb:=rb+1; 
           ypos:=ypos+red;        
           PrintElmData(rb,'','Zidna letva','m',tipkol,tipcj);
           ukupno:=ukupno+tipkol*tipcj;
         end;      
     end;

      
     PrintOstalo;
     PrintDodatnaProdaja;
     sy:=PrintDodatniPodaci;     
     pdf.FontStyles:=[fsBold];
     pdf.setfont('Arial',10);
     PDF.setRgbcolor(0,0,0);    
     PDF.Textbox(pw-130,sy+30,0,taRightJustify,'Osnovica:');
     PDF.Textbox(Zagx2,sy+30,0,taRightJustify,floattostrC1(ukupno,8,2));
     PDF.Textbox(pw-130,sy+45,0,taRightJustify,'PDV 25% :');
     pdv:=round(ukupno*0.25*100)/100;
     PDF.Textbox(Zagx2,sy+45,0,taRightJustify,floattostrC1(pdv,8,2));
     PDF.DrawLine(pw-200,sy+46,Zagx2,sy+46);
     PDF.Textbox(pw-130,sy+60,0,taRightJustify,'Ukupno:');
     PDF.Textbox(Zagx2,sy+60,0,taRightJustify,floattostrC1(ukupno+pdv,8,2));
     pdf.FontStyles:=[];
end;

   
Procedure PrintPdf(fn:string);
var    
   img:tmimage;
   s:string;
   red:integer; 
     
begin
     
     PDF.FileName := fn;
     
     
     
     PDF.LineWidth:=0.1;
     pdf.SetRGBColor(0,0,0);
     
     crtaj_zone;
 
     { 
                             `
     img:=tmimage.create(nil);
     try
        img.loadfromfile('N:\Tri D Corpus\PDF_slike\metar.bmp');     
        PDF.Draw(0,0,img);
     except
           showmessage('promjeni putanju za sliku'); 
     end;
     img.free;
      }  
     // PDF.LineWidth:=1;
     // pdf.SetRGBColor(0.3,0.6,0.2);
     // PDF.DrawLine(12,topLine,pw-12,topLine);

     // PDF.LineWidth:=1;     
     // PDF.DrawRectangle(20,20,pw-20,ph-20);
     // ***********************************************************

     pdf.setfont('Arial',12);
     pdf.FontStyles:=[fsBold];
     PDF.setRgbcolor(0,0,0);  
     PDF.TextOut(Nasx1,Nasy2,'PONUDA: '+ElmHolder.ProjectOpisData.BrojPonude);
     
     Pdf.setfont('Arial-Bold',15);  
     red:=12;
    
     PDF.TextOut(Kupx1,Pody1+(red*1),ElmHolder.ProjectOpisData.KupacNaziv);
     pdf.FontStyles:=[];
     Pdf.setfont('Arial',10);    
     red:=11;       
     PDF.TextOut(Kupx1,Pody1+(red*2),ElmHolder.ProjectOpisData.KupacAdr);
     PDF.TextOut(Kupx1,Pody1+(red*3),'MB: '+ElmHolder.ProjectOpisData.KupacMB);
     PDF.TextOut(Kupx1,Pody1+(red*4),'Kontakt: '+ElmHolder.ProjectOpisData.KupacKontakt);
     PDF.TextOut(Kupx1,Pody1+(red*5),'Telefon: '+ElmHolder.ProjectOpisData.KupacTelefon);
     PDF.TextOut(Kupx1,Pody1+(red*6),'Mobitel: '+ElmHolder.ProjectOpisData.KupacMobitel);
     PDF.TextOut(Kupx1,Pody1+(red*7),'EMail  : '+ElmHolder.ProjectOpisData.KupacEMail);
     
     red:=6;    
     Pdf.setfont('Arial',6);
     
     
     PDF.TextOut(Podx1,Pody1+(red*1),'Po radnom nalogu: '+ElmHolder.ProjectOpisData.BrojRadnogNaloga);
     PDF.TextOut(Podx1,Pody1+(red*2),'Datum izrade    : '+DateToStr(ElmHolder.ProjectOpisData.DatumIzrade));
//     PDF.TextOut(Podx1,Pody1+(red*3),'TrgovacProjektIzradio: '+ElmHolder.ProjectOpisData.TrgovacProjektIzradio);    
//     PDF.TextOut(Podx1,Pody1+(red*4),'TrgovacKontakt: '+ElmHolder.ProjectOpisData.TrgovacKontakt);       
//     PDF.TextOut(Podx1,Pody1+(red*5),'TrgovacProdajnoMjesto: '+ElmHolder.ProjectOpisData.TrgovacProdajnoMjesto);
     PDF.TextOut(Podx1,Pody1+(red*5),'Isporuka');  
     PDF.TextOut(Podx1,Pody1+(red*7),'Naziv: '+ElmHolder.ProjectOpisData.PrimaNaziv);  
     PDF.TextOut(Podx1,Pody1+(red*8),'Adresa: '+ElmHolder.ProjectOpisData.PrimaAdresa);       
     PDF.TextOut(Podx1,Pody1+(red*9),'Grad: '+ElmHolder.ProjectOpisData.PrimaGrad);       
     PDF.TextOut(Podx1,Pody1+(red*10),'Tel: '+ElmHolder.ProjectOpisData.PrimaTel);       
     PDF.TextOut(Podx1,Pody1+(red*11),'Mob: '+ElmHolder.ProjectOpisData.PrimaMob);       
     PDF.TextOut(Podx1,Pody1+(red*12),'Kat: '+IntToStr(ElmHolder.ProjectOpisData.PrimaKatUZgradi));            
//     PDF.TextOut(Podx1,Pody1+(red*13),'PrimaLift: '+ElmHolder.ProjectOpisData.PrimaLift);   
//     PDF.TextOut(Podx1,Pody1+(red*14),'PrimaKatUZgradi: '+ElmHolder.ProjectOpisData.PrimaKatUZgradi);   
//     PDF.TextOut(Podx1,Pody1+(red*15),'PrimaVrijemeIsporuke: '+ElmHolder.ProjectOpisData.PrimaVrijemeIsporuke);   
//     PDF.TextOut(Podx1,Pody1+(red*16),'PrimaDatumIsporuke: '+DateToStr(ElmHolder.ProjectOpisData.PrimaDatumIsporuke));   
                           
   
     
  
     PrintElementLine;    
     PrintEndPage;
     
      {
      }
     PDF.EndDoc;
     PDF.ShowPdf;

     

end;


// Main program

begin
   PDF:=TpdfPrinter.create(1250);
   //PDF.Compress:=true; 
   PDF.TITLE := 'Ispis ponude';                     // ?
   pdf.newpage;  
     pw:=PDF.PageWidth; // �irina stranice
     ph:=PDF.PageHeight; // visina stranice
     
   Zagx1:=25;            // Zaglavlje
   ZagY1:=10;
   ZagX2:=pw-25;
   ZagY2:=ZagY1+80;         // +visina
   Vel_Font_Zagl_Tbl:=10;     
   kupx1:=Zagx1;            // adresa
   kupy1:=zagy2+5;       // 5 = razmak
   kupx2:=kupx1+300;
   kupy2:=kupy1+80;     // 200 = visina
        
   podx1:=kupx2+40;       // Podaci o kupcu
   pody1:=zagy2+5;
   podx2:=ZagX2;//podx1+220;
   pody2:= pody1+80;   //+80
        
   nasx1:=50;       // Naslov
   nasy1:=pody2+15;
   nasx2:=530;
   nasy2:=nasy1+10;

   
   tabx1:=Zagx1;         // tablica
   taby1:=nasy2+15;
   tabx2:=ZagX2;
   tabY2:=taby1+400; 

   k1:=tabx1+0;
   k2:=tabx1+20;
   k3:=tabx1+100;
   k4:=tabx1+340;
   k5:=tabx1+360;
   k6:=tabx1+400;         
   k7:=tabx1+460; 
   k8:=tabx1+0; 

     colarr[0]:=20;
     colarr[1]:=40;
     colarr[2]:=150;
     colarr[3]:=400;
     colarr[4]:=500;
     colarr[5]:=550;
     topLine:=100;                            // pocetak tablice
     
    kd1:=tabx1;
    kd2:=tabx1+round(tabx2/3);
    kd3:=tabx1+round(tabx2/3*2);
    PrintPdf(prgfolder+'\skripte\ponuda\test2.pdf');         // prgfolder = polo�aj Corpus.exe
    PDF.Free;
end.