
{$I ..\include\rcPrint.cps}

var // progress bar
	MessForm: TForm; messLab1, messLab2 : tLabel; box1, box2 : tShape;
{$I ..\include\rcProgress2.cps}

// {$I ..\include\rcMessages.cps}
{$I ..\include\rcStrings.cps}
{$I ..\include\rcControls.cps}
// {$I ..\include\rcObjects.cps}
{$I ..\include\rcConfig.cps}
// {$I ..\include\rcEvents.cps}
// {$I ..\include\rcCurves.cps}
// {$I ..\include\rcMath.cps}
// {$I ..\include\rcOut.cps}
{$I ..\include\rcTranslate.cps}
{$I ..\include\rcSystem.cps}
{$I ..\include\rcGetCodes.cps}

var
	
	versiontxt:string;
	jezik:string;
	brand:string;
	sPath:string;
	main:TForm;
	PanMain : TPanel;
		PanDown : TPAnel;
			ButOK, ButCancel, ButPomoc : TButton;
	
	Odabir:Integer;
	selektiran:boolean;
	ii:Integer;
	BrojSlotova:Integer;
	Obj2Udaljenost:Integer;
	tt:string;

Function rcStringGrid(pParent: TWinControl):TListView;
var
   StrGrd:TStringGrid;
 begin
	StrGrd:=TStringGrid.create(pParent);
	StrGrd.parent:=pParent;
	result:=StrGrd;
end;

procedure ButOK_OnClick(sender:TObject);
var
	sss:string;
	DefPath:string;
	slsl:tStringList;
Begin
	print ('Procedura ButOK_OnClick start')
	
End;	

Procedure ButCancel_OnClick(sender:TObject);
Begin
	Main.close
End;

var
	sg:TStringGrid;

Procedure Create_Main_Window;
var 

	i:Integer;
Begin
	// Print('Create_Main_Window start');


	// Startni prozor
   Main:=rcTool('Test'); 
	Main.Width:=500;
	Main.Height:=500;
 	Main.top:=100;
	Main.Left:=120;
	// Main.BorderStyle:=bsSizeToolWin;
	Main.BorderIcons:=[biSystemMenu];
	// Main.FormStyle:=fsStayOnTop;	

	sg:=rcStringGrid(Main);
	
	
	// Panel Down
	PanDown:=rcPanel('Pan_Down',Main);
	PanDown.Align:=alBottom;
	PanDown.Height:=40;
	
	ButOK:=rcButton('btok','Napravi',PanDown,main.width-120,8);
	ButOK.Width:=100;
	ButOK.OnClick:=@ButOK_OnClick;
	ButOK.enabled:=false;
	ButOk.Hint:='Treba popuniti obavezna polja (ozna�ena �uto)';	
	
	ButCancel:=rcButton('btCancel','Zatvori',PanDown,main.width-240,8);
	ButCancel.Width:=100;
	ButCancel.OnClick:=@ButCancel_OnClick;
	
	ButPomoc:=rcButton('btPomoc','Pomo�',PanDown,main.width-360,8);
	ButPomoc.Width:=100;
	ButPomoc.enabled:=False;
	// ButPomoc.OnClick:=@ButPomoc_OnClick;
	
	ButOK.default:=True;
	ButCancel.ModalResult:=mrCancel;

End;

// Glavni   
Begin
	Print ('==============');
	
	// rcCreateFolder(PrgFolder+'Skripte','Dex1101');
	//	exit;
	
	
   versiontxt:='1.1';	
	Print ('==============');
	PrintTime(' Skripta: "Dodavanje spojnica" '+versiontxt+ // #13#10+ 
								 ' start');
	sPath:=PrgFolder+'Skripte\RC_Tools\Test\';

	Create_Main_Window;
	Main.Caption:='Dodavanje spojnica '+versiontxt;
	Main.Show; 
	Main.FormStyle:=fsStayOnTop; 	
			
	while Main.visible do begin
			   application.processmessages;
	end;	// while Main.visible
	
	// print('nakon "While Main visible" petlje ');
	
   Main.Close;			// ovo �alje u sFormOnCloseQuery 

	PrintTime(' Skripta: "Test" '+versiontxt+ // #13#10+ 
								 ' end');
	Print('____________________');

   
   
End.