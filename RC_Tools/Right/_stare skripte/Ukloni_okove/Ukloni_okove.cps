Program Delete_fittings;  
// v1.0 (19.08.2015)
// Skripta koja uklanja potrosni materijal

   {$I math_rot.cps}
   {$I dex_unit.cps}
         
var
   Prozor:TForm;
   T1, T2, T3: TButton;
   M1:TMemo;
   N1, N2:TLabel;
Procedure ukloni_potrosni(po:TPotrosni);
var
   pt:TPotrosniOsnovni;
   i:integer;
   
begin

  for i:=po.count-1 downto 0 do begin
    pt := TPotrosniOsnovni(po.Getitem(i));  
    if pt.tip = Komadni then begin
       m1.lines.add ('Uklonjen potrosni: "'+pt.naziv+'", kolicina: '+ floattostr(tKomadni(pt).komada));    
       po.removeItem(i);

    end;
  end; 
end;


Procedure ukloni_potrosni_iz_elementa(el:TElement);
var
   i:integer;
begin 
   ukloni_potrosni(el.potrosni);
   for i:=0 to el.childdaska.count-1 do begin
      ukloni_potrosni(el.childdaska.daska[i].potrosni);

   end;
   for i:=0 to el.elmlist.count-1 do ukloni_potrosni_iz_elementa(el.elmlist.element[i]);

end;




Procedure Obrada_projekta(sender:Tobject);
// obra�uje selektirane elemente koji se nalaze u projektu

var
   i:integer;
   elem:Telement;

begin

  m1.lines.add ('-------------------------');
  
  if ElmList.Count=0 then m1.lines.add ('U PROJEKTU NEMA ELEMENATA!');
  
  for i:=0 to ElmList.Count-1 do begin
      elem:=TElement(ElmList.Items[i]);
      if  elem.selected then begin
         m1.lines.add ('======>  El: "'+elem.naziv+', Br. podelemenata: '+IntToStr(elem.ElmList.CountObj)+'   <=======');
         ukloni_potrosni_iz_elementa(elem);
      end else m1.lines.add ('El: "'+elem.Naziv+'" nije selektiran niti obra�en.');
  m1.lines.add ('-------------------------');
  end;
    m1.lines.add ('Posao zavrsen!');
end;

procedure Pomoc(sender:Tobject);
var
   pTekst:string;
begin
        pTekst:='FR v1.0'+#13#10+
                'Skripta za uklanjanje potrosnog '+#13#10
                'materijala na selektiranim elementima.'+#13#10+#13#10+
                ''+#13#10+
                'UPOZORENJE!!!'+#13#10+
                'Nakon prvog iduceg izvrsavanja makroa vratit ce se potrosni'+#13#10+
                'kojeg dodaju makroi'+#13#10+#13#10+
                'Skripta ne radi u editoru nego samo u projektu.';
        ShowMessage(pTekst);
     
end;   


Begin
  //showmessage ('Fittings remover');
  Prozor:=Window('Red Cat',420,440);
  naslov('Brisanje potro�nog materijala',Prozor);
  M1:=Memo(Prozor,10,120,395,250);
  M1.ScrollBars:=ssVertical;
  
  N1:=natpis('Selektiranim elementima �e biti obrisan sav potro�ni materijal!',Prozor,15,50,'lijevo');
  N2:=natpis('Ako u elementu postoje makroi, mogu�e je da �e oni'+#13#10+
				'ponovo generirati potro�ni materijal.',Prozor,15,70,'lijevo');  
  N1.font.size:=10;
  N2.font.size:=10;
       
  T1:=Tipka('Napravi',Prozor,10,375);
  // T2:=Tipka('Pomo�',Prozor,90,370);
  T3:=Tipka('Izlaz',Prozor,330,375);
  
  T1.default:=True;
  
  T1.OnClick:=@Obrada_Projekta;
  // T2.OnClick:=@Pomoc;
  T3.ModalResult:=mrCancel;          // ide dalje (iza prozor.showmodal)
  prozor.showmodal;
  Prozor.free;
  
End.
